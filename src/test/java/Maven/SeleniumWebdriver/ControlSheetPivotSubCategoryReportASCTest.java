package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.TreeMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ControlSheetPivotSubCategoryReportASCTest extends
		ControlSheetPivotReportASCTest {

	@Before
	public void setUp() throws Exception {
		mySheetName = TestFileController.ControlSheetPivot_SubCatWorksheetName;
		testName = "ControlSheetPivotSubCategoryReportASCTest";
		testCaseNumber = "027";
		testDescription = "ASC Test of Control Sheet SubCategory Report";
		logFileName = "Control Sheet Pivot SubCategory ASC Test.txt";
		logFileWriter = TestFileController.getLogFileWriter(logDirectoryName  +"/" + logFileName);
		criteria = new TreeMap<String, String>();

		expectedStringAttributeNames = new String[6];
		actualStringAttributeNames = new String[6];
		expectedStringAttributeNames[0] = actualStringAttributeNames[0] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.StageIndex];
		expectedStringAttributeNames[1] = actualStringAttributeNames[1] = ASCFacultyValues.StringAttributeKeys[ASCFacultyValues.ClusterIndex];
		expectedStringAttributeNames[2] = actualStringAttributeNames[2] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.CategoryIndex];
		expectedStringAttributeNames[3] = actualStringAttributeNames[3] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.DepartmentIndex];
		expectedStringAttributeNames[4] = actualStringAttributeNames[4] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.FacultyIndex];
		//Description attribute has a bug - HANDSON-3269 - associated with it.  It's not copied from the ASC to the Control Sheet Detail Report.
		expectedStringAttributeNames[5] = actualStringAttributeNames[5] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.DescriptionIndex];
		
		expectedAmountAttributeNames = new String[0];
		actualAmountAttributeNames = new String[0];

		expectedSummaryCriteria = new TreeMap<String, ArrayList<String>>();
		ArrayList<String> list = new ArrayList<String>();
		list.add(ExpectedASCData.StringAttributeKeys[ExpectedASCData.FacultyIndex]);
		list.add(ExpectedASCData.StringAttributeKeys[ExpectedASCData.SubCategoryIndex]);
		list.add(ExpectedASCData.StringAttributeKeys[ExpectedASCData.StageIndex]);
		expectedSummaryCriteria.put("FTE Adjusted DO Share Amount", list);
		
		/*
		 * of note, we have to set the pivotCriteria as well, which is going to be complex: 
		 * Sub-Category (Pivot Reports) vs. Subcategory (Control Sheet Detail Report)
		 * Suggestion: implement a method which replaces the pivot criteria string as
		 * input to the SSExcelReader method or the ExpectedASCData method when they have
		 * blanks, replace with empty, when they have a dash, replace with empty, 
		 * and of course it should be equalsIgnoreCase to eliminate the potential case sensitivity
		 * problem.
		 */
		
		pivotCriteriaName = ExpectedASCData.StringAttributeKeys[ExpectedASCData.SubCategoryIndex];
		outputMessage("attribute name being used as pivot criteria is "+pivotCriteriaName + "\n", logFileWriter);
		pivotCriteriaValues = HandSOnTestSuite.expectedASCDataSet.getAllDiscreteStringAttributeValues(pivotCriteriaName, logFileWriter);

		//pivotCriteriaName = "Sub-Category";
		String delimiterValueName = "Sub-Category";
		searchDelimiters = new ArrayList<String[]>(pivotCriteriaValues.length);
		for (int i=0; i<pivotCriteriaValues.length; i++){
			if (pivotCriteriaValues[i].equalsIgnoreCase("Reappointment")){
				pivotCriteriaValues[i] = "Reappointments";
			}
			String[] criterion = {delimiterValueName, pivotCriteriaValues[i]};
			searchDelimiters.add(criterion);
		}//end for loop

		outputMessage(delimiterValueName + " values being used as delimiters are as follows: ", logFileWriter);
		for (int i=0; i<pivotCriteriaValues.length; i++)
			outputMessage(pivotCriteriaValues[i] +", ", logFileWriter);
		outputMessage("\n", logFileWriter);
		
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void test() throws Exception{
		super.test();
	}

}
