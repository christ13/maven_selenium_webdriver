package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;

import org.openqa.selenium.By;
/*
 * This Class will control all interactions between the data and test Classes and the UI of the Retention
 * on the screen at any particular moment.  It does not keep track of either current or historical data.
 * Its only concern is setting or getting data from the UI at any particular point in time.
 */
public class RetentionUIController extends UIController {

	public static final String newRetentionURL = "showRetention.do?faculty";
	public static final String savedRetentionURL = "showRetention.do?retId=";
	public static final String retentionURL = "showRetention.do";
	public static final String myGeneralWorksheetName = "Retention General Tab";
	public static final String standardRetentionType = "Preemptive";

	public RetentionUIController() throws Exception{
		RetentionMasterDataController.writeToLogFile("RetentionUIController Class has been instantiated");
	}//end constructor

	public static void fillOutGeneralRetentionInformation(BufferedWriter logFileWriter, String testCaseID) throws Exception{
		logFileWriter.write("Now filling out General Information for new Retention");
		logFileWriter.newLine();
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myGeneralWorksheetName, "RetentionTypeSelect", logFileWriter);
		if (UIController.selectElementInMenu(by, standardRetentionType)) {
			logFileWriter.write("Successfully selected Retention Type of "+standardRetentionType);
			logFileWriter.newLine();
		}//end if
		else {
			logFileWriter.write("Could not select Retention Type of "+standardRetentionType);
			logFileWriter.newLine();
			return;
		}//end else
		Thread.sleep(1000);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myGeneralWorksheetName, "RetentionOpenDateTextfield", logFileWriter);
		String todaysDate = TestDataRecorder.getTestCaseData(testCaseID, "Today’s Date");
		HandSOnTestSuite.searchAuthorizationUI.writeStringToTextField(by, todaysDate, logFileWriter);
		Thread.sleep(1000);
		logFileWriter.newLine();
		logFileWriter.write("Successfully filled out General Information for new Retention");
		logFileWriter.newLine();
	}//end fillOutGeneralRetentionInformation method
 	
	public static String saveNewRetention(BufferedWriter logFileWriter) throws Exception{
		String retentionIDString = new String();
		logFileWriter.write("Now saving new Retention");
		logFileWriter.newLine();
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myGeneralWorksheetName, "RetentionSaveButton", logFileWriter);
		if (waitAndClick(by)) {
			logFileWriter.write("Successfully clicked Save Button for new Retention");
			logFileWriter.newLine();
		}//end if
		else {
			logFileWriter.write("Could not click on the Save Button for the new Retention");
			logFileWriter.newLine();
			return retentionIDString;
		}//end else
		Thread.sleep(5000);
		dismissAlert(logFileWriter);
		Thread.sleep(2000);
		by =  HandSOnTestSuite.searchAuthorizationUI.getByForName(myGeneralWorksheetName, "RetentionIDSpan", logFileWriter);
		retentionIDString = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by);
		logFileWriter.write("The Retention ID String read is '"+retentionIDString+"'");
		logFileWriter.newLine();
		return retentionIDString;
	}//end saveNewRetention method
	
	public static void closeCurrentTab (BufferedWriter logFileWriter) throws Exception{
		if (switchWindowByURL(retentionURL, logFileWriter)) {
	    	System.out.println("Closing Window with URL "+driver.getCurrentUrl());
			logFileWriter.write("Closing Window with URL " + driver.getCurrentUrl());
	    	logFileWriter.newLine();
	    	driver.close();	
		}//end if
		else {
	    	System.out.println("Could not switch to Retention URL - current URL is "+driver.getCurrentUrl());
			logFileWriter.write("Could not switch to Retention URL - current URL is " + driver.getCurrentUrl());
	    	logFileWriter.newLine();
	    }//end else
	}//end closeCurrentTab method

}//end Class
