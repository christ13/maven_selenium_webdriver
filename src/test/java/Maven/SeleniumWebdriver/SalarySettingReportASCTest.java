package Maven.SeleniumWebdriver;


import java.util.ArrayList;
import java.util.TreeMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SalarySettingReportASCTest extends ASCTest {

	@Before
	public void setUp() throws Exception {
		searchDelimiterColumn = 0;
		setStringAttributes();
		setAmountAttributes();
		setShareAttributes();
		super.setUp();
	}//end setUp method
	
	protected void setStringAttributes(){
		expectedStringAttributeNames = new String[5];
		actualStringAttributeNames = new String[5];
		expectedStringAttributeNames[0] = actualStringAttributeNames[0] = ASCFacultyValues.StringAttributeKeys[ASCFacultyValues.ClusterIndex];
		expectedStringAttributeNames[1] = actualStringAttributeNames[1] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.DepartmentIndex];
		expectedStringAttributeNames[2] = actualStringAttributeNames[2] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.FacultyIndex];
		expectedStringAttributeNames[3] = actualStringAttributeNames[3] = ASCFacultyValues.StringAttributeKeys[ASCFacultyValues.RankIndex];
		expectedStringAttributeNames[4] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.DescriptionIndex];
		actualStringAttributeNames[4] = HandSOnTestSuite.FY + " " + ExpectedASCData.StringAttributeKeys[ExpectedASCData.DescriptionIndex];

	}
	
	protected void setAmountAttributes(){
		expectedAmountAttributeNames = new String[3];
		actualAmountAttributeNames = new String[3];
		expectedAmountAttributeNames[0] = ExpectedASCData.amountAttributeKeys[ExpectedASCData.FTEPercentIndex];
		actualAmountAttributeNames[0] = "Dept FTE";
		expectedAmountAttributeNames[1] = ExpectedASCData.amountAttributeKeys[ExpectedASCData.CurrentSalaryIndex];
		actualAmountAttributeNames[1] = "Final " + HandSOnTestSuite.PY + " Salary";
		expectedAmountAttributeNames[2] = ExpectedASCData.amountAttributeKeys[ExpectedASCData.FTEAdjustedSalaryIndex];
		actualAmountAttributeNames[2] = "Final " + HandSOnTestSuite.PY + " FTE Adjusted Salary";
	}//end setAmountAttributes method
	
	protected void setShareAttributes(){
		shareNames = new String[3];
		shareNames[0] = "Regular Merit Pool Dept Share";
		shareNames[1] = "Market Pool Dept Share";
		shareNames[2] = "DO Share";
		expectedShareCriteria = new TreeMap<String, ArrayList<String>>();
		ArrayList<String>[] list = new ArrayList[3];
		list[0] = new ArrayList<String>();
		list[0].add(ASCFacultyValues.DoShare);
		list[0].add(ASCFacultyValues.ShareTotal);
		expectedShareCriteria.put(shareNames[2], list[0]);
		list[1] = new ArrayList<String>();
		list[1].add(ASCFacultyValues.DeptShare);
		list[1].add(ASCTest.sourcePools[ASCTest.RegularMeritPoolIndex]);
		expectedShareCriteria.put(shareNames[0], list[1]);
		list[2] = new ArrayList<String>();
		list[2].add(ASCFacultyValues.DeptShare);
		list[2].add(ASCTest.sourcePools[ASCTest.MarketPoolIndex]);
		expectedShareCriteria.put(shareNames[1], list[2]);
	}//end setShareAttributes method

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void test() throws Exception{
		super.test();
	}
	
	protected void testNotKnownASCDescriptions() throws Exception{
		testNotKnownASCDescriptions(new TreeMap<String, String>());
	}

	
	protected void testNotKnownASCDescriptions(TreeMap<String, String> criteria) throws Exception{
		//zero out all expected data except for the String attribute "Description"
		outputMessage("**** Now executing testNotKnownASCDescriptions method ****", logFileWriter);
		expectedShareCriteria = new TreeMap<String, ArrayList<String>>();
		shareNames = new String[0];
		expectedAmountAttributeNames = new String[0];
		actualAmountAttributeNames = new String[0];
		expectedStringAttributeNames = new String[3];
		actualStringAttributeNames = new String[3];
		expectedStringAttributeNames[0] = actualStringAttributeNames[0] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.DepartmentIndex];
		expectedStringAttributeNames[1] = actualStringAttributeNames[1] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.FacultyIndex];
		expectedStringAttributeNames[2] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.DescriptionIndex];
		actualStringAttributeNames[2] = HandSOnTestSuite.FY + " " + ExpectedASCData.StringAttributeKeys[ExpectedASCData.DescriptionIndex];
		this.criteria = criteria;
		criteria.put(ExpectedASCData.StringAttributeKeys[ExpectedASCData.StageIndex], 
				ASCTest.stages[ASCTest.PendingStageIndex]);
		outputMessage("* Now testing for Pending ASC Descriptions *", logFileWriter);
		super.test();
		criteria.remove(ExpectedASCData.StringAttributeKeys[ExpectedASCData.StageIndex]);
		criteria.put(ExpectedASCData.StringAttributeKeys[ExpectedASCData.StageIndex], 
				ASCTest.stages[ASCTest.EstimatedStageIndex]);
		outputMessage("* Now testing for Estimated ASC Descriptions *", logFileWriter);
		super.test();
	}

}
