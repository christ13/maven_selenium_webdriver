package Maven.SeleniumWebdriver;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	DeptSalarySettingOverridesTest.class,
	DeptSalarySettingExceptionsTest.class,
	ChairSalarySettingOverridesTest.class,
	ChairSalarySettingExceptionsTest.class,
	DeanSalarySettingOverridesTest.class,
	ClusterSalarySettingExceptionsTest.class,
	ClusterSalarySettingOverridesTest.class,
	SchoolSalarySettingExceptionsTest.class,
	SchoolSalarySettingOverridesTest.class,
	MasterSalarySettingExceptionsTest.class
})

public class SalarySettingMgrTabTestSuite {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
	}

}
