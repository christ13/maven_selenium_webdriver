package Maven.SeleniumWebdriver;

import java.util.TreeSet;

public class ASCFacultyValues {
	
	public static final String[] StringAttributeKeys = 
		{"Name", "Department", "Rank", "Cluster"};
	private String[] StringAttributes = new String[StringAttributeKeys.length];
	public static final int FacultyIndex = 0;
	public static final int DepartmentIndex = 1;
	public static final int RankIndex = 2;
	public static final int ClusterIndex = 3;
	public static final String ShareTotal = "Total";
	public static final String DeptShare = "dept";
	public static final String DoShare = "DO";
		
	private int[][] doShares = new int[ASCTest.stages.length][ASCTest.sourcePools.length];
	private int[][] deptShares = new int[ASCTest.stages.length][ASCTest.sourcePools.length];
	
	private boolean isDeptChair = false;
	private boolean isDean = false;
	private boolean hasMultipleASCs = false;

	public ASCFacultyValues(String faculty, String department) {
		StringAttributes[FacultyIndex] = faculty;
		StringAttributes[DepartmentIndex] = department;
		for(int i=0; i<ASCTest.stages.length; i++){
			for(int j=0; j<ASCTest.sourcePools.length; j++){
				doShares[i][j] = 0;
				deptShares[i][j] = 0;
			}//end inner for - per source pool
		}//end outer for - per stages
	}//end constructor
	
	public boolean hasOutsideAppt() throws Exception {
		SSExcelReader excelReader = TestFileController.ProvostBinderListByDepartment;
		int rowForColumnNames = TestFileController.ProvostBinderListByDepartmentWorksheetHeaderRow;
		String facultyName = this.StringAttributes[FacultyIndex];
		excelReader.setRowForColumnNames(rowForColumnNames);
		String mySheetName = StringAttributes[DepartmentIndex];
		int rowNum = excelReader.getCellRowNum(mySheetName, "Name", facultyName);
		String cellData = excelReader.getCellData(mySheetName, "Outside Appt", rowNum);
		if (cellData == null) return false;
		else if (cellData.length() == 0) return false;
		else if (cellData.equalsIgnoreCase("\n") || cellData.equalsIgnoreCase(" ")) return false;
		else return true;
	}
	
	public boolean hasMultipleASCs(){
		return hasMultipleASCs;
	}
	
	public void setHasMultipleASCs(boolean hasMultipleASCs){
		this.hasMultipleASCs = hasMultipleASCs;
	}

	
	public ASCFacultyValues(String faculty, String department, String rank, String cluster) {
		StringAttributes[FacultyIndex] = faculty;
		StringAttributes[DepartmentIndex] = department;
		StringAttributes[RankIndex] = rank;
		StringAttributes[ClusterIndex] = cluster;
	}//end constructor
	
	public boolean setStringAttribute(int keyIndex, String value){
		if (keyIndex>=0 && keyIndex<StringAttributes.length){
			StringAttributes[keyIndex] = value;
			return true;//the index is valid and the value can be assigned to it
		}//end if
		else return false;
	}//end setStringAttribute method
	
	public void setIsDean(boolean isDean){
		this.isDean = isDean;
	}
	
	public boolean isDean(){
		return this.isDean;
	}
	
	public void setIsDeptChair(boolean isDeptChair){
		this.isDeptChair = isDeptChair;
	}
	
	public boolean isDeptChair(){
		return this.isDeptChair;
	}
	
	public boolean setStringAttribute(String attributeName, String value){
		boolean match = false;//we must prove that there's a match
		for(int i=0;i<StringAttributeKeys.length && (! match); i++){
			if(StringAttributeKeys[i].equalsIgnoreCase(attributeName)){
				StringAttributeKeys[i] = value;
				match = true;//it's a match
			}//end if - match
		}//end for loop
		return match;//if we get to this point, it's not a match
	}//end setStringAttribute method
	
	
	public String getStringAttribute(int keyIndex){
		return StringAttributes[keyIndex];
	}// end getStringAttribute method
	
	public String getStringAttribute(String attributeName){
		int index=-1;
		boolean match = false;
		for(int i=0;i<StringAttributeKeys.length && (! match); i++){
			if(StringAttributeKeys[i].equalsIgnoreCase(attributeName)) index=i;
		}//end for loop
		if (index == -1) return new String("attribute name "+ attributeName +" not found");
		else return getStringAttribute(index);
	}//end getStringAttribute method
	
	public String toString(){
		String message = new String();
		for(int i=0; i<StringAttributeKeys.length; i++){
			message = message.concat(StringAttributeKeys[i] + " = " + StringAttributes[i] + "; ");
		}//end for - printing out the String attributes
		message = message.concat("\n");
		for(int i=0; i<ASCTest.stages.length; i++){
			for(int j=0; j<ASCTest.sourcePools.length; j++){
				message = message.concat("doShare for Stage "+  ASCTest.stages[i]
						+" and for Pool " + ASCTest.sourcePools[j]
						+ " = " + doShares[i][j] + ";\n ");
				message = message.concat("deptShare for Stage " +  ASCTest.stages[i]
						+" and for Pool "+ ASCTest.sourcePools[j] 
								+ " = " + deptShares[i][j] + ";\n ");
			}//end for - printing out the doShares and deptShares for each Pool
		}//end for - printing out doShares and deptShares for each stage
		message = message.concat("isDeptChair = " + isDeptChair + "; ");
		message = message.concat("isDean = " + isDean + "; ");
		message = message.concat("Has multiple ASC's = " + hasMultipleASCs + ";\n ");
		return message;
	}
	
	public void setDOShare(int stageIndex, int sourcePoolIndex, int amount){
		this.doShares[stageIndex][sourcePoolIndex] = amount;
	}

	public void setDeptShare(int stageIndex, int sourcePoolIndex, int amount){
		this.deptShares[stageIndex][sourcePoolIndex] = amount;
	}
	
	public int getAllSharesForStageAndPool(int stageIndex, int sourcePoolIndex){
		return this.doShares[stageIndex][sourcePoolIndex] + this.deptShares[stageIndex][sourcePoolIndex];
	}

	public int getAllSharesForStageAndPool(String stage, String sourcePool){
		int stageIndex = -1;
		int sourcePoolIndex = -1;
		for(int i=0; i<ASCTest.stages.length; i++){
			if (ASCTest.stages[i].equalsIgnoreCase(stage)) stageIndex=i;
		}//end first for loop
		for(int i=0; i<ASCTest.sourcePools.length; i++){
			if (ASCTest.sourcePools[i].equalsIgnoreCase(sourcePool)) sourcePoolIndex=i;
		}//end second for loop
		if ( (stageIndex == -1) || (sourcePoolIndex == -1)){
			return 0;
		}
		else return getAllSharesForStageAndPool(stageIndex, sourcePoolIndex);
	}

	public int getDOShare(int stageIndex, int sourcePoolIndex){
		return this.doShares[stageIndex][sourcePoolIndex];
	}
	
	//this assumes both stage and sourcePool are set and given as valid criteria
	public int getDOShare(String stage, String sourcePool){
		int stageIndex = -1;
		int sourcePoolIndex = -1;
		for(int i=0; i<ASCTest.stages.length; i++){
			if (ASCTest.stages[i].equalsIgnoreCase(stage)) stageIndex=i;
		}//end first for loop
		for(int i=0; i<ASCTest.sourcePools.length; i++){
			if (ASCTest.sourcePools[i].equalsIgnoreCase(sourcePool)) sourcePoolIndex=i;
		}//end second for loop
		if ( (stageIndex == -1) || (sourcePoolIndex == -1)){
			return 0;
		}
		else return getDOShare(stageIndex, sourcePoolIndex);
	}
	
	public int getDeptShare(int stageIndex, int sourcePoolIndex){
		return this.deptShares[stageIndex][sourcePoolIndex];
	}
	
	public int getDeptShare(String stage, String sourcePool){
		int stageIndex = -1;
		int sourcePoolIndex = -1;
		for(int i=0; i<ASCTest.stages.length; i++){
			if (ASCTest.stages[i].equalsIgnoreCase(stage)) stageIndex=i;
		}//end first for loop
		for(int i=0; i<ASCTest.sourcePools.length; i++){
			if (ASCTest.sourcePools[i].equalsIgnoreCase(sourcePool)) sourcePoolIndex=i;
		}//end second for loop
		if ( (stageIndex == -1) || (sourcePoolIndex == -1)){
			return 0;
		}
		else return getDeptShare(stageIndex, sourcePoolIndex);
	}
	
	public int getAllDOSharesForStage(int stageIndex){
		int sum = 0;
		for(int i=0;i<ASCTest.sourcePools.length; i++){
			sum +=doShares[stageIndex][i];
		}
		return sum;
	}
	
	public int getAllDOSharesForStage(String stage){
		int stageIndex = -1;
		for(int i=0; i<ASCTest.stages.length; i++){
			if (ASCTest.stages[i].equalsIgnoreCase(stage)) stageIndex=i;
		}//end first for loop
		if (stageIndex == -1){
			return 0;
		}
		else return getAllDOSharesForStage(stageIndex);
	}

	public int getAllDeptSharesForStage(int stageIndex){
		int sum = 0;
		for(int i=0;i<ASCTest.sourcePools.length; i++){
			sum +=deptShares[stageIndex][i];
		}
		return sum;
	}

	public int getAllDeptSharesForStage(String stage){
		int stageIndex = -1;
		for(int i=0; i<ASCTest.stages.length; i++){
			if (ASCTest.stages[i].equalsIgnoreCase(stage)) stageIndex=i;
		}//end first for loop
		if (stageIndex == -1){
			return 0;
		}
		else return getAllDeptSharesForStage(stageIndex);
	}

}
