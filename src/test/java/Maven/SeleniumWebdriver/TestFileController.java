package Maven.SeleniumWebdriver;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;


public final class TestFileController {

	public static final String directory = "C:/Users/christ13/Documents/HandSon/";
	static String inputFileName = "Input Data.xls";
	static String individualInputWorksheetName = "Individual Test Input Data";
	static String globalInputWorksheetName = "Global Test Input Data";
	static String outputFileName = "Test Results.xls";
	static String outputWorksheetName = "Passed Tests";
	static String errorWorksheetName = "Failed Tests";
	static String locatorFileName = "Locators.xls";
	static String OriginalAPValuesWorksheetName = "Original Reference Values";
	static String AlteredAPValuesWorksheetName = "Altered Reference Values";
	static String FacultyValuesWorksheetName = "Faculty Values";
	static String LogDirectoryName;
	public static HSSFExcelReader fileIn;
	public static final String inputFileQueriesTab = "Queries Tab";
	public static HSSFExcelReader fileOut;
	public static HSSFExcelReader locators;
	public static SSExcelReader ControlSheetReportSuite;
	public static final String ControlSheetReportSuiteFileName = "Control Sheet Report Suite.xls";
	public static final String ControlSheetDetailReportWorksheetName = "Control Sheet Detail";
	static int ControlSheetDetailReportLastRow;
	public static final int ControlSheetDetailReportColumnHeaderRow = 6;
	public static final String ControlSheetReportWorksheetName = "Control Sheet";
	static int ControlSheetReportLastRow;
	public static final int ControlSheetReportColumnHeaderRow = 7;
	public static final String MeritPoolSalaryBaseReportWorksheetName = "Merit Pool Salary Base";
	static int MeritPoolSalaryBaseReportWorksheetLastRow;
	public static final int MeritPoolSalaryBaseReportWorksheetHeaderRow = 13;
	public static final String PoolAllocationWorksheetName = "Pool Allocation";
	static int PoolAllocationWorksheetLastRow;
	public static final int PoolAllocationWorksheetHeaderRow = 9;
	public static final String ControlSheetPivot_StageWorksheetName = "Control Sheet Pivot_Stage";
	static int ControlSheetPivot_StageWorksheetLastRow;
	public static final int ControlSheetPivotHeaderRow = 7;
	public static final String ControlSheetPivot_ClusterWorksheetName = "Control Sheet Pivot_Cluster";
	static int ControlSheetPivot_ClusterWorksheetLastRow;
	public static final String ControlSheetPivot_CategoryWorksheetName = "Control Sheet Pivot_Category";
	static int ControlSheetPivot_CategoryWorksheetLastRow;
	public static final String ControlSheetPivot_SubCatWorksheetName = "Control Sheet Pivot_Sub-Cat";
	static int ControlSheetPivot_SubCatWorksheetLastRow;
	public static final String ControlSheetPivot_HumArtsWorksheetName = "Control Sheet Pivot_HumArts";
	static int ControlSheetPivot_HumArtsWorksheetLastRow;
	public static final String ControlSheetPivot_NatSciWorksheetName = "Control Sheet Pivot_NatSci";
	static int ControlSheetPivot_NatSciWorksheetLastRow;
	public static final String ControlSheetPivot_SocSciWorksheetName = "Control Sheet Pivot_SocSci";
	static int ControlSheetPivot_SocSciWorksheetLastRow;
	
	public static SSExcelReader DeanSalarySettingReport;
	public static final String DeanSalarySettingReportName = "Dean Salary Setting Worksheet.xlsx";
	public static final String DeanSalarySettingWorksheetName = "Dean Salary Setting";
	public static final int DeanSalarySettingWorksheetHeaderRow = 17;
	static int DeanSalarySettingWorksheetLastRow;
	
	public static SSExcelReader DepartmentSalarySettingBiologyReport;
	public static final String DepartmentSalarySettingBiologyReportName = "Department Salary Setting Worksheet Biology.xlsm";
	public static final String DepartmentSalarySettingWorksheetName = "Department Salary Setting";
	public static final int DepartmentSalarySettingWorksheetHeaderRow = 17;
	static int DepartmentSalarySettingBiologyWorksheetLastRow;

	public static SSExcelReader DepartmentSalarySettingEconomicsReport;
	public static final String DepartmentSalarySettingEconomicsReportName = "Department Salary Setting Worksheet Economics.xlsm";
	static int DepartmentSalarySettingEconomicsWorksheetLastRow;
	
	public static SSExcelReader DepartmentSalarySettingHistoryReport;
	public static final String DepartmentSalarySettingHistoryReportName = "Department Salary Setting Worksheet History.xlsm";
	static int DepartmentSalarySettingHistoryWorksheetLastRow;
	
	public static SSExcelReader DepartmentSalarySettingMathematicsReport;
	public static final String DepartmentSalarySettingMathematicsReportName = "Department Salary Setting Worksheet Mathematics.xlsm";
	static int DepartmentSalarySettingMathematicsWorksheetLastRow;

	public static SSExcelReader DepartmentSalarySettingPhysicsReport;
	public static final String DepartmentSalarySettingPhysicsReportName = "Department Salary Setting Worksheet Physics.xlsm";
	static int DepartmentSalarySettingPhysicsWorksheetLastRow;

	public static SSExcelReader DepartmentSalarySettingStatisticsReport;
	public static final String DepartmentSalarySettingStatisticsReportName = "Department Salary Setting Worksheet Physics.xlsm";
	static int DepartmentSalarySettingStatisticsWorksheetLastRow;

	public static SSExcelReader DepartmentSalarySettingTheaterAndPerformanceStudiesReport;
	public static final String DepartmentSalarySettingTheaterAndPerformanceStudiesReportName = "Department Salary Setting Worksheet Theater & Performance Studies.xlsm";
	static int DepartmentSalarySettingTheaterAndPerformanceStudiesWorksheetLastRow;
	
	public static SSExcelReader ChairSalarySettingWorksheetSocialSciences;
	public static final String ChairSalarySettingWorksheetSocialSciencesName = "Chair Salary Setting Worksheet Social Sciences.xlsx";
	public static final String ChairSalarySettingWorksheetWorksheetName = "Chair Salary Setting";
	public static final int ChairSalarySettingWorksheetSocialSciencesHeaderRow = 18;
	static int ChairSalarySettingWorksheetSocialSciencesLastRow;
	
	public static SSExcelReader ChairSalarySettingWorksheetNaturalSciences;
	public static final String ChairSalarySettingWorksheetNaturalSciencesName = "Chair Salary Setting Worksheet Natural Sciences.xlsx";
	public static final int ChairSalarySettingWorksheetNaturalSciencesHeaderRow = 18;
	static int ChairSalarySettingWorksheetNaturalSciencesLastRow;

	public static SSExcelReader ChairSalarySettingWorksheetHumanitiesAndArts;
	public static final String ChairSalarySettingWorksheetHumanitiesAndArtsName = "Chair Salary Setting Worksheet Humanities and Arts.xlsx";
	public static final int ChairSalarySettingWorksheetHumanitiesAndArtsHeaderRow = 18;
	static int ChairSalarySettingWorksheetHumanitiesAndArtsLastRow;
	
	public static SSExcelReader ClusterSalarySettingWorksheetSocialSciences;
	public static final String ClusterSalarySettingWorksheetSocialSciencesName = "Cluster Salary Setting Worksheet Social Sciences.xlsx";
	public static final String ClusterSalarySettingWorksheetWorksheetName = "Cluster Salary Setting";
	public static final int ClusterSalarySettingWorksheetHeaderRow = 10;
	static int ClusterSalarySettingWorksheetSocialSciencesLastRow;
	
	public static SSExcelReader ClusterSalarySettingWorksheetNaturalSciences;
	public static final String ClusterSalarySettingWorksheetNaturalSciencesName = "Cluster Salary Setting Worksheet Natural Sciences.xlsx";
	static int ClusterSalarySettingWorksheetNaturalSciencesLastRow;
	
	public static SSExcelReader ClusterSalarySettingWorksheetHumanitiesAndArts;
	public static final String ClusterSalarySettingWorksheetHumanitiesAndArtsName = "Cluster Salary Setting Worksheet Humanities and Arts.xlsx";
	static int ClusterSalarySettingWorksheetHumanitiesAndArtsLastRow;
	
	public static SSExcelReader DepartmentVerificationReport;
	public static final String DepartmentVerificationReportName = "Department Verification Report.xls";
	public static final String DepartmentVerificationReportWorksheetName = "Department Verification Report";
	public static final int DepartmentVerificationReportHeaderRow = 10;
	static int DepartmentVerificationReportLastRow;
	
	public static SSExcelReader ProvostBinderListByDepartment;
	public static final String ProvostBinderListByDepartmentFileName = "Provost Binder List by Department.xls";
	public static final String[] ProvostBinderListByDepartmentWorksheetNames = {"Economics", "History", "Mathematics", "Physics", "Theater & Performance Studies"};
	public static final int ProvostBinderListByDepartmentWorksheetHeaderRow = 12;
	static int[] ProvostBinderListByDepartmentWorksheetLastRows = new int[ProvostBinderListByDepartmentWorksheetNames.length];
	
	public static SSExcelReader ProvostBinderListByName;
	public static final String ProvostBinderListByNameFileName = "Provost Binder List by Name.xls";
	public static final String[] ProvostBinderListByNameWorksheetNames = {"HumanitiesFullProfessors", "HumanitiesAssocProfessors", 
		"HumanitiesAsstProfessors", "NaturalSciencesFullProfessors", "NaturalSciencesAssocProfessors", "NaturalSciencesAsstProfessors",
		"SocialSciencesFullProfessors", "SocialSciencesAssocProfessors", "SocialSciencesAsstProfessors"};
	public static final int ProvostBinderListByNameWorksheetHeaderRow = 4;
	static int[] ProvostBinderListByNameWorksheetLastRows = new int[ProvostBinderListByNameWorksheetNames.length];
	
	public static SSExcelReader ProvostBinderListBySalary;
	public static final String ProvostBinderListBySalaryFileName = "Provost Binder List by Salary.xls";
	public static final String[] ProvostBinderListBySalaryWorksheetNames = {"HumanitiesFullProfessors", "HumanitiesAssocProfessors", 
		"HumanitiesAsstProfessors", "NaturalSciencesFullProfessors", "NaturalSciencesAssocProfessors", "NaturalSciencesAsstProfessors",
		"SocialSciencesFullProfessors", "SocialSciencesAssocProfessors", "SocialSciencesAsstProfessors"};
	public static final int ProvostBinderListBySalaryWorksheetHeaderRow = 4;
	static int[] ProvostBinderListBySalaryWorksheetLastRows = new int[ProvostBinderListByNameWorksheetNames.length];

	public static SSExcelReader SchoolSalarySettingReport;
	public static final String SchoolSalarySettingReportName = "School Salary Setting Worksheet.xlsx";
	public static final String SchoolSalarySettingReportWorksheetName = "School Salary Setting";
	public static final int SchoolSalarySettingReportHeaderRow = 10;
	static int SchoolSalarySettingReportLastRow;
	
	public static SSExcelReader MasterSalarySettingReport;
	public static final String MasterSalarySettingReportName = "Master Salary Setting Report.xls";
	public static final String MasterSalarySettingReportWorksheetName = "Master Salary Setting";
	public static final int MasterSalarySettingReportHeaderRow = 4;
	static int MasterSalarySettingReportLastRow;
	
	//all of these are relevant to the Salary Budgeting Reports
	public static final String queriesTab = "showQueriesTab.do";
	public static final String dateString = new SimpleDateFormat("MM").format(new Date())+"_"
											+ new SimpleDateFormat("dd").format(new Date())+"_"
											+ new SimpleDateFormat("yyyy").format(new Date());
	public static final String SalaryBudgetingPopulationVerificationReportName = "Salary Budgeting Population Verification Report";
	public static final String SalaryBudgetingPopulationVerificationReportFileName = SalaryBudgetingPopulationVerificationReportName + ".xls";
	public static final String SalaryBudgetingReportName = "Salary Budgeting Report";
	public static final String SalaryBudgetingReportFileName = SalaryBudgetingReportName.replaceAll("\\s+","")+".xls";
	public static final String SalaryBudgetingComparisonReportName = "Salary Budgeting Comparison Report";
	public static final String SalaryBudgetingComparisonReportFileName = SalaryBudgetingComparisonReportName.replaceAll("\\s+","") + ".xls";
	public static final String SalaryRateExportForHyperionUploadName = "Salary Rate Export for Hyperion Upload";
	public static final String SalaryRateExportForHyperionUploadFileName = SalaryRateExportForHyperionUploadName.replaceAll("\\s+","")+"_"+dateString+".csv";
	public static final String SalaryPercentageExportForHyperionUploadName = "Salary Percentage Export for Hyperion Upload";
	public static final String SalaryPercentageExportForHyperionUploadFileName = SalaryPercentageExportForHyperionUploadName.replaceAll("\\s+","")+"_"+dateString+".csv";
	public static final String SalaryReserveDetailReportName = "Salary Reserve Detail Report";
	public static final String SalaryReserveDetailReportFileName = SalaryReserveDetailReportName.replaceAll("\\s+","")+"_"+dateString+".csv";
	public static final String SalaryReserveRollUpExportForHyperionUploadName = "Salary Reserve Roll Up Export for Hyperion Upload";
	public static final String SalaryReserveRollUpExportForHyperionUploadFileName = SalaryReserveRollUpExportForHyperionUploadName.replaceAll("\\s+","")+"_"+dateString+".csv";

	public TestFileController() throws Exception{
		
		fileIn = new HSSFExcelReader(directory + inputFileName);
		fileOut = new HSSFExcelReader(directory + outputFileName);
		locators = new HSSFExcelReader(directory + locatorFileName);
		
		if (! fileOut.addSheet(outputWorksheetName)){
			  System.out.println("Removing and re-creating Output File Worksheet");
			fileOut.removeSheet(outputWorksheetName);
			fileOut.addSheet(outputWorksheetName);
		}
		fileOut.addColumn(outputWorksheetName, "Test Env.");
		fileOut.addColumn(outputWorksheetName, "Test Case #");
		fileOut.addColumn(outputWorksheetName, "Test Description");
		fileOut.addColumn(outputWorksheetName, "Test Case");

		if (! fileOut.addSheet(errorWorksheetName)){
			System.out.println("Removing and re-creating Error Worksheet");
			fileOut.removeSheet(errorWorksheetName);
			fileOut.addSheet(errorWorksheetName);
		}
		fileOut.addColumn(errorWorksheetName, "Test Env.");
		fileOut.addColumn(errorWorksheetName, "Test Case #");
		fileOut.addColumn(errorWorksheetName, "Test Description");
		fileOut.addColumn(errorWorksheetName, "Test Case");
		fileOut.addColumn(errorWorksheetName, "Cumulative Error String");
		
		//resetAllExcelReaders();
		
		LogDirectoryName = directory + new SimpleDateFormat("yyyyMMddhhmm").format(new Date());
		File logDirectory = new File(LogDirectoryName);
		if (!logDirectory.exists()) {
			if (logDirectory.mkdir()) {
				System.out.println("Log Directory with timestamp is created!");
			} else {
				System.out.println("Failed to create log directory with timestamp!");
			}
		}
		else System.out.println("Log Directory with timestamp already exists.");
		
	}
	
	public static boolean setLogDirectoryName(String logDirectoryName) throws Exception{
		File logDirectory = new File(logDirectoryName);
		if (!logDirectory.exists()) {
			if (logDirectory.mkdir()) {
				System.out.println("Log Directory with timestamp is created!");
				return true;
			} else {
				System.out.println("Failed to create log directory with timestamp!");
				return false;
			}
		}
		else System.out.println("Log Directory with timestamp already exists: " + logDirectoryName);
		return false;
		
	}
	
	public static void closeAllExcelReaders() throws Exception{
		ControlSheetReportSuite = closeOneExcelReader(ControlSheetReportSuite);
		ChairSalarySettingWorksheetSocialSciences = closeOneExcelReader(ChairSalarySettingWorksheetSocialSciences);
		ChairSalarySettingWorksheetNaturalSciences = closeOneExcelReader(ChairSalarySettingWorksheetNaturalSciences);
		ChairSalarySettingWorksheetHumanitiesAndArts = closeOneExcelReader(ChairSalarySettingWorksheetHumanitiesAndArts);
		ClusterSalarySettingWorksheetSocialSciences = closeOneExcelReader(ClusterSalarySettingWorksheetSocialSciences);
		ClusterSalarySettingWorksheetNaturalSciences = closeOneExcelReader(ClusterSalarySettingWorksheetNaturalSciences);
		ClusterSalarySettingWorksheetHumanitiesAndArts = closeOneExcelReader(ClusterSalarySettingWorksheetHumanitiesAndArts);
		DeanSalarySettingReport = closeOneExcelReader(DeanSalarySettingReport);
		DepartmentSalarySettingBiologyReport = closeOneExcelReader(DepartmentSalarySettingBiologyReport);
		DepartmentSalarySettingEconomicsReport = closeOneExcelReader(DepartmentSalarySettingEconomicsReport);
		DepartmentSalarySettingHistoryReport = closeOneExcelReader(DepartmentSalarySettingHistoryReport);
		DepartmentSalarySettingMathematicsReport = closeOneExcelReader(DepartmentSalarySettingMathematicsReport);
		DepartmentSalarySettingPhysicsReport = closeOneExcelReader(DepartmentSalarySettingPhysicsReport);
		DepartmentSalarySettingStatisticsReport = closeOneExcelReader(DepartmentSalarySettingStatisticsReport);
		DepartmentSalarySettingTheaterAndPerformanceStudiesReport = closeOneExcelReader(DepartmentSalarySettingTheaterAndPerformanceStudiesReport);
		DepartmentVerificationReport = closeOneExcelReader(DepartmentVerificationReport);
		ProvostBinderListByDepartment = closeOneExcelReader(ProvostBinderListByDepartment);
		ProvostBinderListByName = closeOneExcelReader(ProvostBinderListByName);
		ProvostBinderListBySalary = closeOneExcelReader(ProvostBinderListBySalary);
		SchoolSalarySettingReport = closeOneExcelReader(SchoolSalarySettingReport);
		MasterSalarySettingReport = closeOneExcelReader(MasterSalarySettingReport);

	}
	
	public static SSExcelReader closeOneExcelReader(SSExcelReader reader) throws Exception{
		reader.closeWorkbook();
		reader = null;
		System.gc();
		return reader;
	}
	
	public static void resetAllExcelReaders() throws Exception{
		ControlSheetReportSuite = new SSExcelReader(directory + ControlSheetReportSuiteFileName, ControlSheetDetailReportColumnHeaderRow);
		//Control Sheet Detail Report
		ControlSheetDetailReportLastRow = ControlSheetReportSuite.getRowCount(ControlSheetDetailReportWorksheetName);
		System.out.println("Rows in the Control Sheet Detail Report Worksheet: " + ControlSheetDetailReportLastRow);
		System.out.println("Number of columns in the Control Sheet Detail Report Worksheet: " + ControlSheetReportSuite.getColumnCount(ControlSheetDetailReportWorksheetName));
		//Control Sheet Report
		ControlSheetReportLastRow = ControlSheetReportSuite.getRowCount(ControlSheetReportWorksheetName);
		System.out.println("Rows in the Control Sheet Report Worksheet: " + ControlSheetReportLastRow);
		ControlSheetReportSuite.setRowForColumnNames(ControlSheetReportColumnHeaderRow);
		System.out.println("Number of columns in the Control Sheet Report Worksheet: " + ControlSheetReportSuite.getColumnCount(ControlSheetReportWorksheetName));
		//Merit Pool Salary Base Report
		MeritPoolSalaryBaseReportWorksheetLastRow = ControlSheetReportSuite.getRowCount(MeritPoolSalaryBaseReportWorksheetName);
		System.out.println("Rows in the Merit Pool Salary Base Report Worksheet: " + MeritPoolSalaryBaseReportWorksheetLastRow);
		ControlSheetReportSuite.setRowForColumnNames(MeritPoolSalaryBaseReportWorksheetHeaderRow);
		System.out.println("Number of columns in the Merit Pool Salary Base Report Worksheet: " + ControlSheetReportSuite.getColumnCount(MeritPoolSalaryBaseReportWorksheetName));
		//Pool Allocation Report
		PoolAllocationWorksheetLastRow = ControlSheetReportSuite.getRowCount(PoolAllocationWorksheetName);
		System.out.println("Rows in the Pool Allocation Report Worksheet: " + PoolAllocationWorksheetLastRow);
		ControlSheetReportSuite.setRowForColumnNames(PoolAllocationWorksheetHeaderRow);
		System.out.println("Number of columns in the Pool Allocation Report Worksheet: " + ControlSheetReportSuite.getColumnCount(PoolAllocationWorksheetName));
		//Control Sheet Pivot_Stage Report
		ControlSheetPivot_StageWorksheetLastRow = ControlSheetReportSuite.getRowCount(ControlSheetPivot_StageWorksheetName);
		System.out.println("Rows in the Control Sheet Pivot_Stage Report Worksheet: " + ControlSheetPivot_StageWorksheetLastRow);
		ControlSheetReportSuite.setRowForColumnNames(ControlSheetPivotHeaderRow);
		System.out.println("Number of columns in the Control Sheet Pivot_Stage Report Worksheet: " + ControlSheetReportSuite.getColumnCount(ControlSheetPivot_StageWorksheetName));
		//Control Sheet Pivot_Cluster Report
		ControlSheetPivot_ClusterWorksheetLastRow = ControlSheetReportSuite.getRowCount(ControlSheetPivot_ClusterWorksheetName);
		System.out.println("Rows in the Control Sheet Pivot_Cluster Report Worksheet: " + ControlSheetPivot_ClusterWorksheetLastRow);
		System.out.println("Number of columns in the Control Sheet Pivot_Cluster Report Worksheet: " + ControlSheetReportSuite.getColumnCount(ControlSheetPivot_ClusterWorksheetName));
		//Control Sheet Pivot_Category Report
		ControlSheetPivot_CategoryWorksheetLastRow = ControlSheetReportSuite.getRowCount(ControlSheetPivot_CategoryWorksheetName);
		System.out.println("Rows in the Control Sheet Pivot_Category Report Worksheet: " + ControlSheetPivot_CategoryWorksheetLastRow);
		System.out.println("Number of columns in the Control Sheet Pivot_Category Report Worksheet: " + ControlSheetReportSuite.getColumnCount(ControlSheetPivot_CategoryWorksheetName));
		//Control Sheet Pivot_Sub-Category Report
		ControlSheetPivot_SubCatWorksheetLastRow = ControlSheetReportSuite.getRowCount(ControlSheetPivot_SubCatWorksheetName);
		System.out.println("Rows in the Control Sheet Pivot Sub-Category Report Worksheet: " + ControlSheetPivot_SubCatWorksheetLastRow);
		int numberOfColumns = ControlSheetReportSuite.getColumnCount(ControlSheetPivot_SubCatWorksheetName);
		if (numberOfColumns == 8){
			try{ControlSheetReportSuite.removeColumn(ControlSheetPivot_SubCatWorksheetName, numberOfColumns-1);}
			catch(Exception e){ e.printStackTrace();}
		}//end if
		numberOfColumns = ControlSheetReportSuite.getColumnCount(ControlSheetPivot_SubCatWorksheetName);
		System.out.println("Number of columns in the Control Sheet Pivot Sub-Category Report Worksheet: " + numberOfColumns);
		//Control Sheet Pivot Humanities and Arts Report
		ControlSheetPivot_HumArtsWorksheetLastRow = ControlSheetReportSuite.getRowCount(ControlSheetPivot_HumArtsWorksheetName);
		System.out.println("Rows in the Control Sheet Pivot Humanities And Arts Report Worksheet: " + ControlSheetPivot_HumArtsWorksheetLastRow);
		System.out.println("Number of columns in the Control Sheet Pivot Humanities And Arts Report Worksheet: " + ControlSheetReportSuite.getColumnCount(ControlSheetPivot_HumArtsWorksheetName));
		//Control Sheet Pivot Natural Sciences Report
		ControlSheetPivot_NatSciWorksheetLastRow = ControlSheetReportSuite.getRowCount(ControlSheetPivot_NatSciWorksheetName);
		System.out.println("Rows in the Control Sheet Pivot Natural Sciences Report Worksheet: " + ControlSheetPivot_NatSciWorksheetLastRow);
		System.out.println("Number of columns in the Control Sheet Pivot Natural Sciences Report Worksheet: " + ControlSheetReportSuite.getColumnCount(ControlSheetPivot_NatSciWorksheetName));
		//Control Sheet Pivot Social Sciences Report
		ControlSheetPivot_SocSciWorksheetLastRow = ControlSheetReportSuite.getRowCount(ControlSheetPivot_SocSciWorksheetName);
		System.out.println("Rows in the Control Sheet Pivot Social Sciences Report Worksheet: " + ControlSheetPivot_SocSciWorksheetLastRow);
		System.out.println("Number of columns in the Control Sheet Pivot Social Sciences Report Worksheet: " + ControlSheetReportSuite.getColumnCount(ControlSheetPivot_SocSciWorksheetName));
		
		//instantiate the Dean Salary Setting Social Science SSExcelReader
		DeanSalarySettingReport = new SSExcelReader(directory + DeanSalarySettingReportName, DeanSalarySettingWorksheetHeaderRow);
		DeanSalarySettingWorksheetLastRow = DeanSalarySettingReport.getRowCount(DeanSalarySettingWorksheetName);
		System.out.println("Rows in the Dean Salary Setting Worksheet: " + DeanSalarySettingWorksheetLastRow);
		System.out.println("Number of columns in the Dean Salary Setting Worksheet: " + DeanSalarySettingReport.getColumnCount(DeanSalarySettingWorksheetName));
		
		//instantiate the Chair Salary Setting Social Science SSExcelReader
		ChairSalarySettingWorksheetSocialSciences = new SSExcelReader(directory + ChairSalarySettingWorksheetSocialSciencesName, ChairSalarySettingWorksheetSocialSciencesHeaderRow);
		ChairSalarySettingWorksheetSocialSciencesLastRow = ChairSalarySettingWorksheetSocialSciences.getRowCount(ChairSalarySettingWorksheetWorksheetName);
		System.out.println("Rows in the Chair Salary Setting Social Sciences Worksheet: " + ChairSalarySettingWorksheetSocialSciencesLastRow);
		System.out.println("Number of columns in the Chair Salary Setting Social Sciences Worksheet: " + ChairSalarySettingWorksheetSocialSciences.getColumnCount(ChairSalarySettingWorksheetWorksheetName));
		
		//instantiate the Chair Salary Setting Natural Science SSExcelReader
		ChairSalarySettingWorksheetNaturalSciences = new SSExcelReader(directory + ChairSalarySettingWorksheetNaturalSciencesName, ChairSalarySettingWorksheetNaturalSciencesHeaderRow);
		ChairSalarySettingWorksheetNaturalSciencesLastRow = ChairSalarySettingWorksheetNaturalSciences.getRowCount(ChairSalarySettingWorksheetWorksheetName);
		System.out.println("Rows in the Chair Salary Setting Natural Sciences Worksheet: " + ChairSalarySettingWorksheetNaturalSciencesLastRow);
		System.out.println("Number of columns in the Chair Salary Setting Natural Sciences Worksheet: " + ChairSalarySettingWorksheetNaturalSciences.getColumnCount(ChairSalarySettingWorksheetWorksheetName));
		
		//instantiate the Chair Salary Setting Humanities and Arts SSExcelReader
		ChairSalarySettingWorksheetHumanitiesAndArts = new SSExcelReader(directory + ChairSalarySettingWorksheetHumanitiesAndArtsName, ChairSalarySettingWorksheetHumanitiesAndArtsHeaderRow);
		ChairSalarySettingWorksheetHumanitiesAndArtsLastRow = ChairSalarySettingWorksheetHumanitiesAndArts.getRowCount(ChairSalarySettingWorksheetWorksheetName);
		System.out.println("Rows in the Chair Salary Setting Humanities and Arts Worksheet: " + ChairSalarySettingWorksheetHumanitiesAndArtsLastRow);
		System.out.println("Number of columns in the Chair Salary Setting Humanities and Arts Worksheet: " + ChairSalarySettingWorksheetHumanitiesAndArts.getColumnCount(ChairSalarySettingWorksheetWorksheetName));
		
		//instantiate the Cluster Salary Setting Social Sciences SSExcelReader
		ClusterSalarySettingWorksheetSocialSciences = new SSExcelReader(directory + ClusterSalarySettingWorksheetSocialSciencesName, ClusterSalarySettingWorksheetHeaderRow);
		ClusterSalarySettingWorksheetSocialSciencesLastRow = ClusterSalarySettingWorksheetSocialSciences.getRowCount(ClusterSalarySettingWorksheetWorksheetName);
		System.out.println("Rows in the Cluster Salary Setting Social Sciences Worksheet: " + ClusterSalarySettingWorksheetSocialSciencesLastRow);
		System.out.println("Number of columns in the Chair Salary Setting Natural Sciences Worksheet: " + ClusterSalarySettingWorksheetSocialSciences.getColumnCount(ClusterSalarySettingWorksheetWorksheetName));
		
		//instantiate the Cluster Salary Setting Natural Sciences SSExcelReader
		ClusterSalarySettingWorksheetNaturalSciences = new SSExcelReader(directory + ClusterSalarySettingWorksheetNaturalSciencesName, ClusterSalarySettingWorksheetHeaderRow);
		ClusterSalarySettingWorksheetNaturalSciencesLastRow = ClusterSalarySettingWorksheetNaturalSciences.getRowCount(ClusterSalarySettingWorksheetWorksheetName);
		System.out.println("Rows in the Cluster Salary Setting Natural Sciences Worksheet: " + ClusterSalarySettingWorksheetNaturalSciencesLastRow);
		System.out.println("Number of columns in the Chair Salary Setting Natural Sciences Worksheet: " + ClusterSalarySettingWorksheetNaturalSciences.getColumnCount(ClusterSalarySettingWorksheetWorksheetName));
		
		//instantiate the Cluster Salary Setting Humanities And Arts SSExcelReader
		ClusterSalarySettingWorksheetHumanitiesAndArts = new SSExcelReader(directory + ClusterSalarySettingWorksheetHumanitiesAndArtsName, ClusterSalarySettingWorksheetHeaderRow);
		ClusterSalarySettingWorksheetHumanitiesAndArtsLastRow = ClusterSalarySettingWorksheetHumanitiesAndArts.getRowCount(ClusterSalarySettingWorksheetWorksheetName);
		System.out.println("Rows in the Cluster Salary Setting Humanities And Arts Worksheet: " + ClusterSalarySettingWorksheetHumanitiesAndArtsLastRow);
		System.out.println("Number of columns in the Chair Salary Setting Humanities And Arts Worksheet: " + ClusterSalarySettingWorksheetHumanitiesAndArts.getColumnCount(ClusterSalarySettingWorksheetWorksheetName));
		
		//instantiate the Department Salary Setting Worksheets
		DepartmentSalarySettingBiologyReport = new SSExcelReader(directory + DepartmentSalarySettingBiologyReportName, DepartmentSalarySettingWorksheetHeaderRow);
		DepartmentSalarySettingBiologyWorksheetLastRow = DepartmentSalarySettingBiologyReport.getRowCount(DepartmentSalarySettingWorksheetName);
		System.out.println("Rows in the Department Salary Setting Biology Report Worksheet: " + DepartmentSalarySettingBiologyWorksheetLastRow);
		System.out.println("Number of columns in the Department Salary Setting Biology Report Worksheet: " + DepartmentSalarySettingBiologyReport.getColumnCount(DepartmentSalarySettingWorksheetName));

		DepartmentSalarySettingEconomicsReport = new SSExcelReader(directory + DepartmentSalarySettingEconomicsReportName, DepartmentSalarySettingWorksheetHeaderRow);
		DepartmentSalarySettingEconomicsWorksheetLastRow = DepartmentSalarySettingEconomicsReport.getRowCount(DepartmentSalarySettingWorksheetName);
		System.out.println("Rows in the Department Salary Setting Economics Report Worksheet: " + DepartmentSalarySettingEconomicsWorksheetLastRow);
		System.out.println("Number of columns in the Department Salary Setting Economics Report Worksheet: " + DepartmentSalarySettingEconomicsReport.getColumnCount(DepartmentSalarySettingWorksheetName));
		
		DepartmentSalarySettingHistoryReport = new SSExcelReader(directory + DepartmentSalarySettingHistoryReportName, DepartmentSalarySettingWorksheetHeaderRow);
		DepartmentSalarySettingHistoryWorksheetLastRow = DepartmentSalarySettingHistoryReport.getRowCount(DepartmentSalarySettingWorksheetName);
		System.out.println("Rows in the Department Salary Setting History Report Worksheet: " + DepartmentSalarySettingHistoryWorksheetLastRow);
		System.out.println("Number of columns in the Department Salary Setting History Report Worksheet: " + DepartmentSalarySettingHistoryReport.getColumnCount(DepartmentSalarySettingWorksheetName));
		
		DepartmentSalarySettingMathematicsReport = new SSExcelReader(directory + DepartmentSalarySettingMathematicsReportName, DepartmentSalarySettingWorksheetHeaderRow);
		DepartmentSalarySettingMathematicsWorksheetLastRow = DepartmentSalarySettingMathematicsReport.getRowCount(DepartmentSalarySettingWorksheetName);
		System.out.println("Rows in the Department Salary Setting Mathematics Report Worksheet: " + DepartmentSalarySettingMathematicsWorksheetLastRow);
		System.out.println("Number of columns in the Department Salary Setting Mathematics Report Worksheet: " + DepartmentSalarySettingMathematicsReport.getColumnCount(DepartmentSalarySettingWorksheetName));
		
		DepartmentSalarySettingPhysicsReport = new SSExcelReader(directory + DepartmentSalarySettingPhysicsReportName, DepartmentSalarySettingWorksheetHeaderRow);
		DepartmentSalarySettingPhysicsWorksheetLastRow = DepartmentSalarySettingPhysicsReport.getRowCount(DepartmentSalarySettingWorksheetName);
		System.out.println("Rows in the Department Salary Setting Physics Report Worksheet: " + DepartmentSalarySettingPhysicsWorksheetLastRow);
		System.out.println("Number of columns in the Department Salary Setting Physics Report Worksheet: " + DepartmentSalarySettingPhysicsReport.getColumnCount(DepartmentSalarySettingWorksheetName));
		
		DepartmentSalarySettingStatisticsReport = new SSExcelReader(directory + DepartmentSalarySettingStatisticsReportName, DepartmentSalarySettingWorksheetHeaderRow);
		DepartmentSalarySettingStatisticsWorksheetLastRow = DepartmentSalarySettingStatisticsReport.getRowCount(DepartmentSalarySettingWorksheetName);
		System.out.println("Rows in the Department Salary Setting Statistics Report Worksheet: " + DepartmentSalarySettingStatisticsWorksheetLastRow);
		System.out.println("Number of columns in the Department Salary Setting Statistics Report Worksheet: " + DepartmentSalarySettingStatisticsReport.getColumnCount(DepartmentSalarySettingWorksheetName));
		
		DepartmentSalarySettingTheaterAndPerformanceStudiesReport = new SSExcelReader(directory + DepartmentSalarySettingTheaterAndPerformanceStudiesReportName, DepartmentSalarySettingWorksheetHeaderRow);
		DepartmentSalarySettingTheaterAndPerformanceStudiesWorksheetLastRow = DepartmentSalarySettingTheaterAndPerformanceStudiesReport.getRowCount(DepartmentSalarySettingWorksheetName);
		System.out.println("Rows in the Department Salary Setting Theater And Performance Studies Report Worksheet: " + DepartmentSalarySettingTheaterAndPerformanceStudiesWorksheetLastRow);
		System.out.println("Number of columns in the Department Salary Setting Theater And Performance Studies Report Worksheet: " + DepartmentSalarySettingTheaterAndPerformanceStudiesReport.getColumnCount(DepartmentSalarySettingWorksheetName));
		
		DepartmentVerificationReport = new SSExcelReader(directory + DepartmentVerificationReportName, DepartmentVerificationReportHeaderRow);
		DepartmentVerificationReportLastRow = DepartmentVerificationReport.getRowCount(DepartmentVerificationReportWorksheetName);
		System.out.println("Rows in the Department Verification Report Worksheet: " + DepartmentVerificationReportLastRow);
		System.out.println("Number of columns in the Department Verification Report Worksheet: " + DepartmentVerificationReport.getColumnCount(DepartmentVerificationReportWorksheetName));
		
		/*
		String[] columns = DepartmentVerificationReport.getColumns(DepartmentVerificationReportWorksheetName);
		System.out.println("columns derived from the Department Verification Report are as follows: ");
		for (int i=0;i<columns.length;i++) System.out.print(columns[i] + "; ");
		System.out.println();
		*/
		
		ProvostBinderListByDepartment = new SSExcelReader(directory + ProvostBinderListByDepartmentFileName, ProvostBinderListByDepartmentWorksheetHeaderRow);
		for (int i = 0; i < ProvostBinderListByDepartmentWorksheetNames.length; i++){
			ProvostBinderListByDepartmentWorksheetLastRows[i] = ProvostBinderListByDepartment.getRowCount(ProvostBinderListByDepartmentWorksheetNames[i]);
			System.out.println("Rows in the Provost Binder "+ ProvostBinderListByDepartmentWorksheetNames[i] + " Worksheet: " + ProvostBinderListByDepartmentWorksheetLastRows[i]);
			System.out.println("Number of columns in the Provost Binder "+ ProvostBinderListByDepartmentWorksheetNames[i] + " Worksheet: " + ProvostBinderListByDepartment.getColumnCount(ProvostBinderListByDepartmentWorksheetNames[i]));
		}
		
		ProvostBinderListByName = new SSExcelReader(directory + ProvostBinderListByNameFileName, ProvostBinderListByNameWorksheetHeaderRow);
		for (int i = 0; i < ProvostBinderListByNameWorksheetNames.length; i++){
			ProvostBinderListByNameWorksheetLastRows[i] = ProvostBinderListByName.getRowCount(ProvostBinderListByNameWorksheetNames[i]);
			System.out.println("Rows in the Provost Binder "+ ProvostBinderListByNameWorksheetNames[i] + " Worksheet: " + ProvostBinderListByNameWorksheetLastRows[i]);
			System.out.println("Number of columns in the Provost Binder "+ ProvostBinderListByNameWorksheetNames[i] + " Worksheet: " + ProvostBinderListByName.getColumnCount(ProvostBinderListByNameWorksheetNames[i]));
		}

		ProvostBinderListBySalary = new SSExcelReader(directory + ProvostBinderListBySalaryFileName, ProvostBinderListBySalaryWorksheetHeaderRow);
		for (int i = 0; i < ProvostBinderListBySalaryWorksheetNames.length; i++){
			ProvostBinderListBySalaryWorksheetLastRows[i] = ProvostBinderListBySalary.getRowCount(ProvostBinderListBySalaryWorksheetNames[i]);
			System.out.println("Rows in the Provost Binder "+ ProvostBinderListBySalaryWorksheetNames[i] + " Worksheet: " + ProvostBinderListBySalaryWorksheetLastRows[i]);
			System.out.println("Number of columns in the Provost Binder "+ ProvostBinderListBySalaryWorksheetNames[i] + " Worksheet: " + ProvostBinderListBySalary.getColumnCount(ProvostBinderListBySalaryWorksheetNames[i]));
		}


		SchoolSalarySettingReport = new SSExcelReader(directory + SchoolSalarySettingReportName, SchoolSalarySettingReportHeaderRow);
		SchoolSalarySettingReportLastRow = SchoolSalarySettingReport.getRowCount(SchoolSalarySettingReportWorksheetName);
		System.out.println("Rows in the School Salary Setting Report Worksheet: " + SchoolSalarySettingReportLastRow);
		System.out.println("Number of columns in the School Salary Setting Report Worksheet: " + SchoolSalarySettingReport.getColumnCount(SchoolSalarySettingReportWorksheetName));

		MasterSalarySettingReport = new SSExcelReader(directory + MasterSalarySettingReportName, MasterSalarySettingReportHeaderRow);
		MasterSalarySettingReportLastRow = MasterSalarySettingReport.getRowCount(MasterSalarySettingReportWorksheetName);
		System.out.println("Rows in the Master Salary Setting Report Worksheet: " + MasterSalarySettingReportLastRow);
		System.out.println("Number of columns in the Master Salary Setting Report Worksheet: " + MasterSalarySettingReport.getColumnCount(MasterSalarySettingReportWorksheetName));
		
	}
	
	public static String getLogDirectoryName(){
		return LogDirectoryName;
	}
	
	public static BufferedWriter getLogFileWriter(String logFileName) throws Exception{
		File logFile = new File(logFileName);
		  
		if (! logFile.exists()){
			if(logFile.createNewFile()){
				System.out.println("New Log file created: " + logFileName);
			}
			else{
				System.out.println("Couldn't create new logfile: "+ logFileName);
			}
		}
		else{
			System.out.println("Log file already exists: " + logFileName);
			logFile.delete();
			if(logFile.createNewFile()){
				System.out.println("New Log file created: " + logFileName);
			}
			else{
				System.out.println("Couldn't create new logfile: " + logFileName);
			}
		}
		  
		FileWriter FW = new FileWriter(logFile);
		return new BufferedWriter(FW);
	}// end getLogFileWriter method
	
	  public static void writeTestResults(String testEnv, String testCaseNumber, String testDescription, 
			  	String testCase, StringBuffer verificationErrors) throws Exception{
		  String verificationErrorString = verificationErrors.toString();
		  if (verificationErrorString.length() > 4000)//we have to limit the number of chars so that it doesn't cause a contraint violation in Excel
			  verificationErrorString = verificationErrorString.substring(0, 4000);
		    int lastRow = 1;
		    if (!"".equals(verificationErrorString)) {
		    	System.out.println("Test '"+ testCase +"' FAILED");
		    	System.out.println(verificationErrorString);
		    	lastRow = fileOut.getRowCount(errorWorksheetName) + 1;
		    	Thread.sleep(1000);
		    	fileOut.setCellData(errorWorksheetName, "Test Env.", lastRow, testEnv);
		    	Thread.sleep(1000);
		    	fileOut.setCellData(errorWorksheetName, "Test Case #", lastRow, testCaseNumber);
		    	Thread.sleep(1000);
		    	fileOut.setCellData(errorWorksheetName, "Test Description", lastRow, testDescription);
		    	Thread.sleep(1000);
		    	fileOut.setCellData(errorWorksheetName, "Test Case", lastRow, testCase);
		    	Thread.sleep(1000);
		    	fileOut.setCellData(errorWorksheetName, "Cumulative Error String", lastRow, verificationErrorString);
		    	Thread.sleep(1000);
		    	//we had to comment this out.  If one test fails, we still want the other tests executed.  
		    	//This code, unfortunately, kills all test execution once the "fail" method is called.
		    	//fail(verificationErrorString);
		    }
		    else {
		    	System.out.println("Test passed");
		    	lastRow = fileOut.getRowCount(outputWorksheetName) + 1;
		    	Thread.sleep(1000);
		    	fileOut.setCellData(outputWorksheetName, "Test Env.", lastRow, testEnv);
		    	Thread.sleep(1000);
		    	fileOut.setCellData(outputWorksheetName, "Test Case #", lastRow, testCaseNumber);
		    	Thread.sleep(1000);
		    	fileOut.setCellData(outputWorksheetName, "Test Description", lastRow, testDescription);
		    	Thread.sleep(1000);
		    	fileOut.setCellData(outputWorksheetName, "Test Case", lastRow, testCase);
		    }
		  
	  }
	
	
	  public static void writeToLogFile(String toBeWritten, BufferedWriter logFileWriter) throws Exception{
		  String[] stringsToWrite = toBeWritten.split("\n");
		  if (stringsToWrite.length == 0 || stringsToWrite.length ==1)
			  logFileWriter.write(toBeWritten);
		  else{//there are regex strings indicating line splits
			  for(int i=0; i<stringsToWrite.length; i++){
				  logFileWriter.write(stringsToWrite[i]);
				  logFileWriter.newLine();
			  }
		  }
		  logFileWriter.newLine();//even if there's already a new line, we still put it in
	  }
	  
	  public static void copyScreenShotFile(File screenShotFile, String whichValues, String fileName, BufferedWriter logFileWriter) throws Exception{
		  writeToLogFile("Now attempting to copy the screen shot file to the correct directory", logFileWriter);
		  String destFileName = LogDirectoryName + "/" + whichValues +"/" + fileName;
		  File newScreenShotFile = new File(destFileName);
		  try{
			  FileUtils.copyFile(screenShotFile, newScreenShotFile);
			  writeToLogFile("Screen Shot File " + fileName +" has been successfully copied", logFileWriter);
		  }
		  catch(Exception e){
			  writeToLogFile("Error - could not copy screen shot file " + destFileName, logFileWriter);
			  writeToLogFile(e.getStackTrace().toString(), logFileWriter);
			  
		  }
	  }//end copyScreenShotFile method
	
	  public static void copyFile(String source, String dest, String logDirectory, BufferedWriter logFileWriter) throws Exception{
		  copyFile(source, dest, logDirectory, logFileWriter, 8);
	  }
	  
	  public static void copyFile(String source, String dest, String logDirectory, BufferedWriter logFileWriter, int waitIterations) throws Exception{
		  copyFile("C:\\Users\\christ13\\Downloads\\", source, dest, logDirectory, logFileWriter, waitIterations);
	  }//end copyFile method
	  
	  public static void copyFile(String srcDirectory, String source, String dest, String logDirectory, BufferedWriter logFileWriter, int waitIterations) throws Exception{
		  String sourceFileName = srcDirectory + source;
		  boolean fileIsComplete = false;
		  File reportFile = new File(sourceFileName);
		  if (reportFile.exists()) writeToLogFile("File "+ sourceFileName +" is present", logFileWriter);
		  else {
			  for (int i=1; (i<=waitIterations) && (! fileIsComplete); i++){
				  fileIsComplete = (reportFile.canRead() && reportFile.canWrite());
				  String message = "File "+ sourceFileName +" has not been created in the original directory " 
						  +" - waiting 20 secs before copying - iteration " + i;
				  System.out.println(message);
				  writeToLogFile(message, logFileWriter);
				  Thread.sleep(20000);
				  if (reportFile.exists() && fileIsComplete) {
					  writeToLogFile("File "+ sourceFileName +" has been created", logFileWriter);
					  break;
				  }//get out of the loop once the source file is created
			  }//end for loop - waiting for the source file to be created
		  }
		  
		  writeToLogFile("Now attempting to copy the file from the old location to the new one", logFileWriter);
		  String destFileName = "C:\\Users\\christ13\\Documents\\HandSOn\\" + dest;
		  File newReportFile = new File(destFileName);
		  if (newReportFile.exists()){ 
			  	String message = "Report file already exists in the new location " + destFileName + " - must delete and recreate";
			  	writeToLogFile(message, logFileWriter);
			  	System.out.println(message);
			  	if (newReportFile.canRead() && newReportFile.canWrite())
			  			System.out.println("All file permissions seem valid");
			  	if (!newReportFile.delete()){
			  		newReportFile = null;
			  		System.gc();
			  		newReportFile = new File(destFileName);
			  		System.gc();
			  		try{
			  			FileUtils.forceDelete(newReportFile);
			  		}
			  		catch(IOException e){
			  			message = "*** WARNING *** Unable to delete file " 
			  					+ destFileName +" - leaving old copy";
			  			System.out.println(message);
			  			writeToLogFile(message, logFileWriter);
			  			reportFile.deleteOnExit();
			  			newReportFile.deleteOnExit();
			  		}
			  	}
			  	Thread.sleep(10000);
			  	newReportFile = new File(destFileName);
		  }
		  else writeToLogFile("Report file does NOT already exist in the new location", logFileWriter);
		  
		  if (newReportFile.exists()) writeToLogFile("Destination file " + destFileName +" exists", logFileWriter);
		  else writeToLogFile("*** Destination file " + destFileName +" does not exist*** ", logFileWriter);
		  if (reportFile.exists()) writeToLogFile ("Source file " + sourceFileName +" exists", logFileWriter);
		  else writeToLogFile("**** Source File "+ sourceFileName +" does not exist", logFileWriter);
		  try{
			  FileUtils.copyFile(reportFile, newReportFile);
		  }
		  catch(FileNotFoundException e){
			  System.out.println("Either source or destination report file still does not exist in original location - waiting two more minutes");
			  Thread.sleep(120000);
			  try {
				  FileUtils.copyFile(reportFile, newReportFile);
			  }
			  catch(FileNotFoundException e2) {
				  System.out.println("Either source or destination report file still does not exist in original location - aborting copy");
				  e2.printStackTrace();
				  String newLogDirectoryName = "C:\\Users\\christ13\\Documents\\HandSOn\\";
				  String oldLogDirectoryName = "C:\\Users\\christ13\\Downloads";
				  newReportFile = new File(destFileName);
					if (! newReportFile.exists()){ 
						logFileWriter.write("The new report file "+destFileName+" has not been copied successfully");
						logFileWriter.newLine();
						logFileWriter.write("Here is a list of files that have filenames similar to the old file in the old directory:");
						logFileWriter.newLine();
						File oldLogDirectory = new File(oldLogDirectoryName);
						WildcardFileFilter fileFilter = new WildcardFileFilter("*CommitmentQueryExport*.csv");
						 Collection<File> files = FileUtils.listFiles(oldLogDirectory, fileFilter, null);
						 Iterator<File> iterator = files.iterator();
						  while(iterator.hasNext()) {
							  File file = iterator.next();
							  System.out.println(file.getName());
							  logFileWriter.write(file.getName());
							  logFileWriter.newLine();
						 }//end while
					}//end if
			  }//end inner catch clause
		  }//end outer catch clause
		  if (newReportFile.exists()) {
			  	writeToLogFile("Report file successfully copied from the old location to the new one", logFileWriter);
			  	FileUtils.copyFile(newReportFile, new File(logDirectory + "\\" + dest));
		  }//end if clause
		  else writeToLogFile("Report file NOT successfully copied from the old location to the new one", logFileWriter);
		  if (reportFile.exists())
			  FileUtils.forceDelete(reportFile);		  
	  }//end copyFile method
	  
	  public static void copyFile(String fileName, BufferedWriter logFileWriter, String logDirectory) throws Exception{
		  copyFile(fileName, logFileWriter, logDirectory, 25);
	  }//end copyFile method
	  
	  
	  public static void copyFile(String fileName, BufferedWriter logFileWriter, String logDirectory, int waitIterations) throws Exception{
		  String sourceFileName = "C:\\Users\\christ13\\Downloads\\" + fileName;
		  boolean fileIsComplete = false;
		  File reportFile = new File(sourceFileName);
		  if (reportFile.exists()) writeToLogFile("File "+ sourceFileName +" is present", logFileWriter);
		  else {
			  for (int i=1; (i<=waitIterations) && (! fileIsComplete); i++){
				  fileIsComplete = (reportFile.canRead() && reportFile.canWrite());
				  String message = "File "+ sourceFileName +" has not been created in the original directory " 
						  +" - waiting 20 secs before copying - iteration " + i;
				  System.out.println(message);
				  writeToLogFile(message, logFileWriter);
				  Thread.sleep(20000);
				  if (reportFile.exists() && fileIsComplete) {
					  writeToLogFile("File "+ sourceFileName +" has been created", logFileWriter);
					  break;
				  }//get out of the loop once the source file is created
			  }//end for loop - waiting for the source file to be created
		  }
		  
		  writeToLogFile("Now attempting to copy the file from the old location to the new one", logFileWriter);
		  String destFileName = "C:\\Users\\christ13\\Documents\\HandSOn\\" + fileName;
		  File newReportFile = new File(destFileName);
		  if (newReportFile.exists()){ 
			  	String message = "Report file already exists in the new location " + destFileName + " - must delete and recreate";
			  	writeToLogFile(message, logFileWriter);
			  	System.out.println(message);
			  	if (newReportFile.canRead() && newReportFile.canWrite())
			  			System.out.println("All file permissions seem valid");
			  	if (!newReportFile.delete()){
			  		newReportFile = null;
			  		System.gc();
			  		newReportFile = new File(destFileName);
			  		System.gc();
			  		try{
			  			FileUtils.forceDelete(newReportFile);
			  		}
			  		catch(IOException e){
			  			message = "*** WARNING *** Unable to delete file " 
			  					+ destFileName +" - leaving old copy";
			  			System.out.println(message);
			  			writeToLogFile(message, logFileWriter);
			  			reportFile.deleteOnExit();
			  			newReportFile.deleteOnExit();
			  		}
			  	}
			  	Thread.sleep(10000);
			  	newReportFile = new File(destFileName);
		  }
		  else writeToLogFile("Report file does NOT already exist in the new location", logFileWriter);
		  
		  if (newReportFile.exists()) writeToLogFile("Destination file " + destFileName +" exists", logFileWriter);
		  else writeToLogFile("*** Destination file " + destFileName +" does not exist*** ", logFileWriter);
		  if (reportFile.exists()) writeToLogFile ("Source file " + sourceFileName +" exists", logFileWriter);
		  else writeToLogFile("**** Source File "+ sourceFileName +" does not exist", logFileWriter);
		  try{
			  FileUtils.copyFile(reportFile, newReportFile);
		  }
		  catch(FileNotFoundException e){
			  System.out.println("Either source or destination report file still does not exist in original location - waiting another minute");
			  Thread.sleep(60000);
			  FileUtils.copyFile(reportFile, newReportFile);
		  }
		  if (newReportFile.exists()) {
			  	writeToLogFile("Report file successfully copied from the old location to the new one", logFileWriter);
			  	FileUtils.copyFile(newReportFile, new File(logDirectory + "\\" + fileName));
		  }
		  else writeToLogFile("Report file NOT successfully copied from the old location to the new one", logFileWriter);
		  
		  FileUtils.forceDelete(reportFile);		  
	  }
	  


}
