package Maven.SeleniumWebdriver;

import static org.junit.Assert.fail;

import java.io.BufferedWriter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DeptSalarySettingExceptionsTest {
	protected static String testName;
	protected static String testCaseNumber;
	protected static String testDescription;
	protected static String testEnv;
	protected static StringBuffer verificationErrors;
	protected BufferedWriter logFileWriter;

	@Before
	public void setUp() throws Exception {
		testName = "DeptSalarySettingExceptionsTest";
		testEnv = "Test";
		testCaseNumber = "055";
		testDescription = "Test of Exceptions for Department SSWs";
	}//end setUp method

	@Test
	public void test() throws Exception{
		verificationErrors = new StringBuffer();
		String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		logFileWriter = TestFileController.getLogFileWriter(logFileName);
		String[] depts = HandSOnTestSuite.salarySettingMgrTabUI.getDepartments("Exceptions", logFileWriter);
		logFileWriter.write("The following Salary Setting Worksheets will be examined: ");
		logFileWriter.newLine();
		for (int i=0; i<depts.length; i++){
			String fileName = "Department Salary Setting Worksheet "+ depts[i] + ".xlsm";
			logFileWriter.write(fileName);
			logFileWriter.newLine();
		}//end for loop
		
		//download the department SSW's
		logFileWriter.write("Now attempting to download these Department Salary Setting Worksheets:");
		logFileWriter.newLine();
		if (! SalarySettingManager.switchWindowByURL(SalarySettingManager.salarySettingURL, logFileWriter)) {
			if (! UIController.openSalarySetting(logFileWriter)) {
				String message = "Cannot open Salary Setting - exiting";
				logFileWriter.write(message);
				logFileWriter.newLine();
				fail(message);
			}//end if
		}//end if
		HandSOnTestSuite.salarySettingMgrTabUI.switchToReportTab();
		ReportRunner.downloadDeptSSWs(depts, ReportRunner.referenceWhichValues, logFileWriter);

		for (int i=0; i<depts.length; i++){
			String[] faculty = HandSOnTestSuite.salarySettingMgrTabUI.getFacultyForDepartment(depts[i], "Exception", logFileWriter);
			for (int j=0; j<faculty.length; j++){
				logFileWriter.write("We will attempt to find Professor "+faculty[j]
						+", in the " + depts[i] +" Department");
				logFileWriter.newLine();
				String exclusionOrInclusion 
					= HandSOnTestSuite.salarySettingMgrTabUI.getExceptionAttributeForNameAndDept(faculty[j], 
							depts[i], "ExcludeOrInclude", logFileWriter);
				if (exclusionOrInclusion.equalsIgnoreCase("Exclude")){
					logFileWriter.write(" * Examining the file for Exclusion *");
					logFileWriter.newLine();
					if (! containsExcludedFaculty(faculty[j], depts[i], logFileWriter)){
						if (! isChair(faculty[j], logFileWriter)){
							String message = "ERROR - The file doesn't contain the excluded faculty "+faculty[j] +", as expected";
							logFileWriter.write(message); logFileWriter.newLine();
							verificationErrors.append(message);
						}//end if - the faculty member is not a Chair and should have appeared there
						else{
							logFileWriter.write("Faculty member " + faculty[j] 
									+" is a Chair and is therefore not included in the Department SSW");
							logFileWriter.newLine();
						}
					}//end if - the excluded faculty is not in the file
					else {
						logFileWriter.write("The file contains the excluded faculty " + faculty[j] +", as expected");
						logFileWriter.newLine();
					}
				}//end if - this is an exclusion
				else {//this is an inclusion
					logFileWriter.write(" * Examining the file for Inclusion *");
					logFileWriter.newLine();
					if (containsIncludedFaculty(faculty[j], depts[i], logFileWriter)){
						logFileWriter.write("Included Faculty member " + faculty[j] 
								+" is in the Dept SSW for "+depts[i] +", as expected");
						logFileWriter.newLine();
					}//end if - they're included
					else{
						if (! isChair(faculty[j], logFileWriter)){
							String message = "ERROR - Included Faculty member " + faculty[j] 
									+" is NOT in the Dept SSW for "+depts[i] +", as expected";
							logFileWriter.write(message);
							logFileWriter.newLine();
							verificationErrors.append(message);
						}//end if - the individual is not a Chair and should have appeared
						else{
							logFileWriter.write("Faculty member "+faculty[j] 
									+" is a Chair, and shouldn't appear in the Dept SSW");
							logFileWriter.newLine();
						}
					}//end inner else - the Included Faculty is not in the file, as expected
				}//end else - included faculty
			}//end inner for - faculty
		}//end outer for loop - departments

	}//end test method

	
	private boolean containsExcludedFaculty(String facultyName, String dept, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method containsExceptionFaculty called with faculty name " + facultyName
				+", and Department "+dept + " ***");
		logFileWriter.newLine();
		String fileName = "Department Salary Setting Worksheet "+dept+".xlsm";
		SSExcelReader myReader = new SSExcelReader(TestFileController.directory +"/" +fileName, TestFileController.DepartmentSalarySettingWorksheetHeaderRow);
		int rowCount = myReader.getRowCount(TestFileController.DepartmentSalarySettingWorksheetName); 
		logFileWriter.write("Row count for instantiated reader for " + fileName +" is " + rowCount);
		logFileWriter.newLine();

		int rowForColNames = myReader.getCellRowNum(TestFileController.DepartmentSalarySettingWorksheetName, 0, SalarySettingManagementUIController.exclusionSectionHeading);
		logFileWriter.write("Estimated location of excluded faculty at row " + rowForColNames);
		logFileWriter.newLine();

		int actualRowForColNames = myReader.getCellRowNum(TestFileController.DepartmentSalarySettingWorksheetName, 0, "Name", rowForColNames);
		logFileWriter.write("Actual header row for excluded faculty is " + actualRowForColNames);
		logFileWriter.newLine();
		
		boolean foundUnexpectedString = false;
		int rowNum = -1;
		if (actualRowForColNames != -1){
			myReader.setRowForColumnNames(rowForColNames);
			String[] colNames = {"Name", "Reason", "Description"};
			for (int j=0; j<colNames.length; j++){
				if (colNames[j].equalsIgnoreCase("Name") )
					rowNum = myReader.getCellRowNum(TestFileController.DepartmentSalarySettingWorksheetName, colNames[j], facultyName);
				String value = myReader.getCellData(TestFileController.DepartmentSalarySettingWorksheetName, colNames[j], rowNum);
				logFileWriter.write("Value for the "+ colNames[j] +" column is found: "+ value);
				logFileWriter.newLine();
				
				String UIValue = new String();
				UIValue = HandSOnTestSuite.salarySettingMgrTabUI.getExceptionAttributeForNameAndDept(facultyName, dept, colNames[j], logFileWriter);
				if ((value == null) || (UIValue == null) || (value.isEmpty()) || (UIValue.isEmpty()) ||(! UIValue.equalsIgnoreCase(value)))
					foundUnexpectedString = true;
			}//end for loop
		}//end if
		
		TestFileController.closeOneExcelReader(myReader);
		
		return ((rowForColNames != -1) && (actualRowForColNames != -1) && (rowNum != -1)
				&& (actualRowForColNames == rowForColNames + 1) && (! foundUnexpectedString));
	}//end containsExceptionFaculty method
	
	public boolean containsIncludedFaculty(String facultyName, String dept, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method containsIncludedFaculty called with faculty name " + facultyName
				+", and Department "+dept + " ***");
		logFileWriter.newLine();
		String fileName = "Department Salary Setting Worksheet "+dept+".xlsm";
		SSExcelReader myReader = new SSExcelReader(TestFileController.directory +"/" +fileName, TestFileController.DepartmentSalarySettingWorksheetHeaderRow);
		int rowCount = myReader.getRowCount(TestFileController.DepartmentSalarySettingWorksheetName); 
		logFileWriter.write("Row count for instantiated reader for " + fileName +" is " + rowCount);
		logFileWriter.newLine();
		
		myReader.setRowForColumnNames(TestFileController.DepartmentSalarySettingWorksheetHeaderRow);
		int rowNum = myReader.getCellRowNum(TestFileController.DepartmentSalarySettingWorksheetName, "Name", facultyName);
		logFileWriter.write("Estimated location of included faculty at row " + rowNum);
		logFileWriter.newLine();
		
		boolean foundUnexpectedString = false;		
		if (rowNum != -1){
			String colName = "16-17 Description";
			String cellValue = myReader.getCellData(TestFileController.DepartmentSalarySettingWorksheetName, colName, rowNum);
			String UIValue = new String();
			UIValue = HandSOnTestSuite.salarySettingMgrTabUI.getExceptionAttributeForNameAndDept(facultyName, dept, "Description", logFileWriter);
			if (cellValue.contains(UIValue)){
				logFileWriter.write("Included Faculty attribute with column " +colName +" includes expected text "+UIValue);
				logFileWriter.newLine();
			}//end if - Inclusion Description is in the file, as expected
			else{
				foundUnexpectedString = true;
				logFileWriter.write("ERROR - Included Faculty attribute with column " +colName +" DOES NOT INCLUDE expected text "+UIValue);
				logFileWriter.newLine();				
			}//end else - the Description is not there
		}//end if - they found the proper row
		else if (! isChair(facultyName, logFileWriter)){
			logFileWriter.write("ERROR - Could not find Included Faculty member " +facultyName +" in the Department SSW for Department "+dept);
			logFileWriter.newLine();
		}//end else
		
		TestFileController.closeOneExcelReader(myReader);
		
		return ((rowNum != -1) && (! foundUnexpectedString));
		
	}//end containsIncludedFaculty method
	
	/*
	 * This method returns true if and only if the the faculty member is a Chair.
	 */
	private boolean isChair(String name, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method isChair called for name " + name +" ****");
		logFileWriter.newLine();
		
		SSExcelReader reader = TestFileController.SchoolSalarySettingReport;
		String worksheet = TestFileController.SchoolSalarySettingReportWorksheetName;
		reader.setRowForColumnNames(TestFileController.SchoolSalarySettingReportHeaderRow);
		
		int rowNum = reader.getCellRowNum(worksheet, "Name", name);
		logFileWriter.write("Found name "+name+" at row " +rowNum+", of the School Salary Setting Report");
		logFileWriter.newLine();
		
		if (rowNum == -1) 
			return false;
		else {
			String cellData = reader.getCellData(worksheet, "16-17 Description", rowNum);
			if ((cellData == null) || (cellData.isEmpty())){
				logFileWriter.write("Did not find any Description data in the School Salary Setting Report");
				logFileWriter.newLine();
				reader.setRowForColumnNames(reader.getCellRowNum(worksheet, 0, SalarySettingManagementUIController.exclusionSectionHeading));
				cellData = reader.getCellData(worksheet, "Description", rowNum);
				if ((cellData == null) || (cellData.isEmpty())){
					logFileWriter.write("Got no value from the Description column");
					logFileWriter.newLine();					
				}//end if
				else {
					logFileWriter.write("Got value "+ cellData + " from the Description column");
					logFileWriter.newLine();
				}//end else
			}//end if
			return cellData.contains("Chair -");
		}//end else
	}//end isChair method


	@After
	public void tearDown() throws Exception {
		TestFileController.writeTestResults(testEnv, testCaseNumber, testName, testDescription, verificationErrors);
		logFileWriter.newLine();
		logFileWriter.close();
	}//end tearDown method


}
