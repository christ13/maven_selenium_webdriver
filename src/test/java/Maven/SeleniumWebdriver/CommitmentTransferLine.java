package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

public class CommitmentTransferLine {

		public String[] values;
		public int row = 1;
		public static final int SubCategory = 0;
		public static final int FiscalYear = 1;
		public static final int FundingUnit = 2;
		public static final int SourcePTA = 3;
		public static final int DestinationPTA = 4;
		public static final int ExpenseArea = 5;
		public static final int SummerNinths = 6;
		public static final int SyncCheckbox = 7;
		public static final int Amount = 8;
		public static final int Benefits = 9;
		public static final int TotalAmount = 10;
		public static final int TransferStatus = 11;
		public static final int TransferDate = 12;
		public static final int iBudget = 13;
		public static final int iJournal = 14;
		public static final int holdReason = 15;
		public static final int SendButton = 16;// as per HANDSON-3777
		public static final int Approval = 17;//when an adjustment is made
		public static final String myWorksheet = "Commitment Transfer Line";
		public static final HSSFExcelReader myReader = TestFileController.fileIn;
		
		public CommitmentTransferLine(int row, BufferedWriter logFileWriter) throws Exception{
			values = new String[Approval+1];
			this.row = row;
			initialize(logFileWriter);
			logFileWriter.write("Newly formed CommitmentTransferLine is as follows: ");
			logFileWriter.newLine();
			logFileWriter.write(this.toString());
			logFileWriter.newLine();
		}//end constructor
		
		public String toString(){
			String valueString = new String();
			for (int i=0; i<values.length; i++){
				valueString = valueString + this.getStringValueForIndex(i)+": "+values[i]+"; ";
			}//end for
			return valueString;
		}//end toString method 
				
		//this should only be used when we are on the Commitments Page
		public void initialize(BufferedWriter logFileWriter) throws Exception{
			String baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName (myWorksheet, "SubCategorySelect", logFileWriter);
			String amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			By by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			values[SubCategory] = HandSOnTestSuite.searchAuthorizationUI.getSelectedTextForBy(by, "SubCategorySelect", logFileWriter);
			
			baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName (myWorksheet, "FiscalYearSelect", logFileWriter);
			amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			try {
				values[FiscalYear] = HandSOnTestSuite.searchAuthorizationUI.getSelectedTextForBy(by, "FiscalYearSelect", logFileWriter);
			}
			catch (StaleElementReferenceException e) {
				logFileWriter.write("Got a StaleElementReferenceException - re-trying");
				logFileWriter.newLine();
				by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
				values[FiscalYear] = HandSOnTestSuite.searchAuthorizationUI.getSelectedTextForBy(by, "FiscalYearSelect", logFileWriter);
			}
			baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName (myWorksheet, "FundingUnitSelect", logFileWriter);
			amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			values[FundingUnit] = HandSOnTestSuite.searchAuthorizationUI.getSelectedTextForBy(by, "FundingUnitSelect", logFileWriter);

			baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName (myWorksheet, "SourcePTATextField", logFileWriter);
			amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			values[SourcePTA] = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);

			baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName (myWorksheet, "DestinationPTATextField", logFileWriter);
			amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			values[DestinationPTA] = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);

			baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName (myWorksheet, "ExpenseAreaSelect", logFileWriter);
			amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			values[ExpenseArea] = HandSOnTestSuite.searchAuthorizationUI.getSelectedTextForBy(by, "ExpenseAreaSelect", logFileWriter);

			baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName (myWorksheet, "SummerNinthsTextField", logFileWriter);
			amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			values[SummerNinths] = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
			
			baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName (myWorksheet, "SyncCheckboxInput", logFileWriter);
			amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			WebElement checkbox = null;
			try{
				checkbox = UIController.driver.findElement(by);
				if (checkbox.isSelected())
					values[SyncCheckbox] = "checked";
				else values[SyncCheckbox] = "unchecked";
			}
			catch(Exception e) {
				values[SyncCheckbox] = "checked";
			}
			logFileWriter.write("value of Sync Checkbox is "+values[SyncCheckbox]);
			logFileWriter.newLine();

			baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName (myWorksheet, "AmountTextField", logFileWriter);
			amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			values[Amount] = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);

			baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName (myWorksheet, "BenefitsLabel", logFileWriter);
			amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			values[Benefits] = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);

			baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName (myWorksheet, "TotalAmountLabel", logFileWriter);
			amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			values[TotalAmount] = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);

			baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName (myWorksheet, "TransferStatusSelect", logFileWriter);
			amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			values[TransferStatus] = HandSOnTestSuite.searchAuthorizationUI.getSelectedTextForBy(by, "TransferStatusSelect", logFileWriter);

			baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName (myWorksheet, "TransferDateTextField", logFileWriter);
			amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			values[TransferDate] = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);

			baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName (myWorksheet, "iBudgetTextField", logFileWriter);
			amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			values[iBudget] = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);

			baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName (myWorksheet, "iJournalTextField", logFileWriter);
			amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			values[iJournal] = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);

			baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName (myWorksheet, "HoldReasonSelect", logFileWriter);
			amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			values[holdReason] = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);

			//this is added to determine whether or not the "Send" button (HANDSON-3777)
			baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName (myWorksheet, "SendButton", logFileWriter);
			amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			logFileWriter.write("value of Send Button is "+values[SendButton]);
			logFileWriter.newLine();
			
			//now, determine if the button is there and enabled as per HANDSON-3777
			WebElement button = null;
			try{
				button = UIController.driver.findElement(by);
				if (button.isEnabled() && button.isDisplayed())
					values[SendButton] = "true";
				else values[SendButton] = "false";
			}//end try clause
			catch(Exception e) {
				values[SendButton] = "false";
			}//end catch clause
			
			//now determine whether the Approval/Rejection dropdown is accessible
			baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName (myWorksheet, "ApprovalSelect", logFileWriter);
			amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			
			WebElement approvalSelect = null;
			try{
				approvalSelect = UIController.driver.findElement(by);
				if (approvalSelect.isEnabled() && approvalSelect.isDisplayed())
					values[Approval] = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
				else values[Approval] = "";
			}//end try clause
			catch(Exception e) {
				values[Approval] = "";
			}//end catch clause
			
		}//end initialize method
		
		public String getValueForField(int index, BufferedWriter logFileWriter) throws Exception{
			logFileWriter.write("*** method getValueForField called ***");
			logFileWriter.newLine();
			logFileWriter.write("...retrieving the value for "+getStringValueForIndex(index));
			logFileWriter.newLine();
			logFileWriter.write("... and we are returning value "+values[index]);
			logFileWriter.newLine();
			return values[index];
		}//end getValueForField
		
		public boolean isLineVisible(BufferedWriter logFileWriter) throws Exception{
			boolean isVisible = false;
			
			for (int i=0; i<values.length; i++){
				String whichValue = getStringValueForIndex(i);
				int rowNum = myReader.getCellRowNumWithSubstringMatch(myWorksheet, "Name", whichValue.replaceAll("\\s", ""), 1);
				logFileWriter.write("Row number "+rowNum+" is found for value "+whichValue);
				logFileWriter.newLine();
				String baseLocator = myReader.getCellData(myWorksheet, "Locator", rowNum);
				String amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
				By by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
				WebElement element = UIController.driver.findElement(by);
				if (element.isDisplayed())
					isVisible = true;
				logFileWriter.write("After analyzing value "+whichValue+", line visibility is determined to be "+isVisible);
				logFileWriter.newLine();
			}//end for
			return isVisible;
		}//end isVisible method
		
		public boolean checkTransferLineBox(BufferedWriter logFileWriter, StringBuffer verificationErrors, int row) throws Exception{
			boolean isChecked = false;
			
			String baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName (myWorksheet, "TransferLineCheckbox", logFileWriter);
			String amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			By by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			WebElement checkbox = null;
			try{
				checkbox = UIController.driver.findElement(by);
				checkbox.click();
				logFileWriter.write("Successfully clicked the checkbox on row "+row);
				logFileWriter.newLine();
			}//end try - we did it
			catch(Exception e){
				String message = "ERROR - could not click on the checkbox because of Exception "+e.getMessage()+";\n";
				logFileWriter.write(message);
				logFileWriter.newLine();
				System.out.println(message);
				verificationErrors.append(message);
				return isChecked;//stop right here - we can't go on
			}//end catch - we didn't do it.
			isChecked = checkbox.isSelected();
			if (isChecked){
				logFileWriter.write("Checkbox is checked on row "+row);
				logFileWriter.newLine();
			}//end if - it's checked - all is good
			else{
				String message = "Checkbox on row "+row+" is not checked;\n";
				logFileWriter.write(message);
				logFileWriter.newLine();
			}//end else - error (it's not checked)
			return isChecked;
		}//end checkTransferLineBox method
		
		public boolean setValue(int intValue, String newValue, BufferedWriter logFileWriter) throws Exception{
			if (intValue == this.DestinationPTA && newValue.length()>15)
				newValue = newValue.substring(0, 14);
			values[intValue] = newValue;
			
			String whichValue = getStringValueForIndex(intValue);
			logFileWriter.write("*** method setValue called - adjusting value "+whichValue+" to "+newValue+" ***");
			logFileWriter.newLine();
			boolean isSuccessful = false; 
			
			HSSFExcelReader myReader = SearchAuthorizationUIController.myExcelReader;
			int locatorRow = myReader.getCellRowNumWithSubstringMatch(myWorksheet, "Name", whichValue.replaceAll("\\s", ""), 1);
			String baseLocator = myReader.getCellData(myWorksheet, "Locator", locatorRow);
			String amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			By by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			Thread.sleep(2000);
			
			if (amendedLocator.contains("select")){
				logFileWriter.write("Attempting to set the value in a drop-down");
				logFileWriter.newLine();
				UIController.selectElementInMenu(by, newValue);
				isSuccessful = true;
			}//end if - this is a dropdown
			else if (amendedLocator.contains("input")){
				logFileWriter.write("Attempting to set the value in a textfield");
				logFileWriter.newLine();
				isSuccessful = HandSOnTestSuite.searchAuthorizationUI.writeStringToTextField(by, newValue, logFileWriter);
				//isSuccessful = true;
			}//end else of - this is a textfield			
			else{
				String message = "ERROR - We cannot process this locator "+amendedLocator+";\n";
				logFileWriter.write(message);
				logFileWriter.newLine();
				System.out.println(message);
			}//end else - this is an error condition
			
			if (isSuccessful)
				logFileWriter.write("*** method setValue finished successfully ***");
			else
				logFileWriter.write("ERROR - method setValue did not finish successfully");
			logFileWriter.newLine();
			return isSuccessful;
		}//end setValue method
		
		public String getStringValueForIndex(int index){
			String whichValue = new String();
			switch (index){
			case 0: whichValue = "SubCategory";
			break;
			case 1: whichValue = "Fiscal Year";
			break;
			case 2: whichValue = "Funding Unit";
			break;
			case 3: whichValue = "Source PTA";
			break;
			case 4: whichValue = "Destination PTA";
			break;
			case 5: whichValue = "Expense Area";
			break;
			case 6: whichValue = "Summer Ninths";
			break;
			case 7: whichValue = "Sync Checkbox";
			break;
			case 8: whichValue = "Amount";
			break;
			case 9: whichValue = "Benefits";
			break;
			case 10: whichValue = "Total Amount";
			break;
			case 11: whichValue = "Transfer Status";
			break;
			case 12: whichValue = "Transfer Date";
			break;
			case 13: whichValue = "iBudget";
			break;
			case 14: whichValue = "iJournal";
			break;
			case 15: whichValue = "Hold Reason";
			break;
			case 16: whichValue = "Send Button";// as per HANDSON-3777
			break;
			case 17: whichValue = "Approval Select";
			}//end switch
			return whichValue;
		}//end getStringValueForIndex method
		
		//if we only want to compare some values, as opposed to every single value on the whole transfer line
		public void compareOneTransferLineValue(CommitmentTransferLine toCompareWith, 
				StringBuffer verificationErrors, BufferedWriter logFileWriter, int i) throws Exception{
			String whichValue = getStringValueForIndex(i);
			String SendButtonShouldBePresent = new String();
			/*
			 * The 'Send' button makes for a big exception here.  This whole thing only applies to the 'Send' button.
			 * If we are comparing pre and post submission CLI's then the Send button may show up in post submission.
			 * It never shows up in pre-submission.
			 * There is a whole set of logic that determines whether or not the 'Send' button should show up.
			 * That logic is beyond the scope of this method.
			 * 
			 * If it is not the 'Send' button, then it should always be the same.
			 * If it is the 'Send' button, then we evaluate whether or not it should show up
			 * If we are not comparing it pre and post submission, there should be no difference between 
			 * ... this CLI value and the value of the CLI that it is being compared to.
			 * If it is pre-submission, it should never show up.
			 * If it is post-submission it may show up.
			 * If it is post-submission, and it is determined that it should show up, it should show up.
			 * If it is post-submission, and it is determined that it should not show up,it should not show up.
			 * 
			 * We may be able to eliminate some of the complexity here by evaluating whether or not the Commitment
			 * has been submitted by looking at the Commitment Status within the method that determines whether
			 * or not the 'Send' button should show up.
			 * 
			 * Two things to look at: (1) Does the 'Send' button show up in a Commitment which is submitted if the CLI is new?
			 * --> No, it does not.  The CLI has to be processed first.  Are we dealing with any new CLI's in a submitted Commitment?
			 * --> No, we are not. 
			 * (2) Is the Commitment Draft or Open status accessible to the code which evaluates whether or not the 
			 * ... 'Send' button shows up?
			 * 
			 */
			if (i == CommitmentTransferLine.SendButton) {
				SendButtonShouldBePresent = Boolean.toString(NewRecruitmentTest.shouldBePresent(this, logFileWriter));
				if (toCompareWith.values[i].equalsIgnoreCase(SendButtonShouldBePresent)){
					logFileWriter.write(whichValue +" at row "+row+" is as expected: "+SendButtonShouldBePresent);
					logFileWriter.newLine();
				}//end if
				else {
					String message = "ERROR - Send Button at row "+row+" is '"+toCompareWith.values[i]
							+", but it should be '"+SendButtonShouldBePresent+"';\n ";
					logFileWriter.write(message);
					logFileWriter.newLine();
					verificationErrors.append(message);
					System.out.println(message);
				}//else - it's not as it should be
			}//handle the 'Send' button, specifically
			else {//it's not the 'Send' button
				if (this.values[i].equalsIgnoreCase(toCompareWith.values[i])){
					logFileWriter.write(whichValue+" at row "+row+" is as expected");
					logFileWriter.newLine();
				}//end if
				else {//it's not the Send Button, so it should be equal
					String message = "ERROR - "+ whichValue+" at row "+row+" is expected to be "+values[i] 
							+", but is actually '"+toCompareWith.values[i]+"'; \n";
					logFileWriter.write(message);
					logFileWriter.newLine();
					verificationErrors.append(message);
					System.out.println(message);
				}//end else
			}//end else
		}//end compareOneTransferLineValue method
		
	
		//if we want to compare all of the values in the whole transfer line
		public void compareToTransferLine(CommitmentTransferLine toCompareWith, 
				StringBuffer verificationErrors, BufferedWriter logFileWriter) throws Exception{
			for (int i=0; i<values.length; i++){
				compareOneTransferLineValue(toCompareWith, verificationErrors, logFileWriter, i);
			}//end for loop
		}//end compareToTransferLine method

}//end Class
