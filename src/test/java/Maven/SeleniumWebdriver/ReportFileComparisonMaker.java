package Maven.SeleniumWebdriver;



import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({
	ControlSheetReportSuiteAPRefValuesTestSuite.class, 
	DeptVerificationReportAPRefValuesTest.class,
	DeptSalarySettingWorksheetAPRefValuesTestSuite.class,
	ProvostBinderListByDepartmentAPRefValuesTestSuite.class,
	SchoolSalarySettingReportAPRefValuesTest.class,
	MasterSalarySettingReportAPRefValuesTest.class,
	ASCTestSuite.class,
	ProvostBinderTestDBVerificationSuite.class
	})

public class ReportFileComparisonMaker {

	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws Exception{
	}
	
	

}
