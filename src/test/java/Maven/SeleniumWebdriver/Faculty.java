package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/*
 * This Class contains and compares historical and current Faculty data.  Once instantiated, it will perform most of the 
 * interaction with the FacultyUIController.  
 */
public class Faculty {

	private static String name;
	private static String adminAppt = "NONE";
	private static String testCaseID;
	private static int testCaseIDNumber;
	private static String retentionID;
	private static String handSOnID;
	private static String manualSalaryLogIncreaseEffectiveDate;
	private static float initialOneHundredPercentFTESalary;
	private static float initialFTEAdjustedSalary;
	private static int fte;
	//re-define the variables later
	/*
	private static float currentSalary;
	private static float initialCumulativeAnnualIncrease;
	private static float currentCumulativeAnnualIncrease;
	private static float retentionIncrease;
	private static float manualSalaryLogIncrease;
	private static float cumulativeTestCaseIncrease;
	*/
	public Faculty (String testCase_ID) {
		testCaseID = testCase_ID;
		System.out.println("Faculty has been instantiated for Test Case ID "+testCaseID);
		testCaseIDNumber = new Integer(testCaseID.substring(new String("Test Case ID ").length())).intValue();
	}//end constructor
	
	public String getInitialFacultyData() throws Exception{
		String initialDataString = "Faculty associated with Test Case ID "+testCaseID+" has been instantiated.  ";
		adminAppt = TestDataRecorder.getTestCaseData(testCaseID, "Administrative Appointment");
		initialDataString = initialDataString.concat(";\nAdministrative Appointment is "+adminAppt);
		manualSalaryLogIncreaseEffectiveDate = TestDataRecorder.getTestCaseData(testCaseID, "Manual Salary Log Effective Date");
		initialDataString = initialDataString.concat(";\nManual Salary Log Increase Effective Date is "+manualSalaryLogIncreaseEffectiveDate);
		return initialDataString;
	}//end getInitialFacultyData method
	
	public void getRemainingFacultyData(BufferedWriter logFileWriter) throws Exception{
		name = FacultySelector.getFacultyName(adminAppt, testCaseID, logFileWriter);
		System.out.println("Obtained faculty name "+name+" for "+testCaseID);
		logFileWriter.write("Obtained faculty name "+name+" for "+testCaseID);
		logFileWriter.newLine();
		TestDataRecorder.setTestCaseData(testCaseID, TestDataRecorder.highLevelDataColumns[TestDataRecorder.NAME_OF_FACULTY_INDEX], name);
		handSOnID = TestDataRecorder.getTestCaseData(testCaseID, TestDataRecorder.highLevelDataColumns[TestDataRecorder.HANDSONID_INDEX]);
		logFileWriter.write("Retrieved HandSOn ID value of "+handSOnID);
		retentionID = getRetentionData(logFileWriter, name);
		TestDataRecorder.setTestCaseData(testCaseID, TestDataRecorder.highLevelDataColumns[TestDataRecorder.RETENTION_ID_INDEX], retentionID);
		FacultyUIController.navigateToFacultyRecord(logFileWriter, handSOnID);
		
		//copy the high level test case data to the low level sheet
		for(int j=0; j<=TestDataRecorder.LANGUAGE_OF_COMMITMENT_INDEX;j++) {
			TestDataRecorder.setInitialLowLevelTestCaseDatum(testCaseIDNumber, j);
		}//end inner for loop - initialize the low level values from the high level values
		getSalaryInfo(logFileWriter);
	}//end getRemainingFacultyData method
	
	public String getRetentionData(BufferedWriter logFileWriter, String name) throws Exception {
		String retentionID = new String();
		retentionID = RetentionCreator.createNewRetention(logFileWriter, name, testCaseID);
		return retentionID;
	}//end getRetentionData method
	
	/*
	 * This method returns an array with three float values - the first is the current 100% FTE salary,
	 * the second is the FTE Adjusted salary, and the last is the FTE value.
	 */
	public void getSalaryInfo(BufferedWriter logFileWriter) throws Exception {
		FacultyUIController.openSalaryTab(logFileWriter, name);
		String worksheetName = "Faculty Salary Page";
		
		String current100PctFTESalary = FacultyUIController.getStringValueFromLabelForName(logFileWriter, worksheetName, "CurrentOneHundredPercentSalaryLabel");
		TestDataRecorder.setLowLevelTestCaseDatum(testCaseIDNumber, TestDataRecorder.HUNDREDPCT_FTE_INITIAL_SALARY_INDEX, current100PctFTESalary);
		initialOneHundredPercentFTESalary = new Float(getNumberFormattedString(current100PctFTESalary)).floatValue();
		String currentFTEAdjustedSalary = FacultyUIController.getStringValueFromLabelForName(logFileWriter, worksheetName, "CurrentFTEAdjustedSalaryLabel");
		TestDataRecorder.setLowLevelTestCaseDatum(testCaseIDNumber, TestDataRecorder.FTE_ADJUSTED_INITIAL_SALARY_INDEX, currentFTEAdjustedSalary);
		initialFTEAdjustedSalary = new Float(getNumberFormattedString(currentFTEAdjustedSalary)).floatValue();
		if (initialOneHundredPercentFTESalary == initialFTEAdjustedSalary)
			fte = 100;
		DecimalFormat df = new DecimalFormat("#.##");
		String FTE_String = df.format((initialFTEAdjustedSalary/initialOneHundredPercentFTESalary)  * 100);
		TestDataRecorder.setLowLevelTestCaseDatum(testCaseIDNumber, TestDataRecorder.FACULTY_FTE_INDEX, FTE_String);
		fte = new Float(getNumberFormattedString(FTE_String)).intValue();
	}//end getSalaryInfo method
	
	public String getNumberFormattedString(String raw) {
		String noDollars = raw.replace("$", "");
		//now return the total with no dollars or commas
		return noDollars.replaceAll(",","");
	}//end getNumberFormattedString method

}
