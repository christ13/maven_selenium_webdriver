package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.LinkedList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MasterSalarySettingReportAPRefValuesTest {
	public static final int FacultyName = 0;
	public static final int Department = 1;
	public static final int Description = 2;
	public static final int DOShare = 3;
	public static final int MarketPoolDeptShare = 4;
	public static final int currentYearRaiseDollars = 5;
	public static final int currentYearRaiseAmt = 6;
	public static final int currentYearFTEAdjustedRaiseDollars = 7;

	public static final int numberOfColumns = 8;

	private static String worksheetName;
	private static SSExcelReader myExcelReader;

	private static String testName;
	private static String testCaseNumber;
	private static String testDescription;
	private static String testEnv;
	private static StringBuffer verificationErrors;

	@Before
	public void setUp() throws Exception {
		//System.out.println("setUp method for class MasterSalarySettingReportAPRefValuesTest is being executed");
		myExcelReader = TestFileController.MasterSalarySettingReport;
		worksheetName = TestFileController.MasterSalarySettingReportWorksheetName;
		testEnv = "Test";
		testName = "Test Master Salary Setting Report";
		testDescription = "Verify AP Values in Master Salary Setting Report";
		testCaseNumber = "021";
		/*
		System.out.println("Column Names are as follows: ");
		String[] colNames = myExcelReader.getColumnNames(worksheetName);
		for (int i=0;i<colNames.length;i++){
			System.out.print(colNames[i]);
		}
		System.out.println();
		*/
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws Exception{
		//System.out.println("Now executing test: " + testName);
		verificationErrors = new StringBuffer();
		String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		BufferedWriter logFileWriter = TestFileController.getLogFileWriter(logFileName);
		String[][] expectedValues = getInitialExpectedAPFacultyValues(logFileWriter);
		String[][] actualValues = getActualAPFacultyValues(logFileWriter);
		expectedValues = getUpdatedExpectedAPFacultyValues(expectedValues, actualValues, logFileWriter);
		verifyAPValues (expectedValues, actualValues, logFileWriter);
		logFileWriter.close();
		TestFileController.writeTestResults(testEnv, testCaseNumber, testName, testDescription, verificationErrors);
	}// end test method

	
	protected String[][] getInitialExpectedAPFacultyValues(BufferedWriter logFileWriter) throws Exception{
		String message = "method getExpectedAPFacultyValues called";
		//System.out.println(message);
		TestFileController.writeToLogFile(message, logFileWriter);
		String[][] namesAndDepts = HandSOnTestSuite.apFacultyValues.getUniqueFacultyNameDepartmentCombinations(logFileWriter);
		int numberOfRows = namesAndDepts[0].length;
		String[][] expectedValues = new String[numberOfColumns][numberOfRows];
		for(int row=0; row<numberOfRows; row++){
			expectedValues[FacultyName][row] = namesAndDepts[FacultyName][row];
			expectedValues[Department][row] = namesAndDepts[Department][row];
			expectedValues[Description][row] = HandSOnTestSuite.apFacultyValues.getAPReferenceValueForFacultyName(expectedValues[FacultyName][row]);
			expectedValues[DOShare][row] = 
					String.valueOf((int)
							(HandSOnTestSuite.apFacultyValues.getPrimitiveDoubleDeptFTEForFacultyNameAndDept(expectedValues[FacultyName][row], expectedValues[Department][row]) 
							* HandSOnTestSuite.referenceValues.getFloatAmountForAPType(expectedValues[Description][row]))
							);
		}// end for loop - different faculty-dept combos and their corresponding values
		return expectedValues;
	}//end getExpectedAPFacultyValues method
	
	protected String[][] getActualAPFacultyValues(BufferedWriter logFileWriter) throws Exception{
		TestFileController.writeToLogFile("method getActualAPFacultyValues is executing", logFileWriter);
		String[][] namesAndDepts = HandSOnTestSuite.apFacultyValues.getUniqueFacultyNameDepartmentCombinations(logFileWriter);
		int numberOfRows = namesAndDepts[FacultyName].length;
		String[] columnNames = {"Name", "Department", HandSOnTestSuite.FY + " Description", "DO Share", 
				"Market Pool Dept Share", HandSOnTestSuite.FY + " Raise $", HandSOnTestSuite.FY + " Raise Amt", "Final FY17 FTE Adjusted Raise $"};
		String[][] actualAPFacultyValues = new String[numberOfColumns][numberOfRows];
		for (int row=0; row<numberOfRows; row++){
			String name = namesAndDepts[FacultyName][row];
			String department = namesAndDepts[Department][row];
			HashMap<String, String> searchCriteria = new HashMap<String, String>();
			searchCriteria.put("Department", department);
			searchCriteria.put("Name", name);
			//System.out.println("Search Criteria for " + worksheetName +" is as follows "+searchCriteria.toString());
			LinkedList<HashMap<String, String>> rawValues 
				= myExcelReader.getMultipleValuesInRows(worksheetName, searchCriteria, columnNames, logFileWriter);
			String message = "Number of rows in " + worksheetName + " that match search criteria: " + rawValues.size();
			//System.out.println(message);
			TestFileController.writeToLogFile(message, logFileWriter);
			message = "\n(row " + row +") ";
			logFileWriter.write(message);
			//System.out.println(message);
			for (int column=0; column<numberOfColumns; column++){
				if (rawValues.size() == 0){
					verificationErrors.append("*** TEST FAILED *** search in the file for name '" + name +"' yielded no results");
					//System.out.println(verificationErrors.toString());
					TestFileController.writeToLogFile(verificationErrors.toString(), logFileWriter);
					logFileWriter.close();
					TestFileController.writeTestResults(testEnv, testCaseNumber, testDescription, testName, verificationErrors);
					fail(verificationErrors.toString());
				}
				if (rawValues.size() != 1){
					String warning = "*** WARNING *** There is more than one row with this search criteria - please adjust search criteria";
					//System.out.println(warning);
					TestFileController.writeToLogFile(warning, logFileWriter);
				}
				//we're going to assume that there's always one and only one HashMap returned in the LinkedList
				else{
					actualAPFacultyValues[column][row] = (String)rawValues.get(0).get(columnNames[column]);
					message = actualAPFacultyValues[column][row] + "; ";
					logFileWriter.write(message);
					//System.out.println(message);
				}
			}//end inner for - the LinkedList of values
		}//end for loop
		
		return actualAPFacultyValues;
	}//end getActualAPFacultyValues method 

	protected String[][] getUpdatedExpectedAPFacultyValues(String[][] expectedValues, String[][] actualValues, BufferedWriter logFileWriter) throws Exception{
		int numberOfRows = expectedValues[FacultyName].length;
		String message = "Expected contents of the files are as follows:";
		logFileWriter.write(message);
		//System.out.println(message);
		for (int row=0; row<numberOfRows; row++){
			message = "\n(row " + row + ") ";
			logFileWriter.write(message);
			//System.out.println(message);
			//this is the one place where expected values take their value from actual values
			expectedValues[MarketPoolDeptShare][row] = actualValues[MarketPoolDeptShare][row];
			message = "Market Pool Share of " + expectedValues[FacultyName][row] 
					+ " is " + expectedValues[MarketPoolDeptShare][row] + "\n";
			//System.out.print(message);
			logFileWriter.write(message);
			//now that we've got the Market Pool figure, just add it to the rest of the expected values
			for (int column=currentYearRaiseDollars; column<numberOfColumns; column++){
				expectedValues[column][row] = getSumAsString(expectedValues[DOShare][row], expectedValues[MarketPoolDeptShare][row]);
				message = expectedValues[column][row] + "; ";
				logFileWriter.write(message);
				//System.out.println(message);
			}//end inner for - columns
		}//end outer for - rows
		return expectedValues;
	}// end getUpdatedExpectedAPFacultyValues method
	
	private String getSumAsString(String first, String second){
		double firstNumber = 0;
		double secondNumber = 0;
		//System.out.println("method getSumAsString called");
		try{
			firstNumber = Double.valueOf((Long) NumberFormat.getNumberInstance(java.util.Locale.US).parse(first));
			//System.out.println("Value of first number is " + firstNumber);
		}
		catch(ParseException e){
			fail("Submitted unparseable number: " + first);
		}
		try{
			secondNumber = Double.valueOf((Long) NumberFormat.getNumberInstance(java.util.Locale.US).parse(second));
			//System.out.println("Value of second number is " + secondNumber);
		}
		catch(ParseException e){
			fail("Submitted unparseable number: " + second);
		}
		String sum = Double.toString(firstNumber + secondNumber);
		//System.out.println("method getSumAsString returning " + sum);
		return sum;
	}

	
	public void verifyAPValues (String[][] expectedValues, String[][] actualValues, BufferedWriter logFileWriter) throws Exception{
		verificationErrors = new StringBuffer();
		String message = "";
		logFileWriter.newLine();
		TestFileController.writeToLogFile("Method verifyAPValues executing", logFileWriter);
		message = "Expected vs Actual Value Comparisons are as follows: ";
		TestFileController.writeToLogFile(message, logFileWriter);
		//System.out.println(message);
		int numberOfRows = expectedValues[FacultyName].length;
		for (int row=0; row<numberOfRows; row++){
			for (int column=0; column<numberOfColumns; column++){
				message = "Expected: " + expectedValues[column][row] + " vs Actual: " + actualValues[column][row];
				logFileWriter.write(message);
				//System.out.print(message);
				if (column >= DOShare) {
					double expected = Double.valueOf((Long) NumberFormat.getNumberInstance(java.util.Locale.US).parse(expectedValues[column][row]));
					double actual = Double.valueOf((Long) NumberFormat.getNumberInstance(java.util.Locale.US).parse(actualValues[column][row]));
					//do float-to-float comparison
					logFileWriter.write(" (Number-to-number comparison invoked) - expected number is " + expected +", actual number is "+actual);
					//System.out.print(" (Number-to-number comparison invoked) - expected number is " + expected +", actual number is "+actual);
					if (expected == actual){
						//System.out.print(", match");
						logFileWriter.write(", match");
					}//end if - it matches
					else{
						verificationErrors.append("MISMATCH: " + message);
						logFileWriter.write(", MISMATCH");
						//System.out.print(", MISMATCH");
					}//end else - it doesn't match
				}//end if - number-to-number comparison
				else{
					logFileWriter.write(" (String-to-String comparison invoked)");
					//do String-to-String comparison
					if (expectedValues[column][row].equalsIgnoreCase(actualValues[column][row])){
						logFileWriter.write(", match");
					}//end if - it matches
					else{
						verificationErrors.append("MISMATCH: " + message);
						//System.out.print(", MISMATCH");
						logFileWriter.write(", MISMATCH");
					}//end else - it doesn't match
				}//end outer else - it's not the DO Share, thus requiring String-to-String comparison
				logFileWriter.newLine();
				//System.out.println();
			}//end inner for - columns
		}//end outer for - rows
	}//end verifyAPValues method
	
	private String[][] getMarketPoolFigures(BufferedWriter logFileWriter) throws Exception{
		TestFileController.writeToLogFile("method getActualAPFacultyValues is executing", logFileWriter);
		String[][] namesAndDepts = HandSOnTestSuite.apFacultyValues.getUniqueFacultyNameDepartmentCombinations(logFileWriter);
		int numberOfRows = namesAndDepts[0].length;
		
		String[][] marketPoolFigures = new String[3][numberOfRows];
		for(int row=0; row<numberOfRows; row++){
			marketPoolFigures[FacultyName][row] = namesAndDepts[FacultyName][row];
			marketPoolFigures[Department][row] = namesAndDepts[Department][row];
		}
		return marketPoolFigures;
	}


}
