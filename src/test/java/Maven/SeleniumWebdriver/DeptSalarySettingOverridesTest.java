package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
/*
 * There will be several steps involved with this.  
 * 
 * First, we find out what the departments are, then we download the Department SSW's and
 * then we will examine the SSW's and verify that the Department Overrides are present.
 * 
 */
public class DeptSalarySettingOverridesTest {

	protected static String testName;
	protected static String testCaseNumber;
	protected static String testDescription;
	protected static String testEnv;
	protected static StringBuffer verificationErrors;
	protected BufferedWriter logFileWriter;

	@Before
	public void setUp() throws Exception {
		testName = "DeptSalarySettingOverridesTest";
		testEnv = "Test";
		testCaseNumber = "054";
		testDescription = "Test of Overrides for Department SSWs";
	}//end setUp method

	@Test
	public void test() throws Exception{
		verificationErrors = new StringBuffer();
		String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		logFileWriter = TestFileController.getLogFileWriter(logFileName);
		String[] depts = HandSOnTestSuite.salarySettingMgrTabUI.getDepartments("Override", logFileWriter);
		logFileWriter.write("The following Salary Setting Worksheets will be examined: ");
		logFileWriter.newLine();
		for (int i=0; i<depts.length; i++){
			String fileName = "Department Salary Setting Worksheet "+ depts[i] + ".xlsm";
			logFileWriter.write(fileName);
			logFileWriter.newLine();
		}//end for loop
		
		//download the department SSW's
		logFileWriter.write("Now attempting to download these Department Salary Setting Worksheets:");
		logFileWriter.newLine();
		SalarySettingManager.switchWindowByURL(SalarySettingManager.salarySettingURL, logFileWriter);
		HandSOnTestSuite.salarySettingMgrTabUI.switchToReportTab();
		ReportRunner.downloadDeptSSWs(depts, ReportRunner.referenceWhichValues, logFileWriter);
		
		/*
		 * Now, we have to verify the contents of the SSW's.  For each of the departments that
		 * we have a worksheet for, we have to get a list of Professors who are overridden in 
		 * that department.  The list may only consist of one Professor.
		 * 
		 * Once we have that list, we have to verify that all members of that list appear at
		 * the bottom of the Salary Setting Worksheet.
		 */
		for (int i=0; i<depts.length; i++){
			String[] faculty = HandSOnTestSuite.salarySettingMgrTabUI.getFacultyForDepartment(depts[i], "Override", logFileWriter);
			for (int j=0; j<faculty.length; j++){
				logFileWriter.write("We will attempt to find Professor "+faculty[j]
						+", in the " + depts[i] +" Department");
				logFileWriter.newLine();
				boolean fileContainsFaculty = containsOverriddenFaculty(faculty[j], depts[i], logFileWriter);
				logFileWriter.write("File contains overridden faculty: " + fileContainsFaculty);
				logFileWriter.newLine();
				if (! fileContainsFaculty){
					String message = "File didn't contain faculty member "+faculty[j] 
							+", in department "+depts[i] +", as expected";
					logFileWriter.write(message);
					logFileWriter.newLine();
					verificationErrors.append(message);
				}
			}
		}//end for loop
	}//end test method
	

	private boolean containsOverriddenFaculty(String facultyName, String dept, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method containsOverriddenFaculty called with faculty name " + facultyName
				+", and Department "+dept + " ***");
		logFileWriter.newLine();
		String fileName = "Department Salary Setting Worksheet "+dept+".xlsm";
		SSExcelReader myReader = new SSExcelReader(TestFileController.directory +"/" +fileName, TestFileController.DepartmentSalarySettingWorksheetHeaderRow);
		int rowCount = myReader.getRowCount(TestFileController.DepartmentSalarySettingWorksheetName); 
		logFileWriter.write("Row count for instantiated reader for " + fileName +" is " + rowCount);
		logFileWriter.newLine();
		
		int rowForColNames = myReader.getCellRowNum(TestFileController.DepartmentSalarySettingWorksheetName, 0, SalarySettingManagementUIController.overrideSectionHeading);
		logFileWriter.write("Estimated location of overridden faculty at row " + rowForColNames);
		logFileWriter.newLine();

		int actualRowForColNames = myReader.getCellRowNum(TestFileController.DepartmentSalarySettingWorksheetName, 0, "Name", rowForColNames);
		logFileWriter.write("Actual header row for overridden faculty is " + actualRowForColNames);
		logFileWriter.newLine();
		
		boolean foundUnexpectedString = false;
		int rowNum = -1;
		if (actualRowForColNames != -1){
			myReader.setRowForColumnNames(rowForColNames);
			String[] colNames = {"Name", "Override", "Description"};
			for (int j=0; j<colNames.length; j++){
				if (j==0)
					rowNum = myReader.getCellRowNum(TestFileController.DepartmentSalarySettingWorksheetName, colNames[j], facultyName);
				String value = myReader.getCellData(TestFileController.DepartmentSalarySettingWorksheetName, colNames[j], rowNum);
				logFileWriter.write("Value for the "+ colNames[j] +" column is found: "+ value);
				logFileWriter.newLine();
				String attributeName = new String();
				if (colNames[j].equalsIgnoreCase("Override"))
					attributeName = "WhichWorksheet";
				else attributeName = colNames[j];
				String UIValue = new String();
				UIValue = HandSOnTestSuite.salarySettingMgrTabUI.getOverrideAttributeForNameAndDept(facultyName, dept, attributeName, logFileWriter);
				if ((value == null) || (UIValue == null) || (value.isEmpty()) || (UIValue.isEmpty()) ||(! UIValue.equalsIgnoreCase(value)))
					foundUnexpectedString = true;
			}//end for loop
		}//end if
		
		TestFileController.closeOneExcelReader(myReader);
		
		return ((rowForColNames != -1) && (actualRowForColNames != -1) && (rowNum != -1)
				&& (actualRowForColNames == rowForColNames + 1) && (! foundUnexpectedString));
		
	}//end containsOverriddenFaculty method

	@After
	public void tearDown() throws Exception {
		TestFileController.writeTestResults(testEnv, testCaseNumber, testName, testDescription, verificationErrors);
		logFileWriter.newLine();
		logFileWriter.close();
	}//end tearDown method


}//end DeptSalarySettingOverridesTest Class
