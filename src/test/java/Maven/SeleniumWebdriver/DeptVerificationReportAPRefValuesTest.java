package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.LinkedList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DeptVerificationReportAPRefValuesTest {
	
	public static final int FacultyName = 0;
	public static final int AppointmentType = 1;
	public static final int ApptPromoType = 2;
	public static final int finalAPValue = 3;
	
	public static final int numberOfColumns = 4;
	
	public static final String sheetName = "Department Verification Report";
	
	private StringBuffer verificationErrors;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	/*
	 * This is the method called by the JUnit framework when it's run.  
	 * The main purpose of this method is to call the other methods which actually do the work.
	 */
	@Test
	public void test() throws Exception{
		//System.out.println("Now executing the test method for class DeptVerificationReportAPRefValuesTest.");
		verificationErrors = new StringBuffer();
		String testName = "Verify AP Values in Dept Verification Report";
		//System.out.println("Now executing test: " + testName);
		String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		BufferedWriter logFileWriter = TestFileController.getLogFileWriter(logFileName);
		String[][] expectedValues = getExpectedValues(logFileWriter);
		String[][] actualValues = getActualValues(logFileWriter);
		verifyAPValues(expectedValues, actualValues, logFileWriter);
		logFileWriter.close();
		TestFileController.writeTestResults("Test", "009", "Comparison of Raises", "Department Verification Report Tests", verificationErrors);
	}//end test method
	
	/*
	 * This will involve looking up different Faculty (one or more of which pertains to more than
	 * one department and therefore appears on multiple lines).
	 */
	
	private String[][] getExpectedValues(BufferedWriter logFileWriter) throws Exception {
		String message = "method getExpectedValues has been called";
		//System.out.println(message);
		TestFileController.writeToLogFile(message, logFileWriter);
		String[][] namesAndDepts = HandSOnTestSuite.apFacultyValues.getUniqueFacultyNameDepartmentCombinations(logFileWriter);
		int numberOfRows = namesAndDepts[0].length;
		message = "size of names and departments array is " + namesAndDepts.length 
						+ " by " +numberOfRows;
		//System.out.println(message);
		TestFileController.writeToLogFile(message, logFileWriter);
		String[][] expectedValues = new String[numberOfColumns][numberOfRows];
		message = "Final Expected values to be returned are as follows: ";
		for (int row=0; row<numberOfRows; row++){
			expectedValues[FacultyName][row] = namesAndDepts[FacultyName][row];
			expectedValues[AppointmentType][row] = namesAndDepts[AppointmentType][row];
			String ApptPromoTypeString = HandSOnTestSuite.apFacultyValues.getAPReferenceValueForFacultyName(namesAndDepts[FacultyName][row]);
			expectedValues[ApptPromoType][row] = ApptPromoTypeString;
			float rawAPValue = HandSOnTestSuite.referenceValues.getFloatAmountForAPType(ApptPromoTypeString);
			expectedValues[finalAPValue][row] = Integer.toString((int)(rawAPValue));
			//now, rename the department value so that it's an Appointment Type - either "P" or "S"
			expectedValues[AppointmentType][row] 
					= getApptTypeForNameAndDept(namesAndDepts[FacultyName][row], expectedValues[AppointmentType][row]);
			message = message.concat("(row " + row + ") ");
			for(int column=0; column<numberOfColumns; column++){
				message = message.concat(expectedValues[column][row] + "; ");
			}
		}//end for loop - iterating through each row
		//System.out.println(message);
		TestFileController.writeToLogFile(message, logFileWriter);
		return expectedValues;
	}//end getExpectedValues method
	
	/*
	 * As there is no column corresponding to department for this particular worksheet, we will
	 * have to settle for the Appointment Type.  Currently, there is only one faculty member
	 * who appears on two lines - and this Faculty member has a Primary and a Secondary Appointment
	 * for each of the departments.  The good news is that this Faculty member has a 50% investment
	 * in each department, so it doesn't really matter which department gets the "S" value as a 
	 * Secondary Appointment.  All other values have the "P" value.
	 */
	private String getApptTypeForNameAndDept(String name, String dept){
		if (name.equalsIgnoreCase("Donaldson, David") && dept.equalsIgnoreCase("Economics")){
			return "S";
		}
		else return "P";
	}//end getApptTypeForNameAndDept method
	
	private String[][] getActualValues(BufferedWriter logFileWriter) throws Exception {
		String message = "method getActualValues has been called";
		//System.out.println(message);
		TestFileController.writeToLogFile(message, logFileWriter);
		String[][] namesAndDepts = HandSOnTestSuite.apFacultyValues.getUniqueFacultyNameDepartmentCombinations(logFileWriter);
		int numberOfRows = namesAndDepts[0].length;
		message = "size of names and departments array is " + namesAndDepts.length 
						+ " by " +numberOfRows;
		//System.out.println(message);
		TestFileController.writeToLogFile(message, logFileWriter);
		String[][] actualValues = new String[numberOfColumns][numberOfRows];
		String[] columnNames = {"Name", "Appt Type", HandSOnTestSuite.FY + " Description", HandSOnTestSuite.FY + " Commitments"};
		for (int row=0; row<numberOfRows; row++){
			String name = namesAndDepts[FacultyName][row];
			String dept = namesAndDepts[1][row];
			String description = HandSOnTestSuite.apFacultyValues.getAPReferenceValueForFacultyName(name);
			String apptType = getApptTypeForNameAndDept(name, dept);
			HashMap<String, String> searchCriteria = new HashMap<String, String>();
			searchCriteria.put(columnNames[FacultyName], name);
			searchCriteria.put(columnNames[AppointmentType], apptType);
			searchCriteria.put(columnNames[ApptPromoType], description);
			message = "Search Criteria for " + sheetName +" is as follows "+searchCriteria.toString();
			//System.out.println(message);
			TestFileController.writeToLogFile(message, logFileWriter);
			LinkedList<HashMap<String, String>> rawValues 
					= TestFileController.DepartmentVerificationReport.getMultipleValuesInRows(sheetName, searchCriteria, columnNames, logFileWriter);
			//System.out.println("Raw Values gathered from the " + sheetName + " tab are as follows: "+rawValues.toString());
			message = "Number of rows in " + sheetName + " that match search criteria: " + rawValues.size();
			//System.out.println(message);
			TestFileController.writeToLogFile(message, logFileWriter);
			for (int column=0; column<numberOfColumns; column++){
				if(rawValues.size() == 0){
					message = "TEST FAILED because the following expected values were not found in the file:"
							 + searchCriteria.toString();
					logFileWriter.close();
					fail(message);
				}
				if (rawValues.size() != 1){
					String warning = "*** WARNING *** There is more than one row with this search criteria - please adjust search criteria";
					//System.out.println(warning);
					TestFileController.writeToLogFile(warning, logFileWriter);
				}
				//we're going to assume that there's always one and only one HashMap returned in the LinkedList
				actualValues[column][row] = (String)rawValues.get(0).get(columnNames[column]);
			}//end inner for - the LinkedList of values
		}//end for loop
		message = "Actual values to be returned are as follows: ";
		for (int row=0;row<numberOfRows;row++){
			message = message.concat("(row "+row +"): ");
			for (int column=0;column<numberOfColumns;column++){
				message = message.concat(actualValues[column][row] +"; ");
			}
		}//end for loop - printing out the actual values to be returned
		//System.out.println(message);
		TestFileController.writeToLogFile(message, logFileWriter);
		
		return actualValues;
	}//end getActualValues method

	
	/*
	 * This method takes the two dimensional arrays of expected and actual values and compares them,
	 * making sure that they're the same.
	 */
	public void verifyAPValues(String[][] expectedValues, String[][] actualValues, BufferedWriter logFileWriter) throws Exception{
		verificationErrors = new StringBuffer();
		logFileWriter.newLine();
		TestFileController.writeToLogFile("Method verifyAPValues executing", logFileWriter);
		
		TestFileController.writeToLogFile("Expected vs Actual Value Comparisons are as follows: ", logFileWriter);
		//System.out.println("Expected vs Actual Value Comparisons are as follows: ");
		int numberOfRows = expectedValues[0].length;
		for (int j=0; j<numberOfRows; j++){
			for (int i=0; i<numberOfColumns; i++){
				String comparisonData = "Expected: " + expectedValues[i][j] + " vs Actual: " + actualValues[i][j];
				logFileWriter.write(comparisonData);
				//System.out.print(comparisonData);
				if (i == finalAPValue){
					double expected = Double.valueOf((Long) NumberFormat.getNumberInstance(java.util.Locale.US).parse(expectedValues[i][j]));
					double actual = Double.valueOf((Long) NumberFormat.getNumberInstance(java.util.Locale.US).parse(actualValues[i][j]));
					//do float-to-float comparison
					logFileWriter.write(" (Number-to-number comparison invoked) - expected number is " + expected +", actual number is "+actual);
					if (expected == actual){
						//System.out.print(", match");
						logFileWriter.write(", match");
					}//end if - it matches
					else{
						verificationErrors.append("MISMATCH: " + comparisonData);
						logFileWriter.write(", MISMATCH");
						//System.out.print(", MISMATCH");
					}//end else - it doesn't match
				}//end if - this involves a number-to-number comparison
				else{
					logFileWriter.write(" (String-to-String comparison invoked)");
					//do String-to-String comparison
					if (expectedValues[i][j].equalsIgnoreCase(actualValues[i][j])){
						logFileWriter.write(", match");
					}//end if - it matches
					else{
						verificationErrors.append("MISMATCH: " + comparisonData);
						//System.out.print(", MISMATCH");
						logFileWriter.write(", MISMATCH");
					}//end else - it doesn't match
				}//end outer else - it's not the FTE Adjusted DO Share Amount, thus requiring String-to-String comparison
				logFileWriter.newLine();
			}//end inner for loop - columns
		}//end outer for loop - rows

	}//end verifyAPValues method 

	
}
