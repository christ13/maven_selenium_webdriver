package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.TreeMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ControlSheetPivotStageReportASCTest extends
		ControlSheetPivotReportASCTest {
	

	/*
	 */
	@Before
	public void setUp() throws Exception {
		mySheetName = TestFileController.ControlSheetPivot_StageWorksheetName;
		testName = "ControlSheetPivotStageReportASCTest";
		testCaseNumber = "023";
		testDescription = "ASC Test of Control Sheet Pivot Stage Report";
		logFileName = "Control Sheet Pivot Stage ASC Test.txt";
		logFileWriter = TestFileController.getLogFileWriter(logDirectoryName  +"/" + logFileName);
		criteria = new TreeMap<String, String>();

		expectedStringAttributeNames = new String[6];
		actualStringAttributeNames = new String[6];
		expectedStringAttributeNames[0] = actualStringAttributeNames[0] = ASCFacultyValues.StringAttributeKeys[ASCFacultyValues.ClusterIndex];
		expectedStringAttributeNames[1] = actualStringAttributeNames[1] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.CategoryIndex];
		expectedStringAttributeNames[2] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.SubCategoryIndex];
		actualStringAttributeNames[2] = "Sub-Category";
		expectedStringAttributeNames[3] = actualStringAttributeNames[3] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.DepartmentIndex];
		expectedStringAttributeNames[4] = actualStringAttributeNames[4] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.FacultyIndex];
		//Description attribute has a bug - HANDSON-3269 - associated with it.  It's not copied from the ASC to the Control Sheet Detail Report.
		expectedStringAttributeNames[5] = actualStringAttributeNames[5] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.DescriptionIndex];
		
		expectedAmountAttributeNames = new String[0];
		actualAmountAttributeNames = new String[0];

		expectedSummaryCriteria = new TreeMap<String, ArrayList<String>>();
		ArrayList<String> list = new ArrayList<String>();
		list.add(ExpectedASCData.StringAttributeKeys[ExpectedASCData.FacultyIndex]);
		list.add(ExpectedASCData.StringAttributeKeys[ExpectedASCData.SubCategoryIndex]);
		list.add(ExpectedASCData.StringAttributeKeys[ExpectedASCData.StageIndex]);
		expectedSummaryCriteria.put("FTE Adjusted DO Share Amount", list);
		
		pivotCriteriaName = ExpectedASCData.StringAttributeKeys[ExpectedASCData.StageIndex];
		outputMessage("attribute name being used as pivot criteria is "+pivotCriteriaName + "\n", logFileWriter);
		pivotCriteriaValues = HandSOnTestSuite.expectedASCDataSet.getAllDiscreteStringAttributeValues(pivotCriteriaName, logFileWriter);

		searchDelimiters = new ArrayList<String[]>(pivotCriteriaValues.length);
		for (int i=0; i<pivotCriteriaValues.length; i++){
			String[] criterion = {pivotCriteriaName, pivotCriteriaValues[i]};
			searchDelimiters.add(criterion);
		}//end for loop

		outputMessage(pivotCriteriaName + " values being used as delimiters are as follows: ", logFileWriter);
		for (int i=0; i<searchDelimiters.size(); i++){
			outputMessage(searchDelimiters.get(i)[1] +", ", logFileWriter);
		}//end for loop
		outputMessage("\n", logFileWriter);

		super.setUp();

	}//end setUp method

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}//end tearDown method

	/*
	 * (non-Javadoc)
	 * @see Maven.SeleniumWebdriver.ControlSheetPivotReportASCTest#test()
	 * The only criterion being submitted here is the Stage.
	 */
	@Test
	public void test() throws Exception{
		super.test();
	}//end test method

}
