package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.NumberFormat;
import java.util.Locale;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ProvostBinderListBySalaryDBVerificationTest extends
		ProvostBinderListDBVerificationTest {

	
	private static final String[] sheetColumns = {"Department", "Appt Type", "Name", "Gender", 
		"Ethnicity", "15-16 Final Salary", "16-17 Salary"};

	@Before
	public void setUp() throws Exception {
		myExcelReader = TestFileController.ProvostBinderListBySalary;
		testName = "ProvostBinderListBySalaryDBTest";
		testEnv = "Test";
		testCaseNumber = "051";
		testDescription = "DB Test of Provost Binder List By Salary";
		firstDataRow = 6;
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void test() throws Exception{
		verificationErrors = new StringBuffer();
		String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		BufferedWriter logFileWriter = TestFileController.getLogFileWriter(logFileName);
		logFileWriter.write("Now executing "+ testName);
		logFileWriter.newLine();
		
		myConnection = getConnection (logFileWriter);
		runStoredProcedure (myConnection, logFileWriter);
		String[] worksheets = TestFileController.ProvostBinderListBySalaryWorksheetNames;
		verifyValuesByWorksheet(myConnection, worksheets, logFileWriter);

		logFileWriter.close();
	}//end test method
	
	/*
	 * We need to get the current year salary and order the list by the current year
	 * salary.  Best way to do that is take the Previous Year salary, adjust it according 
	 * to what is in the "$" column under the "16-17 Total Raise", and that gives us the 
	 * final current year salary to order everything by.
	 * 
	 */
	
	private void verifyValuesByWorksheet(Connection conn, String[] worksheets, 
			BufferedWriter logFileWriter) throws Exception{
	Statement stmt = null;
	ResultSet rs=null;
	try {
        stmt=conn.createStatement();
        String query = "SELECT p1.* FROM PROVOST_BINDER p1, "
        		+"(SELECT MAX(p2.APPT_SSW_PRIORITY) AS MAX_PRIORITY, p2.NAME FROM PROVOST_BINDER p2 GROUP BY p2.NAME) p3 "
        		+"WHERE p1.NAME = p3.NAME AND p1.APPT_SSW_PRIORITY = p3.MAX_PRIORITY "
        		+"ORDER BY p1.ROOT_CLUSTER_SHORT, p1.RANK_SORT_ORDER DESC, p1.CY_100PCTFTE_SALARY DESC, p1.NAME";
        rs=stmt.executeQuery(query);
        ResultSetMetaData rsmd = rs.getMetaData();
        int columnCount = rsmd.getColumnCount();
        logFileWriter.write("Number of columns derived from the database is " + columnCount);
        logFileWriter.newLine();
        logFileWriter.write("Column names: types from the database are as follows: ");
        for (int i=1; i<=columnCount; i++){
        	logFileWriter.write(rsmd.getColumnName(i) + ": " + rsmd.getColumnTypeName(i));
        	if (i<columnCount) logFileWriter.write(", ");
        }//end for loop
        logFileWriter.newLine();

        logFileWriter.write("Column names from the worksheet are as follows: ");
        for (int i=0; i<sheetColumns.length; i++){
        	logFileWriter.write(sheetColumns[i]);
        	if (i<(sheetColumns.length - 1)) logFileWriter.write(", ");
        }//end for loop
        logFileWriter.newLine();
        logFileWriter.newLine();
        
        int worksheetIndex = 0;
        int sheetRow = firstDataRow;
        while (rs.next()){
        	String worksheetNameFromDB = rs.getString("ROOT_CLUSTER_SHORT") + rs.getString("RANK_SHORT");
        	String NameOnCurrentWorksheetRow = myExcelReader.getCellData(worksheets[worksheetIndex], "Name", sheetRow);
        	logFileWriter.write("Worksheet Name (by data in the DB) "+worksheetNameFromDB);
        	logFileWriter.write(", Name in the worksheet (by worksheet): " + NameOnCurrentWorksheetRow);
        	logFileWriter.newLine();
        	if (! worksheetNameFromDB.equalsIgnoreCase(worksheets[worksheetIndex])){
        		if ((NameOnCurrentWorksheetRow == null) ||(NameOnCurrentWorksheetRow.isEmpty())){
        			logFileWriter.write("Reached the end of the worksheet for Department '"
        					+ worksheets[worksheetIndex] +"', going to the next worksheet");
        			logFileWriter.newLine();
        			if (verificationErrors.length() == 0){
        				logFileWriter.write("No mismatches found for department " + worksheets[worksheetIndex]);
        			}//end if - no errors so far
        			else{
        				logFileWriter.write("MISMATCHES ARE FOUND for department " + worksheets[worksheetIndex]);
        			}//end else - there have been errors found
        			logFileWriter.newLine();
        			worksheetIndex++;
        			sheetRow = firstDataRow;
        		}//end if - we're at the end of the worksheet & going to the next worksheet
        		else{
        			String message = "ERROR: There is one additional faculty in the row,"
        					+" but not in the DB: " + NameOnCurrentWorksheetRow
        					+"(on the worksheet), for Worksheet '" + worksheets[worksheetIndex]
        					+"', Worksheet according to the DB is '" + worksheetNameFromDB
        					+"', and according to the DB, the faculty member is '"
        					+ rs.getString("NAME") +"'";
        			logFileWriter.newLine();
        			logFileWriter.write(message);
        			verificationErrors.append(message);
        			return;
        		}//end else - there is a mismatch - we've found a faculty member in the department worksheet but not in the DB
        	}//end if - there's a mismatch on the department
        	for (int i=0; i<sheetColumns.length; i++){
        		String actual = myExcelReader.getCellData(worksheets[worksheetIndex], sheetColumns[i], sheetRow);
        		String expected = getExpectedValue(rs, i);
        		if (! actual.equalsIgnoreCase(expected)){
        			String message = "'" + expected + "' vs '" + actual + "' (MISMATCH), ";
        			logFileWriter.write(message);
	        		logFileWriter.newLine();
	        		if (verificationErrors.length()==0)
	        			verificationErrors.append("FAILURE due to mismatch - first mismatch is "+ message);
        		}//end if - there's a mismatch - don't output anything if everything matches
        	}//end for
        	sheetRow++;
        }//end while loop

	}//end try clause
    catch (SQLException e) {
        StackTraceElement[] stackTrace = e.getStackTrace();
        for (int i=0; i<stackTrace.length; i++){
        	logFileWriter.write(stackTrace[i].toString());
        	logFileWriter.newLine();
        }//end for loop
    }//end catch clause
	finally{
		if (stmt != null) stmt.close();
	}//end finally clause
	}//end verifyValuesByWorksheet method

	
	private String getExpectedValue(ResultSet rs, int sheetColumnIndex) throws Exception{
		String expectedValue = new String();
		switch (sheetColumnIndex){
		case 0: expectedValue = rs.getString("ORG_NM"); break;
		case 1: expectedValue = rs.getString("APPT_TYPE");break;
		case 2: expectedValue = rs.getString("NAME");break;
		case 3: expectedValue = rs.getString("GENDER");break;
		case 4: expectedValue = rs.getString("ETHNICITY_NAME");break;
		case 5:	NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
				int py_salary = rs.getInt("FINAL_PY_SALARY");
				expectedValue = numberFormat.format(py_salary);
				break;
		case 6:	numberFormat = NumberFormat.getNumberInstance(Locale.US);
				int cy_salary = rs.getInt("CY_100PCTFTE_SALARY");
				expectedValue = numberFormat.format(cy_salary);
				break;
		}//end switch block
		if (expectedValue == null) expectedValue = " ";
		return expectedValue;
	}//end getExpectedValue method


}
