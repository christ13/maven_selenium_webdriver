package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.remote.DesiredCapabilities;
//import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

public class UIController {
	
	public static WebDriver driver = initializeDriver(TestFileController.LogDirectoryName);
	public static final String startPage = "https://handsonweb-"+HandSOnTestSuite.machineName+".stanford.edu/"+HandSOnTestSuite.envName+"/showDocumentsTab.do";
	public static final String queriesPageURL = "showQueriesTab.do";
	public static final String deansReportURL = "showDeanReportRequest.do";
	public static final String departmentCommitmentReportsURL = "showDeptCommitmentsReports.do?ACTION=zips";
	public static final String summerNinthsURL = "showDeptCommitmentsReports.do?ACTION=summer9th";
	public static final String commitmentReportsURL = "processDeptCommitmentsReportsRequest.do";
	public static final String administrationURL = "administrationTab.jsp";
	public static final String manageDefaultFacultyPTAURL = "processManageFacultyPTA.do";
	public static final String manageStdCostsURL = "processManageStandardCost.do?";
	public static final String salarySettingURL = "https://handsonweb-"+HandSOnTestSuite.machineName+".stanford.edu/"+HandSOnTestSuite.envName+"/processSalarySetting.do";
	public static final String schoolReportDialogURL = "dto.level=school&dto.rptType=schoolSalSetWksh";
	public static final String commitmentSearchURL = "showDocumentSearch.do";
	public static final String clusterReportDialogURL = "dto.level=cluster&dto.rptType=clusterSalSetWksh";
	public static final String chairReportDialogURL = "processSalSetWorksheetRequest.do?ACTION=goToWorkSheet&dto.level=deptchair&dto.rptType=deptChairSalSetWksh";
	public static final String deptReportDialogURL = "processSalSetWorksheetRequest.do?ACTION=goToWorkSheet&dto.dept=";
	public static final String fiscalYearDialogURL = "processSalarySetting.do?ACTION=getAvailableFiscalYearDescending";
	public static final String loadSalarySettingWindowURL = "processSalarySetting.do";
	public static final String facultyRecordWindowURL = "processManageFaculty.do?ACTION=load&facultyDTO.unvlId=";
	public static final String newFacultyURL = "processManageFaculty.do?ACTION=createFromRecruitment&recruitmentId=";
	public static final String existingFacultyURL = "showPersonReference.do?personUnvlId=";
	public static final String showRetentionWindowURL = "showRetention.do";
	public static final String showRetentionLookupWindowURL = "showRetentionLookup.do";
	public static final String annotationPopupWindowURL = "annotationPopup.jsp";
	public static final String commitmentWindowURL = "showFundingCommitment.do";
	public static final String uploadFileWindowURL = "salarySetting/workSheetUpload.jsp";
	public static final String notificationDialogURL = "notification.jsp";
	public static final String fileUploadResultWindowURL = "processSalSetWorksheetRequest.do?ACTION=upload";
	public static final String newFundingCommitmentURL = "showFundingCommitment.do";
	public static final String newSearchAuthorizationURL = "showSearchAuthAction.do";
	public static final String existingSearchAuthorizationURL = "processSearchAuthAction.do";
	public static final String searchAuthorizationLookupURL = "showSearchAuthLookup.do";
	public static final String searchAuthorizationAttachmentDialogURL = "offerAttachment.jsp";
	public static final String newRecruitmentURL = "showRecruitment.do";
	public static final String recruitmentLookupURL = "showRecruitmentLookup.do";
	public static final String updateAppointmentURL = "updateApptConfirm.jsp";
	public static final String currentFiscalYear = "2016-17";
	public static final String SalarySettingManagementTabLocatorID = "salSetManage_a";
	public static final String SalarySettingTabLocatorID = "salarySetting_a";
	public static final String ReportTabLocatorID = "report_a";
	public static final String ControlSheetLocatorID = "control_a";
	public static final String FundingCommitmentAttachmentDialogURL = "fundingCommitmentAttachment.jsp";
	public static final String FundingCommitmentSummaryURL = "showFundingCommitmentSummary.do?";
	public static final String FundingCommitmentHistoryURL = "processDocumentHistoryReportRequest.do?docNbr=";
	public static final String FundingCommitmentSnapshotURL = "showDocumentSnapshotReportRequest.do?docNbr=";
	public static final String FundingCommitmentFYIRouteURL = "processFundingCommitmentFYI.do?";
	public static final String FundingCommitmentSearchExportURL = "processCommitmentSearch.do?ACTION=openExportPage";
	public static final String matchedPersonDialogURL = "showMatchedPersons.do?ACTION";
	public static final String facultySearchPageURL = "processManageFaculty.do?ACTION=showFacultySearch";
	
	//These all apply to the Salary Budgeting Reports
	public static final String SalaryBudgetingPopulationVerificationReportURL = "processSalaryBudgetingReportRequest.do?rptType=salaryBudgetPopVeriReport";
	public static final String SalaryBudgetingReportURL = "processSalaryBudgetingReportRequest.do?rptType=salaryBudgetReport&ACTION=showSalayForm";
	public static final String SalaryBudgetingComparisonReportURL = "processSalaryBudgetingReportRequest.do?rptType=salaryBudgetCompReport&ACTION=showSalayForm";
	public static final String SalaryRateExportForHyperionUploadURL = "salaryBudgetingCsvForm.jsp?rptType=RATE_EXPORT";
	public static final String SalaryPercentageExportForHyperionUploadURL = "salaryBudgetingCsvForm.jsp?rptType=PERCENTAGE_EXPORT";
	public static final String SalaryReserveDetailReportURL = "salaryBudgetingCsvForm.jsp?rptType=REVERSE_EXPORT";
	public static final String SalaryReserveRollUpExportForHyperionUploadURL = "salaryBudgetingCsvForm.jsp?rptType=REVERSE_ROLL_UP_EXPORT";
	
	  private static boolean isElementClickable (By by){
		  try{
				  driver.findElement(by).click();
		  }
		  catch(WebDriverException e){
				  return false;
		  }
		  return true;
	  }
	  
	  public static WebDriver initializeDriver(String logDirectoryName) {
		  String browserName = System.getenv("browserName");
		  System.out.println();
		  System.out.println("***** BROWSER TO BE USED IS "+browserName+" *****");
		  System.out.println();
		  if (browserName.equalsIgnoreCase("FIREFOX"))
			  return initializeFirefoxDriver(logDirectoryName);
		  else 
			  return initializeChromeDriver(logDirectoryName);
	  }//end initializeDriver method
	  
	  private static FirefoxDriver initializeFirefoxDriver (String logDirectoryName) {
		  System.out.println("*** method initializeFirefoxDriver called with log directory name "+logDirectoryName+" ***");
		  FirefoxDriver driver = null;
			/*
			FirefoxOptions options = new FirefoxOptions();
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("profile.default_content_settings.popups", 0);
			prefs.put("download.default_directory", "C:\\Users\\christ13\\Downloads/");
			prefs.put("pdfjs.disabled", true);
			prefs.put("download.prompt_for_download", false);
			prefs.put("download.directory_upgrade", true);
			prefs.put("plugins.always_open_pdf_externally", true);
			*/
			try {
				System.setProperty("webdriver.gecko.driver","C:\\Users\\christ13\\webdrivers\\geckodriver-v0.24.0-win64\\geckodriver.exe");
				//"C:\Users\christ13\webdrivers\geckodriver-v0.24.0-win64\geckodriver.exe"
			}
			catch (Exception e) {
				System.out.println(e);
			}
			ProfilesIni test_profileini = new ProfilesIni();
			FirefoxProfile profile = test_profileini.getProfile("TEST_USER");
			try {
				//profile.setPreference("browser.download.manager.showWhenStarting", false);
/*
				profile.setPreference("browser.download.forbid_open_with", true);
				profile.setPreference("browser.download.panel.shown", true);
				profile.setPreference("browser.download.forbid_open_with", true);
				profile.setPreference("pdfjs.disabled", false);
				profile.setPreference("pdfjs.enabledCache.state", false);
				profile.setPreference("pdfjs.previousHandler.preferredAction", 4);
				profile.setPreference("pdfjs.migrationVersion", 2);
				profile.setPreference("browser.launcherProcess.enabled", true);
				profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.ms-excel,application/csv,application/pdf");

				profile.setPreference("browser.safebrowsing.downloads.remote.block_dangerous", false);
				profile.setPreference("browser.safebrowsing.downloads.remote.block_dangerous_host", false);
				profile.setPreference("browser.safebrowsing.downloads.remote.block_potentially_unwanted", false);
				profile.setPreference("browser.safebrowsing.downloads.remote.block_uncommon", false);
				
				profile.setPreference("browser.tabs.loadInBackground", false);
				profile.setPreference("pdfjs.enabledCache.state", false);
				profile.setPreference("pdfjs.migrationVersion", 2);
				profile.setPreference("pdfjs.previousHandler.preferredAction", 4);
				profile.setPreference("plugin.disable_full_page_plugin_for_types", "application/pdf");
				profile.setPreference("browser.contentblocking.category", "standard");
				profile.setPreference("browser.contentblocking.introCount", 5);
				
				profile.setPreference("pref.downloads.disable_button.edit_actions", false);
				profile.setPreference("privacy.popups.showBrowserMessage", false);
				profile.setPreference("privacy.popups.usecustom", false);
				
				profile.setPreference("security.block_ftp_subresources", false);
				profile.setPreference("security.block_importScripts_with_wrong_mime", false);
				profile.setPreference("security.block_script_with_wrong_mime", false);
				profile.setPreference("security.default_personal_cert", "Never ask");
				
				profile.setPreference("signon.importedFromSqlite", true);
				profile.setPreference("toolkit.telemetry.reportingpolicy.firstRun", false);
				profile.setPreference("ui.popup.disable.autohide", true);
*/
				
/*
				profile.setPreference("browser.download.folderList",2);  
				profile.setPreference("browser.download.manager.showWhenStarting", false); 
				//profile.setPreference("browser.helperApps.neverAsk.openFile","application/vnd.ms-excel");

				profile.setPreference("browser.helperApps.neverAsk.saveToDisk","application/vnd.ms-excel");
				profile.setPreference("browser.download.dir", "C:\\Users\\christ13\\Downloads");

				profile.setPreference("browser.helperApps.alwaysAsk.force", false);

				profile.setPreference("browser.download.manager.showAlertOnComplete", false); 
				profile.setPreference("browser.download.manager.closeWhenDone", false);
*/
				
				//WebDriver driver = new FirefoxDriver(profile);				
				
				//profile.setPreference("profile.default_content_settings.popups", 0);
				//profile.setPreference("download.prompt_for_download", false);
				//profile.setPreference("download.directory_upgrade", true);
				//profile.setPreference("plugins.always_open_pdf_externally", false);
				/*
					+"",application/vnd.ms-excel.sheet.binary.macroenabled.12,"
					+"application/vnd.ms-excel.template.macroenabled.12, application/vnd.ms-excel.sheet.macroenabled.12,"
					+"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,"
					+"application/vnd.openxmlformats-officedocument.wordprocessingml.document,text/csv, application/pdf,"
					+"application/octet-stream,application/x-winzip,application/x-pdf,application/x-gzip");

				*/

				System.out.println("Browser Profile is initialized");
				FirefoxOptions options = new FirefoxOptions();
				options.setCapability("marionette",true);
				options.setCapability(FirefoxDriver.PROFILE, profile);
				System.out.println("Driver Options are initialized");
				System.out.println("Now trying to instantiate the Firefox driver");
				driver = new FirefoxDriver(options);
				System.out.println("Successfully instantiated the new Firefox driver with options");
				driver.get("https://www.google.com/");

				return driver;
			}
			catch(Exception e) {
				System.out.println(e);
			}
			/*
			options.addPreference("profile.default_content_settings.popups", 0);
			options.addPreference("download.default_directory", "C:\\Users\\christ13\\Downloads/");
			options.addPreference("pdfjs.disabled", true);
			options.addPreference("download.prompt_for_download", false);
			options.addPreference("download.directory_upgrade", true);
			options.addPreference("plugins.always_open_pdf_externally", true);


			options.addArguments("--test-type");

			DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			capabilities.setCapability(FirefoxOptions.FIREFOX_OPTIONS, options);
			 */			
			System.out.println("returning null driver");
			return driver;
	  }//end initializeFirefoxDriver method
	  
	  private static ChromeDriver initializeChromeDriver(String logDirectoryName){
			ChromeOptions options = new ChromeOptions();
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("profile.default_content_settings.popups", 0);
		   	prefs.put("permissions.default.desktop-notification", 1);
			prefs.put("download.default_directory", "C:\\Users\\christ13\\Downloads/");
			prefs.put("pdfjs.disabled", true);
			prefs.put("download.prompt_for_download", false);
			prefs.put("download.directory_upgrade", true);
			prefs.put("plugins.always_open_pdf_externally", true);
			options.setExperimentalOption("prefs", prefs);

			options.addArguments("--test-type");
			//DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			//capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			return new ChromeDriver(options);
	  }//end initializeChromeDriver method
	  
		public String[] getLocatorNamesBelowLocator(HSSFExcelReader reader, String worksheetName, String belowLocatorName, BufferedWriter logFileWriter) throws Exception{
			logFileWriter.write("*** method getLocatorNamesBelowLocator called, to get all locator names below "+belowLocatorName+" ***");
			logFileWriter.newLine();
			ArrayList<String> names = new ArrayList<String>();
			int firstRow = reader.getCellRowNum(worksheetName, "Name", belowLocatorName)+1;
			int lastRow = reader.getRowCount(worksheetName);
			for (int i=firstRow; i<=lastRow; i++){
				names.add(reader.getCellData(worksheetName, "Name", i));
			}//end for loop - filling up the names ArrayList
			String[] locatorNameArray = new String[names.size()];
			for(int i=0; i<names.size(); i++){
				locatorNameArray[i] = names.get(i);
				logFileWriter.write("Adding locator name " + locatorNameArray[i]+" to the array to be returned");
				logFileWriter.newLine();
			}//end for - constructing the locator name array to be returned
			return locatorNameArray;
		}//end getLocatorNamesBelowLocator method
		
		public boolean costsMatch(HashMap<String, Integer> expectedCosts, HashMap<String, Integer> actualCosts, BufferedWriter logFileWriter) throws Exception{
			logFileWriter.newLine();
			logFileWriter.write("*** method costsMatch called ***");
			logFileWriter.newLine();
			
			boolean match = true;
			boolean sizesMatch = true;
			outputCosts(expectedCosts, "Expected Costs", logFileWriter);
			outputCosts(actualCosts, "Actual Costs", logFileWriter);
			logFileWriter.newLine();
			logFileWriter.newLine();
			
			if (expectedCosts.size() != actualCosts.size()){
				logFileWriter.write("WARNING!! There is a mismatch in size between the expected and actual costs");
				logFileWriter.newLine();
				logFileWriter.newLine();
				match = false;
				sizesMatch = false;
			}//end if
			
			Iterator iteratorExpected = expectedCosts.entrySet().iterator();
			//first, check to see that all expected values are there and correct
			while(iteratorExpected.hasNext()){
				Map.Entry pair = (Map.Entry)iteratorExpected.next();
				String key = (String)pair.getKey();
				int expectedValue = expectedCosts.get(key).intValue();
				//now, verify that the actual costs match
				Integer actualMapValue = actualCosts.get(key);
				int actualValue = 0;
				if (actualMapValue != null)
					actualValue = actualMapValue.intValue();
				logFileWriter.write("Cost Type "+key+", with expected value "+expectedValue
						+", and actual value "+actualValue);
				if (expectedValue != actualValue){
					match = false;
					logFileWriter.write(", MISMATCH");
				}//end if
				else logFileWriter.write(", and they match");
				logFileWriter.newLine();
			}//end while - checking expected Costs are all present and equal
			if (! sizesMatch){
				Iterator iteratorActual = actualCosts.entrySet().iterator();
				//first, check to see that all expected values are there and correct
				while(iteratorActual.hasNext()){
					Map.Entry pair = (Map.Entry)iteratorActual.next();
					String key = (String)pair.getKey();
					int actualValue = actualCosts.get(key).intValue();
					//now, verify that the actual costs match
					Integer expectedMapValue = expectedCosts.get(key);
					int expectedValue = 0;
					if (expectedMapValue != null)
						expectedValue = expectedMapValue.intValue();
					else{
						logFileWriter.write("ERROR: UNEXPECTED COST is present "
								+ "- Cost Type "+key+", with value "+actualValue);
						logFileWriter.write(", MISMATCH");
						logFileWriter.newLine();
					}//end else
				}//end while				
			}//end if
			
			return match;
		}//end costsMatch method

		
		public void outputCosts(HashMap<String, Integer> expectedCosts, String costGroupName, 
				BufferedWriter logFileWriter) throws Exception{
			logFileWriter.newLine();
			logFileWriter.write("* There are " + expectedCosts.size() + " " 
					+ costGroupName + ", and they are as follows: *");
			logFileWriter.newLine();
			Iterator iterator = expectedCosts.entrySet().iterator();
			while(iterator.hasNext()){
				Map.Entry pair = (Map.Entry)iterator.next();
				String key = (String)pair.getKey();
				Integer value = expectedCosts.get(key);
				logFileWriter.write("Cost Type "+key+", with value "+value.intValue());
				logFileWriter.newLine();
			}//end while			
		}//end outputCosts method
		
		public static boolean isElementAccessible(By by, BufferedWriter logFileWriter) throws Exception{
			return isElementAccessible(by, "", logFileWriter);
		}
		
		public static boolean isElementAccessible(By by, String myWorksheetName, BufferedWriter logFileWriter) throws Exception{
			boolean isAccessible = true;
			if (isAlertPresent()){
				driver.switchTo().alert().accept();
				System.out.println("There was an alert present");
			}//end if
			else{
				System.out.println("There was no alert present");
				isAccessible = false;
			}
			
			String currentURL = driver.getCurrentUrl();
			logFileWriter.write("The current URL is "+currentURL);
			logFileWriter.newLine();
			
			
			if (isElementPresent(by)){
				logFileWriter.write("Element is present");
				logFileWriter.newLine();
				Thread.sleep(2000);
				scrollElementIntoView(by, logFileWriter);
				Thread.sleep(2000);
			}//end if - element is present
			else{
				logFileWriter.write("Element is not present");
				logFileWriter.newLine();
				isAccessible = false;
			}//end else - element is not present
			WebElement insertButton = UIController.driver.findElement(by);
			if (insertButton.isDisplayed()){
				logFileWriter.write("Element is displayed");
				logFileWriter.newLine();
				Thread.sleep(2000);
				UIController.scrollElementIntoView(by, logFileWriter);
				Thread.sleep(2000);
			}//end if - element is present
			else{
				logFileWriter.write("Element is not displayed");
				logFileWriter.newLine();
				isAccessible = false;
			}//end else - element is not present
			if (insertButton.isEnabled()){
				logFileWriter.write("Element is enabled");
				logFileWriter.newLine();
				Thread.sleep(2000);
				UIController.scrollElementIntoView(by, logFileWriter);
				Thread.sleep(2000);
			}//end if - element is present
			else{
				logFileWriter.write("Element is not enabled");
				logFileWriter.newLine();
				isAccessible = false;
			}//end else - element is not present
	
			return isAccessible;
		}//end isElementAccessible method
		
		public static void scrollElementIntoView(By by, BufferedWriter logFileWriter) throws Exception{
			WebElement element = null;
			try{
				element = UIController.driver.findElement(by);
				((JavascriptExecutor) UIController.driver).executeScript("arguments[0].scrollIntoView(true);", element);
				Thread.sleep(500);
			}
			catch(Exception e){
				logFileWriter.newLine();
				logFileWriter.write("Method scrollTextIntoView couldn't locate element with By "+by.toString());
				logFileWriter.newLine();
				e.printStackTrace();
			}//end catch
			if ((element == null) || (element.getText().length() == 0)){
				logFileWriter.newLine();
				logFileWriter.write("Method scrollTextIntoView couldn't locate text with By "+by.toString());
				logFileWriter.newLine();
			}//end if
		}//end method scrollTextIntoView
		
		public String getSelectedTextFromLocator (String rawLocator, String name, BufferedWriter logFileWriter) throws Exception{
			logFileWriter.write("*** method getSelectedText called with name " + name +" ***");
			logFileWriter.newLine();
			By by = getByForLocator(rawLocator, logFileWriter);
			return getSelectedTextForBy(by, name, logFileWriter);
		}//end getSelectedTextFromLocator method

		public static String getSelectedTextForBy(By by, String name, BufferedWriter logFileWriter) throws Exception{
			waitForElementPresent(by);
			Select selector = null;
			try{
				selector = new Select(driver.findElement(by));
			}//end try clause
			catch(Exception e){
				logFileWriter.write("ERROR - got an Exception when trying to instantiate Selector with name "+name);
				logFileWriter.newLine();
				logFileWriter.write(e.getMessage());
				logFileWriter.newLine();
			}//end catch clause
			Thread.sleep(1000);
			String text = selector.getFirstSelectedOption().getText();
			logFileWriter.write("Returning text "+text+" from selector "+ name);
			logFileWriter.newLine();
			logFileWriter.newLine();
			return text;
		}//end getSelectedTextForBy method
		
		public String getSelectedText(HSSFExcelReader reader, String worksheet, String name, BufferedWriter logFileWriter) throws Exception{
			logFileWriter.write("*** method getSelectedText called with name " + name +" ***");
			logFileWriter.newLine();
			By by = getByForName(reader, worksheet, name, logFileWriter);
			return getSelectedTextForBy(by, name, logFileWriter);
		}//end getSelectedText method
		
		public By getByForLocator(String rawLocator, BufferedWriter logFileWriter) throws Exception{
			String locator = new String();
			if (rawLocator.indexOf("=") != -1){
				String locatorType = rawLocator.split("=")[0];
				locator = rawLocator.split("=")[1];
				if (locatorType.equalsIgnoreCase("id"))
					return By.id(locator);
				else if (locatorType.equalsIgnoreCase("link"))
					return By.linkText(locator);
				else if (locatorType.contains("xpath"))
					return By.xpath(locator);
				else if (locatorType.contains("css"))
					return By.cssSelector(locator);
				else if (locatorType.contains("name"))
					return By.name(locator);
				else {
					logFileWriter.write("ERROR - we cannot identify this locator type "+locatorType);
					logFileWriter.newLine();
					return null;//we cannot identify this locator
				}//end else
			}//end if
			else 
				return By.name(rawLocator);			
		}//end getByForLocator method
		
		public By getByForName(HSSFExcelReader reader, String worksheet, String name, BufferedWriter logFileWriter) throws Exception{
			logFileWriter.write("*** method getByForName called with name " + name +" ***");
			logFileWriter.newLine();
			String rawLocator = getLocatorByName(reader, worksheet, name, logFileWriter);
			logFileWriter.write("Locator received is "+rawLocator);
			logFileWriter.newLine();
			return getByForLocator(rawLocator, logFileWriter);
		}//end getByForName method
		

		
		protected String getLocatorByName(HSSFExcelReader reader, String worksheet, String name, BufferedWriter logFileWriter) throws Exception{
			logFileWriter.write("*** method getLocatorByName called with locator name "+ name +" ***");
			logFileWriter.newLine();
			int rowNum = reader.getCellRowNum(worksheet, "Name", name);
			if (rowNum == -1){
				logFileWriter.write("Could not find the row with locator name "+name);
				logFileWriter.newLine();
				return new String();
			}//end if - we don't go any further if we don't get the row number
			String locator = reader.getCellData(worksheet, "Locator", rowNum);
			logFileWriter.write("Locator "+locator +" found on row "+rowNum);
			logFileWriter.newLine();
			if ((locator == null) ||(locator.isEmpty())){
				logFileWriter.write("WARNING!! Could not locate the locator for locator name "+name
						+" on row "+rowNum);	
				logFileWriter.newLine();
			}//end if - we can't get the locator even though we got the right row number
			logFileWriter.write("method getLocatorByName returning locator "+locator);
			logFileWriter.newLine();
			return locator;
		}//end getLocatorByName method


	  
	  public String getTextFromElement(By by) throws Exception{
		  String text = new String();
		  waitForElementPresent(by);
		  text = driver.findElement(by).getText();
		  return text;
	  }//end getTextFromElement method
	  
	  public String getStringValueFromElement(By by) throws Exception{
		  String text = new String();
		  waitForElementPresent(by);
		  text = driver.findElement(by).getAttribute("value");
		  return text;
	  }//end getStringValueFromElement method
	  
	  public boolean writeStringToTextField (By by, String toWrite, BufferedWriter logFileWriter) throws Exception{
		  boolean writeSuccessful = false;
		  waitForElementPresent(by);
		  try{
			  Thread.sleep(1000);
			  driver.findElement(by).clear();
			  Thread.sleep(2000);
			  driver.findElement(by).sendKeys(toWrite);
			  String textWritten = driver.findElement(by).getAttribute("value");
			  if (textWritten.equalsIgnoreCase(toWrite)) {
				  logFileWriter.write("method writeStringToTextField successfully wrote "+toWrite+" to textfield");
				  writeSuccessful = true;
			  }
			  else {
				  writeSuccessful = false;
				  logFileWriter.write("Could not write the String " + toWrite +" - will re-attempt to write through Robot");
				  logFileWriter.newLine();
				  writeStringToTextFieldViaRobot(toWrite, by);
				  writeSuccessful = true;
			  }
		  }
		  catch(Exception e){
			  logFileWriter.write("Couldn't finish writing the String " + toWrite 
					  +", because Exception was thrown: "+e.getMessage());
			  logFileWriter.newLine();
			  try {
				  logFileWriter.write("Attempting to write the String '" + toWrite +"' through Robot");
				  logFileWriter.newLine();
				  WebElement textfield = driver.findElement(by);
				  textfield.click();
				  Thread.sleep(2000);
				  writeStringToTextFieldViaRobot(toWrite, by);
				  Thread.sleep(2000);
				  if (textfield.getAttribute("value").equalsIgnoreCase(toWrite))
					  writeSuccessful = true;
				  else writeSuccessful = false;
				  if (writeSuccessful)
					  logFileWriter.write("Wrote the String " + toWrite +" through Robot");
				  else 
					  logFileWriter.write(" *** Could not write the String " + toWrite +" through Robot ***");
				  logFileWriter.newLine();
			  }
			  catch(Exception e2) {
				  logFileWriter.write("Couldn't finish writing the String " + toWrite 
						  +" through Robot, because Exception was thrown: "+e.getMessage());
				  logFileWriter.newLine();
				  writeSuccessful = false;
			  }

		  }
		  return writeSuccessful;
	  }
	  
	  public void writeStringToTextFieldViaRobot(String toWrite, By by) throws Exception{
		  driver.findElement(by).click();
		  Robot robot = new Robot();
		  robot.keyPress(KeyEvent.VK_CONTROL);
		  Thread.sleep(2000);
		  robot.keyPress(KeyEvent.VK_A);
		  Thread.sleep(2000);
		  robot.keyRelease(KeyEvent.VK_A);
		  Thread.sleep(2000);
		  robot.keyRelease(KeyEvent.VK_CONTROL);
		  Thread.sleep(3000);
		  for(int i=0; i<10; i++) {
			  robot.keyPress(KeyEvent.VK_SHIFT);
			  robot.keyPress(KeyEvent.VK_BACK_SPACE);
			  robot.keyRelease(KeyEvent.VK_BACK_SPACE);
			  robot.keyRelease(KeyEvent.VK_SHIFT);
			  Thread.sleep(2000);
		  }//end for loop - deleting all of the existing characters
		  Thread.sleep(2000);
		  driver.findElement(by).click();
		  Thread.sleep(2000);
		  driver.findElement(by).sendKeys(toWrite);
	  }//end writeStringToTextFieldViaRobot method
	  
	  public static void switchToStartPage(BufferedWriter logFileWriter) throws Exception{
		  if (! switchWindowByURL("showDocumentsTab.do", logFileWriter)){
			  printToAllOutputs("Unable to switch to the Start Page.  Driver will open Start Page.", logFileWriter);
			  driver.get(startPage);
		  }//end if
		  else{
			  printToAllOutputs("Successfully switched to the Start Page the first time.", logFileWriter);
		  }//end else		  
	  }//end switchToStartPage method
	  
	  public static void switchToAdministrationPage(BufferedWriter logFileWriter) throws Exception{
		  if (! switchWindowByURL("administrationTab.jsp", logFileWriter)){
			  switchToStartPage(logFileWriter);
			  Thread.sleep(2000);
			  waitAndClick(By.linkText("Administration"));
			  logFileWriter.write("...had to switch to the Start Page, before getting to the Administration Page");
			  logFileWriter.newLine();
			  Thread.sleep(5000);
			  if (! driver.getCurrentUrl().contains("administrationTab.jsp")){
				  logFileWriter.write("Could not get to the Administration Page, after opening Start Page.  Calling switchToAdministrationPage recursively");
				  logFileWriter.newLine();
				  switchToAdministrationPage(logFileWriter);
			  }//end if - we are not at the Administration Page
			  else {
				  logFileWriter.write("Successfully switched to the Administration Page after opening the Start Page");
				  logFileWriter.newLine();
			  }//end else - we got to the Administration Page
		  }//end if
		  else{
			  logFileWriter.write("Successfully switched to Administration Page the first time");
			  logFileWriter.newLine();
		  }
	  }//end switchToAdministrationPage method
	  
	  
	  
	  public static boolean openSalarySetting(BufferedWriter logWriter) throws Exception{
		  TestFileController.writeToLogFile("Now opening Salary Setting", logWriter);
		  Thread.sleep(3000);
		  try{
			  driver.get(startPage);
		  }
		  catch(NoSuchWindowException e){
			  	Thread.sleep(3000);
			    Set<String> handle=driver.getWindowHandles();
			    for(String handles:handle){
			    	TestFileController.writeToLogFile("Exception thrown - trying to get to a different open window", logWriter);
			    	driver.switchTo().window(handles);
			    	Thread.sleep(3000);
			    	driver.get(startPage);
			    }//end for
		  }//end catch
		  waitAndClick (By.linkText("Operations"));
		  waitAndClick (By.linkText("Salary Setting"));
		  boolean switchIsSuccessful = false;//don't assume it's successful until it is
		  try{
			  TestFileController.writeToLogFile("Trying to switch to the dialog", logWriter);
			  switchIsSuccessful = switchWindowByURL (fiscalYearDialogURL, logWriter);
			  if(switchIsSuccessful){
				  System.out.println("Successfully acquired dialog");
				  TestFileController.writeToLogFile("Successfully acquired dialog", logWriter);
			  }
			  else {
				  System.out.println("Could not successfully acquire dialog");
				  TestFileController.writeToLogFile("Could not successfully acquire dialog", logWriter);
			  }
			  //driver.switchTo().window("ha_dialog");
		  }
		  catch (Exception e){
			  e.printStackTrace();
			  return false;
		  }
		  try{
			  if (! switchIsSuccessful){
				  System.out.println("Trying a second time to acquire dialog");
				  TestFileController.writeToLogFile("Trying a second time to acquire dialog", logWriter);
			  }
			  switchIsSuccessful = switchWindowByURL (fiscalYearDialogURL, logWriter);
			  TestFileController.writeToLogFile("Trying to get the fiscal year dropdown", logWriter);
			  waitForElementPresent(By.id("fiscalYear"));
			  if (! switchIsSuccessful){
				  System.out.println("Trying a third time to acquire dialog");
				  TestFileController.writeToLogFile("Trying a third time to acquire dialog", logWriter);
				  switchIsSuccessful = switchWindowByURL (fiscalYearDialogURL, logWriter);
				  waitForElementPresent(By.id("fiscalYear"));
			  }
			  new Select(driver.findElement(By.id("fiscalYear"))).selectByVisibleText(currentFiscalYear);
		  }
		  catch (Exception e){
			  e.printStackTrace();
			  return false;
		  }
		  try{
			  TestFileController.writeToLogFile("Trying to get to the button on the dialog", logWriter);
			  driver.findElement(By.id("mybutton")).click();
		  }
		  catch (Exception e){
			  TestFileController.writeToLogFile(e.getStackTrace().toString(), logWriter);
			  return false;
		  }
		  Thread.sleep(2000);
		  while (! switchWindowByURL(loadSalarySettingWindowURL, logWriter)){
			  TestFileController.writeToLogFile("Salary Setting Window not yet located", logWriter);
			  Thread.sleep(1000);
		  }
		  TestFileController.writeToLogFile("Salary Setting Window located", logWriter);
		  if (isDropdownAccessible(By.id("statusId"), "Closed", logWriter)) 
			  TestFileController.writeToLogFile("Set status to Closed", logWriter);
		  else{
			  TestFileController.writeToLogFile("Unable to set status to Closed", logWriter);
		  }
		  Thread.sleep(2000);
		  if (isDropdownAccessible(By.id("statusId"), "Open", logWriter)) 
			  TestFileController.writeToLogFile("Set status to Open", logWriter);
		  else{
			  TestFileController.writeToLogFile("Unable to set status to Open", logWriter);
		  }
		  Thread.sleep(2000);
		  return true;
	  }
	  
	  public static void switchToControlSheetTab() throws Exception{
		  waitForElementPresent(By.id(ControlSheetLocatorID));
		  waitAndClick(By.id(ControlSheetLocatorID));
	  }

      public static void getScreenShot(String fileName, String whichValues, BufferedWriter logFileWriter) throws Exception 
      {
              File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
              int periodIndex = fileName.indexOf(".");
              if (periodIndex > 0)
            	  fileName = fileName.substring(0, periodIndex + 1);
              else fileName = fileName + ".";
           //The below method will save the screen shot in d drive with name "screenshot.png"
              TestFileController.copyScreenShotFile(scrFile, whichValues, fileName +"png", logFileWriter);
      }//end getScreenShot method


	  public static boolean isDropdownAccessible (By by, String selection, BufferedWriter logWriter) throws Exception{
		  try{
			    for (int second = 0;; second++) {
			    	if (second >= 5) {
			    		TestFileController.writeToLogFile("widget " + by.toString() + " is not select-able ... returning", logWriter);
			    		return false;
			    	}
			    	try { 
			    		if (isElementPresent(by)){
			    			new Select(driver.findElement(by)).selectByVisibleText(selection);
			    			break; 
			    		}
			    	} 
			    	catch (Exception e) {e.printStackTrace();}
			    	TestFileController.writeToLogFile("waiting for element to be select-able, second: " + second, logWriter);
			    	Thread.sleep(1000);
			    }
		  }
		  catch (Exception e){
			  e.printStackTrace();
			  return false;
		  }
		  TestFileController.writeToLogFile("widget " + by.toString() + " is select-able ... returning", logWriter);
		  return true;
		  
	  }
	  
	  public static boolean closeAllWindowsExceptURLs(String[] URLs, BufferedWriter logFileWriter) throws Exception{
		  String currentWindow = null;
		  Set<String> availableWindows;
		  try {
	            availableWindows = driver.getWindowHandles(); 
	            currentWindow = (String) availableWindows.toArray()[0];
	            //currentWindow = driver.getWindowHandle(); 
	            if (! availableWindows.isEmpty()) { 
		            for (String windowId : availableWindows) {
		                String switchedWindowURL=driver.switchTo().window(windowId).getCurrentUrl();
		                TestFileController.writeToLogFile("Examining window with URL " + driver.getCurrentUrl(), logFileWriter);
		                boolean matchesURL = false;
		                for (int i=0; i<URLs.length; i++){
		                	if ((switchedWindowURL.equals(URLs[i]))||(switchedWindowURL.contains(URLs[i]))){
		                		matchesURL = true;
		                	}//end if
		                }//end for - iterating through the URLs
		                if (! matchesURL){
		                	TestFileController.writeToLogFile("Closing Window with URL " + driver.getCurrentUrl(), logFileWriter);
		                	driver.close();
		                }//end if - the window is not the target window and is to be closed
		                else { 
		                	TestFileController.writeToLogFile("Window with URL " + switchedWindowURL +" is not to be closed, proceeding to next window", logFileWriter);
		                	driver.switchTo().window(currentWindow); 
		                } //end else - this is the target window that is not to be closed
		            } //end outer for loop - iterating through all of the windows
	            }//end if
	            else{
		        	TestFileController.writeToLogFile("All windows appear to be closed", logFileWriter);
		        	return false;
	            }//end else
		  }//end try
		  catch (Exception e){
	        	TestFileController.writeToLogFile("Couldn't finish searching for windows because of Exception " + e.getMessage(), logFileWriter);
	        	if (e.getMessage().contains("target window already closed")){
	        		Thread.sleep(2000);
	        		currentWindow = (String) driver.getWindowHandles().toArray()[0];
	        		switchWindowByURL(currentWindow, logFileWriter);
	        		return closeAllWindowsExceptURLs(URLs, logFileWriter);
	        	}//end if
	        	return false;
	        }//end catch clause
		  TestFileController.writeToLogFile("Finished closing windows", logFileWriter);
		  return true;
	  }//end closeAllWindowsExceptURL method
	  
		public String switchToSearchAuthWindow(String ID, BufferedWriter logFileWriter) throws Exception{
			boolean gotWindow = UIController.switchWindowByURL(UIController.newSearchAuthorizationURL 
					+ "?authId="+ID, logFileWriter);
			if (gotWindow){
				logFileWriter.write("Got window with URL " +UIController.driver.getCurrentUrl());
				logFileWriter.newLine();
				Thread.sleep(2000);
				return new String();
			}//end if - we could switch to the window
			else{
				String message = "Could not get the window with the SA for ID "+ID+" - trying to dismiss alert; ";
				try{
					UIController.dismissAlert(logFileWriter);
					gotWindow = UIController.switchWindowByURL(UIController.newSearchAuthorizationURL 
							+ "?authId="+ID, logFileWriter);
					if (gotWindow)
						return new String();
					else
						return message;
				}
				catch(NoAlertPresentException e){
					message = message.concat("There was no alert to dismiss; ");
					logFileWriter.write(message);
					logFileWriter.newLine();
					return message;
				}
			}//end else
		}//end switchToSearchAuthWindow method

	  
		public String switchToRecruitmentWindow(String ID, BufferedWriter logFileWriter) throws Exception{
			boolean gotWindow = UIController.switchWindowByURL(UIController.newRecruitmentURL
					+ "?recId="+ID, logFileWriter);
			if (gotWindow){
				logFileWriter.write("Got window with URL " +UIController.driver.getCurrentUrl());
				logFileWriter.newLine();
				Thread.sleep(2000);
				return new String();
			}//end if - we could switch to the window
			else{
				String message = "Could not get the window with the Recruitment for ID "+ID+" - trying to dismiss alert; ";
				try{
					dismissAlert(logFileWriter);
					gotWindow = UIController.switchWindowByURL(UIController.newRecruitmentURL 
							+ "?recId="+ID, logFileWriter);
					if (gotWindow)
						return new String();
					else return message;
				}
				catch(NoAlertPresentException e){
					message = message.concat("There was no alert to dismiss; ");
					logFileWriter.write(message);
					logFileWriter.newLine();
					return message;
				}
			}//end else
		}//end switchToRecruitmentWindow method

		public static String dismissAlert(BufferedWriter logFileWriter) throws Exception{
			String message = new String();
			try{
				Alert alert = UIController.driver.switchTo().alert();
				message = "Alert text is "+ alert.getText();
				alert.accept();
			}//end try clause
			catch(NoAlertPresentException e){
				message = "There was no alert present";
				
			}//end catch clause
			logFileWriter.write(message);
			logFileWriter.newLine();
			return message;
		}//end dismissAlert method
		
		public static void cancelAlert(BufferedWriter logFileWriter) throws Exception{
			String message = new String();
			try{
				Alert alert = UIController.driver.switchTo().alert();
				message = "Alert text is "+ alert.getText();
				alert.dismiss();
			}//end try clause
			catch(NoAlertPresentException e){
				message = "There was no alert present";
			}//end catch clause
			logFileWriter.write(message);
			logFileWriter.newLine();
		}//end cancelAlert method
	  
		public static String processPageByLink(String linkText, BufferedWriter logFileWriter) throws Exception{
			boolean gotPage = UIController.clickLinkAndWait(linkText, logFileWriter);
			String message = new String();
			if (! gotPage){
				message = message.concat("Could not load "+linkText);
				logFileWriter.write(message);
				logFileWriter.newLine();
			}//end if
			else{
				logFileWriter.write("Got the page through link text "+linkText);
				logFileWriter.newLine();
			}//end else
			return message;
		}//end processPageByLink method
		
		/*
		 * Of note, this will return the number of the bottom-most row in the UI
		 * -OR- 
		 * the number of the first row in the UI containing the searched-for name or value
		 * Of note, there is an option to include an initial row (when the UI doesn't start
		 * numbering at one).  The UI assumes that the default is one unless otherwise 
		 * indicated.
		 */
		public int getBottomRow (String xpathBaseLocator, String valueSought, 
				BufferedWriter logFileWriter, boolean writeToLogFile) throws Exception{
			return getBottomRow (xpathBaseLocator, valueSought, 1, logFileWriter, writeToLogFile);
		}
		
		public int getBottomRow (String xpathBaseLocator, String valueSought, int initialRow, 
				BufferedWriter logFileWriter, boolean writeToLogFile) throws Exception{
			if (writeToLogFile){
				logFileWriter.write("*** method getBottomRow called with xpathBaseLocator " + xpathBaseLocator 
						+", and value sought '" + valueSought +"' ***");
				logFileWriter.newLine();
			}
			int numberOfRows= initialRow;
			boolean lastRowReached = false;
			valueSought = valueSought.trim();
			while (! lastRowReached){
				String xpathLocator = xpathBaseLocator;
				if (numberOfRows > 1){
					xpathLocator = getAmendedLocator(xpathLocator, numberOfRows, logFileWriter);
				}//end if
				Thread.sleep(3000);
				if (isElementPresent(By.xpath(xpathLocator))){
					String valueFound = new String();
					try {
						WebElement commitmentLink = driver.findElement(By.xpath(xpathLocator));
						valueFound = commitmentLink.getText().trim();
						if (valueFound.isEmpty()) {
							valueFound = driver.findElement(By.xpath(xpathLocator)).getAttribute("value");
							String message = "Had to use getAttribute method to obtain value '"+valueFound+"'";
							if (writeToLogFile) {
								logFileWriter.write(message);
								logFileWriter.newLine();
							}//end if
							else System.out.println(message);
						}//end if - valueFound is empty - we're trying a different tactic to get the value
					}//end try clause
					catch(StaleElementReferenceException e) {
						WebElement commitmentLink = driver.findElement(By.xpath(xpathLocator));
						valueFound = commitmentLink.getText().trim();
					}
					if (writeToLogFile){
						logFileWriter.write("Row at " + numberOfRows +" found, trimmed value is " + valueFound);
						logFileWriter.newLine();
					}//end if - write to log file
					if (valueFound.equalsIgnoreCase(valueSought)){
						lastRowReached = true;
						if (writeToLogFile){
							logFileWriter.write("Match! Value found at row " + numberOfRows);
							logFileWriter.newLine();
						}//end if - write to log file					
					}
					else
						numberOfRows++;
				}//end if - element is found in this column
				else{
					numberOfRows--;
					lastRowReached = true;
					if (writeToLogFile){
						logFileWriter.write("Cannot locate element at locator "+xpathLocator);
						logFileWriter.newLine();
						logFileWriter.write("Bottom of UI reached - bottom-most row is " + numberOfRows);
						logFileWriter.newLine();
					}//end if - write to the log file
				}//end else	- element is not found in this column
			}//end while
			
			return numberOfRows;
		}//end getBottomRow method


		public String getAmendedLocator(String xpathLocator, int numberOfRows, BufferedWriter logFileWriter) throws Exception{
			String toReplace = "/tr";
			String replacement = "/tr["+numberOfRows+"]";
			int start = xpathLocator.lastIndexOf(toReplace);
			logFileWriter.write("found last 'tr' substring at index "+start);
			logFileWriter.newLine();
			StringBuilder amendedLocator = new StringBuilder();
			amendedLocator = amendedLocator.append(xpathLocator.substring(0, start));
			amendedLocator = amendedLocator.append(replacement);
			int postfix_start = start+toReplace.length();
			if (xpathLocator.charAt(start+3) == '['){
				logFileWriter.write("There was already an incremented row - adjusting to eliminate redundant increment");
				logFileWriter.newLine();
				postfix_start +=3;//add three characters to eliminate the redundant increment
			}
			amendedLocator = amendedLocator.append(xpathLocator.substring(postfix_start));
			logFileWriter.write("Modified xpath locator is "+amendedLocator.toString());
			logFileWriter.newLine();
			//xpathLocator = xpathBaseLocator.replaceFirst("/tr", "/tr[" + numberOfRows + "]");
			return amendedLocator.toString();
		}//end getAmendedLocator method
		
		public static void outputAvailableWindows(BufferedWriter logFileWriter) throws Exception{
			printToAllOutputs("", logFileWriter);
			printToAllOutputs("*** method outputAvailableWindows called ***", logFileWriter);
			printToAllOutputs("", logFileWriter);
			printToAllOutputs("List of available windows is as follows:", logFileWriter);

			Thread.sleep(4000);
			Set<String> availableWindows = driver.getWindowHandles(); 
			printToAllOutputs("Number of available windows is "+availableWindows.size(), logFileWriter);
			for (String windowId: availableWindows){
				String switchedWindowURL= new String();
				printToAllOutputs("Now working with window ID "+windowId, logFileWriter);
				try {
					switchedWindowURL=driver.switchTo().window(windowId).getCurrentUrl();
					printToAllOutputs("The URL for window ID "+windowId+" is "+switchedWindowURL, logFileWriter);
				}
				catch(Exception e) {
					String errMsg = e.getMessage();
					printToAllOutputs("Could not switch to the window because of Exception "+errMsg,logFileWriter);
					if (errMsg.contains("Alert")) {
						printToAllOutputs("Encountering an error while trying to switch to a window - Error message is as follows:", logFileWriter);
						printToAllOutputs(errMsg, logFileWriter);
						try {
							printToAllOutputs("Attempting to dismiss an alert", logFileWriter);
							Alert alert = driver.switchTo().alert();
							printToAllOutputs(alert.getText(), logFileWriter);
							alert.accept();
						}
						catch(Exception e2) {
							printToAllOutputs("Couldn't get to the alert - Exception is "+e2.getMessage(), logFileWriter);
							if (e2.getMessage().contains("timeout")) {
								printToAllOutputs("Timeout error -cannot refresh", logFileWriter);
								Thread.sleep(3000);
							}//end if - it's a timeout error, we cannot refresh the window
							else {
								driver.navigate().refresh();
								Thread.sleep(8000);								
								try {
									driver.switchTo().alert().accept();
									printToAllOutputs("Successfully dismissed alert", logFileWriter);
								}
								catch(Exception e3) {
									printToAllOutputs("Could not acquire the alert because of Exception "+e3.getMessage(), logFileWriter);
								}//end inner catch clause to dismiss alert
							}//end else - we can get to the window and refresh it
						}//end catch clause - attempt to refresh the page and dismiss the alert
					}//end if - alert is dismissed
					else if (errMsg.contains("Component")) {
						printToAllOutputs("Exception with a page component was thrown", logFileWriter);
						printToAllOutputs("Attempt will be made to refresh the screen and dismiss alert", logFileWriter);
						driver.navigate().refresh();
						Thread.sleep(10000);
						Alert alert = driver.switchTo().alert();
						String alertMessage = alert.getText();
						alert.accept();
					}
					else {
						printToAllOutputs("Did not encounter an Alert when trying to switch windows to window ID "+windowId, logFileWriter);
					}
				}//end outer catch clause to refresh the tab in the window and hope this resolves the issue
                printToAllOutputs(switchedWindowURL, logFileWriter);
			}//end for
		}//end outputAvailableWindows method
		
	  public static boolean switchWindowByHandle (String windowId, boolean involvesAnnotationDialog, BufferedWriter logFileWriter) throws Exception{
		  boolean switched = false;
		  String currentWindowID = new String();
		  String currentWindowURL = new String();
		  if (! involvesAnnotationDialog) {
			  currentWindowID = driver.getWindowHandle();
			  printToAllOutputs("There is no annotation dialog involved", logFileWriter);
			  printToAllOutputs("Prior to switch, Window ID is ", logFileWriter);
			  currentWindowURL = driver.getCurrentUrl();
			  printToAllOutputs ("Prior to switch, URL is "+currentWindowURL, logFileWriter);
		  }
		  else {
			  printToAllOutputs("There is an annotation dialog involved", logFileWriter);
		  }
		  printToAllOutputs("Now switching to window with ID "+windowId, logFileWriter);
		  try {
			  driver.switchTo().window(windowId);
			  try {
				  Thread.sleep(6000);
				  Alert alert = driver.switchTo().alert();
				  String alertMessage = alert.getText();
				  alert.accept();
				  printToAllOutputs("Alert dismissed - message was "+alertMessage, logFileWriter);
			  }
			  catch(Exception e) {
				  printToAllOutputs("Could not dismiss alert because of Exception "+e.getMessage(), logFileWriter);
			  }
			  switched = true;
		  }//end try clause
		  catch(Exception e) {
			  currentWindowURL = switchToAvailableWindow(logFileWriter, involvesAnnotationDialog);
			  if (! currentWindowURL.isEmpty()){
				  switched = true;
			  }
			  else switched = false;
		  }//end catch clause
		  
		  currentWindowID = driver.getWindowHandle();
		  currentWindowURL = driver.getCurrentUrl();
		  
		  printToAllOutputs("After switch, Window ID is "+currentWindowID, logFileWriter);
		  currentWindowURL = driver.getCurrentUrl();
		  printToAllOutputs ("After switch, URL is "+currentWindowURL, logFileWriter);
		  
		  if (currentWindowID.equalsIgnoreCase(windowId))
			  printToAllOutputs ("Switched successfully to window ID "+windowId+", with URL "+driver.getCurrentUrl(), logFileWriter);
		  else
			  printToAllOutputs("Could not switch successfully to window ID "+windowId, logFileWriter);
		  return switched;
	  }//end switchWindowByHandle method
		
	  public static boolean switchWindowByURL (String URL, BufferedWriter logWriter) throws Exception{
		  return switchWindowByURL(URL, logWriter, false);
	  }//end switchWindowByURL method - no dialog to dismiss

	  public static String switchToAvailableWindow(BufferedWriter logFileWriter, boolean hasDialogURL) throws Exception{
		  	printToAllOutputs("Now trying to get the list of available windows", logFileWriter);
		  	String currentWindow = new String();
			Set<String> availableWindows = driver.getWindowHandles(); 
            printToAllOutputs("Number of available windows is "+availableWindows.size(),logFileWriter);
            String currentURL = new String();
            int arrayLastIndex = availableWindows.toArray().length - 1;
            if (hasDialogURL) {
            	currentWindow = (String) availableWindows.toArray()[arrayLastIndex];
            }
            else {
            	currentWindow = (String) availableWindows.toArray()[0];
            }
            printToAllOutputs("Current Window is "+currentWindow, logFileWriter);
            
            try {
            	printToAllOutputs("Now trying to switch to window with ID "+currentWindow, logFileWriter);
            	driver.switchTo().window(currentWindow);
            }
            catch(Exception e) {
            	printToAllOutputs("Could not switch to the current window because of Exception "+e.getMessage(), logFileWriter);
            }
            try {
            	currentURL = driver.getCurrentUrl();
            }
            catch(Exception e) {
            	String errMsg = e.getMessage();
            	printToAllOutputs("Could not get current window URL because of Exception "+errMsg, logFileWriter);
				if (errMsg.contains("Component")) {
					printToAllOutputs("Exception with a page component was thrown", logFileWriter);
					printToAllOutputs("Attempt will be made to refresh the screen and dismiss alert", logFileWriter);
					driver.navigate().refresh();
					Thread.sleep(10000);
					Alert alert = driver.switchTo().alert();
					String alertMessage = alert.getText();
					alert.accept();
				}
            }
		  return currentURL;
	  }//end switchToAvailableWindow method
	  
	  public static boolean switchWindowByURL(String URL, BufferedWriter logWriter, boolean hasDialogURL) throws Exception{
		  String currentWindow = null;		  
		  Set<String> availableWindows = driver.getWindowHandles(); 

		  try {
			  	String currentURL = new String();
			  	currentURL = switchToAvailableWindow (logWriter, hasDialogURL);
	            //currentWindow = driver.getWindowHandle(); 
	            printToAllOutputs ("Current URL is "+currentURL, logWriter);
	            if (! availableWindows.isEmpty()) { 
				  	printToAllOutputs("list of available windows is not empty", logWriter);
		            for (String windowId : availableWindows) {
		                String switchedWindowURL=driver.switchTo().window(windowId).getCurrentUrl();
		                printToAllOutputs("Examining window with URL " + switchedWindowURL, logWriter);
		                if ((switchedWindowURL.equals(URL)) || (switchedWindowURL.contains(URL))){
		                	printToAllOutputs("Found Window with URL " + driver.getCurrentUrl(), logWriter);
		                    return true; 
		                } else { 
		                	printToAllOutputs("Current Window with URL "+switchedWindowURL+" does not contain "+URL, logWriter);
		                	//printToAllOutputs("attempting to switch back to window with URL " + currentURL, logWriter);
		                	//driver.switchTo().window(currentWindow); 
		                } 
		            } 
	            }
	        }
	        catch (Exception e){
	        	printToAllOutputs("Couldn't finish searching for window with URL "+ URL +" because of Exception " + e.getMessage(), logWriter);
	        	if (e.getMessage().contains("target window already closed")){
	        		Thread.sleep(2000);
	        		currentWindow = (String) driver.getWindowHandles().toArray()[0];
	        		return switchWindowByURL(URL, logWriter);
	        	}//end if
	        	else if (e.getMessage().contains("unexpected alert open")){
	        		try{
	        			driver.switchTo().alert().accept();
	        		}//end try
	        		catch(Exception e2){
	        			TestFileController.writeToLogFile("Couldn't close the alert", logWriter);
	        		}//end catch
	        	}//end else if
	        	try {
	        		outputAvailableWindows(logWriter);
	        	}//end try clause
	        	catch(Exception e2) {
	        		String message = "Couldn't finish output of available windows because of Exception "+e2.getMessage();
	        		printToAllOutputs(message, logWriter);
	        	}//end catch clause
	        	return false;
	        }
	        printToAllOutputs("Couldn't locate window with URL " + URL, logWriter);
	        try {
	        	outputAvailableWindows(logWriter);
	        }
	        catch(Exception e) {
	        	printToAllOutputs("Couldn't output available windows because of Exception "+e.getMessage(), logWriter);
	        }
	        return false;
	   }//end switchWindowByURL method
	  
	  public static void printToAllOutputs(String message, BufferedWriter logFileWriter) throws Exception{
		  System.out.println(message);
		  logFileWriter.write(message);
		  logFileWriter.newLine();
	  }//end printToAllOutputs method


	  public void switchToReportTab() throws Exception{
		  waitForElementPresent(By.id(ReportTabLocatorID));
		  waitAndClick(By.id(ReportTabLocatorID));
	  }
	  
	  public void switchToSalarySettingTab() throws Exception{
		  waitForElementPresent(By.id(SalarySettingTabLocatorID));
		  Thread.sleep(3000);
		  waitAndClick(By.id(SalarySettingTabLocatorID));
		  Thread.sleep(3000);
		  waitAndClick(By.id(SalarySettingTabLocatorID));
		  Thread.sleep(3000);
	  }
	  
	  public void switchToSalarySettingManagementTab() throws Exception{
		  waitForElementPresent(By.id(SalarySettingManagementTabLocatorID));
		  waitAndClick(By.id(SalarySettingManagementTabLocatorID));
	  }
	  
	  public void runOneReportAtLocator(By by, BufferedWriter logFileWriter) throws Exception{
		  TestFileController.writeToLogFile("Now verifying that we have successfully switched to the appropriate tab", logFileWriter);
		  Thread.sleep(2000);//give it a couple of seconds just to make sure that it's present and accessible
		  waitForElementPresent(by);
		  TestFileController.writeToLogFile("Now attempting to run the report through the locator " + by.toString(), logFileWriter);
		  waitAndClick(by);
		  Thread.sleep(2000);//give it a couple more seconds once it's clicked
	  }

	  public static void waitForElementPresent (By by) throws Exception{
		    for (int second = 0;; second++) {
		    	if (second >= 60) throw new Exception("timeout - element not present: " + by.toString());
		    	try { if (isElementPresent(by)) break; } catch (Exception e) {}
		    	Thread.sleep(1000);
		    }
	  }
	  
	  public static String verifyText(By by, String text) throws Exception{
		  waitForElementPresent(by);
		  String actualText = new String();
		  try {
			  actualText = driver.findElement(by).getText();
			  assertEquals(text, actualText);
			} 
		  catch (Error e) {
			  return e.getMessage();
			}
		  return new String();
	  }//end verifyText method

	  
	  protected static boolean isElementPresent(By by) {
		    try {
		      driver.findElement(by);
		      return true;
		    } catch (NoSuchElementException e) {
		      return false;
		    }
		    catch(org.openqa.selenium.TimeoutException e2) {
		    	System.out.println ();
		    	System.out.println("*** ERROR - TIMEOUT EXCEPTION - returning false; Exception message "+e2.getMessage()+" ***");
		    	System.out.println();
		    	System.out.println("*** Stack Trace is as follows ***");
		    	System.out.println(e2.getStackTrace());
		    	System.out.println();
		    	return false;
		    }
		  }
	  
	  protected static boolean hasVisibleText(By by){
		  try{
		      String visibleText = driver.findElement(by).getText();
		      
		      if (visibleText.isEmpty())
		    	  return false;
		      else return true;
		    } 
		  catch (Exception e) {
			      return false;
			}
	  }
	  
	  protected static boolean hasVisibleTextValue(By by){
		  try{
		      String visibleText = driver.findElement(by).getAttribute("value");
		      if (visibleText.isEmpty())
		    	  return false;
		      else return true;
		    } 
		  catch (Exception e) {
			      return false;
			}
	  }
	  
	  protected static boolean isVisible(By by){
		  WebElement element = driver.findElement(by);
		  return element.isDisplayed();
	  }
		  
	  public static boolean waitAndClick (By by) throws Exception{
		  return waitAndClick(by, 30);
	  }
	  
	  public static String[] getAllOptions (By by, BufferedWriter logFileWriter) throws Exception{
		  logFileWriter.write("*** method getAllOptions called ***");
		  logFileWriter.newLine();
		  List<WebElement> optionsList = new Select(driver.findElement(by)).getOptions();
		  String[] optionsArray = new String[optionsList.size()];
		  for (int i=0; i<optionsArray.length; i++){
			  optionsArray[i] = optionsList.get(i).getText();
			  logFileWriter.write("Now adding menu item " +optionsArray[i]);
			  logFileWriter.newLine();
		  }//end for
		  return optionsArray;
	  }//end getAllOptions method
	  
	  public static boolean waitAndSelectElementInMenu (By by, String menuItem) throws Exception{
		  boolean elementNotSelectable = true;//initially, assume we cannot select the menu item
		  for (int i=0; i<30 && elementNotSelectable; i++){
			  elementNotSelectable = (! selectElementInMenu (by, menuItem));
			  if (elementNotSelectable){//it's not select-able, so wait for one second
				  System.out.println("element '" + menuItem + "' not yet select-able");
				  Thread.sleep(1000);
			  }
			  else{//we can select it, so elementNotSelectable = false
				  System.out.println("element '" + menuItem + "' selected");
			  }
		  }//end for loop - we can't select it
		  return (! elementNotSelectable);
	  }//end waitAndSelectElementInMenu
	  
	  public static boolean clickLinkAndWait(String linkText, BufferedWriter logFileWriter) throws Exception{
		  By by = By.linkText(linkText);
		  return waitAndClick(by);
	  }//end clickLinkAndWait method
	  
	  public static boolean selectElementInMenu (By by, String menuItem) throws Exception{
		  waitForElementPresent(by);
		  boolean canSelect = false;
		  try{
			  Select select = new Select(driver.findElement(by));
			  Thread.sleep(2000);
			  select.selectByVisibleText(menuItem);
			  Thread.sleep(2000);
			  canSelect =  select.getFirstSelectedOption().getText().equalsIgnoreCase(menuItem);
		  }//end try clause
		  catch(Exception e){
			  canSelect =  false;
		  }//end catch clause
		  return canSelect;
	  }//end method


	  public static boolean waitAndClick (By by, int secondsToWait) throws Exception{
		    //System.out.print("waiting for the following element to render so it can be clicked ");
		    //System.out.println(by.toString());
		    for (int second = 0;; second++) {
		    	if (second >= secondsToWait) return false;
		    	try { if (isElementClickable(by)) break; } catch (Exception e) {System.out.println(e.getMessage());}
		    	Thread.sleep(1000);
		    }
		    return true;

	  }
	  
	  public static boolean isAlertPresent(){ 
		    try{ 
		        driver.switchTo().alert(); 
		        return true; 
		    }//end try clause
		    catch (NoAlertPresentException Ex){ 
		        return false; 
		    }//end catch clause
		}// isAlertPresent()
	  
	  public static boolean waitForAlertPresent() throws Exception{
		  boolean isPresent = false;
			for (int second = 0;; second++) {
				if (second >= 60) return false;
				try { if (UIController.isAlertPresent()){
					isPresent=true;
					break; 
				}
				} catch (Exception e) {}
				Thread.sleep(1000);
			}
			return isPresent;
	  }//end waitForAlertPresent method

}//end Class
