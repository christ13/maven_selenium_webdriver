package Maven.SeleniumWebdriver;


import java.io.BufferedWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ChairSalarySettingOverridesTest {
	protected static String testName;
	protected static String testCaseNumber;
	protected static String testDescription;
	protected static String testEnv;
	protected static StringBuffer verificationErrors;
	protected BufferedWriter logFileWriter;

	@Before
	public void setUp() throws Exception {
		testName = "ChairSalarySettingOverridesTest";
		testEnv = "Test";
		testCaseNumber = "056";
		testDescription = "Test of Overrides for Chair SSWs";
	}//end setUp method

	@After
	public void tearDown() throws Exception {
		TestFileController.writeTestResults(testEnv, testCaseNumber, testName, testDescription, verificationErrors);
		logFileWriter.newLine();
		logFileWriter.close();
	}//end tearDown method

	@Test
	public void test() throws Exception{
		verificationErrors = new StringBuffer();
		String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		logFileWriter = TestFileController.getLogFileWriter(logFileName);
		String[] clusters = HandSOnTestSuite.salarySettingMgrTabUI.getClusters("Override", logFileWriter);
		logFileWriter.write("The following Salary Setting Worksheets will be examined: ");
		logFileWriter.newLine();
		for (int i=0; i<clusters.length; i++){
			clusters[i] = clusters[i].replaceFirst(" Cluster", "");
			String fileName = "Chair Salary Setting Worksheet "+ clusters[i] + ".xlsx";
			logFileWriter.write(fileName);
			logFileWriter.newLine();
		}//end for loop

		//download the Chair SSW's
		logFileWriter.write("Now attempting to download these Chair Salary Setting Worksheets:");
		logFileWriter.newLine();
		SalarySettingManager.switchWindowByURL(SalarySettingManager.salarySettingURL, logFileWriter);
		HandSOnTestSuite.salarySettingMgrTabUI.switchToReportTab();
		ReportRunner.downloadChairSSWs(clusters, ReportRunner.referenceWhichValues, logFileWriter);
		
		for (int i=0; i<clusters.length; i++){
			logFileWriter.newLine();
			logFileWriter.newLine();
			logFileWriter.write("**** Now attempting to get Overridden Faculty for Cluster " +clusters[i]+" ****");
			logFileWriter.newLine();
			HashMap<String, String[]> clusterFaculty 
				= HandSOnTestSuite.salarySettingMgrTabUI.getFacultyForCluster(clusters[i], "Override", "Chair", logFileWriter);
			Iterator clusterIterator = clusterFaculty.entrySet().iterator();
			while(clusterIterator.hasNext()){
				Map.Entry pair = (Map.Entry)clusterIterator.next();
				String dept = (String)pair.getKey();
				logFileWriter.write("Now working with Department " + dept);
				logFileWriter.newLine();
				String[] faculty = (String[])pair.getValue();
				for (int j=0; j<faculty.length; j++){
					logFileWriter.write("Now working with Faculty " + faculty[j]);
					logFileWriter.newLine();
					boolean fileContainsFaculty = containsOverriddenFaculty(faculty[j], dept, clusters[i], logFileWriter);
					logFileWriter.write("File contains overridden faculty: " + fileContainsFaculty);
					logFileWriter.newLine();
					if (! fileContainsFaculty){
						String message = "File didn't contain faculty member "+faculty[j] 
								+", in department "+dept +", as expected.  ";
						logFileWriter.write(message);
						logFileWriter.newLine();
						verificationErrors.append(message);
					}//end if
				}//end inner for - iterating through the whole list of faculty for that department
			}//end while loop - iterating through the faculty in the cluster
		}//end for
	}//end test method
	
	private boolean containsOverriddenFaculty(String facultyName, String dept, String cluster, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method containsOverriddenFaculty called with faculty name " + facultyName
				+", and Department "+dept +", and Cluster " + cluster + " ***");
		logFileWriter.newLine();
		String fileName = "Chair Salary Setting Worksheet "+cluster+".xlsx";
		SSExcelReader myReader = new SSExcelReader(TestFileController.directory +"/" +fileName, TestFileController.DepartmentSalarySettingWorksheetHeaderRow);
		int rowCount = myReader.getRowCount(TestFileController.ChairSalarySettingWorksheetWorksheetName); 
		logFileWriter.write("Row count for instantiated reader for " + fileName +" is " + rowCount);
		logFileWriter.newLine();
		
		int rowForColNames = myReader.getCellRowNum(TestFileController.ChairSalarySettingWorksheetWorksheetName, 0, SalarySettingManagementUIController.overrideSectionHeading);
		logFileWriter.write("Estimated location of overridden faculty at row " + rowForColNames);
		logFileWriter.newLine();

		int actualRowForColNames = myReader.getCellRowNum(TestFileController.ChairSalarySettingWorksheetWorksheetName, 0, "Department", rowForColNames);
		logFileWriter.write("Actual header row for overridden faculty is " + actualRowForColNames);
		logFileWriter.newLine();

		boolean foundUnexpectedString = false;
		int rowNum = -1;
		if (actualRowForColNames != -1){
			myReader.setRowForColumnNames(rowForColNames);
			String[] colNames = {"Name", "Department", "Override", "Description"};
			for (int j=0; j<colNames.length; j++){
				if (j==0)
					rowNum = myReader.getCellRowNum(TestFileController.ChairSalarySettingWorksheetWorksheetName, colNames[j], facultyName);
				String value = myReader.getCellData(TestFileController.ChairSalarySettingWorksheetWorksheetName, colNames[j], rowNum);
				logFileWriter.write("Value for the "+ colNames[j] +" column is found in the SSW: "+ value);
				logFileWriter.newLine();
				String attributeName = new String();
				if (colNames[j].equalsIgnoreCase("Override"))
					attributeName = "WhichWorksheet";
				else if (colNames[j].equalsIgnoreCase("Department"))
					attributeName = "Dept";
				else attributeName = colNames[j];
				String UIValue = new String();
				UIValue = HandSOnTestSuite.salarySettingMgrTabUI.getOverrideAttributeForNameAndDept(facultyName, dept, attributeName, logFileWriter);
				if ((value == null) || (UIValue == null) || (value.isEmpty()) || (UIValue.isEmpty()) || (! UIValue.equalsIgnoreCase(value))){
					logFileWriter.write("MISMATCH found --> SSW value: " + value +", UI Value: " + UIValue);
					logFileWriter.newLine();
					foundUnexpectedString = true;
				}//end if - there's a mismatch
			}//end inner for loop - iterating through the columns
		}//end if
		TestFileController.closeOneExcelReader(myReader);
		logFileWriter.newLine();
		logFileWriter.write("Values determined by this method: ");
		logFileWriter.newLine();
		logFileWriter.write("Row containing the Override Section Header: " + rowForColNames);
		logFileWriter.newLine();
		logFileWriter.write("Row containing the Column Headers: " + actualRowForColNames);
		logFileWriter.newLine();
		logFileWriter.write("Row containing the Faculty values: " + rowNum);
		logFileWriter.newLine();
		logFileWriter.write("Unexpected String found: " + foundUnexpectedString);
		logFileWriter.newLine();
		return ((rowForColNames != -1) && (actualRowForColNames != -1) && (rowNum != -1)
				&& (actualRowForColNames == rowForColNames + 1) && (! foundUnexpectedString));

	}//end containsOverriddenFaculty method

}//end ChairSalarySettingOverridesTest Class
