package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class SearchAuthorizationUIController extends UIController {

	public static HSSFExcelReader myExcelReader;
	public static final String GeneralWorksheetName = "SA General Page";
	public static final String CostsWorksheetName = "SA Costs Page";

	public SearchAuthorizationUIController() {
		myExcelReader = TestFileController.fileIn;
	}//end constructor
	
	public String getSelectedText(String name, BufferedWriter logFileWriter) throws Exception{
		return getSelectedText(myExcelReader, GeneralWorksheetName, name, logFileWriter);
	}//end getSelectedText method
	
	public String getFacultyLevel(BufferedWriter logFileWriter) throws Exception{
		String level = getSelectedText("FacultyLevelSelect", logFileWriter); 
		logFileWriter.write("Faculty level is "+level);
		logFileWriter.newLine();
		return level;
	}//end getFacultyLevel method
	
	public String getLab(BufferedWriter logFileWriter) throws Exception{
		String labYN = getSelectedText("LabSelect", logFileWriter); 
		logFileWriter.write("Lab (Yes/No) is "+labYN);
		logFileWriter.newLine();
		return labYN;
	}//end getLab method

	public String getCluster(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("**** Now obtaining Cluster info ****");
		logFileWriter.newLine();
		
		String cluster = new String();
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("ClusterLabel", logFileWriter);
		cluster = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by);
		
		logFileWriter.write("Cluster is found to be '"+cluster+"';");
		logFileWriter.newLine();
		
		return cluster;
	}//end getCluster method
	
	public String getTextFromElement(By by, BufferedWriter logFileWriter) throws Exception{
		String text = new String();
		try{
			text = super.getTextFromElement(by);
		}
		catch(Exception e){
			logFileWriter.write("The following Exception was thrown when attempting to get the text of "+by.toString());
			logFileWriter.write(e.getMessage());
			logFileWriter.newLine();
		}
		return text;
	}//end getTextFromElement method
	
	public String getStringValueFromElement(By by, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method getStringValueFromElement called ***");
		logFileWriter.newLine();
		String text = new String();
		try{
			text = super.getStringValueFromElement(by);
			if (text == null)
				text = new String();	
		}
		catch(Exception e){
			logFileWriter.write("The following Exception was thrown when attempting to get the text of "+by.toString());
			logFileWriter.write(e.getMessage());
			logFileWriter.newLine();
		}
		logFileWriter.write("returning text '"+text+"' from method getStringValueFromElement");
		logFileWriter.newLine();
		logFileWriter.newLine();
		return text;
	}//end getStringValueFromElement method
	
	public String[] getLocatorNamesBelowLocator(String worksheetName, String belowLocatorName, 
			BufferedWriter logFileWriter) throws Exception{
		return super.getLocatorNamesBelowLocator(myExcelReader, worksheetName, belowLocatorName, logFileWriter);
	}//end getLocatorNamesBelowLocator method implemented for this Class
	

	public By getByForName(String name, BufferedWriter logFileWriter) throws Exception{
		return getByForName(myExcelReader, GeneralWorksheetName, name, logFileWriter);
	}//end getByForName method
	
	public By getByForName(String worksheetName, String name, BufferedWriter logFileWriter) throws Exception{
		return getByForName(myExcelReader, worksheetName, name, logFileWriter);
	}//end getByForName method
	
	public String getLocatorByName(String name, BufferedWriter logFileWriter) throws Exception{
		return super.getLocatorByName(myExcelReader, GeneralWorksheetName, name, logFileWriter);
	}//end getLocatorByName method
	
	public String getLocatorByName(String worksheet, String name, BufferedWriter logFileWriter) throws Exception{
		return super.getLocatorByName(myExcelReader, worksheet, name, logFileWriter);
	}//end getLocatorByName method
	
	/*
	 * In this method, we will simply check the page to see what Cost types are there, 
	 * and, if they are, we add the Cost type and amount to a HashMap and return the 
	 * HashMap.
	 */
	public HashMap<String, Integer> getActualCosts(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method getActualCosts called***");
		logFileWriter.newLine();
		String[] potentialCosts = getLocatorNamesBelowLocator(CostsWorksheetName, 
				"SearchAuthDescription", logFileWriter);
		HashMap<String, Integer> actualCosts = new HashMap<String, Integer>();
		String costTypeLocatorName = new String();
		for(int i=0; i<potentialCosts.length; i++){
			if (potentialCosts[i].contains("CostType"))
					costTypeLocatorName = potentialCosts[i];
			else if (potentialCosts[i].contains("CostAmt") || potentialCosts[i].contains("FirstFYTextField")){
				By costTypeBy = getByForName(CostsWorksheetName, costTypeLocatorName, logFileWriter);
				String costType = costTypeLocatorName.replace("CostType", "");
				if (isElementPresent(costTypeBy)){
					logFileWriter.write("Cost "+costType+" is present, ");
					logFileWriter.newLine();
					By costAmtBy = getByForName(CostsWorksheetName, potentialCosts[i], logFileWriter);
					String costAmtString = new String();
					if(costType.contains("Housing")){
						costType = adjustHousingCostTypeString(costTypeBy, logFileWriter);
						if (isElementPresent(costAmtBy)){
							costAmtString = getStringValueFromElement(costAmtBy, logFileWriter);
						}//end if - the cost is present
						else costAmtString = "0";//make it zero - it will be removed
					}//end if - adjust the Housing Cost type 
					else costAmtString = this.getStringValueFromElement(costAmtBy, logFileWriter);
					logFileWriter.write("Cost Amount is "+costAmtString);
					logFileWriter.newLine();
					costAmtString = costAmtString.replace("$", "");//get rid of the dollar sign
					costAmtString = costAmtString.replace(",", "");//get rid of the comma
					costAmtString = costAmtString.replace(".000", "");//get rid of the decimal and thousandths
					costAmtString = costAmtString.replace(".00", "");//get rid of the decimal and cents
									Integer newValue = new Integer(costAmtString);
					Integer previousValue = (Integer)actualCosts.putIfAbsent(costType, newValue);
					if (previousValue != null){
						logFileWriter.write("Previous Value "+previousValue.intValue()+" was already there");
						if (previousValue.intValue() == newValue.intValue())
							logFileWriter.write(", and it's equal, as expected");
						else if (costType.contains("Summer9ths")){
							if (newValue.intValue() != 0)
								logFileWriter.write(", and it's nonzero, as expected");
							else {
								logFileWriter.write(", ERROR - it is unexpectedly zero");
								actualCosts.put(costType, new Integer(0));//zero it out so the test fails
							}//end inner else - error condition
						}//end else if
						else{
							logFileWriter.write(", and it's not equal - ERROR");
							actualCosts.put(costType, new Integer(0));//zero it out so the test fails
						}//end else - error condition
						logFileWriter.newLine();
					}
				}//end if - if it's present, check the amount
				else{
					logFileWriter.write("Cost associated with Cost Type "+ costType +" is not present");
					logFileWriter.newLine();
				}
			}//end else if - it's an amount so check to see if it's there and the amt is right
			else{
				logFileWriter.write("Locator Name "+potentialCosts[i] 
						+" cannot be associated with either a Cost Type or Amount");
				logFileWriter.newLine();
			}//end else - something is wrong - we can't use this locator name
		}//end for loop - iterating through the potential cost types and amounts

		//print them out so that we know what we've got
		Iterator iterator = actualCosts.entrySet().iterator();
		ArrayList<String> toBeDeleted = new ArrayList<String>();
		while(iterator.hasNext()){
			Map.Entry pair = (Map.Entry)iterator.next();
			String key = (String)pair.getKey();
			if (actualCosts.get(key).intValue() == 0){
				logFileWriter.write("Removing Cost type "+key+", because the Cost Amount is zero");
				logFileWriter.newLine();
				toBeDeleted.add(key);
			}//end if - we remove it if the Cost Amount is zero
		}//end while loop - specifying costs with zero amounts
		
		//unfortunately, if we try to remove the costs in the above loop, we'll get a ConcurrentModificationException
		for (int i=0; i<toBeDeleted.size(); i++)
			actualCosts.remove(toBeDeleted.get(i));

		outputCosts(actualCosts, "Actual Costs", logFileWriter);

		return actualCosts;
	}//end getActualCosts method
	
	public boolean verifyBlankField(String locatorName, BufferedWriter logFileWriter) throws Exception{
		By by = this.getByForName(CostsWorksheetName, locatorName, logFileWriter);
		String actual = this.getStringValueFromElement(by, logFileWriter);
		if ((actual == null) || (actual.isEmpty()))
			return true;
		else return false;
	}//end verifyBlankField method

	
	public String getInitialFY(BufferedWriter logFileWriter) throws Exception{
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Year1FYLabel", logFileWriter);
		if (! isElementPresent(by)){
			driver.findElement(By.linkText("general_a")).click();
		}
		return HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by);
	}//end getInitialFY method
	
	public String getPlannedBaseBudgetYear(BufferedWriter logFileWriter) throws Exception{
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("PlannedBaseBudgetYearTextfield", logFileWriter);
		WebElement element = null;
		try{
			element = driver.findElement(by);
		}
		catch(Exception e){
			driver.findElement(By.linkText("general_a")).click();
			logFileWriter.write("Clicking on the General Tab to get the element");
			logFileWriter.newLine();
			Thread.sleep(4000);
			if (isElementPresent(by))
				element = driver.findElement(by);
		}
		if (! element.isDisplayed()){
			driver.findElement(By.id("general_a")).click();
		}//end if - we had to click on the "General" tab to get it
		return HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
	}//end getPlannedBaseBudgetYear method
	
	public String getSalaryPromiseFY(BufferedWriter logFileWriter) throws Exception{
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(CostsWorksheetName, "FiscalYearTextField", logFileWriter);
		WebElement element = null;
		String FiscalYearText = new String();
		try{
			element = driver.findElement(by);
			FiscalYearText = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
		}//end try
		catch(Exception e){
			driver.findElement(By.id("costs_a")).click();
			Thread.sleep(4000);
			if (isElementPresent(by)){
				element = driver.findElement(by);
				FiscalYearText = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
			}//end if - element is present
			driver.findElement(By.id("general_a")).click();
		}
		if (! element.isDisplayed()){
			driver.findElement(By.id("costs_a")).click();
			FiscalYearText = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);

			driver.findElement(By.id("general_a")).click();
		}//end if - we had to click on the "Costs" tab to get it
		return FiscalYearText;
	}//end getSalaryPromiseFY method
	

	public String adjustHousingCostTypeString(By costTypeBy, BufferedWriter logFileWriter) throws Exception{
		Select housingMenu = new Select(UIController.driver.findElement(costTypeBy));
		if (housingMenu != null){
			logFileWriter.write("Housing Menu found and has the following options: ");
			Object[] options = (Object[]) housingMenu.getOptions().toArray();
			
			for (int i=0; i<options.length; i++){
				String text = ((WebElement)options[i]).getAttribute("value");
				
				logFileWriter.write(text); 
				logFileWriter.newLine();
			}//end for
		}//end if
		String housingType = housingMenu.getFirstSelectedOption().getAttribute("value");
		String housing = "Housing-";
		if (housingType == null){
			logFileWriter.write("WARNING - Could not locate the Housing Program dropdown");
			logFileWriter.newLine();
			return housing;
		}
		else {
			logFileWriter.write("Housing Program is determined to be '"+housingType+"'");
			logFileWriter.newLine();
		}//end else - not null - getting the Housing Program
		return housing+housingType;
	}//end adjustHousingCostTypeString method

}//end Class
