package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;

/*
 * This Class has one purpose - to find suitable faculty given well-defined search parameters.
 * 
 * It will access the Faculty Search Page and it will access the Faculty Detail Record.  Both the
 * Faculty Search Page and the Faculty Detail Record will have their own UIControllers.  The methods
 * used here are called by the Faculty Class and the data used for the parameters passed here come
 * from the data held by the Faculty Class.  This Class serves as an intermediate layer between the 
 * UIController Classes (FacultyUIController and the FacultySearchPageUIController) and the Faculty
 * Class.  Whereas the UIControllers are concerned with locators, widgets, and what the UI shows,
 * this Class is concerned with how the data given to it by the Faculty Class can influence the UI-specific
 * actions of the UIController Classes.
 * 
 * There is only one FacultySelector - the methods are static and can be called by one Class at a time.
 * There are two components to the selection of a suitable Faculty member: 
 * (1) Coming up with a list of potential matches
 * (2) Determining whether or not an individual faculty member fulfills the desired criteria.  
 * 
 * Once a Faculty member who fulfills the desired criteria is found, the selection is complete.  
 * Each selection will concern itself with one and only one Faculty member.
 * 
*/
public class FacultySelector {

	public FacultySelector () {
		System.out.println("FacultySelector Class has been instantiated");
	}//end constructor
	
	/*
	 * This method will select a Faculty member based on whether or not the 
	 * Faculty member is expected to have an Administrative Appointment.  
	 * All other search parameters are shared by all searches.
	 * These generic search parameters are contained within this Class.
	 */
	public static String getFacultyName(String adminAppt, String testCaseID, BufferedWriter logFileWriter) throws Exception {
		String name = new String();
		FacultySearchPageUIController.navigateToSearchPage(logFileWriter);
		FacultySearchPageUIController.initiateSearch(logFileWriter, adminAppt);
		name = FacultySearchPageUIController.selectValidFaculty (logFileWriter, testCaseID, adminAppt);
		return name;
	}//end getFacultyName method



}//end Class
