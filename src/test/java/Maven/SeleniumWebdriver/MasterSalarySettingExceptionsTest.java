package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;
import java.util.ArrayList;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MasterSalarySettingExceptionsTest {
	protected static String testName;
	protected static String testCaseNumber;
	protected static String testDescription;
	protected static String testEnv;
	protected static StringBuffer verificationErrors;
	protected BufferedWriter logFileWriter;
	protected SSExcelReader myReader;

	@Before
	public void setUp() throws Exception {
		testName = "MasterSalarySettingExceptionsTest";
		testEnv = "Test";
		testCaseNumber = "063";
		testDescription = "Test of Exceptions for Master SSW";
	}//end setUp method

	@After
	public void tearDown() throws Exception {
		TestFileController.writeTestResults(testEnv, testCaseNumber, testName, testDescription, verificationErrors);
		logFileWriter.newLine();
		logFileWriter.close();
	}//end tearDown method


	@Test
	public void test() throws Exception{
		verificationErrors = new StringBuffer();
		String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		logFileWriter = TestFileController.getLogFileWriter(logFileName);
		logFileWriter.write("The Master Salary Setting Worksheet will be examined ");
		logFileWriter.newLine();
		
		ReportRunner.downloadMasterSSW (ReportRunner.referenceWhichValues, logFileWriter);
		
		//instantiate the reader for the Master Salary Setting SSW
		myReader 
			= new SSExcelReader(TestFileController.directory + TestFileController.MasterSalarySettingReportName, 
					TestFileController.MasterSalarySettingReportHeaderRow);

		if (! checkForIncludedFaculty(logFileWriter)){
			String message = "Couldn't locate included faculty in the SSW";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
		}//end if

		if (! checkForExcludedFaculty(logFileWriter)){
			String message = "Couldn't locate excluded faculty in the SSW";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
		}//end if
	}//end test method

	private boolean checkForIncludedFaculty (BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("**** method checkForIncludedFaculty called ****");
		logFileWriter.newLine();
		
		ArrayList<String[]> exceptionAttributes 
			= HandSOnTestSuite.salarySettingMgrTabUI.getExceptionsWithAttributeValue ("ExcludeOrInclude", "Include", logFileWriter);
		myReader.setRowForColumnNames(TestFileController.MasterSalarySettingReportHeaderRow);
		String[] colNames = {"Name", "Cluster", "Department", "16-17 Description"};
		boolean foundUnexpectedString = false;
		for (int i=0; i<exceptionAttributes.size(); i++){
			String[] UIExceptionAttributes = exceptionAttributes.get(i);
			int rowNum = myReader.getCellRowNum(TestFileController.MasterSalarySettingReportWorksheetName, colNames[0], UIExceptionAttributes[0]);
			for (int j=0; (j<colNames.length) && (rowNum != -1); j++){
				String sheetValue = myReader.getCellData(TestFileController.MasterSalarySettingReportWorksheetName, colNames[j], rowNum);
				String UIvalue = UIExceptionAttributes[j];
				if (j==3){
					UIvalue = UIExceptionAttributes[5];
					if ((sheetValue == null) || (sheetValue.isEmpty()) || (! sheetValue.contains(UIvalue)))
						foundUnexpectedString = true;
				}//end if - it's a Description
				else{
					if ((sheetValue == null) || (sheetValue.isEmpty()) || (! sheetValue.equalsIgnoreCase(UIvalue)))
						foundUnexpectedString = true;
				}//end else - not a Description
				if (foundUnexpectedString){
					logFileWriter.write("ERROR - expected value "+UIvalue+" for column "+colNames[j] 
							+" was not found, actual value found was "+sheetValue);
					logFileWriter.newLine();
				}//end if - unexpected string was found
			}//end inner for loop - iterating through the columns
			if (rowNum == -1){
				logFileWriter.write("Couldn't find the correct row with the faculty name "+UIExceptionAttributes[0]);
				logFileWriter.newLine();
				foundUnexpectedString = true;
			}//end if - couldn't find the correct row
		}//end for loop - iterating through the Inclusion Exceptions
		return (! foundUnexpectedString);
	}//end checkForIncludedFaculty method
	
	private boolean checkForExcludedFaculty (BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("**** method checkForExcludedFaculty called ****");
		logFileWriter.newLine();
		
		ArrayList<String[]> exceptionAttributes 
			= HandSOnTestSuite.salarySettingMgrTabUI.getExceptionsWithAttributeValue ("ExcludeOrInclude", "Exclude", logFileWriter);
		int rowForColumnNames 
			= myReader.getCellRowNum(TestFileController.MasterSalarySettingReportWorksheetName, 
					0, SalarySettingManagementUIController.exclusionSectionHeading);
		myReader.setRowForColumnNames(rowForColumnNames);
		String[] colNames = {"Name", "Cluster", "Department", "Reason", "Description"};
		boolean foundUnexpectedString = false;
		for (int i=0; i<exceptionAttributes.size(); i++){
			String[] UIExceptionAttributes = exceptionAttributes.get(i);
			int rowNum = myReader.getCellRowNum(TestFileController.MasterSalarySettingReportWorksheetName, colNames[0], UIExceptionAttributes[0]);
			for (int j=0; (j<colNames.length) && (rowNum != -1); j++){
				String sheetValue = myReader.getCellData(TestFileController.MasterSalarySettingReportWorksheetName, colNames[j], rowNum);
				String UIvalue = UIExceptionAttributes[j];
				if (j==3){
					UIvalue = UIExceptionAttributes[4];
					if ((sheetValue == null) || (sheetValue.isEmpty()) || (! sheetValue.equalsIgnoreCase(UIvalue)))
						foundUnexpectedString = true;
				}//end if - it's the Reason
				else if (j==4){
					UIvalue = UIExceptionAttributes[5];
					if ((sheetValue == null) || (sheetValue.isEmpty()) || (! sheetValue.contains(UIvalue)))
						foundUnexpectedString = true;
				}//end if - it's the Description
				else{
					if ((sheetValue == null) || (sheetValue.isEmpty()) || (! sheetValue.equalsIgnoreCase(UIvalue)))
						foundUnexpectedString = true;
				}//end else - not a Description or a Reason
			}//end inner for - iterating through the column names	
			if (rowNum == -1){
				logFileWriter.write("Couldn't find the correct row with the faculty name "+UIExceptionAttributes[0]);
				logFileWriter.newLine();
				foundUnexpectedString = true;
			}//end if - couldn't find the correct row

		}//end for loop - iterating through the Exceptions
		return (! foundUnexpectedString);
	}//end checkForIncludedFaculty method
	
}//end Class
