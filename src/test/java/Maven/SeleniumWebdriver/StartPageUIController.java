package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

public class StartPageUIController extends UIController {

	private static HSSFExcelReader myExcelReader;
	private static final String myWorksheetName = "Start Page";

	public StartPageUIController() {
		myExcelReader = TestFileController.fileIn;
	}//end constructor
	
	public void openRecruitmentStandardCostsPage(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("*** method openRecruitmentStandardCostsPage called ***");
		logFileWriter.newLine();
		
		// open the Manage Costs Page
		
		switchToAdministrationPage(logFileWriter);
		logFileWriter.write("Got to the Administration Page");
		logFileWriter.newLine();
		Thread.sleep(3000);
		waitAndClick(By.linkText("Manage Standard Cost"));
		logFileWriter.write("Got the Manage Standard Costs Page Link");
		logFileWriter.newLine();
		Thread.sleep(3000);
		
		boolean gotMngStdCostsLink = UIController.switchWindowByURL(manageStdCostsURL, logFileWriter);
		while(! gotMngStdCostsLink){
			Thread.sleep(3000);
			gotMngStdCostsLink = UIController.switchWindowByURL(manageStdCostsURL, logFileWriter);
			logFileWriter.write("Attempting again to switch to the Manage Standard Costs Page");
			logFileWriter.newLine();
		}//end while loop

		Thread.sleep(3000);
		waitForElementPresent(By.id("offerType"));
		logFileWriter.write("Got the Offer Type dropdown");
		logFileWriter.newLine();
		try{
			new Select(driver.findElement(By.id("offerType"))).selectByVisibleText("Recruitment");
		}//end try
		catch(Exception e){
			logFileWriter.write("Could not select offer type because of Exception "+e.getMessage());
			logFileWriter.newLine();
		}//end catch
	}//end openStandardCostsPage method
	
	public void openNewFundingCommitment(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("*** method openNewFundingCommitment called ***");
		logFileWriter.newLine();
		if (! UIController.switchWindowByURL(startPage, logFileWriter)){
			Thread.sleep(2000);
			if (! UIController.switchWindowByURL(administrationURL, logFileWriter)){
				String message = "Couldn't switch to the administration page - exiting;\n ";
				logFileWriter.write(message);
				logFileWriter.newLine();
				System.out.println(message);
				return;
			}
			else
				waitAndClick(By.linkText("Operations"));
		}//end if		
		Thread.sleep(3000);
		String locator = getLocatorByName("NewFundingCommitmentLink", logFileWriter);
		waitAndClick(By.linkText(locator));
		UIController.switchWindowByURL(UIController.newFundingCommitmentURL, logFileWriter);

		//now, make sure that we can get to the tab that holds the new Funding Commitment
		boolean gotNewFC = false;
		while (! gotNewFC){
			//do something to verify we have the new FC and then verify that we are on the correct page
			By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Page", "CommitmentNumberLabel", logFileWriter);
			if (UIController.isElementPresent(by)){
				String text = UIController.driver.findElement(by).getText();
				if ((text != null) && (! text.isEmpty())){
					logFileWriter.write("Got a Funding Commitment ID field - value is "+text);
					logFileWriter.newLine();
					
					Thread.sleep(2000);
					
					if (UIController.driver.getCurrentUrl().contains("processAction=CLOSE")){
						logFileWriter.write("We've reached a window that should be closed - trying to get the notification dialog");
						logFileWriter.newLine();
						Thread.sleep(2000);
						if (UIController.switchWindowByURL(UIController.notificationDialogURL, logFileWriter)){
							logFileWriter.write("Managed to get the notification dialog");
							logFileWriter.newLine();
							Thread.sleep(2000);
							UIController.driver.findElement(By.id("mybutton")).click();
							logFileWriter.write("Successfully dismissed the notification dialog");
							logFileWriter.newLine();
						}//end if - we got the notification dialog - so far so good
						else{
							String message = "ERROR - could not get the notification dialog after saving an update; \n";
							logFileWriter.write(message);
							logFileWriter.newLine();
							System.out.println(message);
						}//end else - error
					}//end if
					else{
						driver.findElement(By.id("buttonSubmit")).click();
					}
					UIController.switchWindowByURL(UIController.startPage, logFileWriter);
					Thread.sleep(5000);
					logFileWriter.write("Got back to the Start Page");
					logFileWriter.newLine();
					try{
						UIController.switchWindowByURL(UIController.newFundingCommitmentURL, logFileWriter);
					}
					catch(Exception e){
						logFileWriter.write("Couldn't switch to the new URL because of Exception "+e.getMessage());
						logFileWriter.newLine();
						Thread.sleep(5000);
						logFileWriter.write("Trying again....");
						logFileWriter.newLine();
						if (! UIController.switchWindowByURL(UIController.newFundingCommitmentURL, logFileWriter)){
							logFileWriter.write("Couldn't switch back to the new Funding Commitment URL - exiting now");
							logFileWriter.newLine();
							return;
						}
					}
				}//end inner if - this is a Funding Commitment that already exists
				else gotNewFC = true;
			}//end if
			else{
				logFileWriter.write("Funding Commitment ID field is empty - we have a new Funding Commitment");
				logFileWriter.newLine();
				gotNewFC = true;
			}//end else
		}//end while
	}//end openNewFundingCommitment method
	
	public void openNewSearchAuthorization(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("*** method openNewSearchAuthorization called ***");
		logFileWriter.newLine();
		boolean switchedToStartPage = false;
		while (! switchedToStartPage){
			if (UIController.switchWindowByURL(administrationURL, logFileWriter))
				waitAndClick(By.linkText("Operations"));
			else 
				switchedToStartPage = UIController.switchWindowByURL("showDocumentsTab.do", logFileWriter);
		}//end if
		String locator = getLocatorByName("NewSearchAuthorizationLink", logFileWriter);
		waitAndClick(By.linkText(locator));
		UIController.switchWindowByURL(UIController.newSearchAuthorizationURL, logFileWriter);
		
		boolean gotNewSA = false;
		while (! gotNewSA){
			By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SearchAuthID", logFileWriter);
			if (UIController.isElementPresent(by)){
				String text = UIController.driver.findElement(by).getText();
				if ((text != null) && (! text.isEmpty())){
					logFileWriter.write("Got a Search Authorization ID field - value is "+text);
					logFileWriter.newLine();
					driver.findElement(By.id("buttonClose")).click();
					UIController.switchWindowByURL(UIController.startPage, logFileWriter);
					UIController.switchWindowByURL(UIController.newSearchAuthorizationURL, logFileWriter);
				}//end if - it's a valid entry
				else gotNewSA = true;
			}//end if
			else{
				logFileWriter.write("Search Authorization ID field is empty - we have a new SA");
				logFileWriter.newLine();
				gotNewSA = true;
			}//end else
		}//end while

	}//end openNewSearchAuthorization method
	
	private String getLocatorByName(String name, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method getLocatorByName called with locator name "+ name +" ***");
		logFileWriter.newLine();
		int rowNum = myExcelReader.getCellRowNum(myWorksheetName, "Name", name);
		if (rowNum == -1){
			logFileWriter.write("Could not find the row with locator name "+name);
			logFileWriter.newLine();
			return new String();
		}//end if - we don't go any further if we don't get the row number
		String locator = myExcelReader.getCellData(myWorksheetName, "Locator", rowNum);
		logFileWriter.write("Locator "+locator +" found on row "+rowNum);
		if ((locator == null) ||(locator.isEmpty())){
			logFileWriter.write("WARNING!! Could not locate the locator for locator name "+name
					+" on row "+rowNum);	
			logFileWriter.newLine();
		}//end if - we can't get the locator even though we got the right row number
		return locator;
	}//end getLocatorByName method

}
