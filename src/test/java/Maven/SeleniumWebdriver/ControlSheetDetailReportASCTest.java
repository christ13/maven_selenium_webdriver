package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.TreeMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ControlSheetDetailReportASCTest extends ASCTest {

	@Before
	public void setUp() throws Exception {
		myExcelReader = TestFileController.ControlSheetReportSuite;
		rowForColumnNames = TestFileController.ControlSheetDetailReportColumnHeaderRow;
		mySheetName = TestFileController.ControlSheetDetailReportWorksheetName;
		verificationErrors = new StringBuffer();
		testName = "ControlSheetDetailReportASCTest";
		testEnv = "Test";
		testCaseNumber = "022";
		testDescription = "ASC Test of Control Sheet Detail Report";
		logFileName = "Control Suite Detail Report ASC Test.txt";
		logFileWriter = TestFileController.getLogFileWriter(logDirectoryName  +"/" + logFileName);
		criteria = new TreeMap<String, String>();
		/*
		//we put these criteria in to test the test runner, normally these are absent from this test
		criteria.put(ExpectedASCData.StringAttributeKeys[ExpectedASCData.PoolIndex], 
				ASCTest.sourcePools[ASCTest.RegularMeritPoolIndex]);
		criteria.put(ExpectedASCData.StringAttributeKeys[ExpectedASCData.DescriptionIndex], 
				"Description entered for ASC");
		criteria.put(ASCFacultyValues.StringAttributeKeys[ASCFacultyValues.ClusterIndex], 
				"Humanities and Arts Cluster");
		criteria.put(ASCFacultyValues.StringAttributeKeys[ASCFacultyValues.RankIndex], 
				"Associate Professor");
		*/
		excludeZeroDOShare = false;
		expectedStringAttributeNames = new String[9];
		actualStringAttributeNames = new String[9];

		expectedStringAttributeNames[0] = actualStringAttributeNames[0] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.FacultyIndex];
		expectedStringAttributeNames[1] = actualStringAttributeNames[1] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.DepartmentIndex];
		expectedStringAttributeNames[2] = actualStringAttributeNames[2] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.StageIndex];
		expectedStringAttributeNames[3] = actualStringAttributeNames[3] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.SubCategoryIndex];
		expectedStringAttributeNames[4] = actualStringAttributeNames[4] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.PoolIndex];
		//Description attribute has a bug - HANDSON-3269 - associated with it.  It's not copied from the ASC to the Control Sheet Detail Report.
		expectedStringAttributeNames[5] = actualStringAttributeNames[5] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.DescriptionIndex];
		expectedStringAttributeNames[6] = actualStringAttributeNames[6] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.NotesIndex];
		expectedStringAttributeNames[7] = actualStringAttributeNames[7] = ASCFacultyValues.StringAttributeKeys[ASCFacultyValues.ClusterIndex];
		expectedStringAttributeNames[8] = actualStringAttributeNames[8] = ASCFacultyValues.StringAttributeKeys[ASCFacultyValues.RankIndex];
		
		expectedAmountAttributeNames = new String[6];
		actualAmountAttributeNames = new String[6];
		expectedAmountAttributeNames[0] = ExpectedASCData.amountAttributeKeys[ExpectedASCData.FTEPercentIndex];
		expectedAmountAttributeNames[1] = ExpectedASCData.amountAttributeKeys[ExpectedASCData.CurrentSalaryIndex];
		expectedAmountAttributeNames[2] = ExpectedASCData.amountAttributeKeys[ExpectedASCData.FTEAdjustedSalaryIndex];
		expectedAmountAttributeNames[3] = ExpectedASCData.amountAttributeKeys[ExpectedASCData.FTEAdjustedDOShareIndex];
		expectedAmountAttributeNames[4] = ExpectedASCData.amountAttributeKeys[ExpectedASCData.FTEAdjustedDepartmentShareIndex];
		expectedAmountAttributeNames[5] = ExpectedASCData.amountAttributeKeys[ExpectedASCData.FTEAdjustedTotalAmountIndex];
		
		actualAmountAttributeNames[0] = "Dept FTE";
		actualAmountAttributeNames[1] = "Current 100% Salary";
		actualAmountAttributeNames[2] = "Current FTE Adj Salary";
		actualAmountAttributeNames[3] = "FTE Adjusted DO Share Amount";
		actualAmountAttributeNames[4] = "FTE Adjusted Department Share Amount";
		actualAmountAttributeNames[5] = "FTE Adj Total Commitment";
		
		// in this case, there is no summary information
		expectedSummaryNames = new String[0];
		actualSummaryNames = new String[0];
		expectedSummaryCriteria = new TreeMap<String, ArrayList<String>>();
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void test() throws Exception{
		super.test();
	}//end test() method 

}
