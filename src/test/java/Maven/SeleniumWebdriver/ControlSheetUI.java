package Maven.SeleniumWebdriver;



import java.io.BufferedWriter;
import java.math.BigDecimal;
import java.text.NumberFormat;

import java.util.HashMap;
import java.util.LinkedList;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class ControlSheetUI extends UIController {
	
	public static final String UITitleColumn = "0";
	public static final String UITotalKnownDiscreteNumberColumn = "1";
	public static final String UITotalKnownDollarsColumn = "2";
	
	public static final String ControlSheetTabName = "Control Sheet";
	private static int[] numberOfFacultyWithAPType = new int[ReferenceValues.secondDimensionSize];
	private static BigDecimal[] dollarAmountsForAPType = new BigDecimal[ReferenceValues.secondDimensionSize];
	public static final int ControlSheetLocatorColumn = 1;
	public static final int ControlSheetUIColumnColumn = 2;
	public static final int ControlSheetUIRowColumn = 3;
	public static final int ControlSheetFirstAPValueRow = 59;
	public static final int ControlSheetLastAPValueRow = 65;
	protected static HSSFExcelReader myExcelReader;
	protected static int numberOfASCs = 0;
	public static boolean ExcelReaderToOutputFile = false;
	public static boolean locatorsToOutputFile = false;

	// all of these pertain to the modification of the Salary Setting Increase Percentages
	public static final String[] originalSalaryIncreaseValues = {"10.00", "4.00", "3.00", 
		"4.50", "3.50", "2.00", "9.70", "10.29", "4.50"};
	public static final String[] SalaryIncreaseLabels = {"MeritPoolPercentageAmount", 
		"DepartmentFacultyPercentageAmount", "TerminalYearPercentageAmount", 
		"DepartmentChairPercentageAmount", "SrAssociateDeansPercentageAmount", 
		"MarketPoolPercentageAmount", "GenderPoolPercentageAmount", "TestPoolPercentageAmount", 
		"AlbertEustacePeasmarchFundPercentageAmount"};
	public static final String[] alteredSalaryIncreaseValues = {"12.00", "5.00", "4.00", 
		"7.50", "7.50", "4.00", "9.70", "12.00", "6.50"};
	
	private static HSSFExcelReader locators = TestFileController.locators;
	public static final String locatorFileSheetName = "Control Sheet";

	public ControlSheetUI() {
		myExcelReader = TestFileController.locators;

		for (int i=0; i< numberOfFacultyWithAPType.length; i++){
			numberOfFacultyWithAPType[i]=0;
			dollarAmountsForAPType[i] = new BigDecimal(0);
		}//end for loop
	}//end constructor
	
	public static void gotoControlSheetUITab() throws Exception{
		  waitAndClick(By.id("control_a"));
	}
	
	
	public static void setNumberOfFacultyWithAPType(String APType, int numberOfFaculty){
		  System.out.println("method setnumberOfFacultyWithAPType has been called with input parameter APType " + APType);
		  String[] labels = HandSOnTestSuite.referenceValues.getLabels();
		  if (APType.equalsIgnoreCase(labels[ReferenceValues.Reappointment])){
			  numberOfFacultyWithAPType[ReferenceValues.Reappointment] = numberOfFaculty;
		  }
		  else if (APType.equalsIgnoreCase(labels[ReferenceValues.PromotionToNonTenure])){
			  numberOfFacultyWithAPType[ReferenceValues.PromotionToNonTenure] = numberOfFaculty;
		  }
		  else if (APType.equalsIgnoreCase(labels[ReferenceValues.PromotionToContinuingTerm])){
			  numberOfFacultyWithAPType[ReferenceValues.PromotionToContinuingTerm] = numberOfFaculty;
		  }
		  else if (APType.equalsIgnoreCase(labels[ReferenceValues.PromotionToTenure])){
			  numberOfFacultyWithAPType[ReferenceValues.PromotionToTenure] = numberOfFaculty;
		  }
		  else if (APType.equalsIgnoreCase(labels[ReferenceValues.PromotionToFull])){
			  numberOfFacultyWithAPType[ReferenceValues.PromotionToFull] = numberOfFaculty;
		  }
		  else if (APType.equalsIgnoreCase(labels[ReferenceValues.RemovalOfSubjectToPhD])){
			  numberOfFacultyWithAPType[ReferenceValues.RemovalOfSubjectToPhD] = numberOfFaculty;
		  }
		  else System.out.println("Could not match '" +APType + "' with any existing ReferenceValue label");
	}
	
	public static int[] getNumberOfFacultyWithAPTypes(){
		return numberOfFacultyWithAPType;
	}
	
	public static int getNumberOfFacultyForAPType(String APType){
		  System.out.println("method getnumberOfFacultyWithAPType has been called with input parameter APType " + APType);
		  String[] labels = HandSOnTestSuite.referenceValues.getLabels();
		  if (APType.equalsIgnoreCase(labels[ReferenceValues.Reappointment])){
			  return numberOfFacultyWithAPType[ReferenceValues.Reappointment];
		  }
		  else if (APType.equalsIgnoreCase(labels[ReferenceValues.PromotionToNonTenure])){
			  return numberOfFacultyWithAPType[ReferenceValues.PromotionToNonTenure];
		  }
		  else if (APType.equalsIgnoreCase(labels[ReferenceValues.PromotionToContinuingTerm])){
			  return numberOfFacultyWithAPType[ReferenceValues.PromotionToContinuingTerm];
		  }
		  else if (APType.equalsIgnoreCase(labels[ReferenceValues.PromotionToTenure])){
			  return numberOfFacultyWithAPType[ReferenceValues.PromotionToTenure];
		  }
		  else if (APType.equalsIgnoreCase(labels[ReferenceValues.PromotionToFull])){
			  return numberOfFacultyWithAPType[ReferenceValues.PromotionToFull];
		  }
		  else if (APType.equalsIgnoreCase(labels[ReferenceValues.RemovalOfSubjectToPhD])){
			  return numberOfFacultyWithAPType[ReferenceValues.RemovalOfSubjectToPhD];
		  }
		  else{
			  System.out.println("Could not match '" +APType + "' with any existing ReferenceValue label");
			  return 0;
		  }
		
	}

	public static void setDollarAmountsForAPType(String APType, BigDecimal dollarAmount){
		  System.out.println("method setDollarAmountsForAPType has been called with input parameter APType " + APType);
		  String[] labels = HandSOnTestSuite.referenceValues.getLabels();
		  if (APType.equalsIgnoreCase(labels[ReferenceValues.Reappointment])){
			  dollarAmountsForAPType[ReferenceValues.Reappointment] = dollarAmount;
		  }
		  else if (APType.equalsIgnoreCase(labels[ReferenceValues.PromotionToNonTenure])){
			  dollarAmountsForAPType[ReferenceValues.PromotionToNonTenure] = dollarAmount;
		  }
		  else if (APType.equalsIgnoreCase(labels[ReferenceValues.PromotionToContinuingTerm])){
			  dollarAmountsForAPType[ReferenceValues.PromotionToContinuingTerm] = dollarAmount;
		  }
		  else if (APType.equalsIgnoreCase(labels[ReferenceValues.PromotionToTenure])){
			  dollarAmountsForAPType[ReferenceValues.PromotionToTenure] = dollarAmount;
		  }
		  else if (APType.equalsIgnoreCase(labels[ReferenceValues.PromotionToFull])){
			  dollarAmountsForAPType[ReferenceValues.PromotionToFull] = dollarAmount;
		  }
		  else if (APType.equalsIgnoreCase(labels[ReferenceValues.RemovalOfSubjectToPhD])){
			  dollarAmountsForAPType[ReferenceValues.RemovalOfSubjectToPhD] = dollarAmount;
		  }
		  else System.out.println("Could not match '" +APType + "' with any existing ReferenceValue label");
	}
	
	public static boolean checkExpectedVersusActualDollarAmounts(String APType, BufferedWriter logFileWriter) throws Exception{
		BigDecimal expected = getDollarAmountForAPType(APType);
		BigDecimal actual = getDollarAmountInGUIForAPType(APType, logFileWriter);
		logFileWriter.write("Expected value: " +expected);
		logFileWriter.write(", actual value: " + actual);
		logFileWriter.newLine();
		return (expected.intValue() == actual.intValue());
	}

	public static boolean checkExpectedVersusActualDiscreteFacultyNumbers(String APType, BufferedWriter logFileWriter) throws Exception{
		int expected = getNumberOfFacultyForAPType(APType);
		int actual = getNumberOfDiscreteFacultyInGUIForAPType(APType, logFileWriter);
		logFileWriter.write("Expected value: " +expected);
		logFileWriter.write(", actual value: " + actual);
		logFileWriter.newLine();
		return (expected == actual);
	}

	/*
	 * In order to do this, we have to do the following:
	 * Step 1 - get the correct row corresponding to this AP Type
	 * Step 2 - get the locator in the correct row for this AP Type
	 * Step 3 - using the locator, get the dollar amount in the GUI for this AP type
	 * Step 4 - return the dollar amount as a BigDecimal value.
	 */
	public static BigDecimal getDollarAmountInGUIForAPType(String APType, BufferedWriter logFileWriter) throws Exception{
		String row = getUIRowForAPType(APType);
		if (row.length() == 0){
			String message = "Couldn't locate the AP Type - returning zero:";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			return new BigDecimal(0);
		}
		String locator = getLocatorForUIRowAndUIColumn(row, UITotalKnownDollarsColumn, logFileWriter);
		if (locator.length() == 0){
			String message = "Couldn't locate the locator for this AP Type - returning zero:";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			return new BigDecimal(0);
		}
		return getDollarAmountForLocator(locator, "xpath", logFileWriter);
	}
	
	/*
	 * This is very similar to the method getDollarAmountInGUIForAPType above, except
	 * that it tries to find the number of discrete faculty members and does the 
	 * comparison there.
	 */
	public static int getNumberOfDiscreteFacultyInGUIForAPType(String APType, BufferedWriter logFileWriter) throws Exception{
		String row = getUIRowForAPType(APType);
		if (row.length() == 0){
			String message = "Couldn't locate the AP Type - returning zero:";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			return 0;
		}
		String locator = getLocatorForUIRowAndUIColumn(row, UITotalKnownDiscreteNumberColumn, logFileWriter);
		if (locator.length() == 0){
			String message = "Couldn't locate the locator for this AP Type - returning zero:";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			return 0;
		}
		return getDiscreteFacultyAmountForLocator(locator, logFileWriter);
	}
	/*
	 * From the first AP Value row to the last, we simply iterate through, 
	 * extracting the locator and then using the locator to read the UI for the 
	 * title text.  Once we have title text that matches, we return the corresponding
	 * row.
	 */
	private static String getUIRowForAPType(String desiredAPType) throws Exception{
		int row = ControlSheetFirstAPValueRow;
		String UIRow = new String();
		boolean match = false;
		while ((! match) && (row <= ControlSheetLastAPValueRow)){
			String locator = myExcelReader.getCellData(ControlSheetTabName, ControlSheetLocatorColumn, row);
			String actualAPType = driver.findElement(By.xpath(locator)).getText();
			match = actualAPType.equalsIgnoreCase(desiredAPType);
			if (! match) row++;
		}
		if (! match){
			System.out.println("Couldn't find matching AP Type - returning blank");
			return new String();
		}
		else {
			UIRow = myExcelReader.getCellData(ControlSheetTabName, ControlSheetUIRowColumn, row);
			System.out.println("Found match - returning UI Row of " + UIRow);
		}
		return UIRow;
	}
	/*
	 * for this method, we will need to use the HSSFExcelReader.getMultipleValuesInRows method
	 * Here's the method declaration:
	 * public LinkedList<HashMap<String, String>> getMultipleValuesInRows (String sheetName, 
	 * 				HashMap<String, String> searchCriteria, String[] columnNames, 
	 * 				BufferedWriter logFileWriter) throws Exception{
	 */
	private static String getLocatorForUIRowAndUIColumn(String UIRow, String UIColumn, BufferedWriter logFileWriter) throws Exception{
		StringBuffer verificationErrors = new StringBuffer();
		HashMap<String, String> searchCriteria = new HashMap<String, String>();
		searchCriteria.put("Row", UIRow);
		searchCriteria.put("Column", UIColumn);
		String[] columnNames = {"Locator"};
		LinkedList<HashMap<String, String>> locatorList = myExcelReader.getMultipleValuesInRows(ControlSheetTabName, searchCriteria, columnNames, logFileWriter);
		if (locatorList.size() == 0){
			verificationErrors.append("*** TEST FAILED *** search in the file for locator with row '" + UIRow 
					+"' and column '" + UIColumn + "' yielded no results");
			//System.out.println(verificationErrors.toString());
			TestFileController.writeToLogFile(verificationErrors.toString(), logFileWriter);
			return new String();
		}
		else if (locatorList.size() != 1){
			String warning = "*** WARNING *** There is more than one row with this search criteria - please adjust search criteria";
			//System.out.println(warning);
			TestFileController.writeToLogFile(warning, logFileWriter);
			return new String();
		}
		//we're going to assume that there's always one and only one HashMap returned in the LinkedList
		else{
			HashMap<String, String> locatorHashMap = (HashMap<String, String>)locatorList.get(0);
			String locator = locatorHashMap.get(columnNames[0]);
			verificationErrors.append("locator being returned is as follows: " + locator);
			return locator;
		}//end else
	}//end getLocatorForUIRowAndUIColumn method
	
	public static double getDoubleFromString(String raw) throws Exception{
		double returned = 0;
		if (raw.startsWith("$"))
			raw = raw.substring(1);
		if (raw.endsWith("%"))
			raw = raw.replaceAll("%", "");
		try{
			returned = ((Number)NumberFormat.getNumberInstance(java.util.Locale.US).parse(raw)).doubleValue();
		}
		catch(Exception e){e.getLocalizedMessage();}
		return returned;
	}
	
	public static void compareValues(String UIValue, String sheetValue, StringBuffer verificationErrors, BufferedWriter logFileWriter) throws Exception{
		compareValues(getDoubleFromString(UIValue), getDoubleFromString(sheetValue), verificationErrors, logFileWriter);
	}//end compareValues method
	
	public static StringBuffer compareValues(double actualValue, double expectedValue, StringBuffer verificationErrors, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method compareValues called with UI value "+actualValue
				+" and Excel sheet value "+expectedValue);
		logFileWriter.newLine();
		boolean testFails = (! ControlSheetUI.isApproximatelyEqual(actualValue, expectedValue));
				//((Number)NumberFormat.getNumberInstance(java.util.Locale.US).parse(UIValue)).doubleValue()
		if (testFails){
			String message = "MISMATCH! expected value is "+expectedValue
					+", but actual value found is "+actualValue;
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
		}//end if - test has failed
		else{
			logFileWriter.write("UI Value and value in Excel sheet are both "+expectedValue+", as expected");
			logFileWriter.newLine();
		}
		return verificationErrors;
	}//end compareValues method


	
	private static BigDecimal getDollarAmountForLocator(String locator, String locatorType, BufferedWriter logFileWriter) throws Exception{
		BigDecimal dollarAmt = new BigDecimal(0);
		By by;
		if (locatorType.equalsIgnoreCase("xpath"))
			by = By.xpath(locator);
		else by = By.id(locator);
		//System.out.println("locator being worked with is " + locator);
		String actual;
		WebElement element = driver.findElement(by);
		//System.out.println(element.toString());
		//try to get the text first
		actual = element.getText();
		//if the text is empty, it may be a text field, so try to get the "value"
		if (actual.isEmpty())
			actual=element.getAttribute("value");
		//if it's still empty, give it a value of zero.
		try{
			if (actual.isEmpty()) 
				actual=new String("0");
		}
		catch (NullPointerException e){ actual=new String("0");}
		
		if (actual.startsWith("$")) actual=actual.substring(1);

		String message = "String value derived from locator is " + actual;

		try{
			actual = ((Long)NumberFormat.getNumberInstance(java.util.Locale.US).parse(actual)).toString();
			dollarAmt = new BigDecimal(actual);
		}
		catch(NumberFormatException e){
			message = message.concat(", and it is not parseable to a number - returning zero");
			System.out.println(message);
			TestFileController.writeToLogFile(message, logFileWriter);
			return new BigDecimal(0);
		}
		//System.out.println(message);
		TestFileController.writeToLogFile(message, logFileWriter);
		return dollarAmt;
	}
	
	private static int getDiscreteFacultyAmountForLocator(String locator, BufferedWriter logFileWriter) throws Exception{
		BigDecimal numberOfDiscreteFaculty = new BigDecimal(0);
		String actual = driver.findElement(By.xpath(locator)).getText();
		actual = ((Long)NumberFormat.getNumberInstance(java.util.Locale.US).parse(actual)).toString();
		String message = "String value derived from locator is " + actual;
		try{
			numberOfDiscreteFaculty = new BigDecimal(actual);
		}
		catch(NumberFormatException e){
			message = message.concat(", and it is not parseable to a number - returning zero");
			System.out.println(message);
			TestFileController.writeToLogFile(message, logFileWriter);
			return 0;
		}
		System.out.println(message);
		TestFileController.writeToLogFile(message, logFileWriter);
		return numberOfDiscreteFaculty.intValue();
	}
	public static BigDecimal[] getDollarAmountsForAPType(){
		return dollarAmountsForAPType;
	}
	
	public static BigDecimal getSalaryBaseForPool (String poolName, BufferedWriter logFileWriter) throws Exception{
		String salaryBaseLocatorName = new String();
		TestFileController.writeToLogFile(poolName + " Salary Base ", logFileWriter);
		if (poolName.equalsIgnoreCase(ASCTest.sourcePools[ASCTest.MarketPoolIndex])){
			salaryBaseLocatorName = "MarketPoolSalaryBase";
		}//end if
		else{
			TestFileController.writeToLogFile("Cannot identify locator name", logFileWriter);
		}
		int locatorRowNum = 0;
		if (! salaryBaseLocatorName.isEmpty()){
			locatorRowNum = myExcelReader.getCellRowNum(ControlSheetTabName, 0, salaryBaseLocatorName);
			TestFileController.writeToLogFile("Row number with the locator is " + Integer.valueOf(locatorRowNum), logFileWriter);
		}
		String locator = myExcelReader.getCellData(ControlSheetTabName, ControlSheetLocatorColumn, locatorRowNum);
		TestFileController.writeToLogFile("locator found in locator file is " + locator, logFileWriter);
		BigDecimal amount = getDollarAmountForLocator(locator, "xpath", logFileWriter);
		TestFileController.writeToLogFile("Salary Base amount for " + poolName + " is " + amount.toPlainString(), logFileWriter);
		return amount;
	}
	
	public static BigDecimal getSalaryBaseDollarSumForPool(String poolName, BufferedWriter logFileWriter) throws Exception{
		String salaryBaseDollarsLocatorBase = new String();
		String baseName = poolName.replaceAll("\\s+","");
		String suffix = "SalaryBaseDollarsColumnBase";
		if (poolName.equalsIgnoreCase(ASCTest.sourcePools[ASCTest.RegularMeritPoolIndex])){
			TestFileController.writeToLogFile("Regular Merit Pool doesn't get summed up in the UI", logFileWriter);
		}
		else{
			salaryBaseDollarsLocatorBase = baseName + suffix;
			TestFileController.writeToLogFile(poolName +" Salary Base will be summed up - locator name is " + salaryBaseDollarsLocatorBase, logFileWriter);
		}
		int locatorRowNum = 0;
		if (! salaryBaseDollarsLocatorBase.isEmpty()){
			locatorRowNum = myExcelReader.getCellRowNum(ControlSheetTabName, 0, salaryBaseDollarsLocatorBase);
			TestFileController.writeToLogFile("Row number with the locator is " + Integer.valueOf(locatorRowNum), logFileWriter);
			if (locatorRowNum == -1) return new BigDecimal(0);
		}
		else return new BigDecimal(0);
		String locator = myExcelReader.getCellData(ControlSheetTabName, ControlSheetLocatorColumn, locatorRowNum);
		TestFileController.writeToLogFile("locator found in locator file is " + locator, logFileWriter);
		BigDecimal sum = new BigDecimal(0);
		BigDecimal amount = getDollarAmountForLocator(locator, "xpath", logFileWriter);
		int rowNum = 2;
		while(amount.intValue() != -1){
			TestFileController.writeToLogFile("Amount added to sum is " + amount.toPlainString(), logFileWriter);
			sum = sum.add(amount);
			amount = getSalaryBaseDollarAmount(rowNum, locator, "xpath", logFileWriter);
			rowNum++;
		}
		return sum;
	}
	
	private static BigDecimal getSalaryBaseDollarAmount(int rowNum, String locatorBase, String locatorType, BufferedWriter logFileWriter) throws Exception{
		BigDecimal salaryBaseDollarAmount = new BigDecimal("-1");
		String locator = locatorBase.replaceFirst("tr", "tr[" + Integer.toString(rowNum) + "]");
		TestFileController.writeToLogFile("locator to be searched for is " + locator, logFileWriter);
		
		if (isElementPresent(By.xpath(locator)))
			salaryBaseDollarAmount = getDollarAmountForLocator(locator, locatorType, logFileWriter);
		return salaryBaseDollarAmount;
	}
	
	public static BigDecimal getDollarAmountForAPType(String APType){
		  System.out.println("method getDollarAmountForAPType has been called with input parameter APType " + APType);
		  String[] labels = HandSOnTestSuite.referenceValues.getLabels();
		  if (APType.equalsIgnoreCase(labels[ReferenceValues.Reappointment])){
			  return dollarAmountsForAPType[ReferenceValues.Reappointment];
		  }
		  else if (APType.equalsIgnoreCase(labels[ReferenceValues.PromotionToNonTenure])){
			  return dollarAmountsForAPType[ReferenceValues.PromotionToNonTenure];
		  }
		  else if (APType.equalsIgnoreCase(labels[ReferenceValues.PromotionToContinuingTerm])){
			  return dollarAmountsForAPType[ReferenceValues.PromotionToContinuingTerm];
		  }
		  else if (APType.equalsIgnoreCase(labels[ReferenceValues.PromotionToTenure])){
			  return dollarAmountsForAPType[ReferenceValues.PromotionToTenure];
		  }
		  else if (APType.equalsIgnoreCase(labels[ReferenceValues.PromotionToFull])){
			  return dollarAmountsForAPType[ReferenceValues.PromotionToFull];
		  }
		  else if (APType.equalsIgnoreCase(labels[ReferenceValues.RemovalOfSubjectToPhD])){
			  return dollarAmountsForAPType[ReferenceValues.RemovalOfSubjectToPhD];
		  }
		  else{
			  System.out.println("Could not match '" +APType + "' with any existing ReferenceValue label");
			  return new BigDecimal(0);
		  }
	}//end method
	
	public static int getNumberOfASCsInGUI(BufferedWriter logFileWriter) throws Exception{
		TestFileController.writeToLogFile("*** method getNumberOfASCsInGUI is running ***", logFileWriter);
		//first, wait for the ASC header information to be present
		waitForElementPresent(By.xpath("//table[2]/tbody/tr[3]/td/table/tbody/tr/td/font/b"));
		Thread.sleep(2000);//wait a couple of seconds, just in case
		int numberOfASCs = 0;
		boolean ascIsPresent = true;
		while(ascIsPresent){
			try{//there's always a name, even if the name is "Unknown", as long as there's an ASC
				String ascLocator = getASCNameForIndex(numberOfASCs);
				TestFileController.writeToLogFile("Using locator value " + ascLocator, logFileWriter);
				driver.findElement(By.xpath(ascLocator));
				numberOfASCs++;
			}//end try clause
			catch(NoSuchElementException e){
				TestFileController.writeToLogFile("No more ASC's are present - returning a value of " + numberOfASCs, logFileWriter);
				ascIsPresent = false;
				return numberOfASCs;
			}//end catch clause
		}//end while
		return numberOfASCs;
	}//end getNumberOfASCsInGUI method
	
	public static HashMap<String, String> getASCStringAttributes(int index, BufferedWriter logFileWriter) throws Exception{
		TestFileController.writeToLogFile("*** method getASCStringAttributes has been called ***", logFileWriter);
		HashMap<String, String> stringValues = new HashMap<String, String>();
		String attributeValue = driver.findElement(By.xpath(getASCNameForIndex(index))).getText();
		stringValues.put(ExpectedASCData.StringAttributeKeys[ExpectedASCData.FacultyIndex], 
				attributeValue);
		TestFileController.writeToLogFile("Now putting name-value combination " 
				+ ExpectedASCData.StringAttributeKeys[ExpectedASCData.FacultyIndex]
				+" - " + attributeValue	+" into the HashMap", logFileWriter);
		attributeValue = getASCDeptForIndex(index);
		stringValues.put(ExpectedASCData.StringAttributeKeys[ExpectedASCData.DepartmentIndex], 
				attributeValue);
		TestFileController.writeToLogFile("Now putting name-value combination " 
				+ ExpectedASCData.StringAttributeKeys[ExpectedASCData.DepartmentIndex]
				+" - " + attributeValue	+" into the HashMap", logFileWriter);
		attributeValue = getASCStageForIndex(index);
		stringValues.put(ExpectedASCData.StringAttributeKeys[ExpectedASCData.StageIndex], 
				attributeValue);
		TestFileController.writeToLogFile("Now putting name-value combination " 
				+ ExpectedASCData.StringAttributeKeys[ExpectedASCData.StageIndex]
				+" - " + attributeValue	+" into the HashMap", logFileWriter);
		attributeValue = getASCCategoryForIndex(index);
		stringValues.put(ExpectedASCData.StringAttributeKeys[ExpectedASCData.CategoryIndex], 
				attributeValue);
		TestFileController.writeToLogFile("Now putting name-value combination " 
				+ ExpectedASCData.StringAttributeKeys[ExpectedASCData.CategoryIndex]
				+" - " + attributeValue	+" into the HashMap", logFileWriter);
		attributeValue = getASCSubCategoryForIndex(index);
		stringValues.put(ExpectedASCData.StringAttributeKeys[ExpectedASCData.SubCategoryIndex], 
				attributeValue);
		TestFileController.writeToLogFile("Now putting name-value combination " 
				+ ExpectedASCData.StringAttributeKeys[ExpectedASCData.SubCategoryIndex]
				+" - " + attributeValue	+" into the HashMap", logFileWriter);
		attributeValue = getASCPoolForIndex(index);
		stringValues.put(ExpectedASCData.StringAttributeKeys[ExpectedASCData.PoolIndex], 
				attributeValue);
		TestFileController.writeToLogFile("Now putting name-value combination " 
				+ ExpectedASCData.StringAttributeKeys[ExpectedASCData.PoolIndex]
				+" - " + attributeValue	+" into the HashMap", logFileWriter);
		attributeValue = getASCDescriptionForIndex(index, logFileWriter);
		stringValues.put(ExpectedASCData.StringAttributeKeys[ExpectedASCData.DescriptionIndex], 
				attributeValue);
		TestFileController.writeToLogFile("Now putting name-value combination " 
				+ ExpectedASCData.StringAttributeKeys[ExpectedASCData.DescriptionIndex]
				+" - " + attributeValue +" into the HashMap", logFileWriter);
		attributeValue = getASCNotesForIndex(index, logFileWriter);
		stringValues.put(ExpectedASCData.StringAttributeKeys[ExpectedASCData.NotesIndex], 
				attributeValue);
		TestFileController.writeToLogFile("Now putting name-value combination " 
				+ ExpectedASCData.StringAttributeKeys[ExpectedASCData.NotesIndex]
				+" - " + attributeValue +" into the HashMap", logFileWriter);
		//now that we've got all of the String values, we're going to make sure we have the right URL back
		TestFileController.writeToLogFile("*** at end of method current URL is " 
				+ driver.getCurrentUrl(), logFileWriter);
		return stringValues;
	}

	public static HashMap<String, Integer> getASCIntAttributes(int index, BufferedWriter logFileWriter) throws Exception{
		String[] amountAttributeLocatorPrefixes = 
			{"fte_", "currentSalary_", "DOShareAmtCurrency_",
			"deptShareAmtCurrency_", "total_", "fteAdjustedSalary_",
			"fteAdjustedDoShare_", "fteAdjustedDeptShare_", "fteAdjustedTotalAmount_"};
		HashMap<String, Integer> intValues = new HashMap<String, Integer>();
		for (int attIndex=0; attIndex<amountAttributeLocatorPrefixes.length; attIndex++){
			Integer amount 
				= new Integer(getDollarAmountForLocator(amountAttributeLocatorPrefixes[attIndex] + index, "id", logFileWriter).intValue());
			intValues.put(ExpectedASCData.amountAttributeKeys[attIndex], amount);
		}//end for loop
		return intValues;
	}

	public static String getASCNameForIndex(int index){
		//System.out.println("*** method getASCNameLocatorForIndex is running ***");
		String prefix = "//div[4]/table/tbody//tr";
		String suffix = "/td[2]";
		if (index==0) return prefix + suffix;
		else return prefix + "[" + ++index +"]" +suffix;
	}
	
	public static String getASCDeptForIndex(int index){
		//System.out.println("*** method getASCDeptLocatorForIndex is running ***");
		String prefix = "additionalSalConsiderationOrg_";
		WebElement selectElement = driver.findElement(By.id(prefix + index));
		Select selectedValue = new Select(selectElement);
		String value = selectedValue.getFirstSelectedOption().getText();
		if (value.equalsIgnoreCase("Select")) value = new String();
		return value;
	}
	
	public static String getASCStageForIndex(int index){
		//System.out.println("*** method getASCStageLocatorForIndex is running ***");
		String prefix = "salSetStageType_";
		WebElement selectElement = driver.findElement(By.id(prefix + index));
		Select selectedValue = new Select(selectElement);
		return selectedValue.getFirstSelectedOption().getText();
	}

	public static String getASCCategoryForIndex(int index){
		//System.out.println("*** method getASCCategoryLocatorForIndex is running ***");
		String prefix = "additionalCate_";
		WebElement selectElement = driver.findElement(By.id(prefix + index));
		Select selectedValue = new Select(selectElement);
		return selectedValue.getFirstSelectedOption().getText();
	}
	
	public static String getASCSubCategoryForIndex(int index){
		//System.out.println("*** method getASCSubCategoryLocatorForIndex is running ***");
		String prefix = "subcategory_";
		WebElement selectElement = driver.findElement(By.id(prefix + index));
		Select selectedValue = new Select(selectElement);
		return selectedValue.getFirstSelectedOption().getText();
	}
	
	public static String getASCPoolForIndex(int index){
		//System.out.println("*** method getASCPoolForIndex is running ***");
		String prefix = "poolSelect_";
		WebElement selectElement = driver.findElement(By.id(prefix + index));
		Select selectedValue = new Select(selectElement);
		return selectedValue.getFirstSelectedOption().getText();
	}
	
	public static String getASCDescriptionForIndex(int index, BufferedWriter logFileWriter) throws Exception{
		String prefix = "notes_";
		Thread.sleep(2000);
		driver.findElement(By.id(prefix + index)).click();
		Thread.sleep(2000);
		switchWindowByURL("https://handsonweb-"+HandSOnTestSuite.machineName+".stanford.edu/"+HandSOnTestSuite.envName+"/salarySetting/considerationNote.jsp?", logFileWriter);
		TestFileController.writeToLogFile("Got the description dialog", logFileWriter);
		waitForElementPresent(By.id("noteContent"));
		String DescriptionText = driver.findElement(By.id("noteContent")).getAttribute("value");
		TestFileController.writeToLogFile("Description: " + DescriptionText, logFileWriter);
		driver.findElement(By.id("buttonSubmit")).click();
		Thread.sleep(2000);
		while(! switchWindowByURL(UIController.salarySettingURL, logFileWriter)){
			TestFileController.writeToLogFile("current URL is " + driver.getCurrentUrl(), logFileWriter);
			Thread.sleep(2000);
		}
		Thread.sleep(2000);
		return DescriptionText;
	}
	
	public static String getStringValueForLocator(String locator, BufferedWriter logFileWriter, boolean writeToLogFile) throws Exception{
		if (writeToLogFile){
			logFileWriter.write("*** method getStringValueForLocator called with locator "+ locator +"***");
			logFileWriter.newLine();
		}//write it only if the boolean value indicates this
		if (isElementPresent(By.xpath(locator))){
			if (locator.endsWith("input"))
				return getValueOfInputForLocator(locator, logFileWriter, writeToLogFile);
			else {
				String value = new String();
				try {
					WebElement stringElement = driver.findElement(By.xpath(locator));
					value = stringElement.getText();
				}//end try
				catch(StaleElementReferenceException e) {
					WebElement stringElement = driver.findElement(By.xpath(locator));
					value = stringElement.getText();
				}//end catch
				return value;
			}
		}//end if - the element is present
		else return new String();
	}//end getStringValueForLocator method
	
	public static String getValueOfInputForLocator(String locator, BufferedWriter logFileWriter, boolean writeToLogFile) throws Exception{
		if (writeToLogFile){
			logFileWriter.write("*** method getValueOfInputForLocator called with locator "+ locator +"***");
			logFileWriter.newLine();
		}//write it only if the boolean value indicates this
		if (isElementPresent(By.xpath(locator))){
			return driver.findElement(By.xpath(locator)).getAttribute("value");
		}
		else return new String();
	}//end getValueOfInputForLocator method

	public static String getASCNotesForIndex(int index, BufferedWriter logFileWriter) throws Exception{
		String prefix = "secretNotes_";
		Thread.sleep(2000);
		driver.findElement(By.id(prefix + index)).click();
		Thread.sleep(2000);
		switchWindowByURL("https://handsonweb-"+HandSOnTestSuite.machineName+".stanford.edu/"+HandSOnTestSuite.envName+"/salarySetting/considerationNote.jsp?", logFileWriter);
		TestFileController.writeToLogFile("Got the Note dialog", logFileWriter);
		waitForElementPresent(By.id("noteContent"));
		String NotesText = driver.findElement(By.id("noteContent")).getAttribute("value");
		TestFileController.writeToLogFile("Note: " + NotesText, logFileWriter);
		driver.findElement(By.id("buttonSubmit")).click();
		Thread.sleep(2000);
		while(! switchWindowByURL(UIController.salarySettingURL, logFileWriter)){
			TestFileController.writeToLogFile("current URL is " + driver.getCurrentUrl(), logFileWriter);
			while (switchWindowByURL("https://handsonweb-"+HandSOnTestSuite.machineName+".stanford.edu/"+HandSOnTestSuite.envName+"/salarySetting/considerationNote.jsp?", logFileWriter)){
				TestFileController.writeToLogFile("The Note dialog is still open - attempting to close it again", logFileWriter);
				Thread.sleep(2000);
				driver.findElement(By.id("buttonSubmit")).click();
			}//end if
			Thread.sleep(2000);
		}//end while
		Thread.sleep(2000);
		return NotesText;
	}

	public static boolean addNewASC (String clusterName, String deptName, String facultyName, BufferedWriter logFileWriter) throws Exception {
		try{
			driver.findElement(By.linkText("Add Salary Consideration")).click();
			driver.findElement(By.xpath("//tr["+ numberOfASCs +1 + "]/td[2]/img")).click();
			Thread.sleep(2000);
			switchWindowByURL("https://handsonweb-"+HandSOnTestSuite.machineName+".stanford.edu/"+HandSOnTestSuite.envName+"/salarySetting/selectFacultyConsideration.jsp", logFileWriter);
			waitForElementPresent(By.id("cluster"));
			new Select(driver.findElement(By.id("cluster"))).selectByVisibleText(clusterName);
			Thread.sleep(3000);
			new Select(driver.findElement(By.id("department"))).selectByVisibleText(deptName);
			Thread.sleep(3000);
			new Select(driver.findElement(By.id("faculty"))).selectByVisibleText(facultyName);
			Thread.sleep(3000);
			driver.findElement(By.id("mybutton")).click();
		}//end try clause
		catch (Exception e){
			e.printStackTrace();
			logFileWriter.write(e.getMessage());
			return false;
		}//end catch clause
		numberOfASCs++;
		return true;
	}//end method

	public static boolean openSalarySetting(BufferedWriter logFileWriter) throws Exception{
		  if (UIController.openSalarySetting(logFileWriter)){
			  TestFileController.writeToLogFile("Successfully opened Salary Setting", logFileWriter);
		  }
		  else{ 
			  TestFileController.writeToLogFile("Attempt to open Salary Setting failed: " 
					  +"Couldn't open Salary Setting", logFileWriter);
			  return false;
		  }
		  UIController.switchToControlSheetTab();
		  //for some reason, it takes a while for the page to finish loading, so wait for it
		  Thread.sleep(30000);
		  return true;
	}//end openSalarySetting method
	
	public static void adjustAllPercentageValues(BufferedWriter logFileWriter, String whichValues) throws Exception{
		  if (openSalarySetting(logFileWriter)) 
			  TestFileController.writeToLogFile("Successfully opened Salary Setting", logFileWriter);
		  else{
			  String message = "Could not open Salary Setting - aborting";
			  TestFileController.writeToLogFile(message, logFileWriter);
			  System.out.println(message);
			  return;
		  }
		  Thread.sleep(90000);
		  for (int i = 0; i<SalaryIncreaseLabels.length; i++){
			  if (whichValues.equalsIgnoreCase(ReportRunner.ALTERED))
				  adjustValue (SalaryIncreaseLabels[i], alteredSalaryIncreaseValues[i]);
			  else if (whichValues.equalsIgnoreCase(ReportRunner.ORIGINAL))
				  adjustValue (SalaryIncreaseLabels[i], originalSalaryIncreaseValues[i]);
			  else {
				  TestFileController.writeToLogFile("Cannot determine what type of value - original or altered", logFileWriter);
				  return;
			  }//end else
		  }//end for
		  Thread.sleep(90000);
		  HandSOnTestSuite.salarySettingManager.saveSalarySetting();
	}

	
	public static void adjustValue (String label, String value) throws Exception{
		String locator = getLocator(label);
		UIController.waitForElementPresent(By.xpath(locator));
		WebElement adjusted = UIController.driver.findElement(By.xpath(locator));
		boolean isEditable = (adjusted.getAttribute("readonly") == null) 
								|| (adjusted.getAttribute("readonly").equalsIgnoreCase("false"))
								&& (adjusted.isEnabled());
					
		if (isEditable){
			Thread.sleep(3000);
			UIController.driver.findElement(By.xpath(locator)).clear();
			Thread.sleep(3000);
			UIController.driver.findElement(By.xpath(locator)).sendKeys(value);
			Thread.sleep(3000);
		}
		else System.out.println("field with label " + label +" cannot be adjusted to value " + value);
	}//"C:\Program Files (x86)\Mozilla Firefox\firefox.exe" -P
	
	public static String getLocator(String label) throws Exception{
		int rowNum = locators.getCellRowNum(locatorFileSheetName, "Name", label);
		return locators.getCellData(locatorFileSheetName, "Locator", rowNum);
	}
	  public static boolean isApproximatelyEqual(double expected, double actual){
		  return ((expected > actual - 2) && (expected < actual + 2));
	  }
	  
	  public static double getSalaryIncreasePercentFromUI(String typeOfRaise, BufferedWriter logFileWriter) throws Exception{
		  TestFileController.writeToLogFile("*** method getSalaryIncreasePercentFromUI called ***", logFileWriter);
		  String locator = new String();
		  String locatorBase = new String();
		  if (typeOfRaise.equalsIgnoreCase("Chairs / Directors"))
			  locatorBase = "DepartmentChairPercentageAmount";
		  else if (typeOfRaise.equalsIgnoreCase("Sr. Associate Deans"))
			  locatorBase = "SrAssociateDeansPercentageAmount";
		  else if (typeOfRaise.contains("Pool") || typeOfRaise.contains("Fund")) 
			  locatorBase = typeOfRaise;
		  
		  TestFileController.writeToLogFile("Locator Base used to derive locator is " + locatorBase, logFileWriter);
		  locator = locators.getValueForColumnNameAndValue(locatorFileSheetName, 
				  locatorBase, "Name", "Locator", logFileWriter, ExcelReaderToOutputFile);
		  TestFileController.writeToLogFile("Locator derived is " + locator, logFileWriter);
		  String rowPercent = ControlSheetUI.getValueOfInputForLocator(locator, logFileWriter, locatorsToOutputFile);
		  TestFileController.writeToLogFile("Row Percent derived is " + rowPercent, logFileWriter);
		  
		  if ((rowPercent == null) || (rowPercent.length() == 0)){
			  TestFileController.writeToLogFile("Could not get a valid value for Raise Percent - returning zero",
					  logFileWriter);
			  return 0;
		  }//end if - we can't return a valid percentage
		  return ((Number)NumberFormat.getNumberInstance(java.util.Locale.US).parse(rowPercent)).doubleValue()/100;
	  }//end getSalaryIncreasePercentFromUI method
	  
	  
	  /*
	   * Start with a sum value of zero.
	   * For each row, determine whether or not "Salary Base" is the destination.
	   * If the row has "Salary Base" as the destination, then get the amount, and add it to the sum.
	   * Determine whether or not there is a next row.
	   * If there is a next row, go through this iteration again.
	   */
	  
	  public static double getTransferSumForDestination (String destination, BufferedWriter logFileWriter, 
			  boolean writeToLogFile) throws Exception{
		  if (writeToLogFile){
			  logFileWriter.write("*** method getTransferSumForDestination called with destination " 
					  + destination + " ****");
			  logFileWriter.newLine();
		  }//end if
		  double sum = 0;
		  int row = 1;
		  boolean rowIsPresent = true;	  
		  while (rowIsPresent){
			  try{
				  if (isSelectedAdjustmentLabel (row, destination, logFileWriter, writeToLogFile)){
					  double amount = getAdjustmentAmountForRow(row, logFileWriter, writeToLogFile);
					  sum += amount;
					  if (writeToLogFile){
						  logFileWriter.write("row with destination " + destination + " found"
								  	 +" - row " +row + ", and amount " + amount
								  	 +" has been added - sum is updated to " + sum);
						  logFileWriter.newLine();
					  }//end if
				  }//end if
				  else if (writeToLogFile){
					  logFileWriter.write("row " + row + " has a different destination value," 
							  	 +" amount has not been added - sum remains " + sum);
					  logFileWriter.newLine();
				  }//end else if - the row is found but doesn't have the designated destination
				  row++;
			  }//end try - we've found that particular row and added the amount to the sum
			  catch (NoSuchElementException e){
				  rowIsPresent = false;
				  if (writeToLogFile){
					  logFileWriter.write("Row number " + row +" does not exist - exiting");
					  logFileWriter.newLine();
				  }//end if
			  }//end catch clause - we can't find the row (hopefully we've looked at all of them)
		  }//end while
		  return sum;
	  }//end getTransferSumForDestination pool
	  
	  
	  /*
	   * This extracts the dollar amount transferred for a given row.
	   * It assumes that (1) the amount desired is either an Adjustment or a Transfer, 
	   * and (2) the int value for the row equals one for the topmost row, two
	   * for the second row, etc....
	   */
	  public static double getAdjustmentAmountForRow(int row, BufferedWriter logFileWriter, boolean writeToLogFile) throws Exception{
		  if (writeToLogFile){
			  logFileWriter.write("*** method getAdjustmentAmountForRow called with row " + row);
			  logFileWriter.newLine();
		  }//end if
		  String locator = "//div[6]/table/tbody/tr/td[4]/input";
		  if (row > 1){
			  locator = "//div[6]/table/tbody/tr[" + row + "]/td[4]/input";
		  }//end if
		  if (writeToLogFile){
			  logFileWriter.write("Locator derived: " + locator);
			  logFileWriter.newLine();
		  }//end if
		  String amountString = driver.findElement(By.xpath(locator)).getAttribute("value");

		  if (writeToLogFile){
			  logFileWriter.write("Amount String derived: " + amountString);
			  logFileWriter.newLine();
		  }//end if
		  return ((Number)NumberFormat.getNumberInstance(java.util.Locale.US).parse(amountString)).doubleValue();
	  }//end getAdjustmentAmountForRow method
	  
	  public static boolean isSelectedAdjustmentLabel (int row, String label, BufferedWriter logFileWriter, boolean writeToLogFile) throws Exception{
		  if (writeToLogFile){
			  logFileWriter.write("*** method isSelectedAdjustmentLabel called with row " + row +" ****");
			  logFileWriter.newLine();
		  }//end if
		  String locator = "//div[6]/table/tbody/tr[" + row +"]/td[3]/select";
		  if  (row == 1) locator = "//div[6]/table/tbody/tr/td[3]/select";
		  if (writeToLogFile){
			  logFileWriter.write("Locator derived: " + locator);
			  logFileWriter.newLine();
		  }//end if
		  By by = By.xpath(locator);
		  if (writeToLogFile){
			  logFileWriter.write("Element to be located: " + by.toString());
			  logFileWriter.newLine();
		  }//end if
		  String selectedOption = new Select(driver.findElement(by)).getFirstSelectedOption().getText();
		  if (writeToLogFile){
			  logFileWriter.write("Text of selected element is " + selectedOption);
			  logFileWriter.newLine();
		  }//end if
		  return selectedOption.equalsIgnoreCase(label);
	  }//end isSelectedLabel method
	  
	  public static double getSalaryBaseForLabel(String label, BufferedWriter logFileWriter) throws Exception{
		  logFileWriter.write("*** method getSalaryBaseForLabel called ***");
		  logFileWriter.newLine();
		  String locator = ControlSheetUI.getLocator(label);
		  ControlSheetUI.waitForElementPresent(By.xpath(locator));
		  String salaryBaseString = UIController.driver.findElement(By.xpath(locator)).getText();
		  if (salaryBaseString.startsWith("$")) salaryBaseString=salaryBaseString.substring(1);
		  return ((Number)NumberFormat.getNumberInstance(java.util.Locale.US).parse(salaryBaseString)).doubleValue();
	  }//end getSalaryBaseForLabel method


	
}//end Class
