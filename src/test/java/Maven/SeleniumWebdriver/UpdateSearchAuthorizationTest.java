package Maven.SeleniumWebdriver;

import static org.junit.Assert.fail;

import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.openqa.selenium.NoSuchElementException;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;


public class UpdateSearchAuthorizationTest {
	protected static String testName;
	protected static String testCaseNumber;
	protected static String testDescription;
	protected static String testEnv;
	protected static StringBuffer verificationErrors;
	protected BufferedWriter logFileWriter;
	protected static String[] IDStrings  = {"2301", "2303", "2392", "2407"};
	protected static final String[] locatorNames={"FacultyLevelSelect", "DeptSelect", "LabSelect", "BilletNumberTextfield"};
	protected static final String[] newValues={"Senior", "Mathematics", "Yes", "TBD2"};
	protected static final String[] menuItems={"Correction", "Cost Adjustment", "Dean Approved", "Initial Offer"};

	protected static final String standardCostsWorksheet = "Standard Costs Page";
	
	@Before
	public void setUp() throws Exception {
		testName = "UpdatedSearchAuthorizationTest";
		testEnv = "Test";
		testCaseNumber = "065";
		testDescription = "Test of Updates to Search Authorizations";
		verificationErrors = new StringBuffer();
	}

	@After
	public void tearDown() throws Exception {
		TestFileController.writeTestResults(testEnv, testCaseNumber, testName, testDescription, verificationErrors);
		logFileWriter.newLine();
		logFileWriter.close();
	}//end tearDown method

	@Test
	public void test() throws Exception{
		String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		logFileWriter = TestFileController.getLogFileWriter(logFileName);
		logFileWriter.write("Search Authorization Updates will be tested");
		logFileWriter.newLine();
		
		//IDStrings = NewRecruitmentTest.selectInitialRecruitments(verificationErrors, logFileWriter);

		UIController.switchToStartPage(logFileWriter);
		
		//look up an SA with a Recruitment - it should be No Offer and Active
		//open the Search Authorization Page
		processPageByLink("Search Authorization Lookup", logFileWriter);
		selectSAByIDStrings(logFileWriter);
		changeSearchAuthAttributes(logFileWriter);
		verifyChangedRecruitmentGeneralAttributes(logFileWriter);
		verifyUnchangedRecruitmentCostAttributes(logFileWriter);
		verifyChangedRecruitmentCostAttributes(logFileWriter);
		closeAllTestDataTabs(logFileWriter);
	}//end test method
	
	public void closeAllTestDataTabs(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("*** method closeAllTestDataTabs called ***");
		logFileWriter.newLine();
		String[] URLs = {UIController.startPage,  UIController.searchAuthorizationLookupURL,
				UIController.salarySettingURL};
		UIController.closeAllWindowsExceptURLs(URLs, logFileWriter);
	}//end closeAllTestDataTabs method
	
	public void testForHandSOn3179(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("Testing to see if HANDSON-3179 is still an issue");
		logFileWriter.newLine();
		String plannedBaseBudgetYear 
			= HandSOnTestSuite.searchAuthorizationUI.getPlannedBaseBudgetYear(logFileWriter);
		String salaryPromiseFY 
			= HandSOnTestSuite.searchAuthorizationUI.getSalaryPromiseFY(logFileWriter);
		String message = "Planned Base Budget Year is determined to be " + plannedBaseBudgetYear
				+", and Salary Promise Initial Fiscal Year is determined to be "+ salaryPromiseFY;
		if (salaryPromiseFY.equalsIgnoreCase(plannedBaseBudgetYear)){
			message = "HandSON-3179 has been resolved - " + message;
			message = message.concat(", and they are equal; ");
		}
		else{
			message = "HANDSON-3179 is still an issue - " + message;
			message = message.concat(", and they are not equal; ");
			verificationErrors.append(message);
		}
		logFileWriter.write(message);
		logFileWriter.newLine();
	}//end testForHandSOn3179 method
	
	public static void resetToSearchAuthCosts(String recruitmentID, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method resetToSearchAuthCosts called for Recruitment ID "+recruitmentID +" ***");
		logFileWriter.newLine();
		System.out.println("*** method resetToSearchAuthCosts called for Recruitment ID "+recruitmentID +" ***");
		Thread.sleep(3000);
		switchToRecruitmentWindow(recruitmentID, verificationErrors, logFileWriter);

		logFileWriter.write("Now refreshing page with URL "+UIController.driver.getCurrentUrl());
		logFileWriter.newLine();
		UIController.driver.navigate().refresh();
		Thread.sleep(5000);
		UIController.dismissAlert(logFileWriter);

		Thread.sleep(3000);
		By by = By.linkText("Reset to Search Auth Costs");
		
		for (int iterations = 0; (iterations<5 && (! UIController.isElementPresent(by))); iterations++){
			logFileWriter.write("Now clicking on the Costs tab");
			logFileWriter.newLine();
			By costsTabLink = By.id("costs_a");
			if (UIController.isElementPresent(costsTabLink))
				UIController.waitAndClick(costsTabLink);
			Thread.sleep(4000);
		}//end if
		
		UIController.waitForElementPresent(by);
		UIController.scrollElementIntoView(by, logFileWriter);
		Thread.sleep(5000);
		UIController.waitAndClick(by);

		logFileWriter.write("Successfully clicked on 'Reset to Search Auth Costs' link");
		logFileWriter.newLine();
		System.out.println("Successfully clicked on 'Reset to Search Auth Costs' link");
		Thread.sleep(3000);
		
		UIController.dismissAlert(logFileWriter);
		Thread.sleep(20000);

		logFileWriter.write("After reset now refreshing page with URL "+UIController.driver.getCurrentUrl());
		logFileWriter.newLine();
		UIController.driver.navigate().refresh();
		Thread.sleep(5000);
		UIController.dismissAlert(logFileWriter);
		Thread.sleep(3000);

		By saveBy 
			= HandSOnTestSuite.searchAuthorizationUI.getByForName(SearchAuthorizationUIController.CostsWorksheetName, 
					"SaveButton", logFileWriter);
		UIController.waitForElementPresent(saveBy);
		UIController.driver.navigate().refresh();
		logFileWriter.write("Page Refreshed");
		logFileWriter.newLine();
		
		Thread.sleep(6000);
		UIController.dismissAlert(logFileWriter);
		if (UIController.waitAndClick(saveBy)){
			logFileWriter.write("Successfully saved the changes");
			logFileWriter.newLine();
		}
		else{
			logFileWriter.write("Could not successfully save the changes");
			logFileWriter.newLine();
		}
		
		Thread.sleep(8000);
		
		try{
			UIController.dismissAlert(logFileWriter);
		}
		catch(Exception e){
			logFileWriter.write("Could not dismiss alert after saving the changes because of Exception "+e.getMessage());
			logFileWriter.newLine();
		}
		
		try{
			logFileWriter.write("Trying to get to the Change Annotation Dialog the first time");
			logFileWriter.newLine();
			handleChangeAnnotationDialog("Correction", verificationErrors, logFileWriter);
			switchToRecruitmentWindow(recruitmentID, verificationErrors, logFileWriter);
			try{
				UIController.dismissAlert(logFileWriter);
				logFileWriter.write("Successfully dismissed the alert after working with the Change Annotation Dialog");
				logFileWriter.newLine();
			}
			catch(Exception e){
				logFileWriter.write("Could not dismiss the alert after working with the Change Annotation Dialog because of Exception "+e.getMessage());
				logFileWriter.newLine();
			}
		}
		catch(Exception e){
			logFileWriter.write("Could not get to the Change Annotation Dialog because of Exception "+e.getMessage());
			logFileWriter.newLine();
		}
		Thread.sleep(5000);
		try{
			UIController.dismissAlert(logFileWriter);
		}
		catch(Exception e){
			handleChangeAnnotationDialog("Correction", verificationErrors, logFileWriter);
			switchToRecruitmentWindow(recruitmentID, verificationErrors, logFileWriter);
		}
		Thread.sleep(5000);

		
		
		
		
		try{
			UIController.dismissAlert(logFileWriter);
		}//end try clause
		catch(Exception e){
			logFileWriter.write("Could not dismiss Alert because of Exception "+e.getMessage());
			logFileWriter.newLine();
		}//end catch clause
	}//end resetToSearchAuthCosts method
	
	/*
	 * This method is going to verify that the "Reset to Search Auth Costs" Button/Link
	 * does what it should do and makes the Costs of a Recruitment identical to the 
	 * Costs of the Search Authorization that it is associated with.  
	 * 
	 * Of note, there is an existing bug - HANDSON-3177 - that has not been fixed 
	 * as of the coding of this test.  Every time the "Reset to Search Auth Costs" button
	 * is clicked on the Recruitment Cost tab, all Housing Costs are eliminated, even 
	 * if there were Housing Costs in the Search Authorization and in the associated 
	 * Recruitment before clicking on that button.  This test is designed to fail and
	 * will make a note of that issue until it is fixed.
	 */
	public void verifyChangedRecruitmentCostAttributes(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("*** method verifyChangedRecruitmentCostAttributes called ***");
		logFileWriter.newLine();
		for (int i=0; i<IDStrings.length; i++){
			switchToSearchAuthWindow(IDStrings[i], logFileWriter);
			String firstRecruitmentID = getRecruitmentID("FirstRecruitmentLink", logFileWriter);
			String secondRecruitmentID = getRecruitmentID("SecondRecruitmentLink", logFileWriter);
			switchToSearchAuthWindow(IDStrings[i], logFileWriter);
			HashMap<String, Integer> searchAuthCosts 
				= HandSOnTestSuite.searchAuthorizationUI.getActualCosts(logFileWriter);
			Thread.sleep(3000);
			iterateCosts("Original Costs, called from verifyChangedRecruitmentCostAttributes method", searchAuthCosts, logFileWriter);
			HashMap<String, Integer> NewSearchAuthCosts 
				= updateSearchAuthCosts(searchAuthCosts, "authId="+IDStrings[i], logFileWriter);
			iterateCosts("Original Costs, called from verifyChangedRecruitmentCostAttributes method, called after update", 
					searchAuthCosts, logFileWriter);
			iterateCosts("Updated Costs, called from verifyChangedRecruitmentCostAttributes method", NewSearchAuthCosts, logFileWriter);
			resetToSearchAuthCosts(secondRecruitmentID, logFileWriter);
			compareChangedRecruitmentAttributes(secondRecruitmentID, verificationErrors, NewSearchAuthCosts, logFileWriter);
			switchToSearchAuthWindow(IDStrings[i], logFileWriter);
			Thread.sleep(3000);
			resetToSearchAuthCosts(firstRecruitmentID, logFileWriter);
			compareChangedRecruitmentAttributes(firstRecruitmentID, verificationErrors, NewSearchAuthCosts, logFileWriter);
			Thread.sleep(3000);
			switchToSearchAuthWindow(IDStrings[i], logFileWriter);
			resetSearchAuthCosts(searchAuthCosts, logFileWriter);
			resetToSearchAuthCosts(secondRecruitmentID, logFileWriter);
			compareChangedRecruitmentAttributes(secondRecruitmentID, verificationErrors, searchAuthCosts, logFileWriter);
			switchToSearchAuthWindow(IDStrings[i], logFileWriter);
			resetToSearchAuthCosts(firstRecruitmentID, logFileWriter);
			compareChangedRecruitmentAttributes(firstRecruitmentID, verificationErrors, searchAuthCosts, logFileWriter);
			Thread.sleep(3000);
			switchToSearchAuthWindow(IDStrings[i], logFileWriter);
		}//end for 		
	}//end verifyChangedRecruitmentCostAttributes method
	/*
	 * This assumes that the "verifyChangedRecruitmentGeneralAttributes" method has
	 * already been run and the pages with the Recruitments have already been
	 * opened and they only need to be refreshed at this point.
	 * 
	 * We will update each of the Search Authorization Costs, and then we will go
	 * to the Costs tab of each of the associated Recruitments and verify 
	 * that the Recruitment Costs are unchanged.
	 * 
	 * It appears that this may be a simpler task than we expected as the Recruitment
	 * Cost amounts have the same locators that the same Costs do in the 
	 * Search Authorization screen.
	 */
	protected void verifyUnchangedRecruitmentCostAttributes(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("*** method verifyUnchangedRecruitmentCostAttributes called ***");
		logFileWriter.newLine();
		for (int i=0; i<IDStrings.length; i++){
			switchToSearchAuthWindow(IDStrings[i], logFileWriter);
			String firstRecruitmentID = getRecruitmentID("FirstRecruitmentLink", logFileWriter);
			String secondRecruitmentID = getRecruitmentID("SecondRecruitmentLink", logFileWriter);
			switchToSearchAuthWindow(IDStrings[i], logFileWriter);
			By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SA General Page", "CostsTabLink", logFileWriter);
			UIController.driver.findElement(by).click();
			HashMap<String, Integer> searchAuthCosts 
				= HandSOnTestSuite.searchAuthorizationUI.getActualCosts(logFileWriter);
			Thread.sleep(3000);
			HashMap<String, Integer> NewSearchAuthCosts 
				= updateSearchAuthCosts(searchAuthCosts, "authId="+IDStrings[i], logFileWriter);
			switchToRecruitmentWindow(firstRecruitmentID, verificationErrors, logFileWriter);
			Thread.sleep(3000);
			UIController.driver.navigate().refresh();
			compareUnchangedRecruitmentAttributes(firstRecruitmentID, NewSearchAuthCosts, verificationErrors, logFileWriter);
			Thread.sleep(3000);
			switchToSearchAuthWindow(IDStrings[i], logFileWriter);
			Thread.sleep(3000);
			switchToRecruitmentWindow(secondRecruitmentID, verificationErrors, logFileWriter);
			Thread.sleep(3000);
			UIController.driver.navigate().refresh();
			compareUnchangedRecruitmentAttributes(secondRecruitmentID, NewSearchAuthCosts, verificationErrors, logFileWriter);
			switchToSearchAuthWindow(IDStrings[i], logFileWriter);
			Thread.sleep(3000);
			resetSearchAuthCosts(searchAuthCosts, logFileWriter);
		}//end for loop
	}//end verifyUnchangedRecruitmentCostAttributes method
	
	
	/*
	 * The goal of this method will be to take the values that the Search Authorization had, at the beginning
	 * of the test, before the values were updated, and reset the updated values to the old values.
	 */
	private void resetSearchAuthCosts(HashMap<String, Integer> originalCosts, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method resetSearchAuthCosts called ***");
		logFileWriter.newLine();
		
		Set<String> costTypes = originalCosts.keySet();
		Iterator<String> iterator = costTypes.iterator();
		String housingNumber = new String();
		while (iterator.hasNext()){
			String costType = iterator.next();
			String locatorName = getLocatorNameFromCostType(costType, housingNumber, logFileWriter);
			if (locatorName.contains("Housing"))
				housingNumber = locatorName.replaceFirst("HousingFirstFYTextField", "");
			String locator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName(SearchAuthorizationUIController.CostsWorksheetName, locatorName, logFileWriter);
			By costBy = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(locator, logFileWriter);
			String costString = originalCosts.get(costType).toString();
			
			logFileWriter.write("Locator Name "+locatorName
					+" leads to locator "+locator
					+", which will be updated with Cost String "+costString);
			logFileWriter.newLine();			
			Thread.sleep(2000);
			WebElement element = null;
			try {
				element = UIController.driver.findElement(costBy);
				element.clear();
				element.sendKeys(costString);
			}
			catch(Exception e) {
				String message = "Could not find the Web Element indicated by Locator Name "+locatorName;
				logFileWriter.write(message);
				logFileWriter.newLine();
				verificationErrors.append(message);
			}
			Thread.sleep(2000);
		}//end while
		
		By saveBy = HandSOnTestSuite.searchAuthorizationUI.getByForName(SearchAuthorizationUIController.CostsWorksheetName, 
				"SaveButton", logFileWriter);
		UIController.driver.findElement(saveBy).click();
		UIController.dismissAlert(logFileWriter);
		By generalTab = HandSOnTestSuite.searchAuthorizationUI.getByForName("GeneralTabLink", logFileWriter);
		UIController.waitAndClick(generalTab);
		testForHandSOn3179(logFileWriter);
	}//end resetSearchAuthCosts method
	
	/*
	 * There are several steps to this: 
	 * (1) Get the locator names for each field that the Search Authorization actually has.
	 * (2) Get the value for each of the fields and increment or decrement it by a certain amount.
	 * (3) Save the Search Authorization.
	 */
	public static HashMap<String, Integer> updateSearchAuthCosts(HashMap<String, Integer> originalCosts, String URL, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method updateSearchAuthCosts called ***");
		logFileWriter.newLine();
		HashMap<String, Integer> updatedCosts = new HashMap<String, Integer>();
		Set<String> costTypes = originalCosts.keySet();
		Iterator<String> iterator = costTypes.iterator();
		String housingNumber = new String();
		while (iterator.hasNext()){
			String costType = iterator.next();
			String locatorName = getLocatorNameFromCostType(costType, housingNumber, logFileWriter);
			if (locatorName.contains("Housing"))
				housingNumber = locatorName.replaceFirst("HousingFirstFYTextField", "");
			String locator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName(SearchAuthorizationUIController.CostsWorksheetName, locatorName, logFileWriter);
			if (locatorName.contains("Summer9ths"))
				locator = "id=offerEstimatedCost(SUMMER).offerCostFundingDTO[0].offerCostFundAlloc[1].units";
			By costBy = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(locator, logFileWriter);
			String costString = "0";
			if (UIController.isElementPresent(costBy)){
				costString = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(costBy, logFileWriter);
				logFileWriter.write("Locator Name "+locatorName+" leads to locator "+locator+", which yields Cost String "+costString);
				logFileWriter.newLine();
				String updatedCost = getUpdatedCost(locatorName, costString, logFileWriter);
				try{
					UIController.driver.findElement(costBy).clear();
				}//end try clause
				catch(InvalidElementStateException e){
					logFileWriter.write("Unable to clear element - clicking on Costs Tab");
					logFileWriter.newLine();
					UIController.waitAndClick(HandSOnTestSuite.searchAuthorizationUI.getByForName("CostsTabLink", logFileWriter));
					Thread.sleep(4000);
					UIController.waitForElementPresent(costBy);
					UIController.driver.findElement(costBy).clear();
				}//end catch clause
				UIController.driver.findElement(costBy).sendKeys(updatedCost);
				updatedCosts.put(costType, new Integer(updatedCost));
				Thread.sleep(2000);
			}
			else {
				logFileWriter.write("Could not acquire the String element - assigning a cost string of zero");
				logFileWriter.newLine();
			}
		}//end while
		
		By saveBy = HandSOnTestSuite.searchAuthorizationUI.getByForName(SearchAuthorizationUIController.CostsWorksheetName, "SaveButton", logFileWriter);
		UIController.driver.findElement(saveBy).click();
		Thread.sleep(5000);
		if (URL.contains("recId=")){
			handleChangeAnnotationDialog("Cost Adjustment", verificationErrors, logFileWriter);
			if (! UIController.switchWindowByURL(URL, logFileWriter)){
				logFileWriter.write("Couldn't switch back to the previous URL: "+URL);
				logFileWriter.newLine();
			}//end if
		}//end outer if - do this for a Recruitment
		Thread.sleep(5000);
		UIController.dismissAlert(logFileWriter);
		Thread.sleep(5000);
		iterateCosts("Original SA Costs", originalCosts, logFileWriter);
		iterateCosts("Updated SA Costs", updatedCosts, logFileWriter);
		return updatedCosts;
	}//end updateSearchAuthCosts method
	
	public static void iterateCosts(String name, HashMap<String, Integer> costs, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method iterateCosts called with HashMap "+name+" ***");
		logFileWriter.newLine();
		logFileWriter.write("There are "+costs.size()+" Costs in the HashMap "+name);
		logFileWriter.newLine();
		logFileWriter.write("Cost values are as follows: ");
		logFileWriter.newLine();
		Set<String> costTypes = costs.keySet();
		Iterator<String> iterator = costTypes.iterator();
		while (iterator.hasNext()){
			String costType = iterator.next();
			logFileWriter.write("Cost Type "+ costType + ", Amount: "+costs.get(costType));
			logFileWriter.newLine();
		}//end while
		logFileWriter.newLine();
		logFileWriter.newLine();
	}//end iterateCosts method
	
	protected static int getIntCost(String locatorName, String costString){
		int cost = 0;
		if (costString != null && (! costString.isEmpty())){
			costString = costString.replace(",", "");
			if (locatorName.equalsIgnoreCase("Summer9thsFirstFYTextField"))
				costString = costString.replace(".000", "");
			else
				costString = costString.replace(".00", "");
			cost = new Integer(costString).intValue();
		}//end if
		return cost;
	}//end getIntCost method
	
	protected static String getUpdatedCost(String locatorName, String costString, BufferedWriter logFileWriter) throws Exception{
		int cost = getIntCost(locatorName, costString);
		logFileWriter.write("Cost has been altered and the current value is "+cost);
		if (! locatorName.contains("Summer9ths"))
			cost = cost + 1000;
		else if (cost < 4)
			cost = cost + 1;
		else cost = cost - 1;
		logFileWriter.write(", and updated to "+cost);
		logFileWriter.newLine();
		
		return Integer.toString(cost);
	}//end getUpdatedCost method
	
	protected static String getUpdatedHousingNumber(String housingNumber){
		if (housingNumber.isEmpty())
			return "First";
		else if(housingNumber.equalsIgnoreCase("First"))
			return "Second";
		else return "Third";
	}//end getHousingNumber method
	
	protected static String getLocatorNameFromCostType(String costType, String housingNumber, BufferedWriter logFileWriter) throws Exception{
		String locatorName = new String();
		if (costType.contains("Housing")){
			housingNumber = getUpdatedHousingNumber(housingNumber);
			logFileWriter.write("Housing number incremented to "+housingNumber);
			logFileWriter.newLine();
			locatorName = housingNumber + "HousingFirstFYTextField";
		}//end if - Housing
		else if (costType.equalsIgnoreCase("Salary"))
			locatorName = "SalaryCostAmt";
		else locatorName = costType + "FirstFYTextField";
		logFileWriter.write("Cost Type "+costType+" converted to locator name "+locatorName);
		logFileWriter.newLine();
		
		return locatorName;
	}//end method
	
	public static void compareChangedRecruitmentAttributes(String ID, StringBuffer verificationErrors, HashMap<String, Integer> searchAuthAttributes, 
			BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("*** method compareChangedRecruitmentAttributes called with Recruitment ID "+ ID +" ***");
		logFileWriter.newLine();
		logFileWriter.write("SA Costs and Recruitment Costs are expected to be equal");
		logFileWriter.newLine();
		
		Set<String> costTypes = searchAuthAttributes.keySet();
		Iterator<String> iterator = costTypes.iterator();
		String housingNumber = new String();
		while (iterator.hasNext()){
			String costType = iterator.next();
			String locatorName = getLocatorNameFromCostType(costType, housingNumber, logFileWriter);
			if (locatorName.contains("Housing"))
				housingNumber = locatorName.replaceFirst("HousingFirstFYTextField", "");
			String locator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName(SearchAuthorizationUIController.CostsWorksheetName, locatorName, logFileWriter);
			By costBy = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(locator, logFileWriter);
			String costString = new String();
			int recruitmentIntCost = 0;
			try{
				WebElement element = UIController.driver.findElement(costBy);
				costString = element.getAttribute("value");
				recruitmentIntCost = getIntCost(locatorName, costString);
			}
			catch(NoSuchElementException e){
				logFileWriter.write("Locator Name "+locatorName+" is not present for recruitment - assigning zero value");
				logFileWriter.newLine();
			}//end catch clause
			catch(UnhandledAlertException e){
				logFileWriter.write("Unexpected Alert was present - attempting to close it");
				logFileWriter.newLine();
				try{
					UIController.driver.switchTo().alert().accept();
					logFileWriter.write("... and we just closed the unexpected alert");
					logFileWriter.newLine();
				}
				catch(Exception e2){
					logFileWriter.write("Could not close the unexpected alert because of Exception "+e2.getMessage());
					logFileWriter.newLine();
				}//catch - couldn't close the alert
			}//catch - Unhandled Alert Exception
			
			logFileWriter.write("Locator Name "+locatorName+" leads to locator "+locator+", which yields Cost String "+costString);
			logFileWriter.newLine();
			
			int searchAuthCost = searchAuthAttributes.get(costType).intValue();
			if (recruitmentIntCost == searchAuthCost){
				logFileWriter.write("Search Authorization Cost for locator "+locatorName
						+" with Cost of "+searchAuthCost
						+" is equal to Recruitment Cost, as expected, for Recruitment ID "+ID);
				logFileWriter.newLine();
			}//end if - the costs are equal, as expected
			else{
				if (locatorName.contains("Housing")){
					if (recruitmentIntCost == 0){
						String message = "HANDSON-3177 is still an issue; ";
						message = message.concat(costType 
								+ " is not present in Recruitment ID "+ID
								+", but it should be;\n ");
						verificationErrors.append(message);
						logFileWriter.write(message);
						logFileWriter.newLine();
					}//end if
				}//end if - handling HANDSON-3177
				else{
					String message = "ERROR - Search Authorization Cost for locator "+locatorName
						+" with Cost of "+searchAuthCost
						+" is not equal to Recruitment Cost of "+recruitmentIntCost 
						+ ", for Recruitment ID "+ID+";\n ";
					logFileWriter.write(message);
					logFileWriter.newLine();
					verificationErrors.append(message);
					System.out.println(message);
				}//end else
			}//end outer else - the costs are unequal
		}//end while
	}//end compareChangedRecruitmentAttributes method
	
	public static void compareUnchangedRecruitmentAttributes(String recruitmentID, HashMap<String, Integer> searchAuthAttributes, 
			StringBuffer verificationErrors, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method compareUnchangedRecruitmentAttributes called with Recruitment ID "+recruitmentID+" ***");
		logFileWriter.newLine();
		logFileWriter.write("SA Costs and Recruitment Costs are expected to be different");
		logFileWriter.newLine();
		logFileWriter.newLine();

		Set<String> costTypes = searchAuthAttributes.keySet();
		Iterator<String> iterator = costTypes.iterator();
		String housingNumber = new String();
		while (iterator.hasNext()){
			String costType = iterator.next();
			String locatorName = getLocatorNameFromCostType(costType, housingNumber, logFileWriter);
			if (locatorName.contains("Housing"))
				housingNumber = locatorName.replaceFirst("HousingFirstFYTextField", "");
			String locator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName(SearchAuthorizationUIController.CostsWorksheetName, locatorName, logFileWriter);
			By costBy = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(locator, logFileWriter);
			String costString = new String();
			int recruitmentIntCost = 0;
			try{
				WebElement element = UIController.driver.findElement(costBy);
				costString = element.getAttribute("value");
				recruitmentIntCost = getIntCost(locatorName, costString);
			}//end try
			catch(NoSuchElementException e){
				logFileWriter.write("Locator Name "+locatorName
						+" is not present for recruitment ID "+recruitmentID
						+" - assigning zero value");
				logFileWriter.newLine();
			}//end catch clause
			
			logFileWriter.write("Locator Name "+locatorName+" leads to locator "+locator
					+", which yields Cost String "+costString);
			logFileWriter.newLine();
			
			int searchAuthCost = searchAuthAttributes.get(costType).intValue();
			if (recruitmentIntCost == searchAuthCost){
				String message = "ERROR - Search Authorization Cost for locator "+locatorName
						+" is unexpectedly equal to Recruitment Cost of "+recruitmentIntCost
						+", and it applies to Recruitment ID " + recruitmentID + ";\n ";
				logFileWriter.write(message);
				logFileWriter.newLine();
				verificationErrors.append(message);
			}//end if - they unexpectedly match
			else{
				logFileWriter.write("Search Authorization Cost for locator "+locatorName
						+" with Cost of "+searchAuthCost
						+" is not equal to Recruitment Cost of "+recruitmentIntCost
						+", for Recruitment ID "+recruitmentID
						+", as expected");
				logFileWriter.newLine();
			}//end else - the Recruitment and Search Authorization Costs are not equal, as expected
			Thread.sleep(2000);
		}//end while
	}//end compareUnchangedRecruitmentAttributes method
	
	protected void verifyChangedRecruitmentGeneralAttributes(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method verifyChangedRecruitmentGeneralAttributes called ***");
		logFileWriter.newLine();
		for (int i=0; i<IDStrings.length; i++){
			String searchAuthLookupURL = UIController.searchAuthorizationLookupURL+"#results";
			UIController.switchWindowByURL(searchAuthLookupURL, logFileWriter);
			Thread.sleep(3000);
			
			switchToSearchAuthWindow(IDStrings[i], logFileWriter);
			String firstRecruitmentID = getRecruitmentID("FirstRecruitmentLink", logFileWriter);
			String secondRecruitmentID = getRecruitmentID("SecondRecruitmentLink", logFileWriter);
			processPageByLink(firstRecruitmentID, logFileWriter);
			processPageByLink(secondRecruitmentID, logFileWriter);
			Thread.sleep(3000);
			switchToSearchAuthWindow(IDStrings[i], logFileWriter);
			Thread.sleep(3000);
			switchToRecruitmentWindow(secondRecruitmentID, verificationErrors, logFileWriter);
			Thread.sleep(3000);
			verifyChangedGeneralValuesInRecruitment(locatorNames[i], newValues[i], logFileWriter);
			switchToSearchAuthWindow(IDStrings[i], logFileWriter);
			Thread.sleep(3000);
			switchToRecruitmentWindow(firstRecruitmentID, verificationErrors, logFileWriter);
			verifyChangedGeneralValuesInRecruitment(locatorNames[i], newValues[i], logFileWriter);
			Thread.sleep(3000);
			switchToSearchAuthWindow(IDStrings[i], logFileWriter);
			Thread.sleep(3000);
		}//end for loop
	}//end verifyChangedRecruitmentGeneralAttributes method
	
	public static String getRecruitmentID(String locatorName, BufferedWriter logFileWriter) throws Exception{
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(locatorName, logFileWriter);
		String recruitmentID 
			= UIController.driver.findElement(by).getText();
		logFileWriter.write("Recruitment ID found for locator name "+locatorName+" is '"+recruitmentID+"'");
		logFileWriter.newLine();
		if (recruitmentID.isEmpty()){
			logFileWriter.write("Could not find the Recruitment ID in the link name - attempting to do it through the hidden textfield");
			logFileWriter.newLine();
			by = HandSOnTestSuite.searchAuthorizationUI.getByForName(locatorName.replace("Link", "Textfield"), logFileWriter);
			recruitmentID = UIController.driver.findElement(by).getAttribute("value");
			if (recruitmentID.isEmpty())
				
				throw new Exception("Could not derive the Recruitment ID");
		}
		logFileWriter.newLine();
		return recruitmentID;
	}//end getRecruitmentID method
	
	private void verifyChangedGeneralValuesInRecruitment(String locatorName, String newValue, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method verifyChangedGeneralValuesInRecruitment called with locator name "
						+locatorName+", and new value "+newValue);
		logFileWriter.newLine();
		
		if (locatorName.contains("Select"))
				locatorName = locatorName.replace("Select", "");
		else if (locatorName.contains("Textfield"))
				locatorName = locatorName.replace("Textfield", "");
		locatorName = locatorName.concat("Label");

		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", 
				locatorName, logFileWriter);
		String actualText = UIController.driver.findElement(by).getAttribute("value");
		if (actualText == null){
			logFileWriter.write("Actual Text for "+locatorName+" is null - attempting to use getText()");
			logFileWriter.newLine();
			actualText = UIController.driver.findElement(by).getText();
		}//end if - trying to get the text as opposed to the value
		if (actualText == null){//test it again to see if it's still null
			String message = "WARNING!!!  Actual Text for "+locatorName+" is null";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
			return;//stop it right here - no point in continuing
		}//we got no value
		else if (actualText.equalsIgnoreCase(newValue)){
			logFileWriter.write("Locator "+locatorName+" matches expected value "+newValue+", as expected");
			logFileWriter.newLine();
		}//end if - it matches
		else{
			String message = "ERROR - Locator "+locatorName+" had expected value "+newValue+", but actual value was '"+actualText+"';\n ";
			verificationErrors.append(message);
			logFileWriter.write(message);
			logFileWriter.newLine();
		}//end else
	}//end verifyChangedGeneralValuesInRecruitment method
	
	public void changeSearchAuthAttributes(BufferedWriter logFileWriter) throws Exception{
		printToAllOutputs("*** method changeSearchAuthAttributes called ***", logFileWriter);
		for (int i=0; i<locatorNames.length; i++){
			if (! changeSearchAuthAttribute(IDStrings[i], locatorNames[i], newValues[i], menuItems[i], logFileWriter)){
				String message = "Couldn't get SA ID "+IDStrings[i]+" adjusted; ";
				verificationErrors.append(message);
				printToAllOutputs(message, logFileWriter);
				logFileWriter.close();
				Thread.sleep(2000);
				fail(message);
			}//end if
			if (locatorNames[i].contains("Select")) {
				newValues[i] = HandSOnTestSuite.searchAuthorizationUI.getSelectedText(locatorNames[i], logFileWriter);
			}
			else if (locatorNames[i].contains("Textfield")){
				By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SA General Page", locatorNames[i], logFileWriter);
				newValues[i] = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
			}//end else
			Thread.sleep(3000);
		}//end for loop
		String message = "*** method changeSearchAuthAttributes successfully ended  ***";
		printToAllOutputs(message, logFileWriter);
	}//end changeSearchAuthAttributes method
	
	/*
	 * This changes the attribute in one of the Search Authorizations that we open,
	 * and we will verify this attribute in the Recruitment(s) attached to this SA
	 * have changed as well.
	 */
	private boolean changeSearchAuthAttribute(String ID, String attLocatorName, String newValue, 
			String menuItem, BufferedWriter logFileWriter) throws Exception{
		String message = "*** method changeSearchAuthAttribute called with SA ID "+ID
				+", locator name "+attLocatorName+", new value "+newValue+", and menu item "+menuItem+" ***";
		printToAllOutputs(message, logFileWriter);

		Thread.sleep(3000);
		if (! switchToSearchAuthWindow(ID, logFileWriter)){
			message = "Couldn't switch to the Search Authorization Window for SA ID "+ID;
			printToAllOutputs(message, logFileWriter);
			return false;
		}//end if - couldn't even get the window
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(SearchAuthorizationUIController.GeneralWorksheetName, 
				attLocatorName, logFileWriter);
		if (attLocatorName.contains("Select")){
			message = "Attribute to be modified is in a drop-down select widget";
			printToAllOutputs(message, logFileWriter);
			Select select = new Select(UIController.driver.findElement(by));
			String currentSelectedOption = select.getFirstSelectedOption().getText();
			if (currentSelectedOption.equalsIgnoreCase(newValue)){
				printToAllOutputs("New value "+newValue+" is already selected, selecting another", logFileWriter);

				List<WebElement> options = select.getOptions();
				Iterator<WebElement> iterator = options.iterator();
				while(iterator.hasNext()){
					newValue = iterator.next().getText();
					if ((! newValue.equalsIgnoreCase(currentSelectedOption)) 
							&& (! newValue.equalsIgnoreCase("Select"))){
						printToAllOutputs("Selected option "+newValue+", instead", logFileWriter);
						UIController.selectElementInMenu(by, newValue);
						break;
					}//end if - it's a new value, so select it
				}//end while
				if (! attLocatorName.contains("Billet"))
					UIController.dismissAlert(logFileWriter);
			}//end if
			else{
				printToAllOutputs("Selected option "+newValue, logFileWriter);
				UIController.selectElementInMenu(by, newValue);
				if (! attLocatorName.contains("Billet")) {
					UIController.dismissAlert(logFileWriter);
					printToAllOutputs("Alert dismissed", logFileWriter);
				}
			}//end else
		}//end if
		else if (attLocatorName.contains("Textfield")){
			WebElement textfield = UIController.driver.findElement(by);
			if (textfield.getText().equalsIgnoreCase(newValue)){
				int increment = new Integer(newValue.replaceFirst("TBD", "")).intValue();
				increment++;
				newValue = "TBD"+increment;
				printToAllOutputs("The new value is already there - changing it to "+newValue, logFileWriter);
			}//end if - the new value is already there - modify it
			printToAllOutputs("Attribute to be modified is in a textfield - setting new value to "+newValue, logFileWriter);

			textfield.clear();
			textfield.sendKeys(newValue);
		}//end else if
		else{
			printToAllOutputs("WARNING!!! Attribute to be modified is in a widget that can't be identified", logFileWriter);
			return false;
		}//end else

		UIController.driver.findElement(By.id("buttonSubmit")).click();
		printToAllOutputs("saved successfully", logFileWriter);
		if (attLocatorName.contains("Billet")) {
			UIController.dismissAlert(logFileWriter);
			printToAllOutputs("Dismissed Alert", logFileWriter);
		}
		else {
			handleChangeAnnotationDialog(menuItem, verificationErrors, logFileWriter);
			printToAllOutputs("Handled Change Annotation Dialog", logFileWriter);
		}
		printToAllOutputs("Now switching to Search Authorization Lookup Window", logFileWriter);
		Thread.sleep(3000);
		switchToSearchAuthLookupWindow(logFileWriter);
		Thread.sleep(3000);
		printToAllOutputs("Now switching to Search Authorization Window for SA ID "+ID, logFileWriter);
		if (! switchToSearchAuthWindow(ID, logFileWriter)){
			return false;
		}//end if - couldn't even get the window
		Thread.sleep(3000);
		printToAllOutputs(" *** method changeSearchAuthAttribute completed successfully ***", logFileWriter);
		return true;
	}//end changeSearchAuthAttribute method
	
	public static void handleChangeAnnotationDialog(String menuItem, StringBuffer verificationErrors, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method handleChangeAnnotationDialog called ***");
		logFileWriter.newLine();
		String currentWindowHandle = UIController.driver.getWindowHandle();
		String currentURL = UIController.driver.getCurrentUrl();
		printToAllOutputs("Current Window handle is "+currentWindowHandle+", with current URL "+currentURL, logFileWriter);
		
		
		if (! UIController.switchWindowByURL(UIController.annotationPopupWindowURL, logFileWriter)){
			String message = "Could not switch to the Change Annotation Dialog;\n ";
			verificationErrors.append(message);
			logFileWriter.write(message);
			logFileWriter.newLine();
			return;
		}//end if - we didn't get to the Change Annotation Dialog - break off now
		else{
			logFileWriter.write("Successfully switched to the Change Annotation Dialog.");
			logFileWriter.newLine();
			try {
				UIController.selectElementInMenu(By.id("annotationReason"), menuItem);
			}
			catch(Exception e) {
				//maximize if we can't get to the change annotation dialog
				logFileWriter.write("Could not operate on the Change Annotation Dialog the first time because of Exception "+e.getMessage());
				logFileWriter.newLine();
				logFileWriter.write("Will maximize Change Annotation Dialog and try again.");
				logFileWriter.newLine();
				UIController.driver.manage().window().maximize();
				try {
						UIController.selectElementInMenu(By.id("annotationReason"), menuItem);
					}
					catch(Exception e2) {
						logFileWriter.write("Could not operate on the Change Annotation Dialog the second time because of Exception "+e2.getMessage());
						logFileWriter.newLine();
					}//end inner catch clause - Exception e2
			}//end outer catch clause - Exception e
			Thread.sleep(2000);
			
			// ERROR: Caught exception [ERROR: Unsupported command [addSelection | id=annotationReason | label=Cost Adjustment]]
			UIController.waitForElementPresent(By.id("annotationContent"));
			UIController.driver.findElement(By.id("annotationContent")).clear();
			UIController.driver.findElement(By.id("annotationContent")).sendKeys("Annotation");
			Thread.sleep(2000);
			UIController.waitAndClick(By.id("buttonSubmit"));
			Thread.sleep(10000);
		}//end else - we got the Change Annotation Dialog
		printToAllOutputs("The annotation dialog is dismissed - now attempting to switch back to original window ID "
					+currentWindowHandle+" with URL "+currentURL, logFileWriter);
		try {
			printToAllOutputs("Attempt will be made to dismiss alert immediately after annotation dialog is dismissed", logFileWriter);
			Thread.sleep(10000);
			Alert alert = UIController.driver.switchTo().alert();
			String alertMessage = alert.getText();
			alert.accept();
			printToAllOutputs("Alert was dismissed - alert message was "+alertMessage, logFileWriter);
		}
		catch(Exception e) {
			printToAllOutputs("Could not dismiss alert because of Exception "+e.getMessage(), logFileWriter);
			Thread.sleep(3000);
			printToAllOutputs("Attempting to dismiss alert by typing the Enter key", logFileWriter);
			try {
			    Robot robot = new Robot();
			    robot.keyPress(KeyEvent.VK_ENTER);
			    robot.keyRelease(KeyEvent.VK_ENTER);
				Thread.sleep(3000);
			}//end try clause
			catch(Exception e2) {
				printToAllOutputs("Could not hit the Enter key because of Exception "+e2.getMessage(), logFileWriter);
			}
		}//end outer catch clause - could not dismiss the alert the standard way
		try {
		    Robot robot = new Robot();
		    robot.keyPress(KeyEvent.VK_ENTER);
		    robot.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(3000);
		}//end try clause
		catch(Exception e2) {
			printToAllOutputs("Could not hit the Enter key because of Exception "+e2.getMessage(), logFileWriter);
		}
		printToAllOutputs("Now attempting to simulate a mouse click", logFileWriter);
		try {
		    Robot robot = new Robot();
		    robot.mousePress(InputEvent.BUTTON1_MASK);
		    robot.mouseRelease(InputEvent.BUTTON1_MASK);
			Thread.sleep(3000);
		}//end try clause
		catch(Exception e2) {
			printToAllOutputs("Could not click the mouse because of Exception "+e2.getMessage(), logFileWriter);
		}
		
		UIController.switchWindowByHandle(currentWindowHandle, true, logFileWriter);
	}//end handleChangeAnnotationDialog method
	
	private void selectSAByIDStrings(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method selectSAByIDStrings called ***");
		logFileWriter.newLine();
		switchToSearchAuthLookupWindow(logFileWriter);
		
		//we need to select Accepted and Inactive SA's through two multi-select widgets
		Thread.sleep(2000);
		UIController.driver.findElement(By.id("authorId")).clear();
		processSelection("searchStatus", "Active", logFileWriter);
		processSelection("recruitmentStatus", "No Offer", logFileWriter);
		UIController.waitAndClick(By.id("mybutton"));
		Thread.sleep(5000);
		//UIController.waitAndClick(By.linkText("LAST"));
		//Thread.sleep(5000);
		int lastIDIndex = 4;
		for (int i=0; i<lastIDIndex; i++){
			By by = By.linkText(IDStrings[i]);
			System.out.println("Now attempting to work with SA ID "+IDStrings[i]);
			boolean gotLink = UIController.waitAndClick(by);
			if (gotLink){
				String message = "Clicked on the link for SA ID "+IDStrings[i];
				System.out.println(message);
				logFileWriter.write(message);
				logFileWriter.newLine();
				Thread.sleep(5000);
				switchToSearchAuthWindow(IDStrings[i], logFileWriter);
				Thread.sleep(3000);
			}//end if
			else{//we didn't get the link with the SA ID
				String message = "Could not get the link for the SA with ID "+IDStrings[i]+"; ";
				System.out.println(message);
				verificationErrors.append(message);
				logFileWriter.write(message);
				logFileWriter.newLine();
				switchToSearchAuthLookupWindow(logFileWriter);
				return;
			}//end else - we didn't get the link with the SA ID
			logFileWriter.newLine();
			System.out.println("Now switching back to Search Authorization Lookup Window");
			switchToSearchAuthLookupWindow(logFileWriter);
		}//end for loop
		String message = "*** method selectSAByIDStrings completed successfully ***";
		System.out.println(message);
		logFileWriter.write(message);
		logFileWriter.newLine();
	}//end selectSAByIDStrings
	
	private void switchToSearchAuthLookupWindow(BufferedWriter logFileWriter) throws Exception{
		String searchAuthLookupURL = UIController.searchAuthorizationLookupURL+"#results";
		
		Thread.sleep(3000);//wait for things to reset so that the search 
		
		if (! UIController.switchWindowByURL(searchAuthLookupURL, logFileWriter, true)){
			String message = "Could not get to the results window - going to the regular lookup window";
			printToAllOutputs(message, logFileWriter);
			UIController.switchWindowByURL(UIController.searchAuthorizationLookupURL, logFileWriter, true);
		}//end if
		else{
			String message = "Successfully switched to the Lookup window with the results";
			printToAllOutputs(message, logFileWriter);
		}//else - we successfully got to the SA Lookup page with results already present
	}//end switchToSearchAuthLookupWindow method
	
	private void processPageByLink(String linkText, BufferedWriter logFileWriter) throws Exception{
		verificationErrors.append(UIController.processPageByLink(linkText, logFileWriter));
	}//end processPageByLink method
	
	public static boolean switchToRecruitmentWindow(String ID, StringBuffer verificationErrors, BufferedWriter logFileWriter) throws Exception{
		String message = HandSOnTestSuite.searchAuthorizationUI.switchToRecruitmentWindow(ID, logFileWriter);
		if (message.isEmpty()){
			logFileWriter.write("... switched to Recruitment ID "+ID+", as expected");
			logFileWriter.newLine();
		}
		else{
			verificationErrors.append(message);
			logFileWriter.write(message);
			return false;
		}//end else - there was an error
		try{
			UIController.driver.switchTo().alert().accept();
			logFileWriter.write("There was an alert present after switching to the Recruitment Window for Recruitment ID "+ID);
			logFileWriter.newLine();
		}
		catch(NoAlertPresentException e){
			logFileWriter.write("There was no alert present after switching to the Recruitment Window for Recruitment ID "+ID);
			logFileWriter.newLine();
		}
		return true;
	}//end switchToRecruitmentWindow method

	
	public static boolean switchToSearchAuthWindow(String ID, BufferedWriter logFileWriter) throws Exception{
		String message = HandSOnTestSuite.searchAuthorizationUI.switchToSearchAuthWindow(ID, logFileWriter);
		if (message.isEmpty()){
			printToAllOutputs("... and we switched to the search Authorization Window, as expected", logFileWriter);
			Thread.sleep(2000);
			verifyRecruitmentLinkExists(HandSOnTestSuite.searchAuthorizationUI.getByForName("FirstRecruitmentLink", logFileWriter), 
					ID, logFileWriter);					
			verifyRecruitmentLinkExists(HandSOnTestSuite.searchAuthorizationUI.getByForName("SecondRecruitmentLink", logFileWriter), 
					ID, logFileWriter);					
		}//end if
		else{
			printToAllOutputs(message, logFileWriter);
			verificationErrors.append(message);
			return false;
		}
		logFileWriter.newLine();
		return true;
		
	}//end switchToSearchAuthWindow method
	
	private static void verifyRecruitmentLinkExists(By by, String ID, BufferedWriter logFileWriter) throws Exception{
		if (UIController.isElementPresent(by)){
			logFileWriter.newLine();
			logFileWriter.write("***Recruitments are present***");
			logFileWriter.newLine();
			logFileWriter.newLine();
		}//end if
		else{
			String message = "Recruitments are not present for SA ID "+ID+"; ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
		}//end else
	}//end verifyRecruitmentLink method
	
	private void processSelection(String selectorID, String menuItem, BufferedWriter logFileWriter) throws Exception{
		boolean gotSelectedItem = UIController.waitAndSelectElementInMenu(By.id(selectorID), menuItem);
		if (! gotSelectedItem){
			String message = "Could not select "+selectorID+", could not select "+menuItem+"; ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
		}//end if
		else {
			logFileWriter.write("Got menu item "+menuItem+" in Select ID "+selectorID);
			logFileWriter.newLine();
		}//end else
	}//end processSelection method
	
	private static void printToAllOutputs(String message, BufferedWriter logFileWriter) throws Exception{
		System.out.println(message);
		logFileWriter.write(message);
		logFileWriter.newLine();
	}//end printToAllOutputs method
	
}//end Class
