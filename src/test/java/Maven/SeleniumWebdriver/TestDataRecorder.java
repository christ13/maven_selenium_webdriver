package Maven.SeleniumWebdriver;

import java.io.File;
import java.io.FileOutputStream;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
//import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TestDataRecorder {
	
	private static SSExcelReader retentionDataReader;
	//private static FileOutputStream fileOut;
	//private static File file;
	public static final String RetentionDataFileName = "RetentionData.xls";
	public static final String RetentionHighLevelWorksheetName = "High Level Retention Data";
	public static final int RetentionHighLevelWorksheetHeaderRow = 0;
	static int RetentionHighLevelWorksheetLastRow;
	public static final String RetentionFilePath = ReportRunner.logDirectoryName +"/"+RetentionDataFileName;

	public static final String[] highLevelDataColumns = {"Test Case ID", "Name of Faculty", 
			"HandSOn ID", "Retention ID", "Zero or Nonzero Raises", 
			"Language of Commitment", "Administrative Appointment", 			
			"Today’s Date", "Retention Effective Date", "Manual Salary Log Effective Date"};
	
	//these indices are common to both high level and low level data columns
	public static final int TEST_CASE_ID_INDEX = 0;
	public static final int NAME_OF_FACULTY_INDEX = 1;
	public static final int HANDSONID_INDEX = 2;
	public static final int RETENTION_ID_INDEX  = 3;
	public static final int ZERO_NONZERORAISE_INDEX  = 4;
	public static final int LANGUAGE_OF_COMMITMENT_INDEX = 5;
	
	//these indices apply only to high level data columns
	public static final int ADM_APPT_INDEX = 6;
	public static final int TODAYS_DATE_INDEX = 7;
	public static final int RETENTION_EFFECTIVE_DATE_INDEX = 8;
	public static final int MANUAL_SALARY_LOG_EFFECTIVE_DATE_INDEX = 9;
	
	//these columns assume an initial manual salary increase one day, two days before the present day, 
	// and a Retention increase the following day (the day before the present day)
	public static final String[] lowLevelDataColumns = {
			//the following seven columns appear in the first row of the low level test case data file
			"Test Case ID", "Name of Faculty", "HandSOn ID", "Retention ID", "Zero or Nonzero Raises",
			"Language of Commitments", "FTE", 
			//the following columns come in pairs, each pair on a separate row
			// the row can be calculated by taking the index, subtracting six, adding one if the result is odd,
			// then dividing the result by two, and then multiplying the result by three
			"Initial 100% FTE Salary (before increases)", "FTE Adjusted Initial Salary (before increases)", 
			"100% FTE Manual Salary Increase Amount", "FTE Adjusted Manual Salary Increase Amount", 
			"100% FTE Salary After Manual Salary Log Increase", "FTE Adjusted Salary After Manual Salary Log Increase",
			"Initial 100% FTE Retention Increase Amount", "Initial FTE Adjusted Retention Increase Amount",
			"Revised 100% FTE Retention Increase Amount", "Revised FTE Adjusted Retention Increase Amount", 
			"100% FTE Salary After Retention Increase", "FTE Adjusted Salary After Retention Increase"
	};
	//these indices apply only to low level data columns
	public static final int FACULTY_FTE_INDEX = 6;
	public static final int HUNDREDPCT_FTE_INITIAL_SALARY_INDEX = 7;
	public static final int FTE_ADJUSTED_INITIAL_SALARY_INDEX = 8;
	public static final int HUNDREDPCT_FTE_MANUAL_SALARY_INCREASE_AMT_INDEX = 9;
	public static final int FTE_ADJUSTED_MANUAL_SALARY_INCREASE_AMT_INDEX = 10;
	public static final int HUNDREDPCT_FTE_SALARY_AFTER_MANUAL_INCREASE_INDEX = 11;
	public static final int FTE_ADJUSTED_SALARY_AFTER_MANUAL_INCREASE_INDEX = 12;
	public static final int INITIAL_HUNDREDPCT_FTE_RETENTION_INCREASE_AMOUNT_INDEX = 13;
	public static final int INITIAL_FTE_ADJUSTED_RETENTION_INCREASE_AMOUNT_INDEX = 14;
	public static final int REVISED_HUNDREDPCT_FTE_RETENTION_INCREASE_AMOUNT_INDEX = 15;
	public static final int REVISED_FTE_ADJUSTED_RETENTION_INCREASE_AMOUNT_INDEX = 16;
	public static final int HUNDREDPCT_FTE_SALARY_AFTER_RETENTION_INCREASE_INDEX = 17;
	public static final int FTE_ADJUSTED_SALARY_AFTER_RETENTION_INCREASE_INDEX = 18;
	
	public TestDataRecorder() {
		// TODO Auto-generated constructor stub
	}
	
	public static void initializeRetentionDataFile() throws Exception{
		retentionDataReader = new SSExcelReader(RetentionFilePath, RetentionHighLevelWorksheetHeaderRow);
		retentionDataReader.addSheet(RetentionHighLevelWorksheetName);

		for (int i=1; i<=12; i++) {
			retentionDataReader.addSheet("Test Case Data ID "+i);
		}//end for loop
		System.out.println("Workbook now has "+retentionDataReader.getNumberOfSheets()+" worksheets");

		initializeHighLevelData();
		initializeLowLevelData();
		retentionDataReader.closeWorkbook();
	}//end initializeRetentionDataFile method
	
	public static void initializeLowLevelData() throws Exception{
		
		for (int i=1; i<=12; i++) {
			//String testCaseName = highLevelDataColumns[TEST_CASE_ID_INDEX]+" "+i;
			String worksheetName = "Test Case Data ID "+i;
			System.out.println("Worksheet "+worksheetName+" is getting "+lowLevelDataColumns.length+" added");
			//the first six columns are all added on the first row, after that, every pair of columns on a separate row
			for(int j=0; j<lowLevelDataColumns.length; j++) {
				System.out.println("Header Row has been set to "+setLowLevelHeaderRow(i, j));
				System.out.println("Now adding column name "+lowLevelDataColumns[j]);
				retentionDataReader.addColumn(worksheetName, lowLevelDataColumns[j]);
			}//end for loop
		}//end for loop
		retentionDataReader.setRowForColumnNames(RetentionHighLevelWorksheetHeaderRow);
	}//end initializeLowLevelData method
	
	public static int setLowLevelHeaderRow(int worksheetIndex, int columnIndex) {
		String worksheetName = "Test Case Data ID "+worksheetIndex;
		int headerRowNumber = 0;
		if(columnIndex>6) {
			//if the index is > 6, the columns come in pairs, each pair on a separate row
			// the row can be calculated by taking the index, subtracting six, adding one if the result is odd,
			// then dividing the result by two, and then multiplying the result by three
			int increment = columnIndex - 6;
			if (increment % 2 == 1) 
					increment++;
			increment = (increment/2)*3;
			headerRowNumber += increment;
		}//end if
		System.out.println("Header Row for worksheet "+worksheetName+", column "+lowLevelDataColumns[columnIndex]
				+" has been set to "+headerRowNumber);
		retentionDataReader.setRowForColumnNames(headerRowNumber);
		return headerRowNumber;
	}//end getLowLevelHeaderRow method
	
	public static void setInitialLowLevelTestCaseDatum(int testCaseID, int dataIndex) throws Exception{
		String testCaseName = highLevelDataColumns[TEST_CASE_ID_INDEX]+" "+testCaseID;
		//retrieve the data from the high level test case data worksheet
		String datum = getTestCaseData(testCaseName, highLevelDataColumns[dataIndex]);
		setLowLevelTestCaseDatum(testCaseID, dataIndex, datum);
	}//end processLowLevelTestCaseDatum method
	
	public static void setLowLevelTestCaseDatum (int testCaseID, int dataIndex, String datum) throws Exception{
		String worksheetName = "Test Case Data ID "+testCaseID;
		System.out.println("Inserting "+lowLevelDataColumns[dataIndex]+" value of "+datum+" into worksheet "+worksheetName);
		//now write it to the low level test case data worksheet
		int headerRowNumber = setLowLevelHeaderRow(testCaseID, dataIndex);
		retentionDataReader.setCellData(worksheetName, lowLevelDataColumns[dataIndex], headerRowNumber+1, datum);
		retentionDataReader.setRowForColumnNames(RetentionHighLevelWorksheetHeaderRow);
	}//end setLowLevelTestCaseDatum method

	
	/*
	 * We must add the following columns to the high level Master Salary Setting Worksheet:
	 * Test Case ID, Name of Faculty, HandSOnID, Zero or Nonzero Raises, Administrative Appointment, 
	 * Retention ID, Language of Commitment
	 * Today’s Date, Retention Effective Date, Manual Salary Log Effective Date 
	 */
	public static void initializeHighLevelData() throws Exception {

		retentionDataReader.addMultipleColumns(RetentionHighLevelWorksheetName, highLevelDataColumns);

		//initialize Test Case ID, Today's Date, Retention Effective Date and Manual Salary Log Effective Date columns
		for (int i=1; i<=RetentionMasterDataController.testCaseDataArray.length; i++) {
			int row = i+1;
			String TestCaseIDName = highLevelDataColumns[0] + " "+ i;
			/*
			setCellData parameters are String sheetName,String colName, int rowNum, String data
			In this case, sheetName = RetentionHighLevelWorksheetName, colName = highLevelDataColumns[TEST_CASE_ID_INDEX],
			rowNum = testCaseID, data = TestCaseIDName
			*/
			retentionDataReader.setCellData(RetentionHighLevelWorksheetName, 
					highLevelDataColumns[TEST_CASE_ID_INDEX], row, TestCaseIDName);
			/*
			 In this case, sheetName = RetentionHighLevelWorksheetName, colName = highLevelDataColumns[TODAYS_DATE_INDEX],
			rowNum = testCaseID, data = getDateValue("TODAY")
			 */
			retentionDataReader.setCellData(RetentionHighLevelWorksheetName, 
					highLevelDataColumns[TODAYS_DATE_INDEX], row, getDateValue("TODAY"));
			//initialize Zero vs. Nonzero columns with "Nonzero" values
			/*
			 In this case, sheetName = RetentionHighLevelWorksheetName, colName = highLevelDataColumns[ZERO_NONZERORAISE_INDEX],
			rowNum = testCaseID, data = {"Nonzero", "Zero"}
			 */
			if (row < 8)
				retentionDataReader.setCellData(RetentionHighLevelWorksheetName, 
					highLevelDataColumns[ZERO_NONZERORAISE_INDEX], row, "Nonzero");
			//initialize Zero vs. Nonzero columns with "Zero" values
			else
				retentionDataReader.setCellData(RetentionHighLevelWorksheetName, 
						highLevelDataColumns[ZERO_NONZERORAISE_INDEX], row, "Zero");
			//Determine the value of the Language of Commitment according to the row number
			String languageOfCommitment = new String();
			if ((row % 4) == 2)
				languageOfCommitment = "% Increase";
			else if ((row % 4) == 3)
				languageOfCommitment = "$ Increase";
			else if ((row % 4) == 0)
				languageOfCommitment = "Total Amount";
			else if ((row % 4) == 1)
				languageOfCommitment = "% Increase Above Pool";
			/*
			 In this case, sheetName = RetentionHighLevelWorksheetName, colName = highLevelDataColumns[LANGUAGE_OF_COMMITMENT_INDEX],
			rowNum = testCaseID, data = {"% Increase", "$ Increase", "Total Amount", "% Increase Above Pool"}
			 */
			retentionDataReader.setCellData(RetentionHighLevelWorksheetName, 
					highLevelDataColumns[LANGUAGE_OF_COMMITMENT_INDEX], row, languageOfCommitment);
			
			String adminApptOrNot = "NONE";
			if (row > 11)
				adminApptOrNot = "Senior Associate Dean";
			else if (row > 9)
				adminApptOrNot = "Department Chair";
			/*
			 In this case, sheetName = RetentionHighLevelWorksheetName, colName = highLevelDataColumns[ADM_APPT_INDEX],
			rowNum = testCaseID, data = {"NONE", "Senior Associate Dean", "Department Chair"}
			 */
			retentionDataReader.setCellData(RetentionHighLevelWorksheetName, 
					highLevelDataColumns[ADM_APPT_INDEX], row, adminApptOrNot);
			
			/*
			 In this case, sheetName = RetentionHighLevelWorksheetName, colName = highLevelDataColumns[RETENTION_EFFECTIVE_DATE_INDEX],
			rowNum = testCaseID, data = getDateValue("YESTERDAY")
			 */
			retentionDataReader.setCellData(RetentionHighLevelWorksheetName, 
					highLevelDataColumns[RETENTION_EFFECTIVE_DATE_INDEX], row, getDateValue("YESTERDAY"));
			/*
			 In this case, sheetName = RetentionHighLevelWorksheetName, 
			 colName = highLevelDataColumns[MANUAL_SALARY_LOG_EFFECTIVE_DATE_INDEX],
			 rowNum = testCaseID, data = getDateValue("DAY BEFORE YESTERDAY")
			 */
			retentionDataReader.setCellData(RetentionHighLevelWorksheetName, 
					highLevelDataColumns[MANUAL_SALARY_LOG_EFFECTIVE_DATE_INDEX], row, getDateValue("DAY BEFORE YESTERDAY"));	
		}//end for loop
		// initialize Zero vs. Nonzero values
		
	}// end initializeHighLevelData method
 	
	public static boolean setTestCaseData(String testCaseID, String columnName, String columnValue) throws Exception{
		System.out.println("Setting column "+columnName+" to "+columnValue+" for Test Case ID "+testCaseID);
		int rowNum = new Integer(testCaseID.substring(highLevelDataColumns[TEST_CASE_ID_INDEX].length()+1)).intValue()+1;
		retentionDataReader.setCellData(RetentionHighLevelWorksheetName, columnName, rowNum, columnValue);
		String dataValue = retentionDataReader.getCellData(RetentionHighLevelWorksheetName, columnName, rowNum);
		return (dataValue.equalsIgnoreCase(columnValue));
	}//end setTestCaseData method
	
	public static String getTestCaseData(String testCaseID, String columnName) throws Exception{
		int rowNum = new Integer(testCaseID.substring(highLevelDataColumns[TEST_CASE_ID_INDEX].length()+1)).intValue()+1;
		String dataValue = retentionDataReader.getCellData(RetentionHighLevelWorksheetName, columnName, rowNum);
		System.out.println("returning "+columnName+" value of "+dataValue);
		return dataValue;
	}//end getTestCaseData method
	
	private static String getDateValue(String whichDate) throws Exception {
		Calendar calendar = Calendar.getInstance();
		
		//if (whichDate.equalsIgnoreCase("TODAY")), don't adjust the Calendar - leave it as is			
		if (whichDate.equalsIgnoreCase("YESTERDAY"))
			calendar.add(Calendar.DAY_OF_YEAR, -1);
		else if (whichDate.equals("DAY BEFORE YESTERDAY"))
			calendar.add(Calendar.DAY_OF_YEAR, -2);
		
		return new SimpleDateFormat("MM/dd/yyyy").format(calendar.getTime());
	}//end getDateValue method

}//end Class
