package Maven.SeleniumWebdriver;


import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
/*
@RunWith(Suite.class)
@Suite.SuiteClasses({
	FacultySelector.class,
	RetentionCreator.class,
	PreAdjustmentDataVerificationTest.class,
	PostAdjustmentDataVerificationTest.class
})
*/

public class RetentionTestSuite {
	
	private RetentionMasterDataController master;
	private FacultySelector facultySelector;
	private RetentionCreator retentionCreator;
	private RetentionUIController retentionUIController;
	private FacultyUIController facultyUIController;
	private JUnitCore junit;
	
	public RetentionTestSuite(JUnitCore junit) throws Exception {
		this.junit = junit;
		master = new RetentionMasterDataController();
		facultySelector = new FacultySelector();
		retentionCreator = new RetentionCreator();
		
		//retentionUIController = new RetentionUIController();

	}//end constructor

	@Before
	public void setUp() throws Exception {
		
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws Exception {
		master.runAllReports("before");
		junit.run(PreAdjustmentDataVerificationTest.class);
		junit.run(PostAdjustmentDataVerificationTest.class);
		master.closeDataFile();
	}//end test class

}//end Class
