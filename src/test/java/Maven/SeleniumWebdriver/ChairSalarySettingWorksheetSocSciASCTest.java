package Maven.SeleniumWebdriver;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ChairSalarySettingWorksheetSocSciASCTest extends
		ChairSalarySettingWorksheetASCTest {

	@Before
	public void setUp() throws Exception {
		clusterName = "Social Sciences Cluster";
		configureOutput();
		super.setUp();
	}

	private void configureOutput() throws Exception{
		myExcelReader = TestFileController.ChairSalarySettingWorksheetSocialSciences;
		mySheetName = TestFileController.ChairSalarySettingWorksheetWorksheetName;
		rowForColumnNames = TestFileController.ChairSalarySettingWorksheetSocialSciencesHeaderRow;
		verificationErrors = new StringBuffer();
		testName = "ChairSalarySettingWorksheetSocSciASCTest";
		testEnv = "Test";
		testCaseNumber = "037";
		testDescription = "ASC Test of Chair Salary Setting Worksheet for Social Sciences";
		logFileName = "Chair Salary Setting SocSci ASC Test.txt";
		logFileWriter = TestFileController.getLogFileWriter(logDirectoryName  +"/" + logFileName);
	}//end configureOutput method
		

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void test() throws Exception{
		super.test();
	}

}
