package Maven.SeleniumWebdriver;

import static org.junit.Assert.fail;

import java.io.BufferedWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
/*
 * The goal of this test Class is to instantiate the new SA's and verify that they have
 * the data as expected.
 * The General Tab is quite easy.  One text field per value.  
 * 
 * The Costs Tab is much more complex and error-prone.  We have to get the actual values
 * from the Costs tab, get the expected values from the Manage Standard Costs page,
 * and verify that the expected and actual values are the same.
 * 
 * We have to get the Expected Costs first.  We have to find out (1) what kinds of Costs
 * are expected, and (2) once we find out what the Costs, are find out what the amounts 
 * associated with these Costs are.
 * 
 * In more detail, we need to determine:
 *  (1) What Costs match our SA exactly (Lab, Level, Department, and Cluster)
 *  (2) What Costs match our SA Level, Dept and Cluster only - add if the Cost is not already there
 *  (3) What Costs match our SA Dept and Cluster only - add if the Cost is not already there
 *  (4) What Costs match our SA Cluster only - add if the Cost is not already there.
 *  
 *  With each step (1-4), we must determine (a) whether or not that type of Cost is already
 *  there, (b) if it's not already there, what the cost amount is, and (3) if it's not already 
 *  there, save both the Cost Type and Amount.
 *  
 *  We believe that the best way to approach this is to iterate through the entire page, 
 *  four times, each time checking to see if the Cost type is already there, and adding it
 *  if it's not.  
 *  We will store Cost types in a HashMap<String, Integer>.  
 *  Use the putIfAbsent(Key, value) to put the Cost in there.  If it returns a value that is
 *  already there, don't put it in there.  
 * 
 * Getting the actual values involves (1) finding out what kinds of Costs there are (and
 * verifying that those kinds of Costs are expected)
 */
public class NewSearchAuthorizationTest {

	protected static String testName;
	protected static String testCaseNumber;
	protected static String testDescription;
	protected static String testEnv;
	protected static StringBuffer verificationErrors;
	protected BufferedWriter logFileWriter;
	protected String[] cluster = new String[HandSOnTestSuite.numberOfNewData];
	protected String[] dept = new String[HandSOnTestSuite.numberOfNewData];
	protected String[] level = new String[HandSOnTestSuite.numberOfNewData];
	protected String[] labYN = new String[HandSOnTestSuite.numberOfNewData];
	
	@Before
	public void setUp() throws Exception {
		testName = "NewSearchAuthorizationTest";
		testEnv = "Test";
		testCaseNumber = "064";
		testDescription = "Test of New Search Authorizations";
		verificationErrors = new StringBuffer();
	}

	@After
	public void tearDown() throws Exception {
		TestFileController.writeTestResults(testEnv, testCaseNumber, testName, testDescription, verificationErrors);
		logFileWriter.newLine();
		logFileWriter.close();
	}//end tearDown method

	@Test
	public void test() throws Exception{
		String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		logFileWriter = TestFileController.getLogFileWriter(logFileName);
		
		
		boolean[] testMult = {true, false, false, false, true, false, false};
		String[] rank = {"Full Professor", "Assistant Professor", 
				"Associate Professor", "Szego Assistant Professor", "Full Professor", 
				"Lecturer", "Full Professor"};
		String[] startDate = {"09/01/2019", "08/31/2019", "09/02/2019", "09/01/2020", 
				"09/02/2020", "08/31/2020", "09/01/2019"};
		String description = "Test Case";
		String[] dept = {"Mathematics", "Mathematics", "BioPhysics", "Iranian Studies", 
				"English", "Economics", "Economics"};
		String[] subfield = {"Tenth Dimensional Modeling", "", "", "Early Influences", 
				"Romantic Period", "Economic Impact of National Optimism", "Macroeconomics"};
		String billet = "TBD";
		String[] fte = {"100", "100", "80", "100", "50", "100", "100"};
		boolean[] addCompOffer = {true, false, true, true, false, true, false};
		
		//for (int i=0; i<2; i++){
		for (int i=0; i<HandSOnTestSuite.numberOfNewData; i++){
			setInitialSAConfig(logFileWriter);
			setRankSelection(rank[i], testMult[i], logFileWriter);
			setStartDateAndInitialFY(startDate[i], testMult[i], logFileWriter);
			selectProbYearAndValues( logFileWriter);
			setDescription(logFileWriter, description);
			selectDeptAndLab(dept[i], testMult[i], logFileWriter);
			enterSubfieldInfo(subfield[i], logFileWriter);
			enterBilletInfo(billet, logFileWriter);
			if (addCompOffer[i]) addCompOffer(logFileWriter);
			HandSOnTestSuite.searchAuthorizationIDs[i] = testSaveFunction(fte[i], logFileWriter);
			Thread.sleep(2000);
			switchToCostsTab(logFileWriter);
			Thread.sleep(2000);
			switchToGeneralTab(logFileWriter);
			Thread.sleep(2000);
			preserveCostVariables(i, logFileWriter);
			
			//open the standard Costs window and set it to Recruitments
			UIController.switchToAdministrationPage(logFileWriter);
			if (! UIController.switchWindowByURL(UIController.manageStdCostsURL, logFileWriter)){
				HandSOnTestSuite.startPageUI.openRecruitmentStandardCostsPage(logFileWriter);
			}//end if
			Thread.sleep(5000);
			
			HashMap<String, Integer> expectedCosts 
				= HandSOnTestSuite.stdCostMgrUI.getExpectedStandardCosts(HandSOnTestSuite.searchAuthorizationIDs[i], 
						cluster[i], dept[i], level[i], labYN[i], true, logFileWriter);
			Thread.sleep(3000);
			if (! UIController.switchWindowByURL(UIController.newSearchAuthorizationURL, logFileWriter)){
				if (! bringUpSearchAuthorization(HandSOnTestSuite.searchAuthorizationIDs[i], logFileWriter)){
					String message = "Couldn't switch to the new SA window for index "+i;
					logFileWriter.write(message);
					logFileWriter.newLine();
					System.out.println(message);
					verificationErrors.append(message);
					Thread.sleep(3000);
					fail(message);
				}//end inner if - the backup plan didn't work either
				else {
					Thread.sleep(3000);
					By by = By.id("costs_a");
					UIController.waitAndClick(by);
					Thread.sleep(3000);
				}
			}//end if - the initial plan didn't work
			else{
				logFileWriter.write("Successfully switched to the Search Authorization for index "+i);
				logFileWriter.newLine();
			}//end else
			Thread.sleep(3000);
			String initialFY = HandSOnTestSuite.searchAuthorizationUI.getInitialFY(logFileWriter);

			UIController.switchWindowByURL(UIController.startPage, logFileWriter);
			switchToCostsTab(logFileWriter);
			HashMap<String, Integer> actualCosts 
				= HandSOnTestSuite.searchAuthorizationUI.getActualCosts(logFileWriter);
			boolean costsMatch = HandSOnTestSuite.searchAuthorizationUI.costsMatch(expectedCosts, actualCosts, logFileWriter);
			verifyCostsMatch(costsMatch, HandSOnTestSuite.searchAuthorizationIDs[i], initialFY, logFileWriter);
			//verificationErrors.append(HandSOnTestSuite.searchAuthorizationUI.verifyInitialFY(initialFY, logFileWriter));		
			UIController.switchWindowByURL(UIController.startPage, logFileWriter);
			switchToCostsTab(logFileWriter);
			verifyBlankFields(logFileWriter);
			verifyLangCommitments(logFileWriter);
			switchToGeneralTab(logFileWriter);
			Thread.sleep(2000);
			clickSave(logFileWriter);
			Thread.sleep(2000);
			boolean isClosed = clickClose(i, logFileWriter);
			if (isClosed) {
				logFileWriter.write("Successfully closed the window for new SA with ID "+HandSOnTestSuite.searchAuthorizationIDs[i]);
				logFileWriter.newLine();
			}
			else {
				logFileWriter.write("Could not close the window for new SA with ID "+HandSOnTestSuite.searchAuthorizationIDs[i]);
				logFileWriter.newLine();
			}
		}//end for loop
		UIController.switchToAdministrationPage(logFileWriter);
		By by = By.linkText("Operations");
		UIController.waitAndClick(by);
		Thread.sleep(5000);
	}//end test method
	
	public static boolean bringUpSearchAuthorization(String ID, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** bringUpSearchAuthorization method called ***");
		logFileWriter.newLine();

		if (! UIController.switchWindowByURL(UIController.searchAuthorizationLookupURL, logFileWriter)){
			UIController.switchToStartPage(logFileWriter);
			//open the Search Authorization Page
			NewRecruitmentTest.processPageByLink("Search Authorization Lookup", verificationErrors, logFileWriter);
			Thread.sleep(3000);
			UIController.switchWindowByURL(UIController.searchAuthorizationLookupURL, logFileWriter);
		}//end if
		Thread.sleep(3000);
		UIController.driver.findElement(By.linkText("CLEAR")).click();
		//select "Active"
		By by = By.id("authorId");
		UIController.driver.findElement(by).sendKeys(ID);
		Thread.sleep(2000);
		by = By.id("mybutton");
		UIController.driver.findElement(by).click();		
		Thread.sleep(6000);
		by = By.linkText(ID);
		UIController.driver.findElement(by).click();
		Thread.sleep(6000);
		return UIController.switchWindowByURL(UIController.newSearchAuthorizationURL+"?authId="+ID, logFileWriter);
	}//end bringUpSearchAuthorization method

	public void verifyLangCommitments(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method verifyLangCommitments method ***");
		logFileWriter.newLine();
		String[] locatorNames = {"IncreasePercentTextField", "IncreaseAmtTextField", 
				"IncreaseTotalAmtTextField", "IncreasePctAbovePoolTextField"};
		String[] contents = {"10", "10000", "10000", "10"};
		String[] menuItems = {"% Increase", "$ Increase", "Total Amount", "% Increase Above Pool"};
		
		By selectBy = HandSOnTestSuite.searchAuthorizationUI.getByForName(SearchAuthorizationUIController.CostsWorksheetName, 
				"LangCommitmentsSelect", logFileWriter);
		for (int i=0; i< locatorNames.length; i++){
			UIController.selectElementInMenu(selectBy, menuItems[i]);
			Thread.sleep(2000);
			if ((! isTextFieldEditable (contents[i], locatorNames[i], logFileWriter)) 
					|| (! isTextFieldEditable("1000", "IncreaseDeptContribTextField", logFileWriter))){
				String message = locatorNames[i] +" text field is not enabled; ";
				verificationErrors.append(message);
				logFileWriter.write(message);
				logFileWriter.newLine();
			}//end if - text field is editable, as expected
			else{
				logFileWriter.write("Successfully sent "+contents[i]+" to "+locatorNames[i]);
				logFileWriter.newLine();
			}//end else - text field is not editable
		}//end for loop
	}//end verifyLangCommitments method
	
	public boolean isTextFieldEditable(String content, String locatorName, BufferedWriter logFileWriter) throws Exception{
		By textfieldBy = HandSOnTestSuite.searchAuthorizationUI.getByForName(SearchAuthorizationUIController.CostsWorksheetName, 
				locatorName, logFileWriter);
		WebElement textfield = UIController.driver.findElement(textfieldBy);
		if (textfield.isEnabled()){
			textfield.sendKeys(content);
			return true;
		}//end if 
		else
			return false;
	}//end isTextFieldEditable method
	
	public void verifyBlankFields(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method verifyBlankFields called ***");
		logFileWriter.newLine();
		String[] fields = {"DescriptionTextArea", "DataSourceTextArea", "DateTextField"};
		for (int i=0; i<fields.length; i++){
			if (! HandSOnTestSuite.searchAuthorizationUI.verifyBlankField(fields[i], logFileWriter)){
				String message = fields[i]+" is not blank, as expected";
				verificationErrors.append(message);
				logFileWriter.write(message);
				logFileWriter.newLine();
			}//end if - it is not blank, fail the test and note the error
			else {
				logFileWriter.write(fields[i]+" is blank, as expected");
				logFileWriter.newLine();
			}//end else
		}//end for loop
	}//end verifyBlankFields method
	
	public void verifyCostsMatch(boolean costsMatch, String ID, String initialFY, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		if (costsMatch)
		logFileWriter.write("Search Authorization ID "+ ID
				+ " has Standard Costs as expected");
		else{
			String message = "MISMATCH Search Authorization ID "+ID 
					+ " DOES NOT HAVE Standard Costs as expected; ";
			logFileWriter.write(message);
			verificationErrors.append(message);
		}//end else - costs do not match
		logFileWriter.newLine();
	}//end verifyCostsMatch method
	

	
	public void enterSubfieldInfo(String subfieldText, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method enterSubfieldInfo called with subfield text "+subfieldText+" ***");
		logFileWriter.newLine();
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SubfieldTextfield", logFileWriter);
		UIController.waitForElementPresent(by);
		
		UIController.driver.findElement(by).sendKeys(subfieldText);
	}//end enterSubfieldInfo method
	
	public void preserveCostVariables(int index, BufferedWriter logFileWriter) throws Exception{
		cluster[index] = HandSOnTestSuite.searchAuthorizationUI.getCluster(logFileWriter);
		logFileWriter.write("Cluster is determined to be "+cluster[index]);
		logFileWriter.newLine();
		dept[index] = HandSOnTestSuite.searchAuthorizationUI.getSelectedText("DeptSelect", logFileWriter);
		logFileWriter.write("Department is determined to be "+dept[index]);
		logFileWriter.newLine();
		level[index] = HandSOnTestSuite.searchAuthorizationUI.getFacultyLevel(logFileWriter);
		logFileWriter.write("Level is determined to be "+level[index]);
		logFileWriter.newLine();
		labYN[index] = HandSOnTestSuite.searchAuthorizationUI.getLab(logFileWriter);
		logFileWriter.write("Lab (Yes/No) is determined to be " + labYN[index]);
		logFileWriter.newLine();
	}//end outputCostVariables method
	
	public void switchToGeneralTab(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("Now switching to General tab");
		logFileWriter.newLine();
		
		UIController.switchWindowByURL(UIController.newSearchAuthorizationURL, logFileWriter);
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("GeneralTabLink", logFileWriter);
		UIController.waitForElementPresent(by);
		
		if (UIController.waitAndClick(by))
			logFileWriter.write("General tab is selected, as expected");
		else logFileWriter.write("ERROR - General tab cannot be selected");
	}//end switchToGeneralTab method
	
	public void switchToCostsTab(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("Now switching to Costs tab");
		logFileWriter.newLine();
		
		UIController.switchWindowByURL(UIController.newSearchAuthorizationURL, logFileWriter);
		Thread.sleep(2000);
		if (! UIController.driver.getCurrentUrl().contains(UIController.newSearchAuthorizationURL))
			UIController.switchWindowByURL(UIController.existingSearchAuthorizationURL, logFileWriter);
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("CostsTabLink", logFileWriter);
		UIController.waitForElementPresent(by);
		
		if (UIController.waitAndClick(by))
			logFileWriter.write("Costs Template tab is selected, as expected");
		else logFileWriter.write("ERROR - Costs Template tab cannot be selected");
	}//end switchToCostsTab method
	
	public void addCompOffer (BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method addCompOffer invoked - we will add a Competitive Offer ***");
		logFileWriter.newLine();
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("OpenCompOfferButton", logFileWriter);
		UIController.driver.findElement(by).click();
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("AddCompOfferButton", logFileWriter);
		UIController.waitForElementPresent(by);
		UIController.driver.findElement(by).click();
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("CompOfferInstitutionSelect", logFileWriter);
		UIController.waitForElementPresent(by);
		String compOfferUniv = "Adelphi University";
		logFileWriter.write("Setting Competitive Offer University to be "+compOfferUniv);
		UIController.waitAndSelectElementInMenu(by, compOfferUniv);
		
		String[] locatorNames = HandSOnTestSuite.searchAuthorizationUI.getLocatorNamesBelowLocator(SearchAuthorizationUIController.GeneralWorksheetName, 
				"CompOfferInstitutionSelect", logFileWriter);
		for(int i=0; i<locatorNames.length; i++){
			logFileWriter.write("Now attempting to work with Comp Offer field "+locatorNames[i]);
			logFileWriter.newLine();
			by = HandSOnTestSuite.searchAuthorizationUI.getByForName(locatorNames[i], logFileWriter);
			if (locatorNames[i].contains("Amt"))
				UIController.driver.findElement(by).sendKeys("10000");
			else if (locatorNames[i].contains("Notes"))
				UIController.driver.findElement(by).sendKeys("Test Notes for "+locatorNames[i]);
			else{
				logFileWriter.write("Could not determine what to enter for Comp Offer field "+locatorNames[i]);
				logFileWriter.newLine();
			}//end else
		}//end for - iterating through the locator names
	}//end addCompOffer method
	
	public String testSaveFunction (String fte, BufferedWriter logFileWriter) throws Exception{
		//now, try to save and get the appropriate error messages
		if (! saveWithError(logFileWriter)){
			logFileWriter.write("Error messages are not correct");
			logFileWriter.newLine();
		}//end if
		
		//now, enter the FTE and save
		if (enterFTE(fte, logFileWriter))
			logFileWriter.write("FTE is entered correctly");
		else
			logFileWriter.write("ERROR: FTE is not correctly entered");
		logFileWriter.newLine();
		
		//now, attempt to save - it should be successful
		String newSA = saveWithoutError(logFileWriter);
		if (! newSA.isEmpty())
			logFileWriter.write("Save was successful - new SA is "+newSA);
		else
			logFileWriter.write("ERROR: Save was not successful - there is no new SA");
		logFileWriter.newLine();
		
		return newSA;
	}//end testSaveFunction method
	
	public void selectDeptAndLab(String dept, boolean testMult, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("**** Now testing Department Selection and Lab auto-select ****");
		logFileWriter.newLine();
		logFileWriter.newLine();
		
		if (! setDepartmentAndVerifyLab(dept, testMult, logFileWriter)){
			logFileWriter.write("Department and Lab auto-select is not correct.");
			logFileWriter.newLine();
		}//end if - setDepartmentAndVerifyLab
	}//end selectDeptAndLab method
	
	public void setInitialSAConfig(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("New Search Authorizations will be tested");
		logFileWriter.newLine();
		//get to the correct page
		UIController.switchToStartPage(logFileWriter);
		HandSOnTestSuite.startPageUI.openNewSearchAuthorization(logFileWriter);
		boolean gotNewSA = false;
		while (! gotNewSA){
			By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SearchAuthID", logFileWriter);
			if (UIController.isElementPresent(by)){
				String text = UIController.driver.findElement(by).getText();
				if ((text != null) && (! text.isEmpty())){
					logFileWriter.write("Got a Search Authorization ID field - value is "+text);
					logFileWriter.newLine();
					UIController.switchWindowByURL(UIController.newSearchAuthorizationURL, logFileWriter);
				}//end if - it's a valid entry
				else gotNewSA = true;
			}//end if
			else{
				logFileWriter.write("Search Authorization ID field is empty - we have a new SA");
				logFileWriter.newLine();
				gotNewSA = true;
			}//end else
		}//end while
			

		UIController.switchWindowByURL(UIController.newSearchAuthorizationURL, logFileWriter);
		
		if (! verifyInitialSearchAuthConfig(logFileWriter)){
			logFileWriter.write("Initial Search Authorization Configuration is not correct.");
			logFileWriter.newLine();
		}//end if
	}//end setInitialSAConfig method
	
	public void selectProbYearAndValues(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("**** Now testing Probability Year and Department Selection ****");
		logFileWriter.newLine();
		logFileWriter.newLine();

		
		String[] probValues = {"30", "20", "20", "20", "10"};
		if (! canAdjustProbValues(logFileWriter, probValues)){
			logFileWriter.write("Projected Probability values can't adjust correctly.");
			logFileWriter.newLine();
		}//end if - canAdjustProbValues
	}//end selectProbYearAndDept method
	
	public void setStartDateAndInitialFY (String startDate, boolean testMult, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("**** Now testing Start Date Selection and initial FY ****");
		logFileWriter.newLine();
		logFileWriter.newLine();

		if (testMult){//we're going to test multiple start dates
			String[] startDates = {"08/31/2019", "09/01/2019", "09/03/2019", "01/01/2020"};
			for (int i=0; i<startDates.length; i++){
				if (! testPlannedBaseBudgetYear(logFileWriter, startDates[i])){
					logFileWriter.write("Planned Base Budget Year doesn't adjust correctly.");
					logFileWriter.newLine();
				}//end testPlannedBaseBudgetYear method
			}//end for
		}//end if - we're going to test multiple start dates
		
		//now, set the start date that will be used for this SA
		testPlannedBaseBudgetYear(logFileWriter, startDate);
	}//end setStartDateAndInitialFY method
	
	public void setRankSelection (String finalRank, boolean testMult, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("**** Now testing initial Rank Selection and Faculty Level ****");
		logFileWriter.newLine();
		logFileWriter.newLine();
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("RankSelect", logFileWriter);
		if (testMult){//we're testing multiple ranks
			String[] rank = UIController.getAllOptions(by, logFileWriter);
			for (int i=0; i<rank.length; i++){
				if (! verifyRankLevelFromSelection(rank[i], logFileWriter)){
					logFileWriter.write("Initial Rank Selection is not correct.");
					logFileWriter.newLine();
				}//end if
			}//end for loop		
			Thread.sleep(2000);
		}//end if - testing multiple ranks
		logFileWriter.write("Setting Rank Selection to be "+finalRank);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("FacultyLevelSelect", logFileWriter);
		UIController.waitAndSelectElementInMenu(by, "Select");
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("RankSelect", logFileWriter);
		UIController.waitAndSelectElementInMenu(by, finalRank);
	}//end setRankSelection method
	
	
	public boolean enterFTE(String fte, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("**** Now entering FTE information ****");
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("FTETextArea", logFileWriter);
		UIController.driver.findElement(by).sendKeys(fte);
		Thread.sleep(2000);
		
		String actualFTE = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
		return compareStrings("FTE value", fte, actualFTE, logFileWriter);
	}
	
	public String saveWithoutError(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("**** Now Attempting to save without an Error ****");
		logFileWriter.newLine();
		logFileWriter.newLine();
		
		String expectedAlertText = "Record saved Successfully";
		String actualAlertText = clickSave(logFileWriter);
		
		String newSearchAuthNumber = new String();
		if (compareStrings("Alert text", expectedAlertText, actualAlertText, logFileWriter)){
			By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SearchAuthID", logFileWriter);
			newSearchAuthNumber = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by);
		}

		return newSearchAuthNumber;

	}//end saveWithoutError method

	
	public boolean saveWithError(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("**** Now Attempting to save to get an Error ****");
		logFileWriter.newLine();
		logFileWriter.newLine();

		
		String expectedAlertText = "Fail to save record, please check errors";
		String actualAlertText = clickSave(logFileWriter);
		boolean warningDialogCorrect = compareStrings("Alert text", expectedAlertText, actualAlertText, logFileWriter);

		String expectedErrMsg = "Error: Appointment -1: FTE is required and should be a number";
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("ErrMsgText", logFileWriter);
		String actualErrMsg = new String();
		try{
			actualErrMsg = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by);
		}//end try clause
		catch(Exception e){
			logFileWriter.write("Could not find the expected error message" + expectedErrMsg);
			logFileWriter.write(", because of Exception "+e.getMessage());
			logFileWriter.newLine();
		}//end catch clause
		boolean errMsgCorrect = compareStrings("Error Message", expectedErrMsg, actualErrMsg, logFileWriter);
		
		return errMsgCorrect && warningDialogCorrect;
	}//end saveWithError method
	
	public String clickSave(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("Now attempting to click on the Save button");
		logFileWriter.newLine();
		Thread.sleep(2000);
		UIController.driver.findElement(By.id("buttonSubmit")).click();
		Thread.sleep(4000);
		logFileWriter.write("Now getting the Alert text - ");
		Alert alert = null;
		int iterations = 0;
		boolean alertAcquired = false;
		while ((! alertAcquired) && (iterations<10)) {
			iterations++;
			try{
				alert = UIController.driver.switchTo().alert();
				alertAcquired = true;
			}//end try
			catch (Exception e) {
				logFileWriter.write("Could not acquire the alert because of "+e.getMessage()+"; Re-trying - iteration #"+iterations);
				logFileWriter.newLine();
			}//end catch
			Thread.sleep(4000);
		}//end while
		String actualAlertText = alert.getText();
		logFileWriter.write("Alert text is "+ actualAlertText);
		logFileWriter.newLine();
		Thread.sleep(4000);
		alert.dismiss();
		Thread.sleep(4000);
		boolean alertIsPresent = true;
		while(alertIsPresent){
			try{
				alert = UIController.driver.switchTo().alert();
				actualAlertText = alert.getText();
				logFileWriter.write("Unexpectedly, there is another alert - Alert text is "+ actualAlertText);
				logFileWriter.newLine();
				alert.dismiss();	
				Thread.sleep(4000);
			}
			catch(Exception e){
				logFileWriter.write("Could not dismiss another alert, as expected");
				logFileWriter.newLine();
				alertIsPresent = false;
			}
		}//end while
		return actualAlertText;
	}//end clickSave method

	public boolean clickClose(int i, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("Now attempting to close the window associated with new SA with ID "+HandSOnTestSuite.searchAuthorizationIDs[i]);
		logFileWriter.newLine();
		UIController.driver.findElement(By.id("buttonClose")).click();
		Thread.sleep(3000);
		boolean isClosed = true;
		Set<String> windowHandles = UIController.driver.getWindowHandles();
	    Iterator<String> it = windowHandles.iterator();
	    while(it.hasNext()){
	    	 String nextHandle = it.next();
	    	 if(nextHandle.contains(UIController.newSearchAuthorizationURL)){
	        	UIController.driver.switchTo().window(nextHandle);
	        	Thread.sleep(3000);
	        	UIController.waitAndClick(By.id("buttonClose"));
	    	 }//end if
	    }//end while
	    windowHandles = null;
	    System.gc();
		Set<String> windowHandles2 = UIController.driver.getWindowHandles();
	    Iterator<String> it2 = windowHandles2.iterator();
	    while(it.hasNext()){
	    	 String nextHandle = it.next();
	    	 if(nextHandle.contains(UIController.newSearchAuthorizationURL)){
	    		 isClosed = false;
	    	 }//end if
	    }//end while    
	     if (isClosed){
	    	 logFileWriter.write("New Search Authorization has been successfully closed");
	    	 logFileWriter.newLine();
	     }//end if - it's closed
	     else{
	    	 String message = "WARNING - Could not close the new Search Authorization window";
	    	 verificationErrors.append(message);
	    	 logFileWriter.write(message);
	    	 logFileWriter.newLine();
	     }//end else
	     return isClosed;

	}//end clickClose method

	public boolean compareStrings(String descriptor, String expected, String actual, BufferedWriter logFileWriter) throws Exception{
		boolean errMsgCorrect = expected.replaceAll("\\s+","").equalsIgnoreCase(actual.replaceAll("\\s+",""));
		String message = new String();
		if (! errMsgCorrect)
			message ="ERROR: ";
		message = message.concat(descriptor + " is expected to be '"+expected);
		message = message.concat("', "+ descriptor + " is actually '"+actual+"'");
		if (errMsgCorrect)
			message = message.concat(", as expected");
		else
			verificationErrors.append(message);
		logFileWriter.write(message);
		logFileWriter.newLine();
		
		return errMsgCorrect;
	}//end compareStrings method
	
	public void enterBilletInfo(String billetText, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("**** Now Entering Billet Info ****");
		logFileWriter.newLine();
		logFileWriter.newLine();
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("BilletNumberTextfield", logFileWriter);
		UIController.driver.findElement(by).sendKeys(billetText);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("BilletNotesTextArea", logFileWriter);
		UIController.driver.findElement(by).sendKeys(billetText+" Test Notes");		
		
	}//end enterBilletInfo method
	
	public boolean setDepartmentAndVerifyLab (String dept, boolean testMult, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method setDepartmentAndVerifyLab called with department "+dept+" ***");
		logFileWriter.newLine();
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("DeptSelect", logFileWriter);
		String[] depts = UIController.getAllOptions(by, logFileWriter);

		boolean labIsCorrect = true;
		if (testMult){//test multiple departments
			for (int i=0; i<depts.length; i++){
				Thread.sleep(2000);
				UIController.selectElementInMenu(by, depts[i]);
				logFileWriter.write("Department '"+depts[i]+"' is selected");
				logFileWriter.newLine();
				if (! verifyLabAutoSelection(depts[i], logFileWriter)){
					logFileWriter.write("Lab Auto-Selection is not correct.");
					logFileWriter.newLine();
					labIsCorrect = false;
				}//end if
			}//end for loop		
		}//end if - test multiple departments
		UIController.selectElementInMenu(by, dept);
		
		return labIsCorrect;
	}//end setDepartmentAndVerifyLab method
	
	public boolean verifyLabAutoSelection(String dept, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method setDepartmentAndVerifyLab called with department "+dept+" ***");
		logFileWriter.newLine();
		
		String expectedLabValue = new String();
		if (dept.equalsIgnoreCase("Art & Art History") || dept.equalsIgnoreCase("Classics") 
				|| dept.equalsIgnoreCase("Comparative Literature") || dept.equalsIgnoreCase("Cont Studies & Summer Session") 
				|| dept.equalsIgnoreCase("Div Lit Cultures and Languages") || dept.equalsIgnoreCase("E. Asian Languages & Cultures") 
				|| dept.equalsIgnoreCase("English") || dept.equalsIgnoreCase("French and Italian") 
				|| dept.equalsIgnoreCase("German Studies") || dept.equalsIgnoreCase("H&S Dean's Office") 
				|| dept.equalsIgnoreCase("H&S Public Relations") || dept.equalsIgnoreCase("History") 
				|| dept.equalsIgnoreCase("Humanities and Global Group") || dept.equalsIgnoreCase("Iberian & Latin Amer Cultures")
				|| dept.equalsIgnoreCase("Linguistics") || dept.equalsIgnoreCase("Music") || dept.equalsIgnoreCase("Philosophy") 
				|| dept.equalsIgnoreCase("Religious Studies") || dept.equalsIgnoreCase("Slavic Languages & Literatures") 
				|| dept.equalsIgnoreCase("TBD - Humanities and Arts") 
				|| dept.equalsIgnoreCase("Theater & Performance Studies")){
			expectedLabValue = "No";
		}//end if
		else if (dept.equalsIgnoreCase("Select"))
			expectedLabValue = "Select";
		else expectedLabValue = "Yes";
		
		String actualLabValue = HandSOnTestSuite.searchAuthorizationUI.getSelectedText("LabSelect", logFileWriter);
		boolean labIsCorrect = actualLabValue.equalsIgnoreCase(expectedLabValue);
		if (labIsCorrect){
			logFileWriter.write("Expected Lab Value for department '"+dept+"' is "+expectedLabValue+", as expected");
			logFileWriter.newLine();
		}//end if
		else {
			String message = "ERROR: Expected Lab Value for department "+dept
					+" is "+expectedLabValue+", but actual value is "+actualLabValue+"; ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
		}//end else
		return labIsCorrect;
	}//end verifyLabAutoSelection method
	
	public void setDescription(BufferedWriter logFileWriter, String description) throws Exception{
		logFileWriter.write("*** method setDescription called with description "+description+" ***");
		logFileWriter.newLine();
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SearchAuthDescriptionTextfield", logFileWriter);
		UIController.driver.findElement(by).sendKeys(description);
	}//end setDescription method

	public boolean canAdjustProbValues(BufferedWriter logFileWriter, String[] probValues) throws Exception{
		logFileWriter.write("*** method canAdjustProbValues called with probability values ");
		for(int i=0; i<probValues.length; i++){
			logFileWriter.write(probValues[i]+", ");
		}
		logFileWriter.write(" ***");
		logFileWriter.newLine();
		
		for (int i=0; i<probValues.length; i++){
			int index=i+1;
			By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("ProjectedProbValueYear"+index+"Textfield", logFileWriter);
			UIController.driver.findElement(by).sendKeys(probValues[i]);
		}//end for loop
		
		boolean canAdjustProbValues = true;
		for (int i=0; i<probValues.length; i++){
			int index=i+1;
			By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("ProjectedProbValueYear"+index+"Textfield", logFileWriter);
			String actualProbValue = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
			if (actualProbValue.equalsIgnoreCase(probValues[i])){
				logFileWriter.write("Year "+index+" Probability Year value is "+probValues[i]+", as expected");
				logFileWriter.newLine();
			}//end if - all is well
			else{
				logFileWriter.write("ERROR: Year "+index+" Probability Year value should have been "+probValues[i]+", but is actually "+actualProbValue);
				logFileWriter.newLine();
				canAdjustProbValues = false;
			}//end else - error
		}//end for loop
		return canAdjustProbValues;
		
	}//end canAdjustProbValues method
	
	
	public boolean testPlannedBaseBudgetYear(BufferedWriter logFileWriter, String startDate) throws Exception{
		logFileWriter.write("*** method testPlannedBaseBudgetYear called with start date "+startDate+" ***");
		logFileWriter.newLine();
		boolean yearAdjustsCorrectly = true;
		
		Thread.sleep(3000);		
		int month = Integer.parseInt(startDate.split("/")[0]);
		int expectedYear = Integer.parseInt(startDate.split("/")[2]);
		
		if (month > 8){
			logFileWriter.write("Month is September or later - expected year is " + ++expectedYear);
			logFileWriter.newLine();
		}//end if
		else{
			logFileWriter.write("Month is August or earlier - expected year is "+ expectedYear);
			logFileWriter.newLine();
		}//end else
		
		By yearTextField = HandSOnTestSuite.searchAuthorizationUI.getByForName("PlannedBaseBudgetYearTextfield", logFileWriter);
		UIController.driver.findElement(yearTextField).clear();		
		Thread.sleep(2000);


		By startDateField = HandSOnTestSuite.searchAuthorizationUI.getByForName("ProjStartDateTextfield", logFileWriter);
		UIController.driver.findElement(startDateField).clear();
		Thread.sleep(5000);
		startDateField = HandSOnTestSuite.searchAuthorizationUI.getByForName("ProjStartDateTextfield", logFileWriter);
		UIController.driver.findElement(startDateField).sendKeys(startDate);
		Thread.sleep(2000);
		
		//we do this just to get the focus off of the start date field to trigger the update to the Planned Base Budget year
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SearchTypeSelect", logFileWriter);
		UIController.waitAndSelectElementInMenu(by, "Select");
		Thread.sleep(2000);
		UIController.waitAndSelectElementInMenu(by, "Standard");

		//wait a few seconds for the screen to refresh
		Thread.sleep(3000);
		UIController.waitForElementPresent(yearTextField);//just in case it's not yet present
		
		String actualYearText = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(yearTextField, logFileWriter);
		String expectedYearText = new Integer(expectedYear).toString();
		if (! actualYearText.equalsIgnoreCase(expectedYearText)){
			yearAdjustsCorrectly = false;
			String message = "ERROR: Expected Planned Base Budget Year is "+expectedYearText+", but actual is "+actualYearText+"; ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
		}//end if
		else{
			logFileWriter.write("Actual Planned Base Budget Year is " +actualYearText+", as expected");
			logFileWriter.newLine();
		}//end else - it is as expected
		
		for (int i=1; i<=5; i++){
			by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Year"+i+"FYLabel", logFileWriter);
			String actualFYtext = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by);
			String expectedFYtext = "FY" + (expectedYear - 2001 + i);
			if (! expectedFYtext.equalsIgnoreCase(actualFYtext)){
				yearAdjustsCorrectly = false;
				String message = "ERROR: Expected FY Year "+i+" is "+expectedFYtext+", but actual is "+actualFYtext+"; ";
				logFileWriter.write(message);
				logFileWriter.newLine();
				verificationErrors.append(message);				
			}//end if - error condition
			else {
				logFileWriter.write("Expected FY label for Year " + i +" is "+expectedFYtext+", as expected");
				logFileWriter.newLine();
			}//end else - it is as expected
		}//end for loop
		return yearAdjustsCorrectly;
	}//end testPlannedBaseBudgetYear method

	public boolean verifyRankLevelFromSelection(String rank, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method verifyRankLevelFromSelection called with rank "+rank+" ***");
		logFileWriter.newLine();
		boolean rankLevelCorrect = true;
		
		Thread.sleep(3000);		
		String name = "FacultyLevelSelect";
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(name, logFileWriter);
		if (! UIController.waitAndSelectElementInMenu(by, "Select")){
			rankLevelCorrect = false;
			String message = "Could not select rank level in rank level selection widget, as expected";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
		}//end if 
		Thread.sleep(3000);
		name = "RankSelect";
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(name, logFileWriter);
		if (UIController.waitAndSelectElementInMenu(by, rank)){
			String level = new String();
			level = HandSOnTestSuite.searchAuthorizationUI.getSelectedText("FacultyLevelSelect", logFileWriter);
			if (rank.equalsIgnoreCase("Full Professor")|| (rank.equalsIgnoreCase("Associate Professor"))){
				if (! level.equalsIgnoreCase("Senior")){
					rankLevelCorrect = false;
				}//end if - error condition
			}//end inner if - rank level should be Senior
			else if (rank.equalsIgnoreCase("TBD") || rank.equalsIgnoreCase("Open")){
				if (! level.equalsIgnoreCase("Open")){
					rankLevelCorrect = false;
				}//end if - error condition
			}//end inner else if - rank should be Open
			else if (rank.equalsIgnoreCase("Select")){
				if (! level.equalsIgnoreCase("Select")){
					rankLevelCorrect = false;
				}//end if - error condition
			}//end inner else if - rank should be Open
			else{
				if (! level.equalsIgnoreCase("Junior")){
					rankLevelCorrect = false;
				}//end if - error condition
			}//end else - rank level should be Junior
			if (rankLevelCorrect){
				logFileWriter.write("Rank Level for Rank "+rank+" is "+level+", as expected");
				logFileWriter.newLine();
			}//end if
			else{
				String message = "ERROR - Rank Level for Rank "+rank+" is "+level+", which is NOT expected; ";
				logFileWriter.write(message);
				logFileWriter.newLine();
				verificationErrors.append(message);
			}//end else
		}//end if
		else{
			rankLevelCorrect = false;
			String message = "Could not select rank in rank selection widget, as expected";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
		}//end else
		return rankLevelCorrect;
	}//end verifyRankLevelFromSelection method
	
	public boolean verifyInitialSearchAuthConfig(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("**** Now testing initial Search Authorization configuration ****");
		logFileWriter.newLine();
		logFileWriter.newLine();
		
		boolean initialConfigCorrect = true;
		
		String[] names = {"RankSelect", "FacultyLevelSelect", "SearchStatusSelect", "SearchTypeSelect", "DeptSelect", 
				"LabSelect", "SchoolSelect", "RenewalSelect", "ApptTypeSelect"};
		String[] expectedValues = {"Select", "Select", "Search", "Standard", "Select", "Select", 
				"Humanities and Sciences", "Yes", "Primary"};
		for (int i=0; i<names.length; i++){
			if (! verifySelectedElement(names[i], expectedValues[i], logFileWriter)){
				initialConfigCorrect = false;
				String message = "MISMATCH: Expected value "+expectedValues[i]+" is not present; ";
				verificationErrors.append(message);
				logFileWriter.write(message);
				logFileWriter.newLine();
			}//end if - the expected value is there
			else{
				logFileWriter.write("Found "+expectedValues[i]+" at locator name "+names[i]+", as expected; ");
				logFileWriter.newLine();
			}//end else
		}//end for loop
		String name = "SearchAuthID";
		String actualValue 
			= HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(HandSOnTestSuite.searchAuthorizationUI.getByForName(name, logFileWriter), 
					logFileWriter);
		
		logFileWriter.newLine();
		logFileWriter.newLine();
		if (! actualValue.isEmpty()){
			initialConfigCorrect = false;
			String message = "ERROR - locator "+name+" is not blank, as expected; ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
		}//end if
		else{
			logFileWriter.write("Locator " + name +" is blank, as expected");
			logFileWriter.newLine();
		}//end else
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("OpenCompOfferButton", logFileWriter);
		UIController.waitAndClick(by, 2);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("AddCompOfferButton", logFileWriter);
		UIController.waitAndClick(by, 5);
		Thread.sleep(5000);
		

		String[] blankLocatorNames = HandSOnTestSuite.searchAuthorizationUI.getLocatorNamesBelowLocator(SearchAuthorizationUIController.GeneralWorksheetName,
				"AddCompOfferButton", logFileWriter);
		for(int i=0; i<blankLocatorNames.length; i++){
			by = HandSOnTestSuite.searchAuthorizationUI.getByForName(blankLocatorNames[i], logFileWriter);
			String actualText = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
			if (actualText.isEmpty()){
				logFileWriter.write("Locator for "+blankLocatorNames[i]+" is blank, as expected");
				logFileWriter.newLine();
			}//end if - it is blank, as expected
			else{
				initialConfigCorrect = false;
				String message = "ERROR: Locator for "+blankLocatorNames[i]+" is NOT blank, as expected, but "+actualText+";";
				logFileWriter.write(message);
				logFileWriter.newLine();
				verificationErrors.append(message);
			}//end else - it is not blank
		}//end for loop
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("DeleteCompOfferButton", logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(2000);
		return initialConfigCorrect;
	}//end verifyInitialSearchAuthConfig method
	
	public boolean verifySelectedElement(String locatorName, String expectedValue, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("* Attempting to get the selected text "+ expectedValue +" for "+locatorName +" *");
		logFileWriter.newLine();
		String actualValue = HandSOnTestSuite.searchAuthorizationUI.getSelectedText(locatorName, logFileWriter);
		logFileWriter.write("Actual value found : " +actualValue);
		logFileWriter.newLine();
		if ((actualValue == null) || actualValue.isEmpty())
			return false;
		else return expectedValue.equalsIgnoreCase(actualValue);
	}

}//end Class
