package Maven.SeleniumWebdriver;

import java.util.ArrayList;
import java.util.HashMap;

public class ExpectedASCData {
	//these are the minimum required values to instantiate an ASC in the GUI
	public static final String[] StringAttributeKeys = 
		{"Name", "Department", "Stage", "Category", "Subcategory", 
		"Source Pool", "Description", "Notes"};
	private String[] StringAttributes = new String[StringAttributeKeys.length];
	public static final int FacultyIndex = 0;
	public static final int DepartmentIndex = 1;
	public static final int StageIndex = 2;
	public static final int CategoryIndex = 3;
	public static final int SubCategoryIndex = 4;
	public static final int PoolIndex = 5;
	public static final int lastRequiredAttributeIndex = 5;
	//these can be set later, or not set at all.
	public static final int DescriptionIndex = 6;
	public static final int NotesIndex = 7;
	
	public static final String isDean = "isDean";
	public static final String isDeptChair = "isDeptChair";

	
	private ASCFacultyValues myFacultyValues;
	private boolean hasMultipleASCs = false;

	
	//these have to be set for the ASC to be meaningful
	public static final String[] amountAttributeKeys = 
		{"FTEPercent", "CurrentSalary", "OneHundredPercentDOShareAmount", 
		"OneHundredPercentDeptShareAmount", "TotalAmount", "FTEAdjustedSalary", 
		"FTEAdjustedDOShare", "FTEAdjustedDepartmentShare","FTEAdjustedTotalAmount"};
	private int[] amountAttributes = new int[amountAttributeKeys.length];
	public static final int FTEPercentIndex = 0;
	public static final int CurrentSalaryIndex = 1;
	public static final int OneHundredPercentDOShareAmountIndex = 2;
	public static final int OneHundredPercentDeptShareAmountIndex = 3;
	public static final int TotalAmountIndex = 4;
	public static final int FTEAdjustedSalaryIndex = 5;
	public static final int FTEAdjustedDOShareIndex = 6;
	public static final int FTEAdjustedDepartmentShareIndex = 7;
	public static final int FTEAdjustedTotalAmountIndex = 8;
	

	/*
	 * There are multiple constructors which take different arguments.
	 * Minimally, to create an ExpectedASCData Object, we must have 
	 * the following: Faculty, Department, Cluster, Category, SubCategory,
	 * and Stage.  In order to make this easier to work with, we simply take
	 * a HashMap that contains (at a minimum) these variables and their values
	 * Optionally, we can also take a HashMap of Integer Objects as well, 
	 * which we will convert to primitive int values.  
	 * If we don't get the HashMap of Integers, we'll set them to default
	 * values - 100% FTE, $0 everything else.
	 */
	public ExpectedASCData(HashMap<String, String> stringValues, HashMap<String, Integer> intValues) throws Exception {
		String message = new String();
		for(int i=0;i<StringAttributes.length; i++){
			if (stringValues.containsKey(StringAttributeKeys[i])){
				this.StringAttributes[i] = stringValues.get(StringAttributeKeys[i]);
			}//end if
			else {
				if(i<=lastRequiredAttributeIndex)
					message = message.concat("missing attribute " + StringAttributeKeys[i] + "; ");
			}//end else
		}//end for loop
		if (! message.isEmpty()) throw new Exception(message);
		

		//Now, we work with amountValues:
		for(int i=0;i<amountAttributeKeys.length;i++){
			if (intValues.containsKey(amountAttributeKeys[i])){
				int amount = intValues.get(amountAttributeKeys[i]).intValue();
				if (amount != -1)
					this.amountAttributes[i] = amount;
				else if (i==FTEPercentIndex) amountAttributes[i] = 100;
				else amountAttributes[i] = 0;
			}//end if
		}//end for
	}//end constructor
	
	public ExpectedASCData(HashMap<String, String> stringValues) throws Exception{
		this(stringValues, new HashMap<String, Integer>());
	}//end constructor
	
	public boolean hasMultipleASCs(){
		return this.hasMultipleASCs;
	}
	
	public void setHasMultipleASCs(boolean hasMultipleASCs){
		this.hasMultipleASCs = hasMultipleASCs;
	}
	
	public boolean hasOutsideAppt() throws Exception {
		return myFacultyValues.hasOutsideAppt();
	}
	public String getStringAttribute(int keyIndex){
		if (keyIndex>=0 && keyIndex<StringAttributeKeys.length)
			return StringAttributes[keyIndex];
		else return new String();
	}// end getStringAttribute method
	
	public String getStringAttribute(String attributeName){
		int index=-1;
		boolean match = false;
		for(int i=0;i<StringAttributeKeys.length && (! match); i++){
			if(StringAttributeKeys[i].equalsIgnoreCase(attributeName)){
				match = true;
				index=i;
			}//end if - match
		}//end for loop
		for(int i=0;i<ASCFacultyValues.StringAttributeKeys.length && (! match); i++){
			String key = ASCFacultyValues.StringAttributeKeys[i];
			if (key.equalsIgnoreCase(attributeName)){
				match = true;
				return myFacultyValues.getStringAttribute(key);
			}
		}
		if (index == -1) return new String("attribute name "+ attributeName +" not found");
		else return getStringAttribute(index);
	}//end getStringAttribute method
	
	public int getAmountAttribute(int keyIndex){
		if (keyIndex>=0 && keyIndex<amountAttributeKeys.length)
			return amountAttributes[keyIndex];
		else return -1;
	}// end getStringAttribute method
	
	public int getAmountAttribute(String attributeName){
		int index=-1;
		boolean match = false;
		for(int i=0;i<amountAttributeKeys.length && (! match); i++){
			if(amountAttributeKeys[i].equalsIgnoreCase(attributeName)) index=i;
		}//end for loop
		if (index == -1) return index;
		else return getAmountAttribute(index);
	}//end getStringAttribute method
	
	public String toString(){
		String message = new String("ExpectedASCData is as follows:\n");
		for(int i=0; i<StringAttributes.length; i++){
			message = message.concat(StringAttributeKeys[i] + " = " + StringAttributes[i] + ";\n");
		}//end for - printing out the String attributes
		message = message.concat("\n");
		for(int i=0; i<amountAttributes.length; i++){
			message = message.concat(amountAttributeKeys[i] + " = " +amountAttributes[i] +";\n");
		}//end for - printing out the amount attributes
		message = message.concat("\n");
		try{
			message = message.concat("ASCFacultyValues data:: " + myFacultyValues.toString());
		}
		catch(NullPointerException e){;}
		return message;
	}
	
	public void assignASCFacultyValues(ASCFacultyValues myFacultyValues) throws Exception{
		this.myFacultyValues = myFacultyValues;
	}//end method assignASCFacultyValues
	
	public String getASCFacultyValue(String attributeName) throws Exception{
		return myFacultyValues.getStringAttribute(attributeName);
	}
	
	public int getAllKnownSharesForPool(int sourcePoolIndex){
		return myFacultyValues.getAllSharesForStageAndPool(ASCTest.KnownStageIndex, sourcePoolIndex);
	}
	
	public int getAllKnownSharesForPool(String sourcePool){
		return myFacultyValues.getAllSharesForStageAndPool(ASCTest.stages[ASCTest.KnownStageIndex], sourcePool);
	}

	public int getAllSharesForStageAndPool(int stageIndex, int sourcePoolIndex){
		return myFacultyValues.getAllSharesForStageAndPool(stageIndex, sourcePoolIndex);
	}
	
	public int getAllSharesForStageAndPool(String stage, String sourcePool){
		return myFacultyValues.getAllSharesForStageAndPool(stage, sourcePool);
	}
	
	public int getDOShare(int stageIndex, int sourcePoolIndex){
		return myFacultyValues.getDOShare(stageIndex, sourcePoolIndex);
	}
	
	public int getDOShare(String stage, String sourcePool){
		return myFacultyValues.getDOShare(stage, sourcePool);
	}
	
	public boolean hasNonZeroDOShare(){
		return myFacultyValues.getDOShare(this.getStringAttribute(ExpectedASCData.StageIndex), 
				this.getStringAttribute(ExpectedASCData.PoolIndex)) != 0;
	}
	
	public int getDeptShare(int stageIndex, int sourcePoolIndex){
		return myFacultyValues.getDeptShare(stageIndex, sourcePoolIndex);
	}
	
	public int getDeptShare(String stage, String sourcePool){
		return myFacultyValues.getDeptShare(stage, sourcePool);
	}

	public int getAllDOSharesForStage(int stageIndex){
		return myFacultyValues.getAllDOSharesForStage(stageIndex);
	}
	
	public int getAllDOSharesForStage(String stage){
		return myFacultyValues.getAllDOSharesForStage(stage);
	}

	public int getAllDeptSharesForStage(int stageIndex){
		return getAllDeptSharesForStage(stageIndex, false);
	}
	
	public int getAllDeptSharesForStage(int stageIndex, boolean excludeMarketShare){
		int sum = 0;
		if (excludeMarketShare){
			sum = myFacultyValues.getAllDeptSharesForStage(stageIndex) 
					- myFacultyValues.getDeptShare(stageIndex, ASCTest.MarketPoolIndex);
		}
		else sum = myFacultyValues.getAllDeptSharesForStage(stageIndex);
		return sum;
	}
	
	public int getAllSharesForStage(int stageIndex){
		return getAllSharesForStage(stageIndex, false);
	}

	public int getAllDeptSharesForStage(String stage, boolean excludeMarketShare){
		int sum = myFacultyValues.getAllDeptSharesForStage(stage);
		if (excludeMarketShare)
			sum -= getDeptShare(stage, ASCTest.sourcePools[ASCTest.MarketPoolIndex]);
		return sum;
	}
	
	public int getAllSharesForStage(int stageIndex, boolean excludeMarketShare){
		int sum = 0;
		if (excludeMarketShare){
			sum = getAllDeptSharesForStage(stageIndex) + getAllDOSharesForStage(stageIndex)
					- getDeptShare(stageIndex, ASCTest.MarketPoolIndex);
		}
		else sum = getAllDeptSharesForStage(stageIndex) + getAllDOSharesForStage(stageIndex);
		return sum;
	}

	public int getAllSharesForStage(String stage){
		return getAllSharesForStage(stage, false);
	}
	public int getAllSharesForStage(String stage, boolean excludeMarketShare){
		int sum = 0;
		if (excludeMarketShare){
			sum = getAllDeptSharesForStage(stage, excludeMarketShare) + getAllDOSharesForStage(stage)
					- getDeptShare(stage, ASCTest.sourcePools[ASCTest.MarketPoolIndex]);
		}
		else sum = getAllDeptSharesForStage(stage, excludeMarketShare) + getAllDOSharesForStage(stage);
		return sum;
	}
	
	public boolean isDean(){
		if (this.myFacultyValues == null) return false;
		else return this.myFacultyValues.isDean();
	}
	
	public boolean isDeptChair(){
		if (this.myFacultyValues == null) return false;
		else return this.myFacultyValues.isDeptChair();
	}

}
