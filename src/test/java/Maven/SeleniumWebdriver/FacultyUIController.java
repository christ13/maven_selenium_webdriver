package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;

import org.openqa.selenium.By;

public class FacultyUIController extends UIController {
	
	public static final String selectedIndicatorText = "Faculty is Selected for Retention Test";
	public static final String facultyURL = "processManageFaculty.do?ACTION=load&facultyDTO.unvlId";


	public static String getFacultyName(BufferedWriter logFileWriter) throws Exception{
		String facultyName = new String();
		
		return facultyName;
	}//end getFacultyName method
	
	public static void navigateToFacultyRecord (BufferedWriter logFileWriter, String handSOnID) throws Exception {
		if (switchWindowByURL(facultyURL+"="+handSOnID, logFileWriter)) {//page is already open
			logFileWriter.newLine();
			logFileWriter.write("Faculty Detail Record is already open - switch is successful");
			return;
		}//end if - no need for further action here
		else {//we have to open the page
			logFileWriter.write("Faculty Detail Record needs to be opened");
			logFileWriter.newLine();
			FacultySearchPageUIController.navigateToSearchPage(logFileWriter);
			By by = By.linkText(handSOnID);
			FacultySearchPageUIController.openFacultyPage(logFileWriter, by);
		}//end else
	}//end navigateToFacultyRecord method
	
	public static void openRetentionTab (BufferedWriter logFileWriter, String facultyName) throws Exception{
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Current Appt Tab", "RetentionsTabLink", logFileWriter);
		if (waitAndClick(by)) {
			logFileWriter.write("Got to the Retentions tab for the Faculty '"+facultyName+"'");
			logFileWriter.newLine();
			Thread.sleep(5000);
		}//end if - switching to the Retention tab
		else {
			logFileWriter.write("Could not get to the Retentions tab for the Faculty '"+facultyName+"'");
			logFileWriter.newLine();
		}//end else - could not switch to the Retention tab
	}//end openRetentionTab method
	
	public static void openSalaryTab (BufferedWriter logFileWriter, String facultyName) throws Exception{
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Current Appt Tab", "SalaryPageLink", logFileWriter);
		if (waitAndClick(by)) {
			logFileWriter.write("Got to the Salary tab for the Faculty '"+facultyName+"'");
			logFileWriter.newLine();
			Thread.sleep(5000);
		}//end if - switching to the Retention tab
		else {
			logFileWriter.write("Could not get to the Salary tab for the Faculty '"+facultyName+"'");
			logFileWriter.newLine();
		}//end else - could not switch to the Retention tab
	}//end openSalaryTab method
	
	public static String getStringValueFromLabelForName (BufferedWriter logFileWriter, String worksheetName, String name) throws Exception{
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(worksheetName, name, logFileWriter);
		logFileWriter.write("Retrieving "+name+" from worksheet "+worksheetName);
		logFileWriter.newLine();
		String value = new String();
		try {
			value = UIController.driver.findElement(by).getText();
			logFileWriter.write("Successfully retrieved String value "+value+" from "+name);
			logFileWriter.newLine();
		}
		catch(Exception e) {
			logFileWriter.write("Could not locate the element - will return empty String");
			logFileWriter.newLine();
		}
		return value;
	}//end getStringValueFromLabelForName method
	
	public static boolean isValidFacultyData(String adminAppt, BufferedWriter logFileWriter) throws Exception{
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Current Appt Tab", "NotesTextField", logFileWriter);
		String indicatorText = UIController.driver.findElement(by).getText().trim();
		logFileWriter.write("Text found in Notes field is "+indicatorText);
		logFileWriter.newLine();
		if (indicatorText.contains(selectedIndicatorText)) {
			logFileWriter.write("This Faculty member has already been selected - we will need to select another Faculty member");
			logFileWriter.newLine();
			closeCurrentTab(logFileWriter);
			return false;
		}//end if - this Faculty member is already selected
		else {
			logFileWriter.write("This Faculty member has not already been selected.  We will investigate further.");
			logFileWriter.newLine();
		}
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Current Appt Tab", "AdminApptRoleSelect", logFileWriter);
		boolean shouldFindAppt = true;
		if (adminAppt.equalsIgnoreCase("NONE")) {
			shouldFindAppt = false;
			logFileWriter.write("We need to find *NO* Administrative Appointment");
			logFileWriter.newLine();
		}//end if - no Administrative Appointment should be found
		else {
			logFileWriter.write("We need to find an Administrative Appointment of "+adminAppt);
			logFileWriter.newLine();
		}//end else - an Administrative Appointment should be found
		try {
			String actualAdminAppt = getSelectedTextForBy(by, "AdminApptRoleSelect", logFileWriter);
			if (actualAdminAppt.equalsIgnoreCase(adminAppt)) {
				logFileWriter.write("We found Administrative Appointment of "+actualAdminAppt+", as desired");
				logFileWriter.newLine();
				return true;
			}
			else {//the Administrative Appointment was found and it's not equal to the Administrative Appt expected
				if (shouldFindAppt) {
					logFileWriter.write("We found Administrative Appointment of "+actualAdminAppt+", which is not as desired");
					logFileWriter.newLine();
					return false;
				}
				else {
					logFileWriter.write("We found *NO* Administrative Appointment, as desired");
					logFileWriter.newLine();
					return true;
				}
			}//end else
		}//end try clause
		catch(Exception e) {//there was an Exception thrown when trying to access the dropdown - probably a NoSuchElementException
			if (shouldFindAppt) {//no Administrative Appointment was found, but we expect one should be found.... return false;
				logFileWriter.write("We found *NO* Administrative Appointment, which is not desired for valid data");
				logFileWriter.newLine();
				return false;
			}//end if
			else {//if no Administrative Appointment should be found and there was none found, then we're all good
				logFileWriter.write("We found *NO* Administrative Appointment, as desired");
				logFileWriter.newLine();
				return true;
			}//end else
		}//end catch clause
	}//end isValidFacultyData method
	
	public static String processFacultyAsValid (BufferedWriter logFileWriter) throws Exception{
		String validFacultyName = new String();
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Current Appt Tab", "NameLabel", logFileWriter);
		validFacultyName = UIController.driver.findElement(by).getText();
		logFileWriter.write("Faculty Name derived is "+validFacultyName);
		logFileWriter.newLine();
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Current Appt Tab",  "NotesTextField", logFileWriter);
		UIController.driver.findElement(by).sendKeys(selectedIndicatorText);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Current Appt Tab",  "SaveButton", logFileWriter);
		UIController.driver.findElement(by).click();
		Thread.sleep(3000);
		UIController.dismissAlert(logFileWriter);
		return validFacultyName;
	}//end processFacultyAsValid method
	
	public static void closeCurrentTab (BufferedWriter logFileWriter) throws Exception{
		if (switchWindowByURL(facultyURL, logFileWriter)) {
	    	System.out.println("Closing Window with URL "+driver.getCurrentUrl());
			logFileWriter.write("Closing Window with URL " + driver.getCurrentUrl());
	    	logFileWriter.newLine();
	    	driver.close();	
		}//end if
		else {
	    	System.out.println("Could not switch to Faculty URL - current URL is "+driver.getCurrentUrl());
			logFileWriter.write("Could not switch to Faculty URL - current URL is " + driver.getCurrentUrl());
	    	logFileWriter.newLine();
	    }//end else
	}//end closeCurrentTab method

}
