package Maven.SeleniumWebdriver;


import java.io.BufferedWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import oracle.jdbc.OracleConnection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ProvostBinderListDBVerificationTest {
	protected static SSExcelReader myExcelReader;

	protected static String testName;
	protected static String testCaseNumber;
	protected static String testDescription;
	protected static String testEnv;
	protected static StringBuffer verificationErrors;
	
	protected static Connection myConnection;
	protected static int firstDataRow;

	@Before
	public void setUp() throws Exception {
		verificationErrors = new StringBuffer();
	}

	@After
	public void tearDown() throws Exception {
		if (myConnection != null) myConnection.close();
		TestFileController.writeTestResults(testEnv, testCaseNumber, testName, testDescription, verificationErrors);
	}

	@Test
	public void test() throws Exception{

	}

	protected Connection getConnection(BufferedWriter logFileWriter) throws Exception{
		Connection conn = null;
		try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            String url = "jdbc:oracle:thin:@//handsondb-tst4.stanford.edu:1568/HON_TST";

            Properties props = new Properties();
			props.setProperty("user", "hsondev_test");
			props.setProperty("password", "skyline20160708");
			props.setProperty("url", url);
			props.setProperty(OracleConnection.CONNECTION_PROPERTY_THIN_NET_CONNECT_TIMEOUT, "20000");
            conn = DriverManager.getConnection(url, props);
		}//end try
	    catch (Exception e) {
	        StackTraceElement[] stackTrace = e.getStackTrace();
	        for (int i=0; i<stackTrace.length; i++){
	        	logFileWriter.write(stackTrace[i].toString());
	        	logFileWriter.newLine();
	        }//end for loop
	    }//end catch clause
		finally{
			if (conn == null)
				logFileWriter.write("method getConnection is returning null Connection");
			else logFileWriter.write("Connected to database");
			logFileWriter.newLine();
		}
		return conn;		
	}//end getConnection method
	
	protected void runStoredProcedure(Connection conn, BufferedWriter logFileWriter) throws Exception{
		CallableStatement storedProc = null;
		try{
	        storedProc = conn.prepareCall("{call TEST_SALSET_ELIG(?, ?, ?)}");
	        storedProc.setString(1, "2017");
	        storedProc.setString(2, "04/01/2017");
	        storedProc.setString(3, null);
	        System.out.println("Test " + testName + " running: executed running stored procedure at time " +System.currentTimeMillis());
	        storedProc.execute();
            System.out.println("Test " + testName + " running: finished running stored procedure at time " +System.currentTimeMillis());
	        logFileWriter.write("Stored procedure successfully executed");
	        logFileWriter.newLine();
		}//end try block
	    catch (SQLException e) {
	    	System.out.println("Test " + testName + " running: failed running stored procedure at time " +System.currentTimeMillis());
	        StackTraceElement[] stackTrace = e.getStackTrace();
	        for (int i=0; i<stackTrace.length; i++){
	        	logFileWriter.write(stackTrace[i].toString());
	        	logFileWriter.newLine();
	        }//end for loop
	        throw e;
	    }//end catch clause
		finally{
			if (storedProc != null) storedProc.close();
            logFileWriter.newLine();
		}//end finally clause
	}//end runStoredProcedure method 

	protected void downloadProvostBinderReports(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("Now attempting to download Provost Binder Reports:");
		logFileWriter.newLine();
		SalarySettingManager.switchWindowByURL(SalarySettingManager.salarySettingURL, logFileWriter);
		HandSOnTestSuite.salarySettingMgrTabUI.switchToReportTab();
		ReportRunner.downloadProvostBinderReports(ReportRunner.referenceWhichValues, logFileWriter);
	}//end downloadProvostBinderReports method

}
