package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.TreeMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ControlSheetPivotOneClusterReportASCTest extends
		ControlSheetPivotReportASCTest {
	
	protected String clusterName;

	@Before
	public void setUp() throws Exception {
		logFileWriter = TestFileController.getLogFileWriter(logDirectoryName  +"/" + logFileName);
		
		criteria = new TreeMap<String, String>();
		
		dataPresentCriterionName = ASCFacultyValues.StringAttributeKeys[ASCFacultyValues.ClusterIndex];
		dataPresentCriterionValue = clusterName;

		expectedStringAttributeNames = new String[5];
		actualStringAttributeNames = new String[5];
		expectedStringAttributeNames[0] = actualStringAttributeNames[0] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.CategoryIndex];
		expectedStringAttributeNames[1] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.SubCategoryIndex];
		actualStringAttributeNames[1] = "Sub-Category";
		expectedStringAttributeNames[2] = actualStringAttributeNames[2] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.DepartmentIndex];
		expectedStringAttributeNames[3] = actualStringAttributeNames[3] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.FacultyIndex];
		//Description attribute has a bug - HANDSON-3269 - associated with it.  It's not copied from the ASC to the Control Sheet Detail Report.
		expectedStringAttributeNames[4] = actualStringAttributeNames[4] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.DescriptionIndex];
		
		expectedAmountAttributeNames = new String[0];
		actualAmountAttributeNames = new String[0];

		//specify the criteria that will be used when determining the expected amounts for the FTE Adjusted DO Share Amount
		expectedSummaryCriteria = new TreeMap<String, ArrayList<String>>();
		ArrayList<String> list = new ArrayList<String>();
		list.add(ExpectedASCData.StringAttributeKeys[ExpectedASCData.FacultyIndex]);
		list.add(ExpectedASCData.StringAttributeKeys[ExpectedASCData.SubCategoryIndex]);
		list.add(ExpectedASCData.StringAttributeKeys[ExpectedASCData.StageIndex]);
		expectedSummaryCriteria.put("FTE Adjusted DO Share Amount", list);

		pivotCriteriaName = ExpectedASCData.StringAttributeKeys[ExpectedASCData.StageIndex];
		outputMessage("attribute name being used as pivot criteria is "+pivotCriteriaName + "\n", logFileWriter);
		pivotCriteriaValues = HandSOnTestSuite.expectedASCDataSet.getAllDiscreteStringAttributeValues(pivotCriteriaName, logFileWriter);

		searchDelimiters = new ArrayList<String[]>(pivotCriteriaValues.length);
		for (int i=0; i<pivotCriteriaValues.length; i++){
			String[] criterion = {pivotCriteriaName, pivotCriteriaValues[i]};
			searchDelimiters.add(criterion);
		}//end for loop

		outputMessage(pivotCriteriaName + " values being used as delimiters are as follows: ", logFileWriter);
		for (int i=0; i<pivotCriteriaValues.length; i++)
			outputMessage(pivotCriteriaValues[i] +", ", logFileWriter);
		outputMessage("\n", logFileWriter);

		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void test() throws Exception{
		super.test();
	}

}
