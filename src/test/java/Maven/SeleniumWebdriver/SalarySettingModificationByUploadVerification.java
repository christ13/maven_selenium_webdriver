package Maven.SeleniumWebdriver;


import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import oracle.jdbc.OracleConnection;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;

public class SalarySettingModificationByUploadVerification {
	
	private static final String fileDirectory = "C:\\Users\\christ13\\Documents\\HandSon\\";

	private static String testEnv;
	private static String testName;
	private static String testDescription;
	private static String testCaseNumber;
	private static BufferedWriter logFileWriter;
	private static StringBuffer verificationErrors;
	private static SSExcelReader myExcelReader;

	private static String myWorksheetName;
	private static final String deptWorksheetName = "Department Salary Setting";
	private static final String clusterWorksheetName = "Cluster Salary Setting";
	private static final String schoolWorksheetName = "School Salary Setting";
	private static final String colorIndicatingSSWAdjustment = "FFFFFFCC";
	private static final String facultyNameLocatorName = "FacultyNameLeftmostColumnTopRow";

	//these variables are related to the JDBC we use to get the data from the database
	protected static Connection myConnection;
	protected static int firstDataRow;

	@Before
	public void setUp() throws Exception {
		testEnv = "Test";
		testName = "Salary Setting Upload Test";
		testDescription = "Verify Salary Values Modification by Upload";
		testCaseNumber = "053";
		
		String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		logFileWriter = TestFileController.getLogFileWriter(logFileName);
		logFileWriter.write("Now executing "+ testName);
		logFileWriter.newLine();
		verificationErrors = new StringBuffer();
	}//end setUp() method

	@After
	public void tearDown() throws Exception {
		if (myConnection != null) myConnection.close();
		TestFileController.writeTestResults(testEnv, testCaseNumber, testName, testDescription, verificationErrors);
		logFileWriter.newLine();
		logFileWriter.close();
	}//end tearDown() method

	@Test
	public void test() throws Exception{
		myConnection = getConnection(logFileWriter);
		runStoredProcedure(myConnection, logFileWriter);
		String[] depts = this.getDepts(myConnection, logFileWriter);
		for (int i=0; i<depts.length; i++){
			logFileWriter.write("Now working with Department " + depts[i]);
			logFileWriter.newLine();
		}//end for loop
		/*
		 * For each Department, download the SSW, verify the zeroed-out fields,
		 * then modify the UI, putting in non-zero values, then download it again,
		 * and verify that the values are now non-zero.  
		 * Then, modify the values so that they are zero, then upload it again,
		 * and verify that the values in the UI are now zero.  
		 */
		
		//for (int i=0; i<depts.length; i++){
		for (int i=0; i<1; i++){
			/*
			String clusterName = "Natural Sciences Cluster";
			String deptName = "Mathematics";
			//un-comment this when we want to enter hard-coded values
			String facultyName = "Brendle, Simon";
			*/
			
			//un-comment these when we want values determined from the depts[] array
			String clusterName = depts[i].split(";")[0];
			String deptName = depts[i].split(";")[1];
			if (! UIController.switchWindowByURL(UIController.salarySettingURL, logFileWriter)){
				logFileWriter.write("Could not switch to Salary Setting URL");
				logFileWriter.newLine();
			}
			else {
				logFileWriter.write("Successfully switched to Salary Setting URL");
				logFileWriter.newLine();
			}
			HandSOnTestSuite.salarySettingTabUI.switchToSalarySettingTab();
			logFileWriter.write(HandSOnTestSuite.salarySettingTabUI.adjustSalarySettingConfiguration(clusterName, deptName, logFileWriter));
			logFileWriter.newLine();
			
			HandSOnTestSuite.salarySettingTabUI.acceptTwoAlerts(logFileWriter);
			
			String[] facultyAndRow = getFacultyForDept(clusterName, deptName).split(";");
			String facultyName = facultyAndRow[0];
			String facultyUIRow = facultyAndRow[1];
			myWorksheetName = deptWorksheetName;
			
			UIController.switchWindowByURL(UIController.loadSalarySettingWindowURL, logFileWriter);
			HandSOnTestSuite.salarySettingTabUI.updateDynamicIndices(logFileWriter);
			HandSOnTestSuite.salarySettingTabUI.updateUIValues("0", facultyUIRow, logFileWriter);
			
			testInitialDeptConfig(clusterName, deptName, facultyName, logFileWriter);
			testInitialClusterConfig (clusterName, deptName, facultyName, logFileWriter);
			testInitialSchoolConfig(clusterName, deptName, facultyName, logFileWriter);
	
			//update values to 1000 on the UI and verify that that's the value present
			UIController.switchWindowByURL(UIController.loadSalarySettingWindowURL, logFileWriter);
			HandSOnTestSuite.salarySettingTabUI.updateUIValues("1,000", facultyUIRow, logFileWriter);
			Thread.sleep(10000);
			HandSOnTestSuite.salarySettingTabUI.verifyUIValues("1,000", facultyUIRow, logFileWriter, true);
	
			downloadDeptSSW(clusterName, deptName, logFileWriter);
			UIController.switchWindowByURL(UIController.loadSalarySettingWindowURL, logFileWriter);
			downloadClusterSSW(clusterName, logFileWriter);
			UIController.switchWindowByURL(UIController.loadSalarySettingWindowURL, logFileWriter);
			downloadSchoolSSW(clusterName, logFileWriter);
			
			if (verifyModifiedDeptSSWFields(clusterName, deptName, facultyName, "1,000", logFileWriter)){
				logFileWriter.write("Dept modified fields are 1,000, as expected");
				logFileWriter.newLine();
			}
			else verificationErrors.append ("Dept Values other than 1,000 found where not expected");
			
			if (verifyModifiedClusterSSWFields(clusterName, deptName, facultyName, "1,000", logFileWriter)){
				logFileWriter.write("Cluster modified fields are 1,000, as expected");
				logFileWriter.newLine();
			}
			else verificationErrors.append ("Cluster Values other than 1,000 found where not expected");
			
			if (verifyModifiedSchoolSSWFields(clusterName, deptName, facultyName, "1,000", logFileWriter)){
				logFileWriter.write("School modified fields are 1,000, as expected");
				logFileWriter.newLine();
			}
			else verificationErrors.append ("School Values other than 1,000 found where not expected");
	
			UIController.switchWindowByURL(UIController.loadSalarySettingWindowURL, logFileWriter);
			String fileName = "UPLOAD Department Salary Setting Worksheet " + deptName +".xlsm";
			uploadAndVerifySSW(clusterName, deptName, fileName, facultyName, logFileWriter);
	
			UIController.switchWindowByURL(UIController.loadSalarySettingWindowURL, logFileWriter);
			String clusterParam = clusterName.replaceFirst(" Cluster", "");
			fileName = "UPLOAD Cluster Salary Setting Worksheet " + clusterParam +".xlsx";
			uploadAndVerifySSW(clusterName, deptName, fileName, facultyName, logFileWriter);
			
			UIController.switchWindowByURL(UIController.loadSalarySettingWindowURL, logFileWriter);
			fileName = "UPLOAD School Salary Setting Worksheet " + clusterName +".xlsx";
			uploadAndVerifySSW(clusterName, deptName, fileName, facultyName, logFileWriter);
	
			//verify that the values have now been updated
			Thread.sleep(10000);
			UIController.switchWindowByURL(UIController.loadSalarySettingWindowURL, logFileWriter);
			HandSOnTestSuite.salarySettingTabUI.verifyUIValues("0", facultyUIRow, logFileWriter, true);
			
			//now, download the spreadsheets again so that they're useable by other tests:
			downloadDeptSSW(clusterName, deptName, logFileWriter);
			UIController.switchWindowByURL(UIController.loadSalarySettingWindowURL, logFileWriter);
			downloadClusterSSW(clusterName, logFileWriter);
			UIController.switchWindowByURL(UIController.loadSalarySettingWindowURL, logFileWriter);
			downloadSchoolSSW(clusterName, logFileWriter);

		}//end for loop - iterating through different Cluster-Dept combinations
	}//end test() method
	
	protected String getFacultyForDept(String clusterName, String deptName) throws Exception{
		logFileWriter.write("*** method getFacultyForDept called with Department " 
					+deptName +", as input ***");
		logFileWriter.newLine();
		
		String facultyName = new String();
		boolean foundFaculty = false;
		int UIRow = 1;
		String baseLocator = HandSOnTestSuite.salarySettingTabUI.getLocator(facultyNameLocatorName, 1, logFileWriter);
		String locator = new String();
		downloadDeptSSW(clusterName, deptName, logFileWriter);
		while (! foundFaculty){
			if (UIRow > 1)
				locator = baseLocator.replaceFirst("/tbody/tr/", "/tbody/tr[" + UIRow + "]/");
			else
				locator = baseLocator;
			logFileWriter.write("Locator found is " + locator);
			logFileWriter.newLine();
			try{
				if (! UIController.switchWindowByURL(UIController.loadSalarySettingWindowURL, logFileWriter)){
					logFileWriter.write("Could not switch to the salary setting window.");
					logFileWriter.newLine();
				}//end if
			}
			catch(Exception e){
				logFileWriter.write("Could not switch to the salary setting window because of Exception "+e.getMessage());
				logFileWriter.newLine();
			}
			try{
				Alert alert = UIController.driver.switchTo().alert();
				logFileWriter.write("There is an alert present with the following text: ");
				logFileWriter.newLine();
				logFileWriter.write(alert.getText());
				logFileWriter.newLine();
				alert.accept();
			}
			catch(Exception e){
				logFileWriter.write("Could not acquire alert because of Exception "+e.getMessage());
				logFileWriter.newLine();
			}
			facultyName = HandSOnTestSuite.salarySettingTabUI.getTextFromElement(locator, logFileWriter);
			logFileWriter.write("Faculty examined is " + facultyName);
			logFileWriter.newLine();

			myExcelReader = new SSExcelReader(fileDirectory + "Department Salary Setting Worksheet " + deptName +".xlsm", 
					TestFileController.DepartmentSalarySettingWorksheetHeaderRow);
			myWorksheetName = deptWorksheetName;
			int rowNum = myExcelReader.getCellRowNum(myWorksheetName, "Name", facultyName);
			if (rowNum == -1){
				logFileWriter.write("Faculty not found in Department Worksheet");
				logFileWriter.newLine();
				int foundRow = myExcelReader.getCellRowNumWithSubstringMatch(myWorksheetName, "Name", facultyName, 1);
				if (foundRow < myExcelReader.getRowForColumnNames() && foundRow != -1){
					logFileWriter.write("Faculty is actually Department Chair");
					logFileWriter.newLine();
				}//end inner if - note that faculty is Department Chair
				UIRow++;
			}//end outer if - faculty member is not found in department worksheet
			else{
				foundFaculty = true;
				logFileWriter.write("Faculty member "+ facultyName +" found in Department Worksheet, will use this faculty member");
				logFileWriter.newLine();
			}//end else
		}//end while loop - 
		logFileWriter.write("Faculty Name being returned is " + facultyName +", found on UI Row " +UIRow);
		logFileWriter.newLine();
		return facultyName +";" +UIRow;
	}
	
	protected void testInitialDeptConfig(String clusterName, String deptName, String facultyName, BufferedWriter logFileWriter) throws Exception{
		UIController.switchWindowByURL(UIController.loadSalarySettingWindowURL, logFileWriter);
		downloadDeptSSW(clusterName, deptName, logFileWriter);
		if (verifyModifiedDeptSSWFields(clusterName, deptName, facultyName, "0", logFileWriter)){
			logFileWriter.write("Modifiable fields are 0, as expected");
			logFileWriter.newLine();
		}//end if
		else verificationErrors.append ("Nonzero values found where not expected");
		if (! getSSWCopy("Department Salary Setting Worksheet " + deptName +".xlsm", logFileWriter)){
			String message = "Could not create the new file to upload";
			logFileWriter.write(message); logFileWriter.newLine();
			verificationErrors.append(message);
		}//end if
	}//end testInitialDeptConfig method
	
	protected void testInitialClusterConfig (String clusterName, String deptName, String facultyName, BufferedWriter logFileWriter) throws Exception{
		UIController.switchWindowByURL(UIController.loadSalarySettingWindowURL, logFileWriter);
		String clusterUIName = clusterName.replaceFirst(" Cluster", "");
		downloadClusterSSW(clusterUIName, logFileWriter);
		if (verifyModifiedClusterSSWFields(clusterUIName, deptName, facultyName, "0", logFileWriter)){
			logFileWriter.write("Modifiable fields are 0, as expected");
			logFileWriter.newLine();
		}//end if
		else verificationErrors.append ("Nonzero values found where not expected");
		if (! getSSWCopy("Cluster Salary Setting Worksheet " + clusterUIName +".xlsx", logFileWriter)){
			String message = "Could not create the new file to upload";
			logFileWriter.write(message); logFileWriter.newLine();
			verificationErrors.append(message);
		}//end if
	}//end testInitialClusterConfig method

	protected void testInitialSchoolConfig (String clusterName, String deptName, String facultyName, BufferedWriter logFileWriter) throws Exception{
		UIController.switchWindowByURL(UIController.loadSalarySettingWindowURL, logFileWriter);
		downloadSchoolSSW(clusterName, logFileWriter);
		if (verifyModifiedSchoolSSWFields(clusterName, deptName, facultyName, "0", logFileWriter)){
			logFileWriter.write("Modifiable fields are 0, as expected");
			logFileWriter.newLine();
		}//end if
		else verificationErrors.append ("Nonzero values found where not expected");
		if (! getSSWCopy("School Salary Setting Worksheet " + clusterName +".xlsx", logFileWriter)){
			String message = "Could not create the new file to upload";
			logFileWriter.write(message); logFileWriter.newLine();
			verificationErrors.append(message);
		}//end if
	}//end testInitialClusterConfig method
	

	protected boolean getSSWCopy (String baseFileName, BufferedWriter logFileWriter) throws Exception{
		boolean fileCopied = false;
		logFileWriter.write("*** method getSSWCopy called with base file name '"+ baseFileName +"' ***");
		logFileWriter.newLine();
	    String source = fileDirectory + baseFileName;
	    String dest = fileDirectory + "UPLOAD " + baseFileName;
	    
	    logFileWriter.write("Source file: " + source);
	    logFileWriter.newLine();
	    logFileWriter.write("Destination file: " + dest);
	    logFileWriter.newLine();
	    File sourceFile = new File(source);
	    File destFile = new File(dest);
		  if (destFile.exists()){ 
			  	String message = "Report file already exists in the new location " + dest + " - must delete and recreate";
			  	logFileWriter.write(message);
			  	System.out.println(message);
			  	if (destFile.canRead() && destFile.canWrite())
			  			System.out.println("All file permissions seem valid");
			  	if (!destFile.delete()){
			  		destFile = null;
			  		System.gc();
			  		destFile = new File(dest);
			  		System.gc();
			  		try{
			  			FileUtils.forceDelete(destFile);
			  		}//end try
			  		catch(IOException e){
			  			message = "*** WARNING *** Unable to delete file " 
			  					+ dest +" - leaving old copy";
			  			System.out.println(message);
			  			logFileWriter.write(message);
			  			destFile.deleteOnExit();
			  		}//end catch clause
			  	}//end if
			  	Thread.sleep(10000);
			  	destFile = new File(dest);
		  }
	    try{
	    	FileUtils.copyFile(sourceFile, destFile);
	    }
	    catch(Exception e){
	    	logFileWriter.write("Could not complete the file copy because of Exception " +e.getMessage());
	    	logFileWriter.newLine();
	    }
	    if (destFile.exists()){
	    	logFileWriter.write("Destination file is created, as expected");
	    	logFileWriter.newLine();
	    	fileCopied = true;
	    }
	    else{
	    	logFileWriter.write("Destination file is NOT created, as expected");
	    	logFileWriter.newLine();
	    }
	    return fileCopied;
	}//end getDeptSSWCopy method
	
	protected void downloadDeptSSW (String clusterName, String deptName, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method downloadDeptSSW called with Cluster "+ clusterName 
				+", and department " + deptName + " ***");
		verificationErrors.append(HandSOnTestSuite.salarySettingTabUI.downloadDepartmentSSW(clusterName, 
				deptName, logFileWriter));
		String worksheetName = "Department Salary Setting Worksheet " + deptName +".xlsm";
		testFileDownloaded(worksheetName, logFileWriter);
	}//end downloadDeptSSW method
	
	protected void downloadClusterSSW (String clusterName, BufferedWriter logFileWriter) throws Exception{
		if (clusterName.contains(" Cluster"))
			clusterName = clusterName.replaceFirst(" Cluster", "");		
		logFileWriter.write("*** method downloadClusterSSW called with Cluster "+ clusterName + " ***");
		logFileWriter.newLine();
		verificationErrors.append(HandSOnTestSuite.salarySettingTabUI.downloadClusterSSW(clusterName, logFileWriter));
		String worksheetName = "Cluster Salary Setting Worksheet " + clusterName +".xlsx";
		testFileDownloaded(worksheetName, logFileWriter);
	}//end downloadClusterSSW method
	
	protected void downloadSchoolSSW (String clusterName, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method downloadSchoolSSW called with Cluster "+ clusterName + " ***");
		logFileWriter.newLine();
		verificationErrors.append(HandSOnTestSuite.salarySettingTabUI.downloadSchoolSSW(clusterName, logFileWriter));
		String worksheetName = "School Salary Setting Worksheet " + clusterName +".xlsx";
		testFileDownloaded(worksheetName, logFileWriter);
	}//end downloadSchoolSSW method

	protected ArrayList<Integer> getModifiableSSWColumns(String facultyName, String deptName, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method getModifiableSSWColumns called with facultyName '" + facultyName
				+"', and department name '"+deptName +"' ***");
		logFileWriter.newLine();
		
		String startColName = new String();
		if (myExcelReader.getPath().contains("School")){
			myWorksheetName = schoolWorksheetName;
			startColName = "Dean Reallocation within Depts";
			logFileWriter.write("Working with the School Worksheet");
			logFileWriter.newLine();
		}//end if - working with the School worksheet
		else if (myExcelReader.getPath().contains("Department")){
			myWorksheetName = deptWorksheetName;
			startColName = "Regular Merit Pool";
			logFileWriter.write("Working with the Department Worksheet");
			logFileWriter.newLine();
		}//end else if - working with the Department worksheet
		else {//it's the Cluster worksheet
			myWorksheetName = clusterWorksheetName;
			startColName = "Cluster Dean Reallocation within Depts";
			logFileWriter.write("Working with the Cluster Worksheet");
			logFileWriter.newLine();
		}
		
		ArrayList<Integer> modifiableColNumbers = new ArrayList<Integer>();
		int colNum = myExcelReader.getCellColumnNumber(myWorksheetName, startColName, logFileWriter, true);
		logFileWriter.write("Beginning with Column '"+startColName+"', at index " + colNum);
		logFileWriter.newLine();
		//if this is the department worksheet, take the first row with the faculty name
		int rowNum = 1;
		if (myWorksheetName.equalsIgnoreCase(deptWorksheetName) ){
			rowNum = myExcelReader.getCellRowNum(myWorksheetName, "Name", facultyName);
		}//end if
		else{ //it's either the School or Cluster Worksheet
			int startRowNum = myExcelReader.getCellRowNum(myWorksheetName, "Department", deptName);
			rowNum = myExcelReader.getCellRowNum(myWorksheetName, "Name", facultyName, 
					startRowNum, myExcelReader.getRowCount(myWorksheetName));
		}
		logFileWriter.write("Beginning with row number " + rowNum +", for Worksheet " + myWorksheetName);
		logFileWriter.newLine();
		int maxCol = myExcelReader.getColumnCount(myWorksheetName);
		
		for (int i=colNum; i<maxCol; i++){
			String actualForegroundColor = myExcelReader.getCellForegroundColor(myWorksheetName, i, rowNum);
			if (actualForegroundColor.equalsIgnoreCase(colorIndicatingSSWAdjustment)){
				logFileWriter.write("Modifiable column index " + i 
						+ ", with foreground color '" + actualForegroundColor + "' is being added");
				logFileWriter.newLine();
				modifiableColNumbers.add(new Integer(i));
			}//modifiable text field - text value is examined
			else{
				logFileWriter.write("Non-modifiable text field found at index " + i 
							+", with foreground color '" + actualForegroundColor +"' will not be added");
				logFileWriter.newLine();
			}//non-modifiable text field - text value found is irrelevant
		}//end for loop		
		return modifiableColNumbers;
	}//end getModifiableFields method
	
	protected boolean verifyModifiedDeptSSWFields(String clusterName, String deptName, String facultyName, 
			String expected, BufferedWriter logFileWriter) throws Exception{
		boolean allExpected = true;
		logFileWriter.write("*** method verifyModifiedDeptSSWFields called with Cluster "+ clusterName 
				+", and department '" + deptName + "', and faculty '"+facultyName +"' ***");
		logFileWriter.newLine();
		myExcelReader = new SSExcelReader(fileDirectory + "Department Salary Setting Worksheet " + deptName +".xlsm", 
				TestFileController.DepartmentSalarySettingWorksheetHeaderRow);
		myWorksheetName = deptWorksheetName;
		allExpected = verifyFields("Regular Merit Pool", deptName, facultyName, expected, logFileWriter);

		return allExpected;
	}//end verifyModifiedDeptSSWFields method
	
	protected boolean verifyModifiedClusterSSWFields(String clusterName, String deptName, String facultyName, String expected, 
			BufferedWriter logFileWriter) throws Exception{
		boolean allExpected = true;
		if (clusterName.contains(" Cluster"))
			clusterName = clusterName.replaceFirst(" Cluster", "");
		logFileWriter.write("*** method verifyModifiedClusterSSWFields called with Cluster '"+ clusterName 
				+ "', and Department '" + deptName +"' and faculty name '" + facultyName + "' ***");
		logFileWriter.newLine();
		myExcelReader = new SSExcelReader(fileDirectory + "Cluster Salary Setting Worksheet " + clusterName.replaceFirst(" Cluster", "") +".xlsx", 
				TestFileController.ClusterSalarySettingWorksheetHeaderRow);
		myWorksheetName = clusterWorksheetName;
		allExpected = verifyFields("Cluster Dean Reallocation within Depts", deptName, facultyName, expected, logFileWriter);
		return allExpected;
	}//end verifyModifiedClusterSSWFields method
	
	protected boolean verifyModifiedSchoolSSWFields(String clusterName, String deptName, String facultyName, 
			String expected, BufferedWriter logFileWriter) throws Exception{
		boolean allExpected = true;
		logFileWriter.write("*** method verifyModifiedSchoolSSWFields called with Cluster "+ clusterName 
				+", and department " + deptName + ", and faculty "+facultyName +" ***");
		logFileWriter.newLine();
		myExcelReader = new SSExcelReader(fileDirectory + "School Salary Setting Worksheet " + clusterName +".xlsx", 
				TestFileController.SchoolSalarySettingReportHeaderRow);
		myWorksheetName = schoolWorksheetName;
		allExpected = verifyFields("Dean Reallocation within Depts", deptName, facultyName, expected, logFileWriter);

		return allExpected;
	}//end verifyZeroedOutFields method

	
	protected boolean verifyFields(String leftmostColName, String deptName, String facultyName, String expected, BufferedWriter logFileWriter) throws Exception{
		boolean allExpected = true;
		ArrayList<Integer> modifiableSSWColumns = getModifiableSSWColumns(facultyName, deptName, logFileWriter);
		int rowNum = -1;
		if (myWorksheetName.equalsIgnoreCase(deptWorksheetName) ){
			rowNum = myExcelReader.getCellRowNum(myWorksheetName, "Name", facultyName);
		}//end if
		else{ //it's either the School or Cluster Worksheet
			int startRowNum = myExcelReader.getCellRowNum(myWorksheetName, "Department", deptName);
			rowNum = myExcelReader.getCellRowNum(myWorksheetName, "Name", facultyName, 
					startRowNum, myExcelReader.getRowCount(myWorksheetName));
		}
		for (int i=0; i<modifiableSSWColumns.size(); i++){
			String actualTextFound = myExcelReader.getCellData(myWorksheetName, modifiableSSWColumns.get(i).intValue(), rowNum);
			if(actualTextFound.equalsIgnoreCase(expected)){
				logFileWriter.write("'" + expected + "' value found in modifiable field, as expected");
				logFileWriter.newLine();
			}//end if - zero value found, as expected
			else{
				logFileWriter.write("MISMATCH text found in modifiable field is " + actualTextFound);
				logFileWriter.newLine();
				allExpected = false;
			}//end else - a nonzero value was found where it shouldn't be
		}//end for loop
		
		return allExpected;
	}//end verifyFields method
	
	protected void uploadAndVerifySSW(String clusterName, String deptName, String fileName, 
			String facultyName, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method uploadAndVerifyDeptSSW called with Cluster name " + clusterName 
				+" and Department Name " + deptName +", and File Name " + fileName 
				+", and Faculty Name " + facultyName +" ***");
		logFileWriter.newLine();
		
		if (fileName.contains("Department")){
			logFileWriter.write("Department Salary Setting Worksheet will be uploaded");
			logFileWriter.newLine();
			myExcelReader = new SSExcelReader(fileDirectory + "Department Salary Setting Worksheet " + deptName +".xlsm", 
					TestFileController.DepartmentSalarySettingWorksheetHeaderRow);
			myWorksheetName = deptWorksheetName;		
		}//end if - Department
		else if (fileName.contains("School")){//it's the School Salary Setting Worksheet
			logFileWriter.write("School Salary Setting Worksheet will be uploaded");
			logFileWriter.newLine();
			myExcelReader = new SSExcelReader(fileDirectory + "School Salary Setting Worksheet " + clusterName +".xlsx", 
					TestFileController.SchoolSalarySettingReportHeaderRow);
			myWorksheetName = clusterWorksheetName;
		}//end else if - Cluster
		else{//it's the Cluster Salary Setting Worksheet
			logFileWriter.write("Cluster Salary Setting Worksheet will be uploaded");
			logFileWriter.newLine();
			myExcelReader = new SSExcelReader(fileDirectory + "Cluster Salary Setting Worksheet " + clusterName.replaceFirst(" Cluster", "") +".xlsx", 
					TestFileController.ClusterSalarySettingWorksheetHeaderRow);
			myWorksheetName = clusterWorksheetName;
		}//end else - School
		
		int numberOfVerifiableFields = getModifiableSSWColumns(facultyName, deptName, logFileWriter).size() + 1;
		logFileWriter.write("We expect to verify "+numberOfVerifiableFields +" fields");
		if (uploadOneFile(fileName, logFileWriter)){
				logFileWriter.write("File "+fileName+" successfully uploaded");
				logFileWriter.newLine();
		}//end if
		else{
			logFileWriter.write("File "+fileName+" NOT successfully uploaded");
			logFileWriter.newLine();
		}
		UIController.switchWindowByURL(UIController.fileUploadResultWindowURL, logFileWriter);
		// use these commands to verify that the correct text is present after the upload


		
		String xPathLocator = "//tr/td";
		String textToVerify = new String();
		boolean foundNameAndDept = false;
		int minRows = 3;
		int maxRows = HandSOnTestSuite.salarySettingTabUI.getBottomRow ("//tr/td", facultyName + " (" + deptName +")", 3,
				 logFileWriter, true);
		int minCols = 1;
		int maxCols = HandSOnTestSuite.salarySettingTabUI.getRightmostColumn("//tr[" + maxRows +"]/td", logFileWriter, true);

		for (int row = minRows; ((row < maxRows) && (! foundNameAndDept)); row++){
			boolean[] textVerified = new boolean[numberOfVerifiableFields];
			int textVerifiedIndex = 0;
			boolean allFieldsVerified = false;
			for (int col = minCols;((col<maxCols)&&(textVerifiedIndex<numberOfVerifiableFields)&&(! allFieldsVerified));col++){
				if (row > 1)
					xPathLocator = "//tr["+row+"]/td";
				if (col == 1){
					textToVerify = facultyName + " (" + deptName +")";
					if (! verifyTextInConfirmationScreen(textToVerify, xPathLocator, logFileWriter)){
						//logFileWriter.write("Did not find the name/dept combo " + textToVerify +", going to next row");
						//logFileWriter.newLine();
						break;
					}//end if (this isn't the name/dept combo)
					else{
						logFileWriter.write("Found desired name/dept combo " + textToVerify);
						logFileWriter.write(", verifying the rest of the row");
						logFileWriter.newLine();
						foundNameAndDept = true;
						for (int i=0; i<textVerified.length; i++)
							textVerified[i] = false;//re-initialize this to false
						textVerifiedIndex = 0;//re-initialize this to zero
						allFieldsVerified = false;
					}//end else - this is the correct name/dept combo
				}//end if - column 1 (where the name/dept combo is)
				else if (foundNameAndDept){
					xPathLocator = xPathLocator.concat("[" + col +"]");
					textToVerify = "$0";
					try{
						if (verifyTextInConfirmationScreen(textToVerify, xPathLocator, logFileWriter)){
							logFileWriter.write("Found the desired text "+textToVerify+", at locator "+xPathLocator);
							logFileWriter.newLine();
							textVerified[textVerifiedIndex++] = true;
							logFileWriter.write("Index of expected/verifiable values incremented to " + textVerifiedIndex);
							logFileWriter.newLine();
						}//end if
						else if (foundNameAndDept){
							String message = "DID NOT find the desired text "+textToVerify+", at locator "+xPathLocator;
							logFileWriter.write(message);
							logFileWriter.newLine();
							System.out.println(message);
						}//end else if - indicate that the desired text was not found in the line for that name/dept
					}//end try
					catch(Exception e){
						logFileWriter.write("Could not find the desired text "+textToVerify+", because of Exception " + e.getMessage());
						logFileWriter.newLine();
						allFieldsVerified = true;
					}//end catch
					if (textVerifiedIndex >= numberOfVerifiableFields){//we're done
						logFileWriter.write("We have now reached text verified index "+textVerifiedIndex
								+", and we're ending our analysis for this row");
						logFileWriter.newLine();
						allFieldsVerified = true;
					}//end if
				}//end else if - column is greater than 1 and we've found the desired name/dept
			}//end inner for loop - col
			if (foundNameAndDept){
				allFieldsVerified = true;
				logFileWriter.write("We have iterated through all of the columns "
						+"- now verifying that all values are as expected");
				logFileWriter.newLine();
				for (textVerifiedIndex=0; textVerifiedIndex<textVerified.length; textVerifiedIndex++){
					if (! textVerified[textVerifiedIndex]){
						allFieldsVerified = false;
						logFileWriter.write("Value at index "+ textVerifiedIndex +" was not as expected");
						logFileWriter.newLine();
						break;
					}//end if
				}//end for loop
				
				if (allFieldsVerified){
					String message = "All expected values are found on row "+ row;
					logFileWriter.write(message);
					logFileWriter.newLine();
					System.out.println(message);
					break;//stop looking through the rows if we found the correct value
				}
				else {
					String message = "Did not find all expected values on row "+ row;
					logFileWriter.write(message);
					logFileWriter.newLine();
					System.out.println(message);
					verificationErrors.append(message);
				}//end else
			}//end if - we're analyzing the desired name and department
		}//end for loop - row
		SalarySettingTabUIController.waitAndClick(By.id("buttonSubmit"), 3);
	}//end uploadOneFile method
	
	protected boolean uploadOneFile(String fileName, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method uploadOneFile called with fileName "+ fileName +" ***");
		logFileWriter.newLine();
		
		//testFileDownloaded(fileName, logFileWriter);
		
		String xPathLocator = HandSOnTestSuite.salarySettingTabUI.getLocator("UploadWorksheetButton", 1, logFileWriter);
		Thread.sleep(3000);
		SalarySettingTabUIController.waitForElementPresent(By.xpath(xPathLocator));
		if (! SalarySettingTabUIController.waitAndClick(By.xpath(xPathLocator), 2)){
			logFileWriter.write("Could not find the upload button at xpath " + xPathLocator);
			logFileWriter.newLine();
			return false;
		}//end if
		if (! UIController.switchWindowByURL(UIController.uploadFileWindowURL, logFileWriter)){
			logFileWriter.write("Could not switch to the upload file dialog with URL " 
								+ UIController.uploadFileWindowURL);
			logFileWriter.newLine();
			return false;
		}//end if
		
		// use these commands to bring up the upload file dialog - need the upload file dialog URL
		// use these commands to upload the file on the upload file dialog
		// ERROR: Caught exception [ERROR: Unsupported command [selectWindow | name=null | ]]
		try{
			SalarySettingTabUIController.waitForElementPresent(By.id("fileLoc"));
		}//end try clause
		catch(Exception e){
			logFileWriter.write("Could not find the file path entry text field");
			logFileWriter.newLine();
			return false;
		}//end catch
		try{
			UIController.driver.findElement(By.id("fileLoc")).clear();
			UIController.driver.findElement(By.id("fileLoc")).sendKeys(fileDirectory + fileName);
		}//end try clause
		catch(Exception e){
			logFileWriter.write("Could not enter the path in the entry text field");
			logFileWriter.newLine();
			return false;
		}//end catch
		try{
			UIController.driver.findElement(By.id("buttonSubmit")).click();
		}//end try clause
		catch(Exception e){
			logFileWriter.write("Could not click on the submit button");
			logFileWriter.newLine();
			return false;
		}//end catch
		return true;
	}//end uploadOneFile method
	
	protected boolean verifyTextInConfirmationScreen(String textToVerify, String xPathLocator, 
			BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method verifyTextInConfirmationScreen called with textToVerify '" 
				+textToVerify +"', and xpath locator" + xPathLocator);
		logFileWriter.newLine();
		try {
			SalarySettingTabUIController.waitForElementPresent(By.xpath(xPathLocator));
			logFileWriter.write("Element with locator '" +xPathLocator+"' located");
			logFileWriter.newLine();
			String actualText = SalarySettingTabUIController.verifyText(By.xpath(xPathLocator), textToVerify);
			if (actualText.equalsIgnoreCase(textToVerify)){
				logFileWriter.write("Text '"+ textToVerify +"' is at xpath " +xPathLocator +", as expected");
				logFileWriter.newLine();
				return true;
			}//end if
			else{
				logFileWriter.write("Text '"+ textToVerify +"' is NOT at xpath " +xPathLocator);
				logFileWriter.newLine();
				logFileWriter.write("Actual text found is " + actualText);
				logFileWriter.newLine();
				return false;
			}//end else
		}//end try clause
		catch (Error e) {
			logFileWriter.write("Could not verify text '" +textToVerify + "' at xpath " + xPathLocator);
			logFileWriter.write(", because of Error " + e.getMessage());
			logFileWriter.newLine();
			return false;
		}//end catch clause

	}//end verifyTextInConfirmationScreen method
	
	public void testFileDownloaded(String fileName, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method testFileDownloaded called with file name " + fileName +" ***");
		logFileWriter.newLine();
		
		File f = new File (fileDirectory + fileName);
		if (! f.exists()){
			if (downloadNewFile(fileName, logFileWriter, ReportRunner.logDirectoryName)){
				logFileWriter.write("File '"+fileName+"' successfully downloaded");
				logFileWriter.newLine();
			}//end inner if - file is successfully downloaded
			else {
				logFileWriter.write("File '"+fileName+"' NOT downloaded");
				logFileWriter.newLine();
			}//end inner else - file is not successfully downloaded
		}//end outer if - file doesn't exist already
		else{
			logFileWriter.write("File '"+ fileName +"' already exists - no need to download");
			logFileWriter.newLine();
		}//end else - the file already exists
		
	}//end testForFileDownload method
	
	public boolean downloadNewFile(String fileName, BufferedWriter logFileWriter, String logDirectory) throws Exception{
		boolean fileDownloaded = false;
		logFileWriter.write("*** method downloadNewFile called for file " + fileName +" ***");
		return fileDownloaded;
	}
	
	protected Connection getConnection(BufferedWriter logFileWriter) throws Exception{
		Connection conn = null;
		try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            String url = "jdbc:oracle:thin:@//handsondb-tst4.stanford.edu:1568/HON_TST";

            Properties props = new Properties();
			props.setProperty("user", "hsondev_test");
			props.setProperty("password", "skyline20160708");
			props.setProperty("url", url);
			props.setProperty(OracleConnection.CONNECTION_PROPERTY_THIN_NET_CONNECT_TIMEOUT, "20000");


            System.out.println("Test " + testName + " running: getting connection at time " +System.currentTimeMillis());
            conn = DriverManager.getConnection(url, props);
            System.out.println("Test " + testName + " running: finished getting connection at time " +System.currentTimeMillis());
            logFileWriter.newLine();
		}//end try
	    catch (Exception e) {
            System.out.println("Test " + testName + " running: failed getting connection at time " +System.currentTimeMillis());
	        StackTraceElement[] stackTrace = e.getStackTrace();
	        for (int i=0; i<stackTrace.length; i++){
	        	logFileWriter.write(stackTrace[i].toString());
	        	logFileWriter.newLine();
	        }//end for loop
	    }//end catch clause
		finally{
			if (conn == null)
				logFileWriter.write("method getConnection is returning null Connection");
			else logFileWriter.write("Connected to database");
			logFileWriter.newLine();
		}
		return conn;		
	}//end getConnection method

	protected void runStoredProcedure(Connection conn, BufferedWriter logFileWriter) throws Exception{
		CallableStatement storedProc = null;
		try{
	        storedProc = conn.prepareCall("{call TEST_SALSET_ELIG(?, ?, ?)}");
	        storedProc.setString(1, "2017");
	        storedProc.setString(2, "04/01/2017");
	        storedProc.setString(3, null);
	        System.out.println("Test " + testName + " running: executed running stored procedure at time " +System.currentTimeMillis());
	        storedProc.execute();
            System.out.println("Test " + testName + " running: finished running stored procedure at time " +System.currentTimeMillis());
	        logFileWriter.write("Stored procedure successfully executed");
	        logFileWriter.newLine();
		}//end try block
	    catch (SQLException e) {
	    	System.out.println("Test " + testName + " running: failed running stored procedure at time " +System.currentTimeMillis());
	        StackTraceElement[] stackTrace = e.getStackTrace();
	        for (int i=0; i<stackTrace.length; i++){
	        	logFileWriter.write(stackTrace[i].toString());
	        	logFileWriter.newLine();
	        }//end for loop
	        throw e;
	    }//end catch clause
		finally{
			if (storedProc != null) storedProc.close();
            logFileWriter.newLine();
		}//end finally clause
	}//end runStoredProcedure method 
	
	private String[] getDepts(Connection conn, BufferedWriter logFileWriter) throws Exception{
		String[] depts = null;
		Statement stmt = null;
		try{
	        stmt=conn.createStatement();
	        String query = "SELECT DISTINCT ROOT_CLUSTER_NM, ORG_NM, ORG_CD FROM SALARY_SETTING_TEST "
	        			+"ORDER BY ROOT_CLUSTER_NM, ORG_NM ";
	
	        //get the results set
	        ResultSet rs=stmt.executeQuery(query);  
	        ArrayList<String> list = new ArrayList<String>();
	        while(rs.next()){
	        	String cluster_dept = rs.getString("ROOT_CLUSTER_NM") + ";" 
	        			+ rs.getString("ORG_NM") + ";" + rs.getString("ORG_CD");
	        	list.add(cluster_dept);
	        	logFileWriter.write("Adding cluster-dept-code " + cluster_dept +" to the list");
	        	logFileWriter.newLine();
	        }//end while loop
	        if (! list.isEmpty()){
	        	depts = new String[list.size()];
	        	for (int i=0; i<depts.length; i++){
	        		depts[i] = (String)list.get(i);
	        	}//end for
	        }//end if
		}//end try clause
	    catch (SQLException e) {
	        StackTraceElement[] stackTrace = e.getStackTrace();
	        for (int i=0; i<stackTrace.length; i++){
	        	logFileWriter.write(stackTrace[i].toString());
	        	logFileWriter.newLine();
	        }//end for loop
	    }//end catch clause
		finally{
			if (stmt != null) stmt.close();
		}//end finally clause
		if (depts == null){
			logFileWriter.write("returning null list of departments");
			logFileWriter.newLine();
		}
		return depts;
	}//end getDepts method


}
