package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ClusterSalarySettingExceptionsTest {
	protected static String testName;
	protected static String testCaseNumber;
	protected static String testDescription;
	protected static String testEnv;
	protected static StringBuffer verificationErrors;
	protected BufferedWriter logFileWriter;

	@Before
	public void setUp() throws Exception {
		testName = "ClusterSalarySettingExceptionsTest";
		testEnv = "Test";
		testCaseNumber = "059";
		testDescription = "Test of Exceptions for Cluster SSWs";
	}//end setUp method

	@After
	public void tearDown() throws Exception {
		TestFileController.writeTestResults(testEnv, testCaseNumber, testName, testDescription, verificationErrors);
		logFileWriter.newLine();
		logFileWriter.close();
	}//end tearDown method


	@Test
	public void test() throws Exception{
		verificationErrors = new StringBuffer();
		String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		logFileWriter = TestFileController.getLogFileWriter(logFileName);
		String[] clusters = HandSOnTestSuite.salarySettingMgrTabUI.getClusters("Exception", logFileWriter);
		logFileWriter.write("The following Cluster Salary Setting Worksheets will be examined: ");
		logFileWriter.newLine();
		for (int i=0; i<clusters.length; i++){
			clusters[i] = clusters[i].replaceFirst(" Cluster", "");
			String fileName = "Cluster Salary Setting Worksheet "+ clusters[i] + ".xlsx";
			logFileWriter.write(fileName);
			logFileWriter.newLine();
		}//end for loop
		
		//download the Cluster SSW's
		logFileWriter.write("Now attempting to download these Cluster Salary Setting Worksheets:");
		logFileWriter.newLine();
		SalarySettingManager.switchWindowByURL(SalarySettingManager.salarySettingURL, logFileWriter);
		HandSOnTestSuite.salarySettingMgrTabUI.switchToReportTab();
		ReportRunner.downloadClusterSSWs(clusters, ReportRunner.referenceWhichValues, logFileWriter);

		for (int i=0; i<clusters.length; i++){
			logFileWriter.newLine();
			logFileWriter.newLine();
			logFileWriter.write("**** Now attempting to get Exceptions for Faculty for Cluster " +clusters[i]+" ****");
			logFileWriter.newLine();
			HashMap<String, String[]> clusterFaculty 
				= HandSOnTestSuite.salarySettingMgrTabUI.getFacultyForCluster(clusters[i], "Exception", "both", logFileWriter);
			Iterator clusterIterator = clusterFaculty.entrySet().iterator();
			while(clusterIterator.hasNext()){
				Map.Entry pair = (Map.Entry)clusterIterator.next();
				String dept = (String)pair.getKey();
				logFileWriter.write("Now working with Department " + dept);
				logFileWriter.newLine();
				String[] faculty = (String[])pair.getValue();
				
				for (int j=0; j<faculty.length; j++){
					logFileWriter.write("Now working with Faculty " + faculty[j]);
					logFileWriter.newLine();
					
					String exclusionOrInclusion 
						= HandSOnTestSuite.salarySettingMgrTabUI.getExceptionAttributeForNameAndDept(faculty[j], 
							dept, "ExcludeOrInclude", logFileWriter);
					if (exclusionOrInclusion.equalsIgnoreCase("Exclude")){
						logFileWriter.write(" * Examining the file for Exclusion *");
						logFileWriter.newLine();
						if (! containsExcludedFaculty(faculty[j], dept, clusters[i], logFileWriter)) {
							String message = "ERROR - The file doesn't contain the excluded faculty "+faculty[j] +", as expected";
							logFileWriter.write(message); logFileWriter.newLine();
							verificationErrors.append(message);
						}//end if - faculty is not present
						else {
							logFileWriter.write("The file contains the excluded faculty " + faculty[j] +", as expected");
							logFileWriter.newLine();
						}//end else - faculty is present
					}//end if - it's an Exclusion
					else {//this is an inclusion
						logFileWriter.write(" * Examining the file for Inclusion *");
						logFileWriter.newLine();
						if (containsIncludedFaculty(faculty[j], dept, clusters[i], logFileWriter)){
							logFileWriter.write("Included Faculty member " + faculty[j] 
									+" is in the Cluster SSW for "+dept +", as expected");
							logFileWriter.newLine();
						}//end else if - this person is included and found
						else {
							String message = "ERROR - Included Faculty member " + faculty[j] 
									+" is NOT in the Cluster SSW for "+dept +", as expected";
							logFileWriter.write(message);
							logFileWriter.newLine();
							verificationErrors.append(message);
						}//end inner else - the Included Faculty Chair is not in the file, as expected
					}//end else - Inclusions
				}//end inner for loop - iterating through faculty
			}//end while - iterating through the different Clusters
		}//end for loop
	}//end test method
		
	private boolean containsExcludedFaculty(String facultyName, String dept, String cluster, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method containsExceptionFaculty called with faculty name " + facultyName
				+", and Department "+dept + " ***");
		logFileWriter.newLine();
		
		String fileName = "Cluster Salary Setting Worksheet "+cluster+".xlsx";
		SSExcelReader myReader = new SSExcelReader(TestFileController.directory +"/" +fileName, TestFileController.ClusterSalarySettingWorksheetHeaderRow);
		int rowCount = myReader.getRowCount(TestFileController.ClusterSalarySettingWorksheetWorksheetName); 
		logFileWriter.write("Row count for instantiated reader for " + fileName +" is " + rowCount);
		logFileWriter.newLine();

		int rowForColNames = myReader.getCellRowNum(TestFileController.ClusterSalarySettingWorksheetWorksheetName, 0, SalarySettingManagementUIController.exclusionSectionHeading);
		logFileWriter.write("Estimated location of excluded faculty at row " + rowForColNames);
		logFileWriter.newLine();

		int actualRowForColNames = myReader.getCellRowNum(TestFileController.ClusterSalarySettingWorksheetWorksheetName, 1, "Name", rowForColNames);
		logFileWriter.write("Actual header row for excluded faculty is " + actualRowForColNames);
		logFileWriter.newLine();

		//now, we examine the contents of the file to ensure that excluded faculty are there, as expected
		boolean foundUnexpectedString = false;
		int rowNum = -1;
		if (actualRowForColNames != -1){
			myReader.setRowForColumnNames(rowForColNames);
			String[] colNames = {"Name", "Department", "Reason", "Description"};
			for (int j=0; j<colNames.length; j++){
				if (j==0)
					rowNum = myReader.getCellRowNum(TestFileController.ClusterSalarySettingWorksheetWorksheetName, colNames[j], facultyName);
				logFileWriter.write("Trying to locate worksheet data on row "+rowNum);
				logFileWriter.newLine();
				String value = myReader.getCellData(TestFileController.ClusterSalarySettingWorksheetWorksheetName, colNames[j], rowNum);
				logFileWriter.write("The following value for the "+ colNames[j] +" column is found in the SSW: "+ value);
				logFileWriter.newLine();
				
				String attributeName = colNames[j];
				if (colNames[j] == "Department")
					attributeName = "Dept";
				String UIValue = new String();
				UIValue = HandSOnTestSuite.salarySettingMgrTabUI.getExceptionAttributeForNameAndDept(facultyName, dept, attributeName, logFileWriter);
				boolean stringsAreEqual = false;
				if (attributeName.equalsIgnoreCase("Description")){
					if (value.contains(UIValue) ||(value.equalsIgnoreCase(UIValue))) 
						stringsAreEqual = true;
					else stringsAreEqual = false;
				}//end if - it's the Description
				else if (UIValue.equalsIgnoreCase(value))//it's not the Description
					stringsAreEqual = true;
				if ((value == null) || (UIValue == null) || (value.isEmpty()) || (UIValue.isEmpty()) || (! stringsAreEqual)){
					logFileWriter.write("MISMATCH found --> SSW value: " + value +", UI Value: " + UIValue);
					logFileWriter.newLine();
					foundUnexpectedString = true;
				}//end if - there's a mismatch
			}//end for loop
		}//end if - we're able to find the row for column names
		TestFileController.closeOneExcelReader(myReader);
		logFileWriter.newLine();
		logFileWriter.write("Values determined by this method: ");
		logFileWriter.newLine();
		logFileWriter.write("Row containing the Override Section Header: " + rowForColNames);
		logFileWriter.newLine();
		logFileWriter.write("Row containing the Column Headers: " + actualRowForColNames);
		logFileWriter.newLine();
		logFileWriter.write("Row containing the Faculty values: " + rowNum);
		logFileWriter.newLine();
		logFileWriter.write("Unexpected String found: " + foundUnexpectedString);
		logFileWriter.newLine();
		return ((rowForColNames != -1) && (actualRowForColNames != -1) && (rowNum != -1)
				&& (actualRowForColNames == rowForColNames + 1) && (! foundUnexpectedString));
	}//end containsExcludedFaculty method
	
	public boolean containsIncludedFaculty(String facultyName, String dept, String cluster, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method containsIncludedFaculty called with faculty name " + facultyName
				+", and Department "+dept + " ***");
		logFileWriter.newLine();

		String fileName = "Cluster Salary Setting Worksheet "+cluster+".xlsx";
		SSExcelReader myReader = new SSExcelReader(TestFileController.directory +"/" +fileName, TestFileController.ClusterSalarySettingWorksheetHeaderRow);
		int rowCount = myReader.getRowCount(TestFileController.ClusterSalarySettingWorksheetWorksheetName); 
		logFileWriter.write("Row count for instantiated reader for " + fileName +" is " + rowCount);
		logFileWriter.newLine();

		myReader.setRowForColumnNames(TestFileController.ClusterSalarySettingWorksheetHeaderRow);
		boolean foundUnexpectedString = false;	
		int rowNum = myReader.getCellRowNum(TestFileController.ClusterSalarySettingWorksheetWorksheetName, 1, facultyName);
		String actualDept = myReader.getCellData(TestFileController.ClusterSalarySettingWorksheetWorksheetName, "Department", rowNum);
		if (! actualDept.equalsIgnoreCase(dept)){
			rowNum = myReader.getCellRowNum(TestFileController.ClusterSalarySettingWorksheetWorksheetName, 1, facultyName, rowNum);
			if (rowNum != -1){
				actualDept = myReader.getCellData(TestFileController.ClusterSalarySettingWorksheetWorksheetName, "Department", rowNum);
				if (! actualDept.equalsIgnoreCase(dept)){
					foundUnexpectedString = true;
					logFileWriter.write("ERROR - Included Faculty Department "+actualDept+" DOES NOT match expected department value "+dept);
					logFileWriter.newLine();	
				}//end if - actual department value is not as expected
			}//end if - we found a second department for this faculty member
			else{
				foundUnexpectedString = true;
				logFileWriter.write("ERROR - Included Faculty Department "+actualDept+" DOES NOT match expected department value "+dept);
				logFileWriter.newLine();				
			}//end else - the Description is not there
		}//end if - make sure the department matches as well
		logFileWriter.write("Estimated location of included faculty at row " + rowNum);
		logFileWriter.newLine();
	
		if (rowNum != -1){
			String colName = "16-17 Description";
			String cellValue = myReader.getCellData(TestFileController.ClusterSalarySettingWorksheetWorksheetName, colName, rowNum);
			String UIValue = new String();
			UIValue = HandSOnTestSuite.salarySettingMgrTabUI.getExceptionAttributeForNameAndDept(facultyName, dept, "Description", logFileWriter);
			if (cellValue.contains(UIValue)){
				logFileWriter.write("Included Faculty attribute with column " +colName +" includes expected text "+UIValue);
				logFileWriter.newLine();
			}//end if - Inclusion Description is in the file, as expected
			else{
				foundUnexpectedString = true;
				logFileWriter.write("ERROR - Included Faculty attribute with column " +colName +" DOES NOT INCLUDE expected text "+UIValue);
				logFileWriter.newLine();				
			}//end else - the Description is not there
		}//end if - they found the proper row
		else{
			logFileWriter.write("ERROR - Could not find Included Faculty member " +facultyName +" in the Chair SSW for Department "+dept);
			logFileWriter.newLine();
		}//end else
		
		TestFileController.closeOneExcelReader(myReader);
		
		return ((rowNum != -1) && (! foundUnexpectedString));


	}//end containsIncludedFaculty method

}//end Class
