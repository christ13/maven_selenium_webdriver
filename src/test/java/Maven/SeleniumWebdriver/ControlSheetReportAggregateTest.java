package Maven.SeleniumWebdriver;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeSet;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
/*
 * This Class will read values from the Control Sheet Detail Report and come up with aggregate
 * head count and dollar amount information, and compare it to the values present in the 
 * Control Sheet Report.
 * Aggregate head counts and dollar amounts are by Stage, by Subcategory, and by Pool.
 * There are also "Total" amounts per Stage (for all Pools) and for all Stages (and all Pools).
 */
public class ControlSheetReportAggregateTest extends ASCTest {
	
	private String[] subcategories;
	private String[] subcatExceptions = {"Transfers", "Overspending", "Other Reserves", "School Dean Reserve"};
	private String[] stages;
	private String[] sourcePools;
	private static final int stageRow = TestFileController.ControlSheetReportColumnHeaderRow;
	private static final int poolRow = TestFileController.ControlSheetReportColumnHeaderRow + 1;
	private static final int subcategoryColumn = 0;
	private static final String total = "Total";
	private String sourceDataSheet = TestFileController.ControlSheetDetailReportWorksheetName;
	private ArrayList<columnAttributes> columns;
	private ArrayList<rowAttributes> rows;
	private ArrayList<cellAttributes> cells;

	@Before
	public void setUp() throws Exception {
		myExcelReader = TestFileController.ControlSheetReportSuite;
		myExcelReader.setRowForColumnNames(TestFileController.ControlSheetDetailReportColumnHeaderRow);
		mySheetName = TestFileController.ControlSheetReportWorksheetName;
		rowForColumnNames = TestFileController.ControlSheetReportColumnHeaderRow;
		verificationErrors = new StringBuffer();
		testName = "ControlSheetReportAggregateTest";
		testEnv = "Test";
		testCaseNumber = "024";
		testDescription = "Aggregate Values Test of Control Sheet Report";
		logFileName = "Control Sheet Aggregate Values Test.txt";
		logFileWriter = TestFileController.getLogFileWriter(logDirectoryName  +"/" + logFileName);
		subcategories = myExcelReader.getAllDiscreteColumnValues(sourceDataSheet, "Subcategory", logFileWriter);
		stages = myExcelReader.getAllDiscreteColumnValues(sourceDataSheet, "Stage", logFileWriter);
		String[] tempPools = myExcelReader.getAllDiscreteColumnValues(sourceDataSheet, "Source Pool", logFileWriter);
		sourcePools = new String[tempPools.length + 1];
		sourcePools[0] = total;
		for (int i=1;i<sourcePools.length;i++) sourcePools[i] = tempPools[i-1];
		columns = new ArrayList<columnAttributes>();
		rows = new ArrayList<rowAttributes>();
		cells = new ArrayList<cellAttributes>();
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void test() throws Exception{
		//super.test();
		listDataCategoryArrays();
		getRowAttributes();
		getColumnAttributes();
		initializeCellAttributes();
		giveCellAttributesActualValues();
		getExpectedCellValues();
		getIndividualStagesTotalValues();
		getAllStagesTotalValues();
		compareCellValues();
	}//end test method
	
	private void getRowAttributes() throws Exception {
		outputMessage("method getRowAttributes has been called", logFileWriter);
		myExcelReader.setRowForColumnNames(TestFileController.ControlSheetReportColumnHeaderRow);
		int startRowNum = myExcelReader.getRowForColumnNames();
		int lastRowNum = myExcelReader.getRowCount(mySheetName)-1;
		//we need to iterate through the rows, adding rowAttributes if it's a valid subcategory
		for (int rowNum = startRowNum; rowNum < lastRowNum; rowNum++){
			String subcategory = myExcelReader.getCellData(mySheetName, subcategoryColumn, rowNum);
			String nextSubcategory = myExcelReader.getCellData(mySheetName, subcategoryColumn, rowNum+1);
			if (isStringPresentInArray(subcategory, subcategories) 
						&& (! subcategory.equalsIgnoreCase(nextSubcategory))
						&&(! isStringPresentInArray(subcategory, subcatExceptions))){
				rowAttributes newRowAttributes = new rowAttributes(subcategory, rowNum);
				rows.add(newRowAttributes);
				//outputMessage(newRowAttributes.toString(), logFileWriter);
			}//end if - found a match
			else {
				String message = "no match: value read at row number " + rowNum +" is " + subcategory;
				//outputMessage(message, logFileWriter);
			}//end else - no match
		}//end for loop - iterating through all of the rows
	}
	
	private void getColumnAttributes() throws Exception{
		TestFileController.writeToLogFile("method getColumnAttributes has been called", logFileWriter);
		myExcelReader.setRowForColumnNames(TestFileController.ControlSheetReportColumnHeaderRow);
		int startColNum = 1;
		int lastColNum = myExcelReader.getColumnCount(mySheetName);
		for (int colNum = startColNum; colNum < lastColNum; colNum++){
			String poolName = myExcelReader.getCellData(mySheetName, colNum, poolRow);
			if (isStringPresentInArray(poolName, sourcePools)){
				String stage = myExcelReader.getCellData(mySheetName, colNum, stageRow);
				if (stage.equalsIgnoreCase("")) stage = columns.get(columns.size()-2).stage;
				columnAttributes newColumnAttributes = new columnAttributes(colNum, stage, poolName);
				if (total.equalsIgnoreCase(myExcelReader.getCellData(mySheetName, colNum, poolRow))){
					newColumnAttributes.isDollarAmt = false;
					columnAttributes nextColumnAttributes = new columnAttributes(++colNum, stage, poolName);
					columns.add(nextColumnAttributes);
					//outputMessage(nextColumnAttributes.toString(), logFileWriter);
				}
				columns.add(newColumnAttributes);
				//outputMessage(newColumnAttributes.toString(), logFileWriter);
			}//end if - the Pool is in the Pool array
			else{
				String message = "No match: value read at column number " + colNum
						+ " is " + poolName;
				//outputMessage(message, logFileWriter);
			}//end else - the Pool is not in the Pool array
		}//end for loop - iterating through all of the columns
	}//end method
	
	private void initializeCellAttributes() throws Exception{
		String message = "*** method initializeCellAttributes has been called ***";
		//outputMessage(message, logFileWriter);
		for (int col=0;col<columns.size(); col++){
			for (int row=0;row<rows.size(); row++){
				cellAttributes newCell = new cellAttributes(columns.get(col), rows.get(row));
				cells.add(newCell);
				message = "new cell added to cells ArrayList with column " + newCell.myColumn.colNum
							+" and row " + newCell.myRow.rowNum;
				//outputMessage(message, logFileWriter);
			}//end inner for - rows
		}// end outer for - columns
		message = "ArrayList cells has been created with size " + cells.size();
		//outputMessage(message, logFileWriter);
	}
	
	private boolean isStringPresentInArray(String element, String[] stringArray){
		boolean match = false;
		for(int i=0; (i<stringArray.length) && (match == false); i++){
			match = element.equalsIgnoreCase(stringArray[i]);
		}
		return match;
	}//end isStringPresentInArray method
	
	private void giveCellAttributesActualValues() throws Exception{
		String message = "*** method giveCellAttributesActualValues has been called ***";
		outputMessage(message, logFileWriter);
		message = "Actual Cell Values are as follows";
		//outputMessage(message, logFileWriter);
		for(int index=0; index<cells.size(); index++){
			cellAttributes newCell = cells.get(index);
			String cellData = myExcelReader.getCellData(mySheetName, newCell.myColumn.colNum, newCell.myRow.rowNum);
			double actual = Double.valueOf((Long) NumberFormat.getNumberInstance(java.util.Locale.US).parse(cellData));         
			newCell.actualValue = (int) actual;
			cells.remove(index);
			cells.add(index, newCell);
			//outputMessage(newCell.toString(), logFileWriter);
		}
	}
	/*
	 * The goal of this method is to get the summary values from the Control Sheet Detail Report.
	 * There are multiple steps involved.  
	 * First, we have to get the summary values for all of the Pools except the "Total" values,
	 * and then, after we get all of the Pool values, we add them up to get the "Total" values. 
	 */
	private void getExpectedCellValues() throws Exception {
		for(int cellIndex=0; cellIndex<cells.size(); cellIndex++){
			cellAttributes newCell = cells.get(cellIndex);
			newCell = getExpectedSummaryValue(newCell);
			cells.remove(cellIndex);
			cells.add(cellIndex, newCell);
			//outputMessage(newCell.toString(), logFileWriter);
		}//end for loop
	}// end getExpectedCellValues method
	
	private cellAttributes getExpectedSummaryValue(cellAttributes cell) throws Exception{
		String message = "*** method getExpectedSummaryValue has been called ***";
		//outputMessage(message, logFileWriter);
		myExcelReader.setRowForColumnNames(TestFileController.ControlSheetDetailReportColumnHeaderRow);
		HashMap<String, String> searchCriteria = new HashMap<String, String>();
		if (! cell.myColumn.pool.equalsIgnoreCase(total)) searchCriteria.put("Source Pool", cell.myColumn.pool);
		if (! cell.myColumn.stage.equalsIgnoreCase(total)) searchCriteria.put("Stage", cell.myColumn.stage);
		searchCriteria.put("Subcategory", cell.myRow.subcategory);
		boolean emptyDatesExcluded = false;
		boolean outsideDatesIncrementFacultyCount = true;
		boolean outsideDatesIncrementSalarySum = false;
		String[] columnNames = {"FTE Adjusted DO Share Amount"};
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Date minDate = sdf.parse("09/01/2016");
		Date maxDate = sdf.parse("09/01/2017");
		BigDecimal[] summaries = myExcelReader.getColumnSummaryValues(sourceDataSheet, searchCriteria, emptyDatesExcluded, 
				outsideDatesIncrementFacultyCount, outsideDatesIncrementSalarySum, 
				columnNames, minDate, maxDate, logFileWriter);
		cell.permittedVariance = summaries[0].intValue();
		if (cell.myColumn.isDollarAmt) cell.expectedValue = summaries[1].intValue();
		else{
			cell.expectedValue = summaries[0].intValue();
		}
		return cell;
	}//end getExpectedSummaryValue method

	/*
	 * This method will update the Discrete Faculty head count Total values for each stage
	 * for each subcategory.
	 * There are multiple steps to this.  
	 * If the Dean's Office is not contributing to the salary of that Faculty member, then that 
	 * Faculty member is not counted toward either the dollar amount or the discrete head count,
	 * UNLESS the Pool is the Regular Merit Pool.  Then, even if the dollar amount is zero,
	 * the entry still contributes to the discrete faculty head count.
	 * 
	 * There's another exception to this.  If the Faculty member gets this salary adjustment
	 * as an ASC, then the DO Share has to be nonzero in order to have this faculty member
	 * increment the discrete head count, even if the Source Pool is the Regular Merit Pool.
	 * 
	 * So, we need to fix this so that it will work with all discrete faculty who either belong
	 * to the Regular Merit Pool with an Effective Date, or have a nonzero amount in the column named 
	 * "FTE Adjusted DO Share Amount".
	 * 
	 * For this purpose, what we need is to get a list of all rows in the Control Sheet Detail
	 * Report with a given subcategory and a given stage, then count all the rows with the 
	 * "Source Pool" column equal to "Regular Merit Pool" AND having an Effective Date
	 * --OR-- rows having a nonzero value in the "FTE Adjusted DO Share Amount" column.
	 * 
	 */
	private void getIndividualStagesTotalValues() throws Exception{
		String message = "*** method getIndividualStagesTotalValues has been called ***";
		outputMessage(message, logFileWriter);
		String sheetName = sourceDataSheet;
		String[] columnNames = {"Name", "Source Pool", "FTE Adjusted DO Share Amount", "Effective Date"};
		for(int subcategoryIndex=0; subcategoryIndex < subcategories.length; subcategoryIndex++){
			for(int stageIndex=0; stageIndex<stages.length; stageIndex++){
				HashMap<String, String> searchCriteria = new HashMap<String, String>();
				searchCriteria.put("Subcategory", subcategories[subcategoryIndex]);
				searchCriteria.put("Stage", stages[stageIndex]);
				String[][] facultyValues = myExcelReader.getMultipleFacultyMultipleValues(sheetName, searchCriteria, columnNames, logFileWriter);
				TreeSet<String> faculty = new TreeSet<String>();
				for(int rowIndex=0; rowIndex<facultyValues.length; rowIndex++){
					boolean isASC = false;
					if ((facultyValues[rowIndex][3].isEmpty()) || (facultyValues[rowIndex][3].equalsIgnoreCase(" "))){//it's not blank in the Control Sheet Detail Report
						outputMessage("Faculty " + facultyValues[rowIndex][0] + " has an ASC", logFileWriter);
						isASC = true;
					}
					boolean isRegularMeritPool = facultyValues[rowIndex][1].equalsIgnoreCase("Regular Merit Pool");
					outputMessage("Faculty " + facultyValues[rowIndex][0] + " is part of the Regular Merit Pool :" + isRegularMeritPool, logFileWriter);
					boolean hasNonZeroDOShare = ! facultyValues[rowIndex][2].equalsIgnoreCase("0");
					outputMessage("Faculty " + facultyValues[rowIndex][0] + " has non-zero DO Share: "+hasNonZeroDOShare, logFileWriter);
					if ( (isRegularMeritPool && (! isASC)) || (hasNonZeroDOShare) ){
							outputMessage("Adding Faculty " + facultyValues[rowIndex][0] + " to the count", logFileWriter);
							faculty.add(facultyValues[rowIndex][0]);
					}//end if - add the name to faculty
					else outputMessage("Not adding Faculty " + facultyValues[rowIndex][0] 
							+ " which is part of the Regular Merit Pool " + isRegularMeritPool
							+" and is ASC: " + isASC +" and has NonZeroDOShare: " + hasNonZeroDOShare, logFileWriter);
				}//end outer for - rows
				int count = faculty.size();
				//now that we know what nonZeroCount is, assign it to the appropriate cellAttributes Object
				for (int cellIndex=0; cellIndex<cells.size(); cellIndex++){
					if (
							(cells.get(cellIndex).myColumn.stage.equalsIgnoreCase(stages[stageIndex]))
							&& (cells.get(cellIndex).myRow.subcategory.equalsIgnoreCase(subcategories[subcategoryIndex]))
							&& (! cells.get(cellIndex).myColumn.isDollarAmt)
							){
						cells.get(cellIndex).expectedValue = count;
						message = "Total discrete faculty count updated for cell " + cellIndex
								+" with attributes "+cells.get(cellIndex).toString();
						outputMessage(message, logFileWriter);
					}//end if
				}//end for - iterating through the list of cells
			}//end for loop - stages
		}//end for loop - subcategories
	}//end method getIndividualStagesTotalValues

	
	/*
	 * This method will update the Discrete Faculty head count Total values for all stages
	 * per subcategory.
	 * There are multiple steps to this.  
	 * First, we have to get the discrete Faculty head count per stage, per subcategory,
	 * as things stand in the cellAttributes ArrayList currently.
	 * then we have to add them all up (all stages) per subcategory,
	 * then we have to update the total per subcategory in each cellAttribute.
	 */
	private void getAllStagesTotalValues() throws Exception{
		//for each subcategory, find the discrete faculty head count per stage
		for(int subcategoryIndex=0; subcategoryIndex < subcategories.length; subcategoryIndex++){
			int totalStages=0;
			for(int cellIndex=0; cellIndex<cells.size(); cellIndex++){
				cellAttributes thisCellAtts = cells.get(cellIndex);
				if (thisCellAtts.myRow.subcategory.equalsIgnoreCase(subcategories[subcategoryIndex])
						&& (! thisCellAtts.myColumn.isDollarAmt) 
						&&(! thisCellAtts.myColumn.stage.equalsIgnoreCase(total))){
					totalStages += thisCellAtts.actualValue;
					String message = "value " + thisCellAtts.actualValue 
							+" has been added to the total headcount for subcategory " + subcategories[subcategoryIndex]
							+" for stage " + thisCellAtts.myColumn.stage
							+" for an updated value of " + totalStages;
					//outputMessage(message, logFileWriter);
				}//end if - it's a total head count value for a stage, not for the grand total
			}//end inner for loop - iterating through the cells ArrayList to get the totals
			for(int cellIndex=0; cellIndex<cells.size(); cellIndex++){
				cellAttributes thisCellAtts = cells.get(cellIndex);
				if (thisCellAtts.myColumn.stage.equalsIgnoreCase(total) 
						&& (thisCellAtts.myColumn.pool.equalsIgnoreCase(total))
						&& (! thisCellAtts.myColumn.isDollarAmt)
						&& (thisCellAtts.myRow.subcategory.equalsIgnoreCase(subcategories[subcategoryIndex]))){
					thisCellAtts.expectedValue = totalStages;
					cells.remove(cellIndex);
					cells.add(cellIndex, thisCellAtts);
					String message = "total headcount for subcategory " + subcategories[subcategoryIndex]
							+" has a total value of " + thisCellAtts.expectedValue;
					//outputMessage(message, logFileWriter);
				}
			}//end inner for loop - iterating through the cells ArrayList to update the total values
		}//end outer for loop - iterating through all of the available categories
	}//end getAllStagesTotalValues method
	
	public void compareCellValues() throws Exception{
		for(int cellIndex=0; cellIndex<cells.size(); cellIndex++){
			cellAttributes newCell = cells.get(cellIndex);
			String message = "";
			if (newCell.myColumn.isDollarAmt){
				if ((newCell.expectedValue >= (newCell.actualValue - newCell.permittedVariance))
						||((newCell.expectedValue <= (newCell.actualValue + newCell.permittedVariance)))){
					message = "Match: " + newCell.toString();
				}//end if - the dollar amount falls within the permitted range
				else{
					message = "MISMATCH: " + newCell.toString() + ";\n";
					verificationErrors.append(message);
				}//end else - the dollar amount doesn't fall within the permitted range
			}//handling of dollar amount
			else{//it's not a dollar amount, but a faculty head count
				if (newCell.expectedValue == newCell.actualValue){
					message = "Match: " + newCell.toString();
				}//end if - faculty count matches
				else{
					message = "MISMATCH: " + newCell.toString() + ";\n";
					verificationErrors.append(message);
				}//end else - faculty count doesn't match
			}//handling of faculty head count
			outputMessage(message, logFileWriter);
		}//end for loop
	}//end compareCellValues method
	
	private void listDataCategoryArrays() throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("subcategories found are as follows: ");
		for (int i = 0; i< subcategories.length; i++){
			logFileWriter.write(subcategories[i] +"; ");
		}//end for loop
		logFileWriter.newLine();
		logFileWriter.write("stages found are as follows: ");
		for (int i = 0; i< stages.length; i++){
			logFileWriter.write(stages[i] +"; ");
		}//end for loop
		logFileWriter.newLine();
		logFileWriter.write("Source Pools found are as follows: ");
		for (int i = 0; i< sourcePools.length; i++){
			logFileWriter.write(sourcePools[i] +"; ");
		}//end for loop
		logFileWriter.newLine();
	}// end listDataCategoryArrays method

	private class columnAttributes{
		public String stage;
		public String pool;
		public int colNum;
		public boolean isDollarAmt = true;
		public columnAttributes(int colNum, String stage, String pool){
			this.colNum = colNum;
			this.stage = stage;
			this.pool = pool;
		}//end constructor
		public String toString(){
			String message = "column attributes has Pool Name " + this.pool
					+ " and Stage " + this.stage 
					+ " and isDollarAmt " + this.isDollarAmt
					+ " at column number " + this.colNum;
			return message;
		}//end toString() method
	}//end private class columnAttributes
	
	private class rowAttributes{
		public String subcategory;
		public int rowNum;
		public rowAttributes(String subcategory, int rowNum){
			this.subcategory = subcategory;
			this.rowNum = rowNum;
		}//end constructor
		public String toString(){
			String message = "rowAttributes has row number " 
					+ this.rowNum +" and subcategory " + this.subcategory;
			return message;
		}//end toString() method
	}//end private class rowAttributes
	
	private class cellAttributes{
		public columnAttributes myColumn;
		public rowAttributes myRow;
		public int expectedValue;
		public int actualValue;
		public int permittedVariance;
		public cellAttributes (columnAttributes col, rowAttributes row){
			this.myColumn = col;
			this.myRow = row;
			this.expectedValue = 0;//most of the time, it will be zero
		}//end constructor
		public String toString(){
			String message = "cell has row " + this.myRow.rowNum
					+ " and subcategory " + this.myRow.subcategory
					+ ", and column " + this.myColumn.colNum
					+ " and Pool " + this.myColumn.pool + " and Stage " + this.myColumn.stage
					+ " and expected value " + this.expectedValue
					+ " and actual value " + this.actualValue;
			return message;
		}//end toString() method
	}//end private class cellAttributes
}


