package Maven.SeleniumWebdriver;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.TreeSet;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.*;


public class SSExcelReader {
	//these constants allow external classes to access the correct type of workbook without memorizing the index
	public static final int HSSF = 0;
	public static final int XSSF = 1;
	
	private int whichWorkbookType = XSSF;
	private String path;
	private File file;
	public  FileInputStream fileIn = null;
	public  FileOutputStream fileOut = null;
	private Workbook[] workbooks;
	private Workbook workbook;
	//private CreationHelper createHelper = null;
	private Sheet sheet = null;
	private Row row   =null;
	private Cell cell = null;
	private int rowForColumnNames;

	public SSExcelReader(String path) throws Exception{
		this(path, 0);
	}//end constructor

	//use this constructor if and only if the file already exists
	public SSExcelReader(String path, int rowForColumnNames) throws Exception{
		ZipSecureFile.setMinInflateRatio(0.0d);
		this.rowForColumnNames = rowForColumnNames;
		this.path=path;
		workbooks = new Workbook[2];
		String extension = path.substring(path.lastIndexOf("."), path.length());
		System.out.println("File extension found is "+extension);
		if ((extension.equalsIgnoreCase(".xls"))){
			whichWorkbookType = HSSF;
			System.out.println("SSExcelReader is being initialized as HSSF");
		}//end if
		else
			System.out.println("SSExcelReader is being initialized as XSSF");
		String worksheetName = new String();
		try {
			this.workbook = getWorkbook(path);
			workbooks[whichWorkbookType] = workbook;
			worksheetName = workbook.getSheetAt(0).getSheetName();
			System.out.println("Got a valid worksheet in the constructor - first sheet is "+worksheetName);
		}
		catch(Exception e) {
			System.out.println("Could not find a valid worksheet in the constructor because of Exception: "+e.getMessage());
			//workbook = workbooks[whichWorkbookType];
		}
		//createHelper = workbook.getCreationHelper();
		/*
		System.out.println("Now determining if there is an existing worksheet in the Excel file ...");
		try {
			sheet = workbook.getSheetAt(0);
			worksheetName = sheet.getSheetName();
			System.out.println("Worksheet found: "+worksheetName);
		}//end try
		catch(Exception e) {
			System.out.println("Worksheet not found ... file has no sheets");
			System.out.println("Exception noted is "+e.getMessage());
		}
		*/
		//workbook.close();
	}// end constructor
	
	private Workbook getWorkbook(String path) throws Exception{
		try{
				this.file = new File(path);
				if (file.length() > 0) {//do the following if the file is not zero length
					System.out.println("File is not empty - initializing the reader with existing file content");
					fileIn = new FileInputStream(file);
					if (whichWorkbookType == HSSF)
						workbooks[whichWorkbookType] = new HSSFWorkbook(fileIn);
					else 
						workbooks[whichWorkbookType] = new XSSFWorkbook(fileIn);
				}//end if
				else {//do the following if the file is zero length
					System.out.println("File is empty - initializing the file");
					file.createNewFile();
					fileOut = new FileOutputStream(file);
					if (whichWorkbookType == HSSF)
						workbooks[whichWorkbookType] = new HSSFWorkbook();
					else 
						workbooks[whichWorkbookType] = new XSSFWorkbook();
				}//end else
				this.workbook = workbooks[whichWorkbookType];
				//fileOut = new FileOutputStream(file);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return workbooks[whichWorkbookType];
	}// end getWorkbook method
	

	
	public void closeWorkbook() throws IOException{
		fileOut.flush();
		fileOut.close();
		workbook.close();
	}
	
	public FileOutputStream getFileOutputStream() {
		return fileOut;
	}

	
	public String getPath(){
		return this.path;
	}//end getPath() method
	
	public int getRowCount(String sheetName){
		int index = workbook.getSheetIndex(sheetName);
		if(index==-1){
			System.out.println("Can't get the worksheet - returning zero");
			return 0;
		}
		else{
		sheet = workbook.getSheetAt(index);
		int number=sheet.getLastRowNum()+1;
		return number;
		}//end else
	}//end getRowCount method
	
	public String[] getColumns(String sheetName){
		int index = workbook.getSheetIndex(sheetName);
		if(index==-1)
			return new String[0];
		
		sheet = workbook.getSheetAt(index);
		row=sheet.getRow(rowForColumnNames);
		String[] columnNames = new String[row.getLastCellNum()];
		for(int i=0;i<row.getLastCellNum();i++){
			//System.out.println(row.getCell(i).getStringCellValue().trim());
			columnNames[i] = row.getCell(i).getStringCellValue().trim();
		}
		return columnNames;
	}
	
	public String[] getColumnNames (String sheetName){
		String[] columnNames = new String[getColumnCount(sheetName)];
		
		for (int i = 0; i < columnNames.length; i++){
			columnNames[i] = getCellData(sheetName, i, rowForColumnNames+1) + "; ";
		}
		
		return columnNames;
	}

	
	public int getColumnCount(String sheetName){
		// check if sheet exists
		if(!isSheetExist(sheetName))
		 return -1;
		
		sheet = workbook.getSheet(sheetName);
		row = sheet.getRow(rowForColumnNames);
		
		if(row==null)
			return -1;
		
		return row.getLastCellNum();
	}//

	  // find whether sheets exists	
		public boolean isSheetExist(String sheetName){
			int index = workbook.getSheetIndex(sheetName);
			if(index==-1){
				index=workbook.getSheetIndex(sheetName.toUpperCase());
					if(index==-1)
						return false;
					else
						return true;
			}
			else
				return true;
		}//end isSheetExist method
		
		public LinkedList<HashMap<String, String>> getMultipleValuesInRows (String sheetName, HashMap<String, String> searchCriteria, String[] columnNames, BufferedWriter logFileWriter) throws Exception{
			logFileWriter.newLine();
			logFileWriter.write("****Method SSExcelReader.getMultipleValuesInRows has been called for worksheet " + sheetName + "****");
			logFileWriter.newLine();		
			
			LinkedList<HashMap<String, String>> matchedRows = new LinkedList<HashMap<String, String>>();
			
			//start with the first name-value pair in the HashMap - look for a row that matches it
			//rowIndex will store the row number we're in so we don't try matching the same row twice
			String name = (String)searchCriteria.keySet().toArray()[0];
			String value = (String)searchCriteria.get(name);
			int rowIndex = getCellRowNum(sheetName, name, value, rowForColumnNames);
			String message = "initial value found at row " + rowIndex;
			TestFileController.writeToLogFile(message, logFileWriter);

			//write the first column and value to the output file and indicate that a match is found
			message = "first criterion column name: " + name + ", first criterion column value: " + value;
			TestFileController.writeToLogFile(message, logFileWriter);
			//System.out.println(message);
			logFileWriter.newLine();
			int numberOfCriteria = searchCriteria.size();
			//look for rows that match the searchCriteria
			//while we still haven't gone through every row in the worksheet
			while( rowIndex != -1){
				
				String foundValue = getCellData(sheetName, name, rowIndex);
				boolean match = true;
				//next, when a match is found, loop (inner loop) through the rest of the name-value pairs
				for (int criterionIndex = 1; criterionIndex < numberOfCriteria && match; criterionIndex++){
					//send information to the output file - it matches the criteria we've examined so far
					//System.out.println(", match");
					//do a comparison on the value in the next column in the row that matched the first value
					//update name and value to the next column name and value
					name = (String)searchCriteria.keySet().toArray()[criterionIndex];
					value = (String)searchCriteria.get(name);
					message = "column name : " + name + ", column value : " + value;
					//System.out.println(message);
					//logFileWriter.write(message);
					//retrieve the cell value in the next column in the same row
					foundValue = getCellData(sheetName, name, rowIndex);
					//compare the actual value found to the expected value
					match = foundValue.equalsIgnoreCase(value);
					//if match = false, write it to the output file
					if (match){
						//logFileWriter.write(", match");
					}//end if - match
					else{
						//System.out.println(", MISMATCH - going on to find the next match");
						//logFileWriter.write(", MISMATCH - going on to find the next match");
					}// end else - mismatch
					//logFileWriter.newLine();
				}// end inner for loop
				/* 
				 * If we've gone all the way through all the criteria and it matches the criteria, 
				 * then we want the values in this row that correspond to the column names given as input
				 */
				
				if(match){
					logFileWriter.newLine();
					message = "Found a matching row (row number " +rowIndex + ")- creating HashMap of matching row values";
					//System.out.println(message);
					logFileWriter.write(message);
					logFileWriter.newLine();
					HashMap<String, String> matchedRow = new HashMap<String, String>();
					//loop through all the column names and retrieve values in this row for them
					for (int columnIndex = 0; columnIndex<columnNames.length; columnIndex++){
						String targetColumn = columnNames[columnIndex];
						String targetValue = getCellData(sheetName, targetColumn, rowIndex);
						logFileWriter.write("Adding value... column name: " + targetColumn);
						logFileWriter.write(", column value: " + targetValue);
						logFileWriter.newLine();
						matchedRow.put(targetColumn, targetValue);
					}// end for - all target values in the row have been added to this HashMap
					matchedRows.add(matchedRow);
					logFileWriter.write("Added the matching row at row index " + rowIndex + " to the list of matches");
					logFileWriter.newLine();
				}//end if - the match criteria for the row is satisfied

				//now that we know we've had a matched row, reset the search criterion
				name = (String)searchCriteria.keySet().toArray()[0];
				value = (String)searchCriteria.get(name);
				
				//increment the rowIndex and search for the next match on the first criterion
				rowIndex = getCellRowNum(sheetName, name, value, ++rowIndex);
				if (rowIndex == -1){
					//System.out.println("The worksheet " + sheetName + " has been searched.  There are no more matches.");
					logFileWriter.write("The worksheet " + sheetName + " has been searched.  There are no more matches.");
					logFileWriter.newLine();
				}//end if - the output file has been updated with the information

			}// end of outer while loop - we're all done adding HashMaps to the LinkedList of HashMaps
			
			//and when we're finally finished going through all of the rows of the worksheet, return the LinkedList
			return matchedRows;
		}//end getMultipleValuesInRows method


	public void setRowForColumnNames(int newRowForColumnNames){
		rowForColumnNames = newRowForColumnNames;
	}
	
	public int getRowForColumnNames (){
		return rowForColumnNames;
	}
	
	public int getCellRowNum(String sheetName, String colName, String cellValue){
		return getCellRowNum(sheetName, colName, cellValue, rowForColumnNames+1, getRowCount(sheetName));
	}
	
	public int getCellRowNum(String sheetName,String colName,String cellValue, int startRow, int endRow){
		return getCellRowNum(sheetName, colName, cellValue, startRow, endRow, false);
	}
	
	public int getCellRowNum(String sheetName,String colName,String cellValue, int startRow, int endRow, boolean ignoreWhiteSpace){
		//System.out.println("method getCellRowNum has been called");
		for(int i=startRow;i<=endRow;i++){
			//System.out.print("value of cell for column '" + colName +"' at row " + i + " is '" + getCellData(sheetName,colName, i)+"'");
			String actualValue = getCellData(sheetName, colName, i);
			if (ignoreWhiteSpace){
				actualValue = actualValue.replaceAll("\\s+","");
				cellValue = cellValue.replaceAll("\\s+", "");
			}//end if
	    	if(actualValue.equalsIgnoreCase(cellValue)){
	    		//System.out.println(", MATCH");
	    		return i;
	    	}//end if
	    	else if (colName.contains("Description") && actualValue.contains(cellValue)){
	    		//System.out.println("Description is being analyzed, value: "+cellValue);
	    		return i;
	    	}//end else if - Description
	    	//else System.out.println(", no match");
	    }//end for
		return -1;
	}//end method
	
	public int getCellRowNum(String sheetName, String colName, String cellValue, int startIndex){
		return getCellRowNum(sheetName, colName, cellValue, startIndex, false);
	}

	public int getCellRowNum(String sheetName, String colName, String cellValue, int startIndex, boolean ignoreWhiteSpace){
		/*
		System.out.println("method getCellRowNum has been called for sheet " +sheetName +", for column '" + colName 
				+ "', for value '" + cellValue +"', for starting row index " + startIndex);
				*/
		int lastRow = getRowCount(sheetName);
		//System.out.println("Sheet '" + sheetName + "' has " +lastRow +" rows");
		for(int i=startIndex;i<=lastRow;i++){
			//System.out.println("value of cell for column " + colName +" at row " + i + " is " + getCellData(sheetName,colName, i));
			String actualValue = getCellData(sheetName, colName, i);
			if (ignoreWhiteSpace){
				actualValue = actualValue.replaceAll("\\s+","");
				cellValue = cellValue.replaceAll("\\s", "");
			}
	    	if(actualValue.equalsIgnoreCase(cellValue)){
	    		return i;
	    	}//end if - equals
	    	else if (colName.contains("Description") && actualValue.contains(cellValue)){
	    		//System.out.println("Description is being analyzed, value: "+cellValue);
	    		return i;
	    	}//end else if - Description
	    }
		return -1;
		
	}

	public int getCellRowNum(String sheetName, int colNum, String cellValue, boolean ignoreWhiteSpace){
		return getCellRowNum(sheetName, colNum, cellValue, rowForColumnNames, getRowCount(sheetName), ignoreWhiteSpace);
	}

	public int getCellRowNum(String sheetName, int colNum, String cellValue){
		return getCellRowNum(sheetName, colNum, cellValue, rowForColumnNames, getRowCount(sheetName), false);
	}

	
	public int getCellRowNum(String sheetName,int colNum,String cellValue, int startRow, int endRow, boolean ignoreWhiteSpace){
		
		for(int i=startRow;i<=endRow;i++){
			String actualValue = getCellData(sheetName, colNum, i);
			if (ignoreWhiteSpace){
				actualValue = actualValue.replaceAll("\\s+","");
				cellValue = cellValue.replaceAll("\\s", "");
			}
	    	if(actualValue.equalsIgnoreCase(cellValue)){
	    		//System.out.println(getCellData(sheetName,colNum , i));
	    		return i;
	    	}
	    }
		return -1;
		
	}
	
	public int getCellRowNum(String sheetName, int colNum, String cellValue, int startIndex){
		return getCellRowNum(sheetName, colNum, cellValue, startIndex, false);
	}

	
	public int getCellRowNum(String sheetName, int colNum, String cellValue, int startIndex, boolean ignoreWhiteSpace){
		for(int i=startIndex;i<=getRowCount(sheetName);i++){
			String actualValue = getCellData(sheetName, colNum, i);
			if (ignoreWhiteSpace){
				actualValue = actualValue.replaceAll("\\s+","");
				cellValue = cellValue.replaceAll("\\s", "");
			}
	    	if(actualValue.equalsIgnoreCase(cellValue)){
	    		//System.out.println("value found for worksheet " + sheetName +" at row " +i +"is " + getCellData(sheetName,colNum , i));
	    		return i;
	    	}
	    }
		
		return -1;
	}
	
	public int getCellColumnNumber(String sheetName, String colName, 
			BufferedWriter logFileWriter, boolean writeToLogFile) throws Exception{
		return getCellColumnNumber(sheetName, colName, 0, logFileWriter, writeToLogFile);
	}//end getCellColumnNumber method
	
	public int getCellColumnNumber (String sheetName, String colName, int startIndex, 
			BufferedWriter logFileWriter, boolean writeToLogFile) throws Exception{
		return getCellColumnNumber(sheetName, colName, startIndex, this.getRowForColumnNames() + 1, 
				logFileWriter, writeToLogFile);
	}

	
	public int getCellColumnNumber(String sheetName, String colName, int startIndex, int rowNum, 
						BufferedWriter logFileWriter, boolean writeToLogFile) throws Exception{
		row=sheet.getRow(rowNum);
		if (writeToLogFile){
			logFileWriter.write("***getCellColumnNumber called for sheet " 
						+ sheetName +" for column " + colName +"***");
			logFileWriter.newLine();
			logFileWriter.write("There are " + row.getLastCellNum() + " cells in this row, "
					+" row number " + rowNum);
			logFileWriter.newLine();
		}//end if
		colName = colName.trim();
		for(int i=startIndex;i<row.getLastCellNum();i++){
			String actualValue = getCellData(sheetName, i, rowNum);
			if ((actualValue == null) || ( actualValue.isEmpty())) 
				actualValue = new String();
			else actualValue = actualValue.trim();
			if (writeToLogFile){
				logFileWriter.write("Actual value found is '" + actualValue+"'");
				logFileWriter.newLine();
			}		
	    	if(actualValue.equalsIgnoreCase(colName)){
	    		//System.out.println(getCellData(sheetName,colNum , i));
	    		logFileWriter.write("returning a column number value of " + i);
	    		logFileWriter.newLine();
	    		return i;
	    	}//end if
		}//end for loop
		//if by this point we don't have a valid column, return -1
		return -1;
	}//end getCellColumnNumber method

	public String getCellBackgroundColor(String sheetName, String colName, int rowNum, 
			BufferedWriter logFileWriter) throws Exception{
		int colNum = 0;		
		colNum = getCellColumnNumber (sheetName, colName, 0, logFileWriter, true);
		return getCellBackgroundColor(sheetName, colNum, rowNum);
	}
	
	public String getCellBackgroundColor(String sheetName, int colNum, int rowNum){
		String bgColor = new String();
		Cell cell = getCell(sheetName, colNum, rowNum);
		bgColor = Short.toString(cell.getCellStyle().getFillBackgroundColor());
		return bgColor;
	}//end getCellBackgroundColor method
	

	public String getCellForegroundColor(String sheetName, String colName, 
			BufferedWriter logFileWriter, int rowNum) throws Exception{
		int colNum = getCellColumnNumber (sheetName, colName, 0, logFileWriter, true);
		return getCellForegroundColor(sheetName, colNum, rowNum);
	}

	public String getCellForegroundColor(String sheetName, int colNum, int rowNum){
		Cell cell = getCell(sheetName, colNum, rowNum);
		Color color = cell.getCellStyle().getFillForegroundColorColor();
		XSSFColor xcolor = XSSFColor.toXSSFColor(color);
		return xcolor.getARGBHex();
	}
	
	private Cell getCell(String sheetName, int colNum, int rowNum){
		int index;
		try{
			if((rowNum <=0) || (colNum < 0))
				return null;
		
			index = workbook.getSheetIndex(sheetName);
			if(index==-1)
				return null;
			sheet = workbook.getSheetAt(index);
			row=sheet.getRow(rowForColumnNames);
			//System.out.println("Searching column names in worksheet " + sheetName + " for column " + colName);
		}//end try clause
		catch(Exception e){
			return null;
		}
		sheet = workbook.getSheetAt(index);
		row = sheet.getRow(rowNum-1);
		if(row==null){
			//System.out.println("row is null - returning a blank");
			return null;
		}
		return row.getCell(colNum);
	}
	
	// returns the data from a cell
	public String getCellData(String sheetName,String colName,int rowNum){
		//System.out.println("method getCellData has been called with worksheet " + sheetName + ", column name: " + colName + ", row number " + rowNum);
		try{
			if(rowNum <=0)
				return "";
		
		int index = workbook.getSheetIndex(sheetName);
		int col_Num=-1;
		if(index==-1)
			return "";
		String cellText = "";
		sheet = workbook.getSheetAt(index);
		row=sheet.getRow(rowForColumnNames);
		//System.out.println("Searching column names in worksheet " + sheetName + " for column " + colName);
		for(int i=0;i<row.getLastCellNum();i++){
			Cell cell = (Cell)row.getCell(i);
			if (cell == null){ 
				//System.out.println("Null cell");
				cellText = "";
			}
			else if(cell.getCellTypeEnum() == CellType.BLANK) cellText = "";
			else if(cell.getCellTypeEnum()==CellType.STRING) cellText = cell.getStringCellValue();
			else if(cell.getCellTypeEnum()==CellType.NUMERIC){
				//System.out.println("Numeric value is being processed");
				cellText  = new DataFormatter(java.util.Locale.US).formatCellValue(cell);
			}
			else if (cell.getCellTypeEnum()==CellType.FORMULA ){
				switch(cell.getCachedFormulaResultTypeEnum()){
				case NUMERIC: cellText = String.valueOf(cell.getNumericCellValue());
					break;
				case STRING:cellText = cell.getStringCellValue();
					break;
				case _NONE: cellText = "0";
					break;
				default:
					break;
				}//end case statement
			}//end else if - it's a formula
			//System.out.print("Column name being evaluated is " + cellText.trim());
			if(cellText.trim().equals(colName.trim())){
				//System.out.println(", MATCH - column number: " + i);
				col_Num=i;
				break;
			}//end if - we found the correct cell
			else; //System.out.println(", no match for column number " + i);
		}
		//System.out.println();
		//System.out.println("...for worksheet " + sheetName);
		if(col_Num==-1)
			return "";
		
		sheet = workbook.getSheetAt(index);
		row = sheet.getRow(rowNum-1);
		if(row==null){
			//System.out.println("row is null - returning a blank");
			return "";
		}
		cell = row.getCell(col_Num);
		
		if(cell==null){
			//System.out.println("cell is null - returning a blank");
			return "";
		}
		FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
		cell = (Cell)evaluator.evaluateInCell(cell);
		CellType cellType = cell.getCellTypeEnum();
		if(cellType==CellType.STRING){
			  //System.out.println("returning value " + cell.getStringCellValue());
			  return cell.getStringCellValue();
		}
		else if(cellType==CellType.NUMERIC){
			  
			  //String cellText  = String.valueOf(cell.getNumericCellValue());
			cellText = new DataFormatter(java.util.Locale.US).formatCellValue(cell);
			//System.out.println("cell text for worksheet " + sheetName + " is being returned as " + cellText);
			  if (DateUtil.isCellDateFormatted(cell)) {
		           
				  double d = cell.getNumericCellValue();

				  Calendar cal =Calendar.getInstance();
				  cal.setTime(DateUtil.getJavaDate(d));
		            cellText =
		             (String.valueOf(cal.get(Calendar.YEAR))).substring(2);
		           cellText = cal.get(Calendar.DAY_OF_MONTH) + "/" +
		                      cal.get(Calendar.MONTH)+1 + "/" + 
		                      cellText;
		           
		          

		         }

			  
			  
			  return cellText;
		  }
		else if (cellType==CellType.FORMULA ){
			switch(cell.getCachedFormulaResultTypeEnum()){
			case NUMERIC: cellText = String.valueOf(cell.getNumericCellValue());
				break;
			case STRING:cellText = cell.getStringCellValue();
				break;
			case _NONE: cellText = "0";
				break;
			default:
				break;
			}//end case statement
			return cellText;
		}//end else if - it's a formula
		else if(cellType==CellType.BLANK)
		      return ""; 
		  else 
			  return String.valueOf(cell.getBooleanCellValue());
		
		}
		catch(Exception e){
			
			e.printStackTrace();
			return "row "+rowNum+" or column "+colName +" does not exist in xls";
		}
	}
	
	public String getCellData(String sheetName,int colNum,int rowNum){
		return getCellData(sheetName,colNum, rowNum, false);
	}

	// returns the data from a cell
	public String getCellData(String sheetName, int colNum, int rowNum, boolean writeToOutput){
		try{
			if(rowNum <=0)
				return "";
		
		int index = workbook.getSheetIndex(sheetName);

		if(index==-1)
			return "";
		
	
		sheet = workbook.getSheetAt(index);
		row = sheet.getRow(rowNum-1);
		if(row==null)
			return "";
		cell = row.getCell(colNum);
		if(cell==null)
			return "";
		FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
		cell = (Cell)evaluator.evaluateInCell(cell);
		//CellValue cellValue = evaluator.evaluate(cell);
		CellType cellType = cell.getCellTypeEnum();

	  if(cellType==CellType.NUMERIC){
		  
		  //String cellText  = String.valueOf(cell.getNumericCellValue());
			String cellText  = new DataFormatter(java.util.Locale.US).formatCellValue(cell);
			if (writeToOutput)
				System.out.println("cell number text for worksheet " + sheetName + " is being returned as " + cellText);
		  if (DateUtil.isCellDateFormatted(cell)) {
	           // format in form of M/D/YY
			  double d = cell.getNumericCellValue();

			  Calendar cal =Calendar.getInstance();
			  cal.setTime(DateUtil.getJavaDate(d));
	            cellText =
	             (String.valueOf(cal.get(Calendar.YEAR))).substring(2);
	           cellText = cal.get(Calendar.MONTH)+1 + "/" +
	                      cal.get(Calendar.DAY_OF_MONTH) + "/" +
	                      cellText;
	         }//end if - cell is date-formatted
		  return cellText;
	  }
		else if (cellType==CellType.FORMULA ){
			String cellText = new String();
			switch(cell.getCachedFormulaResultTypeEnum()){
			case NUMERIC: cellText = String.valueOf(cell.getNumericCellValue());
				break;
			case STRING:cellText = cell.getStringCellValue();
				break;
			default:
				break;
			}//end case statement
			return cellText;
		}//end else if - it's a formula

	  else if(cellType==CellType.BLANK)
	      return "";
	  else if (cellType==CellType.BOOLEAN)
		  return String.valueOf(cell.getBooleanCellValue());
	  else{
		  String cellText = cell.getStringCellValue();
		  if (writeToOutput)
			  System.out.println("cell text for worksheet " + sheetName + " is being returned as " + cellText);
		  return cellText;
		  }//end if - it's a String

		}//end try clause
		catch(Exception e){
			
			e.printStackTrace();
			return "row "+rowNum+" or column "+colNum +" does not exist in the file";
		}
	}

	// returns true if sheet is created successfully else false
	public boolean addSheet(String sheetname){		
		
		//FileOutputStream fileOut;
		
		try {
			this.fileOut = new FileOutputStream(file);
			 workbook.createSheet(sheetname);	
			 //fileOut = new FileOutputStream(path);
			 workbook.write(fileOut);
			 System.out.println("Added new worksheet "+sheetname);
			 System.out.println("Workbook now has "+workbook.getNumberOfSheets()+" worksheets");
		     fileOut.close();		    
		} catch (Exception e) {			
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public int getNumberOfSheets() throws Exception{
		return workbook.getNumberOfSheets();
	}
	
	public String getColumnNameForIndex (String sheetName, int colNum){
		String columnName = new String();
		try{
			int index = workbook.getSheetIndex(sheetName);
			if(index==-1)
				return columnName;
			sheet = workbook.getSheetAt(index);
			row=sheet.getRow(rowForColumnNames);
			columnName = row.getCell(colNum).getStringCellValue();
		}//end try clause
		catch(Exception e){
			e.printStackTrace();
			return columnName;
		}
		return columnName;
		
	}

	// returns true if data is set successfully else false
	public boolean setCellData(String sheetName,String colName, int rowNum, String data){
		int index = 0;

		try {
			fileIn = new FileInputStream(path); 
			if (whichWorkbookType == HSSF)
				workbooks[whichWorkbookType] = new HSSFWorkbook(fileIn);
			else 
				workbooks[whichWorkbookType] = new XSSFWorkbook(fileIn);
			this.workbook = workbooks[whichWorkbookType];
			
			//File f = new File(path);
			//workbooks[whichWorkbookType] =  WorkbookFactory.create(f); 
			index = workbook.getSheetIndex(sheetName);
			if(index==-1)
				return false;
		}//end try
		catch(Exception e) {
			System.out.println("Could not get the workbook instantiated because of Exception "+e.getMessage());
		}//end catch

		Sheet sheet=workbook.getSheetAt(index);

		try{	
			if(rowNum<=0)
				return false;
			
			int colNum=-1;
			if(index==-1)
				return false;

			sheet = workbook.getSheetAt(index);	
			System.out.println("Got the sheet "+sheet.getSheetName());
			row=sheet.getRow(rowForColumnNames);
			try {
			System.out.println("Got the row with the column names: row "+row.getRowNum());
			}
			catch(Exception e) {
				System.out.println("Could not get the row with column names - row "+rowForColumnNames+" because of Exception "+e.getMessage());
			}
			int lastCellNum = 0;
			try {
				lastCellNum = row.getLastCellNum();
			}
			catch(Exception e) {
				System.out.println("Could not get the last cell in the row with column names because of Exception "+e.getMessage());
			}
			for(int i=0;i<lastCellNum;i++){
				System.out.println(row.getCell(i).getStringCellValue().trim());
				if(row.getCell(i).getStringCellValue().trim().equals(colName))
					colNum=i;
			}//end for
			System.out.println("Got the column number: "+colNum);
			if(colNum==-1)
				return false;
	
			sheet.autoSizeColumn(colNum); 
			row = sheet.getRow(rowNum-1);
			if (row == null)
				row = sheet.createRow(rowNum-1);
			System.out.println("row number being worked on is "+row.getRowNum());
			cell = row.getCell(colNum);	
			if (cell == null)
		        cell = row.createCell(colNum);
		    cell.setCellValue(data);
		    System.out.println("Cell created has row "+cell.getRowIndex()+" and column "+cell.getColumnIndex());
		    fileIn.close();
		    fileOut = new FileOutputStream(path);
			workbook.write(fileOut);
		    fileOut.close();	
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/*
	 * This returns all discrete values for a given column in a sheet.  The name of the column 
	 * and the sheet are both given as String input parameters.
	 */
	
	public String[] getAllDiscreteColumnValues (String sheetName, String colName, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** SSExcelReader method getAllDiscreteColumnValues called  with worksheet "+ sheetName + " and column " +colName +" ***");
		logFileWriter.newLine();
		TreeSet<String> discreteValues = new TreeSet<String>();
		int lastRow = getRowCount(sheetName);
		logFileWriter.write("Name values found: ");
		for (int rowNum=rowForColumnNames + 2; rowNum < lastRow; rowNum++){
			String value = getCellData(sheetName, colName, rowNum);
			logFileWriter.write(value +"; ");
			discreteValues.add(value);
		}//end for loop
		if (discreteValues.contains("")) discreteValues.remove("");
		if (discreteValues.contains(" ")) discreteValues.remove(" ");
		logFileWriter.newLine();
		logFileWriter.write("returning a TreeSet with size " + discreteValues.size());
		logFileWriter.newLine();
		return discreteValues.toArray(new String[discreteValues.size()]);
	}//end getAllDiscreteColumnValues method
	
	
	/*
	 * This returns a TreeSet - a discrete, ordered list of values - for one or more columns 
	 * (given as an array of input parameters), given another column name and a value String as input parameters.
	 * The value String given as an input parameter is a substring used to determine which 
	 * rows contain the desired data).  If the values in the second column name contain the
	 * value String as a substring, the values of the array of columns is added to the TreeSet.
	 * It iterates through the entire worksheet of the file under test until it has found
	 * all matches and then it returns the TreeSet.
	 */
	
	public TreeSet<String> getDiscreteOrderedListOfValues(String sheetName, String searchString, String searchColumn, String[] columnNames, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("method getDiscreteOrderedListOfValues has been called");
		logFileWriter.newLine();
		TreeSet<String> matches = new TreeSet<String>();
		/*
		 * iterate through the worksheet, finding all values for columnName that have
		 * a value for searchColumn that meet the search criteria.  The search criteria
		 * is that the value for SearchColumn must contain the searchString as a substring
		 * or match the value of the searchString.
		 */
		int foundRowIndex = getCellRowNumWithSubstringMatch(sheetName, searchColumn, searchString, rowForColumnNames);
		while (foundRowIndex != -1){
			String foundValue = getCellData(sheetName, columnNames[0], foundRowIndex);
			if (columnNames.length > 1){
				for (int i=1; i<columnNames.length; i++){
					foundValue = foundValue.concat("-"+getCellData(sheetName, columnNames[i], foundRowIndex));
				}//end for
			}//end if
			matches.add(foundValue);
			logFileWriter.write("Found value " + foundValue +" at row " + foundRowIndex);
			logFileWriter.newLine();
			//now, advance the initial row index to search from so that it equals the value of the index it found
			foundRowIndex = getCellRowNumWithSubstringMatch(sheetName, searchColumn, searchString, foundRowIndex + 1);
		}
		
		return matches;
	}
	
	/*
	 * The objective of this method is to return a discrete ordered list of String values for a 
	 * given set of search criteria.  
	 * The String values to be returned are the values found in a column, given as an input parameter.
	 * The search criteria are given as a HashMap of name-value pairs.
	 * 
	 * Please note the limitations of this - it will only return discrete values from one column.
	 * It can have multiple search criteria.  Most of the time, this will be used to derive a list
	 * of discrete Faculty names, given search criteria as input.
	 * 
	 * It works by calling and passing the parameters to the getMultipleFacultyValues method and then 
	 * exporting the contents of the HashMap it returns into a TreeSet.
	 */
	public TreeSet<String> getDiscreteOrderedListOfValues(String sheetName, HashMap<String, String> keysAndValues, String value, BufferedWriter logFileWriter) throws Exception{
		TreeSet<String> discreteValues = new TreeSet<String>();
		HashMap<Integer, String> nonDiscreteValues = getMultipleFacultyValues(sheetName, keysAndValues, value, logFileWriter);
		logFileWriter.newLine();
		//iterate through the HashMap, getting the String values and putting them into the TreeSet
		for (Integer key: nonDiscreteValues.keySet()) {
			discreteValues.add(nonDiscreteValues.get(key));
			logFileWriter.write(nonDiscreteValues.get(key) + ", ");
		}//end for loop
		logFileWriter.newLine();
		logFileWriter.write("method getDiscreteOrderedListOfValues is returning a TreeSet of size " + discreteValues.size());
		logFileWriter.newLine();
		return discreteValues;
	}

	public int getCellRowNumWithSubstringMatch(String sheetName, String colName, String cellValue, int startIndex){
		for(int i=startIndex;i<=getRowCount(sheetName);i++){
	    	if(getCellData(sheetName,colName , i).contains(cellValue)){
	    		return i;
	    	}//end if
		}//end for
		return -1;
	}//end method
	
	public int getCellRowNumWithSubstringMatch(String sheetName, int colNum, String cellValue, int startIndex){
		for(int i=startIndex;i<=getRowCount(sheetName);i++){
	    	if(getCellData(sheetName,colNum , i).contains(cellValue)){
	    		return i;
	    	}//end if
		}//end for
		return -1;
	}//end method
	
	/*
	 * Sometimes, we only want to get the information on a single row of data in a file.  This 
	 * method assumes that the keysAndValues HashMap contains sufficiently stringent criteria
	 * that a single row within the file can be located.  
	 * If the criteria is not sufficiently stringent, and there are multiple rows in the file,
	 * then only the first row found will be returned.
	 */
	public String[] getSingleFacultyMultipleValues(String sheetName, HashMap<String, String> keysAndValues, 
			String[] columnNames, BufferedWriter logFileWriter, int startRow, int endRow) throws Exception{
		logFileWriter.write("*** method getSingleFacultyMultipleValues has been called ***");
		logFileWriter.newLine();
		String[][] multiArray = getMultipleFacultyMultipleValues(sheetName, keysAndValues, columnNames, logFileWriter, startRow, endRow);
		if (multiArray != null){
			if (multiArray.length == 1) return multiArray[0];
			else if (multiArray.length == 0) return null;
			else {//multiArray.length > 1
				logFileWriter.write("Multiple Values Found - returning INVALID");
				logFileWriter.newLine();
				multiArray[0][0] = "INVALID";
				return multiArray[0];
			}
		}//end if
		else return null;
	}//end getSingleFacultyMultipleValues method
	
	public String[] getSingleFacultyMultipleValues(String sheetName, HashMap<String, String> keysAndValues, 
			String[] columnNames, BufferedWriter logFileWriter) throws Exception{
		return getSingleFacultyMultipleValues(sheetName, keysAndValues, columnNames, 
				logFileWriter, getRowForColumnNames(), getRowCount(sheetName));
	}//end method

	public String[] getSingleFacultyMultipleValues(String sheetName, HashMap<String, String> keysAndValues, 
			String[] columnNames, BufferedWriter logFileWriter, String startString, String endString, int colNum) throws Exception{
		int startRow = getCellRowNum(sheetName, colNum, startString);
		int endRow = getCellRowNum(sheetName, colNum, endString);
		return getSingleFacultyMultipleValues(sheetName, keysAndValues, columnNames, 
				logFileWriter, startRow, endRow);
	}//end method

	/*
	 * The objective of this method is to return a two-dimensional String array of multiple 
	 * columns' values for multiple rows, given the input, which is a HashMap of column names 
	 * and values which serve as criteria for selecting the faculty to be returned, as well
	 * as a String array of column names whose values are to be returned. 
	 * The values returned will be not be a distinct list of values.  
	 * It simply calls the getMultipleFacultyValues method with the first column name as input,
	 * then, from the HashMap that is returned, it derives the row number from the Integer value
	 * in each item in the HashMap, and then iterates through the column names, fetching the values
	 * for each column for that row.  
	 *  
	 */
	public String[][] getMultipleFacultyMultipleValues(String sheetName, HashMap<String, String> keysAndValues, 
				String[] columnNames, BufferedWriter logFileWriter, int startRow, int endRow) throws Exception{
		HashMap<Integer, String> rowsAndValue = getMultipleFacultyValues (sheetName, keysAndValues, columnNames[0], logFileWriter, startRow, endRow);
		String[][] values = new String[rowsAndValue.size()][columnNames.length];
		int rowIndex = 0;
		for (Integer key: rowsAndValue.keySet()){
			int rowNum = key.intValue();
			for(int columnIndex=0; columnIndex < columnNames.length; columnIndex++){
				values[rowIndex][columnIndex] = getCellData(sheetName, columnNames[columnIndex], rowNum);
			}//end inner for - iterating through the columns
			rowIndex++;
		}//end outer for - iterating through the rows
		return values;
	}
	
	public String[][] getMultipleFacultyMultipleValues(String sheetName, HashMap<String, String> keysAndValues, 
			String[] columnNames, BufferedWriter logFileWriter, String startString, String endString, int colNum) throws Exception{
		int startRow = getCellRowNum(sheetName, colNum, startString);
		int endRow = getCellRowNum(sheetName, colNum, endString);
		return getMultipleFacultyMultipleValues(sheetName, keysAndValues, columnNames, logFileWriter, startRow, endRow);
	}

	public String[][] getMultipleFacultyMultipleValues(String sheetName, HashMap<String, String> keysAndValues, 
			String[] columnNames, BufferedWriter logFileWriter) throws Exception{
		return getMultipleFacultyMultipleValues(sheetName, keysAndValues, columnNames, logFileWriter, rowForColumnNames, getRowCount(sheetName));
	}


	/*
	 * The objective of this method is to return a list of faculty values from a spreadsheet,
	 * given the input, which is a HashMap of column names and values which serve as criteria
	 * for selecting the faculty to be returned.  The values returned will be not be a distinct
	 * list of values.  The HashMap returned will be a list of key-value pairs.  The key will
	 * be the row number on which the faculty value was found and the value will be value of 
	 * the faculty member found.
	 * 
	 * This works using a simple algorithm.  It iterates through the contents of the worksheet
	 * given as input.  Once it locates a row containing the first searched-for
	 * value in the first searched-on column, it then iterates through a "for" loop, for that
	 * row, and it breaks if and when one of the searched-for values in that row differs from 
	 * the value actually found for one of the searched-on columns given as input.
	 * 
	 * Of note, it also takes two int values - "startRow" and "endRow" which specify the
	 * rows in the spreadsheet within which the search is permitted to happen.
	 * 
	 */
	public HashMap<Integer, String> getMultipleFacultyValues(String sheetName, HashMap<String, String> keysAndValues, 
			String value, BufferedWriter logFileWriter, int startRow, int endRow) throws Exception{
		HashMap<Integer, String> faculty = new HashMap<Integer, String>();
		logFileWriter.newLine();
		logFileWriter.write("*** method getMultipleFacultyValues has been called ***");
		logFileWriter.newLine();
		
		/*
		 * first, retrieve the first key and first value, 
		 * which are the first searched-on column and first searched-for value
		 */
		//logFileWriter.write("start row is " + startRow +", and end row is "+endRow);
		//logFileWriter.newLine();
		String firstKey = (String)keysAndValues.keySet().toArray()[0];
	    if ( (firstKey.contains("Description")) 
	    		|| (firstKey.contains("Notes")) 
	    		|| (firstKey.equalsIgnoreCase("Department"))){
	    	//logFileWriter.write("First key examined is " + firstKey + ", and it is invalid; ");
	    	boolean isValidKey = false;
	    	int numberOfKeys = keysAndValues.keySet().toArray().length;
	    	for(int i=0; (i<numberOfKeys) && (! isValidKey); i++){
	    		firstKey = (String)keysAndValues.keySet().toArray()[i];
	    	    if ( (! firstKey.contains("Description")) 
	    	    		&& (! firstKey.contains("Notes")) 
	    	    		&& (! firstKey.equalsIgnoreCase("Department")) ){
	    	    	isValidKey = true;
	    	    }//end if - it's neither Description nor Notes, so it's valid
	    	}//end for - iterating through the list of keys
	    }//end if - what to do if the firstKey is either Description or Notes
		//logFileWriter.write("First key examined: " + firstKey);
		String firstValue = (String)keysAndValues.get(firstKey);
		//logFileWriter.write(", First value examined: " + firstValue);
		//logFileWriter.newLine();
		

		int foundRowIndex = getCellRowNum(sheetName, firstKey, firstValue, startRow, endRow);
		/*
		 * outer loop iterates through the sheet, 
		 * finding any and all rows which match the first searched-on column 
		 * and first searched-for value.
		 */
		while (foundRowIndex != -1){
			String foundValue = getCellData(sheetName, value, foundRowIndex);
			/*
			 * Once a match is found, iterate through the rest of the searched-on column names 
			 * and searched-for column values.
			 */
			boolean match = true;
			//logFileWriter.write("... and match for first value is found at row " + foundRowIndex);
			//logFileWriter.newLine();
			for (Object key: keysAndValues.keySet()) {
			    //logFileWriter.write("column name : " + key.toString());
			    //logFileWriter.write(", column value : " + keysAndValues.get(key).toString());
			    //logFileWriter.newLine();
			    String derivedValue = keysAndValues.get(key).toString();
			    String actualCellData = getCellData(sheetName, key.toString(), foundRowIndex);
			    if ( key.toString().contains("Description")
			    		|| key.toString().contains("Notes") 
			    		|| key.toString().equalsIgnoreCase("Department") ){
			    	//logFileWriter.write("column only needs to contain the value");
			    	//logFileWriter.newLine();
			    	if (actualCellData.contains(derivedValue)){
			    		match = true;
			    	}//end if
			    	else{
			    		match = false;
			    	}
			    }//end inner if - handling a substring
			    else{
				    if (actualCellData.equalsIgnoreCase(derivedValue)){
				    	match = true;
				    } //end if
				    else{
				    	match = false;
				    }//end inner else
			    }//end outer else - it's a string-to-string match
			    if (match){
			    	//logFileWriter.write(": Match");
			    	//logFileWriter.newLine();
			    }//end if - handling a match
			    else {
			    	//logFileWriter.write(": MISMATCH - actual value found is " + actualCellData);
			    	//logFileWriter.newLine();
			    	break;
			    }//end else - handling a mismatch
			}//end for loop
			
			// if this row matches all searched-for values for all searched-on columns, add it in
			if (match) {
				faculty.put(foundRowIndex, foundValue);
				//logFileWriter.write("Found value " + foundValue +" at row " + foundRowIndex);
				//logFileWriter.newLine();
			}//end if
			
			//now, advance the initial row index to search from so that it equals the value of the index it found
			foundRowIndex = getCellRowNum(sheetName, firstKey, firstValue, foundRowIndex + 1, endRow);
		}// end outer "while" loop
		return faculty;
	}
	
	public HashMap<Integer, String> getMultipleFacultyValues(String sheetName, HashMap<String, String> keysAndValues, 
			String value, BufferedWriter logFileWriter) throws Exception{
		return getMultipleFacultyValues(sheetName, keysAndValues, value, logFileWriter, rowForColumnNames, getRowCount(sheetName));
	}//end method
	
	
	/*
	 * This is intended for use with spreadsheets where the search is limited to a
	 * given area in the spreadsheet, but we're not sure what the delimiting row 
	 * numbers are - we just know a value of a String that is found on the start row
	 * and a String that is found on the end row and the column number that both
	 * the start and end row delimiter strings share.
	 */
	public HashMap<Integer, String> getMultipleFacultyValues(String sheetName, HashMap<String, String> keysAndValues, 
			String value, BufferedWriter logFileWriter, String startString, String endString, int colNum) throws Exception{
		logFileWriter.write("*** method getMultipleFacultyValues has been called with delimiters "
				+startString + " and " +endString + " at column number " + colNum + " ***");
		int startRow = getCellRowNum(sheetName, colNum, startString);
		int endRow = getCellRowNum(sheetName, colNum, endString);
		return getMultipleFacultyValues(sheetName, keysAndValues, value, logFileWriter, startRow, endRow);
	}//end method


	
	/*
	 * The objective of this method is to return one or more sums of numbers in column names given as 
	 * input. A HashMap of column names and values is also given as input.  Also,
	 * maximum and minimum dates are given to specify the date range for the data to be returned.
	 * The HashMap lists the criteria for numbers to be included in the sum(s) to be returned.
	 * These are criteria which must all be satisfied to be included in the summary information to 
	 * be returned.  
	 * There are also two date values given as input - one is the latest date, and the other is the
	 * earliest date.
	 * The numbers to be returned are returned as a sum of rows in which the conditions are satisfied
	 * and one or more sums of the columns the names of which are given as input.  They will be returned
	 * as an array of BigDecimal values, the size of which is equal to the size of the array (of column names
	 * given as input) plus one.  The sum is always the first number in the array of BigDecimal values
	 * returned.
	 */
	
	public BigDecimal[] getColumnSummaryValues(String sheetName, HashMap<String, String> criteria, 
			boolean emptyDatesExcluded, boolean outsideDatesIncrementFacultyCount, boolean outsideDatesIncrementSalarySum, 
			String[] columnNames, Date minDate, Date maxDate, BufferedWriter logFileWriter) throws Exception{
		//first, record what method is being called
		//System.out.println("method getColumnSummaryValues has been called");
		logFileWriter.write("\nmethod getColumnSummaryValues has been called\n");
		logFileWriter.newLine();
		boolean willIncrementFacultyCount = false;
		boolean willIncrementSalarySum = false;
		/*
		if (emptyDatesExcluded){
			logFileWriter.write("Rows with empty dates for Effective Dates will NOT increment the number of faculty and the sum of DO share salary increases");
			logFileWriter.newLine();
		}
		else {
			logFileWriter.write("Rows with empty dates for Effective Dates will STILL increment the number of faculty and the sum of DO share salary increases");
			logFileWriter.newLine();
		}
		if (outsideDatesIncrementFacultyCount){
			logFileWriter.write("Rows with dates outside the accepted range will still increment the number of faculty");
			logFileWriter.newLine();
		}
		else{
			logFileWriter.write("Rows with dates outside the accepted range will NOT increment the number of faculty");
			logFileWriter.newLine();
		}
		if (outsideDatesIncrementSalarySum){
			logFileWriter.write("Rows with dates outside the accepted range will still increment the sum of DO share salary increases");
			logFileWriter.newLine();
		}
		else{
			logFileWriter.write("Rows with dates outside the accepted range will NOT increment the sum of DO share salary increases");
			logFileWriter.newLine();
		}
		*/
		//instantiate the "summary" array of BigDecimal values
		BigDecimal[] summary = new BigDecimal[columnNames.length + 1];
		//first, initialize the "summary" array of BigDecimal values
		for (int i=0; i<summary.length; i++) summary[i] = new BigDecimal(0);
		//System.out.println("Number of values to be returned is " + summary.length);
		//logFileWriter.write("Number of values to be returned is " + summary.length);
		//logFileWriter.newLine();
		TreeSet<String> names = new TreeSet<String>();
		
		/*
		 * Read through the entire worksheet, and find rows which satisfy the input criteria, including the max and min Dates.  
		 * When we find one row that satisfies the first input criterion, iterate through the rest of the input criteria and verify
		 * that the rest of the input criteria are satisfied for that row.  If the input criteria are satisfied for 
		 * that row, then increment the count of rows found by one, and then add the value(s) found for that row
		 * for the column name(s) into the summary value(s) being returned.
		 */
		
		//iterate through the worksheet, finding the first row that satisfies the first input criterion
		//start with the first name-value pair in the HashMap - look for a row that matches it
		//rowIndex will store the row number we're in so we don't try matching the same row twice
		//logFileWriter.write("Trying to find the first row that satisfies the first input criterion");
		//logFileWriter.newLine();
		String name = (String)criteria.keySet().toArray()[0];
		String value = (String)criteria.get(name);
		//write the first column and value to the output file and indicate that a match is found
		//System.out.print("criterion column name : " + name);
		//logFileWriter.write("criterion column name : " + name);
		//System.out.print(", criterion column value : " + value);
		//logFileWriter.write(", criterion column value : " + value);
		int rowIndex = getCellRowNum(sheetName, name, value, rowForColumnNames);
		//System.out.print(", found at row number " + rowIndex);
		//logFileWriter.write(", found at row number " + rowIndex); 
		//when we find the first row that satisfies the first input criterion, iterate through the rest of the criteria, see if it still fits
		//write the first column and value to the output file and indicate that a match is found
		
		int numberOfCriteria = criteria.size();
		//look for rows that match the searchCriteria
		//while we still haven't gone through every row in the worksheet
		//logFileWriter.write("Now that we've found the initial row with the first criterion, we look through the rest of the rows");
		//logFileWriter.newLine();
		while( rowIndex != -1){
			
			String foundValue = getCellData(sheetName, name, rowIndex);
			boolean match = true;//reset this every time we examine another row of data
			//next, when a match is found, loop (inner loop) through the rest of the name-value pairs
			for (int criterionIndex = 1; criterionIndex < numberOfCriteria && match; criterionIndex++){
				//send information to the output file - it matches the criteria we've examined so far
				//System.out.print("Data found: " + foundValue);
				//logFileWriter.write("Data found: " + foundValue);
				//System.out.println(", Criteria match");
				//logFileWriter.write(", Criteria match");
				//logFileWriter.newLine();
		
				//if it satisfies all the Criteria, verify that it falls within the correct date range (between max and min dates)
				name = (String)criteria.keySet().toArray()[criterionIndex];
				value = (String)criteria.get(name);
				//System.out.print("criterion column name : " + name);
				//logFileWriter.write("criterion column name : " + name);
				//System.out.print(", criterion column value : " + value);
				//logFileWriter.write(", criterion column value : " + value);
				//retrieve the cell value in the next column in the same row
				foundValue = getCellData(sheetName, name, rowIndex);
				//compare the actual value found to the expected value
				if(! foundValue.equalsIgnoreCase(value)) match = false;
				//if match = false, write it to the output file
				if (! match){
					//System.out.println(", MISMATCH - going on to find the next match");
					//logFileWriter.write(", MISMATCH - going on to find the next match");
					//logFileWriter.newLine();
					break;//get out of the "for" loop
				}// end if
				else{//there's a match - check that it falls within the date range and satisfies Criteria, if specified
					//check the date range first ... that's always required
					//logFileWriter.newLine();
					//logFileWriter.write("Criterion match at row " +rowIndex);
					//logFileWriter.newLine();
					//System.out.print("Checking Date Range ... earliest date is " + minDate.toString());
					//logFileWriter.write("Checking Date Range ... earliest date is " + minDate.toString());
					//System.out.print(", latest date is " + maxDate.toString());
					//logFileWriter.write(", latest date is " + maxDate.toString());
					//System.out.print(", and the date found is: ");
					//logFileWriter.write(", and the date found is: ");
					Date effectiveDate = null;
					try{
						DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
						String effectiveDateString = getCellData(sheetName, "Effective Date", rowIndex);
						//logFileWriter.write("'" + effectiveDateString +"', ");
						if ((! effectiveDateString.isEmpty()) && (! effectiveDateString.equalsIgnoreCase(" "))){//it's not blank in the Control Sheet Detail Report
							effectiveDate = formatter.parse(effectiveDateString);
						} //end if - "Effective Date" column is not blank in this row - try to parse the date
						else {
							//logFileWriter.write("a blank value");//leave effectiveDate as null - this is an ASC
							//logFileWriter.newLine();
						}//end else - the "Effective Date" column is blank in that row
					}//end try clause - trying to get the Effective Date
					catch(Exception e){
						//System.out.println("not a determined date.  Exception thrown:");
						//logFileWriter.write("not a determined date.  Exception thrown: ");
						//System.out.println(e.toString());
						//logFileWriter.write(e.toString());
						//logFileWriter.newLine();
						match = false;
					}//end catch clause
					if (effectiveDate == null){
						if (emptyDatesExcluded){
							match = false;
							//logFileWriter.newLine();
							//logFileWriter.write("This row (Row " + rowIndex +") represents an ASC - it is not counted in this case");
							//logFileWriter.newLine();
						}//end if - empty date fields exclude this row
						else{ //with every case except for Market Retention, an ASC is counted
							//logFileWriter.newLine();
							//logFileWriter.write("This row (Row " + rowIndex +") represents an ASC - it might still be included in this case");
							//logFileWriter.newLine();
							willIncrementSalarySum = true;
							willIncrementFacultyCount = true;
						}//end else - an empty Date field won't exclude this row
					}//end if - the "Effective Date" field in this row is null.
					else if ((effectiveDate.after(minDate) || effectiveDate.equals(minDate)) && (effectiveDate.before(maxDate) || effectiveDate.equals(maxDate))){
						//System.out.print(effectiveDate.toString());
						//logFileWriter.write(effectiveDate.toString());
						//System.out.println(", and the date matches");
						//logFileWriter.write(", and the date falls into the required range");
						willIncrementSalarySum = true;
						willIncrementFacultyCount = true;
						//logFileWriter.newLine();
					}//end else if - the "Effective Date" field is meaningful and falls into the required range
					else{
						//match = false;//if the date doesn't fall in the right range, it's not a match
						//logFileWriter.write(", and the date doesn't fall in the required range");
						//logFileWriter.newLine();
						// ... but it might still increment the faculty count and DO share salary increase sum
						if (outsideDatesIncrementFacultyCount){
							//logFileWriter.write("However, the number of faculty is still incremented by row " + rowIndex);
							//logFileWriter.newLine();
							willIncrementFacultyCount = true;
							/*
							names.add(getCellData(sheetName, "Name", rowIndex));
							summary[0] = new BigDecimal(names.size());
							*/
							//logFileWriter.write("Incrementing count of faculty found to " + summary[0].toPlainString());
							//logFileWriter.newLine();
						}
						else{
							//logFileWriter.write("The number of faculty is NOT incremented by row " + rowIndex);
							//logFileWriter.newLine();
						}
						if(outsideDatesIncrementSalarySum){
							//logFileWriter.write("However, the DO share salary increase sum is still incremented by row " + rowIndex);
							willIncrementSalarySum = true;
							//logFileWriter.newLine();
							/*
							for (int columnIndex = 0; columnIndex<columnNames.length; columnIndex++){
								long addString = ((Long)NumberFormat.getNumberInstance(java.util.Locale.US).parse(getCellData(sheetName, columnNames[columnIndex], rowIndex))).longValue();
								BigDecimal toBeAdded = new BigDecimal(addString);
								summary[columnIndex+1] = summary[columnIndex+1].add(toBeAdded);
								//System.out.print(", and adding " + toBeAdded.toPlainString() + " to sum of " +summary[columnIndex+1].toPlainString());
								logFileWriter.write(", and adding " + toBeAdded.toPlainString() + " to sum of " +summary[columnIndex+1].toPlainString());
							}//end for loop - 
							logFileWriter.newLine();
							*/
						}
						else{
							
							//logFileWriter.write("The DO share salary increase sum is NOT incremented by row " + rowIndex);
							//logFileWriter.newLine();
						}
					}
				}
			}//end for loop - going through the criteria for this one row

			if (match){
				//logFileWriter.newLine();
				//logFileWriter.write("Match found at row " + rowIndex);
				//logFileWriter.newLine();
				if (willIncrementFacultyCount){
					names.add(getCellData(sheetName, "Name", rowIndex));
					summary[0] = new BigDecimal(names.size());
					//System.out.print("incrementing count of faculty found to " + summary[0].toPlainString());
					//logFileWriter.write("Incrementing count of faculty found to " + summary[0].toPlainString());
					//logFileWriter.newLine();
				}//end if - Faculty count gets incremented
				for (int columnIndex = 0; columnIndex<columnNames.length && willIncrementSalarySum; columnIndex++){
					long addString = ((Long)NumberFormat.getNumberInstance(java.util.Locale.US).parse(getCellData(sheetName, columnNames[columnIndex], rowIndex))).longValue();
					BigDecimal toBeAdded = new BigDecimal(addString);
					summary[columnIndex+1] = summary[columnIndex+1].add(toBeAdded);
					//System.out.print(", and adding " + toBeAdded.toPlainString() + " to sum of " +summary[columnIndex+1].toPlainString());
					//logFileWriter.write(", and adding " + toBeAdded.toPlainString() + " to sum of " +summary[columnIndex+1].toPlainString());
				}
				//logFileWriter.newLine();
			}

			//now that we know we've had a matched row, reset the search criterion
			name = (String)criteria.keySet().toArray()[0];
			value = (String)criteria.get(name);
			willIncrementFacultyCount = false;
			willIncrementSalarySum = false;
			
			//increment the rowIndex and search for the next match on the first criterion
			rowIndex = getCellRowNum(sheetName, name, value, ++rowIndex);
			if (rowIndex == -1){
				//System.out.println("The worksheet " + sheetName + " has been searched.  There are no more matches.");
				//logFileWriter.write("The worksheet " + sheetName + " has been searched.  There are no more matches.");
				//logFileWriter.newLine();
			}//end if - the output file has been updated with the information

		}//end while loop - going through all of the rows in the worksheet
		return summary;
	}

	/*
	 * 
	 * Given a worksheet name, a column name, and a value for that column, find the value for 
	 * another (given) column on the same row that the given value is found.
	 * 
	 */
	public String getValueForColumnNameAndValue(String sheetName, String searchString, String searchColumn, 
			String columnName, BufferedWriter logWriter, boolean writeToOutputFile) throws Exception{
		if (writeToOutputFile){
			logWriter.newLine();
			logWriter.write("ExcelReader.getValueForColumnNameAndValue method called");
			logWriter.newLine();
		}
		String value = "";
		int foundRowIndex = getCellRowNum(sheetName, searchColumn, searchString, rowForColumnNames);
		if (writeToOutputFile){
			logWriter.write("value found at row:" + foundRowIndex + "; ");
		}
		if (foundRowIndex == -1) return value;
		else value = getCellData(sheetName, columnName, foundRowIndex);
		if (writeToOutputFile){
			logWriter.write("found value: " + value);
			logWriter.newLine();
		}
		return value;
	}
	
	public String getValueForColumnNameAndValue(String sheetName, String searchString, String searchColumn, 
			String columnName, BufferedWriter logWriter) throws Exception{
		return getValueForColumnNameAndValue(sheetName, searchString, searchColumn, columnName, logWriter, true);
	}

	// returns true if sheet is removed successfully else false if sheet does not exist
	public boolean removeSheet(String sheetName){		
		int index = workbook.getSheetIndex(sheetName);
		if(index==-1)
			return false;
		
		//FileOutputStream fileOut;
		try {
			workbook.removeSheetAt(index);
			//fileOut = new FileOutputStream(path);
			workbook.write(fileOut);
		    //fileOut.close();		    
		} catch (Exception e) {			
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean addMultipleColumns(String sheetName, String[] colNames) {
		boolean colsAdded = false;
		int index = 0;

		try {
			this.workbook = getWorkbook(this.path);
			//File f = new File(path);
			//workbooks[whichWorkbookType] =  WorkbookFactory.create(f); 
			index = workbook.getSheetIndex(sheetName);
			if(index==-1)
				return false;
		}
		catch(Exception e) {
			System.out.println("Could not get the workbook instantiated because of Exception "+e.getMessage());
		}
		Sheet sheet=workbook.getSheetAt(index);
		
		row = sheet.getRow(rowForColumnNames);
		if (row == null)
			row = sheet.createRow(0);
		int initialColumnIndex = 0;
		if(row.getLastCellNum() == -1) //do this if we don't find any columns yet....
			System.out.println("Last cell in the row is not found - starting from the first cell");
		else {//do this if we do find columns.
			initialColumnIndex = (int)row.getLastCellNum();
			System.out.println("Last cell in the row is "+initialColumnIndex);
		}//end else
		for (int i=initialColumnIndex; i<colNames.length+initialColumnIndex; i++) {
			
			CellStyle  style = workbook.createCellStyle();
			style.setFillForegroundColor(HSSFColor.HSSFColorPredefined.GREY_40_PERCENT.getIndex());
			style.setFillPattern(FillPatternType.NO_FILL);
			
			cell = row.createCell(i);
	        cell.setCellValue(colNames[i]);
	        cell.setCellStyle(style);
	        System.out.println("Created cell at column index "+i+" with label "+colNames[i]);
		}//end for loop
		try {
	        this.fileOut = new FileOutputStream(this.path);
			workbook.write(fileOut);
		    fileOut.close();		    
		    colsAdded = true;
		}
		catch(IOException e) {
			System.out.println("Could not write to the file because of IOException "+e.getMessage());
			
		}

		return colsAdded;
	}//end addColumns method
	
	
	// returns true if column is created successfully
	public boolean addColumn(String sheetName,String colName){
		try {
			this.workbook = getWorkbook(this.path);
			//File f = new File(path);
			//workbooks[whichWorkbookType] =  WorkbookFactory.create(f); 
			int index = workbook.getSheetIndex(sheetName);
			if(index==-1)
				return false;
			
		CellStyle  style = workbook.createCellStyle();
		style.setFillForegroundColor(HSSFColor.HSSFColorPredefined.GREY_40_PERCENT.getIndex());
		style.setFillPattern(FillPatternType.NO_FILL);
		
		Sheet sheet=workbook.getSheetAt(index);
		
		row = sheet.getRow(rowForColumnNames);
		if (row == null)
			row = sheet.createRow(rowForColumnNames);
		
		//For some reason, it only engages this "if" and not the "else", even if new columns are created.  
		//Determine what it needs in order to write to the cell, and have that written to the Excel file
		if(row.getLastCellNum() == -1) {//do this if we don't find any columns yet....
			System.out.println("Last cell in the row is not found - starting from the first cell");
			cell = row.createCell(0);
		}//end if
		else {//do this if we do find columns.
			int lastCellNumber = (int)row.getLastCellNum();
			System.out.println("Last cell in the row is "+lastCellNumber);
			cell = row.createCell(lastCellNumber);
		}//end else
	        cell.setCellValue(colName);
	        cell.setCellStyle(style);
	        
	        this.fileOut = new FileOutputStream(this.path);
			workbook.write(fileOut);
		    fileOut.close();		    

		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
		return true;
		
		
	}
	
	// removes a column and all the contents
	public boolean removeColumn(String sheetName, int colNum) {
		try{
		if(!isSheetExist(sheetName))
			return false;
		//File f = new File(path);
		//workbook =  WorkbookFactory.create(f); 
		sheet=workbook.getSheet(sheetName);
		CellStyle style = workbook.createCellStyle();
		style.setFillForegroundColor(HSSFColor.HSSFColorPredefined.GREY_40_PERCENT.getIndex());
		style.setFillPattern(FillPatternType.NO_FILL);
		
	    
	
		for(int i =rowForColumnNames;i<getRowCount(sheetName);i++){
			row=sheet.getRow(i);	
			if(row!=null){
				cell=row.getCell(colNum);
				if(cell!=null){
					cell.setCellStyle(style);
					row.removeCell(cell);
				}
			}
		}
		//fileOut = new FileOutputStream(path);
		workbook.write(fileOut);
	    //fileOut.close();
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
		
	}
	

}
