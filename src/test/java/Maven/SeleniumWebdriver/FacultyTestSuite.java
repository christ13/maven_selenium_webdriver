package Maven.SeleniumWebdriver;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	NewFacultyTest.class
})


public class FacultyTestSuite {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
	}

}
