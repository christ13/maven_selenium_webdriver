package Maven.SeleniumWebdriver;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

import org.apache.commons.io.LineIterator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FileComparisonMaker {
	
	private FileReader beforeFile;
	private FileReader afterFile;
	private SSExcelReader inputs;
	private HashMap<String, String[]> beforeData;
	private HashMap<String, String[]> afterData;
	protected static int columnCount = 0;
	
	protected BufferedWriter logFileWriter;
	protected static StringBuffer verificationErrors;
	protected static String testName = "File Equivalence Verification Test";	
	protected static String testDescription = "Test of Equivalent Contents for two discrete files";
	private static String myWorksheetName = "File Comparison";
	private static String testEnv = "TEST";
	protected static String testCaseNumber = "00";
	
	protected static final String BEFORE = "Before";
	protected static final String AFTER = "After";
	
	@Before
	public void setUp() throws Exception {
		String fileName = "C:/Users/christ13/Documents/HandSon/Input Data.xls";
		inputs = new SSExcelReader(fileName, 0);
		
		int rowNum = inputs.getCellRowNum(myWorksheetName, "Key", "Directory");
		String directory = inputs.getCellData(myWorksheetName, "Value", rowNum);
		
		rowNum = inputs.getCellRowNum(myWorksheetName, "Key", "BeforeFileName");
		fileName = inputs.getCellData(myWorksheetName, "Value", rowNum);
		beforeFile = new FileReader(directory +fileName);
		/*
		rowNum = inputs.getCellRowNum(myWorksheetName, "Key", "BeforeFileWorksheetName");
		beforeWorksheetName = inputs.getCellData(myWorksheetName, "Value", rowNum);
		*/
		rowNum = inputs.getCellRowNum(myWorksheetName, "Key", "AfterFileName");
		fileName = inputs.getCellData(myWorksheetName, "Value", rowNum);	
		afterFile = new FileReader(directory + fileName);
		/*
		rowNum = inputs.getCellRowNum(myWorksheetName, "Key", "AfterFileWorksheetName");
		afterWorksheetName = inputs.getCellData(myWorksheetName, "Value", rowNum);
		*/
		
		beforeData = new HashMap<String, String[]>();
		afterData = new HashMap<String, String[]>();
	}//end setUp() method

	@Test
	public void test() throws Exception{
		verificationErrors = new StringBuffer();
		String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		logFileWriter = TestFileController.getLogFileWriter(logFileName);
		
		//first, identify the nature of the test - verify that the file locations are correct
		logFileWriter.write("Now executing the file comparison");
		logFileWriter.newLine();
		logFileWriter.newLine();
		identifyFileData(logFileWriter);		
		compareFileSizes(logFileWriter);
		compareValues(logFileWriter);
		
	}//end test
	
	private void identifyFileData(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("File containing locations of files to be tested is: " +inputs.getPath());
		logFileWriter.newLine();
		
		logFileWriter.write("File containing data before changes is: "+beforeFile.toString());
		logFileWriter.newLine();
		/*
		logFileWriter.write("The worksheet tab for this file is: "+beforeWorksheetName);
		logFileWriter.newLine();
		*/
		logFileWriter.write("File containing data after changes is: "+afterFile.toString());
		logFileWriter.newLine();
		/*
		logFileWriter.write("The worksheet tab for this file is: "+afterWorksheetName);
		logFileWriter.newLine();
		*/
	}//end identifyFileData method
	
	
	private void compareFileSizes(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** Now comparing the sizes of the two files ***");
		logFileWriter.newLine();
		
		LineIterator before = new LineIterator(beforeFile); 
		LineIterator after = new LineIterator(afterFile);
		String message = new String();

		
		int beforeFileRowCount = 0;
		int beforeFileColumnCount = 0;
		String nextLine = new String();
		try {
			while(before.hasNext()) {
				nextLine = before.nextLine();				
				String[] parsedLine = nextLine.split("\\t");
				beforeFileColumnCount = parsedLine.length;
				String PTA = parsedLine[0]+"-"+parsedLine[11]+"-"+parsedLine[18];
				beforeData.put(PTA, parsedLine);
				beforeFileRowCount++;
			}//end while
		}//end try
		finally {
			beforeFile.close();
			before.close();
		}
		printToAllOutputs ("The "+BEFORE+" file has "+beforeFileRowCount+" rows;", logFileWriter);
		
		int afterFileRowCount = 0;
		int afterFileColumnCount = 0;
		
		nextLine = new String();
		try {
			while(after.hasNext()) {
				nextLine = after.nextLine();
				String[] parsedLine = nextLine.split("\\t");
				afterFileColumnCount = parsedLine.length;
				String PTA = parsedLine[0]+"-"+parsedLine[11]+"-"+parsedLine[18];
				afterData.put(PTA, parsedLine);
				afterFileRowCount++;
			}
		}
		finally {
			afterFile.close();
			after.close();
		}
		
		if (beforeFileColumnCount == afterFileColumnCount) {
			printToAllOutputs ("The two files have the same number of columns: "+beforeFileColumnCount, logFileWriter);
			columnCount = beforeFileColumnCount;
		}//end if
		else {
			message = "**** ERROR: The two files have different numbers of columns: "
					+"Before File has "+beforeFileColumnCount+" columns, but "
					+"After File has "+afterFileColumnCount+" columns; \n****";
			printErrorMessage(message, logFileWriter);
		}//end else

		
		if (beforeFileRowCount == afterFileRowCount) {
			printToAllOutputs ("The two files have the same number of rows: "+beforeFileRowCount, logFileWriter);
		}//end if
		else {
			message = "**** ERROR: The two files have different numbers of rows: "
					+"Before File has "+beforeFileRowCount+" rows, but "
					+"After File has "+afterFileRowCount+" rows; \n****";
			printErrorMessage(message, logFileWriter);
		}//end else
		
		printToAllOutputs ("The "+AFTER+" file has "+beforeFileRowCount+" rows;", logFileWriter);
		
		printToAllOutputs("The total size of the "+BEFORE+" data structure is "+beforeData.size(), logFileWriter);
		printToAllOutputs("The total size of the "+AFTER+" data structure is "+afterData.size(), logFileWriter);
		

	}//end compareFileSizes method
	
	public void compareValues(BufferedWriter logFileWriter) throws Exception{
		Set<String> beforeSet = beforeData.keySet();//we will use the PTA's in the "Before" file 
		Iterator<String> beforeSetIterator = beforeSet.iterator();
		printToAllOutputs("*** Now comparing values, one at a time ****", logFileWriter);
		printToAllOutputs("We will iterate through "+beforeSet.size()+" different PTA's", logFileWriter);
		int iteration = 0;
		int info = 0;
		while(beforeSetIterator.hasNext()) {
			String key =  beforeSetIterator.next();
			iteration++;
			boolean mismatch = false;
			try {
				//printToAllOutputs("Key found: "+key, logFileWriter);
				String[] beforeValue = beforeData.get(key);
				String[] afterValue = afterData.get(key);
				//logFileWriter.write("Value found:");
				//logFileWriter.newLine();
				//boolean ceaseExaminationOfKey = false;
				if (beforeValue.length != afterValue.length) {
					info++;
					logFileWriter.write("Info: There are a different number of values between Before and After files for key "+key);
					logFileWriter.newLine();
					//ceaseExaminationOfKey = true;
					logFileWriter.write("Here are the values for the 'Before' file: ");
					logFileWriter.newLine();
					for (int i=0; i<beforeValue.length; i++) {
						logFileWriter.write(beforeValue[i]+"; ");
					}//end inner for loop
					logFileWriter.newLine();
					logFileWriter.write("Here are the values for the same key for the 'After' file: ");
					logFileWriter.newLine();
					for (int i=0; i<afterValue.length; i++) {
						logFileWriter.write(afterValue[i]+"; ");
					}//end inner for loop
					logFileWriter.newLine();
				}//end if
				for (int i=0; i<columnCount && i<beforeValue.length; i++) {
					if (! beforeValue[i].equalsIgnoreCase(afterValue[i])) {
						mismatch = true;
						String message = "*** ERROR: MISMATCH at index "+i
								+"; Before file has value '"+beforeValue[i]
								+"', but After file has value '"+afterValue[i]+"' ***; \n";
						printErrorMessage(message, logFileWriter);
					}//end if
				}//end for
				//logFileWriter.newLine();
			}//end try clause
			catch(Exception e) {
				printToAllOutputs("Could not finish iterating through everything because of Exception "+e.getMessage(),logFileWriter);
				e.printStackTrace();
			}//end catch clause
			finally {
				if (mismatch) {
					logFileWriter.write("There was a mismatch at iteration "+iteration+" for PTA '"+key+"'");
					logFileWriter.newLine();
				}//end if
				else ;//printToAllOutputs("Complete match at iteration "+iteration+" for PTA '"+key+"'", logFileWriter);
			}//end finally
		}//end while
		
		printToAllOutputs("compareValues method has successfully finished having gone through "
						+iteration+" iterations", logFileWriter);
		printToAllOutputs("There were "+info+" instances in which the Before and After files had a different number of columns", logFileWriter);
	}//end compareValues method
	
	@After
	public void tearDown() throws Exception {
		TestFileController.writeTestResults(testEnv, testCaseNumber, testName, testDescription, verificationErrors);
		logFileWriter.newLine();
		logFileWriter.close();
	}//end tearDown method
	
	private void printToAllOutputs(String message, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write(message);
		logFileWriter.newLine();
		
		System.out.println(message);
	}//end printToAllOutputs method
	
	private void printErrorMessage(String message, BufferedWriter logFileWriter) throws Exception{
		printToAllOutputs(message, logFileWriter);
		verificationErrors.append(message);
	}//end printErrorMessage method
	
	/*
	 * We will need to iterate through each row in the "Before" file and in the "After" file.
	 * We will store the data in two separate data structures - one for "Before" data, 
	 * and one for "After" data.
	 * The data structures will each have a key - the PTA - and a value - the Row as a whole.
	 * Each row will contain only the data in the columns corresponding to the "Before" file.
	 * 
	 * We will create the PTA string from the first, twelfth, and nineteenth columns in each row.
	 * We will concatenate the data into a String, each of the three elements separated by a comma.
	 * We will read the data for each row into an array of Strings
	 * The size of each such array will be equal to the number of columns in the "Before" file
	 * 
	 * The PTA will be the key and the String array will be the value in the two data structures.
	 * 
	 * After the two data structures are fully populated, we will iterate through the "Before" structure.
	 * We will iterate by keys.  For each key in the "Before" structure, we will find the same key in 
	 * the "After" structure and report an error if we cannot find that key.  
	 * If we do find the same key in the "After" data structure, then we will compare the values corresponding
	 * to the keys in both structures.  We will compare them by iterating through the "Before" value array
	 * and comparing each element in the array to the same element in the "After" value array.
	 */
	

}//end Class
