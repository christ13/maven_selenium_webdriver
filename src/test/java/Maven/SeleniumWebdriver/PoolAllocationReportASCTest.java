package Maven.SeleniumWebdriver;


import java.util.ArrayList;
import java.util.TreeMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/*
 * This Class is an immediate child of ASCTest.  It tests the Pool Allocation Report.
 * 
 * The report holds the total pool allocation amount (per pool, both DO and Dept) for
 * the current Fiscal Year, for all Known salary increases.
 * 
 * Of note, Unknown faculty members are not included in this report, so they will need
 * to be excluded from the expected test data.  Because an Unknown faculty member cannot
 * have an ASC in "Known" stage, we solve this problem by including only "Known" ASC's
 * in the test data.
 * 
 * Also, there is an outstanding issue - HANDSON-3251 - that is going to cause the test
 * to fail.  Source Pools other than the Regular Merit Pool and the Market Pool may have 
 * ASC values that are associated with them, but the figures won't appear under the 
 * columns for those Source Pools.  They will be inappropriately placed under the column
 * for the Regular Merit Pool.  Unless this issue is fixed, this test will continue to 
 * fail.  
 * 
 */
public class PoolAllocationReportASCTest extends ASCTest {

	@Before
	public void setUp() throws Exception {
		myExcelReader = TestFileController.ControlSheetReportSuite;
		mySheetName = TestFileController.PoolAllocationWorksheetName;
		rowForColumnNames = TestFileController.PoolAllocationWorksheetHeaderRow;
		verificationErrors = new StringBuffer();
		testName = "PoolAllocationReportASCTest";
		testEnv = "Test";
		testCaseNumber = "031";
		testDescription = "ASC Test of Pool Allocation Report";
		logFileName = "Pool Allocation Report ASC Test.txt";
		logFileWriter = TestFileController.getLogFileWriter(logDirectoryName  +"/" + logFileName);
		criteria = new TreeMap<String, String>();
		criteria.put(ExpectedASCData.StringAttributeKeys[ExpectedASCData.StageIndex], 
				ASCTest.stages[ASCTest.KnownStageIndex]);

		searchDelimiterColumn = 0;
		excludeZeroDOShare = false;
		backoutPYs = true;

		expectedStringAttributeNames = new String[4];
		actualStringAttributeNames = new String[4];
		expectedStringAttributeNames[0] = actualStringAttributeNames[0] = ASCFacultyValues.StringAttributeKeys[ASCFacultyValues.ClusterIndex];
		expectedStringAttributeNames[1] = actualStringAttributeNames[1] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.DepartmentIndex];
		expectedStringAttributeNames[2] = actualStringAttributeNames[2] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.FacultyIndex];
		expectedStringAttributeNames[3] = actualStringAttributeNames[3] = ASCFacultyValues.StringAttributeKeys[ASCFacultyValues.RankIndex];
		
		expectedAmountAttributeNames = new String[1];
		actualAmountAttributeNames = new String[1];
		expectedAmountAttributeNames[0] = ExpectedASCData.amountAttributeKeys[ExpectedASCData.FTEPercentIndex];
		actualAmountAttributeNames[0] = "Dept FTE";
		
		shareNames = new String[6];
		shareNames[0] = "Regular Merit Pool";
		shareNames[1] = "Market Pool";
		shareNames[2] = "Gender Pool";
		shareNames[3] = "Test Pool";
		shareNames[4] = "Albert Eustace Peasmarch Fund";
		shareNames[5] = "Total Allocation";
		
		expectedShareCriteria = new TreeMap<String, ArrayList<String>>();
		
		expectedShareCriteria.put(shareNames[0], instantiateList(ASCTest.sourcePools[ASCTest.RegularMeritPoolIndex]));
		expectedShareCriteria.put(shareNames[1], instantiateList(ASCTest.sourcePools[ASCTest.MarketPoolIndex]));
		expectedShareCriteria.put(shareNames[2], instantiateList(ASCTest.sourcePools[ASCTest.GenderPoolIndex]));
		expectedShareCriteria.put(shareNames[3], instantiateList(ASCTest.sourcePools[ASCTest.TestPoolIndex]));
		expectedShareCriteria.put(shareNames[4], instantiateList(ASCTest.sourcePools[ASCTest.PeasmarchFundIndex]));
		expectedShareCriteria.put(shareNames[5], instantiateList(ASCFacultyValues.ShareTotal));

		super.setUp();
	}//end setUp method
	
	private ArrayList<String> instantiateList(String sourcePool){
		ArrayList<String> list = new ArrayList<String>();
		list.add(ASCFacultyValues.ShareTotal);
		list.add(sourcePool);
		return list;
	}//end instantiateList method

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}//end tearDown method

	/*
	 * (non-Javadoc)
	 * @see Maven.SeleniumWebdriver.ASCTest#test()
	 * Here, we are restricting the data set to two Categories which are handled normally.
	 * There are two iterations - one for each Category.  
	 * We still have to add another category - "Governance Related" but we don't take the 
	 * deptShares and doShares from the ASCFacultyValues objects - we take it directly from
	 * the "Total Amount" value in the ExpectedASCData Object for that Category.  
	 * Other commitments from the Control Sheet Detail Report (other than ASC's) are not 
	 * under consideration.
	 */
	@Test
	public void test() throws Exception {
		String delimiterValueName = "Category";
		String[] validCategories = {"Faculty Appointment Related", "Salary Promises"};
		useASCOnly = false;
		for(int i=1; i<=validCategories.length; i++){
			String message = "iteration " + i + " with criteria name " +delimiterValueName
					+" and value " + validCategories[i-1];
			outputMessage("**** starting " + message +" ****", logFileWriter);
			criteria.put(delimiterValueName, validCategories[i-1]);
			super.test();
			criteria.remove(delimiterValueName);
			outputMessage("**** ending " + message +" ****", logFileWriter);
		}//end for loop
		/*
		 * Governance Related commitments need to be handled in a special way.  The standard
		 * "deptShares" and "doShares" won't work with these.  The only commitment amounts
		 * that show up for ASC's with Category "Governance Related" come from ASC's, not from
		 * any other lines in the Control Sheet Detail Report.  So, we have to make the expected
		 * values come exclusively from the ExpectedASCData Objects' "TotalAmount" value
		 * (that name comes from the "amountAttributeKeys" String array in ExpectedASCData, and
		 * the value is stored in the "amountAttributes" int array.  You can look it up through
		 * the ExpectedASCData.TotalAmountIndex.
		 * 
		 *  
		 *  We have a boolean variable that determines whether the 
		 *  'expectedShareValues' array gets populated from the "doShares" and "deptShares" 
		 *  arrays in ASCFacultyValues or directly from the "TotalAmount" values in the 
		 *  ExpectedASCDataSet. 
		 *  
		 *  If the boolean variable is "false" then process shareNames from
		 *  deptShares and doShares normally.  However, if is "true", then:
		 *  (1) zero out the shareValues array
		 *  (2) iterate through the ExpectedASCDataSet after the filters are applied and 
		 *  acquire the name of the Source Pool - pass that as a parameter to the 
		 *  "getIndexOfShareAmount" method to determine the applicable second dimension index 
		 *  of the shareValues array to assign the "TotalAmount" of the ExpectedASCData Object to.
		 */
		String validCategory = "Governance Related";
		String message = "iteration " + (validCategories.length + 1) + " with criteria name " +delimiterValueName
				+" and value " + validCategory;
		outputMessage("**** starting " + message +" ****", logFileWriter);
		criteria.put(delimiterValueName, validCategory);
		useASCOnly = true;
		super.test();
		criteria.remove(delimiterValueName);
		outputMessage("**** ending " + message +" ****", logFileWriter);
	}//end test method
	

}
