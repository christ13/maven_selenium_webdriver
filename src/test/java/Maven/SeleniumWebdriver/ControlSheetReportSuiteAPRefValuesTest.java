package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.LinkedList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/*
 * This is a superclass for all tests pertaining to the Control Sheet Report Suite.
 * It has several methods which are inherited by its subclasses.
 * 
 * The objective in creating this superclass and having subclasses extend it is to 
 * code something once, and then use the Java property of inheritance so that it doesn't
 * have to be coded again.
 * 
 */

public class ControlSheetReportSuiteAPRefValuesTest {

	public static final int FacultyName = 0;
	public static final int Department = 1;
	public static final int APReferenceValue = 2;
	public static final int FTEAdjustedDOShareAmount = 3;
	
	public static final String DOShareAmountColumnName = "FTE Adjusted DO Share Amount";
	
	static StringBuffer verificationErrors;


	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws Exception{
	}
	
	/*
	 * This will be inherited by subclasses, which will create the parameters and pass them
	 * to it.
	 */
	
	public void verifyAPValuesInControlSheetReportSuite(String[][] expectedValues, String[][] actualValues, BufferedWriter logFileWriter) throws Exception{
		verificationErrors = new StringBuffer();
		logFileWriter.newLine();
		TestFileController.writeToLogFile("Method verifyAPValuesInControlSheetReport executing", logFileWriter);
		/* 
		 * This method "getActualAPFacultyValues" will have to be re-coded.  
		 * It will have to accept the input parameters for this method from
		 * the subclass calling this "verifyAPValuesInControlSheetReport" method
		 */
		TestFileController.writeToLogFile("Expected vs Actual Value Comparisons are as follows: ", logFileWriter);
		//System.out.println("Expected vs Actual Value Comparisons are as follows: ");
		for (int i=0; i<expectedValues.length; i++){
			for (int j=0; j<expectedValues[i].length; j++){
				//System.out.print("expectedValues[i][j] i = " + i);
				//System.out.println(", j = " +j);
				String comparisonData = "Expected: " + expectedValues[i][j] + " vs Actual: " + actualValues[i][j];
				logFileWriter.write(comparisonData);
				//System.out.print(comparisonData);
				if (j == FTEAdjustedDOShareAmount){
					double expected = Double.valueOf((Long) NumberFormat.getNumberInstance(java.util.Locale.US).parse(expectedValues[i][j]));
					double actual = Double.valueOf((Long) NumberFormat.getNumberInstance(java.util.Locale.US).parse(actualValues[i][j]));
					//do float-to-float comparison
					logFileWriter.write(" (Number-to-number comparison invoked) - expected number is " + expected +", actual number is "+actual);
					if (expected == actual){
						//System.out.print(", match");
						logFileWriter.write(", match");
					}//end if - it matches
					else{
						verificationErrors.append("MISMATCH: " + comparisonData);
						logFileWriter.write(", MISMATCH");
						//System.out.print(", MISMATCH");
					}//end else - it doesn't match
				}//end outer if - it's the FTE Adjusted DO Share Amount, requiring number-to-number comparison
				else{
					logFileWriter.write(" (String-to-String comparison invoked)");
					//do String-to-String comparison
					if (expectedValues[i][j].equalsIgnoreCase(actualValues[i][j])){
						logFileWriter.write(", match");
					}//end if - it matches
					else{
						verificationErrors.append("MISMATCH: " + comparisonData);
						//System.out.print(", MISMATCH");
						logFileWriter.write(", MISMATCH");
					}//end else - it doesn't match
				}//end outer else - it's not the FTE Adjusted DO Share Amount, thus requiring String-to-String comparison
				logFileWriter.newLine();
				//System.out.println();
			}//end inner for - iterating through all the same value types for different faculty member/dept combinations
		}//end outer for - iterating through all the different possible value types
		//TestFileController.writeTestResults("Test", "001", "Comparison of FTE Adjusted Raises", "Control Sheet Detail Report Tests", verificationErrors);
		
	}//end verifyAPValuesInControlSheetDetailReport method
	
	protected String[][] getExpectedAPFacultyValues(String clusterName, BufferedWriter logFileWriter) throws Exception{
		String message = "method getExpectedAPFacultyValues called with String clusterName as input";
		//System.out.println(message);
		TestFileController.writeToLogFile(message, logFileWriter);
		return getExpectedAPFacultyValues(HandSOnTestSuite.apFacultyValues.getNameDeptCombosForCluster(clusterName, logFileWriter), logFileWriter);
	}
		
	protected String[][] getExpectedAPFacultyValues(String[][] facultyNamesAndDepartments, BufferedWriter logFileWriter) throws Exception{
		String[][] expectedValues = new String[facultyNamesAndDepartments[FacultyName].length][4];
		//System.out.println("expected Values array has the following size: " + expectedValues[FacultyName].length);
		/*
		 * this simulates rows and columns - each row corresponds to a row in APFacultyValues.
		 * Each column represents a different value - FacultyName, APReferenceValue, FTE, and FTEAdjTotalCommitment
		 */
		TestFileController.writeToLogFile("method getExpectedAPFacultyValues is executing", logFileWriter);
		TestFileController.writeToLogFile("Expected Values are as follows: ", logFileWriter);
		logFileWriter.newLine();
		//System.out.println("Expected Values are as follows: ");
		for(int rowIndex=0; rowIndex < facultyNamesAndDepartments[FacultyName].length; rowIndex++){
			StringBuffer output = new StringBuffer();
			output.append("Row Index: " + rowIndex);
			String facultyName = facultyNamesAndDepartments[FacultyName][rowIndex];
			expectedValues[rowIndex][FacultyName] = facultyName;
			output.append(", Faculty Name: '" + facultyName);
			String department = facultyNamesAndDepartments[Department][rowIndex];
			expectedValues[rowIndex][Department] = department;
			output.append("', Department Name: " + department);
			expectedValues[rowIndex][APReferenceValue] = HandSOnTestSuite.apFacultyValues.getAPReferenceValueForFacultyName(facultyName);
			output.append(", AP Reference Value: '" + expectedValues[rowIndex][APReferenceValue]);
			//System.out.println(output.toString());
			expectedValues[rowIndex][FTEAdjustedDOShareAmount] = 
						String.valueOf((int)
								(HandSOnTestSuite.apFacultyValues.getPrimitiveDoubleDeptFTEForFacultyNameAndDept(facultyName, department) 
								* HandSOnTestSuite.referenceValues.getFloatAmountForAPType(expectedValues[rowIndex][APReferenceValue]))
								);
			output.append("', FTE Adj DO Share Amount: " + expectedValues[rowIndex][FTEAdjustedDOShareAmount]);
			//System.out.println(output.toString());
			TestFileController.writeToLogFile(output.toString(), logFileWriter);
		}//end for loop (rowIndex)
		TestFileController.writeToLogFile("Size of expected Values to be returned is: " + expectedValues.length, logFileWriter);
		return expectedValues;
		
	}

	protected String[][] getExpectedAPFacultyValues(BufferedWriter logFileWriter) throws Exception{
		String[][] facultyNamesAndDepartments = HandSOnTestSuite.apFacultyValues.getUniqueFacultyNameDepartmentCombinations(logFileWriter);
		return getExpectedAPFacultyValues(facultyNamesAndDepartments, logFileWriter);
	}//end getExpectedFacultyValues method
	
	/*
	 * This will extract AP-related values from the Control Sheet Detail tab of the Control Sheet Report Suite.xls file and store them
	 * in a two-dimensional String array which it will return.  We will employ the getMultipleValuesInRows method of the 
	 * ExcelReader class.  Here is the description of that particular method:
	 * 
	 * Given a HashMap of column names and values for those columns, and given a list of 
	 * other column names, we want to return a LinkedList of HashMaps - one HashMap for every
	 * row found which contains the column values found for the column names given in the
	 * HashMap which is specified as input.  Each HashMap will contain name-value pairs.
	 * The names are equal to the other column names given in the input list of column names.
	 * The values are equal to the column values found for those columns in the column list
	 * where the name-value criteria given in a HashMap are specified.
	 * 
	 * ... and here's the method declaration:
	 * LinkedList<HashMap<String, String>> getMultipleValuesInRows (String sheetName, 
	 * 													HashMap<String, String> searchCriteria, 
	 * 													String[] columnNames, 
	 * 													BufferedWriter logFileWriter)
	 * 
	 * So, we need a HashMap of column names and values for those columns that comprise our search criteria and we
	 * will use this as one of the inputs.  We will also use an array of String Objects as the columns whose values we want returned.
	 * That will be another input.  The only two other inputs are pretty self-explanatory.
	 * 
	 * We will accept both the aforementioned HashMap and array of String Objects as input parameters from a subclass.
	 * The subclass can instantiate both the HashMap and the array of String Objects and call this method.
	 * 
	 * 
	 * Once we have the LinkedList of HashMaps, we will need to convert it to a two-dimensional array of Strings.
	 * 
	 */

	protected String[][] getActualAPFacultyValues(String[][] facultyNamesDepts, String sheetName, BufferedWriter logFileWriter) throws Exception{
		TestFileController.writeToLogFile("method getActualAPFacultyValues is executing", logFileWriter);
		String[] columnNames = {"Name", "Department", "Description", "FTE Adjusted DO Share Amount"};
		String[][] actualAPFacultyValues = new String[facultyNamesDepts[0].length][columnNames.length];
		for (int facultyNameIndex=0; facultyNameIndex<facultyNamesDepts[0].length; facultyNameIndex++){
			String name = facultyNamesDepts[0][facultyNameIndex];
			String department = facultyNamesDepts[1][facultyNameIndex];
			String description = HandSOnTestSuite.apFacultyValues.getAPReferenceValueForFacultyName(name);
			HashMap<String, String> searchCriteria = new HashMap<String, String>();
			searchCriteria.put("Department", department);
			searchCriteria.put("Name", name);
			searchCriteria.put("Description", description);
			//first, let's just get the values, then we'll format the values into a two-dimensional array of Strings
			//System.out.println("Search Criteria for " + sheetName +" is as follows "+searchCriteria.toString());
			LinkedList<HashMap<String, String>> rawValues 
				= TestFileController.ControlSheetReportSuite.getMultipleValuesInRows(sheetName, searchCriteria, columnNames, logFileWriter);
			//System.out.println("Raw Values gathered from the " + sheetName + " tab are as follows: "+rawValues.toString());
			String message = "Number of rows in " + sheetName + " that match search criteria: " + rawValues.size();
			//System.out.println(message);
			TestFileController.writeToLogFile(message, logFileWriter);
			for (int columnNameIndex=0; columnNameIndex<columnNames.length; columnNameIndex++){
				if (rawValues.size() != 1){
					String warning = "*** WARNING *** There is more than one row with this search criteria - please adjust search criteria";
					//System.out.println(warning);
					TestFileController.writeToLogFile(warning, logFileWriter);
				}
				//we're going to assume that there's always one and only one HashMap returned in the LinkedList
				actualAPFacultyValues[facultyNameIndex][columnNameIndex] = (String)rawValues.get(0).get(columnNames[columnNameIndex]);
			}//end inner for - the LinkedList of values
		}//end outer for = facultyNameIndex
		return actualAPFacultyValues;
	}//end getActualAPFacultyValues method

	protected String[][] getActualAPFacultyValues(String sheetName, BufferedWriter logFileWriter) throws Exception{
		String[][] facultyNamesDepts = HandSOnTestSuite.apFacultyValues.getUniqueFacultyNameDepartmentCombinations(logFileWriter);
		return getActualAPFacultyValues(facultyNamesDepts, sheetName, logFileWriter);
	}

	protected String[][] getActualAPFacultyValues(String clusterName, String sheetName, BufferedWriter logFileWriter) throws Exception{
		String[][] facultyNamesDepts = HandSOnTestSuite.apFacultyValues.getNameDeptCombosForCluster(clusterName, logFileWriter);
		return getActualAPFacultyValues(facultyNamesDepts, sheetName, logFileWriter);
	}

}
