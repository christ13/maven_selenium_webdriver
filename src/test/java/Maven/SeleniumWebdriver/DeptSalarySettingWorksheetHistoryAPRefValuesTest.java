package Maven.SeleniumWebdriver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DeptSalarySettingWorksheetHistoryAPRefValuesTest extends
		DeptSalarySettingWorksheetAPRefValuesTest {

	@Before
	public void setUp() throws Exception {
		departmentName = "History";
		myExcelReader = TestFileController.DepartmentSalarySettingHistoryReport;
		worksheetName = TestFileController.DepartmentSalarySettingWorksheetName;
		myExcelReader.setRowForColumnNames(TestFileController.DepartmentSalarySettingWorksheetHeaderRow);
		testEnv = "Test";
		testName = "Test Department Salary Setting Worksheet " + departmentName;
		testDescription = "Verify AP Values in Department Salary Setting Worksheet " + departmentName;
		testCaseNumber = "011";
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void test() throws Exception{
		super.test();
	}

}
