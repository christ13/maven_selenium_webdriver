package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;


import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.junit.internal.TextListener;
import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;


@RunWith(JUnitParamsRunner.class)

public class ReportRunner {
	
	public static final String ORIGINAL = "Original";
	public static final String ALTERED = "Altered";
	public static String referenceWhichValues;
	public static String referenceWhichTestSection;
	public static final String FIRST = "FIRST";
	public static final String SECOND = "SECOND";
	public static final String THIRD = "THIRD";
	public static final String FOURTH = "FOURTH";
	public static String logDirectoryName;

	
	private BufferedWriter logFileWriter;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	//@Parameters({ORIGINAL, ALTERED})
	@Parameters({ORIGINAL +","+THIRD})
	public void test(String whichValues, String whichTestSection) throws Exception {
		
		//referenceWhichValues = whichValues;
		referenceWhichValues = System.getenv("referenceWhichValues");
		referenceWhichTestSection = System.getenv("testWhichSection");
		logDirectoryName = "";
		HandSOnTestSuite.renew();
		logDirectoryName = TestFileController.LogDirectoryName +"/" + referenceWhichValues;
		String testName = "HandSOn TEST - Set values, and run reports";
		String logFileName = logDirectoryName +"/" + testName +".txt";
		System.out.println("Log Directory Name: " + logDirectoryName);
		if (TestFileController.setLogDirectoryName(logDirectoryName))
			this.logFileWriter = TestFileController.getLogFileWriter(logFileName);
		else fail ("Could not create log directory: " + logDirectoryName);
		TestFileController.writeToLogFile("Test Class ReportRunner is running", logFileWriter);
		TestFileController.writeToLogFile("This test directory is " + logDirectoryName, logFileWriter);	
		TestFileController.writeToLogFile("The data set is " + whichValues, logFileWriter);
		setAPValues(whichValues);
		if (whichValues.equalsIgnoreCase(ALTERED))
			SalarySettingManager.setAPValuesinGUI(TestFileController.AlteredAPValuesWorksheetName);
		else
			SalarySettingManager.setAPValuesinGUI(TestFileController.OriginalAPValuesWorksheetName);
		ControlSheetUI.adjustAllPercentageValues(logFileWriter, whichValues);
		SalarySettingManager.populateOverrideList();
		JUnitCore junit = new JUnitCore();
		junit.addListener(new TextListener(System.out));
		if (referenceWhichTestSection.equalsIgnoreCase(FIRST)) {
			testFirstSection(junit, whichValues, logFileWriter);
		}
		else if (referenceWhichTestSection.equalsIgnoreCase(SECOND)) {
			testSecondSection(junit, whichValues, logFileWriter);
		}
		else if (referenceWhichTestSection.equalsIgnoreCase(THIRD)) {
			testThirdSection(junit, whichValues, logFileWriter);
		}
		else if (referenceWhichTestSection.equalsIgnoreCase(FOURTH)) {
			testFourthSection(junit, whichValues, logFileWriter);
		}
	}//end test method
	

	protected void testFirstSection(JUnitCore junit, String whichValues, BufferedWriter logFileWriter) throws Exception{
		runReports(whichValues);
		TestFileController.resetAllExcelReaders();

		HandSOnTestSuite.expectedASCDataSet.populateListFromGUI(logFileWriter);
		Thread.sleep(2000);
		int numberOfASCs = HandSOnTestSuite.expectedASCDataSet.size();
		int numberOfFacultyValues = HandSOnTestSuite.expectedASCDataSet.getNumberOfASCFacultyValues();
		TestFileController.writeToLogFile("size of ExpectedASCDataSet is " + numberOfASCs, logFileWriter);
		TestFileController.writeToLogFile("Number of ASCFacultyValues Objects is " + numberOfFacultyValues, logFileWriter);

		junit.run(ReportFileComparisonMaker.class);
		junit.run(UIComparisonMaker.class);
		junit.run(SalaryIncreasePercentagesAndAmountsTestSuite.class);
		junit.run(SalarySettingTabTestSuite.class);
		
		UIController.switchWindowByURL(UIController.loadSalarySettingWindowURL, logFileWriter);
		HandSOnTestSuite.salarySettingMgrTabUI.switchToSalarySettingManagementTab();
		HandSOnTestSuite.salarySettingMgrTabUI.initialize();
		junit.run(SalarySettingMgrTabTestSuite.class);
		
		//now switch back to the original window and go to the Queries tab
		
		TestFileController.closeAllExcelReaders();
		logFileWriter.close();

		runReferenceValuesTests();
		runFacultyValuesTests();
}//end testFirstSection method
	
	
	protected void testSecondSection (JUnitCore junit, String whichValues, BufferedWriter logFileWriter) throws Exception{
		runControlSheetReportSuite (whichValues, logFileWriter);
		TestFileController.resetAllExcelReaders();
		
		junit.run(SearchAuthorizationTestSuite.class);
		junit.run(RecruitmentTestSuite.class);		
	
		TestFileController.closeAllExcelReaders();
		logFileWriter.close();
	}//end testSecondSection method

	protected void testThirdSection (JUnitCore junit, String whichValues, BufferedWriter logFileWriter) throws Exception{
		runControlSheetReportSuite (whichValues, logFileWriter);
		TestFileController.resetAllExcelReaders();
		
		junit.run(FundingCommitmentTestSuite.class);
		//junit.run(FacultyTestSuite.class);
	
		TestFileController.closeAllExcelReaders();
		logFileWriter.close();
	}//end testSecondSection method

	protected void testFourthSection(JUnitCore junit, String whichValues, BufferedWriter logFileWriter) throws Exception {
		
		RetentionTestSuite retentionTestSuite = new RetentionTestSuite(junit);
		retentionTestSuite.test();
		//junit.run(FacultyTestSuite.class);
	
		/*
		TestFileController.closeAllExcelReaders();
		logFileWriter.close();
		*/
	}//end testFourthSection method
	
	
	public void setAPValues(String whichValues){
		  if (whichValues.equalsIgnoreCase("Altered")){
			  HandSOnTestSuite.referenceValues.resetValues(TestFileController.AlteredAPValuesWorksheetName);
		  }
	}
	
	public static void switchToReportTab(BufferedWriter logFileWriter) throws Exception{
		  SalarySettingManager.openSalarySetting(logFileWriter);
		  TestFileController.writeToLogFile("Now attempting to switch to the Report tab of Salary Setting", logFileWriter);
		  HandSOnTestSuite.salarySettingManager.switchToReportTab();
		  Thread.sleep(60000);
	}
	
	  public static void runReports(String whichValues) throws Exception {
		  String testName = "runReports method - Run Reports using Selenium";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = logDirectoryName +"/" + testName + ".txt";
		  BufferedWriter logFileWriter = TestFileController.getLogFileWriter(logFileName);
		  
		  switchToReportTab(logFileWriter);
		  runControlSheetReportSuite(whichValues, logFileWriter);

		  String fileName = "Department Verification Report.xls";
		  HandSOnTestSuite.salarySettingManager.runOneReportAtLocator(By.xpath("//a[14]"), logFileWriter);
		  SalarySettingManager.switchWindowByURL("https://handsonweb-"+HandSOnTestSuite.machineName+".stanford.edu/"+HandSOnTestSuite.envName+"/processSalSetWorksheetRequest.do?ACTION=showDeptSaSettingReport", logFileWriter);
		  SalarySettingManager.waitForElementPresent(By.id("clusterLevel"));
		  if (SalarySettingManager.isDropdownAccessible (By.id("clusterLevel"), "Any", logFileWriter))
			  TestFileController.writeToLogFile("Successfully selected Cluster value Any", logFileWriter);
		  else TestFileController.writeToLogFile("Could not select Cluster value Any", logFileWriter);
		  
		  if (SalarySettingManager.isDropdownAccessible (By.id("reportFormatType"), "XLS", logFileWriter))
			  TestFileController.writeToLogFile("Successfully selected Report Format value XLS", logFileWriter);
		  else TestFileController.writeToLogFile("Could not select Report Format value XLS", logFileWriter);
		  SalarySettingManager.getScreenShot(fileName, whichValues, logFileWriter);

		  SalarySettingManager.waitAndClick(By.id("buttonSubmit"));
		  TestFileController.copyFile (fileName, logFileWriter, logDirectoryName);

		  String[] cluster = {"Humanities and Arts", "Natural Sciences", "Social Sciences"};
		  
		  downloadChairSSWs(cluster, whichValues, logFileWriter);

		  downloadClusterSSWs (cluster, whichValues, logFileWriter);
		  
		  runFinanceReports(whichValues, logFileWriter);

		  //get back to the original window
		  SalarySettingManager.switchWindowByURL(SalarySettingManager.salarySettingURL, logFileWriter);

		  SalarySettingManager.switchWindowByURL(SalarySettingManager.salarySettingURL, logFileWriter);
		  String[] department = {"Biology", "Economics", "History", "Mathematics", "Physics", "Statistics", "Theater & Performance Studies"};
		  downloadDeptSSWs(department, whichValues, logFileWriter);

		  //now we work with the Provost Binder Lists By Reports
		  downloadProvostBinderReports(whichValues, logFileWriter);

		  downloadDeanSSW (whichValues, logFileWriter);
		  
		  downloadSchoolSSW (whichValues, logFileWriter);

		  downloadMasterSSW (whichValues, logFileWriter);
		  
		  logFileWriter.close();
	  }//end runReports() method
	  
		public static void runOneFinanceReportByInputFile(String initialLink, String linkText, String whichValues, BufferedWriter logFileWriter) throws Exception{
			int submitButtonIndex = 3;
			int downloadedFileIndex = 2;
			int destinationURLIndex = 1;
			
			
			logFileWriter.write("*** method runOneFinanceReportByInputFile called with link text '"+linkText+"' ****");
			logFileWriter.newLine();
			
			
			//switch back to the Queries tab
			UIController.switchWindowByURL(initialLink, logFileWriter);
			Thread.sleep(5000);
			//now, click on the applicable link to bring up the appropriate report-specific screen
			SalarySettingManager.waitAndClick(By.linkText(linkText), 5);
			//get all of the column names
			String[] columnNames = TestFileController.fileIn.getColumnNames(TestFileController.inputFileQueriesTab);
			Thread.sleep(5000);
			
			
			int row = TestFileController.fileIn.getCellRowNum(TestFileController.inputFileQueriesTab, 0, linkText);
			//Read all of the values, one at a time, in the columns in that row, and adjust the correct widget appropriately
			for (int col=0; col<columnNames.length; col++) {
				String cellValue = TestFileController.fileIn.getCellData(TestFileController.inputFileQueriesTab, col, row);

				if (cellValue.length() > 0) {
					//in order to use the widget, we need both the locator and the value - the value is always in the column to the right of the one holding the locator
					TestFileController.writeToLogFile("The value of column with name '"+columnNames[col]+"' is '"+cellValue+"';", logFileWriter);
					if (col == destinationURLIndex) {
						UIController.switchWindowByURL(cellValue, logFileWriter);
						Thread.sleep(5000);
					}
					else if (columnNames[col].contains("Menu")) {
						By by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(cellValue, logFileWriter);
						String menuItem = TestFileController.fileIn.getCellData(TestFileController.inputFileQueriesTab, col+1, row);
						System.out.println("Menu Item to be selected is '"+menuItem+"'");
						TestFileController.writeToLogFile("Menu Item to be selected is '"+menuItem+"'", logFileWriter);
						Thread.sleep(5000);
						SalarySettingManager.selectElementInMenu(by, menuItem);
					}//end if
					else if (columnNames[col].contains("Field") && (! columnNames[col].contains("Value"))) {
						By by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(cellValue, logFileWriter);
						String text = TestFileController.fileIn.getCellData(TestFileController.inputFileQueriesTab, col+1, row);
						TestFileController.writeToLogFile("Menu Item to be written to text field is '"+text+"'", logFileWriter);
						HandSOnTestSuite.salarySettingManager.writeStringToTextField(by, text, logFileWriter);
					}//end else if
				}//end if
			}//end for
			
			//click on the button to generate the report
			String rawLocator = TestFileController.fileIn.getCellData(TestFileController.inputFileQueriesTab, submitButtonIndex, row);
			if (rawLocator.length() > 0) {
				By by = HandSOnTestSuite.salarySettingManager.getByForLocator(rawLocator, logFileWriter);
				if (! UIController.waitAndClick(by)) {
					fail("Could not click on the button to submit the form at URL '"+columnNames[downloadedFileIndex]+"';\n");
				}
				String submitButtonLocator = TestFileController.fileIn.getCellData(TestFileController.inputFileQueriesTab, submitButtonIndex, row);
				By submitButtonBy = HandSOnTestSuite.salarySettingManager.getByForLocator(submitButtonLocator, logFileWriter);
				SalarySettingManager.waitAndClick(submitButtonBy);
			}//end if - do this if and only if there is an actual submit button - there may not be
			String fileName = TestFileController.fileIn.getCellData(TestFileController.inputFileQueriesTab, downloadedFileIndex, row);
					
			SalarySettingManager.getScreenShot(fileName, whichValues, logFileWriter);		
			TestFileController.copyFile(fileName, logFileWriter, TestFileController.LogDirectoryName+"/"+whichValues);
			
			if (isFileEmpty(fileName, whichValues, logFileWriter)) {
				fail("ERROR: "+fileName+" is unexpectedly at zero length\n");
			}
			else {
				logFileWriter.write(fileName+" is not empty, as expected");
				logFileWriter.newLine();
			}//end else - file is normal-sized, as expected
		}//end runOneFinanceReportByInputFile method

		public static boolean isFileEmpty(String fileName, String whichValues, BufferedWriter logFileWriter) throws Exception{
			File reportFile = new File(TestFileController.LogDirectoryName+"/"+whichValues+"/"+fileName);
			double bytes = reportFile.length();
			if (bytes == 0) {
				return true;
			}//end if - the file is empty
			else {
				double kilobytes = bytes / 1024;
				double megabytes = kilobytes / 1024;
				double gigabytes = megabytes / 1024;
				
				logFileWriter.newLine();
				TestFileController.writeToLogFile("**** Length of "+fileName+" is "+bytes+" bytes, or "+kilobytes+" kilobytes, or "
				+megabytes+" megabytes, or "+gigabytes+" gigabytes.****\n", logFileWriter);
				logFileWriter.newLine();
			}//end else - file is normal-sized, as expected
			return false;
		}//end isFileEmpty method
		

		public static void runFinanceReports (String whichValues, BufferedWriter logFileWriter) throws Exception{
			logFileWriter.write("**** method runFinanceReports is being run with "+whichValues+" values ****");
			logFileWriter.newLine();
			
			//switch back to the original window and go to the Queries tab
			Thread.sleep(5000);
			UIController.switchWindowByURL(UIController.startPage, logFileWriter);
			Thread.sleep(5000);
			UIController.driver.findElement(By.linkText("Queries")).click();

			runSalaryBudgetingReports(whichValues, logFileWriter);
			/*
					runOneFinanceReportBySelection(UIController.deansReportURL, whichValues, By.name("request.dean"), 
					"Debra Satz", "Dean's Report", logFileWriter);
			*/
			//switch back to the Queries tab
			runOneFinanceReportByInputFile(UIController.queriesPageURL, "Dean's Report", whichValues, logFileWriter);
			runOneFinanceReportByInputFile(UIController.queriesPageURL, "Department Commitment Reports", whichValues, logFileWriter);

			runOneFinanceReportByInputFile(UIController.departmentCommitmentReportsURL, "Summer Ninths Report", whichValues, logFileWriter);
			runOneFinanceReportByInputFile(UIController.departmentCommitmentReportsURL, "Transfer Status Report", whichValues, logFileWriter);
			runOneFinanceReportByInputFile(UIController.departmentCommitmentReportsURL, "Recipient Report", whichValues, logFileWriter);
			runOneFinanceReportByInputFile(UIController.departmentCommitmentReportsURL, "Award Report", whichValues, logFileWriter);
			runOneFinanceReportByInputFile(UIController.departmentCommitmentReportsURL, "Category Report", whichValues, logFileWriter);
			runOneFinanceReportByInputFile(UIController.departmentCommitmentReportsURL, "Sub-Category Report", whichValues, logFileWriter);
			runOneFinanceReportByInputFile(UIController.departmentCommitmentReportsURL, "Data Download", whichValues, logFileWriter);

			Thread.sleep(3000);
			String message = "Now closing the Department Commitment Report Window;\n ";
			System.out.println(message);
			logFileWriter.write(message);
			logFileWriter.newLine();
			UIController.driver.close();


			runOneFinanceReportByInputFile(UIController.queriesPageURL, "Department Quarterly Commitments Report", whichValues, logFileWriter);
			runOneFinanceReportByInputFile(UIController.queriesPageURL, "Endowed Chair Commitments Report", whichValues, logFileWriter);


		}//end runFinanceReports method

	  
	  
		public static void runSalaryBudgetingReports(String whichValues, BufferedWriter logFileWriter) throws Exception{
			//In some cases, there's a date string involved (filename + "_"+ "06_12_2019" +".csv")
			
			//now switch back to the original window and go to the Queries tab
			Thread.sleep(5000);
			UIController.switchWindowByURL(UIController.startPage, logFileWriter);
			Thread.sleep(5000);
			UIController.driver.findElement(By.linkText("Queries")).click();
			
			//first, bring up the Salary Budgeting Reports screen
			Thread.sleep(5000);
			SalarySettingManager.waitAndClick(By.linkText("Salary Budgeting Reports"));
			//now, switch to the Salary Budgeting Reports screen
			
			//now, start running the reports, one at a time
			runOneSalaryBudgetingReport(whichValues, TestFileController.SalaryBudgetingPopulationVerificationReportFileName, TestFileController.SalaryBudgetingPopulationVerificationReportName, logFileWriter);
			runOneSalaryBudgetingReport(whichValues, TestFileController.SalaryBudgetingReportFileName, TestFileController.SalaryBudgetingReportName, logFileWriter);
			runOneSalaryBudgetingReport(whichValues, TestFileController.SalaryBudgetingComparisonReportFileName, TestFileController.SalaryBudgetingComparisonReportName, logFileWriter);
			runOneSalaryBudgetingReport(whichValues, TestFileController.SalaryRateExportForHyperionUploadFileName, TestFileController.SalaryRateExportForHyperionUploadName, logFileWriter);
			runOneSalaryBudgetingReport(whichValues, TestFileController.SalaryPercentageExportForHyperionUploadFileName, TestFileController.SalaryPercentageExportForHyperionUploadName, logFileWriter);
			runOneSalaryBudgetingReport(whichValues, TestFileController.SalaryReserveDetailReportFileName, TestFileController.SalaryReserveDetailReportName, logFileWriter);
			runOneSalaryBudgetingReport(whichValues, TestFileController.SalaryReserveRollUpExportForHyperionUploadFileName, TestFileController.SalaryReserveRollUpExportForHyperionUploadName, logFileWriter);
			
			Thread.sleep(5000);
			UIController.switchWindowByURL("salaryBudgetingReports.jsp", logFileWriter);
			Thread.sleep(2000);
			UIController.driver.close();

			runOneFinanceReportByInputFile(UIController.queriesPageURL, "Dean's Report", whichValues, logFileWriter);
			runOneFinanceReportByInputFile(UIController.queriesPageURL, "Department Commitment Reports", whichValues, logFileWriter);
			runOneFinanceReportByInputFile(UIController.departmentCommitmentReportsURL, "Summer Ninths Report", whichValues, logFileWriter);
			runOneFinanceReportByInputFile(UIController.departmentCommitmentReportsURL, "Transfer Status Report", whichValues, logFileWriter);
			runOneFinanceReportByInputFile(UIController.departmentCommitmentReportsURL, "Recipient Report", whichValues, logFileWriter);
			runOneFinanceReportByInputFile(UIController.departmentCommitmentReportsURL, "Award Report", whichValues, logFileWriter);
			runOneFinanceReportByInputFile(UIController.departmentCommitmentReportsURL, "Category Report", whichValues, logFileWriter);
			runOneFinanceReportByInputFile(UIController.departmentCommitmentReportsURL, "Sub-Category Report", whichValues, logFileWriter);
			runOneFinanceReportByInputFile(UIController.departmentCommitmentReportsURL, "Data Download", whichValues, logFileWriter);

			Thread.sleep(3000);
			String message = "Now closing the Department Commitment Report Window;\n ";
			System.out.println(message);
			logFileWriter.write(message);
			logFileWriter.newLine();
			UIController.driver.close();

			runOneFinanceReportByInputFile(UIController.queriesPageURL, "Department Quarterly Commitments Report", whichValues, logFileWriter);
			runOneFinanceReportByInputFile(UIController.queriesPageURL, "Endowed Chair Commitments Report", whichValues, logFileWriter);

		}//end runSalaryBudgetingReports method
		
		protected static void runOneSalaryBudgetingReport(String whichValues, String fileName, String linkText, BufferedWriter logFileWriter) throws Exception{
			String salaryBudgetingReportsURL = "salaryBudgetingReports.jsp";
			//String whichDepartment = "Chemistry";
			//String whichDepartmentLabel = "salaryBudgetDTO.deptCd";
			String salaryBudgetDTOFiscalYearTextfieldLabel = "salaryBudgetDTOFiscalYear";
			String generateButtonLabel = "generate";
			String effectiveDateTextFieldLabel = "effecDate";
			String effectiveYearString = "20"+HandSOnTestSuite.FY.split("-")[1];
			String effectiveStartDateString = "09/01/20"+HandSOnTestSuite.FY.split("-")[0];
			String effectiveEndDateString = "08/31/"+effectiveYearString;
			String startDateTextFieldLabel = "dateStart";
			String endDateTextFieldLabel = "dateEnd";
			
			
			
			logFileWriter.newLine();
			TestFileController.writeToLogFile("**** Now attempting to run the "+linkText+" ****\n", logFileWriter);
			logFileWriter.newLine();
			
			//first. switch to the Budget Salary Setting Window
			Thread.sleep(5000);
			UIController.switchWindowByURL(salaryBudgetingReportsURL, logFileWriter);
			
			//now, click on the appropriate link
			SalarySettingManager.waitAndClick(By.linkText(linkText), 5);
			
			//now, switch to the appropriate window
			if (linkText.equalsIgnoreCase(TestFileController.SalaryBudgetingPopulationVerificationReportName)){
				UIController.switchWindowByURL(UIController.SalaryBudgetingPopulationVerificationReportURL, logFileWriter);
			}
			else if (linkText.equalsIgnoreCase(TestFileController.SalaryBudgetingReportName)) {
				UIController.switchWindowByURL(UIController.SalaryBudgetingReportURL, logFileWriter);
			}
			else if (linkText.equalsIgnoreCase(TestFileController.SalaryBudgetingComparisonReportName)) {
				UIController.switchWindowByURL(UIController.SalaryBudgetingComparisonReportURL, logFileWriter);
			}
			else if (linkText.equalsIgnoreCase(TestFileController.SalaryRateExportForHyperionUploadName)) {
				UIController.switchWindowByURL(UIController.SalaryRateExportForHyperionUploadURL, logFileWriter);
			}
			else if (linkText.equalsIgnoreCase(TestFileController.SalaryPercentageExportForHyperionUploadName)) {
				UIController.switchWindowByURL(UIController.SalaryPercentageExportForHyperionUploadURL, logFileWriter);
			}
			else if (linkText.equalsIgnoreCase(TestFileController.SalaryReserveDetailReportName)) {
				UIController.switchWindowByURL(UIController.SalaryReserveDetailReportURL, logFileWriter);
			}
			else if (linkText.equalsIgnoreCase(TestFileController.SalaryReserveRollUpExportForHyperionUploadName)) {
				UIController.switchWindowByURL(UIController.SalaryReserveRollUpExportForHyperionUploadURL, logFileWriter);
			}
			else {
				TestFileController.writeToLogFile("Report is "+linkText+" but it is not being run at this time", logFileWriter);
			}

			Thread.sleep(2000);
			//salaryBudgetDTOFiscalYearTextfieldLabel corresponds to the Fiscal Year text field.  It present with all Reports.
			
			HandSOnTestSuite.salarySettingManager.writeStringToTextField (By.id(salaryBudgetDTOFiscalYearTextfieldLabel), effectiveYearString, logFileWriter);
			Thread.sleep(3000);
			if (linkText.equalsIgnoreCase(TestFileController.SalaryBudgetingReportName)) {
				HandSOnTestSuite.salarySettingManager.writeStringToTextField (By.id(effectiveDateTextFieldLabel), effectiveStartDateString, logFileWriter);
				Thread.sleep(3000);
				//UIController.selectElementInMenu(By.name(whichDepartmentLabel), whichDepartment);
				//Thread.sleep(3000);
			}//end if - if it's the Salary Budgeting Report, enter the Effective Date
			else if (linkText.equalsIgnoreCase(TestFileController.SalaryBudgetingComparisonReportName)) {
				HandSOnTestSuite.salarySettingManager.writeStringToTextField(By.id(startDateTextFieldLabel), effectiveStartDateString, logFileWriter);
				Thread.sleep(3000);
				HandSOnTestSuite.salarySettingManager.writeStringToTextField(By.id(endDateTextFieldLabel), effectiveEndDateString, logFileWriter);
				Thread.sleep(3000);
			}
			else if ( linkText.equalsIgnoreCase(TestFileController.SalaryRateExportForHyperionUploadName) 
					|| linkText.equalsIgnoreCase(TestFileController.SalaryPercentageExportForHyperionUploadName)
					|| linkText.equalsIgnoreCase(TestFileController.SalaryReserveDetailReportName)
					|| linkText.equalsIgnoreCase(TestFileController.SalaryReserveRollUpExportForHyperionUploadName)
					) {
				HandSOnTestSuite.salarySettingManager.writeStringToTextField(By.id(startDateTextFieldLabel), effectiveStartDateString, logFileWriter);
				Thread.sleep(3000);			
			}
			
			//this is the Generate button.  It is present with all Reports.
			SalarySettingManager.waitAndClick(By.id(generateButtonLabel));
			
			SalarySettingManager.getScreenShot(fileName, whichValues, logFileWriter);
			
			if (linkText.equalsIgnoreCase(TestFileController.SalaryBudgetingReportName)
					|| linkText.equalsIgnoreCase(TestFileController.SalaryBudgetingComparisonReportName)
					|| linkText.equalsIgnoreCase(TestFileController.SalaryRateExportForHyperionUploadName)
					|| linkText.equalsIgnoreCase(TestFileController.SalaryReserveRollUpExportForHyperionUploadName)
					|| linkText.equalsIgnoreCase(TestFileController.SalaryReserveDetailReportName)
					|| linkText.equalsIgnoreCase(TestFileController.SalaryPercentageExportForHyperionUploadName)
					) {
				Thread.sleep(5000);
				Alert alert = null;
				try {
					alert = ControlSheetUI.driver.switchTo().alert();
					String alertText = alert.getText();
					String message = "Alert encountered - text is "+alertText+";\n";
					logFileWriter.write(message);
					logFileWriter.newLine();
					alert.accept();
				}//end try
				catch(Exception e) {
					logFileWriter.write("Could not complete actions with the alert because of Exception "+e.getMessage()+";");
					logFileWriter.newLine();
				}//end catch
			}//end if
			TestFileController.copyFile(fileName, logFileWriter, TestFileController.LogDirectoryName+"/"+whichValues);
			
			File reportFile = new File(TestFileController.LogDirectoryName+"/"+whichValues+"/"+fileName);
			double bytes = reportFile.length();
			if (bytes == 0) {
				fail("ERROR: "+fileName+" is unexpectedly at zero length\n");
			}
			else {
				double kilobytes = bytes / 1024;
				double megabytes = kilobytes / 1024;
				double gigabytes = megabytes / 1024;
				
				logFileWriter.newLine();
				TestFileController.writeToLogFile("**** Length of "+fileName+" is "+bytes+" bytes, or "+kilobytes+" kilobytes, or "
				+megabytes+" megabytes, or "+gigabytes+" gigabytes.****\n", logFileWriter);
				logFileWriter.newLine();
			}//end else - file is normal-sized, as expected
			Thread.sleep(2000);
			String message = "Now closing the Salary Budgeting Report Window;\n ";
			System.out.println(message);
			logFileWriter.write(message);
			logFileWriter.newLine();
			UIController.driver.close();//close this report window - keep things from getting cluttered
			Thread.sleep(2000);
		}//end runOneSalaryBudgetingReport method
		

	  
	  public static void downloadProvostBinderReports(String whichValues, BufferedWriter logFileWriter) throws Exception{
		  String linkText = "Provost Binder Lists by Reports";
		  String fileName = "Provost Binder List by Department.xls" ;
		  String partialURL = "https://handsonweb-"+HandSOnTestSuite.machineName+".stanford.edu/"+HandSOnTestSuite.envName+"/processSalSetWorksheetRequest.do?ACTION=showListByReports";
		  
		  SalarySettingManager.switchWindowByURL(SalarySettingManager.salarySettingURL, logFileWriter);
		  try{
			  UIController.driver.switchTo().alert().accept();
		  }//end try
		  catch(Exception e){
			  TestFileController.writeToLogFile("Alert not present", logFileWriter);
		  }//end catch
		  HandSOnTestSuite.salarySettingManager.runOneReportAtLocator(By.linkText(linkText), logFileWriter);
		  SalarySettingManager.getScreenShot(fileName, whichValues, logFileWriter);
		  SalarySettingManager.switchWindowByURL(partialURL, logFileWriter);
		  if (SalarySettingManager.isDropdownAccessible (By.id("clusterLevel"), "Department", logFileWriter)){
			  TestFileController.writeToLogFile("Successfully selected Department value on the Provost Binder Report Dialog", logFileWriter);
		  }
		  else TestFileController.writeToLogFile("Could not select Department value on the Provost Binder Report Dialog", logFileWriter);
		  SalarySettingManager.waitAndClick(By.id("buttonSubmit"));
		  TestFileController.copyFile (fileName, logFileWriter, logDirectoryName);

		  fileName = "Provost Binder List by Name.xls" ;
		  if (SalarySettingManager.isDropdownAccessible (By.id("clusterLevel"), "Name", logFileWriter)){
			  TestFileController.writeToLogFile("Successfully selected Name value on the Provost Binder Report Dialog", logFileWriter);
		  }
		  else TestFileController.writeToLogFile("Could not select Name value on the Provost Binder Report Dialog", logFileWriter);
		  SalarySettingManager.waitAndClick(By.id("buttonSubmit"));
		  TestFileController.copyFile (fileName, logFileWriter, logDirectoryName);

		  fileName = "Provost Binder List by Salary.xls" ;
		  if (SalarySettingManager.isDropdownAccessible (By.id("clusterLevel"), "Salary", logFileWriter)){
			  TestFileController.writeToLogFile("Successfully selected Salary value on the Provost Binder Report Dialog", logFileWriter);
		  }
		  else TestFileController.writeToLogFile("Could not select Salary value on the Provost Binder Report Dialog", logFileWriter);
		  SalarySettingManager.waitAndClick(By.id("buttonSubmit"));
		  TestFileController.copyFile (fileName, logFileWriter, logDirectoryName);
		  
		  Thread.sleep(3000);
		  logFileWriter.write("Closing the Provost Binder List Window");
		  logFileWriter.newLine();
		  UIController.driver.close();
	  }
	  
	  public static void downloadMasterSSW (String whichValues, BufferedWriter logFileWriter) throws Exception{
		  SalarySettingManager.switchWindowByURL(SalarySettingManager.salarySettingURL, logFileWriter);
		  String linkText = "Master Salary Setting Report";
		  String fileName = "Master Salary Setting Report.xls";
		  Thread.sleep(2000);
		  HandSOnTestSuite.salarySettingManager.runOneReportAtLocator(By.linkText(linkText), logFileWriter);
		  Thread.sleep(2000);
		  SalarySettingManager.getScreenShot(fileName, whichValues, logFileWriter);
		  TestFileController.copyFile (fileName, logFileWriter, logDirectoryName);
	  }//end downloadMasterSSW method
	  
	  public static void downloadSchoolSSW (String whichValues, BufferedWriter logFileWriter) throws Exception{
		  SalarySettingManager.switchWindowByURL(SalarySettingManager.salarySettingURL, logFileWriter);
		  String linkText = "School Salary Setting Worksheet";
		  String fileName = "School Salary Setting Worksheet.xlsx";
		  String partialURL = "https://handsonweb-"+HandSOnTestSuite.machineName+".stanford.edu/"+HandSOnTestSuite.envName+"/processSalSetWorksheetRequest.do?ACTION=goToWorkSheet&dto.level=school&dto.rptType=schoolSalSetWksh";
		  Thread.sleep(3000);
		  HandSOnTestSuite.salarySettingManager.runOneReportAtLocator(By.linkText(linkText), logFileWriter);
		  Thread.sleep(3000);
		  SalarySettingManager.switchWindowByURL(partialURL, logFileWriter);
		  Thread.sleep(3000);
		  SalarySettingManager.getScreenShot(fileName, whichValues, logFileWriter);
		  Thread.sleep(3000);
		  SalarySettingManager.waitAndClick(By.id("generate"));
		  TestFileController.copyFile (fileName, logFileWriter, logDirectoryName);
		  Thread.sleep(2000);
		  logFileWriter.write("Closing the School Salary Setting Worksheet Window");
		  logFileWriter.newLine();
		  UIController.driver.close();
	  }//end downloadSchoolSSW method
	  
	  public static void downloadChairSSWs(String[] cluster, String whichValues, BufferedWriter logFileWriter) throws Exception{
		  //get back to the original window
		  SalarySettingManager.switchWindowByURL(SalarySettingManager.salarySettingURL, logFileWriter);
		  String baseFileName = "Chair Salary Setting Worksheet";
		  HandSOnTestSuite.salarySettingManager.runOneReportAtLocator(By.linkText("Chair Salary Setting Worksheet"), logFileWriter);
		  SalarySettingManager.switchWindowByURL(SalarySettingManager.chairReportDialogURL, logFileWriter);
		  String fileExtension = ".xlsx";
		  // now switch to the dialog - the URL is processSalSetWorksheetRequest.do?ACTION=goToWorkSheet&dto.level=deptchair&dto.rptType=deptChairSalSetWksh
		  Thread.sleep(3000);//give it a couple of seconds once it's got the window
		  for (int index=0; index < cluster.length; index++){
			  String message = "";
			  String fileName = baseFileName+ " " + cluster[index] + fileExtension;
			  int iterations = 0;
			  while ((! SalarySettingManager.isDropdownAccessible (By.name("dto.clusterCd"), cluster[index] + " Cluster", logFileWriter)) && (iterations<10)){
				  iterations++;
				  Thread.sleep(3000);//give it a few seconds to load
				  message = "Could not select Cluster value " + cluster[index] + " for Chair Salary Setting Worksheet - re-trying - iteration #"+iterations+";\n ";
				  System.out.println(message);
				  TestFileController.writeToLogFile(message, logFileWriter);
				  if (iterations == 10) {
					  message = "ERROR - Could not select the Cluster value - aborting method;\n ";
					  System.out.println(message);
					  TestFileController.writeToLogFile(message, logFileWriter);
					  logFileWriter.close();
					  fail (message);
					  return;
				  }//end if
			  }//end while - the Cluster value could be successfully selected in the drop down
			  
			  message = "Successfully selected Cluster value " + cluster[index];
			  Thread.sleep(3000);// give it a few seconds once the menu item is selected
			  if (SalarySettingManager.waitAndClick(By.id("generate")))
				  message = message.concat(", and successfully clicked Generate button");
			  else message = message.concat(", but could not click Generate button");
			  SalarySettingManager.getScreenShot(fileName, whichValues, logFileWriter);
			  System.out.println(message);
			  TestFileController.writeToLogFile(message, logFileWriter);
			  boolean isGenerated = false;
			  while (! isGenerated) {
				  try {
					  TestFileController.copyFile (fileName, logFileWriter, logDirectoryName);
					  Thread.sleep(3000);
					  isGenerated = true;				  
				  }//end try
				  catch(FileNotFoundException e) {
					  if (SalarySettingManager.waitAndClick(By.id("generate")))
						  message = message.concat("Successfully clicked Generate button");
					  else message = message.concat("Could not click Generate button");
				  }//end catch
			  }//end while
		  }//end for loop
		  Thread.sleep(3000);
		  logFileWriter.write("Closing the Chair Salary Setting Worksheet Window");
		  logFileWriter.newLine();
		  UIController.driver.close();
	  }//end  method
	  
	  public static void downloadDeanSSW (String whichValues, BufferedWriter logFileWriter) throws Exception{
		  //now, run the Dean Salary Setting Worksheet
		  SalarySettingManager.switchWindowByURL(SalarySettingManager.salarySettingURL, logFileWriter);
		  String linkText = "Dean Salary Setting Worksheet";
		  String fileName = "Dean Salary Setting Worksheet.xlsx";
		  HandSOnTestSuite.salarySettingManager.runOneReportAtLocator(By.linkText(linkText), logFileWriter);
		  SalarySettingManager.getScreenShot(fileName, whichValues, logFileWriter);
		  TestFileController.copyFile (fileName, logFileWriter, logDirectoryName);

	  }//end downloadDeanSSW method
	  
	  public static void downloadClusterSSWs(String[] clusters, String whichValues, BufferedWriter logFileWriter) throws Exception{
		  //get back to the original window
		  SalarySettingManager.switchWindowByURL(SalarySettingManager.salarySettingURL, logFileWriter);
		  String baseFileName = "Cluster Salary Setting Worksheet";
		  String fileExtension = ".xlsx";
		  HandSOnTestSuite.salarySettingManager.runOneReportAtLocator(By.linkText("Cluster Salary Setting Worksheet"), logFileWriter);
		  Thread.sleep(6000);
		  SalarySettingManager.switchWindowByURL(SalarySettingManager.clusterReportDialogURL, logFileWriter);
		  // now, wait for the Cluster selection dialog to appear - URL is as follows: https://handsonweb-"+HandSOnTestSuite.machineName+".stanford.edu/"+HandSOnTestSuite.envName+"/processSalSetWorksheetRequest.do?ACTION=goToWorkSheet&dto.level=cluster
		  for (int index=0; index < clusters.length; index++){
			  String message = "";
			  String fileName = baseFileName+ " " + clusters[index] + fileExtension;
			  Thread.sleep(3000);
			  if (SalarySettingManager.isDropdownAccessible (By.name("dto.clusterCd"), clusters[index] + " Cluster", logFileWriter)){
				  message = "Successfully selected Cluster value " + clusters[index];
				  if (SalarySettingManager.waitAndClick(By.id("generate")))
					  message = message.concat(", and successfully clicked Generate button");
				  else message = message.concat(", but could not click Generate button");
			  }//end if - the Cluster value could be successfully selected in the drop down
			  else {
				  message = "Could not select Cluster value " + clusters[index];
			  }//end else - the Cluster could not be selected in the drop down
			  System.out.println(message);
			  SalarySettingManager.getScreenShot(fileName, whichValues, logFileWriter);
			  TestFileController.writeToLogFile(message, logFileWriter);
			  TestFileController.copyFile (fileName, logFileWriter, logDirectoryName);
		  }//end for loop
		  Thread.sleep(2000);
		  logFileWriter.write("Closing the Cluster Salary Setting Worksheet Window");
		  logFileWriter.newLine();
		  UIController.driver.close();
	  }// end method
	  
	  public static void downloadDeptSSWs(String[] department, String whichValues, BufferedWriter logFileWriter) throws Exception{
		  //get back to the original window
		  
		  //before clicking on the next link, make sure that the right window has the focus.
		  String baseFileName = "Department Salary Setting Worksheet";
		  SalarySettingManager.waitForElementPresent(By.linkText(baseFileName));
		  
		  HandSOnTestSuite.salarySettingManager.runOneReportAtLocator(By.linkText(baseFileName), logFileWriter);
		  String fileExtension = ".xlsm";
		  for (int index=0; index < department.length; index++){
			  String fileName = baseFileName+ " " + department[index] + fileExtension;
			  Thread.sleep(6000);
			  SalarySettingManager.switchWindowByURL("https://handsonweb-"+HandSOnTestSuite.machineName+".stanford.edu/"+HandSOnTestSuite.envName+"/processSalSetWorksheetRequest.do?ACTION=goToWorkSheet&dto.level=dept&dto.rptType=deptSalSetWksh", logFileWriter);
			  Thread.sleep(3000);
			  if (SalarySettingManager.isDropdownAccessible (By.id("deptCd"), department[index], logFileWriter))
				  TestFileController.writeToLogFile("Successfully selected Department value " + department[index], logFileWriter);
			  else TestFileController.writeToLogFile("Could not select Department value " + department[index], logFileWriter);
			  Thread.sleep(3000);
			  SalarySettingManager.getScreenShot(fileName, whichValues, logFileWriter);
			  SalarySettingManager.waitAndClick(By.id("generate"));
			  TestFileController.copyFile (fileName, logFileWriter, logDirectoryName);
		  }//end for
		  Thread.sleep(2000);
		  logFileWriter.write("Closing the Department Salary Setting Worksheet Window");
		  logFileWriter.newLine();
		  UIController.driver.close();
	  }//end downloadDeptSSWs method
	  
	  public static void runControlSheetReportSuite (String whichValues, BufferedWriter logFileWriter) throws Exception{
		  String fileName = "Control Sheet Report Suite.xls";
		  UIController.switchWindowByURL(UIController.loadSalarySettingWindowURL, logFileWriter);
		  HandSOnTestSuite.salarySettingManager.switchToReportTab();
		  HandSOnTestSuite.salarySettingManager.runOneReportAtLocator(By.xpath("//td/div/a[4]"), logFileWriter);
		  SalarySettingManager.getScreenShot(fileName, whichValues, logFileWriter);
		  TestFileController.copyFile (fileName, logFileWriter, logDirectoryName);
		  TestFileController.writeToLogFile(fileName + " successfully processed", logFileWriter);
	  }//end method runControlSheetReportSuite

	  
	  public void runFacultyValuesTests() throws Exception{
			String[] facultyNames = HandSOnTestSuite.apFacultyValues.getUniqueFacultyNames();
			System.out.println("Names of Faculty members stored in Faculty Values Object are as follows:");
			for (int i=0;i<facultyNames.length; i++){
				System.out.print(facultyNames[i]);
				System.out.print(": " + HandSOnTestSuite.apFacultyValues.getFTEStringValueForFacultyName(facultyNames[i]) + "% FTE, ");
				System.out.print("and, as a number: " + HandSOnTestSuite.apFacultyValues.getPrimitiveFloatFTEForFacultyName(facultyNames[i]) + ", ");
				System.out.println(HandSOnTestSuite.apFacultyValues.getAPReferenceValueForFacultyName(facultyNames[i]) +" AP Reference Value");
			}//end for loop
			
			String[] departmentNames = HandSOnTestSuite.apFacultyValues.getUniqueDepartmentNames();
			for(int i=0; i<departmentNames.length; i++){
				System.out.print(departmentNames[i] + " Department is associated with the following Faculty members: ");
				facultyNames = HandSOnTestSuite.apFacultyValues.getFacultyNamesInDepartment(departmentNames[i]);
				for (int j=0; j<facultyNames.length; j++){
					System.out.print(facultyNames[j] + " (Dept Percent: " 
							+ HandSOnTestSuite.apFacultyValues.getStringDeptPercentForFacultyNameAndDept(facultyNames[j], departmentNames[i]) 
							+ ", as a number: " + HandSOnTestSuite.apFacultyValues.getPrimitiveFloatDeptPercentForFacultyNameAndDept(facultyNames[j], departmentNames[i])
							+ "); ");
				}//end inner for - list of faculty associated with that department
				System.out.println();
			}//end outer for - list of departments
			
			String[] clusterNames = HandSOnTestSuite.apFacultyValues.getUniqueClusterNames();
			for(int i=0; i<clusterNames.length; i++){
				System.out.print(clusterNames[i] + " Cluster is associated with the following Faculty members: ");
				facultyNames = HandSOnTestSuite.apFacultyValues.getFacultyNamesInCluster(clusterNames[i]);
				for (int j=0; j<facultyNames.length; j++){
					System.out.print(facultyNames[j] + "; ");
				}//end inner for - list of faculty associated with that cluster
				System.out.println();
			}//end outer for - list of clusters
		  
	  }//end method - runFacultyValuesTests()
	  
	  public void runReferenceValuesTests() throws Exception{
			ReferenceValues originals = new ReferenceValues(TestFileController.OriginalAPValuesWorksheetName);
			System.out.println(originals.getFacultyForAPType(" "));
			System.out.println("'" + originals.getFacultyForAPType("Reappointment")[0] +"'");
			System.out.println("'" + originals.getFacultyForAPType("Reappointment")[1] +"'");
			System.out.println(originals.getFacultyForAPType("Promotion to Tenure"));
			System.out.println(originals.getFloatAmountForAPType("Reappointment"));
			System.out.println(originals.getFloatAmountForAPType("Promotion to Tenure"));
			System.out.println(originals.getFloatAmountForAPType("Removal of \"Subject to PhD\""));
			try{
				System.out.println(originals.getFloatAmountForAPType(" "));
			}
			catch (NumberFormatException e){
				System.out.println("NumberFormatException thrown, as expected");
			}
			catch (ParseException p){
				System.out.println("ParseException thrown, as expected");
			}
			System.out.println(originals.getStringAmountForAPType("Reappointment"));
			System.out.println(originals.getStringAmountForAPType("Promotion to Tenure"));
			System.out.println(originals.getStringAmountForAPType("Removal of \"Subject to PhD\""));
			System.out.println(originals.getStringAmountForAPType(" "));
	  }//end method - runReferenceValuesTests()


}
