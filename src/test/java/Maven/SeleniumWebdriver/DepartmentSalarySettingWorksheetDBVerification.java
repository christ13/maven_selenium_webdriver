package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Properties;


import oracle.jdbc.OracleConnection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.internal.Coordinates;

public class DepartmentSalarySettingWorksheetDBVerification {

	//these are variables pertinent to the identification and reporting of this test
	private static String testEnv;
	private static String testName;
	private static String testDescription;
	private static String testCaseNumber;
	private static BufferedWriter logFileWriter;
	private static StringBuffer verificationErrors;
	private static final String[] DBColNames = {"ROOT_CLUSTER_NM", "ORG_NM", "NAME", "RANK_DESC",
		"APPT_TYPE", "FTE", "FINAL_PY_SALARY", "NAME", "RANK_DESC"};
	private static final int FTEDBColIndex = 5;
	private static final int FinalPYSalaryIndex = 6;
	private static final String FTEAdjRaiseAmtLocatorName = "FTEAdjRaiseTopRow";
	private static final String FTEAdjFinalCYOneHundredPctLocatorName = "FinalCYColumnTopRow";

	//these variables are related to the JDBC we use to get the data from the database
	protected static Connection myConnection;
	protected static int firstDataRow;

	@Before
	public void setUp() throws Exception {
		testEnv = "Test";
		testName = "Dept Salary Setting Tab DB Test";
		testDescription = "Verify DB Values in Salary Setting Tab - Departments";
		testCaseNumber = "052";
		
		String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		logFileWriter = TestFileController.getLogFileWriter(logFileName);
		logFileWriter.write("Now executing "+ testName);
		logFileWriter.newLine();
		verificationErrors = new StringBuffer();
	}//end setUp method

	@After
	public void tearDown() throws Exception {
		if (myConnection != null) myConnection.close();
		TestFileController.writeTestResults(testEnv, testCaseNumber, testName, testDescription, verificationErrors);
		logFileWriter.newLine();
		logFileWriter.close();
	}//end tearDown method

	@Test
	public void test() throws Exception{
		myConnection = getConnection(logFileWriter);
		if (myConnection == null) {
			String errMsg = "Could not establish the database connection";
			logFileWriter.write(errMsg);
			logFileWriter.newLine();
			fail(errMsg);
		}//end if
		runStoredProcedure(myConnection, logFileWriter);
		String[] depts = this.getDepts(myConnection, logFileWriter);
		Thread.sleep(3000);
		UIController.switchWindowByURL("processSalarySetting.do", logFileWriter);
		HandSOnTestSuite.salarySettingTabUI.switchToSalarySettingTab();
		Thread.sleep(3000);
		//for (int i=0; i<depts.length; i++){
		for (int i=0; i<2; i++){
			logFileWriter.write("Cluster-Department combo found: " + depts[i]);
			logFileWriter.newLine();
			String[] config = depts[i].split(";");
			testOneDepartment(config[0], config[1], config[2], logFileWriter);
		}//end for loop - iterating through the departments
	}//end test method
	
	public void testOneDepartment(String clusterName, String deptName, String deptCode, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("*** method testOneDepartment called ***");
		logFileWriter.newLine();
		String message = new String();
		String[][] expectedDBValues = getDBValuesForOneDept(myConnection, deptCode, logFileWriter);
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("Expected values derived from the DB are as follows:");
		logFileWriter.newLine();
		for (int j=0; j<expectedDBValues.length; j++){
			for (int k=0; k<expectedDBValues[j].length; k++){
				logFileWriter.write(expectedDBValues[j][k]);
				if (k<(expectedDBValues[j].length -1))
				logFileWriter.write(", ");
			}//end inner for loop - iterating through all the columns for the faculty member
			logFileWriter.newLine();
		}//end outer for loop - iterating through all of the faculty members
				
		verificationErrors.append(HandSOnTestSuite.salarySettingTabUI.adjustSalarySettingConfiguration(clusterName, 
				deptName, logFileWriter));

		//this may be the point where the two alerts come up - this should dismiss them.
		Thread.sleep(3000);
		HandSOnTestSuite.salarySettingTabUI.acceptTwoAlerts(logFileWriter);
		
		if (! HandSOnTestSuite.salarySettingTabUI.selectReportConfiguration(clusterName, deptName, logFileWriter)){
			message = "Could not select the report configuration with the following values: " 
							+ clusterName + ", " + deptName;
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
		}//end if
		else {
			logFileWriter.write("Successfully selected the report configuration with the following values: "
					+ clusterName + ", " + deptName);
			logFileWriter.newLine();
		}//end else
		
		/*
		 * This is going to iterate through all of the columns of the UI and verify the
		 * value.  It is also going to have to iterate through all of the rows and verify
		 * the value on each row for each column.
		 * 
		 * We have stored the locators for the top rows of each column in an Excel file,
		 * "Input Data.xls", in the "Salary Setting Tab" tab.  
		 * 
		 * We also have to derive the row numbers, starting with "2" as the second row.  
		 * The top row with an implied value of "1" simply disregards row values.
		 * 
		 * The simplest way to do this is to get the data for one department, then iterate
		 * through the rows of data, adding the substring [##] - ## being the row # 
		 * - to the "tr" substring, IF the row number is "2" or greater.  Otherwise, the
		 * locator for that row remains as it is.
		 * 
		 */
		
		//this has to be reset to "false" each and every time we get a new department.
		boolean dynamicIndicesUpdated = false;
		for (int h = 0; h<expectedDBValues.length; h++){
			String[] names = new String[9];
			String[] locators = new String[names.length];
			String[] facultyData = new String[names.length];
			for (int i=0; i< locators.length; i++){
				//first, we have to establish what the column names are and get the locator values for the columns
				names[i] = HandSOnTestSuite.salarySettingTabUI.getLocatorNameFromRowNumber(i+2, logFileWriter);
				locators[i] = HandSOnTestSuite.salarySettingTabUI.getLocator(names[i], h+1, logFileWriter);
				
				HandSOnTestSuite.salarySettingTabUI.scrollTextIntoView(locators[i], logFileWriter);
				UIController.waitForElementPresent(By.xpath(locators[i]));
												
				facultyData[i] = UIController.driver.findElement(By.xpath(locators[i])).getText();
				if (i==0){
					facultyData[i] = facultyData[i].substring(0,1);
					expectedDBValues[h][i] = expectedDBValues[h][i].substring(0,1);
				}//end if
				logFileWriter.write("Faculty Data derived from the UI is : "+ facultyData[i]);
				logFileWriter.newLine();
				String errorMsg = UIController.verifyText(By.xpath(locators[i]), facultyData[i]);
				if (errorMsg.length() > 0) {
					verificationErrors.append(errorMsg);
					logFileWriter.write("MISMATCH - text is not as expected: "+errorMsg);
					logFileWriter.newLine();
				}//end if
				if (i==4){
					facultyData[i] = facultyData[i].substring(0,1);
					expectedDBValues[h][i] = expectedDBValues[h][i].substring(0,1);
				}//end if
				else{
					if (facultyData[i].equalsIgnoreCase(expectedDBValues[h][i])){
						logFileWriter.write("Expected Text is present: " + expectedDBValues[h][i]);
					}//end inner if
					else{
						logFileWriter.write("MISMATCH: Expected value '" + expectedDBValues[h][i] 
								+"', but what's actually there is '"+ facultyData[i] + "'");
					}//end inner else
				}//end outer else
				logFileWriter.newLine();
				if (i > 0 && (! dynamicIndicesUpdated)){
					HandSOnTestSuite.salarySettingTabUI.updateDynamicIndices(logFileWriter);
					dynamicIndicesUpdated = true;
				}//end if
			}//end inner for - iterating through one row of the department
			String UIRaiseAmtLocator = HandSOnTestSuite.salarySettingTabUI.getLocator(FTEAdjRaiseAmtLocatorName, h+1, logFileWriter);
			String UIRaiseAmtString = HandSOnTestSuite.salarySettingTabUI.getTextFromElement(UIRaiseAmtLocator, logFileWriter);
			int UIRaiseAmt = 0;
			int fte = 100;
			int expectedCYSalary = 0;
			String FTEString = facultyData[FTEDBColIndex];
			UIRaiseAmt = getIntFromString(UIRaiseAmtString, logFileWriter);
			fte = getIntFromString(FTEString, logFileWriter);
			expectedCYSalary = getIntFromString(facultyData[FinalPYSalaryIndex], logFileWriter);
			if (isNotInvalid(fte, -1, logFileWriter) 
						&& isNotInvalid(expectedCYSalary, -1, logFileWriter) 
						&& isNotInvalid(UIRaiseAmt, -1, logFileWriter)){
				if (fte < 100){
					long UIRaiseNumerator = UIRaiseAmt *100;
					float UIRaiseFloatAmt = UIRaiseNumerator/fte;
					UIRaiseAmt = (int)UIRaiseFloatAmt;
				}//end if - FTE is less than 100
			}//end if - it's not invalid
			else {
				logFileWriter.write("Raise will not be adjusted - FTE = " +fte 
						+", and existing Salary is " + expectedCYSalary 
						+", and Raise amount is " + UIRaiseAmt);
				logFileWriter.newLine();
			}
			if ((expectedCYSalary != -1) && (UIRaiseAmt != -1)){
				expectedCYSalary+=UIRaiseAmt;
				logFileWriter.write("*** Raise WILL be applied - FTE = " +fte 
						+", and existing Salary is " + expectedCYSalary 
						+", and Raise amount is " + UIRaiseAmt +" ***");
				logFileWriter.newLine();
			}
			else{
				logFileWriter.write("Existing Salary " + expectedCYSalary 
						+" will not be adjusted, Raise amount is " + UIRaiseAmt);
				logFileWriter.newLine();
			}
			String CYSalaryStringLocator = HandSOnTestSuite.salarySettingTabUI.getLocator(FTEAdjFinalCYOneHundredPctLocatorName, h+1, logFileWriter);
			String CYSalaryString = HandSOnTestSuite.salarySettingTabUI.getTextFromElement(CYSalaryStringLocator, logFileWriter);
			int CYSalary = getIntFromString(CYSalaryString, logFileWriter);
			if (isNotInvalid(CYSalary, -1, logFileWriter)){
				if (expectedCYSalary == CYSalary){
					logFileWriter.write("Expected value for CY Salary is present: " + expectedCYSalary);
					logFileWriter.newLine();
				}//end inner if
				else{
					logFileWriter.write("MISMATCH: Expected CY Salary " + expectedCYSalary 
							+"', but what's actually there is "+ CYSalary);
					logFileWriter.newLine();
				}//end inner else
			}
			//just in case another user was working on it - we want the latest version - refresh to get the latest values
			Thread.sleep(3000);
			UIController.driver.navigate().refresh();
			Thread.sleep(10000);
			try {
			UIController.driver.switchTo().alert().accept();
			}//end try clause
			catch(Exception e) {
				logFileWriter.write("Could not acquire the alert);");
				logFileWriter.newLine();
			}//end catch clause
		}//end outer for - iterating through all of the rows of the department
		logFileWriter.write("*** method testOneDepartment finished ***");
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.newLine();
	}//end testOneDepartment method
	
	protected boolean isNotInvalid (int actual, int invalid, BufferedWriter logFileWriter) throws Exception{
		boolean isNotInvalid = false;
		if (actual != invalid){
			logFileWriter.write("Actual value " + actual +" is not invalid value "+invalid);
			logFileWriter.newLine();
			isNotInvalid = true;
		}
		else {
			logFileWriter.write("*** Actual value " + actual +" IS invalid value "+invalid +" ***");
			logFileWriter.newLine();
		}
		return isNotInvalid;
	}
	
	protected int getIntFromString(String toBeFormatted, BufferedWriter logFileWriter) throws Exception{
		int formatted = -1;
		try{
			formatted = Integer.parseInt(toBeFormatted.replaceAll(",", ""));
		}
		catch (NumberFormatException e){
			logFileWriter.write("Could not format String '" + toBeFormatted +"' to a number"
					+", because of Exception " + e.getMessage());
			logFileWriter.newLine();			
		}
		return formatted;
	}
	

	
	protected Connection getConnection(BufferedWriter logFileWriter) throws Exception{
		Connection conn = null;
		try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            String url = "jdbc:oracle:thin:@//handsondb-tst5.stanford.edu:1568/HONTST";

            Properties props = new Properties();
			props.setProperty("user", "hsondev_test");
			props.setProperty("password", "Nv8QJM5uK4WIXEVk");
			props.setProperty("url", url);
			props.setProperty(OracleConnection.CONNECTION_PROPERTY_THIN_NET_CONNECT_TIMEOUT, "20000");


            System.out.println("Test " + testName + " running: getting connection at time " +System.currentTimeMillis());
            conn = DriverManager.getConnection(url, props);
            System.out.println("Test " + testName + " running: finished getting connection at time " +System.currentTimeMillis());
            logFileWriter.newLine();
		}//end try
	    catch (Exception e) {
            System.out.println("Test " + testName + " running: failed getting connection at time " +System.currentTimeMillis());
	        String errMsg = "The Exception thrown was "+e.getMessage();
	        logFileWriter.write(errMsg);
	        logFileWriter.newLine();
	        errMsg = "The cause of the Exception was the following "+e.getCause().toString();
	        logFileWriter.write(errMsg);
	        logFileWriter.newLine();
	        
	        StackTraceElement[] stackTrace = e.getStackTrace();
	        for (int i=0; i<stackTrace.length; i++){
	        	logFileWriter.write(stackTrace[i].toString());
	        	logFileWriter.newLine();
	        }//end for loop
	    }//end catch clause
		finally{
			if (conn == null)
				logFileWriter.write("method getConnection is returning null Connection");
			else logFileWriter.write("Connected to database");
			logFileWriter.newLine();
		}
		return conn;		
	}//end getConnection method

	protected void runStoredProcedure(Connection conn, BufferedWriter logFileWriter) throws Exception{
		CallableStatement storedProc = null;
		try{
	        storedProc = conn.prepareCall("{call TEST_SALSET_ELIG(?, ?, ?)}");
	        storedProc.setString(1, "2017");
	        storedProc.setString(2, "04/01/2017");
	        storedProc.setString(3, null);
	        System.out.println("Test " + testName + " running: executed running stored procedure at time " +System.currentTimeMillis());
	        storedProc.execute();
            System.out.println("Test " + testName + " running: finished running stored procedure at time " +System.currentTimeMillis());
	        logFileWriter.write("Stored procedure successfully executed");
	        logFileWriter.newLine();
		}//end try block
	    catch (SQLException e) {
	    	System.out.println("Test " + testName + " running: failed running stored procedure at time " +System.currentTimeMillis());
	        StackTraceElement[] stackTrace = e.getStackTrace();
	        for (int i=0; i<stackTrace.length; i++){
	        	logFileWriter.write(stackTrace[i].toString());
	        	logFileWriter.newLine();
	        }//end for loop
	        throw e;
	    }//end catch clause
		finally{
			if (storedProc != null) storedProc.close();
            logFileWriter.newLine();
		}//end finally clause
	}//end runStoredProcedure method 
	
	private String[] getDepts(Connection conn, BufferedWriter logFileWriter) throws Exception{
		String[] depts = null;
		Statement stmt = null;
		try{
	        stmt=conn.createStatement();
	        String query = "SELECT DISTINCT ROOT_CLUSTER_NM, ORG_NM, ORG_CD FROM SALARY_SETTING_TEST "
	        			+"ORDER BY ROOT_CLUSTER_NM, ORG_NM ";
	
	        //get the results set
	        ResultSet rs=stmt.executeQuery(query);  
	        ArrayList<String> list = new ArrayList<String>();
	        while(rs.next()){
	        	String cluster_dept = rs.getString("ROOT_CLUSTER_NM") + ";" 
	        			+ rs.getString("ORG_NM") + ";" + rs.getString("ORG_CD");
	        	list.add(cluster_dept);
	        	logFileWriter.write("Adding cluster-dept-code " + cluster_dept +" to the list");
	        	logFileWriter.newLine();
	        }//end while loop
	        if (! list.isEmpty()){
	        	depts = new String[list.size()];
	        	for (int i=0; i<depts.length; i++){
	        		depts[i] = (String)list.get(i);
	        	}//end for
	        }//end if
		}//end try clause
	    catch (SQLException e) {
	        StackTraceElement[] stackTrace = e.getStackTrace();
	        for (int i=0; i<stackTrace.length; i++){
	        	logFileWriter.write(stackTrace[i].toString());
	        	logFileWriter.newLine();
	        }//end for loop
	    }//end catch clause
		finally{
			if (stmt != null) stmt.close();
		}//end finally clause
		if (depts == null){
			logFileWriter.write("returning null list of departments");
			logFileWriter.newLine();
		}
		return depts;
	}//end getDepts method
	
	private String[][] getDBValuesForOneDept(Connection conn, String deptCode, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method getDBValuesForOneDept called ***");
		logFileWriter.newLine();
		Statement stmt = null;
		ResultSet rs=null;
        ArrayList<String[]> expectedValuesArray = new ArrayList<String[]>();
		try {
	        stmt=conn.createStatement();
	        String query = "SELECT * FROM SALARY_SETTING_TEST "
	        		+"WHERE ORG_CD = '" + deptCode + "' "
	        		+"ORDER BY SORT_ORDER, RANK_DESC, NAME";
	        logFileWriter.write("Executing the following query to get expected DB values: ");
	        logFileWriter.newLine();
	        logFileWriter.write(query);
	        logFileWriter.newLine();
	        rs=stmt.executeQuery(query);
	        ResultSetMetaData rsmd = rs.getMetaData();
	        int columnCount = rsmd.getColumnCount();
	        logFileWriter.write("Number of columns derived from the database is " + columnCount);
	        logFileWriter.newLine();
	        logFileWriter.write("Column names: types from the database are as follows: ");
	        for (int i=1; i<=columnCount; i++){
	        	logFileWriter.write(rsmd.getColumnName(i) + ": " + rsmd.getColumnTypeName(i));
	        	if (i<columnCount) logFileWriter.write(", ");
	        }//end for loop
	        logFileWriter.newLine();
			while (rs.next()){
				
				String expectedValues = new String();
				for (int i=0; i<DBColNames.length; i++){
					String expectedDBValue = getExpectedValue(rs, i);
					expectedValues = expectedValues.concat(expectedDBValue);
					if (i<DBColNames.length - 1){
						expectedValues = expectedValues.concat(";");
					}//end if
				}//end for loop
				expectedValuesArray.add(expectedValues.split(";"));
			}//end while loop
			logFileWriter.newLine();
		}//end try clause
	    catch (SQLException e) {
	        StackTraceElement[] stackTrace = e.getStackTrace();
	        for (int i=0; i<stackTrace.length; i++){
	        	logFileWriter.write(stackTrace[i].toString());
	        	logFileWriter.newLine();
	        }//end for loop
	    }//end catch clause
		finally{
			if (stmt != null) stmt.close();
		}//end finally clause
		String[][] expectedValues = new String[expectedValuesArray.size()][DBColNames.length];
		for(int i=0; i<expectedValuesArray.size(); i++)
			expectedValues[i] = expectedValuesArray.get(i);
		return expectedValues;
	}//end getDBValues method
	
	
	private String getExpectedValue(ResultSet rs, int dataIndex) throws Exception{
		String expectedValue = new String();
		switch (dataIndex){
		case 0: expectedValue = rs.getString(DBColNames[0]); break;
		case 1: expectedValue = rs.getString(DBColNames[1]);break;
		case 2: expectedValue = rs.getString(DBColNames[2]);break;
		case 3: expectedValue = rs.getString(DBColNames[3]);break;
		case 4: expectedValue = rs.getString(DBColNames[4]);break;
		case 5: expectedValue = Integer.toString(rs.getInt(DBColNames[5]));break;//+"%"
		case 6: NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
				int py_salary = rs.getInt(DBColNames[6]);
				expectedValue = numberFormat.format(py_salary);
				break;
		case 7: expectedValue = rs.getString(DBColNames[7]);break;
		case 8: expectedValue = rs.getString(DBColNames[8]);break;
		}//end switch block
		
		return expectedValue;
	}//end getExpectedValue method



}//end Class
