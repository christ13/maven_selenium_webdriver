package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ControlSheetPivotHumArtsClusterReportASCTest extends
		ControlSheetPivotOneClusterReportASCTest {

	@Before
	public void setUp() throws Exception {
		clusterName = "Humanities and Arts Cluster";

		mySheetName = TestFileController.ControlSheetPivot_HumArtsWorksheetName;
		testName = "ControlSheetPivotHumArtsClusterReportASCTest";
		testCaseNumber = "030";
		testDescription = "ASC Test of Pivot HumArts Cluster Report";
		logFileName = "Control Sheet Pivot HumArts Cluster ASC Test.txt";
		
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void test() throws Exception{
		super.test();
	}

}
