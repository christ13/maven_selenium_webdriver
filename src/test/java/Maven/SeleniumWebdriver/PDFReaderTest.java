package Maven.SeleniumWebdriver;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.text.PDFTextStripper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class PDFReaderTest {

	@Test
	public void readPDFTest() throws IOException{
		
		String urlString = "file:///C:/Users/christ13/Documents/HandSon/Bagwell,Kyle_SabbaticalPrintView.pdf";
		WebDriver driver = new ChromeDriver();
		driver.get(urlString);
		
		System.out.println("URL being worked with is "+urlString);
		System.out.println();
		System.out.println();
		System.out.println();
		
		URL url = new URL(urlString);
		InputStream input = url.openStream();
		BufferedInputStream parsedPDF = new BufferedInputStream(input);
		PDDocument PDFtoRead = null;
		
		PDFtoRead = PDDocument.load(parsedPDF);
		String pdfContent = new PDFTextStripper().getText(PDFtoRead);
		System.out.print(pdfContent);
		
		PDFtoRead.close();
	}//end readPDFTest method
}
