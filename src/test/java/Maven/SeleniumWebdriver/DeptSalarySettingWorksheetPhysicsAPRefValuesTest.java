/**
 * 
 */
package Maven.SeleniumWebdriver;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author christ13
 *
 */
public class DeptSalarySettingWorksheetPhysicsAPRefValuesTest extends
		DeptSalarySettingWorksheetAPRefValuesTest {

	@Before
	public void setUp() throws Exception {
		departmentName = "Physics";
		myExcelReader = TestFileController.DepartmentSalarySettingPhysicsReport;
		worksheetName = TestFileController.DepartmentSalarySettingWorksheetName;
		myExcelReader.setRowForColumnNames(TestFileController.DepartmentSalarySettingWorksheetHeaderRow);
		testEnv = "Test";
		testName = "Test Department Salary Setting Worksheet " + departmentName;
		testDescription = "Verify AP Values in Department Salary Setting Worksheet " + departmentName;
		testCaseNumber = "013";
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void test() throws Exception{
		super.test();
	}

}
