package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.NumberFormat;
import java.util.Locale;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MasterSalarySettingReportDBVerificationTest {

	private static String worksheetName = TestFileController.MasterSalarySettingReportWorksheetName;
	private static SSExcelReader myExcelReader;

	private static String testName;
	private static String testCaseNumber;
	private static String testDescription;
	private static String testEnv;
	private static StringBuffer verificationErrors;
	private static final String[] sheetColumns = {"Employee ID", "Name",
		"Cluster", "Department", "Rank", "Appt Type", "Dept FTE",
		"Final " + HandSOnTestSuite.PY + " Salary", "Gender", "Ethnicity"};


	@Before
	public void setUp() throws Exception {
		myExcelReader = TestFileController.MasterSalarySettingReport;
		testEnv = "Test";
		testName = "Master Salary Setting Report DB Test";
		testDescription = "Verify DB Values in Master Salary Setting Report";
		testCaseNumber = "048";
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws Exception {
		verificationErrors = new StringBuffer();
		String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		BufferedWriter logFileWriter = TestFileController.getLogFileWriter(logFileName);
		logFileWriter.write("Now executing "+ testName);
		logFileWriter.newLine();
		
		callStoredProcedure(logFileWriter);
		testTableData(logFileWriter);
		
		logFileWriter.close();
		TestFileController.writeTestResults(testEnv, testCaseNumber, testName, testDescription, verificationErrors);
	}//end test method
	
	private void callStoredProcedure(BufferedWriter logFileWriter) throws Exception {
		Connection conn = null;
		CallableStatement storedProc = null;
		try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            String url = "jdbc:oracle:thin:@//handsondb-tst4.stanford.edu:1568/HON_TST";
            conn = DriverManager.getConnection(url, "hsondev_test", "skyline20160708");
            logFileWriter.write("Connected to database");
	        logFileWriter.newLine();
	        storedProc = conn.prepareCall("{call TEST_SALSET_ELIG(?, ?, ?)}");
	        storedProc.setString(1, "2017");
	        storedProc.setString(2, "04/01/2017");
	        storedProc.setString(3, null);
	        storedProc.execute();
	        logFileWriter.write("Stored procedure successfully executed");
	        logFileWriter.newLine();
	    } // end try clause
	    catch (SQLException e) {
	        StackTraceElement[] stackTrace = e.getStackTrace();
	        for (int i=0; i<stackTrace.length; i++){
	        	logFileWriter.write(stackTrace[i].toString());
	        	logFileWriter.newLine();
	        }//end for loop
	    }//end catch clause
		finally{
			if (storedProc != null) storedProc.close();
			if (conn != null) conn.close();
		}
	}//end callStoredProcedure method
	
	private void testTableData(BufferedWriter logFileWriter) throws Exception{
		Connection conn = null;
		Statement stmt = null;
		try {
			//do the connection to the DB
	        Class.forName("oracle.jdbc.driver.OracleDriver");
	        String url = "jdbc:oracle:thin:@//handsondb-tst4.stanford.edu:1568/HON_TST";
	        conn = DriverManager.getConnection(url, "hsondev_test", "skyline20160708");
	        logFileWriter.write("Connected to database");
	        logFileWriter.newLine();

	        //query the database
	        stmt=conn.createStatement();
	        String query = "SELECT * FROM MASTER_SSW "
	        		+"ORDER BY ROOT_CLUSTER_NM, ORG_NM, SORT_ORDER, lower(NAME)";
	        ResultSet rs=stmt.executeQuery(query);  

	        //get the results set metadata and get information on the table
	        ResultSetMetaData rsmd = rs.getMetaData();
	        int columnCount = rsmd.getColumnCount();
	        logFileWriter.write("Number of columns derived is " + columnCount);
	        logFileWriter.newLine();
	        logFileWriter.write("Column names: types are as follows: ");
	        for (int i=1; i<=columnCount; i++){
	        	logFileWriter.write(rsmd.getColumnName(i) + ": " + rsmd.getColumnTypeName(i));
	        	if (i<columnCount) logFileWriter.write(", ");
	        }//end for loop
	        logFileWriter.newLine();
	        
	        //now, retrieve the data and compare it to what's in the Master Salary Setting Worksheet
	        int sheetRow = 6;
	        while (rs.next()){
	        	//now, repeatedly use getCellData(String sheetName,String colName,int rowNum) in the ExcelReader
	        	for (int i=0; i<sheetColumns.length; i++){
	        		String actual = myExcelReader.getCellData(worksheetName, sheetColumns[i], sheetRow);
	        		String expected = getExpectedValue(rs, i);
	        		
	        		if (! isAsExpected(expected, actual, logFileWriter))
	        			System.out.println("MISMATCH: Expected value is " + expected 
	        					+ ", but actual value is "+ actual);
	        	}//end for loop
	        	sheetRow++;
	        }//end while loop
		}//end try clause
	    catch (SQLException e) {
	        StackTraceElement[] stackTrace = e.getStackTrace();
	        for (int i=0; i<stackTrace.length; i++){
	        	logFileWriter.write(stackTrace[i].toString());
	        	logFileWriter.newLine();
	        }//end for loop
	    }//end catch clause
		finally{
			if (stmt != null) stmt.close();
			if (conn != null) conn.close();
		}//end finally clause

	}//end getTableData method
	
	private String getExpectedValue(ResultSet rs, int sheetColumnIndex) throws Exception{
		String expectedValue = new String();
		switch (sheetColumnIndex){
		case 0: expectedValue = rs.getString("ENTERPRISE_PERSON_ID"); break;
		case 1: expectedValue = rs.getString("NAME"); break;
		case 2: expectedValue = rs.getString("ROOT_CLUSTER_NM");break;
		case 3: expectedValue = rs.getString("ORG_NM");break;
		case 4: expectedValue = rs.getString("RANK_DESC");break;
		case 5: expectedValue = rs.getString("APPT_TYPE");break;
		case 6: expectedValue = Integer.toString(rs.getInt("FTE"));break;
		case 7: NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
				int salary = rs.getInt("FINAL_PY_SALARY");
				expectedValue = numberFormat.format(salary);
				break;
		case 8: expectedValue = rs.getString("GENDER");break;
		case 9: expectedValue = rs.getString("ETHNICITY_NAME");break;
		}//end switch block
		
		return expectedValue;
	}//end getExpectedValue method
	
	private boolean isAsExpected(String expected, String actual, 
			BufferedWriter logFileWriter) throws Exception{
		boolean match = false;
		if ((expected == null || expected.isEmpty()) 
							&& (actual == null || actual.isEmpty())) match = true;
		else if (expected.equalsIgnoreCase(actual)) 
			match = true;
		else {
			String message = "MISMATCH: Expected value is " + expected
							+ ", but actual value is "+ actual;
			logFileWriter.write(message);
			verificationErrors.append(message);
		}
		
		return match;
	}

}//end MasterSalarySettingReportDBVerificationTest Class
