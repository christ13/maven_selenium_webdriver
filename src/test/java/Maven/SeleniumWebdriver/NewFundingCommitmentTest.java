package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;

import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;


/*
 * The goal of this test Class is to instantiate the new Funding Commitments
 * and verify that they have the data as expected.  
*/

public class NewFundingCommitmentTest {
	public static final String dummyPTA = "9999999-1-ZZZZZ";
	public static final String provostFundedHousingLoanPTA = "1003484-1-BADPB";
	protected static String testName;
	protected static String testCaseNumber;
	protected static String testDescription;
	protected static String testEnv;
	protected static StringBuffer verificationErrors;
	protected BufferedWriter logFileWriter;
	protected static String[] IDStrings;
	protected static final String author = "Chris McFadden";
	protected static String currentDate = "";
	public static final String currentStatus = "Status:  DRAFT / PENDING";
	public static final String archiveStatus = "Status:  ARCHIVE / APPROVED";
	protected static String uploadedFileName = "School Salary Setting Worksheet.xlsx";
	protected static final String uploadedFilePath = "C:\\Users\\christ13\\Documents\\HandSOn\\"+uploadedFileName;
	protected static String attachmentText = "Attachment";
	protected static String noteText = "New Note";
	protected static String descriptionText = "Description";
	protected static String dataSourceText = "Data Source";
	protected static String[] categories = {"Retention", "Recruitment", "Other Teaching"};
	protected static String dataSourceDate = "09/01/2016";
	protected static final String myWorksheetName = "Commitment Page";
	protected static String[] departments = {"Physics", "Mathematics", "Religious Studies"};
	//"Fetter, Alexander", "Cohen,Ralph", "Bashir,Shahzad"
	protected static String[] faculty = {"Irwin,Kent", "Conrad,Brian", "Kieschnick,John"};
	protected static String[] subcategories = {"Staff Support", "Rental Assistance", "Replacement Teaching"};
	protected static String[] secondRowSubcategories = {"Summer 9ths", "Computer", "Lecturers"};
	//protected static String SummerNinths = "Summer 9ths";
	protected static String[] expenseAreas = {"Cont Salary", "RBE Salary", "Tuition"};
	protected static String[] sourcePTAs = {"1005940-1-BADQV", "1065356-1-DADCJ", "1028975-1-AABNZ"};
	//"1132140-1-FZBNQ, "1057698-1-EALIX", "1124249-1-FZADR"
	protected static String[] destPTAs = {"1164171-1-DCJTM", "1154363-1-FZBSI", "1154363-1-FZBSI"};
	protected static String[] commitmentNumbers = new String[faculty.length];
	protected static String[] transferDates = {"09/01/2016", "09/01/2017", "09/03/2017"};
	protected static String[] iBudgetNumbers = {"iBudget7", "iBudget8", "iBudget9"};
	protected static String[] iJournalNumbers = {"iJournal7", "iJournal8", "iJournal9"};
	protected static String holdReason = "Next Yr";
	protected static String nextYr = "20"+HandSOnTestSuite.NY;
	protected static String thisYr = "20"+HandSOnTestSuite.FY;
	protected static String nextNextYr = "20"+HandSOnTestSuite.currentNextNextYear;
	protected static final String mismatchedPTAs = "Source and destination PTA of section Transfer Details: "
					+"The source and destination PTA combination must be the same";
	protected static final String wrongFundingUnit = "Source PTA of section Transfer Details: "
					+"The source PTA's Award Owning Org and the selected Funding Unit are mismatched.";
	
	protected ArrayList<CommitmentTransferLine> preSubmissionTransferLines;
	protected ArrayList<CommitmentTransferLine> postSubmissionTransferLines;

	@Before
	public void setUp() throws Exception {
		testName = "NewFundingCommitmentTest";
		testEnv = "Test";
		testCaseNumber = "066";
		testDescription = "Test of New Funding Commitments";
		verificationErrors = new StringBuffer();
		currentDate = getCurrentDate(logFileWriter);
		preSubmissionTransferLines = new ArrayList<CommitmentTransferLine>();
		postSubmissionTransferLines = new ArrayList<CommitmentTransferLine>();
	}//end setUp() method

	@After
	public void tearDown() throws Exception {
		TestFileController.writeTestResults(testEnv, testCaseNumber, testName, testDescription, verificationErrors);
		logFileWriter.newLine();
		logFileWriter.close();
	}//end tearDown() method

	@Test
	public void test() throws Exception{
		String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		logFileWriter = TestFileController.getLogFileWriter(logFileName);
		logFileWriter.write("New Funding Commitments will be tested");
		logFileWriter.newLine();
		for(int i = 0; i<faculty.length; i++){
			HandSOnTestSuite.startPageUI.openNewFundingCommitment(logFileWriter);
			Thread.sleep(3000);
			verifyNewFundingCommitmentInfo(logFileWriter, i);
			saveCommitment(logFileWriter, false);
			commitmentNumbers[i] = getCommitmentNumber(logFileWriter);
			adjustToSentTransfer(logFileWriter, i);
			saveCommitment(logFileWriter, true);
			adjustToDoneTransfer(logFileWriter, i);
			saveCommitment(logFileWriter, true);
			copyTransferLine(logFileWriter, i);
			deleteTransferLine(logFileWriter, false, i);
			Thread.sleep(3000);
			deleteTransferLine(logFileWriter, true, i);
			Thread.sleep(3000);
			insertNewTransferLine(logFileWriter, i, 2);
			insertNewTransferLine(logFileWriter, i, 3);
			insertNewTransferLine(logFileWriter, i, 4);
			insertNewTransferLine(logFileWriter, i, 5);
			insertNewTransferLine(logFileWriter, i, 6);
			insertNewTransferLine(logFileWriter, i, 7);
			insertNewTransferLine(logFileWriter, i, 8);
			insertNewTransferLine(logFileWriter, i, 9);
			insertNewTransferLine(logFileWriter, i, 10);


			//now, evaluate the Summer Ninths Totals
			evaluateAllTotalsSummerNinths(logFileWriter);
			
			
			//this tests HANDSON-3687

			testTransferLineSelection(logFileWriter, i);
			
			testErrorMessages_MissingSrcDesc(logFileWriter, i);
			testErrorMessages_WrongPTA(logFileWriter, i, 2);
			testErrorMessages_WrongPTA(logFileWriter, i, 3);
			testErrorMessages_WrongPTA(logFileWriter, i, 4);
			testErrorMessages_WrongPTA(logFileWriter, i, 5);
			testErrorMessages_WrongPTA(logFileWriter, i, 6);
			testErrorMessages_WrongPTA(logFileWriter, i, 7);
			testErrorMessages_WrongPTA(logFileWriter, i, 8);
			testErrorMessages_WrongPTA(logFileWriter, i, 9);
			testErrorMessages_WrongPTA(logFileWriter, i, 10);
			
			testErrorMessages_blankField(logFileWriter, i, 2);
			
			testSubmission(logFileWriter, i);
			testCommitmentAsPartOfFaculty(logFileWriter, i);
			searchForAndOpenCommitment(logFileWriter, "Open", i);
			

			verifyViewSummary(logFileWriter, i);
			verifyViewHistory(logFileWriter, i);
			printPDFContentsToLogFile(logFileWriter, "CommitmentHistoryDetailReport.pdf", i);

			verifyViewSnapshot(logFileWriter, i);
			printPDFContentsToLogFile(logFileWriter, "CommitmentSnapshotReport.pdf", i);

			testFYIRoute(logFileWriter, i);
			
			testCopyCommitment(logFileWriter, i);			
			testArchiveCommitment(logFileWriter, i);
			
			String[] URLs = {"showDocumentsTab.do"};
			evaluateAction("Close All Windows Except Operations and Salary Setting", 
					UIController.closeAllWindowsExceptURLs(URLs, logFileWriter), logFileWriter);
		}//end for loop
		testSearchAndExportMultipleTimes(logFileWriter);
	}//end test() method
	
	protected void evaluateAllTotalsSummerNinths(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("*** method evaluateAllTotalsSummerNinths called ***");
		logFileWriter.newLine();
		
		float totalSummerNinths = 0;
		float totalFundedSummerNinths = 0;
		float totalRemainingSummerNinths = 0;
		
		for (int row=2; row<=10; row++){
			//read Summer Ninths value
			CommitmentTransferLine lineToRead = new CommitmentTransferLine(row, logFileWriter);
			
			String SummerNinthsStringValue = lineToRead.getValueForField(CommitmentTransferLine.SummerNinths, logFileWriter);
			logFileWriter.write("Reading Summer Ninths value of "+SummerNinthsStringValue);
			logFileWriter.newLine();
			try {
				totalSummerNinths += Float.parseFloat(SummerNinthsStringValue);
			}//end try clause
			catch (NumberFormatException e) {
				logFileWriter.write("ERROR - we cannot parse "+SummerNinthsStringValue+" as a number... evaluating to zero");
				logFileWriter.newLine();
			}//end catch clause
			//evaluate whether or not the Transfer Status is Done and, if it is, add it to the total funded Summer Ninths
			if (lineToRead.getValueForField(CommitmentTransferLine.TransferStatus, logFileWriter).equalsIgnoreCase("Done")) {
				logFileWriter.write("The transfer status for line "+row+" is Done - adding "
						+SummerNinthsStringValue+" to Total Funded Summer Ninths");
				totalFundedSummerNinths += Float.parseFloat(SummerNinthsStringValue);
				logFileWriter.write("Total Funded Summer Ninths updated to "+totalFundedSummerNinths);
				logFileWriter.newLine();
			}//end if
			else if (lineToRead.getValueForField(CommitmentTransferLine.TransferStatus,  logFileWriter).equalsIgnoreCase("Hold")) {
				logFileWriter.write("The Transfer Status is 'Hold', so we are making sure the update is present by clicking on the Hold Reason");
				logFileWriter.newLine();
				String originalHoldReason = lineToRead.getValueForField(CommitmentTransferLine.holdReason, logFileWriter);
				Thread.sleep(2000);
				logFileWriter.write("The Original Hold Reason is '"+originalHoldReason+"';");
				logFileWriter.newLine();
				lineToRead.setValue(CommitmentTransferLine.holdReason, "select", logFileWriter);
				Thread.sleep(2000);
				lineToRead.setValue(CommitmentTransferLine.holdReason, holdReason, logFileWriter);
				Thread.sleep(2000);
			}//end else if - the Commitment Transfer line is in "Hold" status
			logFileWriter.write("Total Summer Ninths updated to "+totalSummerNinths);
			logFileWriter.newLine();
		}//end for loop
		
		evaluateOneTotalSummerNinths("Total Summer Ninths", totalSummerNinths, logFileWriter);
		evaluateOneTotalSummerNinths("Total Funded Summer Ninths", totalFundedSummerNinths, logFileWriter);
		//the Remaining Summer Ninths are all Summer Ninths which are not yet funded
		evaluateOneTotalSummerNinths("Total Remaining Summer Ninths", (totalSummerNinths-totalFundedSummerNinths), logFileWriter);

	}// end evaluateTotalSummerNinths method
	
	protected void evaluateOneTotalSummerNinths(String nameOfTotal, float expectedTotal, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("* Now evaluating the expected and actual "+nameOfTotal+" values *");
		logFileWriter.newLine();

		logFileWriter.write("Expected "+nameOfTotal+" for this Commitment is "+expectedTotal);
		logFileWriter.newLine();
		Thread.sleep(3000);
		String actualDisplayedTotal = getActualDisplayedTotal (nameOfTotal, logFileWriter);
		if (compareExpectedToActualTotal(expectedTotal, actualDisplayedTotal, logFileWriter)) {
			logFileWriter.write(nameOfTotal+" value is as expected");
			logFileWriter.newLine();
		}//end if
		else {
			String message = "MISMATCH - expected value for "+nameOfTotal+" is "+expectedTotal
					+", but actual total is "+actualDisplayedTotal+"; \n";
			logFileWriter.newLine();
			logFileWriter.write(message);
			verificationErrors.append(message);
		}//end else

	}//end evaluateOneTotalSummerNinths method
	
	protected boolean compareExpectedToActualTotal (float expectedTotal, String actualTotal, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** Now evaluating the difference between the expected and actual totals ***");
		logFileWriter.newLine();
		
		logFileWriter.write("Expected total is "+expectedTotal);
		logFileWriter.newLine();
		logFileWriter.write("Actual total is "+actualTotal);
		logFileWriter.newLine();

		boolean isSufficientlySimilar = false;
		float difference = Math.abs(Math.abs(expectedTotal) - Math.abs(Float.parseFloat(actualTotal)));
		if (difference <= 0.001) {
			isSufficientlySimilar = true;
			logFileWriter.write("The difference between the expected and actual values is "+difference);
			logFileWriter.newLine();
		}//end if clause
		return isSufficientlySimilar;
	}//end compareExpectedToActualTotal method
	
	protected String getActualDisplayedTotal (String totalName, BufferedWriter logFileWriter) throws Exception{
		String actualDisplayedTotal = new String();
		String locatorName = totalName.replaceAll("\\s+","")+"Locator";
		logFileWriter.write("Locator Name for the total is "+locatorName);
		logFileWriter.newLine();
		By by = null;
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, locatorName, logFileWriter);
		if (by == null) {//ERROR condition - fail the test but don't stop execution
			String message = "ERROR - Could not obtain actual displayed total; \n";
			verificationErrors.append(message);
			logFileWriter.write(message);
			logFileWriter.newLine();
			return "0.000";
		}//end if - ERROR condition
		actualDisplayedTotal = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);

		logFileWriter.write("Actual "+totalName+" displayed for this Commitment is "+actualDisplayedTotal);
		logFileWriter.newLine();
		return actualDisplayedTotal;
	}//end getActualDisplayedTotal method
	
	protected void testSearchAndExportMultipleTimes (BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("*** method testSearchAndExportMultipleTimes called ***");
		logFileWriter.newLine();
	
		testSearchAndExportOnce("Archive", "None", logFileWriter);
		testSearchAndExportOnce("Archive", "Some", logFileWriter);
		testSearchAndExportOnce("Archive", "All", logFileWriter);
		testSearchAndExportOnce("Open", "None", logFileWriter);
		testSearchAndExportOnce("Open", "Some", logFileWriter);
		testSearchAndExportOnce("Open", "All", logFileWriter);
		testSearchAndExportOnce("Draft", "None", logFileWriter);
		testSearchAndExportOnce("Draft", "Some", logFileWriter);
		testSearchAndExportOnce("Draft", "All", logFileWriter);

		logFileWriter.newLine();
		logFileWriter.write("*** method testSearchAndExportMultipleTimes is finished***");
		logFileWriter.newLine();
	}//end testSearchAndExportMultipleTimes method
	
	protected void testSearchAndExportOnce (String status, String selection, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method testSearchAndExportOnce called with status '"+status+"', and selection '"+selection+"' ***");
		logFileWriter.newLine();

		getToSearchPage(logFileWriter);
		Thread.sleep(3000);
		//now, select all Archived Commitments
		searchForCommitmentType(logFileWriter, status);
		Thread.sleep(4000);
		
		//now, export them
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Search Page", "ExportToCSVButton", logFileWriter);
		if (! evaluateAction("Click on the Export to CSV Button on the Funding Commitment Search Page", 
				UIController.waitAndClick(by), logFileWriter)) return;
		Thread.sleep(3000);
		
		//now, switch to the appropriate Export page
		if (! evaluateAction("Switch to the Funding Commitment Search Page", 
				UIController.switchWindowByURL(UIController.FundingCommitmentSearchExportURL, logFileWriter), logFileWriter)) return;
		Thread.sleep(3000);
		
		if (selection.equalsIgnoreCase("All")){
			by = HandSOnTestSuite.searchAuthorizationUI.getByForName("CSV Export Page", "CommitmentFieldsSelectAll", logFileWriter);
			WebElement checkbox = UIController.driver.findElement(by);
			if (! checkbox.isSelected())
				checkbox.click();
			
			by = HandSOnTestSuite.searchAuthorizationUI.getByForName("CSV Export Page", "RecipientFieldsSelectAll", logFileWriter);
			checkbox = UIController.driver.findElement(by);
			if (! checkbox.isSelected())
				checkbox.click();
			
			by = HandSOnTestSuite.searchAuthorizationUI.getByForName("CSV Export Page", "TransferFieldsSelectAll", logFileWriter);
			checkbox = UIController.driver.findElement(by);
			if (! checkbox.isSelected())
				checkbox.click();			
		}//end if - All fields selected
		else if (selection.equalsIgnoreCase("None")){
			by = HandSOnTestSuite.searchAuthorizationUI.getByForName("CSV Export Page", "CommitmentFieldsSelectAll", logFileWriter);
			WebElement checkbox = UIController.driver.findElement(by);
			if (! checkbox.isSelected()){
				checkbox.click();
				Thread.sleep(2000);
			}//end if - select all so we can deselect all
			checkbox.click();
			
			by = HandSOnTestSuite.searchAuthorizationUI.getByForName("CSV Export Page", "RecipientFieldsSelectAll", logFileWriter);
			checkbox = UIController.driver.findElement(by);
			if (! checkbox.isSelected()){
				checkbox.click();
				Thread.sleep(2000);
			}//end if - select all so we can deselect all
			checkbox.click();
			
			by = HandSOnTestSuite.searchAuthorizationUI.getByForName("CSV Export Page", "TransferFieldsSelectAll", logFileWriter);
			checkbox = UIController.driver.findElement(by);
			if (! checkbox.isSelected()){
				checkbox.click();
				Thread.sleep(2000);
			}//end if - select all so we can deselect all
			checkbox.click();
		}//end else if - No fields selected
		else if (selection.equalsIgnoreCase("Some")){
			By selectBy = HandSOnTestSuite.searchAuthorizationUI.getByForName("CSV Export Page", "FavoriteSome", logFileWriter);
			WebElement selectSomeLink = UIController.driver.findElement(selectBy);
			if (! selectSomeLink.isDisplayed()){
				By openFavoritesButton = HandSOnTestSuite.searchAuthorizationUI.getByForName("CSV Export Page", "FavoritesExpansionButton", logFileWriter);
				if (! evaluateAction("Click on the button to expand the Favorites section on the CSV Export Page", 
						UIController.waitAndClick(openFavoritesButton), logFileWriter)) return;
				Thread.sleep(3000);
			
			}//end if
			selectSomeLink.click();
		}//end else if
		
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("CSV Export Page", "DoneButton", logFileWriter);
		if (! evaluateAction("Click on the Done Button on the CSV Export Page", 
				UIController.waitAndClick(by), logFileWriter)) return;
		Thread.sleep(3000);
		String oldFileName = "CommitmentQueryExport.csv";
		String newFileName = "CommitmentQueryExport"+"_"+status+"_"+selection+".csv";
		String newLogDirectoryName = "C:\\Users\\christ13\\Documents\\HandSOn\\";
		String oldLogDirectoryName = "C:\\Users\\christ13\\Downloads";
		
		//of note, it may take up to six or possibly seven minutes for the CSV Export file to finish downloading to the "Downloads" directory
		TestFileController.copyFile(oldFileName, newFileName, ReportRunner.logDirectoryName, logFileWriter, 400);

		logFileWriter.newLine();
		logFileWriter.write("*** method testSearchAndExportOnce is finished***");
		logFileWriter.newLine();
}//end testSearchAndExportOnce method

	protected void testArchiveCommitment (BufferedWriter logFileWriter, int i) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("*** method testArchiveCommitment called ***");
		logFileWriter.newLine();
		
		if (! evaluateAction("Switch to the Funding Commitment window", 
				UIController.switchWindowByURL("showFundingCommitment.do?", logFileWriter), logFileWriter)) return;
		Thread.sleep(3000);


		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "ArchiveButton", logFileWriter);
		if (! evaluateAction("Click on the Archive Button on the Funding Commitment Page", 
				UIController.waitAndClick(by), logFileWriter)) return;
		Thread.sleep(3000);
		
		try {
			//first of all, see if there's an unexpected alert
			boolean isAlertPresent = true;
			int attempt = 1;
			while (isAlertPresent) {
				try {
					Alert alert = UIController.driver.switchTo().alert();
					
					//if we make it this far, then there's an alert to dismiss
					logFileWriter.write("Attempted Dismissal #"+attempt+" of Unexpected Alert");
					logFileWriter.newLine();
					logFileWriter.write("Unexpected Alert text is as follows: "+alert.getText());
					logFileWriter.newLine();
					if (alert.getText().contains("failed to")) {
						String message = "HANDSON-3666 is still an issue - there was a failure to archive even though the document was submitted successfully; \n";
						logFileWriter.newLine();
						logFileWriter.write(message);
						verificationErrors.append(message);
					}//end if
					Thread.sleep(3000);
					alert.accept();
					Thread.sleep(3000);	
					
					By reacquiredArchiveButtonBy = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "ArchiveButton", logFileWriter);
					UIController.waitAndClick(reacquiredArchiveButtonBy);
					Thread.sleep(5000);
					//now, increment the attempt once more as we may have to make another attempt
					attempt++;
				}//try clause - try to dismiss the alert
				catch(Exception e2) {
					isAlertPresent = false;
					logFileWriter.write("Could not acquire the alert because of Exception "+e2.getMessage());
					logFileWriter.newLine();
					logFileWriter.write("Alert has been dismissed");
					logFileWriter.newLine();
				}//end catch - could not acquire the alert
			}//end while
			UIController.switchWindowByURL(UIController.notificationDialogURL, logFileWriter);
		}
		catch (Exception e) {
			String message = "ERROR - Couldn't acquire the notification dialog - archival is unexpectedly not successful";
			logFileWriter.write(message);
			logFileWriter.newLine();
			logFileWriter.close();
			System.out.println(message);
			fail(message);
		}//end catch clause

		UIController.switchWindowByURL(UIController.notificationDialogURL, logFileWriter);
		Thread.sleep(3000);
		By statusMessageBy = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "NotificationDialogStatusMessage", logFileWriter);
		String statusMsg = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(statusMessageBy, logFileWriter);
		verifyMessageContains("Status Message", statusMsg, "archived successfully", logFileWriter, verificationErrors);
		
		By OKButtonBy = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "NotificationDialogSubmitButton", logFileWriter);
		UIController.waitAndClick(OKButtonBy);
		Thread.sleep(3000);
		try{
			UIController.waitAndClick(OKButtonBy);
		}//end try
		catch(Exception e){
			logFileWriter.write("Could not click a second time on the OK button because of Exception "+e.getMessage());
			logFileWriter.newLine();
		}//end catch
		
		//at this point, the Commitment window closes, so go back to the search page
		if (! evaluateAction("Switch to the Funding Commitment Search Page", 
				UIController.switchWindowByURL(UIController.commitmentSearchURL, logFileWriter), logFileWriter)) return;
		Thread.sleep(3000);

		try{
			UIController.driver.close();
			logFileWriter.write("Page closed");
			logFileWriter.newLine();
		}
		catch(Exception e){
			logFileWriter.write("Couldn't close the page because of Exception "+e.getMessage());
			logFileWriter.newLine();
		}

		if (! searchForAndOpenCommitment(logFileWriter, "Archive", i)) {
			String message = "Could not successfully open the Archived Commitment; \n";
			logFileWriter.write(message);
			logFileWriter.newLine();
			logFileWriter.write("Aborting method...");
			logFileWriter.newLine();
			verificationErrors.append(message);
			return;
		}//end if - we can't open the commitment
		else {
			logFileWriter.write("Archived Commitment successfully opened");
			logFileWriter.newLine();
		}//end else - we successfully opened the Commitment	
		
		//make sure that the Commitment has 'Archive' status
		Thread.sleep(3000);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "StatusLabel", logFileWriter);
		String actualStatus = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		verifyValue(archiveStatus, actualStatus, "Status", logFileWriter);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Page", "CloseButton", logFileWriter);
		if (! evaluateAction("Close the Commitment window", 
				UIController.waitAndClick(by), logFileWriter)) return;

	
		logFileWriter.newLine();
		logFileWriter.write("*** method testArchiveCommitment is finished***");
		logFileWriter.newLine();
	}//end testArchiveCommitment method
	
	protected void getToSearchPage (BufferedWriter logFileWriter) throws Exception{

		//get to the Start Page
		UIController.switchToStartPage(logFileWriter);
		
		//Do a Commitment Search for Faculty in the same department
		//might want to make sure that the Commitment Search Page isn't already open
		if (! UIController.switchWindowByURL(UIController.commitmentSearchURL, logFileWriter)){
			UIController.switchToStartPage(logFileWriter);
			Thread.sleep(3000); 
			processPageByLink("Commitment Search", verificationErrors, logFileWriter);
			Thread.sleep(6000);
		}//end if - open the page if it's not there.
		
		if (! UIController.switchWindowByURL(UIController.commitmentSearchURL, logFileWriter)) {
			UIController.switchToStartPage(logFileWriter);
			Thread.sleep(3000);
			UIController.driver.findElement(By.linkText("Commitment Search")).click();
			Thread.sleep(6000);
			if (! evaluateAction("Switch to the Funding Commitment Search Page", 
					UIController.switchWindowByURL(UIController.commitmentSearchURL, logFileWriter), logFileWriter))
				return;
		}
		Thread.sleep(3000);

	}//end getToSearchPage method
	
	protected void searchForCommitmentType(BufferedWriter logFileWriter, String commitmentStatus) throws Exception{
		Thread.sleep(4000);
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Search Page", "ClearButton", logFileWriter);
		if (! evaluateAction("Clear previous options on the Funding Commitment Search Page", 
				UIController.waitAndClick(by), logFileWriter)) return;
		Thread.sleep(3000);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Search Page", "FulfillmentStatusSelect", logFileWriter);
		if (! evaluateAction("Select '"+commitmentStatus+"' as Fulfillment Status on the Funding Commitment Search Page", 
				UIController.selectElementInMenu(by, commitmentStatus), logFileWriter)) return;
		Thread.sleep(3000);

		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Search Page", "FindButton", logFileWriter);
		if (! evaluateAction("Click on the Find Button on the Funding Commitment Search Page", 
				UIController.waitAndClick(by), logFileWriter)) return;
		Thread.sleep(3000);

	}//end searchForCommitmentType method
	
	protected boolean searchForAndOpenCommitment (BufferedWriter logFileWriter, String commitmentStatus, int i) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("*** method searchForAndOpenCommitment called with Commitment ID "+commitmentNumbers[i]+" ***");
		logFileWriter.newLine();
		
		
		getToSearchPage(logFileWriter);
		//clear out any previous values first
		
		searchForCommitmentType(logFileWriter, commitmentStatus);
		
		//now, get to the last page
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Search Page", "LastPageLink", logFileWriter);
		if (! evaluateAction("Click on the Last Page Link on the Funding Commitment Search Page", 
				UIController.waitAndClick(by), logFileWriter)) {
			logFileWriter.write("Could not click on the last page in the Commitment Search - exiting method");
			return false;
		}
		Thread.sleep(3000);
		
		String baseCommitmentLinkLocator = "//div[5]/table/tbody/tr/td/a";
		logFileWriter.write("Working with initial Commitment locator of "+baseCommitmentLinkLocator);
		logFileWriter.newLine();
		int commitmentRow = HandSOnTestSuite.searchAuthorizationUI.getBottomRow(baseCommitmentLinkLocator, commitmentNumbers[i], 2, logFileWriter, true);
		String amendedLocator = "xpath="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseCommitmentLinkLocator, commitmentRow, logFileWriter);
		logFileWriter.write("Amended Commitment Locator being worked with is "+amendedLocator);
		logFileWriter.newLine();
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
		if (! evaluateAction("Click on the derived Commitment link on the Funding Commitment Search Page", 
				UIController.waitAndClick(by), logFileWriter)) return false;
		
		Thread.sleep(10000);
		if (! evaluateAction("Switch to the Funding Commitment window", 
				UIController.switchWindowByURL("showFundingCommitment.do?", logFileWriter), logFileWriter)) return false;
		Thread.sleep(3000);

		//verify that the Commitment page we are on is the correct page
		String actualCommitmentNumber = getCommitmentNumber(logFileWriter);
		if (! evaluateAction("Verifying that we're on the correct Funding Commitment window", 
				actualCommitmentNumber.equalsIgnoreCase(commitmentNumbers[i]), logFileWriter)) return false;
		Thread.sleep(3000);
		
		if (commitmentStatus.equalsIgnoreCase("Archive")) {
			logFileWriter.write("***WARNING - this search will take a long time ****");
			logFileWriter.newLine();
			Thread.sleep(120000);
		}
		
		logFileWriter.newLine();
		logFileWriter.write("*** method searchForAndOpenCommitment is finished***");
		logFileWriter.newLine();
		return true;
	}//end searchForAndOpenCommitment method
	
	private static void processPageByLink(String linkText, StringBuffer verificationErrors, BufferedWriter logFileWriter) throws Exception{
		verificationErrors.append(UIController.processPageByLink(linkText, logFileWriter));
	}//end processPageByLink method

	
	public void testCopyCommitment(BufferedWriter logFileWriter, int i) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("*** method testCopyCommitment called ***");
		logFileWriter.newLine();
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "OpenCommitmentCopyButton", logFileWriter);
		if (! evaluateAction("Click on the FYI Route Button", 
				UIController.waitAndClick(by), logFileWriter)) return;
		Thread.sleep(6000);
		
		if (! evaluateAction("Switch to the Funding Commitment window", 
				UIController.switchWindowByURL("processAction=COPY", logFileWriter), logFileWriter)) return;
		Thread.sleep(3000);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "StatusLabel", logFileWriter);
		String actualStatus = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		verifyValue(currentStatus, actualStatus, "Status", logFileWriter);

		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "CommitmentNumberLabel", logFileWriter);
		String actualCommitmentNumber = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		verifyValue(new String(), actualCommitmentNumber, "Commitment Number", logFileWriter);
		Thread.sleep(6000);
		
		CommitmentTransferLine firstLine = new CommitmentTransferLine(1, logFileWriter);
		CommitmentTransferLine secondLine = new CommitmentTransferLine(2, logFileWriter);
		
		if (! evaluateAction("Verify that the first Commitment Transfer Line has an empty Destination ", 
				firstLine.values[CommitmentTransferLine.DestinationPTA].isEmpty(), logFileWriter)) return;
		
		if (! evaluateAction("Verify that the second Commitment Transfer Line has an empty Destination PTA", 
				secondLine.values[CommitmentTransferLine.DestinationPTA].isEmpty(), logFileWriter)) return;

		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Page", "CloseButton", logFileWriter);
		if (! evaluateAction("Close the Commitment window", 
				UIController.waitAndClick(by), logFileWriter)) return;

		if (! evaluateAction("Acquire the notification dialog", 
				UIController.switchWindowByURL(UIController.notificationDialogURL, logFileWriter), 
				logFileWriter)) return;
		Thread.sleep(3000);
		
		if (! evaluateAction("Close the notification dialog", 
				UIController.waitAndClick(By.xpath("//td[2]/a")), logFileWriter)) return;
		Thread.sleep(10000);
		
		logFileWriter.newLine();
		logFileWriter.write("*** method testCopyCommitment is finished***");
		logFileWriter.newLine();
	}//end testCopyCommitment method

	
	public void testFYIRoute(BufferedWriter logFileWriter, int i) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("*** method testFYIRoute called ***");
		logFileWriter.newLine();
		
		Thread.sleep(10000);
		if (! evaluateAction("Switch to the Funding Commitment window", 
				UIController.switchWindowByURL("showFundingCommitment.do?", logFileWriter), logFileWriter)) return;
		Thread.sleep(3000);
		
		By fyiRouteButtonBy = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "OpenCommitmentFYIRouteButton", logFileWriter);
		if (! evaluateAction("Click on the FYI Route Button", 
				UIController.waitAndClick(fyiRouteButtonBy), logFileWriter)) return;
		Thread.sleep(6000);
		
		if (! evaluateAction("Switch to the FYI Route window", 
				UIController.switchWindowByURL(UIController.FundingCommitmentFYIRouteURL, logFileWriter), logFileWriter)) return;
		Thread.sleep(3000);
		
		String userToAdd = "dsatz";
		
		By fyiRoutePersonBy = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "FYIRouteAddPersonInput", logFileWriter);
		if (! evaluateAction("Enter "+userToAdd+" as the Add Person", 
				HandSOnTestSuite.searchAuthorizationUI.writeStringToTextField(fyiRoutePersonBy, userToAdd, logFileWriter), logFileWriter)) return;
		Thread.sleep(3000);
		
		By fyiRouteAddPersonButtonBy = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "FYIRouteAddUserButton", logFileWriter);
		if (! evaluateAction("Click on the FYI Route Add Person Button", 
				UIController.waitAndClick(fyiRouteAddPersonButtonBy), logFileWriter)) return;
		Thread.sleep(3000);
		
		By fyiRouteSubmitButtonBy = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "FYIRouteSubmitButton", logFileWriter);
		if (! evaluateAction("Click on the FYI Route Submit Button", 
				UIController.waitAndClick(fyiRouteSubmitButtonBy), logFileWriter)) return;
		Thread.sleep(3000);

		Thread.sleep(10000);
		if (! evaluateAction("Switch back to the Funding Commitment window", 
				UIController.switchWindowByURL("showFundingCommitment.do?", logFileWriter), logFileWriter)) return;
		Thread.sleep(3000);
		
		fyiRouteButtonBy = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "OpenCommitmentFYIRouteButton", logFileWriter);
		if (! evaluateAction("Click on the FYI Route Button", 
				UIController.waitAndClick(fyiRouteButtonBy), logFileWriter)) return;
		Thread.sleep(6000);
		
		if (! evaluateAction("Switch to the FYI Route window", 
				UIController.switchWindowByURL(UIController.FundingCommitmentFYIRouteURL, logFileWriter), logFileWriter)) return;
		Thread.sleep(3000);
		
		By fyiFirstUserBy = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "FYIRouteFirstUser", logFileWriter);
		String firstUserNameText = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(fyiFirstUserBy, logFileWriter);
		if (! evaluateAction("Make sure the user added is truly added", 
				firstUserNameText.contains(userToAdd), logFileWriter)) return;
		Thread.sleep(3000);
		
		By fyiFirstUserCheckboxBy = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "FYIRouteDeleteUserCheckbox", logFileWriter);
		if (! evaluateAction("Check the checkbox to delete the user", 
				UIController.waitAndClick(fyiFirstUserCheckboxBy), logFileWriter)) return;
		Thread.sleep(3000);	
		
		By fyiFirstUserDeleteButtonBy = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "FYIRouteDeleteUserButton", logFileWriter);
		if (! evaluateAction("Click the button to delete the user", 
				UIController.waitAndClick(fyiFirstUserDeleteButtonBy), logFileWriter)) return;
		Thread.sleep(3000);	
		
		fyiFirstUserBy = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "FYIRouteFirstUser", logFileWriter);
		if (! evaluateAction("Make sure the user deleted is truly deleted", 
				(! UIController.isElementPresent(fyiFirstUserBy)), logFileWriter)) return;
		Thread.sleep(3000);
		
		fyiRouteSubmitButtonBy = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "FYIRouteSubmitButton", logFileWriter);
		if (! evaluateAction("Click on the FYI Route Submit Button", 
				UIController.waitAndClick(fyiRouteSubmitButtonBy), logFileWriter)) return;
		Thread.sleep(3000);

		Thread.sleep(10000);
		if (! evaluateAction("Switch back to the Funding Commitment window", 
				UIController.switchWindowByURL("showFundingCommitment.do?", logFileWriter), logFileWriter)) return;
		Thread.sleep(3000);
		
		logFileWriter.newLine();
		logFileWriter.write("*** method testFYIRoute is finished***");
		logFileWriter.newLine();
	}//end testFYIRoute method
	
	public void testCommitmentAsPartOfFaculty (BufferedWriter logFileWriter, int i) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("*** method testCommitmentAsPartOfFaculty called ***");
		logFileWriter.newLine();
		
		String expectedFirstNameValue = faculty[i].split(",")[1];
		String expectedLastNameValue = faculty[i].split(",")[0];

		if (! evaluateAction("Switch to the Operations tab window", 
				UIController.switchWindowByURL("showDocumentsTab.do", logFileWriter), logFileWriter)) return;

		if (! evaluateAction("Click on the link to the Faculty search page", 
				UIController.waitAndClick(By.linkText("Faculty Search")), logFileWriter)) return;

		if (! evaluateAction("Check the Historic Checkbox on Faculty search page", 
				UIController.switchWindowByURL("processManageFaculty.do?ACTION=showFacultySearch", logFileWriter), logFileWriter)) return;

		By by = By.id("facultyName");
		
		UIController.waitForElementPresent(by);
		WebElement firstName = UIController.driver.findElement(by);
		firstName.clear();
		firstName.sendKeys(expectedFirstNameValue);

		String firstNameValue = firstName.getAttribute("value");
		System.out.println("Value derived as first name is "+firstNameValue);
		if (! firstNameValue.equalsIgnoreCase(expectedFirstNameValue))
			fail("Could not fill in the first name element");

		by = By.id("ckSearchHistoric");
		UIController.waitForElementPresent(by);
		WebElement checkHistoric = UIController.driver.findElement(by);
		if (checkHistoric.isSelected()){
			logFileWriter.write("Check Historic checkbox is already checked - will not re-check");
			logFileWriter.newLine();			
		}//end if
		else{
			checkHistoric.click();
			if (! evaluateAction("Check the Historic Checkbox on Faculty search page", 
					checkHistoric.isSelected(), logFileWriter)) return;
		}//end outer else - checkbox is not checked so we try to check it
		
		UIController.driver.findElement(By.name("facultyQueryDTO.lastName")).clear();
		UIController.driver.findElement(By.name("facultyQueryDTO.lastName")).sendKeys(expectedLastNameValue);
		UIController.waitAndClick(By.xpath("//div[2]/table/tbody/tr[2]/td/a"));
		by = By.xpath("/html/body/table/tbody/tr/td/table[2]/tbody/tr/td/form/table[2]/tbody/tr/td/div[9]/table/tbody/tr[2]/td[1]/a");
		UIController.waitForElementPresent(by);
		try{
			WebElement facultyLink = UIController.driver.findElement(by); 
			String facultyLinkText = facultyLink.getText();
			logFileWriter.write("Link to Faculty is '"+facultyLinkText+"'");
			logFileWriter.newLine();
			facultyLink.click();
		}//end try
		catch(Exception e){
			String message = "Could not open the link to the faculty record because of Exception "+e.getMessage()+";\n";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
			return;
		}//end catch
		
		if (! evaluateAction("Get to the Faculty record", 
				UIController.switchWindowByURL("processManageFaculty.do?ACTION=load&facultyDTO.unvlId", logFileWriter), logFileWriter)) return;
		
		by = By.id("commit_a");
		UIController.waitForElementPresent(by);
		if (! evaluateAction("Click on the Commitments tab", 
				UIController.waitAndClick(by), logFileWriter)) return;
		
		try{
			by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Page", "LastCommitmentPageLink", logFileWriter);
			Thread.sleep(3000);
			if (UIController.isElementPresent(by)){
				if (! evaluateAction("Click on the link to the last Commitment Page", 
						UIController.waitAndClick(by), logFileWriter)) return;
			}
		}
		catch(Exception e){
			logFileWriter.write("There is no last page for Commitments");
			logFileWriter.newLine();
		}
		Thread.sleep(6000);
		String baseCommitmentLocator = "//tr[7]/td/div/table/tbody/tr/td/a";
		String baseStatusLocator = "//tr[7]/td/div/table/tbody/tr/td[4]/span";
		String baseDescriptionLocator = "//tr[7]/td/div/table/tbody/tr/td[9]/span";
		int bottomCommitmentRow = HandSOnTestSuite.searchAuthorizationUI.getBottomRow(baseCommitmentLocator, 
				commitmentNumbers[i], 2, logFileWriter, true);
		logFileWriter.write("Bottom-most commitment is "+bottomCommitmentRow);
		logFileWriter.newLine();
		
		String bottomCommitmentLocator = HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseCommitmentLocator, bottomCommitmentRow, logFileWriter);	
		By bottomCommitmentBy = HandSOnTestSuite.searchAuthorizationUI.getByForLocator("xpath="+bottomCommitmentLocator, logFileWriter);
		String bottomCommitmentID = UIController.driver.findElement(bottomCommitmentBy).getText();
		logFileWriter.write("Bottom Commitment ID is "+bottomCommitmentID);
		if (! bottomCommitmentID.equalsIgnoreCase(commitmentNumbers[i])) {
			Thread.sleep(6000);
			try{
				by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Page", "LastCommitmentPageLink", logFileWriter);
				Thread.sleep(3000);
				if (UIController.isElementPresent(by)){
					if (! evaluateAction("Click on the link to the last Commitment Page", 
							UIController.waitAndClick(by), logFileWriter)) return;
				}
			}
			catch(Exception e){
				logFileWriter.write("There is no last page for Commitments");
				logFileWriter.newLine();
			}
		}
			
		String bottomStatusLocator = HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseStatusLocator, bottomCommitmentRow, logFileWriter);
		String bottomDescriptionLocator = HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseDescriptionLocator, bottomCommitmentRow, logFileWriter);

		by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator("xpath="+bottomStatusLocator, logFileWriter);
		String actualStatus = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		logFileWriter.write("Actual Commitment Status is on the Faculty Record is determined to be: "+actualStatus);
		logFileWriter.newLine();
		String expectedStatus = "Open";
		if (! evaluateAction("Verify Commitment Status in the Faculty Record",
				expectedStatus.equalsIgnoreCase(actualStatus), logFileWriter)) return;
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator("xpath="+bottomDescriptionLocator, logFileWriter);
		String actualDescription = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		logFileWriter.write("Actual Description Text is on the Faculty Record is determined to be: "+actualStatus);
		logFileWriter.newLine();

		if (! evaluateAction("Verify Description text in the Faculty Record",
				expectedStatus.equalsIgnoreCase(actualStatus), logFileWriter)) return;
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(bottomCommitmentLocator, logFileWriter);
		//UIController.scrollElementIntoView(by, logFileWriter);
		Thread.sleep(6000);
		
		if (! evaluateAction("Click on the bottom-most Commitment with text "+commitmentNumbers[i], 
				UIController.clickLinkAndWait(commitmentNumbers[i], logFileWriter), logFileWriter)) return;
		Thread.sleep(10000);
		if (! evaluateAction("Switch to the Funding Commitment window", 
				UIController.switchWindowByURL("showFundingCommitment.do?", logFileWriter), logFileWriter)) return;
		
		//now, we make sure the status is Open/Pending
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Page", "StatusLabel", logFileWriter);
		actualStatus = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		logFileWriter.write("Actual Status is determined to be: "+actualStatus);
		logFileWriter.newLine();
		expectedStatus = "Status:  OPEN / APPROVED";
		if (! evaluateAction("Verify Status on the Commitment Screen",
				expectedStatus.equalsIgnoreCase(actualStatus), logFileWriter)) return;
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Page", "DescriptionTextField", logFileWriter);
		actualDescription = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
		if (! evaluateAction("Verify Description",
				descriptionText.equalsIgnoreCase(actualDescription), logFileWriter)) return;
		
		CommitmentTransferLine expectedTransferLine = preSubmissionTransferLines.get(i);
		CommitmentTransferLine actualTransferLine = new CommitmentTransferLine(1, logFileWriter);
		expectedTransferLine.compareOneTransferLineValue(actualTransferLine, verificationErrors, logFileWriter, 1);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Page", "CloseButton", logFileWriter);
		if (! evaluateAction("Close the Commitment window", 
				UIController.waitAndClick(by), logFileWriter)) return;

		if (! evaluateAction("Switch to the Faculty Record window", 
				UIController.switchWindowByURL("processManageFaculty.do?ACTION=load&facultyDTO.unvlId", logFileWriter), logFileWriter)) return;

		Thread.sleep(3000);
		by = By.id("buttonClose");
		UIController.waitForElementPresent(by);
		if (! evaluateAction("Click on the Close button to close the Faculty Record", 
				UIController.waitAndClick(by), logFileWriter)) return;

		if (! evaluateAction("Acquire the notification dialog", 
				UIController.switchWindowByURL(UIController.notificationDialogURL, logFileWriter), 
				logFileWriter)) return;

		if (! evaluateAction("Close the notification dialog", 
				UIController.waitAndClick(By.id("mybutton")), logFileWriter)) return;
		
		Thread.sleep(3000);
			
		logFileWriter.newLine();
		logFileWriter.write("*** method testCommitmentAsPartOfFaculty is finished***");
		logFileWriter.newLine();
	}//end testCommitmentAsPartOfFaculty method
	
	public void verifyViewSnapshot(BufferedWriter logFileWriter, int index) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method verifyViewSnapshot called ***");
		logFileWriter.newLine();
	
		Thread.sleep(3000);

		if (! evaluateAction("Switch back to the Commitment window - first time in the verifyViewSnapshot method", 
				UIController.switchWindowByURL(UIController.commitmentWindowURL, logFileWriter), 
				logFileWriter)) return;
		Thread.sleep(3000);
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Page", "GenerateSnapshotButton", logFileWriter);
		if (! evaluateAction("Click on the View Snapshot Button on the Commitment Page",
				UIController.waitAndClick(by), logFileWriter)) return;
		Thread.sleep(6000);
		if (! evaluateAction("Switch to the Commitment Snapshot Window",
				UIController.switchWindowByURL(UIController.FundingCommitmentSnapshotURL, logFileWriter), logFileWriter)) return;
		Thread.sleep(3000);
		if (! evaluateAction("Click on the Generate Button on the Generate Snapshot Page",
				UIController.waitAndClick(By.id("buttonSubmit")), logFileWriter)) return;
		Thread.sleep(10000);
		
		

		if (! evaluateAction("Switch back to the Commitment window - second time in the verifyViewSnapshot method", 
				UIController.switchWindowByURL(UIController.commitmentWindowURL, logFileWriter), 
				logFileWriter)) return;
		Thread.sleep(3000);
		
		
		
		logFileWriter.newLine();
		logFileWriter.write("*** method verifyViewSnapshot is finished***");
		logFileWriter.newLine();		
	}//end verifyViewSnapshot method
	
	public void printPDFContentsToLogFile(BufferedWriter logFileWriter, String fileName, int index) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("**** method printPDFContentsToLogFile called with filename '"+fileName
				+"' and commitment ID '"+this.commitmentNumbers[index]+"' ****");
		logFileWriter.newLine();
		logFileWriter.newLine();
		String currentURL = UIController.driver.getCurrentUrl();
		logFileWriter.write("Online URL being worked with is "+currentURL);

		TestFileController.copyFile(fileName, logFileWriter, ReportRunner.logDirectoryName);
		String PDF_filepath = ReportRunner.logDirectoryName + "/"+fileName;
		File PDF_file = new File(PDF_filepath);
		if (PDF_file.exists()) {
			logFileWriter.write("File has been successfully downloaded - "+PDF_filepath);
			logFileWriter.newLine();
		}//end if
		else {
			logFileWriter.write("Could not download the file - returning");
			logFileWriter.newLine();
			return;
		}
		
		String urlString = "file:///"+PDF_filepath;
		
		logFileWriter.newLine();
		logFileWriter.newLine();
		
		Thread.sleep(6000);
		URL url = new URL(urlString);
		InputStream input = url.openStream();
		BufferedInputStream parsedPDF = new BufferedInputStream(input);
		PDDocument PDFtoRead = null;
		
		PDFtoRead = PDDocument.load(parsedPDF);
		String pdfContent = new PDFTextStripper().getText(PDFtoRead);
		verifyPDFContents(logFileWriter, pdfContent, index);
		logFileWriter.write("Contents of PDF are as follows: ");
		logFileWriter.newLine();
		logFileWriter.write(pdfContent);
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("**** method printPDFContentsToLogFile completed successfully ****");
		logFileWriter.newLine();
		logFileWriter.newLine();
		
		PDFtoRead.close();		
	}//end printPDFContentsToLogFile method
	
	protected void verifyPDFContents(BufferedWriter logFileWriter, String pdfContent, int index) throws Exception{
		logFileWriter.write("**** method verifyPDFContents called ****");
		logFileWriter.newLine();
		
		verifyStringInPDFContents(logFileWriter, pdfContent, commitmentNumbers[index]+"Document");
		verifyStringInPDFContents(logFileWriter, pdfContent, "Open/Approved");
		verifyStringInPDFContents(logFileWriter, pdfContent, sourcePTAs[index]);
		verifyStringInPDFContents(logFileWriter, pdfContent, destPTAs[index]);
		verifyStringInPDFContents(logFileWriter, pdfContent, faculty[index]);
		verifyStringInPDFContents(logFileWriter, pdfContent, departments[index]);
		verifyStringInPDFContents(logFileWriter, pdfContent, "Primary");
		verifyStringInPDFContents(logFileWriter, pdfContent, categories[index]);
		logFileWriter.write("**** method verifyPDFContents completed successfully ****");
		logFileWriter.newLine();
	}//end verifyPDFContents method

	protected void verifyStringInPDFContents(BufferedWriter logFileWriter, String pdfContent, String toVerify) throws Exception{
		boolean contains = false;
		contains = pdfContent.contains(toVerify);
		if (contains) {
			logFileWriter.write("PDF contains '"+toVerify+"', as expected");
			logFileWriter.newLine();
		}//end if - we're all good
		else {
			String toVerifyUpperCase = toVerify.toUpperCase();
			contains = pdfContent.contains(toVerifyUpperCase);
			if (contains) {
				logFileWriter.write("PDF contains '"+toVerifyUpperCase+"', as expected");
				logFileWriter.newLine();
			}//end if - it's there, but it's upper case - we're still good
			else {//it's not there in upper case - we will try one more option - to strip out the white space after the comma
				logFileWriter.write("PDF does not contain '"+toVerify+"', as upper case characters - we will try to eliminate white space");
				logFileWriter.newLine();
				String toVerifyWithWhiteSpace = toVerify.replaceAll(",",", ");
				contains = pdfContent.contains(toVerifyWithWhiteSpace);
				if (contains) {
					logFileWriter.write("PDF contains '"+toVerifyWithWhiteSpace+"', as expected");
					logFileWriter.newLine();
				}//end if - it's there, but it's upper case - we're still good
				else {
					logFileWriter.write("ERROR - PDF does not contain '"+toVerify+"', with white space or as upper case - THIS WILL FAIL");
					logFileWriter.newLine();
				}//end else - it's not there in upper case - we will try one more option - to strip out the white space after the comma
			}//end inner else - trying to eliminate white space
		}//end outer else - trying both upper case and eliminating white space
		if (! contains) {
			String errMsg = "ERROR - PDF does NOT contain '"+toVerify+"', as expected; ";
			logFileWriter.write(errMsg);
			logFileWriter.newLine();
			verificationErrors.append(errMsg+"/n");
			System.out.println(errMsg);
		}//end else - ERROR
	}//end verifyStringInPDFContents method

	
	public void verifyViewHistory(BufferedWriter logFileWriter, int index) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method verifyViewHistory called ***");
		logFileWriter.newLine();
	
		Thread.sleep(3000);
		
		if (! evaluateAction("Switch back to the Commitment window - first time in the verifyViewHistory method", 
				UIController.switchWindowByURL(UIController.commitmentWindowURL, logFileWriter), 
				logFileWriter)) return;
		Thread.sleep(3000);
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Page", "GenerateHistoryButton", logFileWriter);
		if (! evaluateAction("Click on the View History Button on the Commitment Page",
				UIController.waitAndClick(by), logFileWriter)) return;
		Thread.sleep(6000);
		/*  we don't have to test this any more because the Commitment History Detail Report is now downloaded as a PDF and its contents evaluated
		
		if (! evaluateAction("Switch to the Commitment History Window",
				UIController.switchWindowByURL(UIController.FundingCommitmentHistoryURL, logFileWriter), logFileWriter)) return;
		Thread.sleep(10000);
		 */
		if (! evaluateAction("Switch back to the Commitment window - second time in the verifyViewHistory method", 
				UIController.switchWindowByURL(UIController.commitmentWindowURL, logFileWriter), 
				logFileWriter)) return;
		Thread.sleep(3000);
		
		logFileWriter.newLine();
		logFileWriter.write("*** method verifyViewHistory is finished***");
		logFileWriter.newLine();
	}//end verifyViewHistory method
	
	/*
	 * This method assumes that we are already on the correct Commitment Record screen.
	 */
	public void verifyViewSummary(BufferedWriter logFileWriter, int index) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method verifyViewSummary called for commitment ID '" + commitmentNumbers[index] + "' ***");
		logFileWriter.newLine();
		
		CommitmentTransferLine secondYearRow = new CommitmentTransferLine(2, logFileWriter);
		String secondYearTotalAmount = getEditedAmount(secondYearRow.values[CommitmentTransferLine.TotalAmount]);

		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Page", "GenerateSummaryButton", logFileWriter);
		if (! evaluateAction("Click on the Generate Summary Button on the Commitment Page",
				UIController.waitAndClick(by), logFileWriter)) return;
		Thread.sleep(6000);
		if (! evaluateAction("Switch to the Commitment Summary Window",
				UIController.switchWindowByURL(UIController.FundingCommitmentSummaryURL, logFileWriter), logFileWriter)) return;
		Thread.sleep(3000);
		
		
		// we have to get the Transfer Line amounts and compare them to what is on the screen
		//get the Transfer Line Amounts
		String firstYearTotalAmount = getEditedAmount(preSubmissionTransferLines.get(index).values[CommitmentTransferLine.TotalAmount]);
		/*
		if (firstYearTotalAmount.contains("-")){
			firstYearTotalAmount = "-$"+firstYearTotalAmount.substring(2);
		}
		*/
		logFileWriter.write("Expected Amount from the Transfer Line for the first year is: "+firstYearTotalAmount);
		logFileWriter.newLine();
		evaluateStringValueFromLocatorName("CommitmentSummarySourcePTAFirstLineFirstYear", firstYearTotalAmount, logFileWriter);
		evaluateStringValueFromLocatorName("CommitmentSummarySourcePTASecondLineFirstYear", firstYearTotalAmount, logFileWriter);
		//evaluateStringValueFromLocatorName("CommitmentSummarySourcePTAThirdLineFirstYear", firstYearTotalAmount, logFileWriter);
		evaluateStringValueFromLocatorName("CommitmentSummaryDestPTAFirstLineFirstYear", firstYearTotalAmount, logFileWriter);
		evaluateStringValueFromLocatorName("CommitmentSummaryDestPTASecondLineFirstYear", firstYearTotalAmount, logFileWriter);
		//evaluateStringValueFromLocatorName("CommitmentSummaryDestPTAThirdLineFirstYear", firstYearTotalAmount, logFileWriter);
		/*
		if (secondYearTotalAmount.contains("-")){
			firstYearTotalAmount = "-$"+firstYearTotalAmount.substring(2);
		}
		*/
		logFileWriter.write("Expected Amount from the Transfer Line for the second year is: "+secondYearTotalAmount);
		logFileWriter.newLine();
		evaluateStringValueFromLocatorName("CommitmentSummarySourcePTAFirstLineSecondYear", secondYearTotalAmount, logFileWriter);
		evaluateStringValueFromLocatorName("CommitmentSummarySourcePTASecondLineSecondYear", secondYearTotalAmount, logFileWriter);
		//evaluateStringValueFromLocatorName("CommitmentSummarySourcePTAThirdLineSecondYear", secondYearTotalAmount, logFileWriter);
		evaluateStringValueFromLocatorName("CommitmentSummaryDestPTAFirstLineSecondYear", secondYearTotalAmount, logFileWriter);
		evaluateStringValueFromLocatorName("CommitmentSummaryDestPTASecondLineSecondYear", secondYearTotalAmount, logFileWriter);
		//evaluateStringValueFromLocatorName("CommitmentSummaryDestPTAThirdLineSecondYear", secondYearTotalAmount, logFileWriter);
		
		
		firstYearTotalAmount = firstYearTotalAmount.replace("$", "");//get rid of the dollar sign
		firstYearTotalAmount = firstYearTotalAmount.replace(",", "");//get rid of the comma
		firstYearTotalAmount = firstYearTotalAmount.replace(".00", "");//get rid of the decimal and cents
		Float firstYearAmount = new Float(firstYearTotalAmount);
		
		secondYearTotalAmount = secondYearTotalAmount.replace("$", "");//get rid of the dollar sign
		secondYearTotalAmount = secondYearTotalAmount.replace(",", "");//get rid of the comma
		secondYearTotalAmount = secondYearTotalAmount.replace(".00", "");//get rid of the decimal and cents
		Float secondYearAmount = new Float(secondYearTotalAmount);
		
		float totalAmount = firstYearAmount.floatValue() + secondYearAmount.floatValue();
		String BothYearsTotalAmount = new Float(totalAmount).toString();
		NumberFormat fmt = NumberFormat.getCurrencyInstance();
		BothYearsTotalAmount = getEditedAmount(fmt.format(totalAmount));
		
		logFileWriter.write("Both Years Total Amount Sum is "+BothYearsTotalAmount);
		logFileWriter.newLine();
		
		evaluateStringValueFromLocatorName("CommitmentSummarySourcePTAFirstLineTotal", BothYearsTotalAmount, logFileWriter);
		evaluateStringValueFromLocatorName("CommitmentSummarySourcePTASecondLineTotal", BothYearsTotalAmount, logFileWriter);
		//evaluateStringValueFromLocatorName("CommitmentSummarySourcePTAThirdLineTotal", BothYearsTotalAmount, logFileWriter);
		evaluateStringValueFromLocatorName("CommitmentSummaryDestPTAFirstLineTotal", BothYearsTotalAmount, logFileWriter);
		evaluateStringValueFromLocatorName("CommitmentSummaryDestPTASecondLineTotal", BothYearsTotalAmount, logFileWriter);
		//evaluateStringValueFromLocatorName("CommitmentSummaryDestPTAThirdLineTotal", BothYearsTotalAmount, logFileWriter);

		Thread.sleep(3000);
		
		logFileWriter.write("Closing Window with URL " + UIController.driver.getCurrentUrl());
    	UIController.driver.close();
    	Thread.sleep(3000);
		
		if (! evaluateAction("Switch back to the Funding Commitment window from the Commitment Summary Window", 
				UIController.switchWindowByURL("showFundingCommitment.do?", logFileWriter), logFileWriter)) return;

		logFileWriter.newLine();
		logFileWriter.write("*** method verifyViewSummary is finished***");
		logFileWriter.newLine();
	}//end verifyViewSummary method
	
	public void evaluateStringValueFromLocatorName(String name, String expectedAmount, BufferedWriter logFileWriter) throws Exception{
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Page", name, logFileWriter);
    	String actualAmount = getEditedAmount(HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter));
    	logFileWriter.write("Actual Amount from name "+name+" is '"+actualAmount+"';");
    	logFileWriter.newLine();
    	if (getEditedAmount(expectedAmount).equalsIgnoreCase(actualAmount)){
    		logFileWriter.write("... and it is equal to the expected amount");
    		logFileWriter.newLine();
    	}//end if - all is well
    	else {
    		String message = "ERROR - expected amount is '"+expectedAmount+"', but actual amount is '"+actualAmount+"';\n";
    		verificationErrors.append(message);
    		System.out.println(message);
    		logFileWriter.write(message);
    		logFileWriter.newLine();
    	}//end else - ERROR
	}//end outputStringValueFromLocatorName
	
	protected String getEditedAmount(String rawAmount) throws Exception{
		if (rawAmount.contains("(")){
			String amountNoLeftParentheses = rawAmount.replaceAll("\\(", "");
			String amountNoParentheses = amountNoLeftParentheses.replaceAll("\\)", "");
			rawAmount = "-"+amountNoParentheses;
		}//end if
		String editedAmount = new String();
		if ( (rawAmount.charAt(0) == '$')&&(rawAmount.charAt(1) == '-') ) {
			editedAmount = "-$"+rawAmount.substring(2);
		}
		else
			editedAmount = rawAmount;
		return editedAmount;
	}//end getEditedAmount method
	
	protected String getModifiedLocator(String locator, int increment, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** ALERT ****");
		logFileWriter.newLine();
		logFileWriter.write("The existing locator yields no value - so getModifiedLocator method is being called");
		logFileWriter.newLine();
		logFileWriter.write("The existing locator is '"+locator+"'");
		logFileWriter.newLine();
		logFileWriter.write("... and the increment is "+increment);
		logFileWriter.newLine();
		String searchString = "tr";
		//first we find out where the problematic tr value is in the String - get the index
		int from_index = 0;
		int last_tr_index = 0;
		last_tr_index = locator.indexOf(searchString);
		while (from_index != -1) {
    		from_index = locator.indexOf(searchString, last_tr_index+1);
    		logFileWriter.write("Now searching for tr string at index "+from_index);
    		logFileWriter.newLine();
    		if (from_index != -1)
    			last_tr_index = from_index;
		}//end while
		//now, get the value of the row that the problematic tr is pointing to
		last_tr_index = last_tr_index + 3;
		logFileWriter.write("Within the locator String, we believe that the number we want to replace is at index "+last_tr_index);
		logFileWriter.newLine();
		int tr_row = 0;
		String tr_row_stringValue = new String();
		try {
			tr_row_stringValue = String.valueOf(locator.charAt(last_tr_index));
			logFileWriter.write("Trying to parse "+tr_row_stringValue+" into a Number");
			logFileWriter.newLine();
			tr_row = Integer.parseInt(tr_row_stringValue);
		}
		catch(NumberFormatException e) {
			logFileWriter.write("We couldn't format "+tr_row_stringValue+" into a number - this method FAILED");
			logFileWriter.newLine();
		}
		String modifiedLocator = new String();
		if (tr_row > 0) {//execute the following if and only if we have successfully extracted the row number of the tr.
			logFileWriter.write("We have successfully parsed '"+tr_row_stringValue+"' into a number");
			logFileWriter.newLine();

			tr_row = tr_row + increment;
			tr_row_stringValue = String.valueOf(tr_row);
			modifiedLocator = locator.substring(0, last_tr_index)+tr_row_stringValue+locator.substring(last_tr_index+1);
			logFileWriter.write("Modified locator is the following: " + modifiedLocator);
			logFileWriter.newLine();
		}//end if - we have extracted the tr
		return modifiedLocator;
	}//end getModifiedLocator method
	
	public boolean evaluateAction (String action, boolean isSuccessful, BufferedWriter logFileWriter) throws Exception{
		Thread.sleep(3000);
		logFileWriter.write("Evaluating action: "+action);
		logFileWriter.newLine();
		if (isSuccessful){
			logFileWriter.write("... Success");
			logFileWriter.newLine();
			return true;
		}//end if
		else{
			String message = "... FAILED - exiting method;\n";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
			return false;
		}//end else		
	}//end evaluateAction method
	
	public void testSubmission (BufferedWriter logFileWriter, int i) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("*** method testSubmission called ***");
		logFileWriter.newLine();
		
		By submitButtonBy = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "DraftCommitmentSubmitButton", logFileWriter);
		UIController.waitAndClick(submitButtonBy);
		Thread.sleep(5000);
		try {
			//first of all, see if there's an unexpected alert
			boolean isAlertPresent = true;
			int attempt = 1;
			while (isAlertPresent) {
				try {
					Alert alert = UIController.driver.switchTo().alert();
					
					//if we make it this far, then there's an alert to dismiss
					logFileWriter.write("Attempted Dismissal #"+attempt+" of Unexpected Alert");
					logFileWriter.newLine();
					logFileWriter.write("Unexpected Alert text is as follows: "+alert.getText());
					logFileWriter.newLine();
					if (alert.getText().contains("failed to submit")) {
						String message = "HANDSON-3666 is still an issue - there was a failure to submit even though the document validated successfully; \n";
						logFileWriter.newLine();
						logFileWriter.write(message);
						verificationErrors.append(message);
					}//end if
					Thread.sleep(3000);
					alert.accept();
					Thread.sleep(5000);	
					//we tried to dismiss the unexpected alert - now try to submit again			
					By reacquiredSubmitButtonBy = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "DraftCommitmentSubmitButton", logFileWriter);
					UIController.waitAndClick(reacquiredSubmitButtonBy);
					Thread.sleep(5000);
					//now, increment the attempt once more as we may have to make another attempt
					attempt++;
				}//try clause - try to dismiss the alert
				catch(Exception e2) {
					isAlertPresent = false;
					logFileWriter.write("Could not acquire the alert because of Exception "+e2.getMessage());
					logFileWriter.newLine();
					logFileWriter.write("Alert has been dismissed");
					logFileWriter.newLine();
				}//end catch - could not acquire the alert
			}//end while loop - attempt to dismiss the unexpected alert
			UIController.switchWindowByURL(UIController.notificationDialogURL, logFileWriter);
		}
		catch (Exception e) {
			String message = "ERROR - Couldn't acquire the notification dialog - submission is unexpectedly not successful";
			logFileWriter.write(message);
			logFileWriter.newLine();
			logFileWriter.close();
			System.out.println(message);
			fail(message);
		}
		Thread.sleep(3000);
		By statusMessageBy = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "NotificationDialogStatusMessage", logFileWriter);
		String statusMsg = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(statusMessageBy, logFileWriter);
		verifyMessageContains("Status Message", statusMsg, "submitted successfully", logFileWriter, verificationErrors);
		
		By firstLineStatusBy = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "NotificationDialogFirstTransferLineStatusMessage", logFileWriter);
		String firstLineStatus = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(firstLineStatusBy, logFileWriter);
		verifyMessageContains("Status of First Transfer Line submission", firstLineStatus, "Done", logFileWriter, verificationErrors);
		
		By secondLineStatusBy = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "NotificationDialogSecondTransferLineStatusMessage", logFileWriter);
		String secondLineStatus = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(secondLineStatusBy, logFileWriter);
		verifyMessageContains("Status of Second Transfer Line submission", secondLineStatus, sourcePTAs[i] +" approved by Finance System" , logFileWriter, verificationErrors);
		verifyMessageContains("Status of Second Transfer Line submission", secondLineStatus, destPTAs[i] +" approved by "+author , logFileWriter, verificationErrors);
		
		By OKButtonBy = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "NotificationDialogSubmitButton", logFileWriter);
		UIController.waitAndClick(OKButtonBy);
		Thread.sleep(3000);
		try{
			UIController.waitAndClick(OKButtonBy);
		}//end try
		catch(Exception e){
			logFileWriter.write("Could not click a second time on the OK button because of Exception "+e.getMessage());
			logFileWriter.newLine();
		}//end catch
		
		try{
			UIController.driver.navigate().refresh();
			Thread.sleep(6000);
			UIController.driver.switchTo().alert().accept();
			logFileWriter.write("Page refreshed - alert dismissed - current URL is "+UIController.driver.getCurrentUrl());
			logFileWriter.newLine();
		}
		catch(Exception e){
			logFileWriter.write("Couldn't dismiss the alert because of Exception "+e.getMessage());
			logFileWriter.newLine();
		}
		logFileWriter.newLine();
		logFileWriter.write("*** method testSubmission is finished***");
		logFileWriter.newLine();
	}//end testSubmission method
	/*
	 * The goal of this method is to verify that the correct error message is presented when 
	 * the Source Data or Description text fields are blank.  These are negative tests.
	 */
	public void testErrorMessages_MissingSrcDesc (BufferedWriter logFileWriter, int i) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("*** method testErrorMessages_MissingSrcDesc called with index "+i+" ***");
		logFileWriter.newLine();
		
		String whichField = new String();
		switch(i){
			case 0: whichField = descriptionText; break;
			case 1: whichField = dataSourceText; break;
			case 2: whichField = descriptionText;
		}//end switch statement			
		
		logFileWriter.write("The correct error message when "+whichField+" is missing will be tested");
		logFileWriter.newLine();

		String locatorName = whichField.replaceAll("\\s+","")+"TextField";
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, locatorName, logFileWriter);
		WebElement textfield = UIController.driver.findElement(by);
		textfield.clear();
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "ValidateButton", logFileWriter);
		WebElement validateButton = UIController.driver.findElement(by);
		validateButton.click();
		Thread.sleep(3000);
		String alertText = UIController.dismissAlert(logFileWriter);
		verifyMessageContains("Alert text", alertText, "failed validation", logFileWriter, verificationErrors);

		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "ErrorMsgLabel", logFileWriter);
		UIController.waitForElementPresent(by);
		String errMsg = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		verifyMessageContains("Error message", errMsg, "Error", logFileWriter, verificationErrors);

		verifyMessageContains("Error message", errMsg, whichField.toLowerCase(), logFileWriter, verificationErrors);
		verifyMessageContains("Error message", errMsg, whichField, logFileWriter, verificationErrors);

		locatorName = whichField.replaceAll("\\s+","")+"TextField";
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, locatorName, logFileWriter);
		textfield = UIController.driver.findElement(by);
		textfield.sendKeys(whichField);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "ValidateButton", logFileWriter);
		validateButton = UIController.driver.findElement(by);
		validateButton.click();
		Thread.sleep(3000);
		alertText = UIController.dismissAlert(logFileWriter);
		verifyMessageContains("Alert text", alertText, "validated successfully", logFileWriter, verificationErrors);

		logFileWriter.newLine();
		logFileWriter.write("*** method testErrorMessages_MissingSrcDesc is finished***");
		logFileWriter.newLine();
	}//end testErrorMessages_MissingSrcDesc method
	
	public void testErrorMessages_blankField (BufferedWriter logFileWriter, int index, int row) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method testErrorMessages_blankField called with index "+index+" and row "+row+" ***");
		logFileWriter.newLine();
		logFileWriter.write("*** This method will verify HANDSON-3713 as a Regression Test ***");
		logFileWriter.newLine();
		logFileWriter.newLine();

		CommitmentTransferLine lineUnderTest = new CommitmentTransferLine(row, logFileWriter);
		
		/*
		 * The original values will be copied and stored into local variables
		 * The following values will be set blank and the Commitment saved, and error messages verified:
		 * The Sub Category, the Funding Unit, the Source PTA, the Destination PTA, the Expense Area, and the Transfer Status
		 * Then, the values will be set back to their original values
		 * 
		 * */
		logFileWriter.write("Here are the original values of row "+row+" at index "+index+": ");
		logFileWriter.newLine();
		String originalSubCategory = lineUnderTest.getValueForField(CommitmentTransferLine.SubCategory, logFileWriter);
		logFileWriter.write("Subcategory: "+originalSubCategory+"; ");
		String originalFundingUnit = lineUnderTest.getValueForField(CommitmentTransferLine.FundingUnit, logFileWriter);
		logFileWriter.write("Funding Unit: "+originalFundingUnit+"; ");
		String originalSourcePTA = lineUnderTest.getValueForField(CommitmentTransferLine.SourcePTA, logFileWriter);
		logFileWriter.write("Source PTA: "+originalSourcePTA+"; ");
		String originalDestPTA = lineUnderTest.getValueForField(CommitmentTransferLine.DestinationPTA, logFileWriter);
		logFileWriter.write("Destination PTA: "+originalDestPTA+"; ");
		String originalExpenseArea = lineUnderTest.getValueForField(CommitmentTransferLine.ExpenseArea, logFileWriter);
		logFileWriter.write("Expense Area: "+originalExpenseArea+"; ");
		String originalTransferStatus = lineUnderTest.getValueForField(CommitmentTransferLine.TransferStatus, logFileWriter);
		logFileWriter.write("Transfer Status: "+originalTransferStatus+"; ");
		logFileWriter.newLine();
		logFileWriter.newLine();

		//testOneBlankField(int row, String valueToReset, int valueIndexToReset, String newValue, String originalValue, BufferedWriter logFileWriter)
		//one at a time, set the value to blank or to "select" and save.  Verify the correct error message is displayed, then reset it back to the original value.
		testOneBlankField(row, "Sub-Category", CommitmentTransferLine.SubCategory, "select", originalSubCategory, logFileWriter);
		testOneBlankField(row, "Funding Unit", CommitmentTransferLine.FundingUnit, "select", originalFundingUnit, logFileWriter);
		testOneBlankField(row, "Source PTA", CommitmentTransferLine.SourcePTA, "", originalSourcePTA, logFileWriter);
		testOneBlankField(row, "Destination PTA", CommitmentTransferLine.DestinationPTA, "", originalDestPTA, logFileWriter);
		testOneBlankField(row, "Expense Area", CommitmentTransferLine.ExpenseArea, "select", originalExpenseArea, logFileWriter);
		testOneBlankField(row, "Transfer Status", CommitmentTransferLine.TransferStatus, "select", originalTransferStatus, logFileWriter);
		//for some reason, whenever the expense area is adjusted, the Fiscal Year gets adjusted as well
		lineUnderTest.setValue(CommitmentTransferLine.FiscalYear, nextNextYr, logFileWriter);
		Thread.sleep(2000);
		lineUnderTest.initialize(logFileWriter);
		saveCommitment(logFileWriter, true);

	}//end testErrorMessages_blankField method
	
	public void testOneBlankField(int row, String valueToReset, int valueIndexToReset, String newValue, String originalValue, BufferedWriter logFileWriter) throws Exception{
		CommitmentTransferLine lineUnderTest = new CommitmentTransferLine(row, logFileWriter);
		logFileWriter.newLine();
		logFileWriter.write("Now testing "+valueToReset+" at row "+row+" with original value '"+originalValue+"' by resetting it to '"+newValue+"'");

		lineUnderTest.setValue(valueIndexToReset, newValue, logFileWriter);
		lineUnderTest.initialize(logFileWriter);
		Thread.sleep(3000);
		saveCommitment(logFileWriter, true);
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "ErrorMsgLabel", logFileWriter);
		UIController.waitForElementPresent(by);
		String errMsg = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		verifyMessageContains("Error message", errMsg, valueToReset+" of section Transfer Details", logFileWriter, verificationErrors);
		verifyMessageContains("Error message", errMsg, "required", logFileWriter, verificationErrors);
		Thread.sleep(3000);
		//now, verify that the buttons are still visible:
		String[] buttonNameArray = {"TransferLineInsertButton", "TransferLineCopyButton", "TransferLineDeleteButton", 
				"CloseButton", "ValidateButton", "SaveButton", "DraftCommitmentSubmitButton","DraftCommitmentFYIRouteButton", 
				"DeleteButton", "DraftCommitmentCopyButton"};
		for(int i=0; i<buttonNameArray.length; i++) {
			if (isButtonPresent(buttonNameArray[i], logFileWriter)) {
				logFileWriter.write("Button is visible after de-selecting the value for "+valueToReset+" at row "+row);
				logFileWriter.newLine();
			}//end if
			else {
				String message = "**** ERROR - Button is NOT visible after de-selecting the value for "+valueToReset+" at row "+row;
				logFileWriter.write(message);
				logFileWriter.newLine();
				verificationErrors.append(message);
			}//end else
		}//end for loop
		
		lineUnderTest.setValue(valueIndexToReset, originalValue, logFileWriter);
		lineUnderTest.initialize(logFileWriter);
		Thread.sleep(3000);
		saveCommitment(logFileWriter, true);
		Thread.sleep(3000);
		
	}//end testOneBlankField
	
	public boolean isButtonPresent(String label, BufferedWriter logFileWriter) throws Exception{
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, label, logFileWriter);
		if (UIController.isElementAccessible(by, myWorksheetName, logFileWriter)){
			logFileWriter.write("We can access the '"+label+"' Button");
			logFileWriter.newLine();
			return true;
		}//end if
		else {
			String message = "**** ERROR - we cannot access the '"+label+"' Button ****";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
			return false;
		}
	}//end isButtonPresent method
	
	public void testErrorMessages_WrongPTA (BufferedWriter logFileWriter, int index, int row) throws Exception{
		logFileWriter.write("*** method testErrorMessages_WrongPTA called with index "+index+" and row "+row+" ***");
		logFileWriter.newLine();
		
		CommitmentTransferLine lineUnderTest = new CommitmentTransferLine(row, logFileWriter);
		CommitmentTransferLine secondLine = new CommitmentTransferLine(1, logFileWriter);
		
		String secondLineDestPTA = secondLine.getValueForField(CommitmentTransferLine.DestinationPTA, logFileWriter);
		String secondLineSourcePTA = secondLine.getValueForField(CommitmentTransferLine.SourcePTA, logFileWriter);
		String secondLineSourceFunding = "H&S";//explicitly name it - use a literal
		
		/*
		 * row 2 - set the Destination PTA to be the Provost Funded Housing Loan PTA - the original Destination PTA is for the Faculty member
		 * --> Expect to get the mismatchedPTAs error message
		 * row 5 - set the Funding Unit to be H&S - source and destination are the dummy PTA
		 * --> Expect to get the wrongFundingUnit error message
		 * row 6 - set the Destination PTA to be the dummy PTA - source and destination are the Provost Funded Housing Loan PTA
		 * --> Expect to get the mismatchedPTAs error message
		 * row 7 - set the Destination PTA to be the PTA for the Faculty member - source PTA is the Provost Funded Housing Loan PTA
		 * --> Expect to get the mismatchedPTAs error message
		 * row 8 - set the Funding Unit to be H&S - source and destination are the Provost Funded Housing Loan PTA
		 * --> Expect to get the wrongFundingUnit error message
		 * row 9 - set the Destination PTA to be the PTA for the Faculty member - source PTA is the dummy PTA
		 * --> Expect to get the mismatchedPTAs error message
		 * row 10 - set the Destination PTA to be the Provost Funded Housing Loan PTA - source PTA is the dummy PTA
		 * --> Expect to get the mismatchedPTAs error message
		 */
	
		if ((row == 1) || (row == 3) || (row ==4)){
			logFileWriter.write("This method is not to be exercised on Rows 1, 3, or 4, returning....");
			logFileWriter.newLine();
			return;
		}
		if (row == 2) {			
			lineUnderTest.setValue(CommitmentTransferLine.DestinationPTA, provostFundedHousingLoanPTA, logFileWriter);
		}
		else if (row == 5) {
			lineUnderTest.setValue(CommitmentTransferLine.FundingUnit, secondLineSourceFunding, logFileWriter);
		}
		else if (row == 6) {
			lineUnderTest.setValue(CommitmentTransferLine.DestinationPTA, dummyPTA, logFileWriter);
		}
		else if (row == 7) {
			lineUnderTest.setValue(CommitmentTransferLine.DestinationPTA, secondLineDestPTA, logFileWriter);
		}
		else if (row == 8) {
			lineUnderTest.setValue(CommitmentTransferLine.FundingUnit, secondLineSourceFunding, logFileWriter);
		}
		else if (row == 9) {
			lineUnderTest.setValue(CommitmentTransferLine.DestinationPTA, secondLineDestPTA, logFileWriter);
		}
		else if (row == 10) {
			lineUnderTest.setValue(CommitmentTransferLine.DestinationPTA, provostFundedHousingLoanPTA, logFileWriter);
		}
		Thread.sleep(2000);
		lineUnderTest.initialize(logFileWriter);
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "ValidateButton", logFileWriter);
		WebElement validateButton = UIController.driver.findElement(by);
		validateButton.click();
		Thread.sleep(3000);
		String alertText = UIController.dismissAlert(logFileWriter);
		verifyMessageContains("Alert text", alertText, "failed validation", logFileWriter, verificationErrors);

		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "ErrorMsgLabel", logFileWriter);
		UIController.waitForElementPresent(by);
		String errMsg = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		verifyMessageContains("Error message", errMsg, "Error", logFileWriter, verificationErrors);
		
		if ((row == 5) || (row == 8)) {
			verifyMessageContains("Error message", errMsg, wrongFundingUnit, logFileWriter, verificationErrors);			
		}
		else if ((row  > 11) || (row < 2)){
			logFileWriter.write("This row does not have an error condition associated with it.");
		}
		else {
			verifyMessageContains("Error message", errMsg, mismatchedPTAs, logFileWriter, verificationErrors);
		}
		
		//now, set everything back
		if (row == 2) {			
			lineUnderTest.setValue(CommitmentTransferLine.DestinationPTA, secondLineDestPTA, logFileWriter);
		}
		else if (row == 5) {
			lineUnderTest.setValue(CommitmentTransferLine.FundingUnit, "Engineering", logFileWriter);
		}
		else if (row == 6) {
			lineUnderTest.setValue(CommitmentTransferLine.DestinationPTA, provostFundedHousingLoanPTA, logFileWriter);
		}
		else if (row == 7) {
			lineUnderTest.setValue(CommitmentTransferLine.DestinationPTA, provostFundedHousingLoanPTA, logFileWriter);
		}
		else if (row == 8) {
			lineUnderTest.setValue(CommitmentTransferLine.FundingUnit, "Provost", logFileWriter);
		}
		else if (row == 9) {
			lineUnderTest.setValue(CommitmentTransferLine.DestinationPTA, dummyPTA, logFileWriter);
		}
		else if (row == 10) {
			lineUnderTest.setValue(CommitmentTransferLine.DestinationPTA, dummyPTA, logFileWriter);
		}
		Thread.sleep(2000);
		lineUnderTest.initialize(logFileWriter);
		//now, verify that there are no more error messages
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "ValidateButton", logFileWriter);
		validateButton = UIController.driver.findElement(by);
		validateButton.click();
		Thread.sleep(3000);
		alertText = UIController.dismissAlert(logFileWriter);
		verifyMessageContains("Alert text", alertText, "validated successfully", logFileWriter, verificationErrors);
		
		logFileWriter.write("*** method testErrorMessages_WrongPTA finished successfully with index "+index+" and row "+row+" ***");
		logFileWriter.newLine();
	}//end method testErrorMessages_WrongPTA
	
	protected void verifyMessageContains(String messageLabel, String message, String toContain, BufferedWriter logFileWriter, StringBuffer verificationErrors) throws Exception{
		if (message.contains(toContain)){
			logFileWriter.write(messageLabel+" contains '"+toContain+"', as expected");
			logFileWriter.newLine();
		}//end if
		else{
			String output = messageLabel+" did not contain '"+toContain+"', as expected;\n";
			logFileWriter.write(output);
			logFileWriter.newLine();
			verificationErrors.append(output);
			System.out.println(output);
		}//end else

	}//end verifyMessageContains method
	
	public void testTransferLineSelection (BufferedWriter logFileWriter, int i) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("*** method testTransferLineSelection called ***");
		logFileWriter.newLine();
		
		CommitmentTransferLine firstRowExpected = new CommitmentTransferLine(1, logFileWriter);
		CommitmentTransferLine secondRowExpected = new CommitmentTransferLine(2, logFileWriter);
		
		logFileWriter.newLine();
		logFileWriter.write("Now selecting first line subcategory and PY - should see only first line visible");
		logFileWriter.newLine();

		selectTransferLineCriteria(subcategories[i], thisYr, logFileWriter);
		Thread.sleep(3000);
		CommitmentTransferLine firstRowActual = new CommitmentTransferLine(1, logFileWriter);
		firstRowExpected.compareOneTransferLineValue(firstRowActual, verificationErrors, logFileWriter, 1);
		CommitmentTransferLine secondRowActual = new CommitmentTransferLine(2, logFileWriter);
		if (secondRowActual.isLineVisible(logFileWriter)){
			String message = "ERROR - second line should not appear, but it does; \n";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
		}//end if
		else{
			logFileWriter.write("first line is visible and second line is not, as expected");
			logFileWriter.newLine();
		}//end else
		
		logFileWriter.newLine();
		logFileWriter.write("Now selecting second line subcategory and Next Year - should only second line visible");
		logFileWriter.newLine();

		selectTransferLineCriteria(secondRowSubcategories[i], nextYr, logFileWriter);
		firstRowActual = new CommitmentTransferLine(1, logFileWriter);
		secondRowActual = new CommitmentTransferLine(2, logFileWriter);
		secondRowExpected.compareToTransferLine(secondRowActual, verificationErrors, logFileWriter);
		if (firstRowActual.isLineVisible(logFileWriter)){
			String message = "ERROR - first line should not appear, but it does; \n";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
		}//end if
		else{
			logFileWriter.write("Second Line is visible and first line is not, as expected");
			logFileWriter.newLine();
		}//end else
		
		logFileWriter.newLine();
		logFileWriter.write("Now selecting first line subcategory and Next Year - should see no lines visible");
		logFileWriter.newLine();
		
		selectTransferLineCriteria(subcategories[i], nextYr, logFileWriter);
		firstRowActual = new CommitmentTransferLine(1, logFileWriter);
		secondRowActual = new CommitmentTransferLine(2, logFileWriter);
		if (firstRowActual.isLineVisible(logFileWriter) || (secondRowActual.isLineVisible(logFileWriter))){
			String message = "ERROR - neither the first nor the second line should appear, but they do; \n";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
		}//end if
		else{
			logFileWriter.write("Neither the first nor the second lines appear, as expected");
			logFileWriter.newLine();
		}//end else
		
		logFileWriter.newLine();
		logFileWriter.write("Now selecting second line subcategory and PY - should see no lines visible");
		logFileWriter.newLine();
		
		selectTransferLineCriteria(secondRowSubcategories[i], thisYr, logFileWriter);
		firstRowActual = new CommitmentTransferLine(1, logFileWriter);
		secondRowActual = new CommitmentTransferLine(2, logFileWriter);
		if (firstRowActual.isLineVisible(logFileWriter) || (secondRowActual.isLineVisible(logFileWriter))){
			String message = "ERROR - second line should not appear, but it does; \n";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
		}//end if
		else{
			logFileWriter.write("Neither the first nor the second lines appear, as expected");
			logFileWriter.newLine();
		}//end else
		
		selectTransferLineCriteria("select", "select", logFileWriter);
		
		logFileWriter.newLine();
		logFileWriter.write("*** method testTransferLineSelection is finished***");
		logFileWriter.newLine();
		
	}//end testTransferLineSelection method
	
	protected void selectTransferLineCriteria(String subcategory, String fiscalYear, BufferedWriter logFileWriter) throws Exception{
		By subCategorySelector = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "TransferLineSubCategorySelect", logFileWriter);
		By fiscalYearSelector = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "TransferLineFiscalYearSelect", logFileWriter);
		
		logFileWriter.write("Attempting to set the subcategory value to "+subcategory);
		logFileWriter.newLine();
		UIController.selectElementInMenu(subCategorySelector, subcategory);
		
		
		logFileWriter.write("Attempting to set the Fiscal Year value to "+fiscalYear);
		logFileWriter.newLine();
		UIController.selectElementInMenu(fiscalYearSelector, fiscalYear);
		
	}//end selectTransferLineCriteria
	
	public void insertNewTransferLine(BufferedWriter logFileWriter, int index, int row) throws Exception{
		logFileWriter.write("*** method insertNewTransferLine called for index "+index+" and for row "+row+" ***");
		logFileWriter.newLine();
		Thread.sleep(1000);
		
		boolean isNewLinePresent = false;
		while (! isNewLinePresent) {
			clickInsertNewLineButton(logFileWriter);
			try {
				String baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName ("Commitment Transfer Line", "SubCategorySelect", logFileWriter);
				String amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
				By by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
				Thread.sleep(2000);
				UIController.selectElementInMenu(by, "Bonus");
				Thread.sleep(2000);
				String subCategory = HandSOnTestSuite.searchAuthorizationUI.getSelectedTextForBy(by, "SubCategorySelect", logFileWriter);
				if (subCategory.isEmpty()) {
					logFileWriter.write("Could not get the SubCategory - will re-try to insert the new line.");
					logFileWriter.newLine();
				}//end if
				else {
					logFileWriter.write("Got the SubCategory in the new line with row number " + row + " - the value is '"+subCategory +"';");
					logFileWriter.newLine();
					isNewLinePresent = true;
				}//end else
			}//end try clause
			catch (Exception e) {
				logFileWriter.write("Could not get the current value of the Subcategory menu in the new line at row " + row + " because of Exception "+e.getLocalizedMessage());
				logFileWriter.newLine();
				e.printStackTrace();
			}
		}//end while
		adjustTransferLine(logFileWriter, index, row);
	}//end insertNewTransferLine method
	
	public void clickInsertNewLineButton(BufferedWriter logFileWriter) throws Exception{
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "TransferLineInsertButton", logFileWriter);
		if (UIController.isElementAccessible(by, myWorksheetName, logFileWriter)){
			logFileWriter.write("We can access the Insert Button");
			logFileWriter.newLine();
		}//end if
		Thread.sleep(1000);
		if (UIController.waitAndClick(by)){
			logFileWriter.write("Successfully clicked on the Insert button for the Transfer Line");
			logFileWriter.newLine();
		}//end if - all is well - the Insert button is clicked
		else{
			String message = "ERROR - could not click on the Insert button for the Transfer Line - exiting method clickInsertNewLineButton;\n";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
			return;
		}//end else - ERROR - the Insert button is not clicked
		Thread.sleep(3000);
	}//end clickInsertNewLineButton method
	
	public void deleteTransferLine(BufferedWriter logFileWriter, boolean confirmDelete, int i) throws Exception{
		logFileWriter.write("*** method deleteTransferLine called, deletion to be confirmed: "+confirmDelete+" ***");
		logFileWriter.newLine();
		

		CommitmentTransferLine toBeDeleted = new CommitmentTransferLine(1, logFileWriter);
		if (! toBeDeleted.checkTransferLineBox(logFileWriter, verificationErrors, 2)){
			String message = "ERROR - could not check the checkbox for the Transfer Line - exiting method deleteTransferLine;\n";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
			return;
		}//end if
		Thread.sleep(2000);

		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "TransferLineDeleteButton", logFileWriter);
		if (UIController.waitAndClick(by)){
			logFileWriter.write("Successfully clicked on the Delete button for the Transfer Line");
			logFileWriter.newLine();
		}//end if - all is well - the Copy button is clicked
		else{
			String message = "ERROR - could not click on the Delete button for the Transfer Line - exiting method copyTransferLine;\n";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
			return;
		}//end else - ERROR - the Delete button is not clicked
		Thread.sleep(3000);
		if (! confirmDelete)
			UIController.cancelAlert(logFileWriter);
		else UIController.dismissAlert(logFileWriter);
		
		if (! confirmDelete){
			if (toBeDeleted.checkTransferLineBox(logFileWriter, verificationErrors, 2) ){
				String message = "ERROR - could not UN-check the checkbox for the Transfer Line - exiting method deleteTransferLine;\n";
				logFileWriter.write(message);
				logFileWriter.newLine();
				System.out.println(message);
				verificationErrors.append(message);
				return;
			}//end if
		}//do this if and only if we are not going to actually delete the line
		Thread.sleep(2000);
		
		logFileWriter.write("*** method deleteTransferLine finished ***");
		logFileWriter.newLine();
	}//end deleteTransferLine method
	
	public void copyTransferLine(BufferedWriter logFileWriter, int i) throws Exception{
		logFileWriter.write("*** method copyTransferLine called ***");
		logFileWriter.newLine();
		
		CommitmentTransferLine toBeCopied = new CommitmentTransferLine(1, logFileWriter);
		if (! toBeCopied.checkTransferLineBox(logFileWriter, verificationErrors, 1)){
			String message = "ERROR - could not check the checkbox for the Transfer Line - exiting method copyTransferLine;\n";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
			return;
		}//end if
		Thread.sleep(2000);
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "TransferLineCopyButton", logFileWriter);
		if (UIController.waitAndClick(by)){
			logFileWriter.write("Successfully clicked on the Copy button for the Transfer Line");
			logFileWriter.newLine();
		}//end if - all is well - the Copy button is clicked
		else{
			String message = "ERROR - could not click on the Copy button for the Transfer Line - exiting method copyTransferLine;\n";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
			return;
		}//end else - ERROR - the Copy button is not clicked
		
		///wait for a few seconds before going on
		Thread.sleep(3000);
		
		CommitmentTransferLine newLine = new CommitmentTransferLine(1, logFileWriter);
		if (! newLine.checkTransferLineBox(logFileWriter, verificationErrors, 2)){
			String message = "ERROR - could not check the checkbox for the Transfer Line;\n";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
			//refresh the web page and dismiss the dialog
			try{
				UIController.driver.navigate().refresh();
				Thread.sleep(6000);
				UIController.driver.switchTo().alert().accept();
				logFileWriter.write("Page refreshed - alert dismissed - current URL is "+UIController.driver.getCurrentUrl());
				logFileWriter.newLine();
			}
			catch(Exception e){
				logFileWriter.write("Couldn't dismiss the alert because of Exception "+e.getMessage());
				logFileWriter.newLine();
			}
			Thread.sleep(3000);
			
			logFileWriter.write("After page is refreshed, making a second attempt to copy the transfer line");
			logFileWriter.newLine();
			CommitmentTransferLine toBeCopied2 = new CommitmentTransferLine(1, logFileWriter);
			if (! toBeCopied2.checkTransferLineBox(logFileWriter, verificationErrors, 1)){
				message = "ERROR - could not check the checkbox for the Transfer Line - exiting method copyTransferLine;\n";
				logFileWriter.write(message);
				logFileWriter.newLine();
				System.out.println(message);
				verificationErrors.append(message);
				return;
			}//end if
			Thread.sleep(2000);
			By by2 = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "TransferLineCopyButton", logFileWriter);
			if (UIController.waitAndClick(by2)){
				logFileWriter.write("Successfully clicked on the Copy button for the Transfer Line the second time");
				logFileWriter.newLine();
			}//end if - all is well - the Copy button is clicked
			else{
				message = "ERROR - could not click on the Copy button for the Transfer Line - exiting method copyTransferLine;\n";
				logFileWriter.write(message);
				logFileWriter.newLine();
				System.out.println(message);
				verificationErrors.append(message);
				return;
			}//end else - ERROR - the Copy button is not clicked
			
		}//end if
		else {
			logFileWriter.write("New Transfer Detail Line successfully created from copy action");
			logFileWriter.newLine();
			//wait a few seconds, then proceed to uncheck the checkbox
			Thread.sleep(3000);
			if (newLine.checkTransferLineBox(logFileWriter, verificationErrors, 2)) {
				String message = "ERROR - could not un-check the Commitment Detail Transfer Line - exiting method copyTransferLine;\n";
				logFileWriter.write(message);
				logFileWriter.newLine();
				System.out.println(message);
				verificationErrors.append(message);			
			}//end if - new transfer line checkbox could not be unchecked
			else {
				logFileWriter.write("New Transfer Detail Line checkbox successfully checked and unchecked");
				logFileWriter.newLine();
			}//end else - we're all good
		}
		Thread.sleep(2000);


		logFileWriter.write("*** method copyTransferLine finished ***");
		logFileWriter.newLine();
	}//end copyTransferLine method

	public void adjustToDoneTransfer(BufferedWriter logFileWriter, int index) throws Exception{
		logFileWriter.write("*** method adjustToDoneTransfer called ***");
		logFileWriter.newLine();

		CommitmentTransferLine doneLine = new CommitmentTransferLine(1, logFileWriter);
		doneLine.setValue(CommitmentTransferLine.TransferStatus, "Done", logFileWriter);
		Thread.sleep(2000);
		doneLine.initialize(logFileWriter);
		Thread.sleep(2000);
		
		CommitmentTransferLine sentLine = preSubmissionTransferLines.get(index);
		
		sentLine.compareOneTransferLineValue(doneLine, verificationErrors, logFileWriter, CommitmentTransferLine.TransferDate);
		sentLine.compareOneTransferLineValue(doneLine, verificationErrors, logFileWriter, CommitmentTransferLine.iBudget);
		sentLine.compareOneTransferLineValue(doneLine, verificationErrors, logFileWriter, CommitmentTransferLine.iJournal);
		
		preSubmissionTransferLines.remove(index);
		preSubmissionTransferLines.add(doneLine);
		
		logFileWriter.write("*** method adjustToDoneTransfer finished ***");
		logFileWriter.newLine();
	}//end adjustToDoneTransfer method

	public void adjustToSentTransfer(BufferedWriter logFileWriter, int index) throws Exception{
		logFileWriter.write("*** method adjustToSentTransfer called ***");
		logFileWriter.newLine();
		
		CommitmentTransferLine sentLine = new CommitmentTransferLine(1, logFileWriter);
		
		sentLine.setValue(CommitmentTransferLine.TransferStatus, "Sent", logFileWriter);
		Thread.sleep(2000);
		UIController.driver.findElement(HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "DataSourceTextField", logFileWriter)).click();
		Thread.sleep(2000);
		sentLine.setValue(CommitmentTransferLine.TransferDate, transferDates[index], logFileWriter);
		sentLine.setValue(CommitmentTransferLine.iJournal, iJournalNumbers[index], logFileWriter);
		sentLine.setValue(CommitmentTransferLine.iBudget, iBudgetNumbers[index], logFileWriter);
		Thread.sleep(2000);
		sentLine.initialize(logFileWriter);
		Thread.sleep(2000);
		preSubmissionTransferLines.remove(index);
		preSubmissionTransferLines.add(sentLine);
		
		logFileWriter.write("*** method adjustToSentTransfer finished ***");
		logFileWriter.newLine();
	}//end adjustToSentTransfer method
	
	public void saveCommitment(BufferedWriter logFileWriter, boolean isUpdate) throws Exception{
		logFileWriter.write("*** method saveCommitment called ***");
		logFileWriter.newLine();
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "SaveButton", logFileWriter);
		if (UIController.waitAndClick(by)){
			logFileWriter.write("Clicked on the Save Button");
			logFileWriter.newLine();
		}//end if
		else{
			String message = "Could not click on the save button; \n";
			verificationErrors.append(message);
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			return;
		}//end else - error message - could not get the save button
		if (isUpdate){
			logFileWriter.write("This is an update to an existing Funding Commitment");
			logFileWriter.newLine();			
		}//end if - this is an update to the existing Funding Commitment
		
		//give it a few seconds for the alert to appear
		Thread.sleep(5000);

		UIController.dismissAlert(logFileWriter);
		//after dismissing the alert, give it a few seconds to adjust
		Thread.sleep(5000);

		logFileWriter.write("*** method saveCommitment finished ***");
		logFileWriter.newLine();
	}//end saveCommitment method
	
	public String getCommitmentNumber(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method getCommitmentNumber called ***");
		logFileWriter.newLine();
		
		String commitmentNumber = new String();
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "CommitmentNumberLabel", logFileWriter);
		commitmentNumber = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);

		if (commitmentNumber.isEmpty()){
			String message = "ERROR - Commmitment Number is empty; \n";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
		}//end if - there is no Commitment Number

		logFileWriter.write("*** method getCommitmentNumber finished - returning Commitment Number '"
				+commitmentNumber+"' ***");
		logFileWriter.newLine();
		return commitmentNumber;

	}//end getCommitmentNumber method
	
	public void verifyNewFundingCommitmentInfo(BufferedWriter logFileWriter, int index) throws Exception{
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "NameLink", logFileWriter);
		String actualName = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		verifyValue(author, actualName, "Name", logFileWriter);

		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "DateLabel", logFileWriter);
		String actualDate = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		verifyValue(currentDate, actualDate, "Date", logFileWriter);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "StatusLabel", logFileWriter);
		String actualStatus = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		verifyValue(currentStatus, actualStatus, "Status", logFileWriter);
		
		addNote(logFileWriter);//modify this to use locators in Input Data.xls file
		verifyNote(logFileWriter);//add code here to check the note contents
		
		completeAttachmentDialog(logFileWriter);
		UIController.switchWindowByURL(UIController.newFundingCommitmentURL, logFileWriter);

		verifyFileAttachment(logFileWriter);
		
		Thread.sleep(2000);
		verifyDescription(logFileWriter);
		Thread.sleep(2000);
		verifyDepartment(logFileWriter, index);
		Thread.sleep(2000);
		verifyDataSource(logFileWriter);
		Thread.sleep(2000);
		verifyCategory(logFileWriter, index);
		Thread.sleep(2000);
		verifyFaculty(logFileWriter, index);
		
		adjustTransferLine(logFileWriter, index, 1);
	}//end verifyNewFundingCommitmentInfo() method
	
	public void adjustTransferLine(BufferedWriter logFileWriter, int index, int row) throws Exception{
		logFileWriter.write("*** method adjustTransferLine called with index "+index+", and row "+row+" ***");
		logFileWriter.newLine();
		
		CommitmentTransferLine newLine = new CommitmentTransferLine(row, logFileWriter);

		if (row == 1)
			newLine.setValue(CommitmentTransferLine.SubCategory, subcategories[index], logFileWriter);
		else if (row ==2){
			newLine.setValue(CommitmentTransferLine.SubCategory, secondRowSubcategories[index], logFileWriter);
		}//end else if
		else {
			newLine.setValue(CommitmentTransferLine.SubCategory, secondRowSubcategories[index], logFileWriter);
		}
		Thread.sleep(2000);

		inputPTAs(newLine, row, index, logFileWriter);
		
		Thread.sleep(2000);
		if (row == 9) {
			newLine.setValue(CommitmentTransferLine.FundingUnit, "Hoover", logFileWriter);
		}
		else if (row == 10) {
			newLine.setValue(CommitmentTransferLine.FundingUnit, "Medicine", logFileWriter);
		}
		else if (row == 5) {
			newLine.setValue(CommitmentTransferLine.FundingUnit, "Engineering", logFileWriter);
		}
		else if (row > 5) {
			newLine.setValue(CommitmentTransferLine.FundingUnit, "Provost", logFileWriter);
		}
		Thread.sleep(2000);
		newLine.setValue(CommitmentTransferLine.ExpenseArea, expenseAreas[index], logFileWriter);
		Thread.sleep(2000);
		newLine.setValue(CommitmentTransferLine.Amount, "10000", logFileWriter);
		Thread.sleep(2000);
		if (row ==2){
			newLine.setValue(CommitmentTransferLine.FiscalYear, nextYr, logFileWriter);
		}//end else if
		else if (row != 1){
			newLine.setValue(CommitmentTransferLine.FiscalYear, nextNextYr, logFileWriter);
		}
		Thread.sleep(2000);


		String[] SummerNinthsNumbers = {"-2.250", "-1.250", "2.225", "m", "-1.000", "2.000", "-0.000", "99.999", "-99.999"};
		if (index == 0) {
			//work with the Summer Ninths
			if (row > 1)
				newLine.setValue(CommitmentTransferLine.SummerNinths, SummerNinthsNumbers[row-2], logFileWriter);

		/*
			if (row == 2)
				newLine.setValue(CommitmentTransferLine.SummerNinths, "-2.250", logFileWriter);
			else if (row == 3)
				newLine.setValue(CommitmentTransferLine.SummerNinths, "-1.250", logFileWriter);
			else if (row == 4)
				newLine.setValue(CommitmentTransferLine.SummerNinths, "2.225", logFileWriter);
			else if (row == 5)//negative case
				newLine.setValue(CommitmentTransferLine.SummerNinths, "m", logFileWriter);
			else if (row == 6)
				newLine.setValue(CommitmentTransferLine.SummerNinths, "-1.000", logFileWriter);
			else if (row == 7)
				newLine.setValue(CommitmentTransferLine.SummerNinths, "2.000", logFileWriter);
			else if (row == 8)
				newLine.setValue(CommitmentTransferLine.SummerNinths, "-0.000", logFileWriter);
			else if (row == 9)
				newLine.setValue(CommitmentTransferLine.SummerNinths, "99.999", logFileWriter);
			else if (row == 10)
				newLine.setValue(CommitmentTransferLine.SummerNinths, "-99.999", logFileWriter);
			*/
			
			if ((row == 7) || (row == 8)) {
				testSyncCheckbox(newLine, row, logFileWriter);
			}
		}//end if - working with Summer Ninths
		
		

		if (row == 1)
			newLine.setValue(CommitmentTransferLine.TransferStatus, "Ready", logFileWriter);
		else
			newLine.setValue(CommitmentTransferLine.TransferStatus, "Hold", logFileWriter);
		Thread.sleep(2000);
		if (row != 1)
			newLine.setValue(CommitmentTransferLine.holdReason, holdReason, logFileWriter);
		
		Thread.sleep(2000);
		if (row==3 || row==4) 
			newLine.setValue(CommitmentTransferLine.TransferStatus, "Done", logFileWriter);
		Thread.sleep(2000);

		newLine.initialize(logFileWriter);
		
		logFileWriter.newLine();
		logFileWriter.write("The completed Transfer Line at row "+row+" is as follows: ");
		logFileWriter.newLine();
		String newTransferLineContents = newLine.toString();
		logFileWriter.write(newTransferLineContents);
		logFileWriter.newLine();
		if (newTransferLineContents.contains("Destination PTA: ;") || newTransferLineContents.contains("Source PTA: ;")) {
			//if either of these strings are present, things are not as they should be - we must re-adjust the Transfer line
			logFileWriter.write("The new Transfer line does not contain PTA's, as expected - will re-adjust");
			logFileWriter.newLine();
			
			inputPTAs(newLine, row, index, logFileWriter);
			newLine.initialize(logFileWriter);			
		}//end outer if
		else {//we're all good - the transfer line is as expected
			logFileWriter.write("The new Transfer line contains both source and destination PTA's, as expected");
			logFileWriter.newLine();
		}//end else
		
		if (row == 1) preSubmissionTransferLines.add(newLine);
		if (row >1)
			newLine.setValue(CommitmentTransferLine.SummerNinths, SummerNinthsNumbers[row-2], logFileWriter);
		newLine.initialize(logFileWriter);

		logFileWriter.newLine();
		logFileWriter.write("*** method adjustTransferLine finished with index "+index+", and row "+row+" ***");
		logFileWriter.newLine();
	}//end adjustTransferLine method
	
	/*
	 * The following method tests the functionality of the sync checkbox as detailed in HANDSON-3697
	 * The following will be tested:
	 * 		The appearing and accessibility of the "Sync" checkbox
	 * 		The default "On" behavior of the "Sync" checkbox
	 * 		The automated synchronization of the amount based on the number of Summer Ninths if the checkbox is checked
	 * 		The automated synchronization of the number of Summer Ninths based on the amount if the checkbox is checked
	 * 		The lack of synchronization of the amount if the number of Summer Ninths is changed and the checkbox is unchecked
	 * 		The lack of synchronization of the number of Summer Ninths if the amount is changed and the checkbox is unchecked
	 * 		The automated re-synchronization of the number of Summer Ninths based on the amount if the checkbox is re-checked
	 * 		The automated re-synchronization of the amount based on the number of Summer Ninths if the checkbox is re-checked
	 */

	
	public void testSyncCheckbox(CommitmentTransferLine line, int row, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("**** method testSyncCheckbox called for row "+row+"; testing HANDSON-3697 ****");
		logFileWriter.newLine();
		
		//first, reset everything so that all dollar amounts and Summer Ninths amounts correspond correctly
		String transferStatus = line.getValueForField(CommitmentTransferLine.TransferStatus, logFileWriter);
		Thread.sleep(2000);
		line.setValue(CommitmentTransferLine.TransferStatus, "select", logFileWriter);
		Thread.sleep(2000);
		line.setValue(CommitmentTransferLine.TransferStatus, transferStatus, logFileWriter);
		Thread.sleep(2000);
		line.initialize(logFileWriter);

		//Test the appearing and accessibility of the "Sync" checkbox by acquiring it
		outputToAllChannels("Now testing the appearing and accessibility of the \"Sync\" checkbox by acquiring it", logFileWriter);
		String message = new String();
		
		String IDLocator = new String();
		switch(row) {
			case 7: IDLocator = "commitmentLineItem[6].summerNinthAmountSyncSelect";
			break;
			case 8: IDLocator = "commitmentLineItem[7].summerNinthAmountSyncSelect";
			break;
		}
		WebElement checkbox = null;
		try {
			//checkbox = UIController.driver.findElement(By.xpath(modifiedLocator));
			checkbox = UIController.driver.findElement(By.name(IDLocator));
		}//end try
		catch(Exception e) {
			message = "**** ERROR: The sync checkbox could not be acquired;**** \n";
			StringWriter outError = new StringWriter();
			e.printStackTrace(new PrintWriter(outError));
			String errorString = outError.toString();
			message = message.concat(errorString);
			outputErrorMessage(message, logFileWriter);
			return;
		}//end catch
		
		//Test the default "On" behavior of the "Sync" checkbox - make sure it's "On" before proceeding....
		outputToAllChannels("Now testing the default \"On\" behavior of the \"Sync\" checkbox - make sure it's \"On\" before proceeding....", logFileWriter);

		if (checkbox.isSelected()) {
			logFileWriter.write("Checkbox is checked - as expected");
			logFileWriter.newLine();
		}//end if
		else {
			message = "**** ERROR: The sync checkbox is unexpectedly not checked; **** \n";
			outputErrorMessage(message, logFileWriter);
			return;
		}//end else
		
		//First, determine and print out the original number of Summer Ninths and the original dollar amount
		String originalNumberOfSummerNinthsString = line.getValueForField(CommitmentTransferLine.SummerNinths, logFileWriter);
		float numberOfSummerNinthsNumber = Float.parseFloat(originalNumberOfSummerNinthsString);
		String originalDollarAmountOfSummerNinthsString = line.getValueForField(CommitmentTransferLine.Amount, logFileWriter);

		logFileWriter.newLine();
		logFileWriter.write("Number of Summer Ninths is determined to be '"+originalNumberOfSummerNinthsString);
		logFileWriter.write("' with a dollar value of '"+originalDollarAmountOfSummerNinthsString);
		logFileWriter.write("' and a Transfer Status of '"+transferStatus+"'");
		logFileWriter.newLine();
		
		//Test the automated synchronization of the amount based on the number of Summer Ninths if the checkbox is checked	
		outputToAllChannels("Now testing the automated synchronization of the amount of Summer Ninths based on the number of Summer Ninths if the checkbox is checked", logFileWriter);
		line.setValue(CommitmentTransferLine.SummerNinths, Float.toString(numberOfSummerNinthsNumber+1), logFileWriter);
		Thread.sleep(2000);
		line.setValue(CommitmentTransferLine.TransferStatus, "select", logFileWriter);
		Thread.sleep(2000);
		line.setValue(CommitmentTransferLine.TransferStatus, transferStatus, logFileWriter);
		Thread.sleep(2000);
		line.initialize(logFileWriter);
		String modifiedDollarAmountOfSummerNinthsString = line.getValueForField(CommitmentTransferLine.Amount, logFileWriter);
		if (! originalDollarAmountOfSummerNinthsString.equalsIgnoreCase(modifiedDollarAmountOfSummerNinthsString)) {
			logFileWriter.write("The original dollar amount of "+originalDollarAmountOfSummerNinthsString);
			logFileWriter.write(" has been modified to a new dollar amount of "+modifiedDollarAmountOfSummerNinthsString);
			logFileWriter.write(", as expected");
			logFileWriter.newLine();
		}//end if - all is well
		else {
			message = "**** ERROR: The dollar amount has not been modified by altering the number of Summer Ninths; **** \n";
			outputErrorMessage(message, logFileWriter);
		}//end else - error
		
		
		//Test the automated synchronization of the number of Summer Ninths based on the amount if the checkbox is checked
		outputToAllChannels("Now testing the automated synchronization of the number of Summer Ninths based on the amount if the checkbox is checked", logFileWriter);
		String modifiedLowDollarAmountOfSummerNinthsString = "5000.00";
		line.setValue(CommitmentTransferLine.Amount, modifiedLowDollarAmountOfSummerNinthsString, logFileWriter);
		Thread.sleep(2000);
		line.setValue(CommitmentTransferLine.TransferStatus, "select", logFileWriter);
		Thread.sleep(2000);
		line.setValue(CommitmentTransferLine.TransferStatus, transferStatus, logFileWriter);
		Thread.sleep(2000);
		line.initialize(logFileWriter);
		String modifiedNumberOfSummerNinthsString = line.getValueForField(CommitmentTransferLine.SummerNinths, logFileWriter);
		if (! originalNumberOfSummerNinthsString.equalsIgnoreCase(modifiedNumberOfSummerNinthsString)) {
			logFileWriter.write("The original number of Summer Ninths, which is "+originalNumberOfSummerNinthsString);
			logFileWriter.write(" has been modified to a new number of Summer Ninths, which is "+modifiedNumberOfSummerNinthsString);
			logFileWriter.write(", as expected");
			logFileWriter.newLine();
		}//end if - all is well
		else {
			message = "**** ERROR: The number of Summer Ninths has not been modified by altering the dollar amount; **** \n";
			outputErrorMessage(message, logFileWriter);
		}//end else - error

		
		//Uncheck the checkbox
		outputToAllChannels("Now un-checking the checkbox", logFileWriter);
		Thread.sleep(2000);
		checkbox.click();
		Thread.sleep(2000);
		if (checkbox.isSelected()) {
			logFileWriter.write("*** ERROR: Unexpectedly, the checkbox remains checked - re-trying;***");
			logFileWriter.newLine();
			checkbox.click();
			Thread.sleep(2000);
			if (checkbox.isSelected()) {
				message = "*** ERROR: Unexpectedly, the checkbox remains checked - aborting method testSyncCheckbox;***";
				outputErrorMessage(message, logFileWriter);				
			}//end if
			else {
				logFileWriter.write("Checkbox was successfully unchecked the second time");
				logFileWriter.newLine();
			}//end inner else
		}//end outer if - checkbox remains checked, unexpectedly
		else {
			logFileWriter.write("Checkbox was successfully unchecked, as expected");
			logFileWriter.newLine();			
		}//end else
		
		//now, reset everything again so that all dollar amounts and Summer Ninths amounts correspond correctly
		Thread.sleep(2000);
		line.setValue(CommitmentTransferLine.TransferStatus, "select", logFileWriter);
		Thread.sleep(2000);
		line.setValue(CommitmentTransferLine.TransferStatus, transferStatus, logFileWriter);
		Thread.sleep(2000);
		line.initialize(logFileWriter);

		
		//Test the lack of synchronization of the amount if the number of Summer Ninths is changed and the checkbox is unchecked
		outputToAllChannels("Now testing the lack of synchronization of the amount if the number of Summer Ninths is changed and the checkbox is unchecked", logFileWriter);
		line.setValue(CommitmentTransferLine.SummerNinths, originalNumberOfSummerNinthsString, logFileWriter);
		Thread.sleep(2000);
		line.setValue(CommitmentTransferLine.TransferStatus, "select", logFileWriter);
		Thread.sleep(2000);
		line.setValue(CommitmentTransferLine.TransferStatus, transferStatus, logFileWriter);
		Thread.sleep(2000);
		
		line.initialize(logFileWriter);
		Thread.sleep(2000);
		String actualDollarAmountOfSummerNinthsString = line.getValueForField(CommitmentTransferLine.Amount, logFileWriter);
		float actualDollarAmountOfSummerNinthsNumber = Float.parseFloat(actualDollarAmountOfSummerNinthsString);
		float modifiedLowDollarAmountOfSummerNinthsNumber = Float.parseFloat(modifiedLowDollarAmountOfSummerNinthsString);
		//if (Math.abs(actualNumberOfSummerNinthsNumber-numberOfSummerNinthsNumber) > 0.1) {

		//if (! originalDollarAmountOfSummerNinthsString.equalsIgnoreCase(actualDollarAmountOfSummerNinthsString)) {
		if (Math.abs(modifiedLowDollarAmountOfSummerNinthsNumber-actualDollarAmountOfSummerNinthsNumber) > 10) {
			message = "**** ERROR: The modified low dollar amount of "+modifiedLowDollarAmountOfSummerNinthsString
						+" has been modified to a new dollar amount of "+actualDollarAmountOfSummerNinthsString
						+", even though the checkbox was unchecked; **** \n";
			outputErrorMessage(message, logFileWriter);
		}//end if - all is well
		else {
			logFileWriter.write("The modified low dollar amount has not been modified by altering the number of Summer Ninths,");
			logFileWriter.write(", as expected, because the sync checkbox is unchecked");
			logFileWriter.newLine();
		}//end else - error
		
		//Test the lack of synchronization of the number of Summer Ninths if the amount is changed and the checkbox is unchecked
		outputToAllChannels("Now testing the lack of synchronization of the number of Summer Ninths if the amount is changed and the checkbox is unchecked", logFileWriter);
		Thread.sleep(2000);
		line.setValue(CommitmentTransferLine.Amount, originalDollarAmountOfSummerNinthsString, logFileWriter);
		Thread.sleep(2000);
		line.setValue(CommitmentTransferLine.TransferStatus, "select", logFileWriter);
		Thread.sleep(2000);
		line.setValue(CommitmentTransferLine.TransferStatus, transferStatus, logFileWriter);
		Thread.sleep(2000);
		modifiedNumberOfSummerNinthsString = line.getValueForField(CommitmentTransferLine.SummerNinths, logFileWriter);
		float modifiedNumberOfSummerNinthsNumber = Float.parseFloat(modifiedNumberOfSummerNinthsString);
		line.initialize(logFileWriter);
		//now, see if the number is affected
		String actualNumberOfSummerNinthsString = line.getValueForField(CommitmentTransferLine.SummerNinths, logFileWriter);		
		float actualNumberOfSummerNinthsNumber = Float.parseFloat(actualNumberOfSummerNinthsString);
		if (Math.abs(actualNumberOfSummerNinthsNumber-modifiedNumberOfSummerNinthsNumber) > 0.1) {
			message = "**** ERROR: The number of Summer Ninths, which is "+modifiedNumberOfSummerNinthsString
						+" has been modified to a new dollar amount of "+actualNumberOfSummerNinthsNumber
						+", even though the checkbox was unchecked; **** \n";
			outputErrorMessage(message, logFileWriter);
		}//end if - all is well
		else {
			logFileWriter.write("The number of Summer Ninths has not been modified by altering the dollar amount,");
			logFileWriter.write(", as expected, because the sync checkbox is unchecked");
			logFileWriter.newLine();
		}//end else - error
		
		
		//Re-check the checkbox
		outputToAllChannels("Now re-checking the checkbox", logFileWriter);
		Thread.sleep(2000);
		checkbox.click();
		Thread.sleep(2000);

		//Test the automated re-synchronization of the number of Summer Ninths based on the amount if the checkbox is re-checked
		outputToAllChannels("Now testing the automated re-synchronization of the number of Summer Ninths based on the amount if the checkbox is re-checked", logFileWriter);

		line.setValue(CommitmentTransferLine.Amount, "250000.00", logFileWriter);//do this first, to make the checkbox reappear
		Thread.sleep(2000);		
		line.setValue(CommitmentTransferLine.TransferStatus, "select", logFileWriter);
		Thread.sleep(2000);
		line.setValue(CommitmentTransferLine.TransferStatus, transferStatus, logFileWriter);
		Thread.sleep(2000);
		line.initialize(logFileWriter);
		Thread.sleep(2000);

		modifiedNumberOfSummerNinthsString = line.getValueForField(CommitmentTransferLine.SummerNinths, logFileWriter);
		modifiedNumberOfSummerNinthsNumber = Float.parseFloat(modifiedNumberOfSummerNinthsString);
		//now, see if the number is affected
		actualNumberOfSummerNinthsString = line.getValueForField(CommitmentTransferLine.SummerNinths, logFileWriter);		
		actualNumberOfSummerNinthsNumber = Float.parseFloat(actualNumberOfSummerNinthsString);
		if (Math.abs(actualNumberOfSummerNinthsNumber-numberOfSummerNinthsNumber) > 0.1) {
			logFileWriter.write("The number of Summer Ninths, which was "+modifiedNumberOfSummerNinthsNumber);
			logFileWriter.write(" has been modified to a new number of Summer Ninths, which is "+actualNumberOfSummerNinthsString);
			logFileWriter.write(", as expected");
			logFileWriter.newLine();
		}//end if - all is well
		else {
			message = "**** ERROR: The number of Summer Ninths has not been modified by altering the dollar amount; "
						+"It remains '"+actualNumberOfSummerNinthsString+"' **** \n";
			outputErrorMessage(message, logFileWriter);
		}//end else - error
		
		outputToAllChannels("Now testing the automated re-synchronization of the amount based on the number of Summer Ninths if the checkbox is re-checked", logFileWriter);
		
		//Test the automated re-synchronization of the amount based on the number of Summer Ninths if the checkbox is re-checked
		line.setValue(CommitmentTransferLine.SummerNinths, originalNumberOfSummerNinthsString, logFileWriter);
		Thread.sleep(2000);
		line.setValue(CommitmentTransferLine.TransferStatus, "select", logFileWriter);
		Thread.sleep(2000);
		line.setValue(CommitmentTransferLine.TransferStatus, transferStatus, logFileWriter);
		Thread.sleep(2000);
		line.initialize(logFileWriter);
		String previousDollarAmountOfSummerNinthsString = line.getValueForField(CommitmentTransferLine.Amount, logFileWriter);
		//get the actual value from the field and get rid of the dollar sign ('$')
		actualDollarAmountOfSummerNinthsString = line.getValueForField(CommitmentTransferLine.Amount, logFileWriter);
		if (originalDollarAmountOfSummerNinthsString.equalsIgnoreCase(actualDollarAmountOfSummerNinthsString)) {
			logFileWriter.write("The dollar amount of "+previousDollarAmountOfSummerNinthsString);
			logFileWriter.write(" has been modified to the original dollar amount of "+originalDollarAmountOfSummerNinthsString);
			logFileWriter.write(", as expected");
			logFileWriter.newLine();
		}//end if - all is well
		else {
			message = "**** ERROR: The dollar amount has not been modified by altering the number of Summer Ninths; Expected: '"
						+originalDollarAmountOfSummerNinthsString+"', actual: '"+actualDollarAmountOfSummerNinthsString+"'**** \n";
			outputErrorMessage(message, logFileWriter);
		}//end else - error
		//now, set the Transfer Status again in case the Amounts and Summer Ninths need to be refreshed
		Thread.sleep(2000);
		line.setValue(CommitmentTransferLine.TransferStatus, "select", logFileWriter);
		Thread.sleep(2000);
		line.setValue(CommitmentTransferLine.TransferStatus, transferStatus, logFileWriter);
		Thread.sleep(2000);

		if (transferStatus.equalsIgnoreCase("Hold"))
			line.setValue(CommitmentTransferLine.holdReason, holdReason, logFileWriter);


		//now, reset the Fiscal Year again just in case it has been inadvertently readjusted
		line.setValue(CommitmentTransferLine.FiscalYear, nextNextYr, logFileWriter);
		Thread.sleep(2000);
		line.initialize(logFileWriter);
		
	}//end method testSyncCheckbox
	
	public void outputErrorMessage(String message, BufferedWriter logFileWriter) throws Exception{
		outputToAllChannels(message, logFileWriter);
		verificationErrors.append(message);
	}//end outputErrorMessage method
	
	public void outputToAllChannels(String message, BufferedWriter logFileWriter) throws Exception{
		System.out.println();
		System.out.println(message);
		System.out.println();
		logFileWriter.newLine();
		logFileWriter.write(message);
		logFileWriter.newLine();
	}//end outputToAllChannels method
	
	public void inputPTAs(CommitmentTransferLine newLine, int row, int index, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method inputPTAs called with index "+index+", and row "+row+" ***");
		logFileWriter.newLine();
		
		if ((row == 3) || (row == 4) || (row == 5)) {//functional test for dummy PTA's
			newLine.setValue(CommitmentTransferLine.SourcePTA, dummyPTA, logFileWriter);
			Thread.sleep(3000);
			newLine.setValue(CommitmentTransferLine.DestinationPTA, dummyPTA, logFileWriter);
			Thread.sleep(6000);
		}
		else if ((row == 6) || (row == 7) || (row == 8)) {// functional test for the Provost Funded Housing Load PTA
			newLine.setValue(CommitmentTransferLine.SourcePTA, provostFundedHousingLoanPTA, logFileWriter);
			Thread.sleep(3000);
			newLine.setValue(CommitmentTransferLine.DestinationPTA, provostFundedHousingLoanPTA, logFileWriter);
			Thread.sleep(6000);
		}//end if
		else if ((row == 9) || (row == 10) ) {//functional test for dummy PTA's
			newLine.setValue(CommitmentTransferLine.SourcePTA, dummyPTA, logFileWriter);
			Thread.sleep(3000);
			newLine.setValue(CommitmentTransferLine.DestinationPTA, dummyPTA, logFileWriter);
			Thread.sleep(6000);
		}
		else {//not any of the rows just mentioned above
			newLine.setValue(CommitmentTransferLine.SourcePTA, sourcePTAs[index], logFileWriter);
			Thread.sleep(3000);
			newLine.setValue(CommitmentTransferLine.DestinationPTA, "", logFileWriter);
			Thread.sleep(3000);
			newLine.setValue(CommitmentTransferLine.DestinationPTA, destPTAs[index], logFileWriter);
			Thread.sleep(6000);
		}//end else
		
		if (row > 2) {//this transfer line has already been saved - we're just saving the update
			Thread.sleep(2000);
			saveCommitment(logFileWriter, true);
			Thread.sleep(5000);
		}//end if
		
		logFileWriter.newLine();
		logFileWriter.write("*** method inputPTAs finished with index "+index+", and row "+row+" ***");
		logFileWriter.newLine();
	}//end inputAndValidateTransferLineAdjustments method
	
	
	
	public void verifyDepartment(BufferedWriter logFileWriter, int deptIndex) throws Exception{
		logFileWriter.write("*** method verifyDepartment called ***");
		logFileWriter.newLine();
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "DepartmentSelect", logFileWriter);
		UIController.selectElementInMenu(by, departments[deptIndex]);

		Thread.sleep(2000);
		String actualDepartmentSelected = HandSOnTestSuite.searchAuthorizationUI.getSelectedTextForBy(by, "DepartmentSelect", logFileWriter);
		verifyValue(departments[deptIndex], actualDepartmentSelected, "Department Text", logFileWriter);

		logFileWriter.write("*** method verifyDepartment finished ***");
		logFileWriter.newLine();
	}//end verifyDepartment method
	
	public void verifyFaculty(BufferedWriter logFileWriter, int facultyIndex) throws Exception{
		logFileWriter.write("*** method verifyFaculty called ***");
		logFileWriter.newLine();
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "FacultySelect", logFileWriter);
		UIController.selectElementInMenu(by, faculty[facultyIndex]);
		
		Thread.sleep(2000);
		String actualFacultySelected = HandSOnTestSuite.searchAuthorizationUI.getSelectedTextForBy(by, "FacultySelect", logFileWriter);
		verifyValue(faculty[facultyIndex], actualFacultySelected, "Faculty Text", logFileWriter);

		logFileWriter.write("*** method verifyFaculty finished ***");
		logFileWriter.newLine();
	}//end verifyFaculty method
	
	public void verifyCategory(BufferedWriter logFileWriter, int categoryIndex) throws Exception{
		logFileWriter.write("*** method verifyCategory called ***");
		logFileWriter.newLine();
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "CategorySelect", logFileWriter);
		UIController.selectElementInMenu(by, categories[categoryIndex]);
		
		Thread.sleep(2000);
		String actualCategorySelected = HandSOnTestSuite.searchAuthorizationUI.getSelectedTextForBy(by, "Commitment Category Select", logFileWriter);
		verifyValue(categories[categoryIndex], actualCategorySelected, "Description Text", logFileWriter);

		logFileWriter.write("*** method verifyCategory finished ***");
		logFileWriter.newLine();
	}//end verifyCategory method
	
	public void verifyDescription(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method verifyDescription called ***");
		logFileWriter.newLine();
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "DescriptionTextField", logFileWriter);
		UIController.driver.findElement(by).clear();
		UIController.driver.findElement(by).sendKeys(descriptionText);
		
		Thread.sleep(3000);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "DescriptionTextField", logFileWriter);
		String actualDescription = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
		verifyValue(descriptionText, actualDescription, "Description Text", logFileWriter);
		
		logFileWriter.write("*** method verifyDescription finished ***");
		logFileWriter.newLine();
	}//end verifyDescription method
	
	public void verifyDataSource(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method verifyDataSource called ***");
		logFileWriter.newLine();
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "DataSourceTextField", logFileWriter);
		UIController.driver.findElement(by).click();
		Thread.sleep(2000);
		UIController.driver.findElement(by).clear();
		UIController.driver.findElement(by).sendKeys(dataSourceText);
		
		Thread.sleep(3000);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "DataSourceTextField", logFileWriter);
		String actualDataSourceText = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
		verifyValue(dataSourceText, actualDataSourceText, "Data Source Text", logFileWriter);

		logFileWriter.write("*** method verifyDataSource finished ***");
		logFileWriter.newLine();
	}//end verifyDataSource method
	
	public void verifyNote(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method verifyNote called ***");
		logFileWriter.newLine();
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "NoteDateLabel", logFileWriter);
		String actualDate = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		verifyValue(currentDate, actualDate, "Note Date", logFileWriter);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "NoteAuthorLink", logFileWriter);
		String actualName = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		verifyValue(author, actualName, "Note Author", logFileWriter);

		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "NoteTextLabel", logFileWriter);
		String actualNoteText = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		verifyValue(noteText, actualNoteText, "Note Text", logFileWriter);
		
		logFileWriter.write("*** method verifyNote finished ***");
		logFileWriter.newLine();
	}//end verifyNote method
	
	public void addNote(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method addNote called ***");
		logFileWriter.newLine();
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "AddNoteButton", logFileWriter);
		UIController.driver.findElement(by).click();

		Thread.sleep(2000);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "NoteTextField", logFileWriter);
		UIController.driver.findElement(by).clear();
		UIController.driver.findElement(by).sendKeys(noteText);

		Thread.sleep(2000);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "InsertNoteButton", logFileWriter);
		UIController.driver.findElement(by).click();

		Thread.sleep(2000);
		logFileWriter.write("*** method addNote finished ***");
		logFileWriter.newLine();
	}//end addNote method
	
	public void verifyFileAttachment(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method verifyFileAttachment called***");
		logFileWriter.newLine();
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "AttachmentDateField", logFileWriter);
		String actualDate = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		verifyValue(currentDate, actualDate, "Attachment Date", logFileWriter);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "AttachmentAuthorField", logFileWriter);
		String actualName = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		verifyValue(author, actualName, "Attachment By Author", logFileWriter);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "AttachmentFileNameLink", logFileWriter);
		String actualLinkName = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		verifyValue(uploadedFileName, actualLinkName, "Attachment Link", logFileWriter);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "AttachmentFileNameLabel", logFileWriter);
		String actualFileName = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		verifyValue(uploadedFileName, actualFileName, "Attachment Label", logFileWriter);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "AttachmentFileTypeLabel", logFileWriter);
		String actualAttachmentType = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		verifyValue("Offer", actualAttachmentType, "Attachment File Type Label", logFileWriter);
		
		logFileWriter.write("*** method verifyFileAttachment finished***");
		logFileWriter.newLine();
	}//end verifyFileAttachment method
	
	public void completeAttachmentDialog(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("**** method completeAttachmentDialog called ***");
		logFileWriter.newLine();
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "AddAttachmentButton", logFileWriter);
		Thread.sleep(3000);
		if (UIController.waitAndClick(by)){
			logFileWriter.write("Successfully clicked on the button to bring up the File Attachment Dialog");
			logFileWriter.newLine();
		}//end if - we're all good - the button was found and clicked
		else{
			String message = "ERROR - could not locate the button required to bring up the File Attachment Dialog;\n";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
			System.out.println(message);
			return;
		}//end else - ERROR;  RETURN;
		if (! UIController.switchWindowByURL(UIController.FundingCommitmentAttachmentDialogURL, logFileWriter)){
			String message = "Could not switch to File Attachment Dialog - exiting method;\n";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
			return;
		}//end if - ERROR - we don't go past this point.  RETURN;
		else{
			logFileWriter.write("Successfully switched to the file attachment dialog");
			logFileWriter.newLine();
		}//end else - we're good so far
		
		Thread.sleep(3000);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "AttachmentFileTextField", logFileWriter);
		//UIController.driver.findElement(By.cssSelector("input[name=\"file\"]")).clear();
		UIController.driver.findElement(by).clear();
		//UIController.driver.findElement(By.cssSelector("input[name=\"file\"]")).sendKeys("C:\\Users\\christ13\\Documents\\HandSon\\School Salary Setting Worksheet.xlsx");
		UIController.driver.findElement(by).sendKeys(uploadedFilePath);
		
		Thread.sleep(3000);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "AttachmentFileDescriptionTextField", logFileWriter);
		UIController.driver.findElement(by).clear();
		UIController.driver.findElement(by).sendKeys(uploadedFileName);
		
		Thread.sleep(3000);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "AttachmentTypeSelect", logFileWriter);
		UIController.selectElementInMenu(by, "Offer");
			
		Thread.sleep(3000);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "AttachmentSubmitButton", logFileWriter);
		UIController.waitAndClick(by);
		
		logFileWriter.write("**** Successfully filled out and submitted file attachment ****");
		logFileWriter.newLine();
	}//end completeAttachmentDialog method
	
	public String getCurrentDate(BufferedWriter logFileWriter) throws Exception{
		return new SimpleDateFormat("MM/dd/yyyy").format(new Date());
	}//end getCurrentDate method
	
	public void verifyValue(String expected, String actual, String name, BufferedWriter logFileWriter) throws Exception{
		if (actual.equalsIgnoreCase(expected)){
			logFileWriter.write("Actual "+name+" found in new Funding Commitment is "+expected+", as expected");
			logFileWriter.newLine();
		}//end if
		else{
			String message = "ERROR: Expected "+name+" of "+expected+", but found "+name+" of '"+actual+"';\n";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
		}//end else - MISMATCH and ERROR
		
	}//end verifyValue
	
	protected ArrayList<CommitmentTransferLine> getTransferLineData (BufferedWriter logFileWriter) throws Exception{
		ArrayList<CommitmentTransferLine> transferLines = new ArrayList<CommitmentTransferLine>();
		String locator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName("Commitment Page", "DestPTANumberTextfieldFirstRow", 
				logFileWriter).split("=")[1];
		logFileWriter.write("Working with initial locator of "+locator);
		logFileWriter.newLine();
		int lastRow = HandSOnTestSuite.searchAuthorizationUI.getBottomRow(locator, "bogus value", 2, logFileWriter, true);
		for (int row=1; row<lastRow; row++){
			transferLines.add(new CommitmentTransferLine(row, logFileWriter));
		}//end for loop
		return transferLines;
	}//end getTransferLineData method


}//end Class
