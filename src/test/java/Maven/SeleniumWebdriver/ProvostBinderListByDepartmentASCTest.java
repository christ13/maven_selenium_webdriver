package Maven.SeleniumWebdriver;

import java.util.ArrayList;
import java.util.TreeMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
/*
 * (1) For Governance Related, only ASC's show up.  This should be done in a separate
 * iteration from the Faculty Appointment Related and Salary Promises ASC's.  With the 
 * iterations, we won't restrict the range of rows to check, but we will restrict what gets 
 * checked.  
 * (2) The figure that shows up on the Provost Binder Report is a summary of both the 
 * DO Share and the Department Share.  Specify "Total" when getting the sums.
 * (3) The figure that shows up on the Provost Binder Report is not influenced by the 
 * FTE Percent of the faculty member.  When retrieving values from the Control Sheet
 * Detail Report, we should specify the boolean variable "adjustForFTE" in ASCTest 
 * (which determines whether or not to adjust for the FTE) to "false".
 * (4) Dean Richard Saller does not appear on any of the Provost Binder Reports, but the Senior
 * Associate Deans do.  So, the "excludeDeans" boolean variable won't work in this case.
 * It has to be set to "true".  We have to specify "Saller, Richard" as one particular Faculty
 * member in the ExpectedASCDataSet to be excluded in the "facultyToExclude" String array.
 * (5) Previous Year Commitments don't show up on any of the Provost Binder Reports 
 * (6) If the Source Pool is the Market Pool or the Regular Merit Pool, 
 * the raises show up in the correct columns on the Provost Binder Reports.  
 * However, if the Source Pool is the Albert Eustace Peasmarch Fund or the Test Pool, 
 * it shows up under the "Regular Merit Pool" column.  
 * Similar behavior occurs in the Pool Allocation Report and this has been filed as HANDSON-3251.  
 * I have updated HANDSON-3251 to include this issue with the Provost Binder Report.
 * This test will be designed to fail until HANDSON-3251 is fixed.  There is also the 
 * issue of the "Gender Pool" column being labeled as null.  This has been filed as
 * HANDSON-3290.  This test will fail until HANDSON-3290 is resolved.
 * (7) FYI, this report will want the total raise amount per faculty, not per faculty per dept.
 * Therefore, if a faculty member belongs to more than one department, we have to add up the raises
 * for the multiple departments.
 * (8) Additionally, if a Faculty member has less than 100% FTE within the department, and only
 * one raise, that raise may or may not appear as FTE-adjusted.  If the faculty member has another
 * (joint) appointment within the School of Humanities and Sciences (the "Outside Appt" column is blank)
 * then the raise will be FTE-adjusted.  Otherwise (if the "Outside Appt" column is not blank), the
 * raise will be calculated at 100% FTE regardless of what the FTE is calculated at.   
 * (9) If a faculty member has two separate ASC's, then the sum of both will be displayed on
 * each and every tab in the Provost Binder Report file that the faculty member appears in.
 * (10) If a faculty member has two separate ASC's, and one of them comes from a Pool other than the 
 * Regular Merit Pool, and this pool is one of the pools that gets inappropriately placed in
 * the Regular Merit Pool (as per HANDSON-3251), and the other comes from the Regular Merit Pool,
 * what will appear on the Provost Binder Report is the sum of both the raise from the ASC that comes
 * from the Regular Merit Pool and the raise that comes from the other Pool, and that sum will 
 * appear in the column that corresponds to the Regular Merit Pool.
 */
public class ProvostBinderListByDepartmentASCTest extends ASCTest {
	
	//protected String department;
	boolean kludgingFor_HANDSON3290 = false;//don't assume it's not fixed yet


	@Before
	public void setUp() throws Exception {
		myExcelReader = TestFileController.ProvostBinderListByDepartment;
		rowForColumnNames = TestFileController.ProvostBinderListByDepartmentWorksheetHeaderRow;
		verificationErrors = new StringBuffer();
		testName = "ProvostBinderListByDepartmentASCTest";
		testEnv = "Test";
		testCaseNumber = "032";
		testDescription = "ASC Test of Provost Binder List By Department";
		logFileName = "Provost Binder Department ASC Test.txt";

		logFileWriter = TestFileController.getLogFileWriter(logDirectoryName  +"/" + logFileName);
		adjustForFTE = true;
		excludeDeans = false;
		backoutPYs = true;

		criteria = new TreeMap<String, String>();
		criteria.put(ExpectedASCData.StringAttributeKeys[ExpectedASCData.StageIndex], 
				ASCTest.stages[ASCTest.KnownStageIndex]);
		
		facultyToExclude = new String[1];
		facultyToExclude[0] = "Saller, Richard";
		
		searchDelimiterColumn = 0;
		excludeZeroDOShare = false;
		consideringOutsideAppt = true;
		summarizingASCs = true;
		expectedStringAttributeNames = new String[3];
		actualStringAttributeNames = new String[3];
		expectedStringAttributeNames[0] = actualStringAttributeNames[0] = ASCFacultyValues.StringAttributeKeys[ASCFacultyValues.FacultyIndex];
		expectedStringAttributeNames[1] = actualStringAttributeNames[1] = ASCFacultyValues.StringAttributeKeys[ASCFacultyValues.RankIndex];
		expectedStringAttributeNames[2] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.DescriptionIndex];
		actualStringAttributeNames[2] = HandSOnTestSuite.FY + " " + ExpectedASCData.StringAttributeKeys[ExpectedASCData.DescriptionIndex];
				
		expectedAmountAttributeNames = new String[2];
		actualAmountAttributeNames = new String[2];
		expectedAmountAttributeNames[0] = ExpectedASCData.amountAttributeKeys[ExpectedASCData.FTEPercentIndex];
		actualAmountAttributeNames[0] = "Dept FTE";
		expectedAmountAttributeNames[1] = ExpectedASCData.amountAttributeKeys[ExpectedASCData.CurrentSalaryIndex];
		actualAmountAttributeNames[1] = HandSOnTestSuite.PY + " Final Salary";
		
		shareNames = new String[ASCTest.sourcePools.length + 1];
		expectedShareCriteria = new TreeMap<String, ArrayList<String>>();
		ArrayList<String>[] lists = new ArrayList[shareNames.length];
		for (int i=0; i<shareNames.length; i++){
			lists[i] = new ArrayList<String>();
			lists[i].add(ASCFacultyValues.ShareTotal);
			if (i == ASCTest.sourcePools.length){
				shareNames[i] = HandSOnTestSuite.FY + " Total Raise $";
				lists[i].add(ASCFacultyValues.ShareTotal);
			}//end if
			//until HANDSON-3290 is fixed, we have to have this kludge
			else if (i==ASCTest.GenderPoolIndex && kludgingFor_HANDSON3290){
				shareNames[i] = HandSOnTestSuite.FY + " null";
				lists[i].add(ASCTest.sourcePools[ASCTest.GenderPoolIndex]);
				outputMessage("HANDSON-3290 kludge is in place - column name is " + shareNames[i], logFileWriter);
			}
			else{
				shareNames[i] = HandSOnTestSuite.FY + " " + ASCTest.sourcePools[i];
				lists[i].add(ASCTest.sourcePools[i]);
			}
			expectedShareCriteria.put(shareNames[i], lists[i]);
		}//end for loop

		super.setUp();
	}
	

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void test() throws Exception {
		String deptAttName = ExpectedASCData.StringAttributeKeys[ExpectedASCData.DepartmentIndex];
		String[] departments 
			= HandSOnTestSuite.expectedASCDataSet.getAllDiscreteStringAttributeValues(deptAttName, logFileWriter);
		//determine whether or not we should kludge for the existing issue HANDSON-3290
		for (int deptIndex=0; deptIndex<departments.length; deptIndex++){
			mySheetName = departments[deptIndex];
			String[] columns = myExcelReader.getColumnNames(mySheetName);
			int genderPoolIndex = java.util.Arrays.asList(columns).indexOf(HandSOnTestSuite.FY 
					+ " " + ASCTest.sourcePools[ASCTest.GenderPoolIndex]);
			if (genderPoolIndex == -1){
				kludgingFor_HANDSON3290 = true;
				verificationErrors.append("\nHANDSON-3290 is still an issue - can't locate the Gender Pool column "
						+" on sheet " + mySheetName + "\n");
			}//end if - we still have to accommodate for HANDSON-3290

			outputMessage("\n**** Now starting the " + mySheetName +" department iteration ***\n", logFileWriter);

			String delimiterValueName = "Category";
			String[] validCategories = {"Faculty Appointment Related", "Salary Promises"};
			useASCOnly = false;
			for(int i=1; i<=validCategories.length; i++){
				String message = "iteration " + i + " with criteria name " +delimiterValueName
						+" and value " + validCategories[i-1];
				outputMessage("\n**** starting " + message +" ****\n", logFileWriter);
				criteria.put(delimiterValueName, validCategories[i-1]);
				criteria.put(deptAttName, departments[deptIndex]);
				super.test();
				criteria.remove(deptAttName);
				criteria.remove(delimiterValueName);
				outputMessage("\n**** ending " + message +" ****\n", logFileWriter);
			}//end for loop
			String validCategory = "Governance Related";
			String message = "iteration " + (validCategories.length + 1) + " with criteria name " +delimiterValueName
					+" and value " + validCategory;
			outputMessage("\n**** starting " + message +" ****\n", logFileWriter);
			criteria.put(delimiterValueName, validCategory);
			criteria.put(deptAttName, departments[deptIndex]);
			useASCOnly = true;
			super.test();
			criteria.remove(delimiterValueName);
			outputMessage("\n**** ending " + message +" ****\n", logFileWriter);
			criteria.remove(deptAttName);
			outputMessage("\n**** Now ending the " + mySheetName +" department iteration ***\n", logFileWriter);
			kludgingFor_HANDSON3290 = false;//re-initialize it.  It may not be an issue for every sheet
		}//end outer for loop - iterating through all of the departments
	}//end test() method

}
