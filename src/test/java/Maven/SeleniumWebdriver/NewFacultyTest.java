package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.interactions.Actions;

public class NewFacultyTest {
	
	protected static final String testIndicatorString = "This faculty member has already been tested.";
	protected static String testName;
	protected static String testCaseNumber;
	protected static String testDescription; 
	protected static String testEnv;
	protected static StringBuffer verificationErrors;
	protected BufferedWriter logFileWriter;
	protected static String[] Names;
	protected static String[] adminApptTitles = {"Dean", "Department Chair", 
		"Executive Director", "Director", "Department Chair", "Associate Department Chair"};
	protected static String[] formerInstTitles = {"Seton Hall University", 
		"School of the Museum of Fine Arts, Boston, MA", "Rush University", "Rice University", 
		"Pennsylvania State University", "Southern Methodist University"};
	protected static final String myWorksheetName = "Faculty Current Appt Tab";
	
	@Before
	public void setUp() throws Exception {
		testName = "NewFacultyTest";
		testEnv = "Test";
		testCaseNumber = "066";
		testDescription = "Test of New Faculty";
		verificationErrors = new StringBuffer();
	}//end setUp() method

	@After
	public void tearDown() throws Exception {
		TestFileController.writeTestResults(testEnv, testCaseNumber, testName, testDescription, verificationErrors);
		logFileWriter.newLine();
		logFileWriter.close();
	}//end tearDown() method

	@Test
	public void test() throws Exception{
		String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		logFileWriter = TestFileController.getLogFileWriter(logFileName);
		logFileWriter.write("New Faculty Records will be tested");
		logFileWriter.newLine();
		int maxIndex = 6;
		Names = selectInitialFaculty(verificationErrors, maxIndex, logFileWriter);

		for(int i=0; i<maxIndex; i++){
			String message = "Faculty Name at index "+i+" is "+Names[i];
			System.out.println(message);
			logFileWriter.write(message);
			logFileWriter.newLine();	
			verificationErrors.append(UIController.processPageByLink(Names[i], logFileWriter));
			Thread.sleep(3000);
			if (! UIController.switchWindowByURL(UIController.existingFacultyURL, logFileWriter)){
				logFileWriter.write("Could not switch to the correct window - exiting");
				logFileWriter.newLine();
				throw new Exception("Could not switch to the Faculty Record Window");
			}//end if
			openNewAdministrativeAppt(logFileWriter);
			Thread.sleep(3000);
			fillInAdminApptData(logFileWriter, i, "first");
			saveData(logFileWriter);
			AddJointAppointment(logFileWriter, i);
			saveData(logFileWriter);
			updateJointAppointment(logFileWriter, i);
			fillInFTE(logFileWriter, "75");
			saveData(logFileWriter);
			verifyWarningMessages(logFileWriter);
			addNewRankAppointment(logFileWriter, i);
			saveData(logFileWriter);
			addApptThruHistory(logFileWriter, i);
			addHistApptThruPresent(logFileWriter, i);
			saveData(logFileWriter);
			addApptFromOutsideInst(logFileWriter, i);
			writeTestIndicatorString(logFileWriter);
			saveAfterClose(logFileWriter);
			UIController.switchWindowByURL(UIController.recruitmentLookupURL, logFileWriter);
			Thread.sleep(3000);
		}//end for loop
		
	}//end test() method
	
	protected void addApptFromOutsideInst(BufferedWriter logFileWriter, int i) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("*** method addNewRankAppointment called with index "+i+" ****");
		logFileWriter.newLine();
		
		//we have to switch back to the current appointment tab and add values before we can save
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "CurrentApptTabLink", logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(4000);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "AddNewRankApptButton", logFileWriter);
		UIController.waitAndClick(by);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "InstitutionSelect", logFileWriter);
		UIController.waitAndSelectElementInMenu(by, formerInstTitles[i]);
		
		fillInStartAndEndDates(logFileWriter, 12);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "CurrentRankSelect", logFileWriter);
		UIController.waitAndSelectElementInMenu(by, "Assistant Professor");

		//Without Limit
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "ApptTermSelect", logFileWriter);
		UIController.waitAndSelectElementInMenu(by, "Without Limit");
	}//end addApptFromOutsideInst
	

	protected void addHistApptThruPresent(BufferedWriter logFileWriter, int i) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("*** method addHistApptThruPresent called with index "+i+" ****");
		logFileWriter.newLine();
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "DeptSelect", logFileWriter);
		String dept = HandSOnTestSuite.searchAuthorizationUI.getSelectedTextForBy(by, "Department Selection dropdown", logFileWriter);

		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "FTEPctTextField", logFileWriter);
		String fte = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
			
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "SabbaticalAccrualTextField", logFileWriter);
		String sabbaticalAccrual = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "BilletNumberTextField", logFileWriter);
		String billetNumber = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
		
		//write a new start and end date in the topmost appointment - make it in the past
		String currentYear = getCurrentYear(logFileWriter);
		String[] dates = fillInStartAndEndDates(logFileWriter, 6).split("-");
		/*
		int decrement = 6;
		String prevYear = Integer.toString(new Integer(currentYear).intValue() - decrement);
		String startDateText = "09/01/"+prevYear;
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "ApptStartDateTextField", logFileWriter);
		HandSOnTestSuite.searchAuthorizationUI.writeStringToTextField(by, startDateText, logFileWriter);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "ApptEndDateTextField", logFileWriter);
		String endDateText = "08/31/"+currentYear;
		HandSOnTestSuite.searchAuthorizationUI.writeStringToTextField(by, endDateText, logFileWriter);
		*/
		saveData(logFileWriter);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "AppointmentHistoryTabLink", logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(4000);
		
		//now, verify that all values are correctly transferred
		String amendedDigit = "3";
		String locator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName(myWorksheetName, "DeptSelect", logFileWriter);
		String amendedLocator = locator.replaceFirst("0", amendedDigit);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
		verifyStringValue("Department", dept, HandSOnTestSuite.searchAuthorizationUI.getSelectedTextForBy(by, "Department Select dropdown", logFileWriter), logFileWriter);

		locator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName(myWorksheetName, "ApptStartDateTextField", logFileWriter);
		amendedLocator = locator.replaceFirst("0", amendedDigit);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
		verifyStringValue("Start Date", dates[0], HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter), logFileWriter);

		locator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName(myWorksheetName, "ApptEndDateTextField", logFileWriter);
		amendedLocator = locator.replaceFirst("0", amendedDigit);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
		verifyStringValue("End Date", dates[1], HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter), logFileWriter);

		locator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName(myWorksheetName, "FTEPctTextField", logFileWriter);
		amendedLocator = locator.replaceFirst("0", amendedDigit);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
		verifyStringValue("FTE", fte, HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter), logFileWriter);
		
		locator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName(myWorksheetName, "SabbaticalAccrualTextField", logFileWriter);
		amendedLocator = locator.replaceFirst("0", amendedDigit);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
		verifyStringValue("Sabbatical Accrual", sabbaticalAccrual, HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter), logFileWriter);
		
		locator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName(myWorksheetName, "BilletNumberTextField", logFileWriter);
		amendedLocator = locator.replaceFirst("0", amendedDigit);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
		verifyStringValue("Billet Number", billetNumber, HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter), logFileWriter);
	}//end addHistApptThruPresent method
	
	protected String fillInStartAndEndDates(BufferedWriter logFileWriter, int delta) throws Exception{
		String currentYear = getCurrentYear(logFileWriter);
		if (delta > 1)
			currentYear = Integer.toString(new Integer(currentYear).intValue() - 1);
		String prevYear = Integer.toString(new Integer(currentYear).intValue() - delta);
		String startDateText = "09/01/"+prevYear;
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "ApptStartDateTextField", logFileWriter);
		HandSOnTestSuite.searchAuthorizationUI.writeStringToTextField(by, startDateText, logFileWriter);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "ApptEndDateTextField", logFileWriter);
		String endDateText = "08/31/"+currentYear;
		HandSOnTestSuite.searchAuthorizationUI.writeStringToTextField(by, endDateText, logFileWriter);
		
		return startDateText+"-"+endDateText;
	}//end fillInStartAndEndDates method
	
	protected void addApptThruHistory(BufferedWriter logFileWriter, int i) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("*** method addApptThruHistory called with index "+i+" ****");
		logFileWriter.newLine();
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "AppointmentHistoryTabLink", logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(4000);
		
		UIController.clickLinkAndWait("New Rank Appointment", logFileWriter);
		Thread.sleep(4000);
		UIController.clickLinkAndWait("Add Administrative Appointment", logFileWriter);
		Thread.sleep(4000);
		
		//we have to switch back to the current appointment tab and add values before we can save
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "CurrentApptTabLink", logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(4000);
		
		verifyBlankFieldsInNewRankAppt(logFileWriter);
		verifyNewRankStartDate(logFileWriter, 9);
		fillInRequiredFieldsInNewRankAppt(logFileWriter);
		fillInAdminApptData(logFileWriter, i, "second");
	}//end addApptThruHistory method
	
	protected String getCorrectNewRank(BufferedWriter logFileWriter) throws Exception{
		String locator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName(myWorksheetName, "CurrentRankSelect", logFileWriter);
		String amendedLocator = locator.replace('0', '1');
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
		String currentRank = HandSOnTestSuite.searchAuthorizationUI.getSelectedTextForBy(by, "Current Rank Select", logFileWriter);
		String newRank = new String();
		if (currentRank.equalsIgnoreCase("Full Professor"))
			newRank = "Full Professor Untenured";
		else if (currentRank.equalsIgnoreCase("Full Professor Untenured"))
			newRank = "Associate Professor";
		else newRank = "Full Professor";
		logFileWriter.write("* method getCorrectNewRank returning new Rank of '"+newRank+"' *");
		logFileWriter.newLine();
		return newRank;
	}//end getCorrectNewRank method
	
	protected void addNewRankAppointment(BufferedWriter logFileWriter, int i) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("*** method addNewRankAppointment called with index "+i+" ****");
		logFileWriter.newLine();
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "AddNewRankApptButton", logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(6000);	
		verifyNewRankApptNumber(logFileWriter);
		verifyNewRankStartDate(logFileWriter, 6);
		//verify blank fields
		verifyBlankFieldsInNewRankAppt(logFileWriter);
		fillInRequiredFieldsInNewRankAppt(logFileWriter);
		//fill in the required values
	}//end addNewRankAppointment method
	
	protected void fillInRequiredFieldsInNewRankAppt(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("**** method fillInRequiredFieldsInNewRankAppt called ****");
		logFileWriter.newLine();
		
		String newRank = getCorrectNewRank(logFileWriter);
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "CurrentRankSelect", logFileWriter);
		UIController.selectElementInMenu(by, newRank);
		if (newRank.equalsIgnoreCase("Full Professor Untenured")){
			by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "ApptTermSelect", logFileWriter);
			UIController.selectElementInMenu(by, "Without Limit");
		}//end if
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "DeptSelect", logFileWriter);
		UIController.selectElementInMenu(by, "Anthropology");
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "ApptReasonSelect", logFileWriter);
		UIController.selectElementInMenu(by, "Reappointment");
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "FTEPctTextField", logFileWriter);
		HandSOnTestSuite.searchAuthorizationUI.writeStringToTextField(by, "100", logFileWriter);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "SabbaticalAccrualTextField", logFileWriter);
		HandSOnTestSuite.searchAuthorizationUI.writeStringToTextField(by, "100", logFileWriter);
	}//end fillInRequiredFieldsInNewRankAppt method
	
	protected void verifyBlankFieldsInNewRankAppt(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("**** method verifyBlankFieldsInNewRankAppt called ****");
		logFileWriter.newLine();

		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "InstitutionSelect", logFileWriter);
		verifyDropdownValue("Institution Name", by, "Stanford University", logFileWriter);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "SchoolSelect", logFileWriter);
		verifyDropdownValue("School Name", by, "Humanities and Sciences", logFileWriter);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "CurrentRankSelect", logFileWriter);
		verifyDropdownValue("Current Rank", by, "Select", logFileWriter);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "DeptSelect", logFileWriter);
		verifyDropdownValue("Department Name", by, "Select", logFileWriter);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "ApptReasonSelect", logFileWriter);
		verifyDropdownValue("Appointment Reason", by, "Select", logFileWriter);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "ApptTypeSelect", logFileWriter);
		verifyDropdownValue("Appointment Type", by, "Primary", logFileWriter);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "ApptEndDateTextField", logFileWriter);
		verifyTextFieldValue("Rank End Date TextField", by, new String(), logFileWriter);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "NotesTextField", logFileWriter);
		verifyTextFieldValue("Notes TextField", by, new String(), logFileWriter);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "BilletNumberTextField", logFileWriter);
		verifyTextFieldValue("Billet Number TextField", by, new String(), logFileWriter);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "BilletNotesTextField", logFileWriter);
		verifyTextFieldValue("Billet Notes TextField", by, new String(), logFileWriter);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "FTEPctTextField", logFileWriter);
		verifyTextFieldValue("FTE Percent TextField", by, new String(), logFileWriter);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "SabbaticalAccrualTextField", logFileWriter);
		verifyTextFieldValue("Sabbatical Accrual TextField", by, new String(), logFileWriter);

	}//end verifyBlankFieldsInNewRankAppt method
	
	protected void verifyDropdownValue (String name, By by, String expected, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("**** method verifyDropdownValue called ****");
		logFileWriter.newLine();
		
		String actual = HandSOnTestSuite.searchAuthorizationUI.getSelectedTextForBy(by, name, logFileWriter);
		verifyStringValue(name, expected, actual, logFileWriter);
	}//end verifyDropdown method
	
	protected void verifyTextFieldValue (String name, By by, String expected, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("**** method verifyTextFieldValue called ****");
		logFileWriter.newLine();
		
		String actual = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
		verifyStringValue(name, expected, actual, logFileWriter);
	}//end verifyTextFieldValue method
	
	protected void verifyNewRankApptNumber(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("**** method verifyNewRankApptNumber called ****");
		logFileWriter.newLine();
		
		/*
		By by = By.id("rankStartDate_1_holder");//this is the label containing the year that the previous rank started
		String prevRankStartDate = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		logFileWriter.write("Previous Rank Appointment Start Date is "+prevRankStartDate);
		String currentYear = new String();
		if ((prevRankStartDate != null) &&(! prevRankStartDate.isEmpty()) && (prevRankStartDate.split("/").length > 2)){	
			currentYear = prevRankStartDate.split("/")[2];
		}//end if - it is as expected
		else{
			logFileWriter.write("We cannot split the start date as expected");
			logFileWriter.newLine();
		}//end else
		*/
		String expectedText = "Rank Appointment (2)";
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "NewRankTitleLabel", logFileWriter);
		String actualText = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		verifyStringValue("New Rank Title Label", expectedText, actualText, logFileWriter);
		
	}//end verifyNewRankApptNumber method
	
	protected void verifyNewRankStartDate(BufferedWriter logFileWriter, int increment) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("**** method verifyNewRankStartDate called with increment '"+increment+"' ****");
		logFileWriter.newLine();
		
		String currentYear = getCurrentYear(logFileWriter);
		String expectedStartDateText = "09/01/"+currentYear;
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "ApptStartDateTextField", logFileWriter);
		String actualStartDateText = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
		verifyStringValue("New Rank Start Date Label", expectedStartDateText, actualStartDateText, logFileWriter);
		
		//now, populate it with a more appropriate Start Date
		String newStartYear = Integer.toString(new Integer(currentYear).intValue() + increment);
		HandSOnTestSuite.searchAuthorizationUI.writeStringToTextField(by, "09/01/"+newStartYear, logFileWriter);
	}//end verifyNewRankStartDate
	
	protected void verifyNewAdminApptStartDate(BufferedWriter logFileWriter, String suffix) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("**** method verifyNewAdminApptStartDate called with suffix '"+suffix+"' ****");
		logFileWriter.newLine();
		
		String currentYear = getCurrentYear(logFileWriter);
		String expectedStartDateText = "09/01/"+currentYear;
		By by = getByForAdjustedLocator(logFileWriter, "AdminApptStartDateTextField", suffix);
		String actualStartDateText = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
		verifyStringValue("New Admin Appointment Start Date", expectedStartDateText, actualStartDateText, logFileWriter);
		
		int increment = 0;
		//now, populate it with a more appropriate Start Date
		if (suffix.equalsIgnoreCase("1"))
			increment = 3;
		String newStartYear = Integer.toString(new Integer(currentYear).intValue() + increment);
		HandSOnTestSuite.searchAuthorizationUI.writeStringToTextField(by, "09/01/"+newStartYear, logFileWriter);
	}//end verifyNewAdminApptStartDate method
	
	protected String getCurrentYear (BufferedWriter logFileWriter) throws Exception{
		By by = By.id("rankStartDate_0_holder");//this is the label containing the year that the previous rank started
		String prevRankStartDate = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		logFileWriter.write("Previous Rank Appointment Start Date is "+prevRankStartDate);
		String currentYear = new String();
		if ((prevRankStartDate != null) &&(! prevRankStartDate.isEmpty()) && (prevRankStartDate.split("/").length > 2)){	
			currentYear = prevRankStartDate.split("/")[2];
		}//end if - it is as expected
		else{
			logFileWriter.write("We cannot split the start date as expected");
			logFileWriter.newLine();
		}//end else
		return currentYear;
	}//end getCurrentYear method
	
	protected void verifyStringValue(String name, String expectedText, String actualText, BufferedWriter logFileWriter) throws Exception{
		if (expectedText.replaceAll("\\s", "").equalsIgnoreCase(actualText.replaceAll("\\s", ""))){
			logFileWriter.write(name+" has value '"+expectedText+"', as expected");
			logFileWriter.newLine();
		}//end if - it is as expected
		else{
			String message = "ERROR: "+name+" was expected to have value '"+expectedText
					+"', but the actual text is '"+actualText+"';\n";
			verificationErrors.append(message);
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
		}//end else - error condition
	}//end verifyStringValue method
	
	protected static void saveAfterClose(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method saveAfterClose called ****");
		logFileWriter.newLine();
		
		WebElement closeButton = UIController.driver.findElement(By.id("buttonClose"));
		closeButton.click();
		Thread.sleep(6000);
		closeNotificationDialog(logFileWriter);

		if ( UIController.switchWindowByURL("processManageFaculty.do", logFileWriter)){
			logFileWriter.write("It didn't close successfully the first time - trying again");
			logFileWriter.newLine();
			try {
				saveAfterClose(logFileWriter);
			}
			catch(NoSuchElementException e) {
				logFileWriter.write("Couldn't get to the close button - closing the window the crude waye");
				logFileWriter.newLine();
				UIController.driver.close();
				Thread.sleep(4000);
			}
		}//end if - it didn't close - try again
		else{
			logFileWriter.write("The window closed successfully the first time - no need to close it again");
			logFileWriter.newLine();
		}//it closed - no problem
		
	}//end saveAfterClose method
	
	protected static void closeNotificationDialog(BufferedWriter logFileWriter) throws Exception{
		try{
			if (UIController.switchWindowByURL(UIController.notificationDialogURL, logFileWriter)){
				Thread.sleep(2000);
				logFileWriter.write("Got the notification dialog");
				logFileWriter.newLine();
			}//end if
			else{
				logFileWriter.write("Could not get the notification dialog");
				logFileWriter.newLine();
				Thread.sleep(2000);
			}//end else
			UIController.driver.findElement(By.id("mybutton")).click();
			Thread.sleep(3000);
		}//end try
		catch(Exception e){
			logFileWriter.write("Could not acquire notification dialog because of "+e.getMessage());
			logFileWriter.newLine();
		}//end catch		
		Thread.sleep(6000);
		if ( UIController.switchWindowByURL(UIController.notificationDialogURL, logFileWriter)){
			logFileWriter.write("Notification dialog didn't close successfully the first time - trying again");
			logFileWriter.newLine();
			closeNotificationDialog(logFileWriter);
		}//end if - it didn't close - try again
		else{
			logFileWriter.write("Notification dialog closed successfully");
			logFileWriter.newLine();
		}//end else - no problem, acquired and closed the notification dialog
	}//end closeNotificationDialog method
	
	protected void updateJointAppointment (BufferedWriter logFileWriter, int i) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("*** method updateJointAppointment called with index "+i+" ****");
		logFileWriter.newLine();
		
		String currentURL = UIController.driver.getCurrentUrl();
		
		//get the start date
		By by = By.id("apptStartDate_0");
		String apptStartDate = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
		String[] dateComponents = apptStartDate.split("/");
		String year = Integer.toString(new Integer(dateComponents[2]).intValue() + 3);
		String updatedStartDate = dateComponents[0]+"/"+dateComponents[1]+"/"+year;
		
		//click on the Update Appointment button and switch to the window with it
		by = By.linkText("Update Appointment");
		if (UIController.waitAndClick(by)){
			logFileWriter.write("Got the Update Appointment Button");
			logFileWriter.newLine();
		}
		else {
			String message = "Could not get the Update Appointment Button";
			logFileWriter.write(message);
			logFileWriter.newLine();
			logFileWriter.close();
			fail(message);
		}//end else
		//WebElement updateApptButton = UIController.driver.findElement(by);
		//updateApptButton.click();
		Thread.sleep(3000);
		if (! UIController.switchWindowByURL(UIController.updateAppointmentURL, logFileWriter))
			throw new Exception("Could not switch to the Update Appointment Dialog");

		//select a Reason, and a start date, and click "OK" to confirm
		by = By.id("reason");
		UIController.selectElementInMenu(by, "Appointment Continuing Term");
		Thread.sleep(2000);
		
		by = By.id("effectiveDate");
		HandSOnTestSuite.searchAuthorizationUI.writeStringToTextField(by, updatedStartDate, logFileWriter);
		Thread.sleep(2000);
		
		by = By.id("mybutton");
		UIController.waitAndClick(by);
		Thread.sleep(3000);
		
		//switch back to the original Faculty Record
		if (! UIController.switchWindowByURL(currentURL, logFileWriter))
			throw new Exception("Could not switch back to the current Faculty Record URL");
		//verify that the new fields are as they should be.
		verifyUpdatedAppointmentFields(logFileWriter, i);
	}//end updateJointAppointment method
	
	protected void verifyUpdatedAppointmentFields (BufferedWriter logFileWriter, int i) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method verifyUpdatedAppointmentFields called with index "+i+" ****");
		logFileWriter.newLine();
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Current Appt Tab", "TopApptNumberLabel", logFileWriter);
		WebElement topApptNumberLabel = UIController.driver.findElement(by);
		String apptNumber = topApptNumberLabel.getText();
		if (apptNumber.equalsIgnoreCase("1.2.2")){
			logFileWriter.write("New Joint Appointment Number is 1.2.2, as expected");
			logFileWriter.newLine();
		}//it is as expected
		else{
			String message = "ERROR: Joint Appointment Number was expected to be 1.2.2, but was actually "+ apptNumber+";\n";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
			System.out.println(message);
		}//error condition

	}//end verifyUpdatedAppointmentFields method
	
	protected void verifyWarningMessages(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method AddJointAppointment called ****");
		logFileWriter.newLine();
		
		String expectedWarningText = "Info: 1.2: FTE total should be between 0 and 100 at a given time.";
		By by = By.cssSelector("td.errorMessage");
		WebElement firstWarning = UIController.driver.findElement(by);
		String firstWarningText = firstWarning.getText();
		logFileWriter.write("Got the first warning - text is "+firstWarningText);
		logFileWriter.newLine();
		if(firstWarningText.equalsIgnoreCase(expectedWarningText)){
			logFileWriter.write("First warning line is as expected");
			logFileWriter.newLine();
		}
		else{
			String message = "ERROR: Expected warning text is "+expectedWarningText+", but it was actually '"+firstWarningText+"'";
			verificationErrors.append(message);
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
		}//end else
		
		expectedWarningText = "Info: 1.2: FTE total should be between 0 and 100 at a given time.";
		by = By.xpath("//table[2]/tbody/tr[2]/td");
		WebElement secondWarning = UIController.driver.findElement(by);
		String secondWarningText = firstWarning.getText();
		logFileWriter.write("Got the second warning - text is "+secondWarningText);
		logFileWriter.newLine();
		if(firstWarningText.equalsIgnoreCase(expectedWarningText)){
			logFileWriter.write("Second warning line is as expected");
			logFileWriter.newLine();
		}
		else{
			String message = "ERROR: Expected warning text is "+expectedWarningText+", but it was actually '"+secondWarningText+"'";
			verificationErrors.append(message);
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
		}//end else
		
	}//end verifyWarningMessages
	
	protected void AddJointAppointment (BufferedWriter logFileWriter, int i) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("*** method AddJointAppointment called with index "+i+" ****");
		logFileWriter.newLine();
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Current Appt Tab", "AddJointApptButton", logFileWriter);
		WebElement addJointApptButton = UIController.driver.findElement(by);
		addJointApptButton.click();
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Current Appt Tab", "TopApptNumberLabel", logFileWriter);
		WebElement topApptNumberLabel = UIController.driver.findElement(by);
		String apptNumber = topApptNumberLabel.getText();
		if (apptNumber.equalsIgnoreCase("1.1.2")){
			logFileWriter.write("New Joint Appointment Number is 1.1.2, as expected");
			logFileWriter.newLine();
		}//it is as expected
		else{
			String message = "ERROR: Joint Appointment Number was expected to be 1.1.2, but was actually "+ apptNumber+";\n";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
			System.out.println(message);
		}//error condition
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Current Appt Tab", "ApptTypeSelect", logFileWriter);
		UIController.selectElementInMenu(by, "Joint");
		fillInFTE(logFileWriter, "50");
		fillInDeptInfo(logFileWriter);
		fillInApptReason(logFileWriter);
		fillInBilletInfo(logFileWriter);
	}//end AddJointAppointment method
	
	protected void fillInBilletInfo (BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method fillInBilletInfo called ****");
		logFileWriter.newLine();
		
		Thread.sleep(2000);
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Current Appt Tab", "BilletNumberTextField", logFileWriter);
		HandSOnTestSuite.searchAuthorizationUI.writeStringToTextField(by, "tbd", logFileWriter);
		Thread.sleep(2000);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Current Appt Tab", "BilletNotesTextField", logFileWriter);
		HandSOnTestSuite.searchAuthorizationUI.writeStringToTextField(by, "Joint Appointment Note", logFileWriter);
		Thread.sleep(2000);
	}//end fillInBilletInfo method
	
	
	protected void fillInApptReason (BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method fillInApptReason called ****");
		logFileWriter.newLine();
		
		Thread.sleep(2000);
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Current Appt Tab", "ApptReasonSelect", logFileWriter);
		UIController.selectElementInMenu(by, "Appointment");
		Thread.sleep(2000);
	}//end fillInApptReason method
	
	protected void fillInFTE (BufferedWriter logFileWriter, String percent) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method fillInFTE called ****");
		logFileWriter.newLine();
		
		Thread.sleep(2000);
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Current Appt Tab", "FTEPctTextField", logFileWriter);
		HandSOnTestSuite.searchAuthorizationUI.writeStringToTextField(by, percent, logFileWriter);
		Thread.sleep(2000);
		if (percent.equals("50"))
			by = by.id("fte_0_1");
		else
			by = by.id("fte_0_0");
		WebElement ftePercentTextfield = UIController.driver.findElement(by);
		ftePercentTextfield.clear();
		ftePercentTextfield.sendKeys(percent);
		//HandSOnTestSuite.searchAuthorizationUI.writeStringToTextField(by, percent, logFileWriter);
		Thread.sleep(2000);
		if (percent.equals("50"))
			by = by.id("sa_0_1");
		else
			by = by.id("sa_0_0");
		WebElement sabbaticalAccrualTextfield = UIController.driver.findElement(by);
		sabbaticalAccrualTextfield.clear();
		sabbaticalAccrualTextfield.sendKeys(percent);
		//HandSOnTestSuite.searchAuthorizationUI.writeStringToTextField(by, percent, logFileWriter);
		Thread.sleep(2000);
	}//end fillInFTE method
	
	protected void fillInDeptInfo (BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method fillInDeptInfo called ****");
		logFileWriter.newLine();
		
		Thread.sleep(2000);
		By by = By.id("dept_0_1");
		String primaryDept = HandSOnTestSuite.searchAuthorizationUI.getSelectedTextForBy(by, "Primary Appointment Department Selection", logFileWriter);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Current Appt Tab", "DeptSelect", logFileWriter);
		if (primaryDept.equalsIgnoreCase("Anthropology"))
			UIController.selectElementInMenu(by, "Biology");
		else UIController.selectElementInMenu(by, "Anthropology");
		Thread.sleep(2000);
	}//end fillInDeptInfo method
	
	protected void saveData(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("*** method saveData called ****");
		logFileWriter.newLine();
		
		boolean saveFailed = true;
		int attempt = 1;
		while (saveFailed) {
			logFileWriter.write("Attempting to save - attempt #"+attempt);
			logFileWriter.newLine();
			
			//all of this is centered around finding and clicking the Save button
			By by = By.id("buttonSubmit");
			WebElement saveButton = UIController.driver.findElement(by);
			Thread.sleep(2000);
			UIController.scrollElementIntoView(by, logFileWriter);
			Thread.sleep(2000);
			saveButton.click();
			Thread.sleep(6000);
			attempt++;
			//this next block of code is about dismissing the alert that comes up
			try{
				Alert alert = null;
				alert = UIController.driver.switchTo().alert();
				String alertText = alert.getText();
				logFileWriter.write("Got the alert dialog - text is "+alertText);
				logFileWriter.newLine();
				try {
				alert.accept();
				}
				catch(Exception e) {
					logFileWriter.write("Could not accept the alert because of Exception "+e.getMessage());
					logFileWriter.newLine();
				}
				logFileWriter.newLine();
				if (alertText.contains("successfully")) {
					saveFailed = false;
					break;
				}//end if
				else {
					String message = "HANDSON-3666 is still an issue - Save unexpectedly failed even though there were no critical data errors - alert text is '"+alertText+"'\n";
					logFileWriter.write(message);
					logFileWriter.newLine();
					verificationErrors.append(message);
				}//end else - we will fail the test because the save fails unexpectedly
			}//end try clause
			catch(NoAlertPresentException e) {
				logFileWriter.write("Did not get the alert dialog.  There is no alert present");
				logFileWriter.newLine();
			}//end catch clause
		}//end while
	}//end method saveData
	
	protected void fillInAdminApptData (BufferedWriter logFileWriter, int i, String whichAppt) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("*** method fillInAdminApptData called with index "+i+" ****");
		logFileWriter.newLine();
		String suffix = "0";
		int increment = 0;
		if (whichAppt.equalsIgnoreCase("second")){
			suffix = "1";
			increment = 3;
		}
		verifyNewAdminApptStartDate(logFileWriter, suffix);
		String startDate = verifyStartDateFormat(logFileWriter, i, suffix);
		fillInEndDate(startDate, logFileWriter, suffix);
		fillInRole(logFileWriter, i, suffix);
		fillInDepartment(logFileWriter, suffix);
		fillInSabbaticalAccrual(logFileWriter, suffix);
		fillInNotes(logFileWriter, suffix);
	}//end fillInAdminApptData method
	
	protected By getByForAdjustedLocator(BufferedWriter logFileWriter, String name, String suffix) throws Exception{
		String locator = 
				HandSOnTestSuite.searchAuthorizationUI.getLocatorByName(myWorksheetName, name, logFileWriter);
		String amendedLocator = locator.replaceFirst("0", suffix);	
		
		if (suffix.equalsIgnoreCase("0")){
			logFileWriter.write("Locator being returned is "+locator);
			logFileWriter.newLine();
			return HandSOnTestSuite.searchAuthorizationUI.getByForLocator(locator, logFileWriter);
		}
		else{
			logFileWriter.write("Locator being returned is "+amendedLocator);
			logFileWriter.newLine();
			return HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
		}//end else
	}//end getByForAdjustedLocator method
	
	protected void fillInNotes (BufferedWriter logFileWriter, String suffix) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method fillInNotes called ****");
		logFileWriter.newLine();
		//HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Current Appt Tab", "AdminApptNotesTextArea", logFileWriter);
		By by = getByForAdjustedLocator(logFileWriter, "AdminApptNotesTextArea", suffix);
		WebElement adminNotesTextField = UIController.driver.findElement(by);
		adminNotesTextField.clear();
		adminNotesTextField.sendKeys("Administrative Appointment Note");
	}//end fillInNotes method

		protected void fillInSabbaticalAccrual (BufferedWriter logFileWriter, String suffix) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method fillInSabbaticalAccrual called ****");
		logFileWriter.newLine();
		By by = getByForAdjustedLocator(logFileWriter, "AdminApptSabbAccrualTextField", suffix);
				//HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Current Appt Tab", "AdminApptSabbAccrualTextField", logFileWriter);
		WebElement addlSabbAccrualTextField = UIController.driver.findElement(by);
		addlSabbAccrualTextField.clear();
		addlSabbAccrualTextField.sendKeys("1");
	}//end fillInSabbaticalAccrual method
	
	protected void fillInRole (BufferedWriter logFileWriter, int i, String suffix) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method fillInRole called - will apply role "+adminApptTitles[i]+" ****");
		logFileWriter.newLine();
		//By adminApptRole = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Current Appt Tab", "AdminApptRoleSelect", logFileWriter);
		By adminApptRole = getByForAdjustedLocator(logFileWriter, "AdminApptRoleSelect", suffix);
		UIController.selectElementInMenu(adminApptRole, adminApptTitles[i]);
		Thread.sleep(3000);
	}//end fillInRole method
	
	
	
	protected void fillInDepartment (BufferedWriter logFileWriter, String suffix) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method fillInDepartment called ****");
		logFileWriter.newLine();
		By assignedDeptSelect = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Current Appt Tab", "DeptSelect", logFileWriter);
		String department = HandSOnTestSuite.searchAuthorizationUI.getSelectedTextForBy(assignedDeptSelect, "Primary Appointment Dept", logFileWriter);
		By adminApptDeptSelect = getByForAdjustedLocator(logFileWriter, "AdminApptDeptSelect", suffix);
		//By adminApptDeptSelect = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Current Appt Tab", "AdminApptDeptSelect", logFileWriter);
		UIController.selectElementInMenu(adminApptDeptSelect, department);
	}//end fillInDepartment method
	
	protected void fillInEndDate (String startDate, BufferedWriter logFileWriter, String suffix) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method fillInEndDate called with Start Date "+startDate+" ****");
		logFileWriter.newLine();
		String[] dateComponents = startDate.split("/");
		if (dateComponents.length != 3){
			String message = "ERROR - There are not three components to the start date;\n";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
			System.out.println(message);
			return;
		}//end if
		String endYear = Integer.toString((Integer.parseInt(dateComponents[2]) + 3));
		String endDate = dateComponents[0]+"/"+dateComponents[1]+"/"+endYear;
		//By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Current Appt Tab", "AdminApptEndDateTextField", logFileWriter);
		By by = getByForAdjustedLocator(logFileWriter, "AdminApptEndDateTextField", suffix);
		WebElement endDateTextField = UIController.driver.findElement(by);
		endDateTextField.clear();
		Thread.sleep(1000);
		endDateTextField.sendKeys(endDate);
		Thread.sleep(3000);
	}//end fillInEndDate method
	
	protected String verifyStartDateFormat (BufferedWriter logFileWriter, int i, String suffix) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method verifyStartDateFormat called with index "+i+" ****");
		logFileWriter.newLine();
		String locator = "pOrgStartDate_"+suffix;
		By by = By.id(locator);
		WebElement textfield = null;
		if (UIController.isElementPresent(by)){
			textfield = UIController.driver.findElement(by);
		}//end if 
		else{
			String message = "Could not get the Administrative Appointment open;\n";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
			System.out.println(message);
			return new String();//get out
		}//end outer else
		String actualStartDateText = textfield.getAttribute("value");
		//if (actualStartDateText.contains("09") && actualStartDateText.contains("01")){
		if (actualStartDateText.contains("09/01")){
			logFileWriter.write("Text of Faculty "+Names[i]+" contains 09/01 as expected");
			logFileWriter.newLine();
		}//end if
		else{
			String message = "ERROR - Faculty "+Names[i]+" does not contain 09/01 as expected - value is '"+actualStartDateText+"';\n";
			System.out.println(message);
			verificationErrors.append(message);
			logFileWriter.write(message);
			logFileWriter.newLine();
		}//end else
		return actualStartDateText;
	}//end verifyStartDateFormat method
	
	protected void openNewAdministrativeAppt(BufferedWriter logFileWriter) throws Exception{
		By by = By.linkText("Add Administrative Appointment");	
		UIController.waitForElementPresent(by);
		Thread.sleep(2000);
		UIController.scrollElementIntoView(by, logFileWriter);
		if (! UIController.waitAndClick(by)){
			String message = "ERROR - could not click on the 'Add Administrative Appointment' button;\n";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
			throw new Exception (message);
		}//end if
	}//end openNewAdministrativeAppt method
	
	protected static String[] selectInitialFaculty(StringBuffer verificationErrors, int maxIndex, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** selectInitialFaculty method called ***");
		logFileWriter.newLine();

		if (! UIController.switchWindowByURL(UIController.startPage, logFileWriter)){
			logFileWriter.write("Could not switch to the start page - attempting Administration Page");
			logFileWriter.newLine();
			if (! UIController.switchWindowByURL(UIController.administrationURL, logFileWriter)){
				logFileWriter.write("Could not get to the Adminstration Page either - exiting");
				logFileWriter.newLine();
				return new String[0];
			}//end if
			else {
				if (UIController.clickLinkAndWait("Operations", logFileWriter)){
					logFileWriter.write("Clicked on the link to go to the Operations tab");
					logFileWriter.newLine();
				}//end if
				else{
					logFileWriter.write("Could not click on the link to go to the Operations tab");
					logFileWriter.newLine();
					return new String[0];
				}//end else
			}//end else
		}//end if
		
		//look up an SA with no Recruitment - it should be Active, with reasons either Search or Pending
		//open the Search Authorization Page
		verificationErrors.append(UIController.processPageByLink("Recruitment Search", logFileWriter));
		Thread.sleep(3000);

		if (! UIController.switchWindowByURL(UIController.recruitmentLookupURL, logFileWriter)) {
			String message = "Could not get to the Recruitment Search Page the first time, trying again...";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			
			Thread.sleep(3000);
			UIController.switchToStartPage(logFileWriter);
			Thread.sleep(3000);
			verificationErrors.append(UIController.processPageByLink("Recruitment Search", logFileWriter));
			Thread.sleep(6000);
			if (! UIController.switchWindowByURL(UIController.recruitmentLookupURL, logFileWriter)) {
				message = "Could not get to the Recruitment Search Page the second time, failing this test....";
				logFileWriter.write(message);
				logFileWriter.newLine();
				logFileWriter.close();
				System.out.println(message);
				fail(message);
			}//end inner if - we couldn't get to the Recruitment Lookup page again
		}//end if
		refreshAndSelectLastPage(logFileWriter);
		try{
			UIController.driver.findElement(By.linkText("LAST")).click();
		}//end try clause
		catch(Exception e){
			refreshAndSelectLastPage(logFileWriter);
			try {
				UIController.driver.findElement(By.linkText("LAST")).click();
			}//end inner try
			catch(Exception e2) {
				String message = "Couldn't acquire the 'LAST' link, failing this test....";
				logFileWriter.write(message);
				logFileWriter.newLine();
				logFileWriter.close();
				System.out.println(message);
				fail(message);
			}//end inner catch
		}//end catch clause
		Thread.sleep(3000);
		
		String baseIDLocator = "//div[5]/table/tbody/tr/td/a";
		String baseNameLocator = "//div[5]/table/tbody/tr/td[2]/a";
		UIController.waitForElementPresent(By.xpath(baseIDLocator));
		int bottomRow = HandSOnTestSuite.searchAuthorizationUI.getBottomRow(baseIDLocator, new String(), logFileWriter, true);
		if (bottomRow < maxIndex) {//there are fewer initial SA's than we need in the LAST page, so go to the PREVIOUS page
			logFileWriter.write("Couldn't find enough Faculty on the LAST page, so we're going to the PREVIOUS page");
			logFileWriter.newLine();
			try{
				UIController.driver.findElement(By.linkText("PREVIOUS")).click();
				Thread.sleep(6000);
				bottomRow = HandSOnTestSuite.searchAuthorizationUI.getBottomRow(baseIDLocator, new String(), logFileWriter, true);
			}//end try clause
			catch(Exception e){
				String message = "Couldn't acquire the 'PREVIOUS' link, failing this test....";
				logFileWriter.write(message);
				logFileWriter.newLine();
				logFileWriter.close();
				System.out.println(message);
				fail(message);
			}//end if
		}//end if

		ArrayList<String> IDs = new ArrayList<String>();
		for (int row=1; row<=bottomRow; row++){
			//String IDLocator = baseIDLocator;
			String NameLocator = baseNameLocator;
			String name = new String();
			if (row>1){
				//IDLocator = baseIDLocator.replaceFirst("/tr", "/tr[" + row + "]");
				NameLocator = baseNameLocator.replaceFirst("/tr", "/tr[" + row + "]");
			}//end if
			name = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(By.xpath(NameLocator), logFileWriter);
			boolean untested = isFacultyUntested(logFileWriter, NameLocator, name);
			if ((! name.isEmpty()) && untested){
				//By idBy = By.xpath(IDLocator);
				By nameBy = By.xpath(NameLocator);
				String ID = /*HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(idBy, logFileWriter) 
						+ "-"+*/
						HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(nameBy, logFileWriter);
				IDs.add(ID);
			}//end if - add this to the array
		}//end for loop
		String[] NameArray = new String[IDs.size()];
		logFileWriter.write("The following Recruitments-Faculty are going to be examined: ");
		logFileWriter.newLine();
		for (int i=0; i<IDs.size(); i++){
			NameArray[i] = IDs.get(i);
			logFileWriter.write(NameArray[i]+"; ");
		}//end for loop
		return NameArray;
	}//end selectInitialFaculty method
	
	protected static void refreshAndSelectLastPage(BufferedWriter logFileWriter) throws Exception{
		Thread.sleep(3000);
		UIController.driver.navigate().refresh();
		Thread.sleep(6000);
		//first, clear everything that was set before by any other test
		UIController.driver.findElement(By.linkText("CLEAR")).click();

		UIController.waitAndSelectElementInMenu(By.name("lookupDTO.recruitmentStatus"), "Close");
		Thread.sleep(1000);		
		UIController.waitAndSelectElementInMenu(By.name("lookupDTO.offerStatusCd"), "Accepted");
		
		UIController.waitAndClick(By.id("mybutton"));
		Thread.sleep(3000);		
	}//end refreshAndSelectLastPage method
	
	protected static boolean isFacultyUntested (BufferedWriter logFileWriter, String NameLocator, String name) throws Exception{
		boolean untested = true;
		
		//switch to the Faculty window that has been opened - use partial URL showPersonReference.do?personUnvlId=HS_LastName2111_2
		String errMsg = UIController.processPageByLink(name, logFileWriter);
		if (errMsg.length() > 0) {
			logFileWriter.write(errMsg);
			logFileWriter.newLine();
		}//end if
		Thread.sleep(6000);
		if (UIController.switchWindowByURL(UIController.existingFacultyURL+"HS_"+name.split(" ")[1], logFileWriter)){
			logFileWriter.write("Successfully switched to the record for Faculty member "+name);
			logFileWriter.newLine();
		}//end if
		else{
			String message = "Could not switch to the record for Faculty member '"+name+"' the first time - re-trying\n";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			if (! UIController.switchWindowByURL(UIController.recruitmentLookupURL, logFileWriter)) {
				message = "Could not get to the Recruitment Search Page the second time, failing this test....";
				logFileWriter.write(message);
				logFileWriter.newLine();
				System.out.println(message);
				verificationErrors.append(message);
				fail(message);
			}//end inner if - we couldn't get to the Recruitment Lookup page again
			else {
				verificationErrors.append(UIController.processPageByLink(name, logFileWriter));
				Thread.sleep(6000);
				if (UIController.switchWindowByURL(UIController.existingFacultyURL+"HS_"+name.split(" ")[1], logFileWriter)){
					logFileWriter.write("Successfully switched to the record for Faculty member "+name);
					logFileWriter.newLine();
				}//end if
				else{
					message = "Could not switch to the record for Faculty member '"+name+"' the second time - re-trying\n";
					logFileWriter.write(message);
					logFileWriter.newLine();
					System.out.println(message);
				}//end inner else
			}//end outer else

		}//end else
		Thread.sleep(3000);
		//switch to the Demographics tab
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "DemographicsTabLink", logFileWriter);
		UIController.waitAndClick(by);
		//verify that the key String value "This faculty member has already been tested." is present
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Demographics Tab", "ResearchInterestsTextField", logFileWriter);
		String indicator = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
		logFileWriter.write("Indicator of Faculty Member "+name+" is as follows: '"+indicator+"';");
		logFileWriter.newLine();
		//set untested = false if and only if it is present
		if (indicator.equalsIgnoreCase(testIndicatorString)){
			logFileWriter.write(testIndicatorString + "  This faculty member will not be used in testing");
			logFileWriter.newLine();
			untested = false;
		}// end if - determine if the Faculty member is already used or not
		//close the Faculty Record
		saveAfterClose(logFileWriter);
		UIController.switchWindowByURL(UIController.recruitmentLookupURL, logFileWriter);
		return untested;
	}//end isFacultyAlreadyTested method
	
	protected void writeTestIndicatorString(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method writeTestIndicatorString called ****");
		logFileWriter.newLine();
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(myWorksheetName, "DemographicsTabLink", logFileWriter);
		UIController.waitAndClick(by);
		//verify that the key String value "This faculty member has already been tested." is present
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Demographics Tab", "ResearchInterestsTextField", logFileWriter);
		HandSOnTestSuite.searchAuthorizationUI.writeStringToTextField(by, testIndicatorString, logFileWriter);
		logFileWriter.write("Test indicator String successfully written");
		logFileWriter.newLine();
	}//end writeTestIndicatorString method
	
}//end Class
