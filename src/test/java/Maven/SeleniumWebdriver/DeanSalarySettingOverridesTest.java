package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DeanSalarySettingOverridesTest {
	protected static String testName;
	protected static String testCaseNumber;
	protected static String testDescription;
	protected static String testEnv;
	protected static StringBuffer verificationErrors;
	protected BufferedWriter logFileWriter;
	protected SSExcelReader myReader;
	protected static final String sheetName = TestFileController.DeanSalarySettingWorksheetName;

	@Before
	public void setUp() throws Exception {
		testName = "DeanSalarySettingOverridesTest";
		testEnv = "Test";
		testCaseNumber = "058";
		testDescription = "Test of Overrides for Dean SSWs";
	}//end setUp method

	@After
	public void tearDown() throws Exception {
		TestFileController.writeTestResults(testEnv, testCaseNumber, testName, testDescription, verificationErrors);
		logFileWriter.newLine();
		logFileWriter.close();
	}//end tearDown method


	@Test
	public void test() throws Exception{
		verificationErrors = new StringBuffer();
		String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		logFileWriter = TestFileController.getLogFileWriter(logFileName);
		logFileWriter.write("The Dean Salary Setting Worksheet will be examined ");
		logFileWriter.newLine();
		
		ReportRunner.downloadDeanSSW (ReportRunner.referenceWhichValues, logFileWriter);
		
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("**** Now attempting to get Overridden Faculty for the Dean Salary Setting Report ****");
		logFileWriter.newLine();
		
		//all faculty from all Clusters/Departments who are overridden to Dean SSW appear here
		myReader 
			= new SSExcelReader(TestFileController.directory + TestFileController.DeanSalarySettingReportName, 
					TestFileController.DeanSalarySettingWorksheetHeaderRow);
		verifyValuesInWorksheetBody();
		
		//now, adjust the reader so that we are reading from the Exclusion section
		int rowNum = myReader.getCellRowNum(sheetName, 0, 
				SalarySettingManagementUIController.overrideSectionHeading);
		myReader.setRowForColumnNames(rowNum);
		logFileWriter.write(" * New row for column headers is adjusted to "+rowNum+" *");
		logFileWriter.newLine();
		verifyValuesInExclusionSection();
		
	}//end test method
	
	private void verifyValuesInWorksheetBody() throws Exception{
		logFileWriter.write("*** method verifyValuesInWorksheetBody called ***");
		logFileWriter.newLine();
		ArrayList<String[]> expectedFacultyValues 
			= HandSOnTestSuite.salarySettingMgrTabUI.getOverridesWithAttributeValue("WhichWorksheet", "Dean Worksheet", logFileWriter);
		logFileWriter.write("Retrieved a list of " +expectedFacultyValues.size() + " overrides from the UI");
		logFileWriter.newLine();

		String[] expectedColNames = {"Name", "Cluster", "Department", "16-17 Description"};
		for (int i=0; i<expectedFacultyValues.size(); i++){
			String[] expectedValues = expectedFacultyValues.get(i);
			String name = expectedValues[0];
			logFileWriter.write("Now working with override #"+i+", with name "+name);
			logFileWriter.newLine();

			int rowNum = myReader.getCellRowNum(sheetName, expectedColNames[0], name);
			if (rowNum != -1){
				logFileWriter.write("Found name "+name+" at row "+rowNum);
				logFileWriter.newLine();
				for (int j=1; j<expectedColNames.length; j++){
					if ((j == 1) || (j == 2)){
						compareStrings(expectedValues[j], myReader.getCellData(sheetName, expectedColNames[j], rowNum), false, logFileWriter);
					}//end if - it's not a Description
					else compareStrings (expectedValues[4], myReader.getCellData(sheetName, expectedColNames[j], rowNum), true, logFileWriter);
				}//end inner for - iterating through the column names
			}//end if 
			else {
				logFileWriter.write("Couldn't find name "+name+", in worksheet "+sheetName);
				logFileWriter.newLine();
			}//end else - don't even try to find the other attributes in this case b/c we missed it		
		}//end for loop - iterating through all of the overrides
	}//end verifyValuesInWorksheetBody method
	
	private void verifyValuesInExclusionSection() throws Exception{
		logFileWriter.write("*** method verifyValuesInExclusionSection called ***");
		logFileWriter.newLine();
		ArrayList<String[]> expectedFacultyValues 
			= HandSOnTestSuite.salarySettingMgrTabUI.getOverridesWithAttributeValue("WhichWorksheet", "Dean Worksheet", logFileWriter);
		logFileWriter.write("Retrieved a list of " +expectedFacultyValues.size() + " overrides from the UI");
		logFileWriter.newLine();

		String[] expectedColNames = {"Name", "Cluster", "Department", "Override", "Description"};
		for (int i=0; i<expectedFacultyValues.size(); i++){
			String[] expectedValues = expectedFacultyValues.get(i);
			String name = expectedValues[0];
			logFileWriter.write("Now working with override #"+i+", with name "+name);
			logFileWriter.newLine();
			
			int rowNum = myReader.getCellRowNum(sheetName, expectedColNames[0], name);
			if (rowNum != -1){
				logFileWriter.write("Found name "+name+" at row "+rowNum);
				logFileWriter.newLine();
			}//end if - we found the name
			else {
				logFileWriter.write("Couldn't find name "+name+", in worksheet "+sheetName);
				logFileWriter.newLine();
				for (int j=1; j<expectedColNames.length; j++){
					if (j==4){//if it's a Description
						compareStrings (expectedValues[4], myReader.getCellData(sheetName, expectedColNames[j], rowNum), true, logFileWriter);
					}//see that the sheet contains the UI Description
					else{//otherwise, not a Description
						compareStrings (expectedValues[j], myReader.getCellData(sheetName, expectedColNames[j], rowNum), false, logFileWriter);
					}//do a String-to-String comparison, Strings should be equal
				}//end inner for - iterating through all of the expected column names
			}//end else - don't even try to find the other attributes in this case b/c we missed it		
		}//end for loop - iterating through the overrides
	}//end verifyValuesInExclusionSection method
	
	private void compareStrings(String expectedValue, String actualValue, 
			boolean shouldContain, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("Now comparing expected value "+expectedValue
				+" with actual value "+actualValue);
		logFileWriter.newLine();
		boolean match = false;
		if (shouldContain){
			if (actualValue.contains(expectedValue))
				match = true;
		}//end if - this is a Description
		else {
			if (actualValue.equalsIgnoreCase(expectedValue))
				match = true;
		}//end else - this is not a Description
		if (match){
			logFileWriter.write("Match: expected value: " + expectedValue +", actual value: "+actualValue);
			logFileWriter.newLine();
		}//end if
		else{
			String message = "MISMATCH: expected value: " + expectedValue +", actual value: "+actualValue+"; ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
		}//end else - mismatch
	}//end compareStrings method

}//end Class
