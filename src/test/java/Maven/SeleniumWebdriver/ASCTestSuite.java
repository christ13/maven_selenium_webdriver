package Maven.SeleniumWebdriver;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({
	ControlSheetDetailReportASCTest.class,
	ControlSheetPivotStageReportASCTest.class,
	ControlSheetPivotClusterReportASCTest.class,
	ControlSheetPivotCategoryReportASCTest.class,
	ControlSheetPivotSubCategoryReportASCTest.class,
	ControlSheetPivotSocSciClusterReportASCTest.class,
	ControlSheetPivotNatSciClusterReportASCTest.class,
	ControlSheetPivotHumArtsClusterReportASCTest.class,
	PoolAllocationReportASCTest.class,
	DepartmentVerificationReportASCTest.class,
	ProvostBinderListByDepartmentASCTest.class,
	ControlSheetReportAggregateTest.class,
	DeanSalarySettingWorksheetASCTest.class,
	ChairSalarySettingWorksheetHumArtsASCTest.class,
	ChairSalarySettingWorksheetNatSciASCTest.class, 
	ChairSalarySettingWorksheetSocSciASCTest.class,
	ClusterSalarySettingWorksheetHumArtsASCTest.class,
	ClusterSalarySettingWorksheetSocSciASCTest.class,
	ClusterSalarySettingWorksheetNatSciASCTest.class, 
	SchoolSalarySettingReportASCTest.class,
	MasterSalarySettingReportASCTest.class
	})


public class ASCTestSuite {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
	}

}
