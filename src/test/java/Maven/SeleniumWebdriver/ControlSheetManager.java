package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;
import java.math.BigDecimal;
import java.text.NumberFormat;

import org.openqa.selenium.By;

import Maven.SeleniumWebdriver.UIController;

public class ControlSheetManager extends UIController {
	
	public static boolean ExcelReaderToOutputFile = false;
	public static boolean locatorsToOutputFile = false;

	// all of these pertain to the modification of the Salary Setting Increase Percentages
	public static final String[] originalSalaryIncreaseValues = {"10.00", "4.00", "3.00", 
		"4.50", "3.50", "2.00", "9.70", "10.29", "4.50"};
	public static final String[] SalaryIncreaseLabels = {"MeritPoolPercentageAmount", 
		"DepartmentFacultyPercentageAmount", "TerminalYearPercentageAmount", 
		"DepartmentChairPercentageAmount", "SrAssociateDeansPercentageAmount", 
		"MarketPoolPercentageAmount", "GenderPoolPercentageAmount", "TestPoolPercentageAmount", 
		"AlbertEustacePeasmarchFundPercentageAmount"};
	public static final String[] alteredSalaryIncreaseValues = {"12.00", "5.00", "4.00", 
		"7.50", "7.50", "4.00", "9.70", "12.00", "6.50"};
	private static HSSFExcelReader locators = TestFileController.locators;
	public static final String locatorFileSheetName = "Control Sheet";



	/*
	 * This Class executes UI actions on the Control Sheet tab of Salary Setting
	 */
	public ControlSheetManager() {
		// TODO Auto-generated constructor stub

	}
	
	public static boolean openSalarySetting(BufferedWriter logFileWriter) throws Exception{
		  if (UIController.openSalarySetting(logFileWriter)){
			  TestFileController.writeToLogFile("Successfully opened Salary Setting", logFileWriter);
		  }
		  else{ 
			  TestFileController.writeToLogFile("Attempt to open Salary Setting failed: " 
					  +"Couldn't open Salary Setting", logFileWriter);
			  return false;
		  }
		  UIController.switchToControlSheetTab();
		  //for some reason, it takes a while for the page to finish loading, so wait for it
		  Thread.sleep(30000);
		  return true;
	}//end openSalarySetting method
	
	public static void adjustAllPercentageValues(BufferedWriter logFileWriter, String whichValues) throws Exception{
		  if (ControlSheetManager.openSalarySetting(logFileWriter)) 
			  TestFileController.writeToLogFile("Successfully opened Salary Setting", logFileWriter);
		  else{
			  String message = "Could not open Salary Setting - aborting";
			  TestFileController.writeToLogFile(message, logFileWriter);
			  System.out.println(message);
			  return;
		  }
		  for (int i = 0; i<SalaryIncreaseLabels.length; i++){
			  if (whichValues.equalsIgnoreCase(ReportRunner.ALTERED))
				  adjustValue (SalaryIncreaseLabels[i], alteredSalaryIncreaseValues[i]);
			  else if (whichValues.equalsIgnoreCase(ReportRunner.ORIGINAL))
				  adjustValue (SalaryIncreaseLabels[i], originalSalaryIncreaseValues[i]);
			  else {
				  TestFileController.writeToLogFile("Cannot determine what type of value - original or altered", logFileWriter);
				  return;
			  }//end else
		  }//end for
		  HandSOnTestSuite.salarySettingManager.saveSalarySetting();
	}

	
	public static void adjustValue (String label, String value) throws Exception{
		String locator = getLocator(label);
		UIController.waitForElementPresent(By.xpath(locator));
		UIController.driver.findElement(By.xpath(locator)).clear();
		UIController.driver.findElement(By.xpath(locator)).sendKeys(value);
	}
	
	public static String getLocator(String label) throws Exception{
		int rowNum = locators.getCellRowNum(locatorFileSheetName, "Name", label);
		return locators.getCellData(locatorFileSheetName, "Locator", rowNum);
	}

	public static String sumUpSalaryBases(BufferedWriter logFileWriter) throws Exception{
		  StringBuffer verificationErrors = new StringBuffer();
		  UIController.waitForElementPresent(By.xpath("//table[@id='topboxTab']/tbody/tr/td[2]"));
		  String MeritPoolSalaryBase = new String();
		  while (MeritPoolSalaryBase.isEmpty()){
			  Thread.sleep(1000);
			  MeritPoolSalaryBase 
			  = UIController.driver.findElement(By.xpath("//table[@id='topboxTab']/tbody/tr/td[2]")).getText();
		  }//end while
		  TestFileController.writeToLogFile("Merit Pool Salary Base value is determined to be " + MeritPoolSalaryBase, logFileWriter);
		  BigDecimal marketPoolSalaryBaseSum 
		  	= ControlSheetUI.getSalaryBaseDollarSumForPool(ASCTest.sourcePools[ASCTest.MarketPoolIndex], logFileWriter);
		  TestFileController.writeToLogFile ("Sum of Market Pool Designees Salary Base is " + marketPoolSalaryBaseSum.toPlainString(), logFileWriter);
		  BigDecimal marketPoolSalaryBase = ControlSheetUI.getSalaryBaseForPool(ASCTest.sourcePools[ASCTest.MarketPoolIndex], logFileWriter);
		  TestFileController.writeToLogFile ("Market Pool Salary Base is " + marketPoolSalaryBaseSum.toPlainString(), logFileWriter);
		  if (! isApproximatelyEqual(marketPoolSalaryBaseSum.doubleValue(), marketPoolSalaryBase.doubleValue()))
			  verificationErrors.append("Unexpected mismatch - salary Base is " + marketPoolSalaryBase.toPlainString()
					  +", but the sum is " + marketPoolSalaryBaseSum.toPlainString());
		  return verificationErrors.toString();
	}//end sumUpSalaryBases method

	  private static boolean isApproximatelyEqual(double expected, double actual){
		  return ((expected > actual - 2) && (expected < actual + 2));
	  }
	  
	  public static double getSalaryIncreasePercentFromUI(String typeOfRaise, BufferedWriter logFileWriter) throws Exception{
		  TestFileController.writeToLogFile("*** method getSalaryIncreasePercentFromUI called ***", logFileWriter);
		  String locator = new String();
		  if (typeOfRaise.equalsIgnoreCase("Chairs / Directors"))
			  locator = locators.getValueForColumnNameAndValue(locatorFileSheetName, 
					  "DepartmentChairPercentageAmount", "Name", "Locator", logFileWriter, ExcelReaderToOutputFile);
		  else if (typeOfRaise.equalsIgnoreCase("Sr. Associate Deans"))
			  locator = locators.getValueForColumnNameAndValue(locatorFileSheetName, 
					  "SrAssociateDeansPercentageAmount", "Name", "Locator", logFileWriter, ExcelReaderToOutputFile);
		  String rowPercent = ControlSheetUI.getValueOfInputForLocator(locator, logFileWriter, locatorsToOutputFile);
		  if (rowPercent.length() == 0){
			  TestFileController.writeToLogFile("Could not get a valid value for Raise Percent - returning zero",
					  logFileWriter);
			  return 0;
		  }
		  return ((Number)NumberFormat.getNumberInstance(java.util.Locale.US).parse(rowPercent)).doubleValue()/100;
	  }//end getSalaryIncreasePercentFromUI method


}
