package Maven.SeleniumWebdriver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({
	DeptSalarySettingWorksheetEconomicsAPRefValuesTest.class,
	DeptSalarySettingWorksheetHistoryAPRefValuesTest.class,
	DeptSalarySettingWorksheetMathematicsAPRefValuesTest.class,
	DeptSalarySettingWorksheetPhysicsAPRefValuesTest.class,
	DeptSalarySettingWorksheetTheatreAndPerformanceAPRefValuesTest.class
	})

public class DeptSalarySettingWorksheetAPRefValuesTestSuite {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws Exception{
	}
	
}
