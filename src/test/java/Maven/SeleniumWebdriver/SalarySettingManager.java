/**
 * 
 */
package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeSet;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;


/**
 * @author christ13
 *
 */
public class SalarySettingManager extends UIController {
	
	public static final String DeanWorksheetOverrideLabel = "SRASSOCDEAN";
	public static final String ChairWorksheetOverrideLabel = "DEPTCHAIR";

	/**
	 * 
	 */
		public SalarySettingManager() {

		}

	  public static void setAPValuesinGUI(String sheetName) throws Exception{
		  String testName = "Setting AP Values in GUI";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		  BufferedWriter logFileWriter = TestFileController.getLogFileWriter(logFileName);

		  openSalarySetting(logFileWriter);
		  waitForElementPresent(By.xpath("//thead/tr/td[2]/input"));
		  
		  //String Reappointment = driver.findElement(By.xpath("//thead/tr/td[2]/input")).getAttribute("value");
		  String[] APValues = HandSOnTestSuite.referenceValues.getLabels();
		  for (int index = 0; index< APValues.length; index++){
			  setIndividualAPValueInGUI(APValues[index], logFileWriter, sheetName);
		  }
		  //Now, click on the "Save" button
		  waitAndClick(By.id("buttonSubmit"));
		  Thread.sleep(25000);
		  int numberOfAttempts = 0;
		  boolean alertIsPresent = false;
		  while((! alertIsPresent) &&(numberOfAttempts<8)){
			  Thread.sleep(5000);
			  //driver.switchTo().alert().accept();
			  alertIsPresent = isAlertPresent();
			  String message= "Attempt to dismiss the alert is successful "
					  +"(true or false): " + alertIsPresent + "; iteration "+numberOfAttempts++;
			  System.out.println(message);
			  logFileWriter.write(message);
			  logFileWriter.newLine();
		  }
		  logFileWriter.close();
	  }
	  
		public void saveSalarySetting() throws Exception{
			UIController.driver.findElement(By.xpath("//div[7]/input")).click();
			  boolean alertIsPresent = false;
			  int numberOfAttempts = 0;
			  while ((! alertIsPresent) && (numberOfAttempts < 8)){
				  Thread.sleep(5000);
				  //driver.switchTo().alert().accept();
				  alertIsPresent = isAlertPresent();
				  String message= "Attempt to dismiss the alert is successful "
						  +"(true or false): " + alertIsPresent + "; iteration "+numberOfAttempts++;
				  System.out.println(message);
			  }
		}
		

	  public void switchToReportTab() throws Exception{
		  super.switchToReportTab();
		  //before we run the reports, save everything to make sure that we don't get any unnecessary confirmation dialogs
		  waitAndClick(By.xpath("//td/div[2]/input"), 90);
		  boolean alertIsPresent = false;
		  int numberOfAttempts = 0;
		  while(( ! alertIsPresent) &&(numberOfAttempts < 8)){
			  Thread.sleep(5000);
			  alertIsPresent = isAlertPresent();
			  //driver.switchTo().alert().accept();
			  String message= "Attempt to dismiss the alert is successful "
					  +"(true or false): " + alertIsPresent + "; iteration "+numberOfAttempts++;
			  System.out.println(message);
		  }

	  }
	  
	  
	  
	  public static boolean isAlertPresent() {
		  boolean presentFlag = false;
		  try {
		   // Check the presence of alert
		   Alert alert = driver.switchTo().alert();
		   // Alert present; set the flag
		   presentFlag = true;
		   // if present consume the alert
		   alert.accept();
		  } catch (NoAlertPresentException ex) {
		   // Alert not present
		   System.out.println("Could not dismiss alert because of Exception "+ ex.getLocalizedMessage());
		  }
		  return presentFlag;
		 }
	  
	  public static void gotoSalarySettingManagementTab() throws Exception{
		  waitAndClick(By.id("salarySetting_a"));
	  }
	  
	  public static void populateOverrideList() throws Exception{
		  boolean endOfList = false;
		  //gotoSalarySettingManagementTab();
		  int index = 1;
		  String nameLocator = "//tr[3]/td/div/table/tbody/tr["+ index +"]/td";
		  String departmentLocator = "//tbody[@id='roleOverrideBody']/tr["+ index +"]/td[3]/select";
		  String worksheetLocator = "//tr[3]/td/div/table/tbody/tr[" + index +"]/td[4]/select";
		  waitForElementPresent(By.xpath(nameLocator));
		  waitForElementPresent(By.xpath(worksheetLocator));
		  TreeSet<String> deanList = new TreeSet<String>();
		  TreeSet<String> chairList = new TreeSet<String>();
		  while (! endOfList){
			  try{
				  String name = driver.findElement(By.xpath(nameLocator)).getText();
				  String dept = HandSOnTestSuite.codeToDept.get(driver.findElement(By.xpath(departmentLocator)).getAttribute("value"));
				  String label= driver.findElement(By.xpath(worksheetLocator)).getAttribute("value");
				  if (label.equalsIgnoreCase(DeanWorksheetOverrideLabel)) deanList.add(name + "-" +dept);
				  else chairList.add(name + "-" +dept);
			  }//end try clause
			  catch (Exception e){
				  endOfList = true;
			  }//end catch clause
			  //now increment and adjust
			  index++;
			  nameLocator = "//tr[3]/td/div/table/tbody/tr["+ index +"]/td";
			  departmentLocator = "//tbody[@id='roleOverrideBody']/tr["+ index +"]/td[3]/select";
			  worksheetLocator = "//tr[3]/td/div/table/tbody/tr[" + index +"]/td[4]/select";
		  }//end while loop
		  HandSOnTestSuite.overrideList.put(DeanWorksheetOverrideLabel, deanList);
		  HandSOnTestSuite.overrideList.put(ChairWorksheetOverrideLabel, chairList);
		  String testName = "Setting Override values from GUI";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		  BufferedWriter logFileWriter = TestFileController.getLogFileWriter(logFileName);
		  logFileWriter.write("*** method populateOverrideList called ***");
		  logFileWriter.newLine();
		  logFileWriter.write("Override values found are the following:");
		  logFileWriter.newLine();
		  outputOverrideList(DeanWorksheetOverrideLabel, logFileWriter);
		  outputOverrideList(ChairWorksheetOverrideLabel, logFileWriter);
		  logFileWriter.close();
	  }//end populateOverrideList method
	  
	  private static void outputOverrideList(String name, BufferedWriter logFileWriter) throws Exception{
		  logFileWriter.write(name + ": ");
		  TreeSet<String> nameDept = HandSOnTestSuite.overrideList.get(name);
		  Iterator<String> iterator = nameDept.iterator();
		  while (iterator.hasNext())
			  logFileWriter.write(iterator.next() + "; ");
		  logFileWriter.newLine();
	  }
	  
	  public static void setIndividualAPValueInGUI(String name, BufferedWriter logFileWriter, String sheetName) throws Exception{
		  String value = TestFileController.locators.getValueForColumnNameAndValue(sheetName, name, "Name", "Value", logFileWriter, true);
		  String locator = TestFileController.locators.getValueForColumnNameAndValue(sheetName, name, "Name", "Locator", logFileWriter, true);
		  TestFileController.writeToLogFile("Initial " + name + " figure is " + value, logFileWriter);
		  System.out.println("Initial " + name + " figure is " + value);
		  System.out.println("locator value retrieved is '" + locator +"'");
		  driver.findElement(By.xpath(locator)).clear();
		  driver.findElement(By.xpath(locator)).sendKeys(value);

	  }
	  
	  public static void verifyAPValuesInGUI(String sheetName) throws Exception{
		  String testName = "Verifying AP Values in GUI";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = TestFileController.LogDirectoryName +"/" + testName +".txt";
		  BufferedWriter logFileWriter = TestFileController.getLogFileWriter(logFileName);
		  
		  openSalarySetting(logFileWriter);
		  waitForElementPresent(By.xpath("//thead/tr/td[2]/input"));
		  
		  String[] APTypes = HandSOnTestSuite.referenceValues.getLabels();
		  for (int APTypeIndex=0; APTypeIndex < APTypes.length; APTypeIndex++){
			  String APType = APTypes[APTypeIndex];
			  boolean valueChecksOut = HandSOnTestSuite.referenceValues.getStringAmountForAPType(APType).equalsIgnoreCase(getIndividualAPValueInGUI(APType, logFileWriter, sheetName));
			  String message = APType + " value checks out: " + valueChecksOut;
			  TestFileController.writeToLogFile(message, logFileWriter);
		  }
		  logFileWriter.close();
	  }
	  
	  
	  public static String getIndividualAPValueInGUI(String name, BufferedWriter logFileWriter, String sheetName) throws Exception{
		  String locator = TestFileController.locators.getValueForColumnNameAndValue(sheetName, name, "Name", "Locator", logFileWriter, true);
		  return driver.findElement(By.xpath(locator)).getAttribute("value");
	  }
  

}
