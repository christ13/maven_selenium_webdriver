package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ChairSalarySettingExceptionsTest {

	protected static String testName;
	protected static String testCaseNumber;
	protected static String testDescription;
	protected static String testEnv;
	protected static StringBuffer verificationErrors;
	protected BufferedWriter logFileWriter;

	@Before
	public void setUp() throws Exception {
		testName = "ChairSalarySettingExceptionsTest";
		testEnv = "Test";
		testCaseNumber = "057";
		testDescription = "Test of Exceptions for Department SSWs";
	}//end setUp method

	@Test
	public void test() throws Exception{
		verificationErrors = new StringBuffer();
		String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		logFileWriter = TestFileController.getLogFileWriter(logFileName);
		String[] clusters = HandSOnTestSuite.salarySettingMgrTabUI.getClusters("Exception", logFileWriter);
		logFileWriter.write("The following Salary Setting Worksheets will be examined: ");
		logFileWriter.newLine();
		for (int i=0; i<clusters.length; i++){
			clusters[i] = clusters[i].replaceFirst(" Cluster", "");
			String fileName = "Chair Salary Setting Worksheet "+ clusters[i] + ".xlsx";
			logFileWriter.write(fileName);
			logFileWriter.newLine();
		}//end for loop
		
		//download the Chair SSW's
		logFileWriter.write("Now attempting to download these Chair Salary Setting Worksheets:");
		logFileWriter.newLine();
		SalarySettingManager.switchWindowByURL(SalarySettingManager.salarySettingURL, logFileWriter);
		HandSOnTestSuite.salarySettingMgrTabUI.switchToReportTab();
		ReportRunner.downloadChairSSWs(clusters, ReportRunner.referenceWhichValues, logFileWriter);

		for (int i=0; i<clusters.length; i++){
			logFileWriter.newLine();
			logFileWriter.newLine();
			logFileWriter.write("**** Now attempting to get Exceptions for Faculty for Cluster " +clusters[i]+" ****");
			logFileWriter.newLine();
			
			HashMap<String, String[]> clusterFaculty 
				= HandSOnTestSuite.salarySettingMgrTabUI.getFacultyForCluster(clusters[i], "Exception", "Chair", logFileWriter);
			Iterator clusterIterator = clusterFaculty.entrySet().iterator();
			while(clusterIterator.hasNext()){
				Map.Entry pair = (Map.Entry)clusterIterator.next();
				String dept = (String)pair.getKey();
				logFileWriter.write("Now working with Department " + dept);
				logFileWriter.newLine();
				String[] faculty = (String[])pair.getValue();
				for (int j=0; j<faculty.length; j++){
					logFileWriter.write("Now working with Faculty " + faculty[j]);
					logFileWriter.newLine();
					
					String exclusionOrInclusion 
						= HandSOnTestSuite.salarySettingMgrTabUI.getExceptionAttributeForNameAndDept(faculty[j], 
							dept, "ExcludeOrInclude", logFileWriter);
					boolean isChair = isChair(faculty[j], logFileWriter);
					if (exclusionOrInclusion.equalsIgnoreCase("Exclude")){
						logFileWriter.write(" * Examining the file for Exclusion *");
						logFileWriter.newLine();
						if (! containsExcludedFaculty(faculty[j], dept, clusters[i], logFileWriter)) {
							if (isChair){
								String message = "ERROR - The file doesn't contain the excluded faculty "+faculty[j] +", as expected";
								logFileWriter.write(message); logFileWriter.newLine();
								verificationErrors.append(message);
							}//end if
							else {
								logFileWriter.write("Faculty " + faculty[j] +" is not a Chair, so shouldn't be in this file");
								logFileWriter.newLine();
							}
						}//end if - the excluded faculty is not in the file
						else if (! isChair){
							logFileWriter.write("The file contains the excluded faculty " + faculty[j] +", as expected");
							logFileWriter.newLine();
						}
					}//end if - this is an Exclusion
					else {//this is an inclusion
						logFileWriter.write(" * Examining the file for Inclusion *");
						logFileWriter.newLine();
						if (! isChair) {
							logFileWriter.write("Faculty " +faculty[j] +" is not a Chair, so won't be in this worksheet");
							logFileWriter.newLine();
						}//end if - first of all, is this person even a Chair?
						else if (containsIncludedFaculty(faculty[j], dept, clusters[i], logFileWriter)){
							logFileWriter.write("Included Faculty member " + faculty[j] 
									+" is in the Chair SSW for "+dept +", as expected");
							logFileWriter.newLine();
						}//end else if - this person is included
						else {
							String message = "ERROR - Included Faculty member " + faculty[j] 
									+" is NOT in the Chair SSW for "+dept +", as expected";
							logFileWriter.write(message);
							logFileWriter.newLine();
							verificationErrors.append(message);
						}//end inner else - the Included Faculty Chair is not in the file, as expected
					}//end else
				}//end for loop

			}//end while

		}//end for loop
	}
	
	private boolean containsExcludedFaculty(String facultyName, String dept, String cluster, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method containsExceptionFaculty called with faculty name " + facultyName
				+", and Department "+dept + " ***");
		logFileWriter.newLine();
		
		String fileName = "Chair Salary Setting Worksheet "+cluster+".xlsx";
		SSExcelReader myReader = new SSExcelReader(TestFileController.directory +"/" +fileName, TestFileController.DepartmentSalarySettingWorksheetHeaderRow);
		int rowCount = myReader.getRowCount(TestFileController.ChairSalarySettingWorksheetWorksheetName); 
		logFileWriter.write("Row count for instantiated reader for " + fileName +" is " + rowCount);
		logFileWriter.newLine();

		int rowForColNames = myReader.getCellRowNum(TestFileController.ChairSalarySettingWorksheetWorksheetName, 0, SalarySettingManagementUIController.exclusionSectionHeading);
		logFileWriter.write("Estimated location of excluded faculty at row " + rowForColNames);
		logFileWriter.newLine();

		int actualRowForColNames = myReader.getCellRowNum(TestFileController.ChairSalarySettingWorksheetWorksheetName, 1, "Name", rowForColNames);
		logFileWriter.write("Actual header row for excluded faculty is " + actualRowForColNames);
		logFileWriter.newLine();
		
		boolean foundUnexpectedString = false;
		int rowNum = -1;
		if (actualRowForColNames != -1){
			myReader.setRowForColumnNames(rowForColNames);
			String[] colNames = {"Name", "Dept", "Reason", "Description"};
			for (int j=0; j<colNames.length; j++){
				if (j==0)
					rowNum = myReader.getCellRowNum(TestFileController.ChairSalarySettingWorksheetWorksheetName, colNames[j], facultyName);
				String value = myReader.getCellData(TestFileController.ChairSalarySettingWorksheetWorksheetName, colNames[j], rowNum);
				logFileWriter.write("Value for the "+ colNames[j] +" column is found in the SSW: "+ value);
				logFileWriter.newLine();
				String attributeName = colNames[j];
				String UIValue = new String();
				UIValue = HandSOnTestSuite.salarySettingMgrTabUI.getExceptionAttributeForNameAndDept(facultyName, dept, attributeName, logFileWriter);
				boolean stringsAreEqual = false;
				if (attributeName.equalsIgnoreCase("Description")){
					if (value.contains(UIValue) ||(value.equalsIgnoreCase(UIValue))) 
						stringsAreEqual = true;
					else stringsAreEqual = false;
				}//end if - it's the Description
				else if (UIValue.equalsIgnoreCase(value))//it's not the Description
					stringsAreEqual = true;
				if ((value == null) || (UIValue == null) || (value.isEmpty()) || (UIValue.isEmpty()) || (! stringsAreEqual)){
					logFileWriter.write("MISMATCH found --> SSW value: " + value +", UI Value: " + UIValue);
					logFileWriter.newLine();
					foundUnexpectedString = true;
				}//end if - there's a mismatch
			}//end inner for loop - iterating through the columns
		}//end if
		TestFileController.closeOneExcelReader(myReader);
		logFileWriter.newLine();
		logFileWriter.write("Values determined by this method: ");
		logFileWriter.newLine();
		logFileWriter.write("Row containing the Override Section Header: " + rowForColNames);
		logFileWriter.newLine();
		logFileWriter.write("Row containing the Column Headers: " + actualRowForColNames);
		logFileWriter.newLine();
		logFileWriter.write("Row containing the Faculty values: " + rowNum);
		logFileWriter.newLine();
		logFileWriter.write("Unexpected String found: " + foundUnexpectedString);
		logFileWriter.newLine();
		return ((rowForColNames != -1) && (actualRowForColNames != -1) && (rowNum != -1)
				&& (actualRowForColNames == rowForColNames + 1) && (! foundUnexpectedString));
	}//end containsExcludedFaculty method
	
	public boolean containsIncludedFaculty(String facultyName, String dept, String cluster, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method containsIncludedFaculty called with faculty name " + facultyName
				+", and Department "+dept + " ***");
		logFileWriter.newLine();

		String fileName = "Chair Salary Setting Worksheet "+cluster+".xlsx";
		SSExcelReader myReader = new SSExcelReader(TestFileController.directory +"/" +fileName, 0);
		int rowCount = myReader.getRowCount(TestFileController.ChairSalarySettingWorksheetWorksheetName); 
		logFileWriter.write("Row count for instantiated reader for " + fileName +" is " + rowCount);
		logFileWriter.newLine();

		int headerRow = 0;
		if (cluster.contains("Humanities and Arts"))
			headerRow = TestFileController.ChairSalarySettingWorksheetHumanitiesAndArtsHeaderRow;
		else if (cluster.contains("Natural Sciences"))
			headerRow = TestFileController.ChairSalarySettingWorksheetNaturalSciencesHeaderRow;
		else 
			headerRow = TestFileController.ChairSalarySettingWorksheetSocialSciencesHeaderRow;
		myReader.setRowForColumnNames(headerRow);
		int rowNum = myReader.getCellRowNum(TestFileController.ChairSalarySettingWorksheetWorksheetName, 1, facultyName);
		logFileWriter.write("Estimated location of included faculty at row " + rowNum);
		logFileWriter.newLine();

		boolean foundUnexpectedString = false;		
		if (rowNum != -1){
			String colName = "16-17 Description";
			String cellValue = myReader.getCellData(TestFileController.ChairSalarySettingWorksheetWorksheetName, colName, rowNum);
			String UIValue = new String();
			UIValue = HandSOnTestSuite.salarySettingMgrTabUI.getExceptionAttributeForNameAndDept(facultyName, dept, "Description", logFileWriter);
			if (cellValue.contains(UIValue)){
				logFileWriter.write("Included Faculty attribute with column " +colName +" includes expected text "+UIValue);
				logFileWriter.newLine();
			}//end if - Inclusion Description is in the file, as expected
			else{
				foundUnexpectedString = true;
				logFileWriter.write("ERROR - Included Faculty attribute with column " +colName +" DOES NOT INCLUDE expected text "+UIValue);
				logFileWriter.newLine();				
			}//end else - the Description is not there
		}//end if - they found the proper row
		else{
			logFileWriter.write("ERROR - Could not find Included Faculty member " +facultyName +" in the Chair SSW for Department "+dept);
			logFileWriter.newLine();
		}//end else
		
		TestFileController.closeOneExcelReader(myReader);
		
		return ((rowNum != -1) && (! foundUnexpectedString));
	}//end containsIncludedFaculty method
	
	/*
	 * This method returns true if and only if the the faculty member is a Chair.
	 */
	private boolean isChair(String name, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method isChair called for name " + name +" ****");
		logFileWriter.newLine();
		
		SSExcelReader reader = TestFileController.SchoolSalarySettingReport;
		String worksheet = TestFileController.SchoolSalarySettingReportWorksheetName;
		
		int rowNum = reader.getCellRowNum(worksheet, "Name", name);
		logFileWriter.write("Found name "+name+" at row " +rowNum+", of the School Salary Setting Report");
		logFileWriter.newLine();
		
		if (rowNum == -1) 
			return false;
		else
			return reader.getCellData(worksheet, "Description", rowNum).contains("Chair - ");
	}//end isChair method

	@After
	public void tearDown() throws Exception {
		TestFileController.writeTestResults(testEnv, testCaseNumber, testName, testDescription, verificationErrors);
		logFileWriter.newLine();
		logFileWriter.close();
	}//end tearDown method


}
