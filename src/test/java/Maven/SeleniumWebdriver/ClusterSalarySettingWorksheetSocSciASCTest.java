package Maven.SeleniumWebdriver;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ClusterSalarySettingWorksheetSocSciASCTest extends
		ClusterSalarySettingWorksheetASCTest {

	@Before
	public void setUp() throws Exception {
		clusterName = "Social Sciences Cluster";
		configureOutput();
		super.setUp();
	}
	
	private void configureOutput() throws Exception{
		myExcelReader = TestFileController.ClusterSalarySettingWorksheetSocialSciences;
		mySheetName = TestFileController.ClusterSalarySettingWorksheetWorksheetName;
		rowForColumnNames = TestFileController.ClusterSalarySettingWorksheetHeaderRow;
		verificationErrors = new StringBuffer();
		testName = "ClusterSalarySettingWorksheetSocSciASCTest";
		testEnv = "Test";
		testCaseNumber = "040";
		testDescription = "ASC Test of Cluster Salary Setting Worksheet for Social Sciences";
		logFileName = "Cluster Salary Setting SocSci ASC Test.txt";
		logFileWriter = TestFileController.getLogFileWriter(logDirectoryName  +"/" + logFileName);
	}//end configureOutput method



	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void test() throws Exception{
		super.test();
	}

}
