package Maven.SeleniumWebdriver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ClusterSalarySettingWorksheetHumArtsASCTest extends
		ClusterSalarySettingWorksheetASCTest {

	@Before
	public void setUp() throws Exception {
		clusterName = "Humanities and Arts Cluster";
		configureOutput();
		super.setUp();
	}
	
	private void configureOutput() throws Exception{
		myExcelReader = TestFileController.ClusterSalarySettingWorksheetHumanitiesAndArts;
		mySheetName = TestFileController.ClusterSalarySettingWorksheetWorksheetName;
		rowForColumnNames = TestFileController.ClusterSalarySettingWorksheetHeaderRow;
		verificationErrors = new StringBuffer();
		testName = "ClusterSalarySettingWorksheetHumArtsASCTest";
		testEnv = "Test";
		testCaseNumber = "038";
		testDescription = "ASC Test of Cluster Salary Setting Worksheet for Humanities and Arts";
		logFileName = "Cluster Salary Setting HumArts ASC Test.txt";
		logFileWriter = TestFileController.getLogFileWriter(logDirectoryName  +"/" + logFileName);
	}//end configureOutput method



	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void test() throws Exception{
		super.test();
	}

}
