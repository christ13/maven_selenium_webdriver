package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeSet;

/*
 * This Class serves as the container for the ASCFacultyValues Objects.
 * It actually instantiates the ASCFacultyValues Objects and stores them in an ArrayList.
 * 
 * Although the list of ASCFacultyValues Objects is initially created from a list
 * of Faculty names and departments only, most of the data stored in the 
 * ASCFacultyValues Objects comes from the Control Sheet Detail Report.
 * In order to extract the information, we need to conduct the following queries
 * on the Control Sheet Detail Report
 * 
 * (1) Get any row with the Name and Department to get the Rank and Cluster,
 * get the values in the "Rank" and "Cluster" columns of the report, 
 * set the values in the ASCFacultyValues Object using the setStringAttribute method,
 * 
 * (2) Do a search for any row with the Name and Department, look up the Description
 * column, determine if the String ("Chair - " + <Department>) or "Program Director -" 
 * is a substring of the Description value for that row, 
 * if yes, then set isDeptChair = true;
 * Similarly, do a search for any row with the Name and Department, look up the 
 * Description column, determine if the String ("Dean, H&S Dean's Office") is a substring
 * of the Description value for that row.  If yes, then set isDean = true;
 * Please note that the two conditions are exclusive.  
 * If one is a Dean, then one cannot be a Department Chair, and vice versa.
 * 
 * (3) doShares and deptShares are both two-dimensional arrays.  The first dimension
 * corresponds to the Stage of the amount and the second dimension corresponds
 * to the Pool which holds the amount.  Also, we only include lines which have an
 * Effective Date that is blank or an effective Date that is on or after 9/1 
 * of the current FY.  
 * 
 */
public class ASCFacultyValuesSet {
	
	private SSExcelReader myExcelReader;
	private static final String mySheetName = TestFileController.ControlSheetDetailReportWorksheetName;
	private static final int myColumnHeaderRow = TestFileController.ControlSheetDetailReportColumnHeaderRow;
	private ArrayList<ASCFacultyValues> myList;
	public static final String[] validCategories = {"Salary Promises", "Previous Year Commitments", 
		"Faculty Appointment Related", "Governance Related"};
	public static final int SalaryPromiseIndex = 0;
	public static final int PreviousYearCommitmentsIndex = 1;
	public static final int FacultyAppointmentRelatedIndex = 2;
	public static final int GovernanceRelatedIndex = 3;

	public ASCFacultyValuesSet() {
		myExcelReader = TestFileController.ControlSheetReportSuite;
		myList = new ArrayList<ASCFacultyValues>();
	}
	
	public ASCFacultyValues getASCFacultyValueForNameAndDept(String name, String dept){
		for(int i=0; i<myList.size(); i++){
			if(
				(myList.get(i).getStringAttribute(ASCFacultyValues.FacultyIndex).equalsIgnoreCase(name))
			&&	(myList.get(i).getStringAttribute(ASCFacultyValues.DepartmentIndex).equalsIgnoreCase(dept))
					){
				return myList.get(i);
			}
		}//end for loop
		return null;
	}//end getASCFacultyValueForNameAndDept method
	
	public boolean addElement(ASCFacultyValues ascFacultyValues){
		for(int i=0; i<myList.size(); i++){
			if(
					(myList.get(i).getStringAttribute(ASCFacultyValues.FacultyIndex).equalsIgnoreCase(ascFacultyValues.getStringAttribute(ASCFacultyValues.FacultyIndex)))
				&&	(myList.get(i).getStringAttribute(ASCFacultyValues.DepartmentIndex).equalsIgnoreCase(ascFacultyValues.getStringAttribute(ASCFacultyValues.DepartmentIndex)))
						){
					return false;//it's a duplicate - don't add it
			}//end if
		}//end for
		//if we got to this point, it's not in the list, so add it
		myList.add(ascFacultyValues);
		return true;
	}//end addElement method
	
	public void addAllElements(TreeSet<String> uniqueNamesAndDepts, BufferedWriter logFileWriter) throws Exception{
		outputMessage("\n*** method addAllElements is being called ***\n", logFileWriter);
		myExcelReader = TestFileController.ControlSheetReportSuite;
		myExcelReader.setRowForColumnNames(myColumnHeaderRow);
		Iterator<String> iterator = uniqueNamesAndDepts.iterator();
		while (iterator.hasNext()){
			String[] nameAndDept = iterator.next().split("-");
			if (nameAndDept.length == 0){
				nameAndDept = new String[2];
				nameAndDept[0] = nameAndDept[1] = new String();
			}
			this.addElement(new ASCFacultyValues(nameAndDept[0], nameAndDept[1]));
		}//end while loop - adding a whole new set of ASCFacultyValues
		populateElements(logFileWriter);
		for (int i=0; i<myList.size(); i++){
			String message = "ASCFacultyValues at index "+ i + " has the following data:\n"+ myList.get(i).toString();
			TestFileController.writeToLogFile(message, logFileWriter);
		}//iterating through the whole List - printing out the ASCFacultyValues in it
	}//end addAllElements method
	
	public void populateElements(BufferedWriter logFileWriter) throws Exception{
		outputMessage("\n*** method populateElements is being called ***\n", logFileWriter);
		for (int i=0; i<myList.size(); i++){
			String name = myList.get(i).getStringAttribute(ASCFacultyValues.FacultyIndex);
			//System.out.println("Name being submitted is "+ name);
			String dept = myList.get(i).getStringAttribute(ASCFacultyValues.DepartmentIndex);
			//System.out.println("Department being submitted is " + dept);
			String[] names = {ASCFacultyValues.StringAttributeKeys[ASCFacultyValues.ClusterIndex],
					ASCFacultyValues.StringAttributeKeys[ASCFacultyValues.RankIndex], "Description"};
			//System.out.print("column names submitted are as follows: ");
			for (int j=0;j<names.length;j++) System.out.print(names[j]+", ");
			//System.out.println();
			String[] values = getNonNumericData(name, dept, names, logFileWriter);
			myList.get(i).setStringAttribute(ASCFacultyValues.ClusterIndex, values[0]);
			myList.get(i).setStringAttribute(ASCFacultyValues.RankIndex, values[1]);
			if (! values[2].isEmpty()){
				myList.get(i).setIsDean(values[2].contains("Dean, H&S Dean's Office"));
				myList.get(i).setIsDeptChair(values[2].contains("Chair - " + dept));
			}//end if - the last item is not empty
			else{
				myList.get(i).setIsDean(false);
				myList.get(i).setIsDeptChair(false);
			}//end else - the last item is empty
			for(int j=0; j<ASCTest.stages.length; j++){
				for(int k=0; k<ASCTest.sourcePools.length; k++){
					int[] shares = getNumericData(name, dept, 
							ASCTest.stages[j], ASCTest.sourcePools[k],
							logFileWriter);
					myList.get(i).setDOShare(j, k, shares[0]);
					myList.get(i).setDeptShare(j, k, shares[1]);
				}//end inner for - doShares and deptShares for a given Pool (source pools)
			}//end outer for - doShares and deptShares for a given Stage (stages)
			if (HandSOnTestSuite.expectedASCDataSet.getMultipleASCData(false, logFileWriter).containsKey(name)){
				outputMessage("Faculty " + name + " has multiple ASC's", logFileWriter);
				myList.get(i).setHasMultipleASCs(true);
			}//end if
		}//end for loop
	}//end populateElements method
	
	/*
	 * Sometimes, we need to find out how many ASC's have a certain value for a certain
	 * attribute.  That is what this method is for.
	 */
	public int getNumberOfASCsWithStringValue(String attributeName, String attributeValue){
		int count = 0;
		for (int i=0; i<myList.size(); i++){
			ASCFacultyValues asc = myList.get(i);
			if (asc.getStringAttribute(attributeName).equalsIgnoreCase(attributeValue)) count++;
		}//end for loop
		return count;
	}
	
	public ArrayList<ASCFacultyValues> getASCFacultyValuesWithAttribute(String attributeName, String attributeValue){
		ArrayList<ASCFacultyValues> ascs = new ArrayList<ASCFacultyValues>();
		for (int i=0; i<myList.size(); i++){
			if (myList.get(i).getStringAttribute(attributeName).equalsIgnoreCase(attributeValue))
				ascs.add(myList.get(i));
		}		
		return ascs;
	}
	
	public String[] getNonNumericData(String name, String dept, String[] names, BufferedWriter logFileWriter) throws Exception{
		System.out.println("*** method getNonNumericData is running ***");
		HashMap<String, String> keysAndValues = new HashMap<String, String>();
		keysAndValues.put(ASCFacultyValues.StringAttributeKeys[ASCFacultyValues.FacultyIndex], name);
		keysAndValues.put(ASCFacultyValues.StringAttributeKeys[ASCFacultyValues.DepartmentIndex], dept);
		String[][] values = myExcelReader.getMultipleFacultyMultipleValues(mySheetName, keysAndValues, names, logFileWriter);
		//we assume that these values are the same for all rows, so we only return the first row
		System.out.println("The length of array returned is " + values.length);
		if (values == null || values.length == 0){//if it's not found, return empty strings
			values = new String[1][names.length];
			for(int i=0;i<names.length;i++) values[0][i] = new String();
		}
		return values[0];
	}//end populateElementNameAndRank method
	
	/*
	 * The objective of this method is to extract the summaries of all values for 
	 * the "FTE Adjusted DO Share Amount", and "FTE Adjusted Department Share Amount" columns 
	 * in the Control Sheet Detail Report, given a faculty name, department, Stage of the 
	 * Commitment, and Source Pool that the Commitment comes from.  
	 * In order to do this, we will use the SSExcelReader "getColumnSummaryValues" method.
	 * Here's the summary of this method:
	 * 	public BigDecimal[] getColumnSummaryValues(String sheetName, 
	 * HashMap<String, String> criteria, 
	 * boolean emptyDatesExcluded, boolean outsideDatesIncrementFacultyCount, 
	 * boolean outsideDatesIncrementSalarySum, 
	 * String[] columnNames, 
	 * Date minDate, Date maxDate, 
	 * BufferedWriter logFileWriter)
	 */
	public int[] getNumericData(String name, String dept, String stage, String pool, 
			BufferedWriter logFileWriter) throws Exception{
		String message = "*** method getNumericData is being called with faculty name " + name
				+" and faculty department " + dept +" and Stage " + stage 
				+ " and Source Pool " + pool + " ***";
		//System.out.println(message);
		TestFileController.writeToLogFile(message, logFileWriter);
		int[] sumOfValues = new int[2];//this will be returned by this method
		
		for(int i=0; i<validCategories.length; i++){
			int[] values = new int[2];//this is incremental - will be instantiated and added to the totals
			
			//now, specify the input criteria and call the SSExcelReader method getColumnSummaryValues
			HashMap<String, String> criteria = new HashMap<String, String>();
			criteria.put(ASCFacultyValues.StringAttributeKeys[ASCFacultyValues.FacultyIndex], name);
			criteria.put(ASCFacultyValues.StringAttributeKeys[ASCFacultyValues.DepartmentIndex], dept);
			criteria.put(ExpectedASCData.StringAttributeKeys[ExpectedASCData.StageIndex], stage);
			criteria.put(ExpectedASCData.StringAttributeKeys[ExpectedASCData.PoolIndex], pool);
			criteria.put("Category", validCategories[i]);
			boolean emptyDatesExcluded = false;
			boolean outsideDatesIncrementFacultyCount = false;
			boolean outsideDatesIncrementSalarySum = false;
			String[] columnNames = {"FTE Adjusted DO Share Amount", "FTE Adjusted Department Share Amount"};
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			Date minDate = sdf.parse("09/01/2016");
			Date maxDate = sdf.parse("09/01/2017");
			BigDecimal[] rawValues = myExcelReader.getColumnSummaryValues(mySheetName, criteria, emptyDatesExcluded, 
					outsideDatesIncrementFacultyCount, outsideDatesIncrementSalarySum, 
					columnNames, minDate, maxDate, logFileWriter);
			
			if (rawValues[0].intValue() == 0){
				message = "No matching rows found";
				//System.out.println(message);
				//TestFileController.writeToLogFile(message, logFileWriter);
				values[0] = 0; values[1] = 0;
			}
			if (rawValues[0].intValue() > 1) {
				message = "*** WARNING - search criteria matches more than one faculty member "
						+"and the total count is " + rawValues[0].intValue() + ", returning zeroes ***";
				//System.out.println(message);
				TestFileController.writeToLogFile(message, logFileWriter);
				values[0] = 0; values[1] = 0;
			}//end if
	
			values[0] = rawValues[1].intValue();
			
			values[1] = rawValues[2].intValue();
			sumOfValues[0] += values[0];
			sumOfValues[1] += values[1];
		}//end for loop

		message = "DO Share sum derived is " + sumOfValues[0];
		//System.out.println(message);
		//TestFileController.writeToLogFile(message, logFileWriter);

		message = "Dept Share sum derived is " + sumOfValues[1];
		//System.out.println(message);
		//TestFileController.writeToLogFile(message, logFileWriter);

		return sumOfValues;
	}//end getNumericData method
	
	public String[] getAllDiscreteStringAttributeValues(String attributeName, String requiredSubstring, BufferedWriter logFileWriter) throws Exception{
		outputMessage("\n method getAllDiscreteStringAttributeValues called\n", logFileWriter);
		TreeSet<String> rawAttributeValues = new TreeSet<String>();
		int sizeOfList = myList.size();
		outputMessage("size of list to search through is "+sizeOfList, logFileWriter);
		for(int i=0; i<sizeOfList; i++){
			String attribute = myList.get(i).getStringAttribute(attributeName);
			if (attribute.contains(requiredSubstring))
				rawAttributeValues.add(attribute);
			outputMessage("adding the following attribute: " + attribute, logFileWriter);
		}//end for
		String[] attribValues = new String[rawAttributeValues.size()];
		Iterator<String> iterator = rawAttributeValues.iterator();
		int i=0;
		outputMessage("method getAllDiscreteStringAttributeValues is returning the following values: ", logFileWriter);
		while(iterator.hasNext()){
			attribValues[i] = (String)iterator.next();
			outputMessage(attribValues[i++] + "; ", logFileWriter);
		}
		outputMessage("\n", logFileWriter);
		return attribValues;
	}//end getAllDiscreteStringAttributeValues method
	
	public int size(){
		return myList.size();
	}//end method size

	
	public void outputMessage(String message, BufferedWriter logFileWriter) throws Exception{
		TestFileController.writeToLogFile(message, logFileWriter);
		System.out.println(message);
	}//end method outputMessage


}//end Class
