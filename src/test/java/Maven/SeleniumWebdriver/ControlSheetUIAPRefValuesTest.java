package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import java.io.BufferedWriter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.NoSuchWindowException;

public class ControlSheetUIAPRefValuesTest {
	protected static String testName;
	protected static String testCaseNumber;
	protected static String testDescription;
	protected static String testEnv;
	protected static StringBuffer verificationErrors;

	@Before
	public void setUp() throws Exception {
		testEnv = "Test";
		testName = "Verify AP Values in Control Sheet UI";
		testDescription = "Verify AP Values in Control Sheet UI";
		testCaseNumber = "022";
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws Exception{
		verificationErrors = new StringBuffer();
		System.out.println("Now executing test: " + testName);
		String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		BufferedWriter logFileWriter = TestFileController.getLogFileWriter(logFileName);
		
		getControlSheetUI(logFileWriter);
		reportExpectedValues(logFileWriter);
		verifyAPTypes(logFileWriter);
		TestFileController.writeTestResults(testEnv, testCaseNumber, testName, testDescription, verificationErrors);
		logFileWriter.close();
	}
	
	private void getControlSheetUI(BufferedWriter logFileWriter) throws Exception{
		TestFileController.writeToLogFile("Test Class UIComparisonMaker is running - the browser version is "+ HandSOnTestSuite.browserName, logFileWriter);
		TestFileController.writeToLogFile("...and file controller's directory is " + TestFileController.getLogDirectoryName(), logFileWriter);
		String currentURL = new String();
		while (currentURL.isEmpty()){
			try{
				currentURL = SalarySettingManager.driver.getCurrentUrl();
			}//end try
			catch(NoSuchWindowException e){
				currentURL = "Could not acquire the URL - window is already closed";
				TestFileController.writeToLogFile(currentURL, logFileWriter);
			}//end catch
		}//end while
		TestFileController.writeToLogFile("... and the browser is currently at the following URL: " + currentURL, logFileWriter);
		if (SalarySettingManager.openSalarySetting(logFileWriter)){
			TestFileController.writeToLogFile("... and we've successfully opened Salary Setting again", logFileWriter);
		}//end if - Salary Setting is now opened
		else TestFileController.writeToLogFile("***NOTE*** we've NOT successfully opened Salary Setting again", logFileWriter);
		ControlSheetUI.gotoControlSheetUITab();
	}//end method
	
	private void reportExpectedValues(BufferedWriter logFileWriter) throws Exception{
		StringBuffer message = new StringBuffer();
		message.append("Expected values for the Control Sheet tab are as follows:\n");
		System.out.println(message.toString());
		TestFileController.writeToLogFile(message.toString(), logFileWriter);
		message = new StringBuffer();
		String[] labels = HandSOnTestSuite.referenceValues.getLabels();
		for (int i=0; i<labels.length; i++){
			message.append("AP Label: " + labels[i]);
			message.append("; number of discrete faculty: " + ControlSheetUI.getNumberOfFacultyForAPType(labels[i]));
			message.append("; total dollar amount: " + ControlSheetUI.getDollarAmountForAPType(labels[i]).toString() + "\n");
			System.out.println(message.toString());
			TestFileController.writeToLogFile(message.toString(), logFileWriter);
			message = new StringBuffer();
		}//end for loop
	}//end method
	
	/*
	 * In order to compare the expected numbers and dollar amounts with the actual numbers and
	 * dollar amounts, we'll have to pull the numbers from the GUI using locators and then
	 * compare the numbers and dollar amounts with the numbers and dollar amounts in the 
	 * ControlSheetUI Object.
	 * The ControlSheetUI Object is the best candidate to look up the values in itself and 
	 * report these values.  The job here is to tell the ControlSheetUI which values we want
	 */
	
	private void verifyAPTypes(BufferedWriter logFileWriter) throws Exception{
		StringBuffer message = new StringBuffer();
		message.append("Actual checks for the Control Sheet tab are as follows:\n");
		System.out.println(message.toString());
		TestFileController.writeToLogFile(message.toString(), logFileWriter);
		message = new StringBuffer();
		String[] labels = HandSOnTestSuite.referenceValues.getLabels();
		for(int i=0; i<labels.length; i++){
			message = new StringBuffer();
			message.append("check of AP Type " + labels[i] +": ");
			boolean match = 
					((ControlSheetUI.checkExpectedVersusActualDollarAmounts(labels[i], logFileWriter)) 
						&& 
					(ControlSheetUI.checkExpectedVersusActualDiscreteFacultyNumbers(labels[i], logFileWriter)));
			message.append(match + "\n");
			System.out.println(message.toString());
			TestFileController.writeToLogFile(message.toString(), logFileWriter);
			if (! match) verificationErrors.append(message.toString());
		}//end for loop
	}//end verifyAPTypes method

}
