package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

public class ReportAmountsVerificationFromUI {
	protected String locatorSheetName;
	protected String testName;
	protected StringBuffer verificationErrors;
	protected BufferedWriter logFileWriter;
	protected String logFileName;
	protected String testEnv;
	protected String testCaseNumber;
	protected String testDescription;
	protected String logDirectoryName = ReportRunner.logDirectoryName;
	
	private String sheetName;

	private HSSFExcelReader locators;
	protected int rowForColumnNames;
	
	private ArrayList<SSExcelReader> deptWorksheets;
	private static final int BiologyIndex = 0;
	private static final int EconomicsIndex = 1;
	private static final int HistoryIndex = 2;
	private static final int MathematicsIndex = 3;
	private static final int PhysicsIndex = 4;
	private static final int StatisticsIndex = 5; 
	private static final int TheatreAndPerformanceStudiesIndex = 6;
	
	private ArrayList<SSExcelReader> clusterWorksheets;
	private static final int HumArtsIndex = 0;
	private static final int NatSciIndex = 1;
	private static final int SocSciIndex = 2;
	
	private static final String[] funds = {"Regular Merit Pool Natural Sciences Dean Reserve - Test - 2017", 
			"Test Pool Test Funding Reserve Name 2017 - TEST", 
			"Albert Eustace Peasmarch Fund Albert Eustace Peasmarch Fund Reserve 2017"};

	
	private boolean locatorsToOutputFile = true;
	private boolean ExcelReaderToOutputFile = true;

	@Before
	public void setUp() throws Exception {
		testName = "ReportAmountsVerificationFromUI";
		testEnv = "Test";
		testCaseNumber = "047";
		testDescription = "Report Amounts Verification From UI";
		logFileName = testDescription + ".txt";
		logFileName = ReportRunner.logDirectoryName +"/" + logFileName;
		logFileWriter = TestFileController.getLogFileWriter(logFileName);

		locators = TestFileController.locators;
		locators.setRowForColumnNames(0);
		
		verificationErrors = new StringBuffer();
		locatorSheetName = "Control Sheet";
		
		deptWorksheets = new ArrayList<SSExcelReader>();
		deptWorksheets.add(TestFileController.DepartmentSalarySettingBiologyReport);
		deptWorksheets.add(TestFileController.DepartmentSalarySettingEconomicsReport);
		deptWorksheets.add(TestFileController.DepartmentSalarySettingHistoryReport);
		deptWorksheets.add(TestFileController.DepartmentSalarySettingMathematicsReport);
		deptWorksheets.add(TestFileController.DepartmentSalarySettingPhysicsReport);
		deptWorksheets.add(TestFileController.DepartmentSalarySettingStatisticsReport);
		deptWorksheets.add(TestFileController.DepartmentSalarySettingTheaterAndPerformanceStudiesReport);
		
		clusterWorksheets = new ArrayList<SSExcelReader>();
		clusterWorksheets.add(TestFileController.ClusterSalarySettingWorksheetHumanitiesAndArts);
		clusterWorksheets.add(TestFileController.ClusterSalarySettingWorksheetNaturalSciences);
		clusterWorksheets.add(TestFileController.ClusterSalarySettingWorksheetSocialSciences);
	}

	@After
	public void tearDown() throws Exception {
		TestFileController.writeTestResults(testEnv, testCaseNumber, testName, testDescription, verificationErrors);
		logFileWriter.close();
	}

	@Test
	public void test() throws Exception{
		String message = "Now executing the '" + testDescription +"' test";
		writeToLogFile(message, logFileWriter);
		System.out.println(message);
		if (! ControlSheetUI.openSalarySetting(logFileWriter)){
			fail("Could not open Salary Setting");
		}//end if Salary Setting is not open
		ControlSheetUI.switchToControlSheetTab();
		//make sure that the Control Sheet on the UI is loaded fully
		String locator = locators.getCellData(locatorSheetName, "Locator", 3);
		UIController.waitForElementPresent(By.xpath(locator));

		
		verifyControlSheet(logFileWriter);
		verifyPoolAllocationReport(logFileWriter);
		verifyDeptSalarySettingWorksheets(logFileWriter);
		verifyClusterSalarySettingWorksheets(logFileWriter);
		verifySchoolSalarySettingWorksheets(logFileWriter);
	}//end test method
	/*
	 * This will verify the aggregate figures in the "Pool Calculations" section.
	 */
	private void verifyControlSheet(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		writeToLogFile("**** method verifyControlSheet has been called ****", logFileWriter);
		sheetName = TestFileController.ControlSheetReportWorksheetName;
		TestFileController.ControlSheetReportSuite.setRowForColumnNames(TestFileController.ControlSheetReportColumnHeaderRow);
				
		String poolName = "Regular Merit Pool";
		String sheetRowLabel = "Merit Pool Salary Base";
		String locatorName = sheetRowLabel.replaceAll("\\s+","");
		compareSheetAmtWithUIAmt(poolName, sheetRowLabel, locatorName, logFileWriter);
		
		sheetRowLabel = "Merit Pool Salary Raise %";
		locatorName = "KnownRegularMeritPoolSalaryRaisePercent";
		compareSheetAmtWithUIAmt(poolName, sheetRowLabel, locatorName, logFileWriter);
		
		verifyPoolSummaryFigures(logFileWriter);
	}//end verifyControlSheet method
	
	private void verifyPoolSummaryFigures(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		writeToLogFile("**** method verifyPoolSummaryFigures has been called ****", logFileWriter);
		String[] pools = ASCTest.sourcePools;
		for (String pool : pools){
			String[] sheetRowLabels = {"Pool Amount", "Adjustments", "Total Raise Pool Amount"};
			for (String sheetRowLabel: sheetRowLabels){
				String locatorName = pool.replaceAll("\\s", "") + "Summary" + sheetRowLabel.replaceAll("\\s", "");
				if (locatorName.contains("Regular"))
					locatorName = locatorName.replaceAll("Regular", "");
				compareSheetAmtWithUIAmt (pool, sheetRowLabel, locatorName, logFileWriter);
			}//end inner for loop
		}//end outer for loop
	}//end verifyPoolSummaryFigures method
	
	
	public void compareSheetAmtWithUIAmt (String poolName, String sheetRowLabel, String locatorName, BufferedWriter logFileWriter) throws Exception{
		writeToLogFile("*** method compareSheetAmtWithUIAmt called ***", logFileWriter);
		int row = TestFileController.ControlSheetReportSuite.getCellRowNum(sheetName, 0, sheetRowLabel);
		if (ExcelReaderToOutputFile)
			writeToLogFile(sheetRowLabel +" information in the Control Sheet will be found at row "+row, logFileWriter);
		String sheetAmtString = TestFileController.ControlSheetReportSuite.getCellData(sheetName, poolName, row);
		double sheetValue = ControlSheetUI.getDoubleFromString(sheetAmtString);
		if (ExcelReaderToOutputFile)
			writeToLogFile(sheetRowLabel +" found in the Control Sheet is " + sheetValue, logFileWriter);
		double UIValue = getDoubleValueFromUI(locatorName, logFileWriter);
		verificationErrors = ControlSheetUI.compareValues(UIValue, sheetValue, verificationErrors, logFileWriter);
	}//end method compareSheetAmtWithUIAmt
	
	
	
	private void verifyPoolAllocationReport(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		writeToLogFile("**** method verifyPoolAllocationReport has been called ****", logFileWriter);
		sheetName = TestFileController.PoolAllocationWorksheetName;
		
		String[] pools = ASCTest.sourcePools;
		for (String pool : pools)
			verifyRowLabels(TestFileController.ControlSheetReportSuite, sheetName, pool, 4, 5, logFileWriter);

		for (String fund: funds)
			verifyRowLabels(TestFileController.ControlSheetReportSuite, sheetName, fund, 4, 5, logFileWriter);
	}//end verifyPoolAllocationReport method
	
	private void verifyRowLabels(SSExcelReader reader, String sheetName, String pool, int rowForColumnNames, 
			int colForSheetRowLabels, BufferedWriter logFileWriter) throws Exception{
		String[] sheetRowLabels = {"Total Amount", "Allocated Amount", "Remaining Amount"};
		writeToLogFile("*** method verifyRowLabels called with sheet name " +sheetName 
				+", and pool " +pool +", and row for column names " + rowForColumnNames 
				+", and column for Sheet Row Labels "+ colForSheetRowLabels, logFileWriter);
		for (String sheetRowLabel: sheetRowLabels){
			String sheetValueString = getSheetDataForPool(reader, sheetName, pool, rowForColumnNames, colForSheetRowLabels, sheetRowLabel, logFileWriter);
			/*
			int originalRowForColumnNames = reader.getRowForColumnNames();
			reader.setRowForColumnNames(rowForColumnNames);
			int row = reader.getCellRowNum(sheetName, colForSheetRowLabels, sheetRowLabel);
			if (ExcelReaderToOutputFile)
				writeToLogFile("Row number for " + sheetRowLabel + " is " + row, logFileWriter);
			reader.setRowForColumnNames(originalRowForColumnNames);
			String sheetValueString = reader.getCellData(sheetName, pool, row);
			if (ExcelReaderToOutputFile)
				writeToLogFile("Value for " + sheetRowLabel +" for Pool " + pool + " is "+sheetValueString, logFileWriter);
			*/
			double sheetValue = 0;
			try{
				sheetValue = ((Number)NumberFormat.getNumberInstance(java.util.Locale.US).parse(sheetValueString)).doubleValue();
			}
			catch(ParseException e){
				writeToLogFile("Cannot parse " + sheetValueString +" to a number", logFileWriter);
			}
			double UIValue = getUIValue(pool, sheetRowLabel, logFileWriter);
			if (locatorsToOutputFile)
				writeToLogFile("Value found in UI is "+UIValue, logFileWriter);
			verificationErrors = ControlSheetUI.compareValues(UIValue, sheetValue, verificationErrors, logFileWriter);
		}//end inner for loop - iterating through the rows in the Report
	}//end method verifyRowLabels
	
	private String getSheetDataForPool(SSExcelReader reader, String sheetName, String pool, int rowForColumnNames, 
			int colForSheetRowLabels, String sheetRowLabel, BufferedWriter logFileWriter) throws Exception{
		writeToLogFile("*** method getSheetDataForPool called with sheet "+sheetName
				+", and Pool "+pool +", and row for column names " + rowForColumnNames
				+", and column for sheet row labels " + colForSheetRowLabels
				+", and sheet row label "+sheetRowLabel, logFileWriter);

		int row = reader.getCellRowNum(sheetName, colForSheetRowLabels, sheetRowLabel, 1);

		if (ExcelReaderToOutputFile)
			writeToLogFile("Row number for " + sheetRowLabel + " is " + row +"; ", logFileWriter);
		int col = reader.getCellColumnNumber(sheetName, pool, colForSheetRowLabels, rowForColumnNames, logFileWriter, ExcelReaderToOutputFile);
		String sheetValueString = new String();
		if (col == -1){
			if (ExcelReaderToOutputFile)
				writeToLogFile("Pool " + pool + " is not included in this file; ", logFileWriter);
			return sheetValueString;
		}
		else
			sheetValueString = reader.getCellData(sheetName, col, row);
		if (ExcelReaderToOutputFile)
			writeToLogFile("Value for " + sheetRowLabel +" for " + pool + " is "+sheetValueString +"; ", logFileWriter);

		return sheetValueString;
	}//end getSheetDataForPool method

	/*
	private void compareValues(double UIValue, double sheetValue, BufferedWriter logFileWriter) throws Exception{
		writeToLogFile("*** method compareValues called with UI value "+UIValue
				+" and Excel sheet value "+sheetValue, logFileWriter);
		boolean testFails = (! ControlSheetUI.isApproximatelyEqual(UIValue, sheetValue));
				//((Number)NumberFormat.getNumberInstance(java.util.Locale.US).parse(UIValue)).doubleValue()
		if (testFails){
			String message = "MISMATCH! UI Value found is "+UIValue
					+", but value in the Excel sheet is "+sheetValue;
			writeToLogFile(message, logFileWriter);
			verificationErrors.append(message);
		}//end if - test has failed
		else writeToLogFile("UI Value and value in Excel sheet are both "+sheetValue+", as expected", logFileWriter);
	}//end compareValues method
	*/
	private double getUIValue(String pool, String rowLabel, BufferedWriter logFileWriter) throws Exception{
		double UIValue = 0;
		String firstValue = new String();
		String secondValue = new String();
		if (rowLabel.equalsIgnoreCase("Total Amount")){
			writeToLogFile(rowLabel +" will be calculated - adding Amount $ and Adjustments", logFileWriter);
			firstValue = getStringUIValueForPoolAndLabel(pool, "PoolAmount", logFileWriter);
			secondValue = getStringUIValueForPoolAndLabel(pool, "Adjustments", logFileWriter);
		}//end if - Total Amount
		else if (rowLabel.equalsIgnoreCase("Allocated Amount")){
			writeToLogFile(rowLabel +" will be calculated - adding Total Deductions and Department Allocation", logFileWriter);
			firstValue = getStringUIValueForPoolAndLabel(pool, "TotalDeductions", logFileWriter);
			secondValue = getStringUIValueForPoolAndLabel(pool, "DepartmentAllocation", logFileWriter);
		}//end else if - Allocated Amount
		else if (rowLabel.equalsIgnoreCase("Remaining Amount")){
			writeToLogFile(rowLabel +" will not be calculated - it's simply equal to the Variance", logFileWriter);
			firstValue = getStringUIValueForPoolAndLabel(pool, "Variance", logFileWriter);		
		}//end else if - Remaining Amount
		if (firstValue.startsWith("$")) firstValue = firstValue.substring(1);
		UIValue = ((Number)NumberFormat.getNumberInstance(java.util.Locale.US).parse(firstValue)).doubleValue();
		if (! secondValue.isEmpty()){
			if (secondValue.startsWith("$")) secondValue = secondValue.substring(1);
			UIValue += ((Number)NumberFormat.getNumberInstance(java.util.Locale.US).parse(secondValue)).doubleValue();
		}//end if - adding the second value, if applicable
		return UIValue;
	}//end getUIValue method
	
	private String getStringUIValueForPoolAndLabel(String pool, String label, BufferedWriter logFileWriter) throws Exception{
		writeToLogFile("Attempting to locate "+label+" for "+pool+" on the UI", logFileWriter);
		String UIValue = new String();
		pool = pool.replaceAll("\\s", "");
		pool = pool.replaceAll("Regular", "");
		String locatorName = pool + "Summary"+ label;
		writeToLogFile("Locator name to be used is " + locatorName, logFileWriter);
		int rowNum = locators.getCellRowNum(locatorSheetName, "Name", locatorName);
		if (rowNum != -1){
			String locator = locators.getCellData(locatorSheetName, "Locator", rowNum);
			if (locatorsToOutputFile)
				writeToLogFile("Locator to be used is " + locator, logFileWriter);
			UIValue = ControlSheetUI.getStringValueForLocator(locator, logFileWriter, locatorsToOutputFile);
		}
		else UIValue = "0";
		if (locatorsToOutputFile)
			writeToLogFile("Value derived from UI is " + UIValue, logFileWriter);
		return UIValue;
	}//end getStringUIValueForPoolAndLabel method
	
	private void verifyDeptSalarySettingWorksheets(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		writeToLogFile("**** method verifyDeptSalarySettingWorksheets has been called ****", logFileWriter);
		verifyRegularMeritPoolAmounts(logFileWriter);
		verifyOtherPoolAmounts(logFileWriter);
	}//end verifyDeptSalarySettingWorksheets method
	
	private void verifyOtherPoolAmounts(BufferedWriter logFileWriter) throws Exception{
		for(int j=0; j<deptWorksheets.size(); j++){
			String department = getDeptStringForIndex(j);
			ArrayList<String[]> designees = getDesigneesForDepartment(j, logFileWriter);
			if (! designees.isEmpty()){
				for(String[] designee: designees){
					String UIPercentage = getDesigneePercentage(designee[0], department, designee[1], designee[2], logFileWriter);
					writeToLogFile("UI Percentage derived is "+UIPercentage, logFileWriter);

					if (ControlSheetUI.getDoubleFromString(designee[3]) == ControlSheetUI.getDoubleFromString(UIPercentage)){
						writeToLogFile("Allocation Percentage found in department worksheet matches value in the UI, as expected", logFileWriter);
					}//end if - it matches
					else{
						String message = "MISMATCH! Allocation Percentage found in department " + department
								+ " worksheet is " + designee[3] +", but Allocation Percentage found in UI is"
								+ UIPercentage + "; ";
						writeToLogFile(message, logFileWriter);
						verificationErrors.append(message);
					}
				}//end for loop
			}//end if - there are actual Designee rows not in the Market Pool
		}//end for loop - iterating through the departments
	}//end verifyOtherPoolAmounts method
	
	/*
	 * get the locator from the locator file, and iterate through the Department column in 
	 * the section for the Pool, and then, when a match is found, take the row number and use
	 * it to find the locator for the same row for the Rank column, and see if it's a match.
	 * If it matches the Department and the Rank, then verify that there's a match for the Gender
	 * as well.  If all three match, then return the String in the same row under the 
	 * "% of Salary Base" column.
	 */

	private String getDesigneePercentage(String pool, String dept, String rank, String gender, 
			BufferedWriter logFileWriter) throws Exception{
		String UIPercent = new String();
		boolean rowExists = true;
		for (int i=1; rowExists; i++){
			String locatorNameSuffix = "DepartmentColumnBase";
			String UIdept = SalaryBaseVerificationFromUITest.getStringValue(pool, i, locatorNameSuffix, 
			logFileWriter, locatorsToOutputFile);
			if (UIdept.isEmpty()){
				writeToLogFile("Reached the last row", logFileWriter);
				rowExists = false;
			}//end if - the row does not exist
			else{
				writeToLogFile("Department examined in the UI is " + UIdept, logFileWriter);
				if (UIdept.equalsIgnoreCase(dept)){
					writeToLogFile("Match on Department "+ dept, logFileWriter);
					locatorNameSuffix = "RankColumnBase";
					String UIRank = SalaryBaseVerificationFromUITest.getStringValue(pool, i, locatorNameSuffix, 
							logFileWriter, locatorsToOutputFile);
					if (UIRank.equalsIgnoreCase(rank)){
						writeToLogFile("Match on Rank "+ rank, logFileWriter);
						locatorNameSuffix = "GenderColumnBase";
						String UIGender = SalaryBaseVerificationFromUITest.getStringValue(pool, i, locatorNameSuffix, 
								logFileWriter, locatorsToOutputFile);
						if (UIGender.equalsIgnoreCase(gender)){
							writeToLogFile("Match on Gender "+ gender +", match is complete", logFileWriter);
							locatorNameSuffix = "PercentSalaryBaseColumnBase";
							UIPercent = SalaryBaseVerificationFromUITest.getStringValue(pool, i, locatorNameSuffix, 
									logFileWriter, locatorsToOutputFile);
						}//end if - Gender matches
					}//ed if - Rank matches
				}//end if - Department matches
			}//end else - the row exists
		}
		return UIPercent;
	}//end getDesigneePercentage method
	
	private ArrayList<String[]> getDesigneesForDepartment(int deptIndex, 
										BufferedWriter logFileWriter) throws Exception{
		ArrayList<String[]> designees = new ArrayList<String[]>();
		String sheetName = TestFileController.DepartmentSalarySettingWorksheetName;
		logFileWriter.newLine();
		writeToLogFile("Department under analysis is " +getDeptStringForIndex(deptIndex), logFileWriter);
		SSExcelReader worksheet = deptWorksheets.get(deptIndex);
		worksheet.setRowForColumnNames(5);
		String[] columns = worksheet.getColumnNames(sheetName);
		int rowNum = worksheet.getCellRowNum(sheetName,	0, "Allocation Percent");
		for (int col=0; col<columns.length; col++){
			writeToLogFile("Column index "+ col +" has column name "+columns[col], logFileWriter);
			if ( (! ((columns[col] == null) || (columns[col].isEmpty()) ) ) 
					&& (columns[col].contains("(")) ){
				writeToLogFile("Found column for Designees - "+columns[col], logFileWriter);
				String[] designee = new String[4];
				logFileWriter.write("... with components ");
				String poolOrFund = "Pool";
				if (columns[col].contains("Fund"))
					poolOrFund = "Fund";
				designee[0] = columns[col].substring(0, columns[col].indexOf(poolOrFund)) + poolOrFund;
				logFileWriter.write("Pool: '"+designee[0]);
				designee[1] = columns[col].substring(columns[col].indexOf('(')+1, 
													columns[col].indexOf(','));
				logFileWriter.write("', Rank: '"+designee[1]);
				designee[2] = columns[col].substring(columns[col].indexOf(',')+1, 
														columns[col].indexOf(')'));
				logFileWriter.write("', Gender: '" + designee[2] + "'");
				designee[3] = worksheet.getCellData(sheetName, col, rowNum);
				logFileWriter.write("', Allocation Percent: '"+designee[3]+"'");
				logFileWriter.newLine();
				designees.add(designee);
			}//end if
		}//end for loop
		return designees;
	}//end getDesigneesForDepartment method

	
	private void verifyRegularMeritPoolAmounts(BufferedWriter logFileWriter) throws Exception{
		writeToLogFile("*** method verifyRegularMeritPoolAmounts called ***", logFileWriter);
		
		String colName = "Regular Merit Pool";
		
		for (int i=0; i<deptWorksheets.size(); i++){
			String dept = getDeptStringForIndex(i);
			logFileWriter.newLine();
			writeToLogFile("Department "+dept+" is being examined", logFileWriter);
			double sheetValue = getAllocPercentFromDeptWorksheet(i, colName, logFileWriter);
			String locatorName = "DepartmentFacultyPercentageAmount";
			double UIValue = getDoubleValueFromUI(locatorName, logFileWriter);
			verificationErrors = ControlSheetUI.compareValues(UIValue, sheetValue, verificationErrors, logFileWriter);
		}//end for
	}//end verifyRegularMeritPoolAmounts method
	
	private String getDeptStringForIndex(int index) throws Exception{
		String dept = new String();
		switch(index){
		case BiologyIndex:
			dept = "Biology";
			break;
		case EconomicsIndex:
			dept = "Economics";
			break;
		case HistoryIndex:
			dept = "History";
			break;
		case MathematicsIndex:
			dept = "Mathematics";
			break;
		case PhysicsIndex:
			dept = "Physics";
			break;
		case StatisticsIndex:
			dept = "Statistics";
			break;
		case TheatreAndPerformanceStudiesIndex:
			dept = "Theatre And Performance Studies";
			break;
		}//end switch statement
		return dept;
	}
	
	private double getDoubleValueFromUI(String locatorName, BufferedWriter logFileWriter) throws Exception{
		double allocPercent = 0;
		int rowNum = locators.getCellRowNum(locatorSheetName, "Name", locatorName);
		if (ExcelReaderToOutputFile)
			writeToLogFile("Row number for " + locatorName + " is " + rowNum, logFileWriter);
		String locator = locators.getCellData(locatorSheetName, "Locator", rowNum);
		if (ExcelReaderToOutputFile)
			writeToLogFile("Locator used to find " + locatorName +" is " + locator, logFileWriter);
		String UIAmt = ControlSheetUI.getStringValueForLocator(locator, logFileWriter, locatorsToOutputFile);
		if (! (UIAmt == null || UIAmt.isEmpty())){
			allocPercent = ControlSheetUI.getDoubleFromString(UIAmt);
			writeToLogFile(locatorName +" amount derived from the UI is " + allocPercent, logFileWriter);
		}//end if - UIAmt is not null or an empty Strings
		return allocPercent;
	}//end getAllocPercentFromUI method
	
	private double getAllocPercentFromDeptWorksheet(int deptIndex, String colName, BufferedWriter logFileWriter) throws Exception{
		writeToLogFile("*** method getAllocPercentFromDeptWorksheet called with deptIndex "+deptIndex 
				+ ", and column name " +colName +" ***", logFileWriter);
		String sheetName = TestFileController.DepartmentSalarySettingWorksheetName;
		double allocPercent = 0;
		SSExcelReader worksheet = deptWorksheets.get(deptIndex);
		
		int colNum = worksheet.getCellColumnNumber(sheetName, colName, 1, 6, logFileWriter, ExcelReaderToOutputFile);
		int rowNum = worksheet.getCellRowNum(sheetName,	colNum-1, "Allocation Percent", 1);
		if (ExcelReaderToOutputFile){
			writeToLogFile("Row number for Allocation Percent is " + rowNum, logFileWriter);
		}
		String allocPercentString = worksheet.getCellData(sheetName, colNum, rowNum, true);
		if (ExcelReaderToOutputFile)
			writeToLogFile("String found for Allocation Percent is " + allocPercentString, logFileWriter);
		if (! (allocPercentString == null || allocPercentString.isEmpty()))
			allocPercent = ControlSheetUI.getDoubleFromString(allocPercentString);
		return allocPercent;
	}
	
	private void verifyClusterSalarySettingWorksheets(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		writeToLogFile("**** method verifyClusterSalarySettingWorksheets has been called ****", logFileWriter);
		
		sheetName = TestFileController.ClusterSalarySettingWorksheetWorksheetName;
		ArrayList<String> pools = new ArrayList<String>();
		for (int i=1; i<ASCTest.sourcePools.length; i++)
			pools.add(ASCTest.sourcePools[i]);
		for (String fund: funds)
			pools.add(fund);
		
		for (SSExcelReader reader: clusterWorksheets){
			int colForSheetRowLabels = reader.getCellColumnNumber(sheetName, "Cluster Dean Reallocation within Depts", 2, logFileWriter, ExcelReaderToOutputFile) - 1;
			if (colForSheetRowLabels < 0 ) colForSheetRowLabels = 0;
			for (String pool : pools){
				verifyClusterRowLabels(reader, sheetName, pool +" Amount", 2, colForSheetRowLabels, logFileWriter);
			}//iterating through the pools and funds
		}//end outer for - iterating through the Cluster Worksheets

	}//end verifyClusterSalarySettingWorksheets method
	
	private void verifySchoolSalarySettingWorksheets(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		writeToLogFile("**** method verifySchoolSalarySettingWorksheets has been called ****", logFileWriter);
		
		sheetName = TestFileController.SchoolSalarySettingReportWorksheetName;
		ArrayList<String> pools = new ArrayList<String>();
		for (int i=1; i<ASCTest.sourcePools.length; i++)
			pools.add(ASCTest.sourcePools[i]);
		for (String fund: funds)
			pools.add(fund);
		SSExcelReader reader = TestFileController.SchoolSalarySettingReport;
		int colForSheetRowLabels = reader.getCellColumnNumber(sheetName, "Dean Reallocation within Depts", 2, logFileWriter, ExcelReaderToOutputFile) - 1;

		
		for (String fund: funds)
			verifyAllClusterRowLabels(reader, sheetName, fund +" Amount", 2, colForSheetRowLabels, logFileWriter);
		for (String pool : pools){
			verifyAllClusterRowLabels(reader, sheetName, pool +" Amount", 2, colForSheetRowLabels, logFileWriter);
		}//end for
		
	}//end verifySchoolSalarySettingWorksheets method
	
	private void verifyAllClusterRowLabels(SSExcelReader reader, String sheetName, String pool, int rowForColumnNames, 
			int colForSheetRowLabels, BufferedWriter logFileWriter) throws Exception{

		String[] clusterNames = {"Natural Sciences", "Humanities and Arts", "Social Sciences"};
		logFileWriter.newLine();
		writeToLogFile("*** method verifyAllClusterRowLabels called for pool " + pool 
				+", and ALL Clusters "
				+ ", and " + rowForColumnNames + " as row for column names, and " 
				+ colForSheetRowLabels +" as the column for Sheet Row Labels ***", logFileWriter);
		String[] sheetRowLabels = {"Total Dean Reserve Funds:", "Under/Over Allocation: "};
		
		for (String sheetRowLabel: sheetRowLabels){
			String sheetValueString = getSheetDataForPool(reader, sheetName, pool, rowForColumnNames, colForSheetRowLabels, sheetRowLabel, logFileWriter);
			if ((! (sheetValueString == null)) && (! sheetValueString.isEmpty())){
				double sheetValue = ControlSheetUI.getDoubleFromString(sheetValueString);
				double UIValue = 0;
				for (int i=0; i<clusterNames.length; i++){
					UIValue += getClusterSummaryFromUI(clusterNames[i], pool.replaceAll(" Amount", ""), logFileWriter);
				}//end for loop
				verificationErrors = ControlSheetUI.compareValues(UIValue, sheetValue, verificationErrors, logFileWriter);
			}//end if
		}//end for loop - iterating through the sheet Row Labels
	}//end method verifyAllClusterRowLabels

	
	private void verifyClusterRowLabels(SSExcelReader reader, String sheetName, String pool, int rowForColumnNames, 
			int colForSheetRowLabels, BufferedWriter logFileWriter) throws Exception{
		String path = reader.getPath();
		String clusterName = path.substring(path.indexOf("Worksheet ")+10, path.indexOf(".x"));
		logFileWriter.newLine();
		writeToLogFile("*** method verifyClusterRowLabels called for pool " + pool 
				+", and Cluster '"+clusterName 
				+ "', and " + rowForColumnNames + " as row for column names, and " 
				+ colForSheetRowLabels +" as the column for Sheet Row Labels ***", logFileWriter);
		String[] sheetRowLabels = {"Total Dean Reserve Funds:", "Under/Over Allocation: "};
		
		for (String sheetRowLabel: sheetRowLabels){
			String sheetValueString = getSheetDataForPool(reader, sheetName, pool, rowForColumnNames, colForSheetRowLabels, sheetRowLabel, logFileWriter);
			if ((! (sheetValueString == null)) && (! sheetValueString.isEmpty())){
				double sheetValue = ControlSheetUI.getDoubleFromString(sheetValueString);
				double UIValue = getClusterSummaryFromUI(clusterName, pool.replaceAll(" Amount", ""), logFileWriter);
				verificationErrors = ControlSheetUI.compareValues(UIValue, sheetValue, verificationErrors, logFileWriter);
			}//end if
		}//end for loop - iterating through the sheet Row Labels
	}//end method verifyClusterRowLabels
	
	private double getClusterSummaryFromUI(String clusterName, String pool, BufferedWriter logFileWriter) throws Exception{
		writeToLogFile("*** method getClusterSummaryFromUI called with Cluster " + clusterName
								+", for " + pool + " ***", logFileWriter);
		double clusterSummary = 0;
		String clusterlocatorName = "ClusterColumnBase";
		String allocDollarsLocatorName = "AllocationDollarsColumnBase";
		boolean rowExists = true;
		pool = pool.replaceAll("Regular ",  "");
		for (int i=1; rowExists; i++){
			String UIClusterName = SalaryBaseVerificationFromUITest.getStringValue(pool, i, clusterlocatorName, 
					logFileWriter, locatorsToOutputFile);
			if (UIClusterName.equalsIgnoreCase(clusterName +" Cluster")){
				if (locatorsToOutputFile){
					writeToLogFile("Match found for Cluster " + clusterName 
							+", for "+pool +" at row " + i, logFileWriter);
				}
				String UIRowClusterAmount = SalaryBaseVerificationFromUITest.getStringValue(pool, i, allocDollarsLocatorName, 
						logFileWriter, locatorsToOutputFile);
				if ( (! (UIRowClusterAmount == null)) && (! UIRowClusterAmount.isEmpty()))
					if (locatorsToOutputFile){
						writeToLogFile("Adding amount " + UIRowClusterAmount +" to existing amount "+clusterSummary, logFileWriter);
					}
				clusterSummary += ControlSheetUI.getDoubleFromString(UIRowClusterAmount);
			}
			if (UIClusterName.isEmpty()){
				writeToLogFile("Reached the last row", logFileWriter);
				rowExists = false;
			}//end if - the row does not exist
		}
		if (locatorsToOutputFile){
			writeToLogFile("Returning a Cluster Summary value of " + clusterSummary 
					+" for Cluster "+clusterName +" for "+pool, logFileWriter);
		}
		return clusterSummary;
	}
	
	  public void writeToLogFile(String toBeWritten, BufferedWriter logFileWriter) throws Exception{
		  logFileWriter.write(toBeWritten);
		  logFileWriter.newLine();
	  }//end writeToLogFile method


}//end Class
