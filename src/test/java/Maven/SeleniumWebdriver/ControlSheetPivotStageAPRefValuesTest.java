package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.util.HashMap;
import java.util.LinkedList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ControlSheetPivotStageAPRefValuesTest extends ControlSheetReportSuiteAPRefValuesTest{
	
	StringBuffer verificationErrors;
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws Exception{
		System.out.println("Now executing the test method for class ControlSheetPivotStageAPRefValuesTest.");
		verificationErrors = new StringBuffer();
		String testName = "Verify AP Values in Control Sheet Pivot Stage Report";
		System.out.println("Now executing test: " + testName);
		String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		BufferedWriter logFileWriter = TestFileController.getLogFileWriter(logFileName);
		verifyAPValuesInControlSheetPivotStageReport(logFileWriter);
		TestFileController.writeTestResults("Test", "002", "Comparison of FTE Adjusted Raises", "Control Sheet Pivot Stage Report Tests", verificationErrors);
		logFileWriter.close();
	}

	/*
	 * This applies specifically to the Control Sheet Pivot Stage Report worksheet in the 
	 * Control Sheet Report Suite. 
	 * 
	 * This will simply call getActualAPFacultyValues to get the values from the 
	 * Control Sheet Pivot Stage Report that are to be compared with the expected values, 
	 * and then call the superclass method "verifyAPValuesInControlSheetReportSuite",
	 * passing the actual values to it as a parameter.  
	 * 
	 * The superclass method will gather the expected values and do the comparison 
	 * between the expected and actual values.
	 * 
	 */
	public void verifyAPValuesInControlSheetPivotStageReport(BufferedWriter logFileWriter) throws Exception{
		TestFileController.ControlSheetReportSuite.setRowForColumnNames(TestFileController.ControlSheetPivotHeaderRow);
		String[][] actualValues = super.getActualAPFacultyValues(TestFileController.ControlSheetPivot_StageWorksheetName,logFileWriter);
		String[][] expectedValues = getExpectedAPFacultyValues(logFileWriter);
		super.verifyAPValuesInControlSheetReportSuite(expectedValues, actualValues, logFileWriter);
	}//end verifyAPValuesInControlSheetPivotStageReport method

	

}
