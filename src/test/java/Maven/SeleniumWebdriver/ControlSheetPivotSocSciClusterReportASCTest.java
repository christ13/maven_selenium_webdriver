package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ControlSheetPivotSocSciClusterReportASCTest extends
		ControlSheetPivotOneClusterReportASCTest {

	@Before
	public void setUp() throws Exception {
		clusterName = "Social Sciences Cluster";

		mySheetName = TestFileController.ControlSheetPivot_SocSciWorksheetName;
		testName = "ControlSheetPivotSocSciClusterReportASCTest";
		testCaseNumber = "028";
		testDescription = "ASC Test of Pivot SocSci Cluster Report";
		logFileName = "Control Sheet Pivot SocSci Cluster ASC Test.txt";
		
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void test() throws Exception{
		super.test();
	}

}
