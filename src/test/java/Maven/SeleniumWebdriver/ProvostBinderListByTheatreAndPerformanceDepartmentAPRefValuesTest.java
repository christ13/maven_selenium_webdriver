package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ProvostBinderListByTheatreAndPerformanceDepartmentAPRefValuesTest
		extends ProvostBinderListByDepartmentAPRefValuesTest {

	@Before
	public void setUp() throws Exception {
		System.out.println("setUp method for class ProvostBinderListByTheatreAndPerformanceDepartmentAPRefValuesTest is being executed");
		departmentName = "Theater & Performance Studies";
		myExcelReader = TestFileController.ProvostBinderListByDepartment;
		worksheetName = departmentName;
		testEnv = "Test";
		testName = "Test Provost Binder List Theater And Performance Studies";
		testDescription = "Verify AP Values in Provost Binder List Theater And Performance Studies";
		testCaseNumber = "019";
		System.out.println("Column Names are as follows: ");
		String[] colNames = myExcelReader.getColumnNames(worksheetName);
		for (int i=0;i<colNames.length;i++){
			System.out.print(colNames[i]);
		}
		System.out.println();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void test() throws Exception{
		super.test();
	}

}
