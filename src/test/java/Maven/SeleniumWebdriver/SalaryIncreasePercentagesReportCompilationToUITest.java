package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.text.NumberFormat;
/*
import java.util.Date;
import java.util.HashMap;
*/
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
//import org.openqa.selenium.By;

public class SalaryIncreasePercentagesReportCompilationToUITest {
	protected String mySheetName;
	protected String testName;
	protected StringBuffer verificationErrors;
	protected BufferedWriter logFileWriter;
	protected String logFileName;
	protected String testEnv;
	protected String testCaseNumber;
	protected String testDescription;
	protected String logDirectoryName = ReportRunner.logDirectoryName;
	private HSSFExcelReader inputFile;
	private HSSFExcelReader locators;
	private static final String MeritPoolSalaryBase = "MeritPoolSalaryBase";
	private static final String DepartmentFacultySalaryBase = "DepartmentFacultySalaryBase";
	private static final String TerminalYearFacultySalaryBase = "TerminalYearSalaryBase";
	private static final String DeptChairsSalaryBase = "DeptChairsSalaryBase";
	private static final String SrAssociateDeansSalaryBase = "SrAssociateDeansSalaryBase";
	
	protected SSExcelReader deptVerification;
	protected SSExcelReader deanWorksheet;
	protected int rowForColumnNames;
	private boolean locatorsToOutputFile = true;
	private boolean ExcelReaderToOutputFile = true;

	@Before
	public void setUp() throws Exception {
		testName = "SalaryIncreasePercentagesReportCompilationToUITest";
		testEnv = "Test";
		testCaseNumber = "045";
		testDescription = "Salary Setting - Report Compilation to UI";
		logFileName = "Report Compilation to UI.txt";
		logFileName = ReportRunner.logDirectoryName +"/" + logFileName;
		logFileWriter = TestFileController.getLogFileWriter(logFileName);

		deptVerification= TestFileController.DepartmentVerificationReport;
		deanWorksheet= TestFileController.DeanSalarySettingReport;

		locators = TestFileController.locators;
		locators.setRowForColumnNames(0);
		
		inputFile = TestFileController.fileIn;
		inputFile.setRowForColumnNames(0);
		verificationErrors = new StringBuffer();

	}

	@After
	public void tearDown() throws Exception {
		TestFileController.writeTestResults(testEnv, testCaseNumber, testName, testDescription, verificationErrors);
		logFileWriter.close();
	}//end tearDown method

	@Test
	public void test() throws Exception{
		String message = "Now executing the '" + testDescription +"' test";
		writeToLogFile(message, logFileWriter);
		System.out.println(message);
		if (! ControlSheetUI.openSalarySetting(logFileWriter)){
			fail("Could not open Salary Setting");
		}//end if Salary Setting is not open
		double expectedSalaryBaseSum = 0; 
		double actualSalaryBase = ControlSheetUI.getSalaryBaseForLabel(DeptChairsSalaryBase, logFileWriter);
		double expectedSalaryBase = getDepartmentChairSalaryBaseFromReports(logFileWriter, ExcelReaderToOutputFile);
		writeToLogFile("Department Chairs actual Salary Base is " + actualSalaryBase, logFileWriter);
		compareSalaryBases(expectedSalaryBase, actualSalaryBase, logFileWriter);
		expectedSalaryBaseSum += expectedSalaryBase;

		expectedSalaryBase = getTerminalYearSalaryBaseFromReports(logFileWriter, ExcelReaderToOutputFile);
		actualSalaryBase = ControlSheetUI.getSalaryBaseForLabel(TerminalYearFacultySalaryBase, logFileWriter);
		writeToLogFile("Terminal Year Faculty actual Salary Base is " + actualSalaryBase, logFileWriter);
		compareSalaryBases(expectedSalaryBase, actualSalaryBase, logFileWriter);
		expectedSalaryBaseSum += expectedSalaryBase;

		expectedSalaryBase = getDepartmentFacultySalaryBaseFromReports(logFileWriter, ExcelReaderToOutputFile);
		actualSalaryBase = ControlSheetUI.getSalaryBaseForLabel(DepartmentFacultySalaryBase, logFileWriter);
		writeToLogFile("Department Faculty actual Salary Base is " + actualSalaryBase, logFileWriter);
		compareSalaryBases(expectedSalaryBase, actualSalaryBase, logFileWriter);
		expectedSalaryBaseSum += expectedSalaryBase;
		
		expectedSalaryBase = getSrAssocDeanSalaryBaseFromReports(logFileWriter);
		actualSalaryBase = ControlSheetUI.getSalaryBaseForLabel(SrAssociateDeansSalaryBase, logFileWriter);
		writeToLogFile("Senior Associate Deans actual Salary Base is " + actualSalaryBase, logFileWriter);
		compareSalaryBases(expectedSalaryBase, actualSalaryBase, logFileWriter);
		expectedSalaryBaseSum += expectedSalaryBase;
		
		expectedSalaryBase = getMeritPoolSalaryBase(expectedSalaryBaseSum, logFileWriter);
		actualSalaryBase = ControlSheetUI.getSalaryBaseForLabel(MeritPoolSalaryBase, logFileWriter);
		writeToLogFile("Merit Pool actual Salary Base is " + actualSalaryBase, logFileWriter);
		compareSalaryBases(expectedSalaryBase, actualSalaryBase, logFileWriter);
	}//end test method
	
	private void compareSalaryBases(double expectedSalaryBase, double actualSalaryBase, BufferedWriter logFileWriter) throws Exception{
		if ((expectedSalaryBase <= (actualSalaryBase + 2)) && (expectedSalaryBase >= (actualSalaryBase - 2))){
			writeToLogFile("Expected and Actual Salary Bases are about equal - Expected: " 
		+ expectedSalaryBase +", Actual: "+actualSalaryBase, logFileWriter);
		}//end if - they're about equal
		else{
			String message = "Expected and Actual Salary Bases are NOT about equal - Expected: " 
					+ expectedSalaryBase +", Actual: "+actualSalaryBase + ".  \n";
			verificationErrors.append(message);
			writeToLogFile(message, logFileWriter);
		}//end else - FAILURE!!! They're not equal
	}//end compareSalaryBases method
	
	/*
	 * This method extracts the Department Chair Salary information from Faculty members in the
	 * Department Verification Report.  
	 *  
	 */
	private double getDepartmentChairSalaryBaseFromReports(BufferedWriter logFileWriter, boolean writeToOutputFile) throws Exception{
		return getSalaryBaseFromColumnAndValue(HandSOnTestSuite.FY + " Description", "Chair - ", logFileWriter, writeToOutputFile)
				+ getSalaryBaseFromColumnAndValue(HandSOnTestSuite.FY + " Description", "Program Director - ", logFileWriter, writeToOutputFile);
	}//end getDepartmentChairSalaryBaseFromReports method
	
	/*
	 * This method extracts the Terminal Year Salary information from Faculty members in the
	 * Department Verification Report.  
	 *  
	 */
	private double getTerminalYearSalaryBaseFromReports(BufferedWriter logFileWriter, boolean writeToOutputFile) throws Exception{
		return getSalaryBaseFromColumnAndValue(HandSOnTestSuite.FY + " Description", "Terminal Year", logFileWriter, writeToOutputFile);
	}//end getTerminalYearSalaryBaseFromReports method

	/*
	 * This method extracts the Department Faculty Salary information from Faculty members in the
	 * Department Verification Report.  
	 *  
	 */
	private double getDepartmentFacultySalaryBaseFromReports(BufferedWriter logFileWriter, boolean writeToOutputFile) throws Exception{
		
		return getSalaryBaseFromColumnAndValue("Eligible", "Yes", logFileWriter, writeToOutputFile)
				- getTerminalYearSalaryBaseFromReports(logFileWriter, writeToOutputFile)
				 - getDepartmentChairSalaryBaseFromReports(logFileWriter, writeToOutputFile);
	}//end getDepartmentFacultySalaryBaseFromReports method

	/*
	 * This method examines the Dean Salary Setting Worksheet and extracts the Salary information 
	 * from the Senior Associate Deans, then adds it up and returns it.
	 */
	private double getSrAssocDeanSalaryBaseFromReports(BufferedWriter logFileWriter) throws Exception{
		double salaryBase = 0;//initialize this to zero
		writeToLogFile("*** method getSrAssocDeanSalaryBaseFromReports called ***", logFileWriter);
		String sheetName = TestFileController.DeanSalarySettingWorksheetName;
		int rowNum = deanWorksheet.getRowForColumnNames();
		writeToLogFile("The column header row for the sheet '" + sheetName +"' is at row " + rowNum, logFileWriter);
		
		int lastRow = deanWorksheet.getRowCount(sheetName);
		writeToLogFile("There are " +  lastRow +" rows in the worksheet '" + sheetName + "'", logFileWriter);
		while (rowNum > 0 && rowNum<TestFileController.DeanSalarySettingWorksheetLastRow){
			rowNum = deanWorksheet.getCellRowNum(sheetName, "16-17 Description",
					"Senior Associate Dean, H&S Dean's Office;", rowNum, lastRow);
			String salaryString = deanWorksheet.getCellData(sheetName, 
					"Final 15-16 FTE Adjusted Salary", rowNum);
			//writeToLogFile("Row number examined is "+rowNum + ", Salary: "+salaryString, logFileWriter);
			rowNum++;
			if (salaryString.length() != 0){
				double salary = Double.valueOf((Long) NumberFormat.getNumberInstance(java.util.Locale.US).parse(salaryString));
				salaryBase += salary;
			}//end if 
		}//end while
		return salaryBase;
	}//end getSrAssocDeanSalaryBaseFromReports method


	/*
	 * What we're going to have to do is iterate through the all of the "Destination" fields in the 
	 * "Adjustments/Transfers" section and then determine which of them have "Salary Base" as 
	 * a label that is selected (what is the command that will do that)?  Then, once we know which
	 * row(s) have that, we add up all of the fields in the "Amount" section with that "Salary Base" 
	 * selection having been made, and then we add that to the Salary Base sum
	 * 
	 * In order to do this, we will have to write methods that will go into the ControlSheetUI class.
	 */
	private double getMeritPoolSalaryBase(double salaryBaseSum, BufferedWriter logFileWriter) throws Exception{
		writeToLogFile("*** method getMeritPoolSalaryBase has been called with salary base sum " 
						+ salaryBaseSum, logFileWriter);
		double salaryBase = salaryBaseSum 
				+ ControlSheetUI.getTransferSumForDestination ("Salary Base", logFileWriter, locatorsToOutputFile);
		
		return salaryBase;
	}//end getMeritPoolSalaryBase method
	
	private double getSalaryBaseFromColumnAndValue(String colName, String value, BufferedWriter logFileWriter, boolean writeToOutputFile) throws Exception{
		double salaryBase = 0;
		String sheetName = TestFileController.DepartmentVerificationReportWorksheetName;
		int rowNum = deptVerification.getRowForColumnNames();
		while (rowNum > 0 && rowNum<deptVerification.getRowCount(sheetName)){
			rowNum = deptVerification.getCellRowNum(sheetName, colName, value, rowNum);
			String salaryString = deptVerification.getCellData(sheetName, 
					"Final " + HandSOnTestSuite.PY +" Salary", rowNum);
			String FTE = deptVerification.getCellData(sheetName, "FTE", rowNum);
			boolean isEligible = deptVerification.getCellData(sheetName, "Eligible", rowNum).equalsIgnoreCase("Yes");
			if (writeToOutputFile)
				writeToLogFile("Row number examined is "+rowNum + ", Salary: "+salaryString+", and is eligible: "+isEligible, logFileWriter);
			rowNum++;
			if ((salaryString.length() != 0) &&(isEligible)){
				double salary = Double.valueOf((Long) NumberFormat.getNumberInstance(java.util.Locale.US).parse(salaryString));
				if (! FTE.equalsIgnoreCase("100")){
					double fte = Double.valueOf((Long) NumberFormat.getNumberInstance(java.util.Locale.US).parse(FTE))/100;
					salary *= fte;
				}
				salaryBase += salary;
			}//end if
		}//end while loop
		return salaryBase;
	}//end getSalaryBaseFromColumnAndValue method
	

	  public void writeToLogFile(String toBeWritten, BufferedWriter logFileWriter) throws Exception{
		  logFileWriter.write(toBeWritten);
		  logFileWriter.newLine();
	  }//end writeToLogFile method

}//end Class
