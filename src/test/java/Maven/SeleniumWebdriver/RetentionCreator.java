package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

public class RetentionCreator {

	public RetentionCreator() {
		System.out.println("RetentionCreator Class has been instantiated");
	}
	
	@Before
	public void setUp() throws Exception {
		System.out.println("setUp method of RetentionCreator Class is running");
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("tearDown method of RetentionCreator Class is running");
	}

	@Test
	public void test() {
		System.out.println("test method of RetentionCreator Class is running");
	}
	
	public static String createNewRetention (BufferedWriter logFileWriter, String facultyName, String testCaseID) throws Exception{
		String retentionID = new String();
		FacultySearchPageUIController.navigateToSearchPage(logFileWriter);
		String linkText = TestDataRecorder.getTestCaseData(testCaseID, TestDataRecorder.highLevelDataColumns[TestDataRecorder.HANDSONID_INDEX]);
		String message = "Attempting to navigate to Faculty Page with HandSOn ID "+linkText+" to create a new Retention";
		System.out.println(message);
		logFileWriter.write(message);
		logFileWriter.newLine();
		FacultySearchPageUIController.openFacultyPage(logFileWriter, By.linkText(linkText));
		FacultyUIController.openRetentionTab(logFileWriter, facultyName);
		Thread.sleep(2000);
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Retention Tab", "NewRetentionButton", logFileWriter);
		FacultyUIController.waitAndClick(by);
		Thread.sleep(2000);
		if (RetentionUIController.switchWindowByURL(RetentionUIController.newRetentionURL, logFileWriter)) {
			logFileWriter.write("Successfully switched to the new Retention window");
			logFileWriter.newLine();
		}
		else return retentionID;
		RetentionUIController.fillOutGeneralRetentionInformation(logFileWriter, testCaseID);
		retentionID = RetentionUIController.saveNewRetention(logFileWriter);
		return retentionID;
	}

}
