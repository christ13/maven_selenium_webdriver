/**
 * 
 */
package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.TreeSet;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author christ13
 *
 * OK, so we need to create infrastructure that will allow the ASCTest superclass to get
 * access to the methods in the ExpectedASCData Class so that we can find out what the 
 * DOShares and deptShares are.  
 * 
 * So, we want to get the values from the ASCFacultyValues Objects corresponding
 * to each of the ExpectedASCData Objects.  We'll have to get the sum of the doShare and 
 * deptShare for each of the pools.  We'll have to use the 
 * getAllSharesForStageAndPool(String stage, String sourcePool) method associated with the
 * ASCFacultyValues Object by calling it through each corresponding ExpectedASCData Object.
 * 
 * We have to devise mechanics that will call these methods with the appropriate String or int
 * parameters and get the results into int arrays in the ASCTest Class which are designated for
 * this particular purpose.
 * 
 * First, we fetch the data from the ASCFacultyValues Objects via the ExpectedASCData Objects
 * after the subset is taken.  We may want a sum of both DO Shares and Dept Shares, or we may
 * want just DO Shares, and we may want to do DO Shares and Dept Shares separately.  We have
 * to specify a way in which the subclass can specify what, exactly is to be done.  Maybe we 
 * can simply let the subclasses take care of this and we can simply hold the variables here,
 * and do the applicable comparisons.
 * 
 */
public class ASCTest {
	
	public static final String[] sourcePools 
		= {"Regular Merit Pool", "Market Pool", "Test Pool", "Albert Eustace Peasmarch Fund", 
		"Gender Pool"};
	public static final int RegularMeritPoolIndex = 0;
	public static final int MarketPoolIndex = 1;
	public static final int TestPoolIndex = 2;
	public static final int PeasmarchFundIndex = 3;
	public static final int GenderPoolIndex = 4;
	public static final String[] stages = {"Known", "Pending", "Estimated"};
	public static final int KnownStageIndex = 0;
	public static final int PendingStageIndex = 1;
	public static final int EstimatedStageIndex = 2;
	
	//these are instantiated in the setUp() methods of the subclasses
	protected SSExcelReader myExcelReader;
	protected int rowForColumnNames;
	protected String mySheetName;
	protected String testName;
	protected StringBuffer verificationErrors;
	protected BufferedWriter logFileWriter;
	protected String logFileName;
	protected String testEnv;
	protected String testCaseNumber;
	protected String testDescription;
	protected String logDirectoryName = ReportRunner.logDirectoryName;
	
	//these restrict the data to a given subset
	protected TreeMap<String, String> criteria;
	
	//sometimes, we need to eliminate ExpectedASCData Objects with zero amount in the DO Share
	protected boolean excludeZeroDOShare;
	
	//sometimes, we need to exclude deans and/or chairs
	protected boolean excludeDeans = false;
	protected boolean excludeChairs = false;
	
	//sometimes, we need to exclude one or more particular faculty member(s)
	protected String[] facultyToExclude;
	
	//sometimes, we need to force the system to include one or more faculty member(s)
	//any faculty here will have any and all of their ASC's included
	protected TreeSet<String> facultyToInclude;
	
	//sometimes, we exclude the Market Share from the overall calculation
	boolean excludeMarketShare = false;
	
	//sometimes, we need to take the salary commitment without the FTE Adjustment
	protected boolean adjustForFTE = true;
	//sometimes, we have to consider outside appointments when adjusting for FTE
	protected boolean consideringOutsideAppt = false;
	//sometimes, we return the sum of ASC's
	protected boolean summarizingASCs = false;
	//sometimes, we need to make adjustments and back out summary data that includes Previous Year Commitments
	protected boolean backoutPYs = false;
	
	//these are relevant to expected and actual data
	protected String[] expectedStringAttributeNames;
	protected String[][] expectedStringAttributeValues;
	protected String[] actualStringAttributeNames;
	protected String[][] actualStringAttributeValues;
	protected String[] expectedAmountAttributeNames;
	protected int[][] expectedAmountAttributeValues;
	protected String[] actualAmountAttributeNames;
	protected int[][] actualAmountAttributeValues;
	//this is an attribute that isn't explicitly present, but is a criterion for the row to appear on the worksheet:
	protected String dataPresentCriterionName;
	protected String dataPresentCriterionValue;
	
	/*
	 * This is relevant to specific, customized summary values.
	 * Each String key is a name that maps to an ArrayList of names of
	 * types of data that has to be met in order for data to be included in
	 * the summary values that are listed here.
	 * Of note, the String key is the column name in the 
	 * Control Sheet Detail Report that we're going to derive summary
	 * values from.
	 */
	protected TreeMap<String, ArrayList<String>> expectedSummaryCriteria;
	protected String[] expectedSummaryNames;
	protected String[] actualSummaryNames;
	protected int[][] expectedSummaryValues;
	protected int[][] actualSummaryValues;
	
	/*
	 * These are the variables that hold the DO Share and/or Dept Share values
	 * that are to be compared with each other.
	 * shareNames is the set of columns, and it is used by the subclasses to 
	 * instantiate the expectedShareCriteria.  In each element in expectedShareCriteria,
	 * one of the names in the shareNames array is the String key used to access the 
	 * ArrayList<String> value which is a set of important criteria for selecting the 
	 * method to be used.  
	 */
	protected String[] shareNames;
	protected TreeMap<String, ArrayList<String>> expectedShareCriteria;
	protected int[][] expectedShareValues;
	protected int[][] actualShareValues;
	//sometimes, we won't use the doShares and deptShares - only the ASC "Total Amount"
	protected boolean useASCOnly = false;
	
	//these will be the variables that determine where, in the file, a search may happen
	protected ArrayList<String[]> searchDelimiters;
	protected int searchDelimiterColumn;
	protected int searchDelimiterIndex = -1;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		outputMessage("*** setUp() method for test name " +testName 
				+ " is being called", logFileWriter);
		if (testName.equalsIgnoreCase("ControlSheetReportAggregateTest")){
			outputMessage("Note: there will be no comparison between the numbers of "
					+"expected and actual attribute names", logFileWriter);
		}//if this the Control Sheet Aggregate test, the comparison will not be done
		else if (expectedStringAttributeNames.length != actualStringAttributeNames.length){
			outputMessage("!!!! WARNING !!!! Different number of attributes names specified; "
					+ "number of expected attribute names: " + expectedStringAttributeNames.length
					+", number of actual attribute names: " 
					+ actualStringAttributeNames.length, logFileWriter);
		}//end else if - the number of attribute names is not the same, warning is given
		myExcelReader.setRowForColumnNames(rowForColumnNames);
		outputMessage("Excel file is set with row # " + rowForColumnNames + " as the row with column names", logFileWriter);
		//printExpectedASCDataStatus();
		if ( (dataPresentCriterionName == null) || (dataPresentCriterionValue == null) ){
			dataPresentCriterionName = new String();
			dataPresentCriterionValue = new String();
		}//end if
		if ( (expectedShareCriteria == null) || (shareNames == null)) {
			shareNames = new String[0];
			expectedShareCriteria = new TreeMap<String, ArrayList<String>>();
			expectedShareValues = new int[0][0];
			actualShareValues = new int[0][0];
		}//end if
		if ( (expectedSummaryNames == null ) || (actualSummaryNames == null) ){
			expectedSummaryNames = new String[0];
			actualSummaryNames = new String[0];
			expectedSummaryCriteria = new TreeMap<String, ArrayList<String>>();
			expectedSummaryValues = new int[0][0];
			actualSummaryValues = new int[0][0];
		}//end if
		if (facultyToExclude == null) {
			facultyToExclude = new String[0];
		}//end if
		if (facultyToInclude == null){
			facultyToInclude = new TreeSet<String>();
		}
	}//end setUp() method

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		//printExpectedASCDataStatus();
		TestFileController.writeTestResults(testEnv, testCaseNumber, testName, testDescription, verificationErrors);
		logFileWriter.close();
		myExcelReader.closeWorkbook();
	}
	
	@Test
	public void test() throws Exception {
		//here, we identify the test being run
		outputMessage("*** test() method for test name " +testName 
				+ " is being called", logFileWriter);
		//now, we send a message indicating that this is the beginning of the iteration
		if (searchDelimiterIndex == -1)
			outputMessage("\n\n**** There are no delimiters to this test.  "
					+"The entire report will be searched  ***\n\n", logFileWriter);
		else{
			outputMessage("\n\n**** Iteration "+ (searchDelimiterIndex + 1) 
					+ " is now beginning with attribute name "
					+ searchDelimiters.get(searchDelimiterIndex)[0] 
					+" and attribute value " + searchDelimiters.get(searchDelimiterIndex)[1] 
							+ " ***\n\n", logFileWriter);
			//criteria.put(searchDelimiters.get(searchDelimiterIndex)[0], searchDelimiters.get(searchDelimiterIndex)[1]);
		}
		//this is the actual test - calling three methods
		getExpectedAttributes();
		if ( !( 
				(expectedStringAttributeValues.length == 0)
				&& (expectedAmountAttributeValues.length == 0)
				&& (expectedSummaryValues.length == 0)
				&& (expectedShareValues.length == 0)
				)){
			getActualAttributes();
			compareData();
		}
		//now, we send a message indicating that this is the end of the iteration
		if (searchDelimiterIndex != -1){
			outputMessage("\n\n**** Iteration "+ (searchDelimiterIndex + 1) 
					+ " has now ended with attribute name "
					+ searchDelimiters.get(searchDelimiterIndex)[0] 
					+" and attribute value " + searchDelimiters.get(searchDelimiterIndex)[1] 
							+ " ***\n\n", logFileWriter);
			//criteria.remove(searchDelimiters.get(searchDelimiterIndex)[0]);
		}
	}//end test method
	
	/*
	 * This will select the required subset of data from the ExpectedASCDataSet.  
	 * 
	 * The result of this method is that we have a two-dimensional array of Strings
	 * and a two-dimensional array of primitive int values which are populated.
	 * The first index within the String array corresponds to a unique set of data
	 * that will be selected and the int values at the same first index within the
	 * two-dimensional primitive int array.  The int values within that first index
	 * correspond to the String values within that same index in the two-dimensional
	 * String array.  
	 * 
	 * The values within the String array are used to (a) verify that a similar set
	 * of data occurs within the file under test (myExcelReader) and (b) locate the 
	 * corresponding numeric values within the same file under test.  
	 * 
	 * Of note, there will have to be additional code to 
	 * (a) derive the summary values that go into the "expectedSummaryNames" 
	 * and "expectedSummaryValues" and then 
	 * (b) map the summary values to the proper ExpectedASCData Objects
	 * within the ExpectedASCDataSet, and then
	 */
	private void getExpectedAttributes() throws Exception{
		outputMessage("*** method getExpectedAttributes is being called ***", logFileWriter);
			//printExpectedASCDataStatus();
		ExpectedASCDataSet subset = getExpectedSubset();
		if (subset.size() == 0){
			processZeroSet();
			return;
		}
		expectedStringAttributeValues 
				= subset.getExpectedStringDataValues(expectedStringAttributeNames, logFileWriter);
		expectedAmountAttributeValues 
				= subset.getExpectedAmountDataValues(expectedAmountAttributeNames, logFileWriter);
		expectedSummaryValues 
				= subset.getExpectedSummaryValues(expectedSummaryCriteria, logFileWriter);
		if (shareNames.length > 0){
			if (useASCOnly)
				expectedShareValues = subset.getASCAmounts(shareNames, adjustForFTE, excludeMarketShare, 
						consideringOutsideAppt, summarizingASCs, logFileWriter);
			else
				expectedShareValues 
					= subset.getExpectedShareValues(shareNames, expectedShareCriteria, adjustForFTE, excludeMarketShare, 
							consideringOutsideAppt, backoutPYs, summarizingASCs, logFileWriter);
		}//end if - shareNames length is greater than zero
		else expectedShareValues = new int[0][0];
		//printExpectedASCDataStatus();
		for (int i=0; i<expectedStringAttributeValues.length; i++){
			for (int j=0; j<expectedStringAttributeNames.length; j++){
				outputMessage("Expected String Attribute name " + expectedStringAttributeNames[j] 
							+ " has expected value " + expectedStringAttributeValues[i][j], logFileWriter);
			}//end inner for - each attribute in the set of Strings returned
			for (int j=0; j<expectedAmountAttributeNames.length; j++){
				outputMessage("Expected Amount Attribute name " + expectedAmountAttributeNames[j]
						+ " has expected value " + expectedAmountAttributeValues[i][j], logFileWriter);
			}//end inner for - each attribute in the set of int values returned
			for (int j=0; j<expectedSummaryNames.length; j++){
				outputMessage("Expected Summary Attribute name " + expectedSummaryNames[j]
						+ " has expected value " + this.expectedSummaryValues[i][j], logFileWriter);
			}//end inner for - each summary attribute
			for (int j=0; j<shareNames.length; j++){
				outputMessage("Expected Share Attribute name " + shareNames[j]
						+" has expected value " + this.expectedShareValues[i][j], logFileWriter);
			}//end inner for - each share attribute
		}//end outer for - each set of attributes returned
		//printExpectedASCDataStatus();
		myExcelReader.setRowForColumnNames(rowForColumnNames);
		//printExpectedASCDataStatus();
		outputMessage("Excel file is set with row # " + rowForColumnNames + " as the row with column names", logFileWriter);
	}//end getExpectedAttributes method
	
	protected ExpectedASCDataSet getExpectedSubset() throws Exception{
		outputMessage("There are " + criteria.size() + " criteria being applied", logFileWriter);
		outputMessage("Now deriving a subset and printing out the values...", logFileWriter);
		//printExpectedASCDataStatus();
		if ( (! dataPresentCriterionName.isEmpty()) && (! dataPresentCriterionValue.isEmpty()) )
			criteria.put(dataPresentCriterionName, dataPresentCriterionValue);
		ExpectedASCDataSet subset 
				= HandSOnTestSuite.expectedASCDataSet.subset(criteria, logFileWriter);
		//printExpectedASCDataStatus();
		if (subset.size() == 0){
			return subset;
		}
		if (facultyToExclude.length > 0){
			subset 
			= subset.removeFacultyNames(facultyToExclude, logFileWriter);
		}
		if (facultyToInclude.size() > 0){
			subset = addIncludedFaculty(subset);
		}//end if - facultyToInclude is populated
		if (subset.size() == 0){
			return subset;
		}//end if
		if (excludeZeroDOShare){
			subset = subset.removeAllZeroDOShares(logFileWriter);
			outputMessage("***All expected data objects with zero DO Shares have been removed****", logFileWriter);
		}
		if (subset.size() == 0){
			return subset;
		}
		if (excludeDeans){
			subset = subset.removeDeans(logFileWriter);
			outputMessage("***All expected data objects associated with deans have been removed****", logFileWriter);
		}
		if (subset.size() == 0){
			return subset;
		}		
		if (backoutPYs)
			outputMessage("\nThis test requires any Previous Year contributions to the Total Raise to be backed out\n", logFileWriter);
		outputMessage("**** After all filters are applied, there are " + subset.size() 
				+" expected data objects ****", logFileWriter);
		return subset;
	}//end getExpectedSubset method
	
	protected ExpectedASCDataSet addIncludedFaculty(ExpectedASCDataSet set) throws Exception{
		/*
		 * We need to get a subset of the HandSOnTestSuite.expectedASCDataSet
		 * that contains only the names and departments that are included in
		 * the overrides and then iterate through this new subset, adding 
		 * the ExpectedASCData objects to our existing subset.
		 * In order to get the subset mentioned above, we need to take multiple
		 * subsets of the HandSOnTestSuite.expectedASCDataSet, with the 
		 * name and department as inclusion String criteria.  
		 * With every subset that we get, we need to perform the following:
		 * We use a "for" loop, which terminates when the
		 * index equals the new subset's size, and use the 
		 * "ExpectedASCDataSet.getExpectedASCDataFromIndex(int index)" method
		 * to get the ExpectedASCData Object at the index, then add that
		 * ExpectedASCData Object to our subset.
		 */
		Iterator<String> iterator = facultyToInclude.iterator();
		while(iterator.hasNext()){
			String[] split = ((String)iterator.next()).split("-");
			if (split.length == 0){
				split = new String[2];
				split[0] = split[1] = new String();
			}//end if - if facultyToInclude is blank
			outputMessage("Element derived from Override Set: Name: " + split[0]
							+", Department: " + split[1] + "; ", logFileWriter);
			ArrayList<ExpectedASCData> overrideList = ExpectedASCDataSet.getExpectedASCDataForNameAndDept(split[0], split[1]);
			for (int i=0; i< overrideList.size(); i++){
				set.add(overrideList.get(i));
			}//end for loop
			outputMessage("Added " + overrideList.size() + " new ExpectedASCData Objects to the subset", logFileWriter);
		}//end while - going through facultyToInclude
		set.addAllElements(facultyToInclude, logFileWriter);
		return set;
	}
	
	protected void processZeroSet() throws Exception{
		outputMessage("\n**** subset size is zero", logFileWriter);
		expectedStringAttributeValues = new String[0][0];
		expectedAmountAttributeValues = new int[0][0];
		expectedSummaryValues = new int[0][0];
		expectedShareValues = new int[0][0];
	}
	
	/*
	 * This method will take the two-dimensional array of Strings that has been instantiated 
	 * as the expected set of data, and use it to locate the actual data in the file under test,
	 * accessed through the "myExcelReader" Object.  Each array of Strings within the 
	 * expected data set should have sufficient data in it to isolate a unique line in the 
	 * file under test.  
	 * The line in the file under test is located through the SSExcelReader method 
	 * "getSingleFacultyMultipleValues".  This method will call the "getSingleFacultyMultipleValues"
	 * method repeatedly, until all of the arrays within the two-dimensional String array
	 * and the two-dimensional int array are populated.  
	 * The input to this method will be the following:
	 * String sheetName: mySheetName
	 * HashMap<String, String> keysAndValues: a HashMap created on the fly with keys taken from
	 * 		the actualStringAttributeNames array and values taken from one row in the 
	 * 		expectedStringAttributeValues array.  The deal is that we need the names of the actual
	 * 		columns in the file but the values that we got from the expected data.
	 * String[] columnNames - one String array created out of both the actualStringAttributeNames
	 * 		array concatenated with the actualAmountAttributeNames array.  
	 * BufferedWriter logFileWriter - the logFileWriter Object in this Class
	 * 
	 * for every array of Strings in the two-dimensional expectedStringAttributeValues array,
	 * this "getSingleFacultyMultipleValues" will be called with this input, and the 
	 * actualAmountAttributeValues array will be populated with the results, one line at a time.
	 * 
	 */
	private void getActualAttributes() throws Exception{
		outputMessage("*** method getActualAttributes is being called ***", logFileWriter);
		int strAttLen = actualStringAttributeNames.length;
		int amtAttLen = actualAmountAttributeNames.length;
		int sumAttLen = actualSummaryNames.length;
		int sumShareLen = shareNames.length;
		int numberOfRows = expectedStringAttributeValues.length;
		actualStringAttributeValues = new String[numberOfRows][strAttLen];
		actualAmountAttributeValues = new int[numberOfRows][amtAttLen];
		actualSummaryValues = new int[numberOfRows][sumAttLen];
		if (shareNames.length == 0)
			actualShareValues = new int[0][0];
		else
			actualShareValues = new int[numberOfRows][sumShareLen];
		String[] columnNames 
			= (String[]) Array.newInstance(
						actualStringAttributeNames.getClass().getComponentType(), 
						strAttLen + amtAttLen + sumAttLen + sumShareLen);
		System.arraycopy(actualStringAttributeNames, 0, columnNames, 0, strAttLen);
		System.arraycopy(actualAmountAttributeNames, 0, columnNames, strAttLen, amtAttLen);
		System.arraycopy(actualSummaryNames, 0, columnNames, strAttLen + amtAttLen, sumAttLen);
		System.arraycopy(shareNames, 0, columnNames, strAttLen + amtAttLen + sumAttLen, sumShareLen);
		outputMessage ("actual attribute column names are as follows", logFileWriter);
		for (int i=0; i<columnNames.length; i++) outputMessage(columnNames[i], logFileWriter);
		//first, we instantiate one big array to hold every actual value as a String
		String[][] rawActualValues = new String[numberOfRows][strAttLen + amtAttLen];
		
		for (int i=0; i<numberOfRows; i++){
			//instantiate the "criteria" HashMap
			HashMap<String, String> criteria = new HashMap<String, String>();
			outputMessage("Expected name-values to be used as criteria are as follows:", logFileWriter);
			for(int j=0; j<strAttLen; j++){
				outputMessage("Name: " + actualStringAttributeNames[j] 
						+ ", Value: '" + expectedStringAttributeValues[i][j] +"'", logFileWriter);
				criteria.put(actualStringAttributeNames[j], expectedStringAttributeValues[i][j]);
			}//end for loop
			if (searchDelimiterIndex == -1)
				rawActualValues[i] 
					= myExcelReader.getSingleFacultyMultipleValues(mySheetName, criteria, columnNames, logFileWriter);
			else{
				String startString = searchDelimiters.get(searchDelimiterIndex)[0]
							+":  " + searchDelimiters.get(searchDelimiterIndex)[1];
				int startRow = myExcelReader.getCellRowNum(mySheetName, searchDelimiterColumn, startString, true);
				if (startRow == -1) startRow = myExcelReader.getRowForColumnNames();
				outputMessage("Start Row for the search is row # "+startRow, logFileWriter);
				
				String endString = searchDelimiters.get(searchDelimiterIndex)[0];
				int endRow = myExcelReader.getCellRowNumWithSubstringMatch(mySheetName, searchDelimiterColumn, endString, startRow+1);
				if (endRow == -1) endRow = myExcelReader.getRowCount(mySheetName);
				outputMessage("End Row for the search is row # "+endRow, logFileWriter);
				
				rawActualValues[i] 
						= myExcelReader.getSingleFacultyMultipleValues(mySheetName, criteria, 
								columnNames, logFileWriter, startRow, endRow);
			}//end else - searchDelimiters are being used here
			//first, let the user know if there has been a successful retrieval of values
			boolean valuesFound = rawActualValues[i] != null;
			boolean multipleRowsFound = false;
			if (valuesFound) multipleRowsFound = rawActualValues[i][0].equalsIgnoreCase("INVALID");
			outputMessage("Actual values successfully retrieved: " + valuesFound, logFileWriter);
			outputMessage("Multiple rows retrieved: " + multipleRowsFound, logFileWriter);
			if (multipleRowsFound){
				outputMessage("As multiple rows were retrieved, we are adding number criteria", logFileWriter);
				for(int j=0; j<amtAttLen; j++){
					DecimalFormat decimalFormat = new DecimalFormat("#,##0");
					String numberAsString = decimalFormat.format(expectedAmountAttributeValues[i][j]);
					criteria.put(actualAmountAttributeNames[j], numberAsString);
					outputMessage("Adding criteria name " + actualAmountAttributeNames[j]
							+ " with value " + numberAsString, logFileWriter);
				}
				rawActualValues[i] 
						= myExcelReader.getSingleFacultyMultipleValues(mySheetName, criteria, columnNames, logFileWriter);
				//re-evaluate the "multipleRowsFound" attribute
				multipleRowsFound = false;
				valuesFound = rawActualValues[i] != null;
				if (valuesFound) multipleRowsFound = rawActualValues[i][0].equalsIgnoreCase("INVALID");
				outputMessage("Actual values successfully retrieved (2nd attempt): " + valuesFound, logFileWriter);
				outputMessage("Multiple rows retrieved (2nd attempt): " + multipleRowsFound, logFileWriter);
			}//end if - we're think we've got the right row this time
			if (valuesFound && (! multipleRowsFound)){
				for(int j=0; j<columnNames.length; j++){
					outputMessage("Name: " + columnNames[j] 
							+ ", Value: " + rawActualValues[i][j], logFileWriter);
				}//end for - output of actual values
			}//end if - values are found
			else outputMessage("could not retrieve a unique list", logFileWriter);
		}//end for - iterating through the whole list of arrays
		for (int i=0; i<numberOfRows; i++){
			for(int j=0;j<strAttLen; j++){
				outputMessage("Now evaluating " + actualStringAttributeNames[j] + " at index "+ i, logFileWriter);
				try{
					actualStringAttributeValues[i][j]=rawActualValues[i][j];
				}
				catch (NullPointerException e){
					actualStringAttributeValues[i][j] = new String();
				}
			}//end inner for - populating the actual String values
			for (int j=0; j<amtAttLen; j++){
				outputMessage("Now evaluating " + actualAmountAttributeNames[j] + " at index "+ i, logFileWriter);
				String actual = new String();
				try{
					actual = ((Long)NumberFormat.getNumberInstance(java.util.Locale.US).parse(rawActualValues[i][j+strAttLen])).toString();
				}
				catch (NullPointerException e){
					outputMessage ("Null encountered while trying to evaluate "+ actualAmountAttributeNames[j], logFileWriter);
				}
				BigDecimal amount;
				try{
					amount = new BigDecimal(actual);
				}//end try clause - it may or may not be parseable
				catch(NumberFormatException e){
					outputMessage(actual + " is not parseable to a number - evaluating to zero", logFileWriter);
					amount = new BigDecimal(0);
				}//end catch clause - it's not parseable
				actualAmountAttributeValues[i][j] = amount.intValue();
			}//end inner for - populating the actual amount values in each item in the list
			
			for (int j=0; j<sumAttLen; j++){
				outputMessage("Now evaluating " + actualSummaryNames[j] + " at index "+ i, logFileWriter);
				String actual = new String();
				try{
					actual = ((Long)NumberFormat.getNumberInstance(java.util.Locale.US).parse(rawActualValues[i][j+strAttLen+amtAttLen])).toString();
				}
				catch (NullPointerException e){
					outputMessage ("Null encountered while trying to evaluate "+ actualSummaryNames[j], logFileWriter);
				}
				BigDecimal amount;
				try{
					amount = new BigDecimal(actual);
				}//end try clause - it may or may not be parseable
				catch(NumberFormatException e){
					outputMessage(actual + " is not parseable to a number - evaluating to zero", logFileWriter);
					amount = new BigDecimal(0);
				}//end catch clause - it's not parseable
				actualSummaryValues[i][j] = amount.intValue();
			}//end inner for - populating the actual amount values in each item in the list

			for (int j=0; j<sumShareLen; j++){
				outputMessage("Now evaluating " + shareNames[j] + " at index "+ i, logFileWriter);
				String actual = new String();
				try{
					actual = ((Long)NumberFormat.getNumberInstance(java.util.Locale.US).parse(rawActualValues[i][j+strAttLen+amtAttLen+sumAttLen])).toString();
				}
				catch (NullPointerException e){
					outputMessage ("Null encountered while trying to evaluate "+ shareNames[j], logFileWriter);
				}
				BigDecimal amount;
				try{
					amount = new BigDecimal(actual);
				}//end try clause - it may or may not be parseable
				catch(NumberFormatException e){
					outputMessage(actual + " is not parseable to a number - evaluating to zero", logFileWriter);
					amount = new BigDecimal(0);
				}//end catch clause - it's not parseable
				actualShareValues[i][j] = amount.intValue();
			}//end inner for - populating the actual amount values in each item in the list

		}//end outer for - iterating through the whole list - all rows
	}//end getActualAttributes method
	
	public void compareData() throws Exception{
		String message = new String();
		if (expectedStringAttributeValues.length != actualStringAttributeValues.length){
			message = "!!!! WARNING !!!! Different number of String attribute combinations found; "
					+ "number of expected sets of String attributes: " + expectedStringAttributeValues.length
					+", number of actual sets of String attributes: " 
					+ actualStringAttributeValues.length + "; exiting";
		}//end if - the number of rows found in the file is different from what is expected
		if (expectedAmountAttributeValues.length != actualAmountAttributeValues.length){
			message ="!!!! WARNING !!!! Different number of amount attribute combinations found; "
					+ "number of expected sets of amount attributes: " + expectedAmountAttributeValues.length
					+", number of actual sets of amount attributes: " 
					+ actualAmountAttributeValues.length + "; exiting";
		}//end if - the number of rows found in the file is different from what is expected
		if (expectedSummaryValues.length != actualSummaryValues.length){
			message ="!!!! WARNING !!!! Different number of summary attribute combinations found; "
					+ "number of expected sets of summary attributes: " + expectedSummaryValues.length
					+", number of actual sets of summary attributes: " 
					+ actualSummaryValues.length + "; exiting";
		}//end if - the number of rows found in the file is different from what is expected
		if (expectedShareValues.length != actualShareValues.length){
			message ="!!!! WARNING !!!! Different number of share attribute combinations found; "
					+ "number of expected sets of share attributes: " + expectedShareValues.length
					+", number of actual sets of share attributes: " 
					+ actualShareValues.length + "; exiting";
		}//end if - the number of rows found in the file is different from what is expected

		if (! message.isEmpty()){
			outputMessage(message, logFileWriter);
			verificationErrors.append(message);
			return;
		}//end if
		int facultyIndex = getAttributeNameIndex(ExpectedASCData.StringAttributeKeys[ExpectedASCData.FacultyIndex]);
		int deptIndex = getAttributeNameIndex(ExpectedASCData.StringAttributeKeys[ExpectedASCData.DepartmentIndex]);
		message = new String();//reset the error message string - make sure that the mismatches are reported only once
		for (int i=0; i<expectedStringAttributeValues.length; i++){
			for(int j=0; j<expectedStringAttributeValues[i].length;j++){
				if (! expectedStringAttributeValues[i][j].equalsIgnoreCase(actualStringAttributeValues[i][j])){
					if (expectedStringAttributeNames[j].equalsIgnoreCase(ExpectedASCData.StringAttributeKeys[ExpectedASCData.DescriptionIndex])
							|| expectedStringAttributeNames[j].equalsIgnoreCase(ExpectedASCData.StringAttributeKeys[ExpectedASCData.NotesIndex])
							|| expectedStringAttributeNames[j].equalsIgnoreCase(ExpectedASCData.StringAttributeKeys[ExpectedASCData.DepartmentIndex])){
						if(! actualStringAttributeValues[i][j].contains(expectedStringAttributeValues[i][j])){
							message = message.concat("MISMATCH! Expected " + expectedStringAttributeNames[j]
									+ " value: " + expectedStringAttributeValues[i][j]
									+", actual " + actualStringAttributeNames[j]
									+ " value: " + actualStringAttributeValues[i][j] 
									+ ", for Faculty " + expectedStringAttributeValues[i][facultyIndex]
									+ " at index " + i);
							if (deptIndex != -1) 
								message = message.concat(", for Department " + expectedStringAttributeValues[i][deptIndex]);
							message = message.concat(";\n");
						}//it's not a substring
					}//end if - make sure that the expected String is a substring
					else{
						message = message.concat("MISMATCH! Expected " + expectedStringAttributeNames[j]
							+ " value: " + expectedStringAttributeValues[i][j]
							+", actual " + actualStringAttributeNames[j]
									+ " value: " + actualStringAttributeValues[i][j] 
									+ ", for Faculty " + expectedStringAttributeValues[i][facultyIndex]
										+ " at index " + i);
						if (deptIndex != -1) 
							message = message.concat(", for Department " + expectedStringAttributeValues[i][deptIndex]);
						message = message.concat(";\n");
					}//end else - it's a String-to-String comparison
				}//end if - checking to see if there's a mismatch
			}//end inner for - iterating through an individual set of values
		}//end outer for - iterating through the set of sets of values
		if (message.isEmpty()) outputMessage("All String Attributes match", logFileWriter);
		else{
			outputMessage(message, logFileWriter);
			verificationErrors.append(message);
		}
		message = new String();//reset the error message string - make sure that the mismatches are reported only once

		for (int i=0; i<expectedAmountAttributeValues.length; i++){
			for(int j=0; j<expectedAmountAttributeValues[i].length; j++){
				if ( (expectedAmountAttributeValues[i][j] != actualAmountAttributeValues[i][j])
						//if we're not adjusting for FTE, allow a 1% inaccuracy
				&& (//! adjustForFTE && 
							((expectedAmountAttributeValues[i][j] - 1) < ((actualAmountAttributeValues[i][j]/100)*99)
									|| ((expectedAmountAttributeValues[i][j] + 1) > ((actualAmountAttributeValues[i][j]/100)*101))) )
				){
					message = message.concat("MISMATCH! Expected " + expectedAmountAttributeNames[j]
							+ " value: " + expectedAmountAttributeValues[i][j]
							+", actual "  + actualAmountAttributeNames[j]
							+ " value: " + actualAmountAttributeValues[i][j] 
							+ ", for Faculty " + expectedStringAttributeValues[i][facultyIndex]
							+ " at index " + i);
					if (deptIndex != -1) 
						message = message.concat(", for Department " + expectedStringAttributeValues[i][deptIndex]);
					message = message.concat(";\n");
				}//end if - checking to see if there's a mismatch
			}//end inner for - iterating through an individual set of values
		}//end outer for - iterating through the set of sets of values
		if (message.isEmpty()) outputMessage("All Amount Attributes match", logFileWriter);
		else{
			outputMessage(message, logFileWriter);
			verificationErrors.append(message);
		}
		
		message = new String();//reset the error message string - make sure that the mismatches are reported only once

		for (int i=0; i<expectedSummaryValues.length; i++){
			for(int j=0; j<expectedSummaryValues[i].length; j++){
				/*
				 * First, check to see if their absolute values are equal - do nothing if they are
					 */
				if (java.lang.Math.abs(expectedSummaryValues[i][j]) == java.lang.Math.abs(actualSummaryValues[i][j]));
				/*
				 * If they're not equal, see if there's only a difference of one in their absolute values
				 */
				else if (java.lang.Math.abs(expectedSummaryValues[i][j] - 1) < java.lang.Math.abs((actualSummaryValues[i][j]/100)*99)
						|| (java.lang.Math.abs(expectedSummaryValues[i][j] + 1) > java.lang.Math.abs((actualSummaryValues[i][j]/100)*101))) {

				//(expectedSummaryValues[i][j] != actualSummaryValues[i][j]){
					message = message.concat("MISMATCH! Expected " + expectedSummaryNames[j]
							+ " value: " + expectedSummaryValues[i][j]
							+", Actual "  + actualSummaryNames[j]
							+ " value: " + actualSummaryValues[i][j] 
							+ ", for Faculty " + expectedStringAttributeValues[i][facultyIndex]
							+ " at index " + i);
					if (deptIndex != -1) 
						message = message.concat(", for Department " + expectedStringAttributeValues[i][deptIndex]);
					message = message.concat(";\n");
				}//end if - checking to see if there's a mismatch
			}//end inner for - iterating through an individual set of values
		}//end outer for - iterating through the set of sets of values
		if (message.isEmpty()) outputMessage("All Summary Attributes match", logFileWriter);
		else{
			outputMessage(message, logFileWriter);
			verificationErrors.append(message);
		}

		message = new String();//reset the error message string - make sure that the mismatches are reported only once

		for (int i=0; i<expectedShareValues.length; i++){
			for(int j=0; j<expectedShareValues[i].length; j++){
				if (expectedShareValues[i][j] != actualShareValues[i][j]){
					message = message.concat("MISMATCH! Expected " + shareNames[j]
							+ " value: " + expectedShareValues[i][j]
							+", actual "  + shareNames[j]
							+ " value: " + actualShareValues[i][j] 
							+ ", for Faculty " + expectedStringAttributeValues[i][facultyIndex]
							+ " at index " + i);
					if (deptIndex != -1) 
						message = message.concat(", for Department " + expectedStringAttributeValues[i][deptIndex]);
					message = message.concat(";\n");
				}//end if - checking to see if there's a mismatch
			}//end inner for - iterating through an individual set of values
		}//end outer for - iterating through the set of sets of values
		if (message.isEmpty()) outputMessage("All DO Share and Dept Share Attributes match", logFileWriter);
		else{
			outputMessage(message, logFileWriter);
			verificationErrors.append(message);
		}
	}//end compareData method
	
	protected int getAttributeNameIndex(String attributeName){
		for (int i=0; i<expectedStringAttributeNames.length; i++){
			if (expectedStringAttributeNames[i].equalsIgnoreCase(attributeName))
				return i;
		}//end for
		return -1;
	}// end method 
	
	protected void outputMessage(String message, BufferedWriter logFileWriter) throws Exception{
		System.out.println(message);
		TestFileController.writeToLogFile(message, logFileWriter);
	}
	
	protected void printExpectedASCDataStatus() throws Exception{
		outputMessage("At this point in test " + testName+", the number of ExpectedASCData Objects is " + HandSOnTestSuite.expectedASCDataSet.size(), logFileWriter);
		outputMessage("At this point in test " + testName+", the number of ASCFacultyValues Objects is " + HandSOnTestSuite.expectedASCDataSet.getNumberOfASCFacultyValues(), logFileWriter);
	}
	
	

}
