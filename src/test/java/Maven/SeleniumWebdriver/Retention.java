package Maven.SeleniumWebdriver;
/*
 * This Class holds all historical and current data on a Retention.  It is also the Class which contains 
 * methods for checking Retention data, especially when doing comparisons between historical and current data.
 * This Class will do most if not all of the interaction with the RetentionUIController.
 */
public class Retention {
	

	private String retentionID;
	private String facultyName;
	private String testCaseID;
	private boolean isNonZero;
	private String languageOfCommitment;
	private String effectiveDate;
	private String effectiveYear;
	private float originalSalaryAmt;
	private float updatedSalaryAmt;
	private float originalIncreaseAmt;
	private float updatedIncreaseAmt;
	private float originalIncreasePct;
	private float updatedIncreasePct;
	private float increasePctAbovePool;
	

	public Retention(String testCaseID) throws Exception {
		this.testCaseID = testCaseID;
		System.out.println(getInitialRetentionData());
	}//end constructor
	
	public String getInitialRetentionData() throws Exception{
		String initialDataString = "Retention for Test Case ID "+testCaseID+" has been instantiated.  ";
		this.isNonZero = TestDataRecorder.getTestCaseData(testCaseID, "Zero or Nonzero Raises").equalsIgnoreCase("Nonzero");
		initialDataString = initialDataString.concat(";\nRetention is non-zero: "+isNonZero);
		this.languageOfCommitment = TestDataRecorder.getTestCaseData(testCaseID, "Language of Commitment");
		initialDataString = initialDataString.concat(";\nLanguage of Commitment is "+languageOfCommitment);
		this.effectiveDate = TestDataRecorder.getTestCaseData(testCaseID, "Retention Effective Date");
		initialDataString = initialDataString.concat(";\nRetention Effective Date is "+effectiveDate);
		return initialDataString;
	}//end getInitialFacultyData method

	public String getRetentionID() throws Exception{
		retentionID = TestDataRecorder.getTestCaseData(testCaseID, "Retention ID");
		return retentionID;
	}//end getRetentionID method
}
