/**
 * 
 */
package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;
import java.text.NumberFormat;
import java.text.ParseException;

/**
 * @author christ13
 * This is used to store the different A&P types and the different dollar amounts and faculty associated with them.
 * It will be read into a two-dimensional String array – ReferenceValues.  Given the reality that it will entail 
 * storing the data, returning specific values in specific formats, and that there will be constants used, 
 * we want to encapsulate ReferenceValues in its own Java Class.  
 * 
 * That keeps everything separate and contained, increases cohesion and reduces coupling
 *
 */
public class ReferenceValues {
	
	//these are the constants that apply to the first dimension of the two-dimensional ReferenceValues String array (similar to columns)
	public static final int Label = 0;
	public static final int Amount = 1;
	public static final int Faculty = 2;
	public static final int firstDimensionSize = 3;
	
	//these are the constants that apply to the second dimension of the two-dimensional ReferenceValues String array (similar to rows)
	public static final int Reappointment = 0;
	public static final int PromotionToNonTenure = 1;
	public static final int PromotionToContinuingTerm = 2;
	public static final int PromotionToTenure = 3;
	public static final int PromotionToFull = 4;
	public static final int RemovalOfSubjectToPhD = 5;
	public static final int secondDimensionSize = 6;
	
	//this must be inaccessible to other classes - accessed only by public methods in this class
	private String[][] ReferenceValues;

	/**
	 * The constructor takes the name of a worksheet as an input parameter.
	 * We assume the name of the file and ExcelReader Object.
	 */
	public ReferenceValues(String sheetName) {
		ReferenceValues = new String[firstDimensionSize][secondDimensionSize];
		//System.out.println("Referenced worksheet is '" +sheetName +"' with " + TestFileController.fileIn.getRowCount(sheetName) +" rows");
		for (int ValueType=0; ValueType < firstDimensionSize; ValueType++){
			for (int APTypeIndex=0; APTypeIndex < secondDimensionSize; APTypeIndex++){
				ReferenceValues[ValueType][APTypeIndex] = TestFileController.fileIn.getCellData(sheetName, ValueType, APTypeIndex+2);
				System.out.println("Reference Value at [" + APTypeIndex +"][" + ValueType + "] is '" + ReferenceValues[ValueType][APTypeIndex] + "'");
			}//end inner for loop - types of AP Reference values - rather like rows
		}//end outer for loop - types of data for each AP Reference value - rather like columns
	}//end constructor
	
	//when the values in the ReferenceValues object need to be adjusted, this is the method that does it
	public void resetValues(String sheetName){
		ReferenceValues = new String[firstDimensionSize][secondDimensionSize];
		//System.out.println("Referenced worksheet is '" +sheetName +"' with " + TestFileController.fileIn.getRowCount(sheetName) +" rows");
		for (int ValueType=0; ValueType < firstDimensionSize; ValueType++){
			for (int APTypeIndex=0; APTypeIndex < secondDimensionSize; APTypeIndex++){
				ReferenceValues[ValueType][APTypeIndex] = TestFileController.fileIn.getCellData(sheetName, ValueType, APTypeIndex+2);
				//System.out.println("Reference Value at [" + APType +"][" + ValueType + "] is " + TestFileController.fileIn.getCellData(sheetName, ValueType, APType+2));
				//System.out.println("Reference Value at [" + APTypeIndex +"][" + ValueType + "] is " + ReferenceValues[ValueType][APTypeIndex]);
			}//end inner for loop - types of AP Reference values - rather like rows
		}//end outer for loop - types of data for each AP Reference value - rather like columns
	}//end method resetValues
		
		public String getStringAmountForAPType(String APType){
			for (int APTypeIndex=0; APTypeIndex < secondDimensionSize; APTypeIndex++){
				if (APType.equalsIgnoreCase(ReferenceValues[Label][APTypeIndex])){
					//System.out.println("Match found: '" + ReferenceValues[Label][APTypeIndex] + "'");
					return ReferenceValues[Amount][APTypeIndex];
				}//end if - when a match is found
			}//end for loop - going through all of the types of AP Reference Values
			//System.out.println(" Match **NOT** found for AP Type: '" + APType +"'");
			return "";
		}//end getStringAmountForAPType method
		
		public Float getFloatAmountForAPType(String APType) throws ParseException{
			return Float.valueOf((Long) NumberFormat.getNumberInstance(java.util.Locale.US).parse(getStringAmountForAPType(APType)));
		}//end getFloatAmountForAPType method
		
		public String[] getFacultyForAPType(String APType){
			for (int APTypeIndex=0; APTypeIndex < secondDimensionSize; APTypeIndex++){
				if (APType.equalsIgnoreCase(ReferenceValues[Label][APTypeIndex])){
					//System.out.println("Match found: " + ReferenceValues[Label][APTypeIndex]);
					return ReferenceValues[Faculty][APTypeIndex].split("; ");
				}//end if - when a match is found
			}//end for loop - going through all of the types of AP Reference Values
			//System.out.println("Match **NOT** found for AP Type: '" + APType +"'");
			return new String[1];
		}//end getFacultyForAPType method
		
		//this returns all of the labels in the ReferenceValues Object
		public String[] getLabels(){
			return this.ReferenceValues[Label];
		}//end getLabels method
		

}
