package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.File;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class SalaryBudgetingReportsRunner {

	private WebDriver driver;
	//private String baseUrl;
	private BufferedWriter logFileWriter;
	private static TestFileController fileController;
	// !!!! COPY ALL OF THESE variables to the appropriate Classes
	private static String inputFileQueriesTab = "Queries Tab";
	
	


	@Before
	public void setUp() throws Exception {
		//driver = new ChromeDriver();
	    System.out.println("Welcome to Maven World");
		System.setProperty("webdriver.gecko.driver","C:\\Users\\christ13\\webdrivers\\geckodriver-v0.24.0-win64\\geckodriver.exe");
	    driver = new FirefoxDriver();       
	    driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	    driver.navigate().to("http://www.google.com");
		//baseUrl = "https://handsonweb-"+HandSOnTestSuite.machineName+".stanford.edu/"+HandSOnTestSuite.envName;
		fileController =  new TestFileController();
	}

	@After
	public void tearDown() throws Exception {
	    driver.quit();
	    logFileWriter.close();
	}

	@Test
	public void test() throws Exception {
		String referenceWhichValues = ReportRunner.ORIGINAL;
		String logDirectoryName = "";
		
		
		logDirectoryName = TestFileController.LogDirectoryName +"/" + referenceWhichValues;
		String testName = "HandSOn TEST - Set values, and run reports";
		String logFileName = logDirectoryName +"/" + testName +".txt";
		System.out.println("Log Directory Name: " + logDirectoryName);
		if (TestFileController.setLogDirectoryName(logDirectoryName))
			this.logFileWriter = TestFileController.getLogFileWriter(logFileName);
		else fail ("Could not create log directory: " + logDirectoryName);
		driver = UIController.initializeDriver(logDirectoryName);
		UIController.openSalarySetting(logFileWriter);
		runFinanceReports(ReportRunner.ORIGINAL, logFileWriter);
	}//end test method
	
	public static void runFinanceReports (String whichValues, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("**** method runFinanceReports is being run with "+whichValues+" values ****");
		logFileWriter.newLine();
		
		//switch back to the original window and go to the Queries tab
		Thread.sleep(5000);
		UIController.switchWindowByURL(UIController.startPage, logFileWriter);
		Thread.sleep(5000);
		UIController.driver.findElement(By.linkText("Queries")).click();
		/*
		runOneFinanceReportBySelection(UIController.deansReportURL, whichValues, By.name("request.dean"), 
				"Debra Satz", "Dean's Report", logFileWriter);
		*/
		runSalaryBudgetingReports(whichValues, logFileWriter);
		//now,run the Finance Reports
		runOneFinanceReportByInputFile(UIController.queriesPageURL, "Dean's Report", whichValues, logFileWriter);
		runOneFinanceReportByInputFile(UIController.queriesPageURL, "Department Commitment Reports", whichValues, logFileWriter);
		runOneFinanceReportByInputFile(UIController.departmentCommitmentReportsURL, "Summer Ninths Report", whichValues, logFileWriter);
		runOneFinanceReportByInputFile(UIController.departmentCommitmentReportsURL, "Transfer Status Report", whichValues, logFileWriter);
		runOneFinanceReportByInputFile(UIController.departmentCommitmentReportsURL, "Recipient Report", whichValues, logFileWriter);
		runOneFinanceReportByInputFile(UIController.departmentCommitmentReportsURL, "Award Report", whichValues, logFileWriter);
		runOneFinanceReportByInputFile(UIController.departmentCommitmentReportsURL, "Category Report", whichValues, logFileWriter);
		runOneFinanceReportByInputFile(UIController.departmentCommitmentReportsURL, "Sub-Category Report", whichValues, logFileWriter);
		runOneFinanceReportByInputFile(UIController.departmentCommitmentReportsURL, "Data Download", whichValues, logFileWriter);
		runOneFinanceReportByInputFile(UIController.queriesPageURL, "Department Quarterly Commitments Report", whichValues, logFileWriter);
		runOneFinanceReportByInputFile(UIController.queriesPageURL, "Endowed Chair Commitments Report", whichValues, logFileWriter);
		runOneFinanceReportByInputFile(UIController.queriesPageURL, "Faculty Salary Explorer", whichValues, logFileWriter);
		
		
	}//end runFinanceReports method

	public static void runSalaryBudgetingReports(String whichValues, BufferedWriter logFileWriter)throws Exception{		
		//first, bring up the Salary Budgeting Reports screen
		Thread.sleep(5000);
		SalarySettingManager.waitAndClick(By.linkText("Salary Budgeting Reports"));
		//now, switch to the Salary Budgeting Reports screen
		
		//now, start running the reports, one at a time
		runOneSalaryBudgetingReport(whichValues, TestFileController.SalaryBudgetingPopulationVerificationReportFileName, 
				UIController.SalaryBudgetingPopulationVerificationReportURL, TestFileController.SalaryBudgetingPopulationVerificationReportName, logFileWriter);
		runOneSalaryBudgetingReport(whichValues, TestFileController.SalaryBudgetingReportFileName, 
				UIController.SalaryBudgetingReportURL, TestFileController.SalaryBudgetingReportName, logFileWriter);
		runOneSalaryBudgetingReport(whichValues, TestFileController.SalaryBudgetingComparisonReportFileName, 
				UIController.SalaryBudgetingComparisonReportURL, TestFileController.SalaryBudgetingComparisonReportName, logFileWriter);
		runOneSalaryBudgetingReport(whichValues, TestFileController.SalaryRateExportForHyperionUploadFileName, 
				UIController.SalaryRateExportForHyperionUploadURL, TestFileController.SalaryRateExportForHyperionUploadName, logFileWriter);
		runOneSalaryBudgetingReport(whichValues, TestFileController.SalaryPercentageExportForHyperionUploadFileName, 
				UIController.SalaryPercentageExportForHyperionUploadURL, TestFileController.SalaryPercentageExportForHyperionUploadName, logFileWriter);
		runOneSalaryBudgetingReport(whichValues, TestFileController.SalaryReserveDetailReportFileName, 
				UIController.SalaryReserveDetailReportURL, TestFileController.SalaryReserveDetailReportName, logFileWriter);
		runOneSalaryBudgetingReport(whichValues, TestFileController.SalaryReserveRollUpExportForHyperionUploadFileName, 
				UIController.SalaryReserveRollUpExportForHyperionUploadURL, TestFileController.SalaryReserveRollUpExportForHyperionUploadName, logFileWriter);
		
		Thread.sleep(5000);
		UIController.switchWindowByURL("salaryBudgetingReports.jsp", logFileWriter);
		UIController.driver.close();
		
	}//end runSalaryBudgetingReports method
	
	protected static void runOneSalaryBudgetingReport(String whichValues, String fileName, String destinationURL, String linkText, BufferedWriter logFileWriter) throws Exception{
		//DELETE THIS LINE when this gets put into the ReportRunner code
		SalarySettingManager salarySettingManager = new SalarySettingManager();
		
		String salaryBudgetingReportsURL = "salaryBudgetingReports.jsp";
		String whichDepartment = "Chemistry";
		String whichDepartmentLabel = "salaryBudgetDTO.deptCd";
		String salaryBudgetDTOFiscalYearTextfieldLabel = "salaryBudgetDTOFiscalYear";
		String generateButtonLabel = "generate";
		String effectiveDateTextFieldLabel = "effecDate";
		String effectiveYearString = "20"+HandSOnTestSuite.FY.split("-")[1];
		String effectiveStartDateString = "09/01/20"+HandSOnTestSuite.FY.split("-")[0];
		String effectiveEndDateString = "08/31/"+effectiveYearString;
		String startDateTextFieldLabel = "dateStart";
		String endDateTextFieldLabel = "dateEnd";
		
		
		logFileWriter.newLine();
		TestFileController.writeToLogFile("**** Now attempting to run the "+linkText+" ****\n", logFileWriter);
		logFileWriter.newLine();
		
		//first. switch to the Budget Salary Setting Window
		Thread.sleep(5000);
		UIController.switchWindowByURL(salaryBudgetingReportsURL, logFileWriter);
		
		//now, click on the appropriate link
		SalarySettingManager.waitAndClick(By.linkText(linkText), 5);
		
		//now, switch to the appropriate window
		UIController.switchWindowByURL(destinationURL, logFileWriter);
		Thread.sleep(2000);
		//salaryBudgetDTOFiscalYearTextfieldLabel corresponds to the Fiscal Year text field.  It present with all Reports.
		
		//UNCOMMENT the following line when this is ported to ReportRunner
		//HandSOnTestSuite.salarySettingManager.writeStringToTextField (By.id(salaryBudgetDTOFiscalYearTextfieldLabel), effectiveYearString, logFileWriter);
		//DELETE the following line when this is ported to ReportRunner
		salarySettingManager.writeStringToTextField (By.id(salaryBudgetDTOFiscalYearTextfieldLabel), effectiveYearString, logFileWriter);
		Thread.sleep(3000);
		if (linkText.equalsIgnoreCase(TestFileController.SalaryBudgetingReportName)) {
			salarySettingManager.writeStringToTextField (By.id(effectiveDateTextFieldLabel), effectiveStartDateString, logFileWriter);
			Thread.sleep(3000);
			UIController.selectElementInMenu(By.name(whichDepartmentLabel), whichDepartment);
			Thread.sleep(3000);
		}//end if - if it's the Salary Budgeting Report, enter the Effective Date
		else if (linkText.equalsIgnoreCase(TestFileController.SalaryBudgetingComparisonReportName)) {
			salarySettingManager.writeStringToTextField(By.id(startDateTextFieldLabel), effectiveStartDateString, logFileWriter);
			Thread.sleep(3000);
			salarySettingManager.writeStringToTextField(By.id(endDateTextFieldLabel), effectiveEndDateString, logFileWriter);
			Thread.sleep(3000);
			UIController.selectElementInMenu(By.name(whichDepartmentLabel), whichDepartment);
			Thread.sleep(3000);
		}
		else if ( linkText.equalsIgnoreCase(TestFileController.SalaryRateExportForHyperionUploadName) 
				|| linkText.equalsIgnoreCase(TestFileController.SalaryPercentageExportForHyperionUploadName)
				|| linkText.equalsIgnoreCase(TestFileController.SalaryReserveDetailReportName)
				|| linkText.equalsIgnoreCase(TestFileController.SalaryReserveRollUpExportForHyperionUploadName)
				) {
			salarySettingManager.writeStringToTextField(By.id(startDateTextFieldLabel), effectiveStartDateString, logFileWriter);
			Thread.sleep(3000);			
		}
		
		//this is the Generate button.  It is present with all Reports.
		SalarySettingManager.waitAndClick(By.id(generateButtonLabel));
		
		SalarySettingManager.getScreenShot(fileName, whichValues, logFileWriter);		
		TestFileController.copyFile(fileName, logFileWriter, TestFileController.LogDirectoryName+"/"+whichValues);
		
		if (isFileEmpty(fileName, whichValues, logFileWriter)) {
			fail("ERROR: "+fileName+" is unexpectedly at zero length\n");
		}
		else {
			logFileWriter.write(fileName+" is not empty, as expected");
			logFileWriter.newLine();
		}//end else - file is normal-sized, as expected
	}//end runOneSalaryBudgetingReport method
	
	public static boolean isFileEmpty(String fileName, String whichValues, BufferedWriter logFileWriter) throws Exception{
		File reportFile = new File(TestFileController.LogDirectoryName+"/"+whichValues+"/"+fileName);
		double bytes = reportFile.length();
		if (bytes == 0) {
			return true;
		}//end if - the file is empty
		else {
			double kilobytes = bytes / 1024;
			double megabytes = kilobytes / 1024;
			double gigabytes = megabytes / 1024;
			
			logFileWriter.newLine();
			TestFileController.writeToLogFile("**** Length of "+fileName+" is "+bytes+" bytes, or "+kilobytes+" kilobytes, or "
			+megabytes+" megabytes, or "+gigabytes+" gigabytes.****\n", logFileWriter);
			logFileWriter.newLine();
		}//end else - file is normal-sized, as expected
		return false;
	}//end isFileEmpty method
	
	public static void runOneFinanceReportBySelection(String destinationURL, String whichValues, By by, String inputValue, 
			String linkText, BufferedWriter logFileWriter) throws Exception{
		//DELETE THIS LINE when this gets put into the ReportRunner code
		SalarySettingManager salarySettingManager = new SalarySettingManager();
		
		//switch back to the Queries tab
		UIController.switchWindowByURL(UIController.queriesPageURL, logFileWriter);
		Thread.sleep(5000);
		//now, click on the applicable link to bring up the appropriate report-specific screen
		SalarySettingManager.waitAndClick(By.linkText(linkText), 5);
		
		//first. switch to the Budget Salary Setting Window
		Thread.sleep(5000);
		if (destinationURL.length() > 0)
			UIController.switchWindowByURL(destinationURL, logFileWriter);
		else
			TestFileController.writeToLogFile("There is no need to switch to another window for "+linkText, logFileWriter);
		Thread.sleep(3000);

		UIController.selectElementInMenu(by, inputValue);
		Thread.sleep(3000);

		UIController.waitAndClick(By.id("buttonSubmit"));
		
	}//end runOneFinanceReportBySelection method
	
	public static void runOneFinanceReportByInputFile(String initialLink, String linkText, String whichValues, BufferedWriter logFileWriter) throws Exception{
		int submitButtonIndex = 3;
		int downloadedFileIndex = 2;
		int destinationURLIndex = 1;
		
		
		logFileWriter.write("*** method runOneFinanceReportByInputFile called with link text '"+linkText+"' ****");
		logFileWriter.newLine();
		
		//DELETE THESE LINES when this gets put into the ReportRunner code
		SalarySettingManager salarySettingManager = new SalarySettingManager();
		SearchAuthorizationUIController searchAuthorizationUI = new SearchAuthorizationUIController();
		
		//switch back to the Queries tab
		UIController.switchWindowByURL(initialLink, logFileWriter);
		Thread.sleep(5000);
		//now, click on the applicable link to bring up the appropriate report-specific screen
		SalarySettingManager.waitAndClick(By.linkText(linkText), 5);
		//get all of the column names
		String[] columnNames = TestFileController.fileIn.getColumnNames(inputFileQueriesTab);
		Thread.sleep(5000);
		
		int row = TestFileController.fileIn.getCellRowNum(inputFileQueriesTab, 0, linkText);
		//Read all of the values, one at a time, in the columns in that row, and adjust the correct widget appropriately
		for (int col=0; col<columnNames.length; col++) {
			String cellValue = TestFileController.fileIn.getCellData(inputFileQueriesTab, col, row);

			if (cellValue.length() > 0) {
				//in order to use the widget, we need both the locator and the value - the value is always in the column to the right of the one holding the locator
				TestFileController.writeToLogFile("The value of column with name '"+columnNames[col]+"' is '"+cellValue+"';", logFileWriter);
				if (col == destinationURLIndex) {
					UIController.switchWindowByURL(cellValue, logFileWriter);
					Thread.sleep(5000);
				}
				else if (columnNames[col].contains("Menu")) {
					By by = searchAuthorizationUI.getByForLocator(cellValue, logFileWriter);
					String menuItem = TestFileController.fileIn.getCellData(inputFileQueriesTab, col+1, row);
					System.out.println("Menu Item to be selected is '"+menuItem+"'");
					TestFileController.writeToLogFile("Menu Item to be selected is '"+menuItem+"'", logFileWriter);
					Thread.sleep(5000);
					SalarySettingManager.selectElementInMenu(by, menuItem);
				}//end if
				else if (columnNames[col].contains("Field") && (! columnNames[col].contains("Value"))) {
					By by = searchAuthorizationUI.getByForLocator(cellValue, logFileWriter);
					String text = TestFileController.fileIn.getCellData(inputFileQueriesTab, col+1, row);
					TestFileController.writeToLogFile("Menu Item to be written to text field is '"+text+"'", logFileWriter);
					salarySettingManager.writeStringToTextField(by, text, logFileWriter);
				}//end else if
			}//end if
		}//end for
		
		//click on the button to generate the report
		String rawLocator = TestFileController.fileIn.getCellData(inputFileQueriesTab, submitButtonIndex, row);
		if (rawLocator.length() > 0) {
			By by = salarySettingManager.getByForLocator(rawLocator, logFileWriter);
			if (! salarySettingManager.waitAndClick(by)) {
				fail("Could not click on the button to submit the form at URL '"+columnNames[downloadedFileIndex]+"';\n");
			}
		}//end if - do this if and only if there is an actual submit button - there may not be
		String fileName = TestFileController.fileIn.getCellData(inputFileQueriesTab, downloadedFileIndex, row);
				
		SalarySettingManager.getScreenShot(fileName, whichValues, logFileWriter);		
		TestFileController.copyFile(fileName, logFileWriter, TestFileController.LogDirectoryName+"/"+whichValues);
		
		if (isFileEmpty(fileName, whichValues, logFileWriter)) {
			fail("ERROR: "+fileName+" is unexpectedly at zero length\n");
		}
		else {
			logFileWriter.write(fileName+" is not empty, as expected");
			logFileWriter.newLine();
		}//end else - file is normal-sized, as expected
		
	}//end runOneFinanceReportByInputFile method
	

}//end Class
