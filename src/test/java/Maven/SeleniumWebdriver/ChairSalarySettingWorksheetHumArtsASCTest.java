package Maven.SeleniumWebdriver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ChairSalarySettingWorksheetHumArtsASCTest extends
		ChairSalarySettingWorksheetASCTest {

	@Before
	public void setUp() throws Exception {
		clusterName = "Humanities and Arts Cluster";
		configureOutput();
		super.setUp();
	}
	
	private void configureOutput() throws Exception{
		myExcelReader = TestFileController.ChairSalarySettingWorksheetHumanitiesAndArts;
		mySheetName = TestFileController.ChairSalarySettingWorksheetWorksheetName;
		rowForColumnNames = TestFileController.ChairSalarySettingWorksheetHumanitiesAndArtsHeaderRow;
		verificationErrors = new StringBuffer();
		testName = "ChairSalarySettingWorksheetHumArtsASCTest";
		testEnv = "Test";
		testCaseNumber = "035";
		testDescription = "ASC Test of Chair Salary Setting Worksheet for Humanities and Arts";
		logFileName = "Chair Salary Setting HumArts ASC Test.txt";
		logFileWriter = TestFileController.getLogFileWriter(logDirectoryName  +"/" + logFileName);
	}//end configureOutput method


	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void test() throws Exception{
		super.test();
	}

}
