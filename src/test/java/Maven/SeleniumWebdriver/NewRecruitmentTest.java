package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.text.PDFTextStripper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;

/*
 * The goal of this test Class is to instantiate the new Recruitments from existing SA's
 * and verify that they have the data as expected.  
*/
public class NewRecruitmentTest {
	
	protected static String testName;
	protected static String testCaseNumber;
	protected static String testDescription;
	protected static String testEnv;
	protected static StringBuffer verificationErrors;
	protected BufferedWriter logFileWriter;
	protected static String[] IDStrings;
	protected static String uploadedFileName = "School Salary Setting Worksheet.xlsx";
	protected static String attachmentText = "Attachment";
	protected static String noteText = "Note";
	protected static String dataSourceDate = "09/01/2016";
	protected ArrayList<CommitmentTransferLine> preSubmissionTransferLines;
	protected ArrayList<CommitmentTransferLine> postSubmissionTransferLines;

	@Before
	public void setUp() throws Exception {
		testName = "NewRecruitmentTest";
		testEnv = "Test";
		testCaseNumber = "065";
		testDescription = "Test of New Recruitments";
		verificationErrors = new StringBuffer();
	}//end setUp method

	@After
	public void tearDown() throws Exception {
		TestFileController.writeTestResults(testEnv, testCaseNumber, testName, testDescription, verificationErrors);
		logFileWriter.newLine();
		logFileWriter.close();
	}//end tearDown method

	@Test
	public void test() throws Exception{
		String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		logFileWriter = TestFileController.getLogFileWriter(logFileName);
		logFileWriter.write("New Recruitments will be tested");
		logFileWriter.newLine();

		
		int maxIndex = 6;//initially, we set it to six
		IDStrings = selectInitialRecruitments(verificationErrors, maxIndex, logFileWriter);
		if (IDStrings.length < maxIndex) {//if there aren't six Recruitments there, we limit the iterations
			maxIndex = IDStrings.length;
			logFileWriter.write("NOTE: the number of Recruitments tested will be limited to "+maxIndex);
			logFileWriter.newLine();
		}//end if - limit the number of iterations if there just aren't that many initial Recruitments
		
		for(int i=0; i<maxIndex; i++){
			verifyInitialSearchAuth(IDStrings[i], logFileWriter);
			adjustSearchAuthIncidentalData(IDStrings[i], logFileWriter);
			addNewRecruitment(IDStrings[i], true, logFileWriter);
			enterBasicRecruitmentInfo(IDStrings[i], true, logFileWriter);
			verifyBasicRecruitmentInfo(IDStrings[i], logFileWriter);
			addNewRecruitment(IDStrings[i], false, logFileWriter);
			switchPrimaryRecruitmentScenario(IDStrings[i], logFileWriter);
			enterBasicRecruitmentInfo(IDStrings[i], false, logFileWriter);
			verifyBasicRecruitmentInfo(IDStrings[i], logFileWriter);
			verifySearchAuthorizationData(IDStrings[i], logFileWriter);
			performChangedDataScenario(IDStrings[i], logFileWriter);
			testOfferStatus(IDStrings[i], logFileWriter);
			testUpdatedSearchAuthStatus(IDStrings[i], logFileWriter);
			testBackupRecruitmentStatus(IDStrings[i], logFileWriter);
			String recruitmentID = testFacultyCreationFromRecruitment(IDStrings[i], logFileWriter);
			testFacultyDemographics(IDStrings[i], logFileWriter);
			testFacultySalary(IDStrings[i], logFileWriter);
			testFacultyAttachments(IDStrings[i], logFileWriter);
			String PTA = testDefaultPTAs(IDStrings[i], logFileWriter);
			String commitmentNumber = testCreateCommitment(IDStrings[i], PTA, logFileWriter);
			verifyCommitmentConnection(IDStrings[i], commitmentNumber, recruitmentID, logFileWriter);
			verifyUpdatedRecruitmentStatus(IDStrings[i], logFileWriter);
		}//end for loop - iterating through the SA's
		logFileWriter.write("All Recruitments are successfully created - now verifying Recruitment Status Impact");
		logFileWriter.newLine();
		
		verifyRecruitmentStatusImpact(logFileWriter);
	}//end test method
	
	protected void verifyRecruitmentStatusImpact (BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method verifyRecruitmentStatusImpact called ***");
		logFileWriter.newLine();
		
		//first, close all URL's except the SA Lookup page, the Administration page, and the Salary Setting Page
		String[] URLs = {UIController.administrationURL, UIController.salarySettingURL};
		UIController.closeAllWindowsExceptURLs(URLs, logFileWriter);
		
		if (UIController.switchWindowByURL(UIController.administrationURL, logFileWriter)){
			logFileWriter.write("Successfully switched to the Administration page, as expected");
			logFileWriter.newLine();
		}//end if - everything works so far
		else{
			String message = "ERROR: Could not switch to the Administration page - aborting verifyRecruitmentStatusImpact method;\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
			return;
		}//end else - error condition
		
		Thread.sleep(2000);
		
		if (UIController.clickLinkAndWait("Operations", logFileWriter)){
			logFileWriter.write("Successfully switched to the Start page, as expected");
			logFileWriter.newLine();
		}//end if - everything works so far
		else{
			String message = "ERROR: Could not switch to the Administration page - aborting verifyRecruitmentStatusImpact method;\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
			return;
		}//end else - error condition
		
		int numberOfSAsTested = 2;
		String[] IDs = selectInitialRecruitments(verificationErrors, numberOfSAsTested, logFileWriter);
		for (int id=0; id<numberOfSAsTested; id++){
			logFileWriter.write("SA ID selected is "+IDs[id]);
			logFileWriter.newLine();
		}//end for loop
		/*
		if (UIController.clickLinkAndWait(IDs[0], logFileWriter)){
			logFileWriter.write("Search Authorization Page for SA ID "+IDs[0]+" successfully opened");
			logFileWriter.newLine();
		}//end if - this is as expected
		else{
			String message = "ERROR - could not open Search Authorization window for SA ID "+IDs[0]
					+" in method verifyRecruitmentStatusImpact;\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
			System.out.println(message);
			return;
		}//end else - error condition - abort method
		*/
		Thread.sleep(5000);
		
		if (switchToSearchAuthWindow(IDs[0], verificationErrors, logFileWriter)){
			logFileWriter.write("Successfully switched to Search Authorization Page for SA ID "+IDs[0]+" successfully opened");
			logFileWriter.newLine();
		}//end if - this is as expected
		else{
			String message = "ERROR - could not switch to Search Authorization window for SA ID "+IDs[0]
					+" in method verifyRecruitmentStatusImpact - returning;\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
			System.out.println(message);
			return;
		}//end else - error condition - abort method

		//use one SA - create a Recruitment and set its Offer Status to "Pending"
		verifyRecruitmentStatus(IDs[0], logFileWriter);
		declineFirstRecruitment(IDs[0], logFileWriter);
		

		//now, close all URL's except the SA Lookup page, the Administration page, and the Salary Setting Page
		String[] newURLs = {UIController.startPage, UIController.salarySettingURL, UIController.searchAuthorizationLookupURL};
		UIController.closeAllWindowsExceptURLs(newURLs, logFileWriter);
		
		try{
			UIController.switchWindowByURL(UIController.searchAuthorizationLookupURL, logFileWriter);
			logFileWriter.write("Search Authorization Lookup Page successfully opened");
			logFileWriter.newLine();
		}//end try - this is as expected
		catch(Exception e){
			String message = "ERROR - could not open Search Authorization Lookup Page "
					+" in method verifyRecruitmentStatusImpact, because of Exception "+e.getMessage()+";\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
			System.out.println(message);
			return;
		}//end else - error condition - abort method
		
		Thread.sleep(5000);
		
		try{
			UIController.clickLinkAndWait(IDs[1], logFileWriter);
			logFileWriter.write("Search Authorization Page for SA ID "+IDs[1]+" successfully opened");
			logFileWriter.newLine();
		}//end try - this is as expected
		catch(Exception e){
			String message = "ERROR - could not open Search Authorization window for SA ID "+IDs[1]
					+" in method verifyRecruitmentStatusImpact, because of Exception "+e.getMessage()+";\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
			System.out.println(message);
			return;
		}//end else - error condition - abort method
		
		Thread.sleep(5000);
		
		try{
			switchToSearchAuthWindow(IDs[1], verificationErrors, logFileWriter);
			logFileWriter.write("Successfully switched to Search Authorization Page for SA ID "+IDs[1]+" successfully opened");
			logFileWriter.newLine();
		}//end if - this is as expected
		catch(Exception e){
			String message = "ERROR - could not switch to Search Authorization window for SA ID "+IDs[1]
					+" in method verifyRecruitmentStatusImpact because of Exception "+e.getMessage()+";\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
			System.out.println(message);
			return;
		}//end else - error condition - abort method


		failAnotherRecruitment(IDs[1], logFileWriter);
	}//end verifyRecruitmentStatusImpact method
	
	//create two Recruitments.  The first will be Open or Pending and will have "Open" to the right of the dropdown.  
	//The second will open with the drop-down not selectable and "Open" at the right of the dropdown
	protected void verifyRecruitmentStatus(String searchAuthIDString, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method verifyRecruitmentStatus called with SA ID "+searchAuthIDString+" ***");
		logFileWriter.newLine();
		System.out.println("*** method verifyRecruitmentStatus called with SA ID "+searchAuthIDString+" ***");
		
		addNewRecruitment(searchAuthIDString, true, logFileWriter);
		enterBasicRecruitmentInfo(searchAuthIDString, true, logFileWriter);
		Thread.sleep(3000);
		try{
			UIController.driver.switchTo().alert().accept();
		}
		catch(Exception e){
			logFileWriter.write("Could not dismiss alert because of Exception "+e.getMessage());
			logFileWriter.newLine();
		}
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "RecruitmentIDLabel", logFileWriter);
		String recruitmentID = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "RecruitmentStatusLabel", logFileWriter);
		String expectedRecruitmentStatus = "Open";
		String actualRecruitmentStatus = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		logFileWriter.write("Actual Recruitment Status is determined to be '"+actualRecruitmentStatus+"';");
		logFileWriter.newLine();
		compareExpectedAndActualValues(expectedRecruitmentStatus, actualRecruitmentStatus, "Recruitment General Page", "RecruitmentStatusLabel", 
				recruitmentID, logFileWriter);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "CloseButton", logFileWriter);
		UIController.waitAndClick(by);
		logFileWriter.write("Window with Recruitment ID "+recruitmentID+" successfully closed");
		logFileWriter.newLine();

		try{
			switchToSearchAuthWindow(searchAuthIDString, verificationErrors, logFileWriter);
			logFileWriter.write("Successfully switched back to Search Authorization Window for SA ID "+searchAuthIDString);
			logFileWriter.newLine();
		}
		catch(Exception e){
			String message = "ERROR - could not switch to Search Authorization window for SA ID "+searchAuthIDString
					+" in method verifyRecruitmentStatus because of Exception "+e.getMessage()+";\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
			System.out.println(message);
			return;
		}//end else
		
		UIController.driver.navigate().refresh();
		Thread.sleep(3000);
		try{
			UIController.driver.switchTo().alert().accept();
			logFileWriter.write("Alert successfully dismissed after refreshing SA page for SA ID "+searchAuthIDString);
			logFileWriter.newLine();
		}
		catch(Exception e){
			logFileWriter.write("Alert could not be discmissed because of "+e.getMessage());
			logFileWriter.newLine();
		}
		
		addNewRecruitment(searchAuthIDString, false, logFileWriter);
/*		
		enterBasicRecruitmentInfo(searchAuthIDString, false, logFileWriter);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "RecruitmentIDLabel", logFileWriter);
		recruitmentID = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);

		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "CloseButton", logFileWriter);
		UIController.waitAndClick(by);
		logFileWriter.write("Window with Recruitment ID "+recruitmentID+" successfully closed");
		logFileWriter.newLine();
		
		try{
			switchToSearchAuthWindow(searchAuthIDString, verificationErrors, logFileWriter);
			logFileWriter.write("Successfully switched back to Search Authorization Window for SA ID "+searchAuthIDString);
			logFileWriter.newLine();
		}//end try
		catch(Exception e){
			String message = "ERROR - could not switch to Search Authorization window for SA ID "+searchAuthIDString
					+" in method verifyRecruitmentStatus because of Exception "+e.getMessage()+";\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
			System.out.println(message);
			return;
		}//end else
		
		UIController.driver.navigate().refresh();
		Thread.sleep(5000);
		try{
			UIController.driver.switchTo().alert().accept();
			logFileWriter.write("Alert successfully dismissed after refreshing SA page for SA ID "+searchAuthIDString);
			logFileWriter.newLine();
		}
		catch(Exception e){
			logFileWriter.write("Alert could not be discmissed because of "+e.getMessage());
			logFileWriter.newLine();
		}
		
		Thread.sleep(3000);
		
		
		
		//the last time we saved the Recruitment ID, it applied to the Backup Recruitment, so we click on the link for that
		UIController.clickLinkAndWait(recruitmentID, logFileWriter);

		Thread.sleep(5000);

		//the current recruitmentID value applies to the Backup Recruitment.  Switch to the Backup Recruitment Window
		try{
			UIController.switchWindowByURL(UIController.newRecruitmentURL+"?recId="+recruitmentID, logFileWriter);
			logFileWriter.write("Successfully switched back to Window for Backup Recruitment ID "+recruitmentID);
			logFileWriter.newLine();
		}//end try
		catch(Exception e){
			String message = "ERROR - could not switch to Window for Recruitment ID "+recruitmentID
					+" in method verifyRecruitmentStatus because of Exception "+e.getMessage()+";\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
			System.out.println(message);
			return;
		}//end else
*/		
		Thread.sleep(3000);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "RecruitmentStatusLabel", logFileWriter);
		expectedRecruitmentStatus = "Open";
		actualRecruitmentStatus = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		logFileWriter.write("Actual Recruitment Status is determined to be '"+actualRecruitmentStatus+"';");
		logFileWriter.newLine();
		compareExpectedAndActualValues(expectedRecruitmentStatus, actualRecruitmentStatus, "Recruitment General Page", "RecruitmentStatusLabel", 
				recruitmentID, logFileWriter);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "OfferStatusSelect", logFileWriter);
		try{
			if (UIController.selectElementInMenu(by, "Accepted")){
				String message = "ERROR - could unexpectedly select an item in the Offer Status Select menu for Recruitment ID "+recruitmentID
						+" in method verifyRecruitmentStatus;\n ";
				logFileWriter.write(message);
				logFileWriter.newLine();
				verificationErrors.append(message);
				System.out.println(message);
				return;
			}//end if - error condition
			else{
				logFileWriter.write("Could not select item in Offer Status Select menu as expected for Recruitment ID "+recruitmentID);
				logFileWriter.newLine();
			}//end else - this is correct
		}//end try - we might not get through this without an Exception
		catch(Exception e){
			logFileWriter.write("Could not select item in menu, as expected - Exception thrown is "+e.getMessage());
			logFileWriter.newLine();
		}//end catch - we might get this from trying to select an item in a non-editable dropdown
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "CloseButton", logFileWriter);
		UIController.waitAndClick(by);
		
		Thread.sleep(5000);
		
		UIController.switchWindowByURL(UIController.notificationDialogURL, logFileWriter);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Page", "NotificationDialogNoButton", logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(3000);
		
		logFileWriter.write("method verifyRecruitmentStatus passes");
		logFileWriter.newLine();
		
		}//end verifyRecruitmentStatus method

	protected void declineFirstRecruitment(String searchAuthIDString, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method declineFirstRecruitment called with SA ID "+searchAuthIDString+" ***");
		logFileWriter.newLine();
		System.out.println("*** method declineFirstRecruitment called with SA ID "+searchAuthIDString+" ***");
		
		switchToSearchAuthWindow(searchAuthIDString, verificationErrors, logFileWriter);
		logFileWriter.write("Successfully switched to Search Authorization window for SA ID "+searchAuthIDString+", as expected");
		logFileWriter.newLine();

		
		UIController.driver.navigate().refresh();
		Thread.sleep(5000);
		try{
			UIController.driver.switchTo().alert().accept();
			logFileWriter.write("Alert successfully dismissed after refreshing SA page for SA ID "+searchAuthIDString);
			logFileWriter.newLine();
		}
		catch(Exception e){
			logFileWriter.write("Alert could not be discmissed because of "+e.getMessage());
			logFileWriter.newLine();
		}
		

		//in this case, the Primary Recruitment is the only Recruitment
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SA General Page", "FirstRecruitmentLink", logFileWriter);
		
		String recruitmentID = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		logFileWriter.write("Primary Recruitment ID is "+recruitmentID);
		logFileWriter.newLine();
		
		UIController.clickLinkAndWait(recruitmentID, logFileWriter);
		Thread.sleep(5000);
		
		UIController.switchWindowByURL(UIController.newRecruitmentURL+"?recId="+recruitmentID, logFileWriter);
		logFileWriter.write("Successfully switched to window with Primary Recruitment ID "+recruitmentID);
		logFileWriter.newLine();

		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "OfferStatusSelect", logFileWriter);
		try{
			if (UIController.selectElementInMenu(by, "Declined")){
				logFileWriter.write("Selected 'Declined' in the Offer status menu, as expected");
				logFileWriter.newLine();
			}//end if
			else{
				String message = "ERROR - could not select 'Declined' in the Offer status menu;\n ";
				logFileWriter.write(message);
				logFileWriter.newLine();
				System.out.println(message);
				verificationErrors.append(message);
				return;
			}//end else
		}//end try
		catch(Exception e){
			String message = "ERROR - could not select 'Declined' in the Offer status menu because of Exception "+e.getMessage()+";\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
			return;
		}//end catch clause
		Thread.sleep(2000);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "DeclinedReasonSelect", logFileWriter);
		Select reasonCode = new Select(UIController.driver.findElement(by));
		reasonCode.selectByVisibleText("Benefits");
		Thread.sleep(2000);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "SaveButton", logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(4000);
		try{
			UIController.driver.switchTo().alert().accept();
		}//end try
		catch(Exception e){
			logFileWriter.write("Could not accept the alert after saving the page after selecting Reason for Decline");
			logFileWriter.newLine();
		}//end catch
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "CloseButton", logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(3000);
		
		switchToSearchAuthWindow(searchAuthIDString, verificationErrors, logFileWriter);
		logFileWriter.write("Successfully switched back to Search Authorization window for SA ID "+searchAuthIDString+", as expected");
		logFileWriter.newLine();

		UIController.driver.navigate().refresh();
		Thread.sleep(4000);
		try{
			UIController.driver.switchTo().alert().accept();
		}//end try
		catch(Exception e){
			logFileWriter.write("Could not accept the alert after refreshing the Search Authorization Page because of Exception "+e.getMessage());
			logFileWriter.newLine();
		}//end catch
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SA General Page", "AddRecruitmentButton", logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(3000);
		
		UIController.switchWindowByURL(UIController.newRecruitmentURL+"?searchAuthId="+searchAuthIDString, logFileWriter);
		Thread.sleep(3000);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "RecruitmentStatusLabel", logFileWriter);
		String expectedRecruitmentStatus = "Open";
		String actualRecruitmentStatus = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		logFileWriter.write("Actual Recruitment Status is determined to be '"+actualRecruitmentStatus+"';");
		logFileWriter.newLine();
		compareExpectedAndActualValues(expectedRecruitmentStatus, actualRecruitmentStatus, "Recruitment General Page", "RecruitmentStatusLabel", 
				"N/A", logFileWriter);

		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "OfferStatusSelect", logFileWriter);
		try{
			if (UIController.selectElementInMenu(by, "Accepted")){
				logFileWriter.write("Could select item in Offer Status Select menu as expected for new Recruitment");
				logFileWriter.newLine();
			}//end if - error condition
			else{
				String message = "ERROR - could unexpectedly not select an item in the Offer Status Select menu for new Recruitment"
						+" in method declineFirstRecruitment;\n ";
				logFileWriter.write(message);
				logFileWriter.newLine();
				verificationErrors.append(message);
				System.out.println(message);
				return;
			}//end else - this is correct
		}//end try - we might not get through this without an Exception
		catch(Exception e){
			String message = "ERROR - could unexpectedly not select an item in the Offer Status Select menu for new Recruitment; "
					+"Exception thrown is "+e.getMessage()
					+" in method declineFirstRecruitment;\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
			System.out.println(message);
			return;
		}//end catch - we might get this from trying to select an item in a non-editable dropdown
		
		
		Thread.sleep(2000);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "CloseButton", logFileWriter);
		UIController.waitAndClick(by);
		
		Thread.sleep(3000);
		
		if (! UIController.switchWindowByURL(UIController.notificationDialogURL, logFileWriter)){
			String message = "ERROR - could not get to the notification dialog when closing new Recruitment"
					+" in method declineFirstRecruitment;\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
			System.out.println(message);
			return;
		}//end if
		else{
			logFileWriter.write("Successfully acquired the notification dialog to close the new Recruitment window in method declineFirstRecruitment");
			logFileWriter.newLine();
		}//end else
		Thread.sleep(2000);
		try{
			UIController.driver.findElement(By.xpath("//td[2]/a")).click();
			logFileWriter.write("Successfully clicked on the 'No' Button to dismiss the notification dialog");
			logFileWriter.newLine();
		}//end try
		catch(Exception e){
			String message = "ERROR - could not click 'No' on the notification dialog while closing new Recruitment"
					+" in method declineFirstRecruitment;\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
			System.out.println(message);
			return;
		}//end catch
		
		logFileWriter.write("method declineFirstRecruitment passes");
		logFileWriter.newLine();
	}//end declineFirstRecruitment method
	
	protected void failAnotherRecruitment(String searchAuthIDString, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method failAnotherRecruitment called with SA ID "+searchAuthIDString+" ***");
		logFileWriter.newLine();
		System.out.println("*** method failAnotherRecruitment called with SA ID "+searchAuthIDString+" ***");
		
		addNewRecruitment(searchAuthIDString, true, logFileWriter);
		enterBasicRecruitmentInfo(searchAuthIDString, true, logFileWriter);
		UIController.driver.navigate().refresh();
		try{
			UIController.driver.switchTo().alert().dismiss();
		}
		catch(Exception e){
			logFileWriter.write("Could not dismiss alert because of Exception "+e.getMessage());
			logFileWriter.newLine();
		}
		Thread.sleep(3000);
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "RecruitmentIDLabel", logFileWriter);
		String recruitmentID = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "OfferStatusSelect", logFileWriter);
		UIController.selectElementInMenu(by, "Failed");
		Thread.sleep(3000);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "FailureReasonSelect", logFileWriter);
		Select reasonCode = new Select(UIController.driver.findElement(by));
		reasonCode.selectByVisibleText("Benefits");
		Thread.sleep(2000);

		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "SaveButton", logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(4000);
		try{
			UIController.driver.switchTo().alert().accept();
		}//end try
		catch(Exception e){
			logFileWriter.write("Could not accept the alert after saving the page after selecting Failed as Offer Status");
			logFileWriter.newLine();
		}//end catch
		
		
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "CloseButton", logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(5000);
		
		switchToSearchAuthWindow(searchAuthIDString, verificationErrors, logFileWriter);
		UIController.driver.navigate().refresh();
		
		Thread.sleep(4000);
		try{
			UIController.driver.switchTo().alert().accept();
		}//end try
		catch(Exception e){
			logFileWriter.write("Could not accept the alert after refreshing the Search Authorization Page");
			logFileWriter.newLine();
		}//end catch
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SA General Page", "AddRecruitmentButton", logFileWriter);
		Thread.sleep(2000);
		UIController.waitAndClick(by);
		Thread.sleep(5000);
		
		UIController.switchWindowByURL(UIController.newRecruitmentURL+"?searchAuthId="+searchAuthIDString, logFileWriter);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "OfferStatusCode", logFileWriter);
		try{
			if (UIController.selectElementInMenu(by, "Accepted")){
				String message = "ERROR - could unexpectedly select an item in the Offer Status Select menu for new Recruitment"
						+" in method failAnotherRecruitment;\n ";
				logFileWriter.write(message);
				logFileWriter.newLine();
				verificationErrors.append(message);
				System.out.println(message);
				return;
			}//end if - error condition
			else{
				logFileWriter.write("Could not select item in Offer Status Select menu as expected for Recruitment ID "+recruitmentID);
				logFileWriter.newLine();
			}//end else - this is correct
		}//end try - we might not get through this without an Exception
		catch(Exception e){
			logFileWriter.write("Could not select item in menu, as expected - Exception thrown is "+e.getMessage());
			logFileWriter.newLine();
		}//end catch - we might get this from trying to select an item in a non-editable dropdown
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "RecruitmentStatusLabel", logFileWriter);
		String expectedRecruitmentStatus = "Open";
		String actualRecruitmentStatus = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		logFileWriter.write("Actual Recruitment Status is determined to be '"+actualRecruitmentStatus+"';");
		logFileWriter.newLine();
		compareExpectedAndActualValues(expectedRecruitmentStatus, actualRecruitmentStatus, "Recruitment General Page", "RecruitmentStatusLabel", 
				recruitmentID, logFileWriter);
		
		Thread.sleep(2000);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "CloseButton", logFileWriter);
		UIController.waitAndClick(by);
		
		Thread.sleep(3000);
		
		if (! UIController.switchWindowByURL(UIController.notificationDialogURL, logFileWriter)){
			String message = "ERROR - could not get to the notification dialog when closing new Recruitment"
					+" in method failAnotherRecruitment;\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
			System.out.println(message);
			return;
		}//end if
		else{
			logFileWriter.write("Successfully acquired the notification dialog to close the new Recruitment window in method declineFirstRecruitment");
			logFileWriter.newLine();
		}//end else
		
		Thread.sleep(3000);
		
		try{
			UIController.driver.findElement(By.xpath("//td[2]/a")).click();
			logFileWriter.write("Successfully clicked on the 'No' Button to dismiss the notification dialog");
			logFileWriter.newLine();
		}//end try
		catch(Exception e){
			String message = "ERROR - could not click 'No' on the notification dialog while closing new Recruitment"
					+" because of Exception "+e.getMessage()
					+" in method declineFirstRecruitment;\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
			System.out.println(message);
			return;
		}//end catch
		
		logFileWriter.write("method failAnotherRecruitment passes");
		logFileWriter.newLine();
	}//end failAnotherRecruitment method
	
	protected void verifyUpdatedRecruitmentStatus (String searchAuthIDString, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method verifyUpdatedRecruitmentStatus called with SA ID "+searchAuthIDString+" ***");
		logFileWriter.newLine();
	
		if (! switchToSearchAuthWindow(searchAuthIDString, verificationErrors, logFileWriter)){
			String message = "ERROR - could not switch to Search Authorization window for SA ID "+searchAuthIDString
					+" in method verifyUpdatedRecruitmentStatus;\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
			System.out.println(message);
		}
		else{
			logFileWriter.write("Successfully switched to Search Authorization window for SA ID "+searchAuthIDString+", as expected");
			logFileWriter.newLine();
		}
		
		String recruitmentID = switchToRecruitmentWindow(searchAuthIDString, "Primary", "verifyUpdatedRecruitmentStatus", verificationErrors, logFileWriter);
		logFileWriter.write("Recruitment being examined is Recruitment ID "+recruitmentID);
		logFileWriter.newLine();
		
		Thread.sleep(3000);
		
		UIController.driver.navigate().refresh();
		Thread.sleep(5000);
		try{
			Alert alert = UIController.driver.switchTo().alert();
			alert.accept();
			logFileWriter.write("Alert with text "+alert.getText()+" was successfully dismissed");
			logFileWriter.newLine();
		}//end try
		catch(Exception e){
			logFileWriter.write("Could not dismiss the alert because of "+e.getMessage());
			logFileWriter.newLine();
		}//end catch
		
		logFileWriter.write("Recruitment Page successfully refreshed");
		logFileWriter.newLine();
		
		By createCommitmentButton = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", 
				"CreateCommitmentButton", logFileWriter);
		try{
			UIController.driver.findElement(createCommitmentButton).click();
		}
		catch(Exception e){
			logFileWriter.write("Create Commitment Button is no longer accessible, as expected, Exception thrown is "+e.getMessage());
			logFileWriter.newLine();
		}
		
		By printRecordButton = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", 
				"PrintRecordButton", logFileWriter);
		try{
			UIController.driver.findElement(printRecordButton).click();
			logFileWriter.write("Print Button is accessible, as expected");
			logFileWriter.newLine();
		}
		catch(Exception e){
			String message = "ERROR - Print Record Button is unexpectedly not accessible, Exception thrown is "+e.getMessage()+";\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
		}//end catch
		
		Thread.sleep(5000);

		printPDFContentsToLogFile(logFileWriter, searchAuthIDString, recruitmentID);

		//now, try to switch to the tab containing the PDF
		if (! UIController.switchWindowByURL("processSAReportRequest.do?rptType=RecDetailRpt&RECID="+recruitmentID, logFileWriter)){
			logFileWriter.write("Could not switch to the PDF Print Record page for Recruitment ID "+recruitmentID + ", as expected");
			logFileWriter.newLine();
		}//end if - PDF was downloaded, not brought up in the browser, we're OK
		else{
			String message = "ERROR - could unexpectedly switch to the PDF Print Record page for Recruitment ID "+recruitmentID+";\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
		}//end else
		
		if (UIController.switchWindowByURL(UIController.searchAuthorizationLookupURL, logFileWriter)){
			logFileWriter.write("Successfully switched to Search Authorization Lookup Window");
			logFileWriter.newLine();
		}//end if - everything is correct and we've switched to the Search Authorization window
		else{
			String message = "ERROR - could not switch to the Search Authorization Lookup Window";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
		}//end else - error - we can't find the Search Authorization window
		logFileWriter.write("method verifyUpdatedRecruitmentStatus passes");
		logFileWriter.newLine();
	}//end verifyUpdatedRecruitmentStatus method
	
	public void printPDFContentsToLogFile(BufferedWriter logFileWriter, String searchAuthIDString, String recruitmentID) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("**** method printPDFContentsToLogFile called ****");
		logFileWriter.newLine();
		logFileWriter.newLine();
		String currentURL = UIController.driver.getCurrentUrl();
		logFileWriter.write("Online URL being worked with is "+currentURL);

		TestFileController.copyFile("RecruitmentDetailReportRequest.pdf", logFileWriter, ReportRunner.logDirectoryName);
		String PDF_filepath = ReportRunner.logDirectoryName + "/"+"RecruitmentDetailReportRequest.pdf";
		File PDF_file = new File(PDF_filepath);
		if (PDF_file.exists()) {
			logFileWriter.write("File has been successfully downloaded - "+PDF_filepath);
			logFileWriter.newLine();
		}//end if
		else {
			logFileWriter.write("Could not download the file - returning");
			logFileWriter.newLine();
			return;
		}
		
		String urlString = "file:///"+PDF_filepath;
		
		logFileWriter.newLine();
		logFileWriter.newLine();
		
		Thread.sleep(6000);
		URL url = new URL(urlString);
		InputStream input = url.openStream();
		BufferedInputStream parsedPDF = new BufferedInputStream(input);
		PDDocument PDFtoRead = null;
		
		PDFtoRead = PDDocument.load(parsedPDF);
		String pdfContent = new PDFTextStripper().getText(PDFtoRead);
		verifyPDFContents(logFileWriter, pdfContent, searchAuthIDString, recruitmentID);
		logFileWriter.write("Contents of PDF are as follows: ");
		logFileWriter.newLine();
		logFileWriter.write(pdfContent);
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("**** method printPDFContentsToLogFile completed successfully ****");
		logFileWriter.newLine();
		logFileWriter.newLine();
		
		PDFtoRead.close();		
	}//end printPDFContentsToLogFile method
	
	protected void verifyPDFContents(BufferedWriter logFileWriter, String pdfContent, String searchAuthIDString, String recruitmentID) throws Exception{
		logFileWriter.write("**** method verifyPDFContents called ****");
		logFileWriter.newLine();
		
		verifyStringInPDFContents(logFileWriter, pdfContent, "Recruitment ID: "+recruitmentID);
		verifyStringInPDFContents(logFileWriter, pdfContent, "FirstName"+searchAuthIDString+"_2Candidate");
		verifyStringInPDFContents(logFileWriter, pdfContent, "ClosedStatus");
		verifyStringInPDFContents(logFileWriter, pdfContent, "LastName"+searchAuthIDString+"_2");
		verifyStringInPDFContents(logFileWriter, pdfContent, "Offer Status Accepted");
		verifyStringInPDFContents(logFileWriter, pdfContent, "Auth ID "+searchAuthIDString);
		verifyStringInPDFContents(logFileWriter, pdfContent, "Approved");
		verifyStringInPDFContents(logFileWriter, pdfContent, "Recruitment Primary");
		verifyStringInPDFContents(logFileWriter, pdfContent, "SpouseFirst"+searchAuthIDString+"Name");
		verifyStringInPDFContents(logFileWriter, pdfContent, "Notes "+searchAuthIDString);
		logFileWriter.write("**** method verifyPDFContents completed successfully ****");
		logFileWriter.newLine();
	}//end verifyPDFContents method
	
	protected void verifyStringInPDFContents(BufferedWriter logFileWriter, String pdfContent, String toVerify) throws Exception{
		boolean contains = false;
		contains = pdfContent.contains(toVerify);
		if (contains) {
			logFileWriter.write("PDF contains '"+toVerify+"', as expected");
			logFileWriter.newLine();
		}//end if - we're all good
		else {
			String errMsg = "ERROR - PDF does NOT contain '"+toVerify+"', as expected; ";
			logFileWriter.write(errMsg);
			logFileWriter.newLine();
			verificationErrors.append(errMsg+"/n");
			System.out.println(errMsg);
		}//end else - ERROR
	}//end verifyStringInPDFContents method
	
	protected void verifyCommitmentConnection (String searchAuthIDString, String expectedCommitmentNumber, String recruitmentID, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method verifyCommitmentConnection called with SA ID "+searchAuthIDString+" ***");
		logFileWriter.newLine();
		
		String currentURL = UIController.driver.getCurrentUrl();
		logFileWriter.write("Current URL is "+currentURL);
		logFileWriter.newLine();
		if (! switchToFacultyWindow(searchAuthIDString, logFileWriter)){
			String message = "Couldn't switch to Faculty window - will re-open through the Recruitment Link;\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			if (! openFacultyWindowThroughCommitment(searchAuthIDString, recruitmentID, logFileWriter)) {
				message = "ERROR - Couldn't switch to Faculty window;\n ";
				logFileWriter.write(message);
				logFileWriter.newLine();
				System.out.println(message);
				verificationErrors.append(message);
				return;
			}//end if - we couldn't switch to the Faculty window even after we re-opened it
		}//end if
		
		Thread.sleep(5000);
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Current Appt Tab", "CommitmentsTabLink", logFileWriter);
		if (! UIController.waitAndClick(by)){
			String message = "ERROR - Couldn't get the Commitments tab on the Faculty record;\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
			return;
		}//end if
		
		Thread.sleep(3000);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Commitments Tab", "FirstCommitmentLink", logFileWriter);
		String actualCommitmentNumber = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		logFileWriter.write("Actual Commitment Number read from the Faculty Record is "+actualCommitmentNumber);
		logFileWriter.newLine();
		
		if (expectedCommitmentNumber.equalsIgnoreCase(actualCommitmentNumber)){
			logFileWriter.write("... and it is as expected");
			logFileWriter.newLine();
		}//end if
		else{
			String message = "ERROR - expected commitment number of "+expectedCommitmentNumber+", but it is actually "+actualCommitmentNumber+";\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
		}//end else
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Commitments Tab", "AttachmentsTabLink", logFileWriter);
		
		if (! UIController.waitAndClick(by)){
			String message = "ERROR - Couldn't get the Attachments tab on the Faculty record;\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
			return;
		}//end if
		
		Thread.sleep(3000);
		UIController.driver.navigate().refresh();
		Thread.sleep(5000);
		try{
			UIController.driver.switchTo().alert().accept();
		}//end try
		catch(Exception e){
			logFileWriter.write("Could not dismiss alert because of "+e.getMessage());
			logFileWriter.newLine();
		}//end catch
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Attachments Tab", "FirstRowCommitmentAttachmentIDLink", logFileWriter);
		if (! (
				UIController.isElementPresent(by) && UIController.hasVisibleText(by) 
						) ){
			logFileWriter.write("We're not on the Attachments Tab any more - trying to switch back");
			logFileWriter.newLine();
			by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Current Appt Tab", "AttachmentsTabLink", logFileWriter);
			
			if (! UIController.waitAndClick(by)){
				String message = "ERROR - Couldn't get the Attachments tab on the Faculty record;\n ";
				logFileWriter.write(message);
				logFileWriter.newLine();
				System.out.println(message);
				verificationErrors.append(message);
				return;
			}//end inner if
			else{
				logFileWriter.write("Successfully clicked on the Attachments tab link");
				logFileWriter.newLine();
			}//end else
		}//end outer if
		//reset it to point to the Commitment Link, just in case
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Attachments Tab", "FirstRowCommitmentAttachmentIDLink", logFileWriter);
		boolean gotCommitmentLink = false;
		if (! UIController.waitAndClick(by)){
			by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Attachments Tab", "CommitmentsExpansionButton", logFileWriter);
			if (UIController.waitAndClick(by)){
				by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Attachments Tab", "FirstRowCommitmentAttachmentIDLink", logFileWriter);
				if (UIController.waitAndClick(by)){
					gotCommitmentLink = true;
				}//end if
			}//end if - expanded the Commitment section
			else{
				logFileWriter.write("Couldn't click on the button to expand the Commitments Section");
				logFileWriter.newLine();
			}//end else
		}//end if
		else{
			gotCommitmentLink = true;
		}//end else
		
		if (! gotCommitmentLink){
			String message = "ERROR - Couldn't open the Commitment Record on the Faculty Attachments tab;\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
			return;
		}//end if
		else{
			logFileWriter.write("Got the Commitment Link in the Attachments tab of the Faculty Record");
			logFileWriter.newLine();
			if (UIController.clickLinkAndWait(actualCommitmentNumber, logFileWriter)){
				logFileWriter.write("Successfully clicked on the link to the Commitment again");
				logFileWriter.newLine();
			}//end if
		}//end else
		
		Thread.sleep(5000);
		
		if (UIController.switchWindowByURL(UIController.commitmentWindowURL, logFileWriter)){
			logFileWriter.write("Successfully switched to the Commitment window with URL "+UIController.driver.getCurrentUrl());
			logFileWriter.newLine();
		}//end if
		else{
			String message = "ERROR - Could not switch to the Commitment window;\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
			return;
		}//end else
		
		Thread.sleep(5000);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Page", "CommitmentStatusLabel", logFileWriter);
		String expectedStatus="Status:  OPEN / APPROVED";
		String actualStatus = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		logFileWriter.write("Actual Status read from the Commitment Record is "+actualStatus);
		logFileWriter.newLine();
		
		if (expectedStatus.equalsIgnoreCase(actualStatus)){
			logFileWriter.write("... and it is as expected");
			logFileWriter.newLine();
		}//end if
		else{
			String message = "ERROR - expected status of "+expectedStatus+", but it is actually "+actualStatus+";\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
		}//end else
		
		postSubmissionTransferLines = getTransferLineData(logFileWriter);
		
		for (int line=0; line<preSubmissionTransferLines.size(); line++){
			preSubmissionTransferLines.get(line).compareToTransferLine(postSubmissionTransferLines.get(line), verificationErrors, logFileWriter);
		}//end for
		
		//call the method that tests HANDSON-3777 
		testSendButton(logFileWriter, expectedCommitmentNumber);
	
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Page", "CloseButton", logFileWriter);
		if (! UIController.waitAndClick(by)){
			String message = "ERROR - Could not close the Commitment window;\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
		}//end if
		else {
			logFileWriter.write("Closed Commitment Window, as expected");
			logFileWriter.newLine();
		}
		
	}//end verifyCommitmentConnection method
	
	protected boolean openFacultyWindowThroughCommitment(String searchAuthID, String recruitmentID, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method openFacultyWindowThroughCommitment called ***");
		logFileWriter.newLine();
		boolean gotFacultyWindow = false;
		logFileWriter.write("Working with Recruitment ID "+recruitmentID);
		logFileWriter.newLine();
		UIController.waitAndClick(By.linkText(recruitmentID));
		Thread.sleep(5000);
		if  (! UIController.switchWindowByURL(UIController.newRecruitmentURL+"?recId="+recruitmentID, logFileWriter))
			return gotFacultyWindow;
		logFileWriter.write("Successfully switched to the Recruitment Window");
		logFileWriter.newLine();
		
		String facultyName = "FirstName"+searchAuthID+"_2 LastName"+searchAuthID+"_2";
		logFileWriter.write("Got Faculty Name "+facultyName+" from the Recruitment Page");
		logFileWriter.newLine();

		UIController.waitAndClick(By.linkText(facultyName));
		Thread.sleep(5000);
		if  (! UIController.switchWindowByURL("showPersonReference.do?personUnvlId=HS_LastName"+searchAuthID, logFileWriter))
			return gotFacultyWindow;
		else gotFacultyWindow = true;
		logFileWriter.write("Successfully switched to the Faculty Window");
		logFileWriter.newLine();

		return gotFacultyWindow;
	}//end openFacultyWindowThroughCommitment method
	
	protected String testCreateCommitment (String searchAuthIDString, String PTA, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method testCreateCommitment called with SA ID "+searchAuthIDString+" ***");
		logFileWriter.newLine();
	
		switchToSearchAuthWindow(searchAuthIDString, verificationErrors, logFileWriter);
		String recruitmentID = switchToRecruitmentWindow (searchAuthIDString, "Primary", "testFacultySalary", 
				verificationErrors, logFileWriter);
		UIController.driver.navigate().refresh();
		Thread.sleep(3000);
		try{
			UIController.driver.switchTo().alert().accept();
		}//end try clause
		catch(Exception e){
			logFileWriter.write("Could not dismiss dialog because of Exception "+e.getMessage()+"; ");
			logFileWriter.newLine();
		}//end catch clause
		
		validateRecruitmentPageButtonStatus(recruitmentID, false, true, logFileWriter);
		Thread.sleep(5000);
		if (UIController.switchWindowByURL(UIController.commitmentWindowURL, logFileWriter)){
			logFileWriter.write("Successfully switched to the Commitment Window");
			logFileWriter.newLine();
		}//end if
		else{
			String message = "ERROR - Could not switch to Commitment Window - returning;\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
			return new String();
		}//end else
		Thread.sleep(3000);
		

		String locator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName("Commitment Page", "DataSourceTextField", 
				logFileWriter);
		String fieldName = "Data Source";
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Page", "DataSourceTextField", logFileWriter);
		String actualDataSource = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
		String expectedDataSource = "09/01/2016: Data Source";
		verifyStringValueIsExpected(fieldName, expectedDataSource, actualDataSource, 1, logFileWriter);

		//this locator will be used to get the bottom-most row in the Transfer Lines
		locator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName("Commitment Page", "TransferStatusFirstRowSelect", 
				logFileWriter);
		int lastRow = HandSOnTestSuite.searchAuthorizationUI.getBottomRow(locator.split("=")[1], "bogus value", 1, logFileWriter, true);
		
		testFailureByOmittedHoldReason(lastRow, PTA, logFileWriter);
		testPassedValidationByReadyStatus(lastRow, PTA, logFileWriter);
		
		testFailureByEmptyField("Description", "DescriptionTextField", logFileWriter);
		testPassedValidationByNonEmptyField("Description", "DescriptionTextField", "Description", logFileWriter);
		
		testFailureByEmptyField("Data Source", "DataSourceTextField", logFileWriter);
		testPassedValidationByNonEmptyField("Data Source", "DataSourceTextField", "09/01/2016: Data Source", logFileWriter);
		
		preSubmissionTransferLines = getTransferLineData (logFileWriter);
		
		//make sure that the 'Send' button doesn't appear - this tests HANDSON-3777
		testSendButton(logFileWriter, "Not Yet Submitted");
		
		String commitmentNumber = testSubmission(PTA, recruitmentID, logFileWriter);
		logFileWriter.write("Commitment number being returned is "+commitmentNumber);
		logFileWriter.newLine();
		return commitmentNumber;
	}//end testCreateCommitment method
	
	protected ArrayList<CommitmentTransferLine> getTransferLineData (BufferedWriter logFileWriter) throws Exception{
		ArrayList<CommitmentTransferLine> transferLines = new ArrayList<CommitmentTransferLine>();
		String locator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName("Commitment Page", "DestPTANumberTextfieldFirstRow", 
				logFileWriter).split("=")[1];
		logFileWriter.write("Working with initial locator of "+locator);
		logFileWriter.newLine();
		int lastRow = HandSOnTestSuite.searchAuthorizationUI.getBottomRow(locator, "bogus value", 2, logFileWriter, true);
		for (int row=1; row<lastRow; row++){
			CommitmentTransferLine newLine = new CommitmentTransferLine(row, logFileWriter);
			if ((row == 4 || row == 5) && 
					(newLine.getValueForField(CommitmentTransferLine.SubCategory, logFileWriter).equalsIgnoreCase("Summer 9ths"))) {
				String nextNextYear = "20"+HandSOnTestSuite.currentNextNextYear;
				logFileWriter.write("Summer Ninths value encountered - adjusting Fiscal Year to match Faculty Employment Year - ");
				logFileWriter.write("value: "+nextNextYear);
				logFileWriter.newLine();
				newLine.setValue(CommitmentTransferLine.FiscalYear, nextNextYear, logFileWriter);
				Thread.sleep(3000);
				newLine.initialize(logFileWriter);
				Thread.sleep(3000);
				/*
				//now click on the Destination PTA text field in the first row to switch the focus
				By by = By.xpath(locator);
				UIController.driver.findElement(by).click();
				Thread.sleep(3000);
				logFileWriter.write("Now marking line as Approved");
				logFileWriter.newLine();
				newLine.setValue(CommitmentTransferLine.Approval, "Approved", logFileWriter);
				Thread.sleep(3000);
				*/
			}//adjusting Summer Ninths Commitment Line to be in accordance with Faculty employment
			transferLines.add(newLine);
		}//end for loop
		return transferLines;
	}//end getTransferLineData method
	
	protected String testSubmission(String PTA, String recruitmentID, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method testSubmission called ***");
		logFileWriter.newLine();
		System.out.println("*** method testSubmission called ***");

		String commitmentNumber = new String();
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Page", "DraftCommitmentSubmitButton", logFileWriter);
		if (UIController.waitAndClick(by)){
			logFileWriter.write("Submission button successfully clicked");
			logFileWriter.newLine();
		}//end if
		Thread.sleep(10000);
		try{
			if (UIController.switchWindowByURL(UIController.notificationDialogURL, logFileWriter)){
				Thread.sleep(2000);
				logFileWriter.write("Got the notification dialog");
				logFileWriter.newLine();
			}//end if
			else{
				logFileWriter.write("Could not get the notification dialog - re-trying...");
				logFileWriter.newLine();
				Thread.sleep(3000);
				if (UIController.switchWindowByURL(UIController.notificationDialogURL, logFileWriter)){
					Thread.sleep(2000);
					logFileWriter.write("Got the notification dialog the second time");
					logFileWriter.newLine();
				}//end if
				else{
					logFileWriter.write("Could not get the notification dialog a second time.");
					logFileWriter.newLine();
					Thread.sleep(2000);
				}//end inner else
			}//end else
			analyzeNotificationMessages(PTA, logFileWriter);
			UIController.driver.findElement(By.id("mybutton")).click();
		}//end try
		catch(Exception e){
			logFileWriter.write("Could not acquire notification dialog because of "+e.getMessage());
			logFileWriter.newLine();
		}//end catch
		Thread.sleep(5000);
		if (UIController.switchWindowByURL(UIController.newRecruitmentURL+"?recId="+recruitmentID, logFileWriter)){
			UIController.driver.navigate().refresh();
			Thread.sleep(5000);
			try{
				UIController.driver.switchTo().alert().accept();
				logFileWriter.write("Successfully dismissed the dialog after refreshing the Recruitment Page");
				logFileWriter.newLine();
			}//end try
			catch(Exception e){
				logFileWriter.write("Could not dismiss the dialog after refreshing the Recruitment Page because of "+e.getMessage());
				logFileWriter.newLine();
			}//end catch
			Thread.sleep(2000);
			by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "FirstCommitmentLink", logFileWriter);
			commitmentNumber =  HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
			logFileWriter.write("Returning Commitment Number "+commitmentNumber);
			logFileWriter.newLine();
		}//end if
		logFileWriter.write("Returning a Commitment Number of "+commitmentNumber);
		logFileWriter.newLine();
		return commitmentNumber;
	}//end testSubmission method
	
	protected void analyzeNotificationMessages(String PTA, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("Now attempting to analyze the messages in the notification dialog");
		logFileWriter.newLine();
		boolean reachedBottom = false;
		String baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName("Commitment Page", 
				"NotificationDialogFirstMessageLabel", logFileWriter);
		logFileWriter.write("Lines found in Notification Dialog are as follows:");
		logFileWriter.newLine();
		for (int row=1;  (! reachedBottom); row+=2){
			//String amendedLocator = HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			By by = By.xpath("//div/table/tbody/tr["+row+"]/td[2]");
			
			if (UIController.isElementPresent(by)){
				String actualText = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
				logFileWriter.write("Row "+ row + ": "+actualText);
				if (actualText.contains(PTA)){
					logFileWriter.write(", contains PTA as expected");
					logFileWriter.newLine();
				}//end if
				else{
					String message = "Row "+row+" of notification dialog doesn't contain the PTA:\n"+actualText;
					logFileWriter.write(message);
					logFileWriter.newLine();
					System.out.println(message);
					verificationErrors.append(message);
				}//end else
			}//end if
			else{
				reachedBottom = true;
				logFileWriter.write("Reached the last of the messages at row "+row);
				logFileWriter.newLine();
			}//end else
		}//end for loop
	}
	/*
	 * This sets up a scenario in which validation is attempted with a valid entry 
	 * in the Description field.  It is expected to pass
	 */
	
	protected void testPassedValidationByNonEmptyField(String fieldName, String locatorName, String expectedDescription, 
			BufferedWriter logFileWriter) throws Exception{
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Page", locatorName, logFileWriter);
		HandSOnTestSuite.searchAuthorizationUI.writeStringToTextField(by, expectedDescription, logFileWriter);
		String actualDescription = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
		verifyStringValueIsExpected(fieldName, expectedDescription, actualDescription, 1, logFileWriter);
		
		String expectedAlertText = "This document was validated successfully\n";
		String expectedErrMsg ="N/A";
		validateCommitment(expectedAlertText, expectedErrMsg, 1, logFileWriter);
	}//end testPassedValidationByNonEmptyField method
	
	/*
	 * This sets up a scenario in which validation is attempted with an empty Description
	 * field.  It is expected to fail.
	 */
	protected void testFailureByEmptyField(String fieldName, String locatorName, BufferedWriter logFileWriter) throws Exception{
		String expectedDescription = "";
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Page", locatorName, logFileWriter);
		HandSOnTestSuite.searchAuthorizationUI.writeStringToTextField(by, expectedDescription, logFileWriter);
		String actualDescription = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
		verifyStringValueIsExpected(fieldName, expectedDescription, actualDescription, 1, logFileWriter);
		
		String expectedErrMsg = "Error: "+fieldName+" of section Funding Commitment: A "+fieldName.toLowerCase()+" is required.";
		String expectedAlertText = "This document failed validation\n";
		validateCommitment(expectedAlertText, expectedErrMsg, 1, logFileWriter);

	}//end testFailureByEmptyField method

	/*
	 * This validates the two status items of the Recruitment page.  It validates
	 * whether or not the "Create New Faculty" button is present and it validates 
	 * whether or not the "Create New Commitment" button is present.
	 */
	protected void validateRecruitmentPageButtonStatus(String recruitmentID, boolean createNewFacultyButtonPresent, boolean createCommitmentButtonPresent, 
			BufferedWriter logFileWriter) throws Exception{
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment Costs Page", "GeneralTabLink", logFileWriter);
		if (UIController.isElementPresent(by)){
			if (UIController.waitAndClick(by)){
				logFileWriter.write("Had to switch to the General Tab on the Recruitment for Recruitment ID "+recruitmentID+";");
				logFileWriter.newLine();
			}//end if
		}//end if
		
		Thread.sleep(3000);
		verifyButtonStatus(recruitmentID, "CreateFacultyRecordButton", "Create New Faculty", true, logFileWriter);
		verifyButtonStatus(recruitmentID, "CreateCommitmentButton", "Create New Commitment", false, logFileWriter);
	}//end validateRecruitmentPageButtonStatus method
	
	/*
	 * There are three boolean variables being worked with here:
	 * (1) whether the Button should be absent
	 * (2) is the Button present (may or may not be visible and clickable)
	 * (3) is the Button visible and clickable
	 * 
	 * If (1) and (2) and (3) are all true, then it's an error
	 * If (1) and (2) are both true, and (3) is false, then no error.
	 * If (1) is true and (2) is false, then no error.
	 * If (1) is false and (2) and (3) are both true, then no error
	 * If (1) is false and (2) is true, and (3) is false, then it's an error.
	 * If (1) is false, and (2) is false, then it's an error.
	 * 
	 * 
	 */
	protected void verifyButtonStatus(String recruitmentID, String locatorName, String buttonLabel, boolean shouldBeAbsent, 
			BufferedWriter logFileWriter) throws Exception{
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", locatorName, logFileWriter);
		String message = new String();
		boolean isError = true;//we assume that it's an error until proven otherwise
		boolean isPresent = UIController.isElementPresent(by);
		if (shouldBeAbsent){
			message = message.concat("Button '"+buttonLabel+"' should be absent");
			if (isPresent){
				message = message.concat(", but it's present");
				if (UIController.waitAndClick(by, 5)){
					message = "ERROR - "+ message.concat(", and it's clickable");
				}//end if - ERROR (should not be there, but it's present and clickable)
				else{
					message = message.concat(", but it's not clickable, so it's acceptable;");
					isError = false;
				}//end else - no error (should not be there, is there, but is not clickable)
			}//end if - button is present but should not be
			else{
				message = message.concat(", and it is absent, as expected; ");
				isError = false;
			}//end else - no error - should be absent and it is
		}//end if - button should be absent
		else {//should not be absent but present
			message = message.concat("Button '"+buttonLabel+"' should be present");
			if (isPresent){
				message = message.concat(", and it is present");
				if (UIController.waitAndClick(by, 5)){
					message = message.concat(", and it's clickable");
					isError = false;
				}//end if - it's present and it's clickable, as it should be
				else{
					message = "ERROR - "+message.concat(", but it's not clickable");
				}//end else - ERROR (it's present but not clickable)
			}//end if - it's present as it should be
			else{
				message = "ERROR - "+message.concat(", but it is absent; ");				
			}//end else - ERROR - it should be present but it's not
		}//end else - should be present
		
		if (isError){
			message = message.concat("\n ");
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
			return;
		}//end if
		else{
			logFileWriter.write(message);
			logFileWriter.newLine();
		}//end else
	}//end verifyButtonStatus method

	protected void testPassedValidationByReadyStatus(int lastRow, String PTA, BufferedWriter logFileWriter) throws Exception{
		for (int row=1; row<=lastRow; row++){
			selectItemForRow("Transfer Status", "TransferStatusFirstRowSelect", "Ready", row, logFileWriter);
		}//end for loop
		String expectedAlertText = "This document was validated successfully\n";
		String expectedTransferLineFailureMsg ="N/A";
		validateCommitment(expectedAlertText, expectedTransferLineFailureMsg, lastRow, logFileWriter);
	}//end testPassedValidationByReadyStatus method
	
	/*
	 * This method will test a failure by setting the Transfer Status to "Hold" and deliberately
	 * omitting a reason for the transfer being held.  
	 */
	protected void testFailureByOmittedHoldReason(int lastRow, String PTA, BufferedWriter logFileWriter) throws Exception{
		for (int row=1; row<=lastRow; row++){
			selectItemForRow("Transfer Status", "TransferStatusFirstRowSelect", "Hold", row, logFileWriter);
			selectItemForRow("Transfer Status Hold Reason", "TransferStatusHoldReasonFirstRowSelect", "select", row, logFileWriter);
			verifyLabelInRow("Source PTA Description", "SourcePTADescriptionLabelFirstRow", "Recruitment H & S", row, logFileWriter);
			verifyTextfieldInRow("Destination PTA Number", "DestPTANumberTextfieldFirstRow", PTA, row, logFileWriter);
		}//end for loop
		String expectedAlertText = "This document failed validation\n";
		String expectedTransferLineFailureMsg ="Error: At line ##, Hold Reason of section Transfer Details: A hold reason selection is required when the transfer status is Hold.";
		validateCommitment(expectedAlertText, expectedTransferLineFailureMsg, lastRow, logFileWriter);
	}//end testFailureByOmittedHoldReason method
	
	/*
	 * This method will verify the text value in one text field for one row.
	 * It assumes that we are on the Commitment Page already.
	 */

	protected void verifyTextfieldInRow(String fieldName, String locatorName, String expectedText, 
			int row, BufferedWriter logFileWriter) throws Exception{
		String actualText = getStringFromTextFieldInRow(fieldName, locatorName, row, logFileWriter);
		if (locatorName.contains("PTANumberTextfield") && ((actualText==null) ||(actualText.isEmpty()))){
			String message = "HANDSON-3487 is still an issue: Default PTA "+expectedText
					+" is not in the Destination PTA field, as expected;\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
			return;
		}//end if
		verifyStringValueIsExpected(fieldName, expectedText, actualText, row, logFileWriter);
	}//end verifyTextfieldInRow method
	
	protected String getStringFromTextFieldInRow(String fieldName, String locatorName, int row, 
			BufferedWriter logFileWriter) throws Exception{
		String locator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName("Commitment Page", locatorName, logFileWriter);
		String amendedLocator = locator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(locator.split("=")[1], row, logFileWriter);
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
		return HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
	}//end getStringFromTextFieldInRow method

	/*
	 * This method will verify the text value in one label for one row.
	 * It assumes that we are on the Commitment Page already.
	 */

	protected void verifyLabelInRow(String fieldName, String locatorName, String expectedText, 
			int row, BufferedWriter logFileWriter) throws Exception{
		String locator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName("Commitment Page", locatorName, logFileWriter);
		String amendedLocator = locator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(locator.split("=")[1], row, logFileWriter);
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
		String actualText = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		verifyStringValueIsExpected(fieldName, expectedText, actualText, row, logFileWriter);
	}//end verifyLabelInRow method
	
	/*
	 * This method will select one item in one dropdown menu for one row.
	 * It assumes that we are on the Commitment Page already.
	 */
	protected void selectItemForRow(String fieldName, String locatorName, String expectedValue, int row,
			BufferedWriter logFileWriter) throws Exception{
		String locator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName("Commitment Page", locatorName, logFileWriter);
		String amendedLocator = locator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(locator.split("=")[1], row, logFileWriter);
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
		UIController.selectElementInMenu(by, expectedValue);
		Thread.sleep(1000);
		String actualValue = HandSOnTestSuite.searchAuthorizationUI.getSelectedTextFromLocator(amendedLocator, locatorName, logFileWriter);
		verifyStringValueIsExpected(fieldName, expectedValue, actualValue, row, logFileWriter);
		Thread.sleep(1000);
	}//end selectItemForRow method
	
	/*
	 * This method assumes that we're already on the Commitment page of the new Commitment
	 * and that we've already configured the Commitment with the fields that we expect
	 * and that we know whether we expect the Commitment to pass or fail validation.
	 * The expectedAlertText must reflect our expectation (pass or fail validation).
	 */
	
	protected void validateCommitment(String expectedAlertText, String expectedTransferLineErrorMessage, int numberOfTransferLines, 
			BufferedWriter logFileWriter) throws Exception{
		boolean shouldFail = expectedAlertText.contains("failed");
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Page",  "ValidateButton", logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(4000);

		attemptAlertDismissal(expectedAlertText, "... waiting before trying again", logFileWriter);
		//try again - click the "Validate" button
		attemptAlertDismissal(expectedAlertText, "...second attempt failed, giving up", logFileWriter);
		if (shouldFail){
			for (int row=1; row<=numberOfTransferLines; row++){
				String expectedErrMsg = expectedTransferLineErrorMessage.replaceAll("##", new Integer(row).toString());
				verifyLabelInRow("Transfer Line Status", "ValidationErrorTextLabelFirstRow", expectedErrMsg, row, logFileWriter);
			}//end for
		}//end if
		
	}//end validateCommitment method
	
	protected void attemptAlertDismissal(String expectedAlertText, String subsequentMessage, BufferedWriter logFileWriter) throws Exception{
		try{
			Alert alert = UIController.driver.switchTo().alert();
			String actualAlertText = alert.getText();
			String fieldName = "Alert text";
			int row = 0;
			verifyStringValueIsExpected(fieldName, expectedAlertText, actualAlertText, row, logFileWriter);
			alert.accept();
		}//end try clause
		catch(Exception e){
			logFileWriter.write("Could not acquire the alert because of Exception "+e.getMessage());
			logFileWriter.newLine();
			logFileWriter.write(subsequentMessage);
			logFileWriter.newLine();
			Thread.sleep(4000);
		}//end catch clause
	}//end attemptAlertDismissal method

	protected void verifyStringValueIsExpected(String fieldName, String expectedTransferStatus, String actualTransferStatus, int row, 
			BufferedWriter logFileWriter) throws Exception{
		if (actualTransferStatus.equalsIgnoreCase(expectedTransferStatus)){
			logFileWriter.write(fieldName+" of Commitment is '"+expectedTransferStatus+"', as expected, for row "+row+"; ");
			logFileWriter.newLine();
		}//end if
		else{
			String message = "ERROR - expected "+fieldName+" of '"+expectedTransferStatus+"' but what was actually there was '"+actualTransferStatus+"' for row "+row+";\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
		}//end else
	}//end verifyStringValueIsExpected method
	
	protected String testDefaultPTAs (String searchAuthIDString, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method testDefaultPTAs called with SA ID "+searchAuthIDString+" ***");
		logFileWriter.newLine();
		
		String department = getDepartmentForFacultyMember(searchAuthIDString, logFileWriter);
		logFileWriter.write("Department that we expect to have is the following: "+department);
		logFileWriter.newLine();
		//get to the Start Page
		UIController.switchToStartPage(logFileWriter);
		
		//Do a Commitment Search for Faculty in the same department
		//might want to make sure that the Commitment Search Page isn't already open
		if (! UIController.switchWindowByURL(UIController.commitmentSearchURL, logFileWriter)){
			UIController.switchToStartPage(logFileWriter);
			Thread.sleep(2000);
			processPageByLink("Commitment Search", verificationErrors, logFileWriter);
			Thread.sleep(3000);
		}//end if - open the page if it's not there.
		if (! UIController.switchWindowByURL(UIController.commitmentSearchURL, logFileWriter)){
			String message = "ERROR - could not reach the Commitment Search Page - exiting method testDefaultPTA's";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
			System.out.println(message);
			return new String();
		}//end if
		//clear out any previous values first
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Search Page", "ClearButton", logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(3000);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Search Page", "FulfillmentStatusSelect", logFileWriter);
		UIController.selectElementInMenu(by, "Open");
		Thread.sleep(3000);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Search Page", "DepartmentSelect", logFileWriter);
		UIController.selectElementInMenu(by, department);
		Thread.sleep(3000);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Search Page", "FindButton", logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(3000);
		logFileWriter.write("Selected "+department+" as the Department for Faculty with SA ID "+searchAuthIDString);
		logFileWriter.newLine();
		//select a Commitment and open it - could be the first Commitment we see
		
		//the legacy PTA's have the word "Legacy" and are at the top of the screen
		//select a row containing a non-legacy PTA and copy that PTA number
		String PTANumber = retrievePTANumber(logFileWriter);
		logFileWriter.write("PTA Number retrieved is the following: "+PTANumber);
		logFileWriter.newLine();
		
		UIController.switchToStartPage(logFileWriter);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Start Page", "AdministrationTabLink", logFileWriter);
		UIController.waitAndClick(by);
		
		UIController.switchToAdministrationPage(logFileWriter);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Administration Page", "ManageDefaultFacultyPTALink", logFileWriter);
		UIController.waitAndClick(by);
		
		UIController.switchWindowByURL(UIController.manageDefaultFacultyPTAURL, logFileWriter);
		Thread.sleep(3000);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Add Default PTA Page", "FacultyNameSelector", logFileWriter);
		UIController.selectElementInMenu(by, getFacultyName(searchAuthIDString, logFileWriter));
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Add Default PTA Page", "AddDefaultPTAButton", logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(2000);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Add Default PTA Page", "PTATextField", logFileWriter);
		UIController.driver.findElement(by).clear();
		UIController.driver.findElement(by).sendKeys(PTANumber);
		Thread.sleep(2000);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Add Default PTA Page", "DepartmentSelector", logFileWriter);
		UIController.selectElementInMenu(by, department);
		Thread.sleep(2000);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Add Default PTA Page", "SaveButton", logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(2000);

		if (UIController.waitForAlertPresent()){
			UIController.driver.switchTo().alert().accept();
		}//end if
		else {
			logFileWriter.write("No alert was present");
		}//end else
		Thread.sleep(2000);
		
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Add Default PTA Page", "DescriptionLabel", logFileWriter);
		String description = UIController.driver.findElement(By.xpath("//td[5]/span")).getText();
		
		logFileWriter.write("Description found is the following: '"+description+"'; ");
		logFileWriter.newLine();
		if ((description == null) || (description.isEmpty())){
			String message = "ERROR - Description field of the PTA should not be empty;\n ";
			System.out.println(message);
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
		}//end if
		
		
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Add Default PTA Page", "CloseButton", logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(3000);
		if (UIController.switchWindowByURL(UIController.manageDefaultFacultyPTAURL, logFileWriter)){
			logFileWriter.write("Could not close the Add Default PTA Page the first time - trying a second time");
			logFileWriter.newLine();
			Thread.sleep(3000);
			by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Add Default PTA Page", "CloseButton", logFileWriter);
			UIController.waitAndClick(by);
		}//end if
		else{
			logFileWriter.write("Add Default PTA Page was successfully closed");
			logFileWriter.newLine();
		}//end else
		return PTANumber;
	}//end testDefaultPTAs method
	
	protected String retrievePTANumber(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method retrievePTANumber called ***");
		logFileWriter.newLine();

		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Search Page", "FirstCommitmentLink", logFileWriter);
		String commitmentNumber = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		logFileWriter.write("Retrieved Commitment Number of "+commitmentNumber);
		logFileWriter.newLine();
		UIController.waitAndClick(by);
		Thread.sleep(5000);
		if (! UIController.switchWindowByURL(UIController.commitmentWindowURL, logFileWriter)){
			logFileWriter.write("Could not switch to Commitment Window");
			logFileWriter.newLine();
			UIController.driver.findElement(by).click();
			if (! UIController.switchWindowByURL(UIController.commitmentWindowURL, logFileWriter)){
				logFileWriter.write("Tried again - Could not switch to Commitment Window");
				logFileWriter.newLine();
				return new String();
			}			
			else{
				logFileWriter.write("On second attempt - Successfully switched to Commitment window");
				logFileWriter.newLine();
			}
		}//end if
		else{
			logFileWriter.write("Successfully switched to Commitment window");
			logFileWriter.newLine();
		}

		
		String locator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName("Commitment Page", "DestPTANumberTextfieldFirstRow", 
				logFileWriter).split("=")[1];
		logFileWriter.write("Working with initial locator of "+locator);
		logFileWriter.newLine();
		int lastRow = HandSOnTestSuite.searchAuthorizationUI.getBottomRow(locator, "bogus value", 2, logFileWriter, true);
		String amendedLocator = HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(locator, lastRow-1, logFileWriter);
		logFileWriter.write("Amended Locator (bottom row) being worked with is "+amendedLocator);
		logFileWriter.newLine();
		Thread.sleep(3000);
		HandSOnTestSuite.salarySettingTabUI.scrollTextIntoView(amendedLocator, logFileWriter);
		UIController.waitForElementPresent(By.xpath(amendedLocator));		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator("xpath="+amendedLocator, logFileWriter);
		
		String lastPTA = new String();
		try {
			lastPTA = UIController.driver.findElement(by).getAttribute("value");
		}
		catch(Exception e) {
			String message = "Could not acquire element associated with locator 'xpath="+amendedLocator+"'";
			UIController.printToAllOutputs("***********************************************************************", logFileWriter);
			UIController.printToAllOutputs("", logFileWriter);
			UIController.printToAllOutputs("***********************************************************************", logFileWriter);
			UIController.printToAllOutputs(message, logFileWriter);
			e.printStackTrace();
			logFileWriter.close();		
			fail (message);
		}
		logFileWriter.write("Bottom-most PTA number being worked with is "+lastPTA);
		logFileWriter.newLine();
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Page", "CloseButton", logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(3000);
		
		UIController.switchWindowByURL(UIController.notificationDialogURL, logFileWriter);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Page", "NotificationDialogNoButton", logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(3000);
		
		if (UIController.switchWindowByURL(UIController.commitmentWindowURL, logFileWriter)){
			logFileWriter.write("Could not successfully close the Commitment Page the first time - trying again");
			logFileWriter.newLine();
			by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Page", "CloseButton", logFileWriter);
			UIController.waitAndClick(by);
			Thread.sleep(3000);
			
			if (UIController.switchWindowByURL(UIController.notificationDialogURL, logFileWriter)){
				logFileWriter.write("Successfully acquired to notification dialog which has the No button");;
				logFileWriter.newLine();
			}//end if
			else{
				String message = "ERROR - could not acquire the notification dialog with the No button;\n";
				logFileWriter.write(message);
				logFileWriter.newLine();
				System.out.println(message);
				verificationErrors.append(message);
			}//end else
			by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Page", "NotificationDialogNoButton", logFileWriter);
			if (UIController.isElementPresent(by)){
				logFileWriter.write("No Button is present and available");
				logFileWriter.newLine();
				UIController.driver.findElement(By.xpath("//td[2]/a")).click();
				Thread.sleep(3000);
				try{
					if (UIController.isElementPresent(by)){
						logFileWriter.write("No Button is still present and available - trying to click it a second time");
						logFileWriter.newLine();
						UIController.driver.findElement(By.xpath("//td[2]/a")).click();
					}//end if	
				}//end try
				catch(Exception e){
					logFileWriter.write("Could not locate the'No' button because of Exception "+e.getMessage());
					logFileWriter.newLine();
				}//end catch
			}//end if
			else{
				String message = "ERROR - cannot locate 'No' button on the dialog;\n ";
				verificationErrors.append(message);
				logFileWriter.write(message);
				logFileWriter.newLine();
				System.out.println(message);
			}
			Thread.sleep(3000);
			if (UIController.switchWindowByURL(UIController.commitmentWindowURL, logFileWriter)){
				String message = "ERROR - Could not successfully close the Commitment Page";
				logFileWriter.write(message);
				logFileWriter.newLine();
				System.out.println(message);
				verificationErrors.append(message);
			}//end if
			else{
				logFileWriter.write("The Commitment Page was successfully closed the second time");
				logFileWriter.newLine();
			}
		}//end if - the Commitment page is still open
		else{
			logFileWriter.write("The Commitment Page was successfully closed the first time");
			logFileWriter.newLine();
		}//end else
		
		return lastPTA;
	}//end retrievePTANumber method
	
	protected String getFacultyName(String searchAuthIDString, BufferedWriter logFileWriter) throws Exception{
		return "LastName"+searchAuthIDString+"_2, FirstName"+searchAuthIDString+"_2";
	}//end getFacultyName method
	
	protected String getDepartmentForFacultyMember(String searchAuthIDString, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method getDepartmentForFacultyMember called with SA ID "+searchAuthIDString+" ***");
		logFileWriter.newLine();
		
		if (! switchToFacultyWindow(searchAuthIDString, logFileWriter)){
			logFileWriter.write("Could not switch to the Faculty Window - getting the Department from the Recruitment instead");
			logFileWriter.newLine();
			String currentURL = UIController.driver.getCurrentUrl();
			String recruitmentID = switchToPrimaryRecruitmentWindow(searchAuthIDString, logFileWriter);
			Thread.sleep(3000);
			By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "DeptLabel", logFileWriter);
			UIController.waitForElementPresent(by);
			String department = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
			if (department == null) department = new String();
			UIController.switchWindowByURL(currentURL, logFileWriter);
			Thread.sleep(3000);
			return department;
		}
		/*
		if (UIController.switchWindowByURL(UIController.existingFacultyURL+"HS_LastName"+searchAuthIDString, logFileWriter)){
			logFileWriter.write("Successfully switched tab to Faculty window with URL "+UIController.driver.getCurrentUrl());
			logFileWriter.newLine();
		}//end if
		else{
			logFileWriter.write("Could not switch to the tab with the current Faculty member - returning empty String");
			logFileWriter.newLine();
			return new String();
		}//end else
		*/
		
		Thread.sleep(3000);
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Attachments Tab", "CurrentApptTabLink", logFileWriter);
		if (UIController.waitAndClick(by)){
			logFileWriter.write("Successfully clicked on the link to the Current Appointment tab on the Faculty record");
			logFileWriter.newLine();
		}//end if
		else{
			logFileWriter.write("Could not successfully click on the link to the Current Appointment tab on the Faculty record");
			logFileWriter.newLine();
		}
		
		String department = HandSOnTestSuite.searchAuthorizationUI.getSelectedText(SearchAuthorizationUIController.myExcelReader, 
				"Faculty Current Appt Tab", "DeptSelect", logFileWriter);
		//just in case we get nothing, at least we don't return null
		if (department == null) department = new String();
		logFileWriter.write("Method getDepartmentForFacultyMember returning Department value of '"+department+"'");
		logFileWriter.newLine();
		return department;
	}//end getDepartmentForFacultyMember method
	
	protected boolean switchToFacultyWindow(String searchAuthIDString, BufferedWriter logFileWriter) throws Exception{
		if (UIController.switchWindowByURL(UIController.existingFacultyURL+"HS_LastName"+searchAuthIDString, logFileWriter)){
			logFileWriter.write("Successfully switched tab to Faculty window with URL "+UIController.driver.getCurrentUrl());
			logFileWriter.newLine();
			return true;
		}//end if
		else{
			logFileWriter.write("Could not switch to the tab with the current Faculty member - returning empty String");
			logFileWriter.newLine();
			return false;
		}//end else
	}//end switchToFacultyWindow method
	
	protected void testFacultyAttachments (String searchAuthIDString, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method testFacultyAttachments called with SA ID "+searchAuthIDString+" ***");
		logFileWriter.newLine();

		switchToSearchAuthWindow(searchAuthIDString, verificationErrors, logFileWriter);
		String recruitmentID = switchToRecruitmentWindow (searchAuthIDString, "Primary", "testFacultySalary", 
				verificationErrors, logFileWriter);
		if (! this.switchToFacultyWindow(searchAuthIDString, recruitmentID, logFileWriter)){
			String message = "ERROR - could not switch to Faculty Window for SA ID "+searchAuthIDString+", recruitment ID "+recruitmentID+";\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
			System.out.println(message);
		}//end if
		Thread.sleep(3000);
		switchToFacultyTab("Faculty Attachments Tab", logFileWriter);
		
		String actualSearchAuthIDAttachmentBaseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName("Faculty Attachments Tab",  
				"FirstRowSearchAuthAttachmentIDLink", logFileWriter).split("=")[1];
		
		int lastSearchAuthAttachment = HandSOnTestSuite.searchAuthorizationUI.getBottomRow(actualSearchAuthIDAttachmentBaseLocator, new String(), logFileWriter, true);
		lastSearchAuthAttachment = lastSearchAuthAttachment -1;//we want the bottom row, not the index below the bottom that has nothing

		for (int row=1; row<=lastSearchAuthAttachment; row++){
			String NameLocator = actualSearchAuthIDAttachmentBaseLocator;
			if (row>1){
				NameLocator = HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(actualSearchAuthIDAttachmentBaseLocator, row, logFileWriter);
				logFileWriter.write("Locator being worked with is "+NameLocator);
				logFileWriter.newLine();
			}//end if
			String actualSearchAuthIDAttachmentString = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(By.xpath(NameLocator), logFileWriter);	

			logFileWriter.write("Search Authorization ID examined is "+searchAuthIDString);
			logFileWriter.newLine();
			
			logFileWriter.write("Search Authorization ID String found in the Attachments tab of the Faculty Record is "+actualSearchAuthIDAttachmentString+";\n ");
			logFileWriter.newLine();
			
			if (searchAuthIDString.equalsIgnoreCase(actualSearchAuthIDAttachmentString)){
				logFileWriter.write("Search Authorization ID String on row "+row+" is "+searchAuthIDString+", as expected");
				logFileWriter.newLine();
			}//end if
			else{
				String message = "ERROR: Expected SA ID is "+searchAuthIDString
						+", but what is found on row "+ row+" of the Attachments tab is "+actualSearchAuthIDAttachmentString+";\n ";
				logFileWriter.write(message);
				logFileWriter.newLine();
				verificationErrors.append(message);
				System.out.println(message);
			}//end else
		}//end for
		
		String actualRecruitmentIDAttachmentBaseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName("Faculty Attachments Tab",  
				"FirstRowRecruitmentAttachmentIDLink", logFileWriter).split("=")[1];
		int lastRecruitmentIDAttachment = HandSOnTestSuite.searchAuthorizationUI.getBottomRow(actualSearchAuthIDAttachmentBaseLocator, new String(), logFileWriter, true);
		lastRecruitmentIDAttachment = lastRecruitmentIDAttachment - 1;//we want the last row with meaningful data
		for (int row=1; row<=lastRecruitmentIDAttachment; row++){
			String NameLocator = actualRecruitmentIDAttachmentBaseLocator;
			if (row>1){
				NameLocator = HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(actualRecruitmentIDAttachmentBaseLocator, row, logFileWriter);
				logFileWriter.write("Locator being worked with is "+NameLocator);
				logFileWriter.newLine();
			}//end if
			String actualRecruitmentIDAttachmentString = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(By.xpath(NameLocator), logFileWriter);	

			logFileWriter.write("Recruitment ID examined is "+recruitmentID);
			logFileWriter.newLine();
			logFileWriter.write("Recruitment ID String found in the Attachments tab of the Faculty Record is "+actualRecruitmentIDAttachmentString+";\n ");
			logFileWriter.newLine();
	
			if (recruitmentID.equalsIgnoreCase(actualRecruitmentIDAttachmentString)){
				logFileWriter.write("Recruitment ID String on row "+row+" is "+recruitmentID+", as expected");
				logFileWriter.newLine();
			}//end if
			else{
				String message = "ERROR: Expected Recruitment ID is "+recruitmentID
						+", but what is found on row "+ row+" of the Attachments tab is "+actualRecruitmentIDAttachmentString+";\n ";
				logFileWriter.write(message);
				logFileWriter.newLine();
				verificationErrors.append(message);
				System.out.println(message);
			}//end else
		}//end second for loop
	}//end testFacultyAttachments method
	
	protected void testFacultySalary (String searchAuthIDString, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method testFacultySalary called with SA ID "+searchAuthIDString+" ***");
		logFileWriter.newLine();

		switchToSearchAuthWindow(searchAuthIDString, verificationErrors, logFileWriter);
		
		String recruitmentID = switchToRecruitmentWindow (searchAuthIDString, "Primary", "testFacultySalary", 
				verificationErrors, logFileWriter);
		
		boolean fails = stillFailsHandSOn3181(searchAuthIDString, logFileWriter);
		String when = "Future";
		if (fails)
			when = "Current";
		verifyFacultyInfoAgainstRecruitmentInfo("EffectiveDateTextField", "Recruitment Costs Page",
				when+"SalaryPromiseEffectiveDateLabel", "Faculty Salary Page", recruitmentID, 
				searchAuthIDString, logFileWriter);
		verifyFacultyInfoAgainstRecruitmentInfo("StartingSalaryTextField", "Recruitment Costs Page",
				when+"SalaryPromiseOneHundredPercentSalaryLabel", "Faculty Salary Page", recruitmentID, 
				searchAuthIDString, logFileWriter);
		String expectedText = "Recruitment";
		String actualText = getValueFromFaculty(when+"SalaryPromiseReasonLabel", "Faculty Salary Page", 
				searchAuthIDString, recruitmentID, logFileWriter);
		compareExpectedAndActualValues(expectedText, actualText, "N/A", "N/A", recruitmentID, logFileWriter);

		expectedText = expectedText.concat(" #"+recruitmentID);
		if (fails)
			expectedText = "Initial Salary";
		actualText = getValueFromFaculty(when+"SalaryPromiseTypeLink", "Faculty Salary Page", 
				searchAuthIDString, recruitmentID, logFileWriter);
		compareExpectedAndActualValues(expectedText, actualText, "N/A", "N/A", recruitmentID, logFileWriter);
	}//end testFacultySalary method
	
	/*
	 * Of note, this has to return true if HANDSON-3181 is still an issue.
	 * HANDSON-3181 is a bug in HandSOn which results in a faculty record for
	 * a faculty member who will be hired at a future date having salary information
	 * as the current salary rather than a future salary promise.
	 */
	protected boolean stillFailsHandSOn3181(String searchAuthIDString, BufferedWriter logFileWriter) throws Exception{
		String IDsubstring = "HS_LastName"+searchAuthIDString+"_2";
		logFileWriter.write("**** Testing to see if HANDSON-3181 is still an issue - ID Substring being used is '"+IDsubstring+"' ****");
		logFileWriter.newLine();
		if (UIController.switchWindowByURL(UIController.existingFacultyURL+IDsubstring, logFileWriter)){
			logFileWriter.write("Successfully switched to the Faculty URL for Faculty with ID "+IDsubstring);
			logFileWriter.newLine();
		}//end if
		else {
			logFileWriter.write("Could not switch to the Faculty URL for Faculty with ID "+IDsubstring+" - exiting method stillFailsHandSOn3181");
			logFileWriter.newLine();
			return true;
		}
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Salary Page", "FutureSalaryPromiseEffectiveDateLabel", logFileWriter);
		if (UIController.isElementPresent(by)){
			String actualText = UIController.driver.findElement(by).getText();
			if (actualText == null || actualText.isEmpty()){
				String message = "HANDSON-3181 is still an issue - actual text found for Future Salary Promise is "+actualText;
				logFileWriter.write(message);
				logFileWriter.newLine();
				verificationErrors.append(message);
				System.out.println(message);
			}//end if
			else{
				logFileWriter.write("HANDSON-3181 is fixed - actual text found for Future Salary Promise is present");
				logFileWriter.newLine();
				return false;
			}//end inner else - it's fixed - no longer an issue
		}
		else{
			String message = "HANDSON-3181 is still an issue - no Web Element found for Future Salary Promise;\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
			System.out.println(message);
		}
		return true;
	}//end stillFailsHandSOn3181 method
	
	/*
	 * This method presumes that a new Faculty Record has just been created from a 
	 * Recruitment and it compares the first and last name of the candidate and of the
	 * spouse in the Recruitment and the Demographics tab of the Recruitments tab.
	 */
	
	protected void testFacultyDemographics (String searchAuthIDString, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method testFacultyDemographics called with SA ID "+searchAuthIDString+" ***");
		logFileWriter.newLine();

		switchToSearchAuthWindow(searchAuthIDString, verificationErrors, logFileWriter);
		
		String recruitmentID = switchToRecruitmentWindow (searchAuthIDString, "Primary", "testFacultyDemographics", 
				verificationErrors, logFileWriter);

		verifyFacultyInfoAgainstRecruitmentInfo("FirstNameTextfield", "Recruitment General Page",
				"FirstNameTextField", "Faculty Demographics Tab", recruitmentID, 
				searchAuthIDString, logFileWriter);
		verifyFacultyInfoAgainstRecruitmentInfo("LastNameTextfield", "Recruitment General Page",
				"LastNameTextField", "Faculty Demographics Tab", recruitmentID, 
				searchAuthIDString, logFileWriter);
		
		String IDsubstring = "HS_LastName"+searchAuthIDString+"_2";
		if (! UIController.switchWindowByURL(UIController.existingFacultyURL+IDsubstring, logFileWriter)){
			String message = "ERROR - Could not switch to New Faculty window for Recruitment ID "+recruitmentID+";\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
			System.out.println(message);
			return;
		}//end if

		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Demographics Tab", "SpouseNameTextField", logFileWriter);
		String actualText = actualText = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);

		switchToRecruitmentViaSearchAuth(searchAuthIDString, recruitmentID, verificationErrors, logFileWriter);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "SpouseFirstNameTextField", logFileWriter);
		String expectedText = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "SpouseMiddleNameTextField", logFileWriter);
		expectedText = expectedText+" "+ HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "SpouseLastNameTextField", logFileWriter);
		expectedText = expectedText+" "+ HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
		
		if (expectedText == null) expectedText = new String();
		logFileWriter.write("Expected Value derived from Spouse Name is '"+expectedText+"';");
		logFileWriter.newLine();
		
		if (expectedText.equalsIgnoreCase(actualText)){
			logFileWriter.write("Spouse Name has value "+expectedText+", as expected;");
			logFileWriter.newLine();
		}//end if - everything is as it should be
		else{
			String message = "ERROR - Spouse Name for Recruitment ID "+recruitmentID+" should have value "+expectedText
					+", but it was actually "+actualText+";\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
		}//end else - we have an error
	}//end testFacultyDemographics method

	/*
	 * This method presumes that a new Faculty record has just been successfully created
	 * from a Recruitment and that the expected information is in the Recruitment 
	 * General tab, and we collect the information from the new Faculty Record 
	 * (Current Appointment tab) and verify it against the expected information.
	 */
	protected String testFacultyCreationFromRecruitment (String searchAuthIDString, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method testFacultyCreationFromRecruitment called with SA ID "+searchAuthIDString+" ***");
		logFileWriter.newLine();

		String recruitmentID = switchToRecruitmentWindow (searchAuthIDString, "Primary", "testFacultyCreationFromRecruitment", 
				verificationErrors, logFileWriter);
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "CloseButton", logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(5000);
		boolean recruitmentIsClosed = false;
		while (! recruitmentIsClosed){
			if (UIController.switchWindowByURL(UIController.newRecruitmentURL+"?recId="+recruitmentID, logFileWriter)){
				logFileWriter.write("Primary Recruitment Window is still open - attempting to close it");
				logFileWriter.newLine();
				UIController.driver.navigate().refresh();
				try{
					UIController.dismissAlert(logFileWriter);
					logFileWriter.write("Closed the dialog");
					logFileWriter.newLine();
				}
				catch(Exception e){
					logFileWriter.write("Could not close the dialog because of Exception "+e.getMessage());
					logFileWriter.newLine();
				}
				UIController.waitAndClick(by);
				Thread.sleep(5000);
			}//end if
			else{
				logFileWriter.write("Primary Recruitment window was closed, as expected");
				logFileWriter.newLine();
				recruitmentIsClosed = true;
			}//end else
		}//end while
		switchToSearchAuthWindow(searchAuthIDString, verificationErrors, logFileWriter);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SA General Page", "FirstRecruitmentLink", logFileWriter);
		UIController.waitAndClick(by);
		if(UpdateSearchAuthorizationTest.switchToRecruitmentWindow(recruitmentID, verificationErrors, logFileWriter)){
			logFileWriter.write("Successfully closed, reopened, and switched to the window for Primary Recruitment ID "+recruitmentID);
			logFileWriter.newLine();
		}//end if
		
		activateNewFacultyWindow(searchAuthIDString, recruitmentID, logFileWriter);		

		verifyFacultyInfoAgainstRecruitmentInfo("AnticipatedStartDateTextfield", "Recruitment General Page",
				"AnticipatedStartDateTextField", "Faculty Current Appt Tab", recruitmentID, 
				searchAuthIDString, logFileWriter);
		verifyFacultyInfoAgainstRecruitmentInfo("RankLabel", "Recruitment General Page",
				"CurrentRankSelect", "Faculty Current Appt Tab", recruitmentID, 
				searchAuthIDString, logFileWriter);
		verifyFacultyInfoAgainstRecruitmentInfo("ApptTypeTextField", "Recruitment General Page",
				"ApptTypeSelect", "Faculty Current Appt Tab", recruitmentID, 
				searchAuthIDString, logFileWriter);
		verifyFacultyInfoAgainstRecruitmentInfo("SchoolLabel", "Recruitment General Page",
				"SchoolSelect", "Faculty Current Appt Tab", recruitmentID, 
				searchAuthIDString, logFileWriter);
		verifyFacultyInfoAgainstRecruitmentInfo("ClusterTextField", "Recruitment General Page",
				"ClusterLabel", "Faculty Current Appt Tab", recruitmentID, 
				searchAuthIDString, logFileWriter);
		if (isRecruitmentDeptInFaculty(recruitmentID, searchAuthIDString, logFileWriter)){
			verifyFacultyInfoAgainstRecruitmentInfo("DeptLabel", "Recruitment General Page",
					"DeptSelect", "Faculty Current Appt Tab", recruitmentID, 
					searchAuthIDString, logFileWriter);
		}//end if - we can select the Recruitment department in the Faculty Department menu
		else{
			logFileWriter.write("Department in Recruitment is not selectable for Faculty member");
			logFileWriter.newLine();
		}//end else - we can't select the Recruitment department in the Faculty Department menu
		verifyFacultyInfoAgainstRecruitmentInfo("BilletNumberTextField", "Recruitment General Page",
				"BilletNumberTextField", "Faculty Current Appt Tab", recruitmentID, 
				searchAuthIDString, logFileWriter);
		verifyFacultyInfoAgainstRecruitmentInfo("FTEPctTextField", "Recruitment General Page",
				"FTEPctTextField", "Faculty Current Appt Tab", recruitmentID, 
				searchAuthIDString, logFileWriter);
		return recruitmentID;
	}//end testFacultyCreationFromRecruitment method
	
	protected boolean isRecruitmentDeptInFaculty(String recruitmentID, String searchAuthIDString, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("** Method isRecruitmentDeptInFaculty called with Recruitment ID '"+recruitmentID+"' and Search Authorization ID '"
				+searchAuthIDString+"' **");
		logFileWriter.newLine();
		switchToRecruitmentViaSearchAuth(searchAuthIDString, recruitmentID, verificationErrors, logFileWriter);
		String facultyDept = getValueFromRecruitment("DeptLabel", "Recruitment General Page", recruitmentID, logFileWriter);
		logFileWriter.write("Department found for Faculty member is "+facultyDept);
		logFileWriter.newLine();
		switchToFacultyWindow(searchAuthIDString, recruitmentID, logFileWriter);
		switchToFacultyTab("Faculty Current Appt Tab", logFileWriter);
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Current Appt Tab", "DeptSelect", logFileWriter);
		return UIController.selectElementInMenu(by, facultyDept);
	}//end isRecruitmentDeptInFaculty method
	
	protected void verifyFacultyInfoAgainstRecruitmentInfo(String recruitWidgetName, String recruitTabName, 
			String facWidgetName, String facTabName,  
			String recruitmentID, String searchAuthIDString, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method verifyFacultyInfoAgainstRecruitmentInfo called with SA ID "+searchAuthIDString
				+", and Recruitment ID "+recruitmentID+" ***");
		logFileWriter.newLine();
		
		String actualText = getValueFromFaculty(facWidgetName, facTabName, searchAuthIDString, recruitmentID, logFileWriter);
		
		switchToRecruitmentViaSearchAuth(searchAuthIDString, recruitmentID, verificationErrors, logFileWriter);
		
		String expectedText = getValueFromRecruitment(recruitWidgetName, recruitTabName, recruitmentID, logFileWriter);
		if (recruitWidgetName.equalsIgnoreCase("ClusterTextField") && (expectedText==null || expectedText.isEmpty())){
			String message = "HANDSON-3473 is still an issue - no Cluster information is displayed in the Recruitment;\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
			return;
		}//end if
		else if (recruitWidgetName.equalsIgnoreCase("EffectiveDateTextField") &&(! expectedText.equalsIgnoreCase(actualText))){
			String message = "HANDSON-3474 is still an issue - the Effective Date of "+expectedText
					+" should not be different from the Start Date of "+actualText+";\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
			return;
		}//end if
		else compareExpectedAndActualValues(expectedText, actualText, recruitTabName, recruitWidgetName, 
				recruitmentID, logFileWriter);
	}//end verifyFacultyInfoAgainstRecruitmentInfo method
	
	protected void compareExpectedAndActualValues(String expectedText, String actualText, 
			String recruitTabName, String recruitWidgetName, String recruitmentID,
			BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("Pre-edited Expected Value is "+expectedText);
		logFileWriter.newLine();
		logFileWriter.write("Pre-edited Actual Value is "+actualText);
		logFileWriter.newLine();
		//if it's a dollar amount, reduce it to a simple String
		expectedText = expectedText.replace("$", "");
		expectedText = expectedText.replace(".00", "");
		expectedText = expectedText.replaceAll(",", "");
		actualText = actualText.replaceAll(",", "");
		actualText = actualText.replace("$", "");

		
		logFileWriter.write("Expected Value derived from "+recruitTabName+", "+recruitWidgetName+" is '"+expectedText+"';");
		logFileWriter.newLine();
		
		if (expectedText.equalsIgnoreCase(actualText)){
			logFileWriter.write(recruitWidgetName+" has value "+expectedText+", as expected;");
			logFileWriter.newLine();
		}//end if - everything is as it should be
		else{
			String message = "ERROR - "+recruitWidgetName+" for Recruitment ID "+recruitmentID+" should have value "+expectedText
					+", but it was actually "+actualText+";\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
		}//end else - we have an error
		
	}//end compareExpectedAndActualValues method
	
	protected String getValueFromFaculty(String facWidgetName, String facTabName, String searchAuthIDString, String recruitmentID, BufferedWriter logFileWriter) throws Exception{
		if (! switchToFacultyWindow(searchAuthIDString, recruitmentID, logFileWriter))
			return new String();
		
		switchToFacultyTab(facTabName, logFileWriter);

		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(facTabName, facWidgetName, logFileWriter);
		String actualText = new String();
		if (facWidgetName.contains("TextField") || facWidgetName.contains("Textfield")){
			actualText = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
		}//end if
		else if (facWidgetName.contains("Select")){
			actualText = HandSOnTestSuite.searchAuthorizationUI.getSelectedText(SearchAuthorizationUIController.myExcelReader, facTabName, 
					facWidgetName, logFileWriter);
		}//end else if
		else if (facWidgetName.contains("Label")){
			actualText = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		}//end else if
		else if (facWidgetName.contains("Link")){
			actualText = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		}//end else if
		logFileWriter.write("Actual Value derived from "+facTabName+", "+facWidgetName+" is "+actualText+";");
		logFileWriter.newLine();
		
		return actualText;
	}//end getValueFromFaculty method
	
	protected void switchToFacultyTab(String facTabName, BufferedWriter logFileWriter) throws Exception{
		String tabLinkName = facTabName.replaceFirst("Faculty ", "");
		tabLinkName = tabLinkName.replaceAll("\\s", "")+"Link";
		
		logFileWriter.write("Deriving a tab link name of "+tabLinkName+" from the Faculty Tab Name "+facTabName);
		logFileWriter.newLine();
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Current Appt Tab", tabLinkName, logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(4000);
	}//end switchToFacultyTab method
	
	protected boolean switchToFacultyWindow(String searchAuthIDString, String recruitmentID, BufferedWriter logFileWriter) throws Exception{
		String IDsubstring = "HS_LastName"+searchAuthIDString+"_2";
		if (! UIController.switchWindowByURL(UIController.existingFacultyURL+IDsubstring, logFileWriter)){
			String message = "ERROR - Could not switch to New Faculty window for Recruitment ID "+recruitmentID+";\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
			System.out.println(message);
			return false;
		}//end if		
		else return true;
	}//end switchToFacultyWindow method
	
	protected String getValueFromRecruitment(String recruitWidgetName, String recruitTabName, String recruitmentID, 
			BufferedWriter logFileWriter) throws Exception{
		if (recruitTabName.contains("Costs")){
			By tabBy = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "CostsTabLink", logFileWriter);
			UIController.waitAndClick(tabBy);
			Thread.sleep(3000);
		}
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(recruitTabName, recruitWidgetName, logFileWriter);
		String expectedText = new String();
		if (recruitWidgetName.contains("TextField") || recruitWidgetName.contains("Textfield")){
			expectedText = UIController.driver.findElement(by).toString();
			System.out.println("total toString dump of "+recruitWidgetName+" is "+expectedText);
			expectedText = //HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
					UIController.driver.findElement(by).getAttribute("value");
			System.out.println("value of "+recruitWidgetName+" is "+expectedText);
		}
		else if (recruitWidgetName.contains("Dropdown")||(recruitWidgetName.contains("Select"))){
			expectedText = HandSOnTestSuite.searchAuthorizationUI.getSelectedText(recruitWidgetName, logFileWriter);
		}
		else if (recruitWidgetName.contains("Label")){
			expectedText = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		}
		if (expectedText == null) expectedText = new String();
		
		return expectedText;
	}//end getValueFromRecruitment method
	
	protected static void activateNewFacultyWindow(String searchAuthIDString, String recruitmentID, BufferedWriter logFileWriter) throws Exception{
		String newFacultyURL = UIController.newFacultyURL+recruitmentID;
		String newFacultyLastName = "HS_LastName"+searchAuthIDString;
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "DeptLabel", logFileWriter);
		String department = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "CreateFacultyRecordButton", logFileWriter);
		Thread.sleep(3000);
		UIController.waitAndClick(by);
		Thread.sleep(5000);
		UIController.switchWindowByURL(newFacultyURL, logFileWriter);
		//if the new Faculty lacks a department, select a department
		ensureDepartmentIsSelected(department, logFileWriter);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Current Appt Tab", "SaveButton", logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(3000);
		UIController.driver.switchTo().alert().accept();
		Thread.sleep(3000);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Current Appt Tab", "CloseButton", logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(3000);
		if (! UIController.switchWindowByURL(UIController.notificationDialogURL, logFileWriter)) {
			logFileWriter.newLine();
			logFileWriter.write("**** WARNING: Could not switch to the notification dialog initially after trying to close Faculty Record ***");
			logFileWriter.newLine();
			logFileWriter.write("Now making a second attempt to close the Faculty Record and bring up the notification dialog");
			logFileWriter.newLine();
			logFileWriter.newLine();
			if (! UIController.switchWindowByURL(UIController.existingFacultyURL+newFacultyLastName, logFileWriter)) {
				logFileWriter.write("**** ERROR: Could not switch to the existing Faculty Record with last name "+newFacultyLastName+" ***");
				logFileWriter.newLine();
				fail ("Could not find the Faculty Record with last name "+newFacultyLastName+" that was just saved in the 'activateNewFacultyWindow' method");
			}
			by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Current Appt Tab", "CloseButton", logFileWriter);
			UIController.scrollElementIntoView(by, logFileWriter);
			Thread.sleep(2000);
			UIController.waitAndClick(by);
		}//end if
		Thread.sleep(3000);
		UIController.driver.findElement(By.id("mybutton")).click();
		Thread.sleep(3000);
		switchToRecruitmentViaSearchAuth(searchAuthIDString, recruitmentID, verificationErrors, logFileWriter);
		Thread.sleep(3000);
		UIController.driver.navigate().refresh();
		Thread.sleep(3000);
		//UIController.driver.switchTo().alert().accept();
		//Thread.sleep(3000);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment Costs Page", "GeneralTabLink", logFileWriter);
		UIController.driver.findElement(by).click();
		Thread.sleep(5000);
		by = By.linkText("FirstName"+searchAuthIDString+"_2 LastName"+searchAuthIDString+"_2");
		UIController.driver.findElement(by).click();
		Thread.sleep(5000);
		UIController.switchWindowByURL(UIController.existingFacultyURL+"HS_LastName"+searchAuthIDString, logFileWriter);
	}//end switchToNewFacultyWindow method
	
	/*
	 * This method will make sure that a department is selected if a department is 
	 * not already selected for the new faculty member
	 */
	protected static void ensureDepartmentIsSelected(String department, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("Ensuring that a department is selected for the new Faculty member - department in Recruitment is "+department);
		logFileWriter.newLine();
		
		String currentDepartment = HandSOnTestSuite.searchAuthorizationUI.getSelectedText(SearchAuthorizationUIController.myExcelReader, "Faculty Current Appt Tab", "DeptSelect", logFileWriter);
		
		if (currentDepartment.equalsIgnoreCase(department)){
			logFileWriter.write("Department is correctly selected already, no need to adjust it");
			logFileWriter.newLine();
			return;
		}//end if
		else{
			By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Current Appt Tab", "DeptSelect", logFileWriter);
			String newDepartment = "Mathematics";
			if (department.equalsIgnoreCase("Iranian Studies"))
				newDepartment = "E. Asian Cultures & Languages";
			else if (department.equalsIgnoreCase("BioPhysics"))
				newDepartment = "Biology";
			else if (department.equalsIgnoreCase("Advancement Women's Leadership"))
				newDepartment = "Sociology";
			UIController.selectElementInMenu(by, newDepartment);
			Thread.sleep(2000);
			logFileWriter.write("New Department selected is "+newDepartment);
			logFileWriter.newLine();
		}//end else
	}//end ensureDepartmentIsSelected method
	
	protected static void switchToRecruitmentViaSearchAuth(String searchAuthIDString, String recruitmentID, 
			StringBuffer verificationErrors, BufferedWriter logFileWriter) throws Exception{ 
		//first, switch to the Search Authorization window
		if (switchToSearchAuthWindow(searchAuthIDString, verificationErrors, logFileWriter)){
			logFileWriter.write("Successfully acquired the window for SA ID "+searchAuthIDString);
			logFileWriter.newLine();
		}//end if - we got the search authorization window
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SA Costs Page", "GeneralTabLink", logFileWriter);
		UIController.waitAndClick(by);
		
		String returnedRecruitmentID = switchToRecruitmentWindow(searchAuthIDString, "Primary", "switchToRecruitmentViaSearchAuth", 
				verificationErrors, logFileWriter);
		
		if (! returnedRecruitmentID.equalsIgnoreCase(recruitmentID)){
			String message = "ERROR - Could not acquire the correct Recruitment ID - aborting method testFacultyCreationFromRecruitment;\n";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
			return;
		}//end if - got a problem
		else{
			logFileWriter.write("Got back to the window for Recruitment ID "+recruitmentID+", as expected");
			logFileWriter.newLine();
		}//end else - no problem
	}//end switchToRecruitmentViaSearchAuth method
	
	protected void testBackupRecruitmentStatus(String searchAuthIDString, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method testBackupRecruitmentStatus called with SA ID "+searchAuthIDString+" ***");
		logFileWriter.newLine();

		String recruitmentID = switchToRecruitmentWindow (searchAuthIDString, "Backup", "testBackupRecruitmentStatus", 
				verificationErrors, logFileWriter);
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "OfferStatusCode", logFileWriter);
		String actualText = new String();
		try{
			new Select(UIController.driver.findElement(by)).selectByVisibleText("Accept");
			String message = "ERROR - Offer Status Dropdown was unexpectedly editable for Backup Recruitment ID"+recruitmentID+";\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
		}
		catch(Exception e){
			String message = "Offer Status Dropdown was not editable, as expected for Backup Recruitment ID "+recruitmentID+";\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
		}
		String fieldUnderTest = "Offer Status";
		String expectedText = "No Offer";
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "OfferStatusCode", logFileWriter);
		actualText = new Select(UIController.driver.findElement(by)).getFirstSelectedOption().getText();
				
		isStringValueAsExpected(fieldUnderTest, expectedText, actualText, logFileWriter);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "CloseButton", logFileWriter);
		UIController.waitAndClick(by);
		
		HandSOnTestSuite.searchAuthorizationUI.switchToSearchAuthWindow(searchAuthIDString, logFileWriter);

	}//end testBackupRecruitmentStatus method
	
	protected void testUpdatedSearchAuthStatus(String searchAuthIDString, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method testUpdatedSearchAuthStatus called with SA ID "+searchAuthIDString+" ***");
		logFileWriter.newLine();
		
		//first, switch to the Search Authorization tab and get the Primary Recruitment
		HandSOnTestSuite.searchAuthorizationUI.switchToSearchAuthWindow(searchAuthIDString, logFileWriter);
		Thread.sleep(5000);
		UIController.driver.navigate().refresh();
		Thread.sleep(5000);
		
		try{
			UIController.driver.switchTo().alert().accept();
			logFileWriter.write("Had to close alert in method testOfferStatus");
			logFileWriter.newLine();
		}
		catch(Exception e){
			logFileWriter.write("Could not close alert in method testUpdatedSearchAuthStatus because of Exception "+e.getMessage());
			logFileWriter.newLine();
		}
		
		//first, click on the "General" tab
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SA Costs Page", "GeneralTabLink", logFileWriter);

		try{
			UIController.waitForElementPresent(by);
			UIController.driver.findElement(by).click();
		}
		catch(Exception e){
			logFileWriter.write("Couldn't get to the General tab because of Exception "+e.getMessage());
			logFileWriter.newLine();
		}
		Thread.sleep(3000);
		
		String fieldUnderTest = "Search Authorization Status";
		String actualText = HandSOnTestSuite.searchAuthorizationUI.getSelectedText("SearchStatusSelect", logFileWriter);
		String expectedText = "Accept";
		isStringValueAsExpected(fieldUnderTest, expectedText, actualText, logFileWriter);
		
		fieldUnderTest = "Search Authorization Status Text";
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SA General Page", "SearchStatusText", logFileWriter);
		actualText = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		expectedText = "Inactive";
		isStringValueAsExpected(fieldUnderTest, expectedText, actualText, logFileWriter);
		
		fieldUnderTest = "First Recruitment Type";
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SA General Page", "FirstRecruitmentType", logFileWriter);
		actualText = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		expectedText = "Primary";
		isStringValueAsExpected(fieldUnderTest, expectedText, actualText, logFileWriter);
		
		fieldUnderTest = "First Recruitment Offer Status";
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SA General Page", "FirstRecruitmentOfferStatus", logFileWriter);
		actualText = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		expectedText = "Accepted";
		isStringValueAsExpected(fieldUnderTest, expectedText, actualText, logFileWriter);
		
		fieldUnderTest = "Second Recruitment Type";
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SA General Page", "SecondRecruitmentType", logFileWriter);
		actualText = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		expectedText = "Backup";
		isStringValueAsExpected(fieldUnderTest, expectedText, actualText, logFileWriter);
		
		fieldUnderTest = "Second Recruitment Offer Status";
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SA General Page", "SecondRecruitmentOfferStatus", logFileWriter);
		actualText = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		expectedText = "No Offer";
		isStringValueAsExpected(fieldUnderTest, expectedText, actualText, logFileWriter);
		
	}//end testUpdatedSearchAuthStatus method
	
	protected void isStringValueAsExpected(String fieldUnderTest, String expectedText, String actualText, 
			BufferedWriter logFileWriter) throws Exception{
		if (actualText.equalsIgnoreCase(expectedText)){
			logFileWriter.write(fieldUnderTest + " is as expected: "+expectedText);
			logFileWriter.newLine();
			logFileWriter.write("Test of "+fieldUnderTest+" passes");
			logFileWriter.newLine();
		}//end if - it is as it should be
		else{
			String message = "ERROR - expected "+fieldUnderTest+" is "+expectedText+", but actual is "+actualText+";\n ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			verificationErrors.append(message);
			logFileWriter.newLine();
			logFileWriter.write("**********************************");
			logFileWriter.write("Test of "+fieldUnderTest+" FAILS");
			logFileWriter.newLine();
			logFileWriter.write("**********************************");
			logFileWriter.newLine();
			logFileWriter.newLine();
		}//end else - this is an error condition
	}//end isStringValueAsExpected method
	
	protected void testOfferStatus(String searchAuthIDString, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method testOfferStatus called with SA ID "+searchAuthIDString+" ***");
		logFileWriter.newLine();
		
		//first, switch to the Search Authorization tab and get the Primary Recruitment
		HandSOnTestSuite.searchAuthorizationUI.switchToSearchAuthWindow(searchAuthIDString, logFileWriter);
		Thread.sleep(5000);
		UIController.driver.navigate().refresh();
		Thread.sleep(5000);
		
		try{
			UIController.driver.switchTo().alert().accept();
			logFileWriter.write("Had to close alert in method testOfferStatus");
			logFileWriter.newLine();
		}
		catch(Exception e){
			logFileWriter.write("Could not close alert in method testOfferStatus because of Exception "+e.getMessage());
			logFileWriter.newLine();
		}
		//first, click on the "General" tab
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SA Costs Page", "GeneralTabLink", logFileWriter);

		try{
			UIController.waitForElementPresent(by);
			UIController.driver.findElement(by).click();
		}
		catch(Exception e){
			logFileWriter.write("Couldn't get to the General tab because of Exception "+e.getMessage());
			logFileWriter.newLine();
		}
		try{
			UIController.driver.navigate().refresh();
			UIController.driver.switchTo().alert().accept();
			logFileWriter.write("Second attempt to refresh screen and close alert in method testOfferStatus");
			logFileWriter.newLine();
		}
		catch(Exception e){
			logFileWriter.write("Could not close alert in method testOfferStatus because of Exception "+e.getMessage());
			logFileWriter.newLine();
		}

		Thread.sleep(3000);

		String recruitmentID = switchToRecruitmentWindow (searchAuthIDString, "Primary", "testOfferStatus", 
				verificationErrors, logFileWriter);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment Costs Page", "GeneralTabLink", logFileWriter);
		UIController.waitAndClick(by);
		
		
		By printBy = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "PrintRecordButton", logFileWriter);
		By createFacultyBy = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "CreateFacultyRecordButton", logFileWriter);
		By createCommitmentBy = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "CreateCommitmentButton", logFileWriter);
		By searchStatusBy = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "SearchStatusLabel", logFileWriter);
		String searchStatus = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(searchStatusBy, logFileWriter);

		setOfferStatus("Pending", "Open", logFileWriter);
		Thread.sleep(3000);
		testIfPresent(printBy, "Print Button", recruitmentID, verificationErrors, true, logFileWriter);
		testIfPresent(createFacultyBy, "Create Faculty Button", recruitmentID, verificationErrors, false, logFileWriter);
		testIfPresent(createCommitmentBy, "Create Commitment Button", recruitmentID, verificationErrors, false, logFileWriter);
		if (searchStatus.equalsIgnoreCase("Active")){
			logFileWriter.write("Search Status is Active, as expected");
		}
		else{
			String message = "ERROR - Search Status should have been Active, but is actually "+searchStatus+";\n ";
			logFileWriter.write(message);
			verificationErrors.append(message);
		}
		logFileWriter.newLine();
		
		setOfferStatus("Accepted", "Closed", logFileWriter);
		Thread.sleep(3000);
		searchStatus = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(searchStatusBy, logFileWriter);
		testIfPresent(printBy, "Print Button", recruitmentID, verificationErrors, true, logFileWriter);
		testIfPresent(createFacultyBy, "Create Faculty Button", recruitmentID, verificationErrors, true, logFileWriter);
		testIfPresent(createCommitmentBy, "Create Commitment Button", recruitmentID, verificationErrors, true, logFileWriter);

		if (searchStatus.equalsIgnoreCase("Inactive")){
			logFileWriter.write("Search Status is Inactive, as expected");
		}
		else{
			String message = "ERROR - Search Status should have been Inactive, but is actually "+searchStatus+";\n ";
			logFileWriter.write(message);
			verificationErrors.append(message);
		}
		logFileWriter.newLine();
	}//end testOfferStatus method
	
	/*
	 * This method assumes that we have switched to the Search Authorization window already
	 * and we're already on the General tab.
	 */	
	protected static String switchToRecruitmentWindow (String searchAuthIDString, String primaryOrBackup, String methodName, 
			StringBuffer verificationErrors, BufferedWriter logFileWriter) throws Exception{
		String recruitmentID = new String();
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SA General Page", "FirstRecruitmentLink", logFileWriter);
		String linkName = new String();
		if (primaryOrBackup.equalsIgnoreCase("Primary"))
			linkName = "FirstRecruitmentLink";
		else if (primaryOrBackup.equalsIgnoreCase("Backup"))
			linkName = "SecondRecruitmentLink";
		else{
			logFileWriter.write("ERROR - parameter must be either Primary or Backup, not "+primaryOrBackup);
			logFileWriter.newLine();
			return new String();
		}

		try{
			//recruitmentID = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
			recruitmentID = UpdateSearchAuthorizationTest.getRecruitmentID(linkName, logFileWriter);
			logFileWriter.write(primaryOrBackup+" Recruitment of SA ID "+searchAuthIDString+" has Recruitment ID '"+recruitmentID+"';");
			logFileWriter.newLine();
		}
		catch(Exception e){
			logFileWriter.write("Couldn't get the text value from the "+primaryOrBackup+" Recruitment ID because of Exception "+e.getMessage());
			logFileWriter.newLine();
			logFileWriter.write("*** WARNING!! Aborting method "+methodName+" called with SA ID "+searchAuthIDString+" ***");
			logFileWriter.newLine();
			return new String();
		}
		
		String switched = HandSOnTestSuite.searchAuthorizationUI.switchToRecruitmentWindow(recruitmentID, logFileWriter);
		if (! switched.isEmpty()){
			logFileWriter.write("Could not switch to the window with Recruitment ID "+recruitmentID);
			logFileWriter.write(", trying to open this window");
			logFileWriter.newLine();
			Thread.sleep(3000);

			switchToSearchAuthWindow(searchAuthIDString, verificationErrors, logFileWriter);
			Thread.sleep(3000);
			try {
				UIController.driver.navigate().refresh();
				Thread.sleep(10000);
				UIController.driver.switchTo().alert().accept();
				Thread.sleep(3000);
			}//end try clause
			catch(Exception e) {
				logFileWriter.write("Could not refresh because of Exception "+e.getMessage());
				logFileWriter.newLine();
				logFileWriter.write("Re-trying....");
				logFileWriter.newLine();
				try {
					UIController.driver.navigate().refresh();
					Thread.sleep(10000);
					UIController.driver.switchTo().alert().accept();
					Thread.sleep(3000);
				}//end try clause
				catch(Exception e2) {
					logFileWriter.write("Could not refresh on the second attempt because of Exception "+e2.getMessage());
					logFileWriter.newLine();
				}//end inner catch clause
			}//end outer catch clause
			by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SA Costs Page", "GeneralTabLink", logFileWriter);
			UIController.waitAndClick(by);
			Thread.sleep(4000);

			by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SA General Page", linkName, logFileWriter);
			UIController.waitAndClick(by);
			Thread.sleep(5000);
			switched = HandSOnTestSuite.searchAuthorizationUI.switchToRecruitmentWindow(recruitmentID, logFileWriter);
			if (! switched.isEmpty()){
				String message = "ERROR - could not switch to the window for Recruitment ID "+recruitmentID+";\n ";
				verificationErrors.append(message);
				logFileWriter.write(message);
				logFileWriter.newLine();
				System.out.println(message);
			}//end if
			else{
				logFileWriter.write("Successfully switched to the window for Recruitment ID "+recruitmentID+", the second time worked");
				logFileWriter.newLine();
			}//end else
		}//end if
		else{
			logFileWriter.write("Successfully switched to the window with Recruitment ID "+recruitmentID);
			logFileWriter.newLine();
		}//end else
		return recruitmentID;
	}//end switchToRecruitmentWindow method
	
	protected void setOfferStatus(String offerStatus, String expectedRecruitmentStatus, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("* method setOfferStatus called with Offer Status "+offerStatus+" and Recruitment Status "+expectedRecruitmentStatus+" *");
		logFileWriter.newLine();
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "OfferStatusCode", logFileWriter);
		UIController.waitAndSelectElementInMenu(by, offerStatus);	
		Thread.sleep(3000);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "SaveButton", logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(3000);
			
		try{
			UIController.driver.switchTo().alert().accept();
			logFileWriter.write("Alert was accepted, as expected");
		}
		catch(Exception e){
			logFileWriter.write("Could not close the alert because of "+e.getMessage());
		}
		logFileWriter.newLine();
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "RecruitmentStatusLabel", logFileWriter);
		String recruitmentStatus = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		if (recruitmentStatus.equalsIgnoreCase(expectedRecruitmentStatus)){
			logFileWriter.write("Recruitment Status remains "+ expectedRecruitmentStatus +", as expected;\n");
		}
		else{
			String message = "ERROR - Recruitment Status should be "+ expectedRecruitmentStatus +", but it is "+recruitmentStatus+";\n";
			verificationErrors.append(message);
			logFileWriter.write(message);
		}//end else
		logFileWriter.newLine();

	}//end setOfferStatus method
	
	public static void testIfPresent(By by, String label, String recruitmentID, StringBuffer verificationErrors, 
			boolean shouldBePresent, BufferedWriter logFileWriter) throws Exception{

		if (UIController.isElementPresent(by)){
			if (shouldBePresent){
				logFileWriter.write(label+" is present, as expected for Recruitment ID "+recruitmentID);
				logFileWriter.newLine();
			}//it's present as it should be
			else{
				String message = "ERROR - "+label+" is unexpectedly present for Recruitment ID "+recruitmentID+";\n ";
				verificationErrors.append(message);
				logFileWriter.write(message);
				logFileWriter.newLine();
			}//it's present but it shouldn't be
		}//it's present
		else{
			if (shouldBePresent){
				String message = "ERROR - "+label+" is not present for Recruitment ID "+recruitmentID+";\n ";
				verificationErrors.append(message);
				logFileWriter.write(message);
				logFileWriter.newLine();
			}//it's not present, but it should be
			else{
				logFileWriter.write(label+" is not present, as expected, for Recruitment ID "+recruitmentID);
				logFileWriter.newLine();
			}//end else - it's not present, as expected
		}//it's not present
	}//end testIfPresent method
	
	protected void performChangedDataScenario(String searchAuthIDString, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method performChangedDataScenario called with SA ID "+searchAuthIDString +" ***");
		logFileWriter.newLine();
		System.out.println("*** method performChangedDataScenario called with SA ID "+searchAuthIDString +" ***");
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SA General Page", "FirstRecruitmentLink", logFileWriter);
		if (UIController.waitAndClick(by)){
			logFileWriter.write("Now clicking on the Primary Recruitment Link");
			logFileWriter.newLine();
		}//end if
		String recruitmentID = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);

		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SA General Page", "CostsTabLink", logFileWriter);
		UIController.waitAndClick(by);
		//make sure that at least one element is present before refreshing the page
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SA Costs Page", "SalaryCostAmt", logFileWriter);
		UIController.waitForElementPresent(by);
		UIController.driver.navigate().refresh();
		Thread.sleep(4000);
		UIController.dismissAlert(logFileWriter);
		Thread.sleep(3000);
		HashMap<String, Integer> originalCosts 
			= HandSOnTestSuite.searchAuthorizationUI.getActualCosts(logFileWriter);	
		
		if (UpdateSearchAuthorizationTest.switchToRecruitmentWindow(recruitmentID, verificationErrors, logFileWriter)){
			logFileWriter.write("Successfully switched back to the Recruitment window for Recruitment ID "+recruitmentID);
			logFileWriter.newLine();
		}//end if
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "CostsTabLink", logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(4000);

		HashMap<String, Integer> updatedCosts 
			= UpdateSearchAuthorizationTest.updateSearchAuthCosts(originalCosts, "recId="+recruitmentID, logFileWriter);	
		UpdateSearchAuthorizationTest.compareUnchangedRecruitmentAttributes(recruitmentID, originalCosts, verificationErrors, logFileWriter);

		//Now, click on the ResetToSearchAuthCostsButton to reset the costs - dismiss the reset the first time
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment Costs Page", "ResetToSearchAuthCostsButton", logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(3000);
		UIController.driver.switchTo().alert().dismiss();
		Thread.sleep(3000);
		UpdateSearchAuthorizationTest.compareUnchangedRecruitmentAttributes(recruitmentID, originalCosts, verificationErrors, logFileWriter);
		
		//Now, click on the ResetToSearchAuthCostsButton to reset the costs - accept the reset the second time
		UpdateSearchAuthorizationTest.resetToSearchAuthCosts(recruitmentID, logFileWriter);
		UpdateSearchAuthorizationTest.compareChangedRecruitmentAttributes(recruitmentID, verificationErrors, originalCosts, logFileWriter);
		
		updatedCosts = UpdateSearchAuthorizationTest.updateSearchAuthCosts(originalCosts, "recId="+recruitmentID, logFileWriter);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment Costs Page", "SaveButton", logFileWriter);
		UIController.waitAndClick(by);
		//UpdateSearchAuthorizationTest.handleChangeAnnotationDialog("Cost Adjustment", verificationErrors, logFileWriter);
		try{
			UIController.driver.switchTo().alert().accept();
			logFileWriter.write("Successfully closed the alert dialog");
			logFileWriter.newLine();
		}//end try
		catch(Exception e){
			logFileWriter.write("Could not acquire the alert because of Exception "+e.getMessage());
		}
		logFileWriter.newLine();
		
	}//end performChangedDataScenario method
	
	protected void verifySearchAuthorizationData(String searchAuthIDString, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method verifySearchAuthorizationData called with SA ID "+searchAuthIDString+" ***");
		logFileWriter.newLine();
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SA Costs Page", "GeneralTabLink", logFileWriter);
		UIController.waitAndClick(by);
		
		//verify that the "Print Button" is present:
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SA General Page", "PrintRecordButton", logFileWriter);
		if (UIController.isElementPresent(by)){
			logFileWriter.write("Print Button is present, as expected");
			logFileWriter.newLine();
		}//end if

		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SA General Page", "FirstRecruitmentLink", logFileWriter);
		String primaryRecruitmentText = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by);
		int primaryRecruitmentID = 0;
		try{
			primaryRecruitmentID = Integer.parseInt(primaryRecruitmentText);
			logFileWriter.write("Primary Recruitment ID is "+primaryRecruitmentID);
		}
		catch(Exception e){
			logFileWriter.write("Could not convert '"+primaryRecruitmentText+"' to an int value for primary Recruitment ID");
		}
		logFileWriter.newLine();
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SA General Page", "SecondRecruitmentLink", logFileWriter);
		String backupRecruitmentText = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by);
		int backupRecruitmentID = 0;
		try{
			backupRecruitmentID = Integer.parseInt(backupRecruitmentText);
			logFileWriter.write("Backup Recruitment ID is "+backupRecruitmentID);
		}//end try clause
		catch(Exception e){
			logFileWriter.write("Could not convert '"+backupRecruitmentText+"' to an int value for backup Recruitment ID");
		}//end catch clause
		logFileWriter.newLine();
		
		if ((primaryRecruitmentID - backupRecruitmentID) == 1){
			logFileWriter.write("Primary Recruitment and Backup Recruitment are reversed, as expected");
			logFileWriter.newLine();
		}//end if
		else{
			String message = "Primary Recruitment ID '"+primaryRecruitmentText
					+"' and Backup Recruitment ID '"+backupRecruitmentText+"' are not reversed, as expected";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message+";\n");
		}//end else
	}//end verifySearchAuthorizationData method
	
	protected void switchPrimaryRecruitmentScenario(String searchAuthIDString, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** switchPrimaryRecruitmentScenario method called with Search Authorization ID "+searchAuthIDString+" ***");
		logFileWriter.newLine();
		
		String searchAuthWindowURL = UIController.newRecruitmentURL+"?searchAuthId="+searchAuthIDString;
		
		confirmCorrectWindow(searchAuthWindowURL, searchAuthIDString, logFileWriter);
		
		doPrimaryRecruitmentSwitch(searchAuthIDString, true, logFileWriter);
		
		String primaryRecruitmentID = switchToPrimaryRecruitmentWindow(searchAuthIDString, logFileWriter);

		verifyPrimaryRecruitment(primaryRecruitmentID, logFileWriter);
		
		if (! UIController.switchWindowByURL(searchAuthWindowURL, logFileWriter)){
			logFileWriter.write("Could not switch to the Backup Recruitment window.");
			logFileWriter.newLine();
		}//end if
		
		//
		doPrimaryRecruitmentSwitch(searchAuthIDString, false, logFileWriter);

	}//end switchPrimaryRecruitmentScenario method
	
	protected void verifyPrimaryRecruitment(String primaryRecruitmentID, BufferedWriter logFileWriter) throws Exception{
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", 
				"PrimaryRecruitmentTypeRadioButton", logFileWriter);
		String value = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
		if (value.equalsIgnoreCase("active")){
			logFileWriter.write("Primary Radio button is still checked, as expected");
			logFileWriter.newLine();
		}//end if - the primary radio button is still checked
		else{
			String message = "The Primary Radio button is not checked, as it should be, for Recruitment ID "+primaryRecruitmentID+"; ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
		}//end else
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "CloseButton", logFileWriter);
		UIController.waitAndClick(by);
	}//end verifyPrimaryRecruitment method
	
	protected String switchToPrimaryRecruitmentWindow(String searchAuthIDString, BufferedWriter logFileWriter) throws Exception{
		switchToSearchAuthWindow(searchAuthIDString, verificationErrors, logFileWriter);
		Thread.sleep(3000);
		UIController.driver.navigate().refresh();
		Thread.sleep(4000);
		try{
			UIController.driver.switchTo().alert().accept();
			logFileWriter.write("Successfully dismissed alert after refreshing Search Authorization Page - SA ID "+searchAuthIDString);
			logFileWriter.newLine();
		}//end try
		catch(Exception e){
			logFileWriter.write("Could not dismiss alert because of Exception "+e.getMessage());
			logFileWriter.newLine();
		}//end catch
		Thread.sleep(3000);
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SA Costs Page", "GeneralTabLink", logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(4000);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SA General Page", "FirstRecruitmentLink", logFileWriter);
		UIController.waitForElementPresent(by);
		
		String primaryRecruitmentID = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		UIController.waitAndClick(by);
		UIController.switchWindowByURL(UIController.newRecruitmentURL+"?recId="+primaryRecruitmentID, logFileWriter);
		
		return primaryRecruitmentID;
	}//end switchToPrimaryRecruitmentWindow method
	
	protected void doPrimaryRecruitmentSwitch(String searchAuthIDString, boolean isAbortive, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method doPrimaryRecruitmentSwitch called with SA ID "+searchAuthIDString+" ***");
		logFileWriter.newLine();
		
		UIController.driver.findElement(By.name("recruitmentDTO.candiateStatus")).click();
		Thread.sleep(3000);
		Alert warningDialog = null;
		try{
			warningDialog = UIController.driver.switchTo().alert();
			logFileWriter.write("Successfully acquired alert");
		}
		catch(Exception e){
			String message = "ERROR - Could not acquire the expected warning dialog";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
		}
		logFileWriter.newLine();
		String expectedDialogText1 = "A primary recruitment record is already associated with this search authorization.";
		String expectedDialogText2 =   "Do you want to make this the primary recruitment record?";
		String dialogText = new String();
		if (warningDialog != null)
			dialogText = warningDialog.getText();
		logFileWriter.write("The text on the dialog is as follows: ");
		logFileWriter.newLine();
		logFileWriter.write(dialogText);
		logFileWriter.newLine();
		if (dialogText.contains(expectedDialogText1) && (dialogText.contains(expectedDialogText2))){
			logFileWriter.write("The dialog text is as expected");
			logFileWriter.newLine();
		}//end if
		else{
			String message = "The dialog text is not as expected for the new Backup Recruitment for SA ID "+searchAuthIDString+"; ";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
		}//end else
		logFileWriter.newLine();
		if (isAbortive)
			warningDialog.dismiss();
		else
			warningDialog.accept();
	}//end doPrimaryRecruitmentSwitch method
	
	protected void confirmCorrectWindow(String URL, String ID, BufferedWriter logFileWriter) throws Exception{
		if (UIController.driver.getWindowHandle().contains(URL)){
			logFileWriter.write("We are in the new Backup Recruitment window for Search Authorization "+ID+", as expected");
			logFileWriter.newLine();
		}//end if - we're in the window of the new Backup Recruitment, as we need to be
		else{
			if (UIController.switchWindowByURL(URL, logFileWriter)){
				logFileWriter.write("...and we've switched to the window of the new Backup Recruitment for SA "+ID);
				logFileWriter.newLine();
			}//end if - we've successfully switched to the new Backup Recruitment Window, we can continue this test
			else{
				logFileWriter.write("We couldn't get to the window of the new Backup Recruitment for SA "+ID +"; returning....");
				logFileWriter.newLine();
				return;
			}//end else - we can't get to the new Backup Recruitment Window, so we can't continue this test
		}//end else - we didn't start the test at the window for the new Backup Recruitment that we need to be in
	}//end confirmCorrectWindow method
	
	protected void verifyBasicRecruitmentInfo(String searchAuthIDString, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method verifyBasicRecruitmentInfo called with SA ID "+searchAuthIDString+" ***");
		logFileWriter.newLine();
		switchToSearchAuthWindow(searchAuthIDString, verificationErrors, logFileWriter);
		Thread.sleep(3000);
		UIController.driver.navigate().refresh();
		Thread.sleep(6000);
		try{
			UIController.driver.switchTo().alert().accept();
		}//end try
		catch(Exception e){
			logFileWriter.write("Could not accept the alert because of "+e.getMessage());
			logFileWriter.newLine();
		}//end catch
		Thread.sleep(3000);
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SA Costs Page", "GeneralTabLink", logFileWriter);
		if (! UIController.waitAndClick(by)){
			logFileWriter.write("Couldn't click on the General Tab");
			logFileWriter.newLine();
		}//end if
		String recruitmentID = UpdateSearchAuthorizationTest.getRecruitmentID("FirstRecruitmentLink", logFileWriter);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SA General Page", "CostsTabLink", logFileWriter);
		UIController.driver.findElement(by).click();
		HashMap<String, Integer> searchAuthCosts 
			= HandSOnTestSuite.searchAuthorizationUI.getActualCosts(logFileWriter);
		UpdateSearchAuthorizationTest.switchToRecruitmentWindow(recruitmentID, verificationErrors, logFileWriter);
		Thread.sleep(3000);
		UIController.driver.navigate().refresh();
		Thread.sleep(3000);
		UIController.driver.switchTo().alert().accept();
		Thread.sleep(3000);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "RecruitmentIDLabel", logFileWriter);
		String actualRecruitmentID = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		if (recruitmentID.equalsIgnoreCase(actualRecruitmentID)){
			logFileWriter.write("Recruitment ID "+recruitmentID+" is displayed in the Recruitment, as expected");
			logFileWriter.newLine();
		}//end if
		else{
			String message = "ERROR - Recruitment ID "+recruitmentID+" is not displayed in the Recruitment, as expected"
					+", but what is displayed is "+actualRecruitmentID+"; ";
			verificationErrors.append(message);
			logFileWriter.write(message);
			logFileWriter.newLine();
		}//end else method
		//verify that the costs are the same
		UpdateSearchAuthorizationTest.iterateCosts("Original Costs, called from verifyBasicRecruitmentInfo method", searchAuthCosts, logFileWriter);
		UpdateSearchAuthorizationTest.compareChangedRecruitmentAttributes(recruitmentID, verificationErrors, searchAuthCosts, logFileWriter);
		
		//verify that the data and attachments are the same
		compareIncidentalData(searchAuthIDString, recruitmentID, logFileWriter);
		
		Thread.sleep(3000);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment Costs Page", 
				"CloseButton", logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(3000);
		if (switchToSearchAuthWindow(searchAuthIDString, verificationErrors, logFileWriter)){
			logFileWriter.write("Successfully switched back to window with Search Authorization ID "+searchAuthIDString);
			logFileWriter.newLine();
		}//end if
		else{
			logFileWriter.write("Could not switch back to the tab with Search Authorization ID "+searchAuthIDString);
			logFileWriter.newLine();
		}//end else
		Thread.sleep(3000);
		UIController.driver.navigate().refresh();
		Thread.sleep(3000);
		UIController.driver.switchTo().alert().accept();
		Thread.sleep(3000);
	}//end verifyBasicRecruitmentInfo method
	
	protected void compareIncidentalData(String searchAuthIDString, String recruitmentID, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method compareIncidentalData called with SA ID "+searchAuthIDString+" ***");
		logFileWriter.newLine();
		switchToSearchAuthWindow(searchAuthIDString, verificationErrors, logFileWriter);
		Thread.sleep(3000);
		UIController.driver.navigate().refresh();
		Thread.sleep(4000);
		UIController.driver.switchTo().alert().accept();
		Thread.sleep(3000);
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "CostsTabLink", logFileWriter);
		UIController.waitAndClick(by);
		int numUploads[] = new int[2];
		numUploads[0] = HandSOnTestSuite.searchAuthorizationUI.getBottomRow("//tr/td[3]/span/pre", new String(), logFileWriter, true);
		logFileWriter.write("The number of uploads for the SA is "+numUploads[0]);
		logFileWriter.newLine();
		
		String[] SA_textValues = new String[5];
		SA_textValues[0] = getStringValueFromIncidentalData("Recruitment Costs Page", "AttachmentFirstRowText", logFileWriter);
		SA_textValues[1] = getStringValueFromIncidentalData("Recruitment Costs Page", "NoteFirstRowText", logFileWriter);
		SA_textValues[2] = getStringValueFromIncidentalData("Recruitment Costs Page", "DescriptionTextArea", logFileWriter);
		SA_textValues[3] = getStringValueFromIncidentalData("Recruitment Costs Page", "DataSourceTextArea", logFileWriter);
		SA_textValues[4] = getStringValueFromIncidentalData("Recruitment Costs Page", "DataSourceDateTextField", logFileWriter);
		
		if (UIController.switchWindowByURL(UIController.newRecruitmentURL+"?recId="+recruitmentID, logFileWriter)){
			logFileWriter.write("Successfully switched back to the Recruitment window");
			logFileWriter.newLine();
		}
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", "CostsTabLink", logFileWriter);
		UIController.waitAndClick(by);

		numUploads[1] = HandSOnTestSuite.searchAuthorizationUI.getBottomRow("//tr/td[3]/span/pre", new String(), logFileWriter, true);
		logFileWriter.write("The number of uploads for the Recruitment is "+numUploads[1]);
		logFileWriter.newLine();
		
		String[] names = {"AttachmentFirstRowText", "NoteFirstRowText", "DescriptionTextArea", "DataSourceTextArea", "DataSourceDateTextField"};

		String[] recruitmentValues = new String[5];
		recruitmentValues[0] = getStringValueFromIncidentalData("Recruitment Costs Page", names[0], logFileWriter);
		recruitmentValues[1] = getStringValueFromIncidentalData("Recruitment Costs Page", names[1], logFileWriter);
		recruitmentValues[2] = getStringValueFromIncidentalData("Recruitment Costs Page", names[2], logFileWriter);
		recruitmentValues[3] = getStringValueFromIncidentalData("Recruitment Costs Page", names[3], logFileWriter);
		recruitmentValues[4] = getStringValueFromIncidentalData("Recruitment Costs Page", names[4], logFileWriter);

		if (numUploads[0] != numUploads[1]){
			String message = "ERROR - The number of uploads between the Search Authorization and Recruitment is different.  ";
			message = message.concat("Search Authorization "+searchAuthIDString+" has "+numUploads[0]+" uploads");
			message = message.concat(", but Recruitment "+recruitmentID+" has "+numUploads[1]+" uploads;\n");
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
		}//end if - error condition
		else {
			logFileWriter.write("The number of uploads between the SA and Recruitment are equal, as expected");
			logFileWriter.newLine();
		}//end else - error condition
		for (int i=0; i<SA_textValues.length; i++){
			if (SA_textValues[i].equalsIgnoreCase(recruitmentValues[i])){
				logFileWriter.write("Value of Search Authorization and Recruitment are equal for "+names[i]+", as expected; ");
				logFileWriter.newLine();
			}//end if - no error condition
			else{
				String message = "Value of Search Authorization "+searchAuthIDString+" and Recruitment "+recruitmentID;
				message = message.concat(", for "+names[i]);
				message = message.concat("Search Authorization has value "+SA_textValues[i]);
				message = message.concat(", but Recruitment has value "+recruitmentValues[i]+";\n ");
				logFileWriter.write(message);
				logFileWriter.newLine();
				verificationErrors.append(message);
			}//end else - it's an error condition
		}//end for loop
	}//end compareIncidentalData method
	
	public String getStringValueFromIncidentalData(String worksheet, String name, BufferedWriter logFileWriter) throws Exception{
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(worksheet, name, logFileWriter);
		UIController.waitForElementPresent(by);
		String text = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		if (text == null || text.isEmpty()){
			text = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
		}//end if
		logFileWriter.write("The value for "+name+" is '"+text+"'"); 
		logFileWriter.newLine();
		return text;
	}//end getStringValueFromIncidentalData method
	
	/*
	 * In this method, I want to enter a candidate name and an anticipated start date
	 * which is in the future and save this Recruitment.
	 */
	
	protected void enterBasicRecruitmentInfo(String searchAuthIDString,	boolean isFirst, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method enterBasicRecruitmentInfo called with SA ID "+searchAuthIDString+" ***");
		logFileWriter.newLine();
		String anticipStartDate = "09/01/20"+HandSOnTestSuite.currentNY.split("-")[1];
		String nameSuffix = searchAuthIDString;
		if (isFirst)
			nameSuffix = nameSuffix.concat("_1");
		else
			nameSuffix = nameSuffix.concat("_2");
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", 
				"FirstNameTextfield", logFileWriter);
		UIController.waitForElementPresent(by);
		UIController.driver.findElement(by).clear();
		UIController.driver.findElement(by).sendKeys("FirstName"+nameSuffix);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", 
				"LastNameTextfield", logFileWriter);
		UIController.driver.findElement(by).clear();
		UIController.driver.findElement(by).sendKeys("LastName"+nameSuffix);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", 
				"AnticipatedStartDateTextfield", logFileWriter);
		UIController.driver.findElement(by).clear();
		UIController.driver.findElement(by).sendKeys(anticipStartDate);
		
		if (! isFirst)
			enterSpouseInformation(searchAuthIDString, logFileWriter);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", 
				"SaveButton", logFileWriter);
		UIController.driver.findElement(by).click();
		
		Thread.sleep(5000);
		boolean gotAlert = false;
		for(int i=0; (i<60) && (! gotAlert); i++) {
			try{
				UIController.driver.switchTo().alert().accept();
				logFileWriter.write("Save confirmation dialog successfully closed");
				logFileWriter.newLine();
				gotAlert = true;
			}
			catch(Exception e){
				logFileWriter.write("Could not switch to save confirmation dialog because of "+e.getMessage());
				logFileWriter.newLine();
			}
			logFileWriter.write("Waiting a few more seconds to get the save confirmation dialog");
			logFileWriter.newLine();
			Thread.sleep(5000);
		}//end outer for - iterating through this to try to catch the save dialog
		
		if (UIController.switchWindowByURL(UIController.matchedPersonDialogURL, logFileWriter)){
			Thread.sleep(1000);
			logFileWriter.write("Successfully acquired the matched person dialog - attempting to click select");
			logFileWriter.newLine();
			UIController.waitAndClick(By.linkText("select"));
			Thread.sleep(5000);
			if (UIController.switchWindowByURL(UIController.matchedPersonDialogURL, logFileWriter)){
				Thread.sleep(1000);
				logFileWriter.write("Initial attempt to hit select on the matched person dialog was not successful - trying a second time");
				logFileWriter.newLine();
				UIController.waitAndClick(By.linkText("select"));
				Thread.sleep(5000);
			}//end if
			else{
				logFileWriter.write("matched person dialog was successfully closed");
				logFileWriter.newLine();
			}//else - we closed the matched person dialog
		}//end if
		
		Thread.sleep(3000);
		
		if (UIController.switchWindowByURL(UIController.newRecruitmentURL+"?recId=", logFileWriter)){
			logFileWriter.write("Successfully switched back to the Recruitment window");
			logFileWriter.newLine();
		}
		//wait for the alert to be present
		Thread.sleep(3000);
		UIController.driver.navigate().refresh();
		Thread.sleep(3000);
		try{
			UIController.driver.switchTo().alert().accept();
		}//end try clause
		catch(NoAlertPresentException e){
			logFileWriter.write("Alert confirming save was not present");
			logFileWriter.newLine();
		}//end catch clause
		logFileWriter.write("*** method enterBasicRecruitmentInfo is now finished - recruitment info is entered ***");
		logFileWriter.newLine();
	}//end enterBasicRecruitmentInfo method
	
	protected void enterSpouseInformation(String searchAuthIDString, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method enterSpouseInformation called with SA ID "+searchAuthIDString+" ***");
		logFileWriter.newLine();
		
		String nameSuffix = searchAuthIDString;

		try{
			UIController.driver.switchTo().alert().accept();
			logFileWriter.write("At the start of the method, alert was present");
			logFileWriter.newLine();
		}//end try clause
		catch(NoAlertPresentException e){
			logFileWriter.write("At the start of the method, alert was not present");
			logFileWriter.newLine();
		}//end catch clause

		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", 
				"SpouseFirstNameTextField", logFileWriter);
		UIController.waitForElementPresent(by);
		Thread.sleep(1000);
		UIController.driver.findElement(by).clear();
		Thread.sleep(1000);
		UIController.driver.findElement(by).sendKeys("SpouseFirst"+nameSuffix);

		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", 
				"SpouseMiddleNameTextField", logFileWriter);
		Thread.sleep(1000);
		UIController.driver.findElement(by).clear();
		Thread.sleep(1000);
		UIController.driver.findElement(by).sendKeys("SpouseMiddle"+nameSuffix);

		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", 
				"SpouseLastNameTextField", logFileWriter);
		Thread.sleep(1000);
		UIController.driver.findElement(by).clear();
		Thread.sleep(1000);
		UIController.driver.findElement(by).sendKeys("SpouseLast"+nameSuffix);
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", 
				"SpouseInstDropdown", logFileWriter);
		Thread.sleep(1000);
		UIController.waitAndSelectElementInMenu(by, "Aarhus University");
		
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", 
				"SpouseNotesTextField", logFileWriter);
		Thread.sleep(1000);
		UIController.driver.findElement(by).clear();
		Thread.sleep(1000);
		UIController.driver.findElement(by).sendKeys("Notes "+nameSuffix);
		
	}//end enterSpouseInformation method
	
	/*
	 * In this method, we want to go to the "General" tab of the SA and click on the 
	 * "Add New Recruitment" button and verify that the initial Recruitment which appears
	 * has no existing Name information, and has default values pre-selected
	 */
	protected void addNewRecruitment(String ID, boolean isFirstRecruitment, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method addNewRecruitment called with SA ID "+ID+" ***");
		logFileWriter.newLine();
		//click on the "General" tab
		Thread.sleep(2000);
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName(SearchAuthorizationUIController.CostsWorksheetName, 
				"GeneralTabLink", logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(3000);
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName(SearchAuthorizationUIController.GeneralWorksheetName, 
				"AddRecruitmentButton", logFileWriter);
		UIController.waitAndClick(by);
		//now, verify that the default values are set
		Thread.sleep(5000);
		UIController.switchWindowByURL("showRecruitment.do?searchAuthId="+ID, logFileWriter);

		verifyWidgetValue("SearchAuthIDLabel", ID, ID, logFileWriter);
		verifyWidgetValue("RecruitmentIDLabel", ID, new String(), logFileWriter);
		verifyWidgetValue("AnticipatedStartDateTextfield", ID, new String(), logFileWriter);
		verifyWidgetValue("FirstNameTextfield", ID, new String(), logFileWriter);
		verifyWidgetValue("MiddleNameTextfield", ID, new String(), logFileWriter);
		verifyWidgetValue("LastNameTextfield", ID, new String(), logFileWriter);
		String message = new String();
		boolean radioButtonExpected = false;
		if (isFirstRecruitment){
			by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", 
					"PrimaryRecruitmentTypeRadioButton", logFileWriter);
			String value = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
			if (value.equalsIgnoreCase("active"))
				radioButtonExpected = true;
			message = message.concat("The Primary Recruitment Radio Button is set to "+value);
		}//end if - first recruitment
		else{//it's the second recruitment
			by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", 
					"BackupRecruitmentTypeRadioButton", logFileWriter);
			String value = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
			if (value.equalsIgnoreCase("backup"))
				radioButtonExpected = true;
			message = message.concat("The Backup Recruitment Radio Button is set to "+value);
		}//end else - the second recruitment
		if (radioButtonExpected)
			message = message.concat(", as expected");
		else{
			message = message.concat(", which is not the expected value");
			verificationErrors.append(message);
		}//end else - ERROR
		logFileWriter.write(message);
		logFileWriter.newLine();
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", 
				"OfferStatusCode", logFileWriter);
		Select offerSelect = new Select(UIController.driver.findElement(by));
		String actualOfferStatus = offerSelect.getFirstSelectedOption().getText();
		if (actualOfferStatus.equalsIgnoreCase("No Offer")){
			logFileWriter.write("Offer Status is 'No Offer', as expected");
		}//end if
		else{
			message = "Offer Status should have been 'No Offer', but is actually "+actualOfferStatus;
			verificationErrors.append(message);
			logFileWriter.write(message);
		}//end else
		processExpectedNonBlankField("RankLabel", logFileWriter);
		processExpectedNonBlankField("FacultyLevelLabel", logFileWriter);
		processExpectedNonBlankField("ProjectedStartDate", logFileWriter);
		processExpectedNonBlankField("PlannedBaseBudgetYear", logFileWriter);

		logFileWriter.newLine();
	}//end addNewRecruitment method
	
	protected void processExpectedNonBlankField(String name, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method processExpectedNonBlankField called with field name "+name+" ***");
		logFileWriter.newLine();
		if (! isFieldBlank(name, logFileWriter)){
			logFileWriter.write(name+" is not blank.  It is in the expected state");
			logFileWriter.newLine();
		}//end if
		else{
			String message = "ERROR: " + name +" is blank, unexpectedly";
			verificationErrors.append(message);
			logFileWriter.write(message);
			logFileWriter.newLine();
		}//end else
	}//end processExpectedNonBlankField method
	
	protected void verifyWidgetValue(String widgetName, String ID, String expectedValue, 
			BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method verifyWidgetValue called with widget name "
			+widgetName+", and SA ID "+ID);
		logFileWriter.newLine();
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", 
				widgetName, logFileWriter);
		UIController.waitForElementPresent(by);
		String text = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);

		boolean actualFieldIsBlank = isFieldBlank(widgetName, logFileWriter);
		if (expectedValue.isEmpty()){
			if (actualFieldIsBlank) {
				logFileWriter.write(widgetName + " for Recruitment for SA ID "+ID+" is empty, as expected");
				logFileWriter.newLine();
			}//end if
			else{
				String message = widgetName + " for Recruitment for SA ID "+ID+" not is empty, as expected, but has value "+text;
				logFileWriter.write(message);
				logFileWriter.newLine();
				verificationErrors.append(message);				
			}//end else
		}//end if - we expect the value to be blank
		else {//the expected value is not blank or null
			if (actualFieldIsBlank) {
				String message = "Recruitment for SA ID "+ID+" does not have the " + widgetName +", as expected, but is blank";
				logFileWriter.write(message);
				logFileWriter.newLine();
				verificationErrors.append(message);
			}//end if - error condition - SA ID  is empty
			else if (text.equalsIgnoreCase(expectedValue)){
				logFileWriter.write("Recruitment has "+widgetName+" with value " + expectedValue +", as expected");
				logFileWriter.newLine();
			}//end else if - it is as it should be
			else{
				String message = "Recruitment does not have "+widgetName+" with value " + expectedValue 
						+", as expected, but has value "+text;
				logFileWriter.write(message);
				logFileWriter.newLine();
				verificationErrors.append(message);
			}//end else		
		}//end if - expected value is not null
	}//end verifyWidgetValue method
	
	protected boolean isFieldBlank(String name, BufferedWriter logFileWriter) throws Exception{
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Recruitment General Page", 
				name, logFileWriter);
		UIController.waitForElementPresent(by);
		String text = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		if (text == null || text.isEmpty()) {
			text = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);
		}
		if (text == null || text.isEmpty()) {
			logFileWriter.write(name +" is blank");
			logFileWriter.newLine();
			return true;
		}//end if
		else{
			logFileWriter.write(name + " is not blank");
			logFileWriter.newLine();
			return false;
		}
	}//end isFieldBlank method
	
	protected void adjustSearchAuthIncidentalData(String ID, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method adjustSearchAuthIncidentalData called with SA ID "+ID+" ***");
		logFileWriter.newLine();
		logFileWriter.write("Now refreshing the SA");
		logFileWriter.newLine();
		UIController.driver.navigate().refresh();
		try {
			Thread.sleep(3000);
			UIController.driver.switchTo().alert().accept();
			Thread.sleep(3000);
		}
		catch(Exception e) {
			logFileWriter.write("Could not accept the alert because of Exception "+e.getMessage());
			logFileWriter.newLine();
		}
		UIController.waitAndClick(By.id("costs_a"));
		
		UIController.waitForElementPresent(By.name("searchAuthDTO.offerDTO.offerNotes"));
		UIController.driver.findElement(By.name("searchAuthDTO.offerDTO.offerNotes")).clear();
		UIController.driver.findElement(By.name("searchAuthDTO.offerDTO.offerNotes")).sendKeys("Description");
		UIController.driver.findElement(By.id("focusDatasourceId")).clear();
		UIController.driver.findElement(By.id("focusDatasourceId")).sendKeys("Data Source");
		UIController.driver.findElement(By.id("dataSourceDate")).clear();
		UIController.driver.findElement(By.id("dataSourceDate")).sendKeys(dataSourceDate);
		UIController.driver.findElement(By.linkText("ADD")).click();
		UIController.waitForElementPresent(By.id("newNoteContent"));
		UIController.driver.findElement(By.id("newNoteContent")).clear();
		UIController.driver.findElement(By.id("newNoteContent")).sendKeys(noteText);
		UIController.driver.findElement(By.linkText("INSERT")).click();
		
		UIController.driver.findElement(By.linkText("Add")).click();
		
		UIController.switchWindowByURL(UIController.searchAuthorizationAttachmentDialogURL, logFileWriter);
		UIController.driver.findElement(By.xpath("//tr[2]/td[2]/input")).clear();
		UIController.driver.findElement(By.xpath("//tr[2]/td[2]/input")).sendKeys(attachmentText);
		UIController.driver.findElement(By.name("file")).clear();
		UIController.driver.findElement(By.name("file")).sendKeys("C:\\Users\\christ13\\Documents\\HandSon\\"+uploadedFileName);
		UIController.driver.findElement(By.id("mybutton")).click();
		if (switchToSearchAuthWindow(ID, verificationErrors, logFileWriter)){
			logFileWriter.write("Got back to the Search Authorization window, as expected, for Search Auth ID "+ID);
			logFileWriter.newLine();
		}//end if
		//now, save the SA
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("SA Costs Page", "SaveButton", logFileWriter);
		UIController.waitAndClick(by);
		Thread.sleep(3000);
		UIController.driver.switchTo().alert().accept();
		Thread.sleep(3000);
	}//end adjustSearchAuthIncidentalData method
	
	protected void verifyInitialSearchAuth(String ID, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method verifyInitialSearchAuth called with SA ID "+ID +" ***");
		logFileWriter.newLine();
		UIController.switchWindowByURL(UIController.searchAuthorizationLookupURL, logFileWriter);
		Thread.sleep(3000);
		if (switchToSearchAuthWindow(ID, verificationErrors, logFileWriter)){
			UIController.waitForElementPresent(By.cssSelector("div.readState"));
			if (UIController.isElementPresent(By.linkText("Add New Recruitment Record"))){
				logFileWriter.write("'Add New Recruitment Record' button is present, as expected");
				logFileWriter.newLine();
			}
			else{
				String message = "ERROR - 'Add New Recruitment Record' button is unexpectedly not present; ";
				logFileWriter.write(message);
				logFileWriter.newLine();
				verificationErrors.append(message);
			}
		}//end if
		else{
			logFileWriter.write("Could not switch to the Search Authorization ID "+ID);
			logFileWriter.newLine();
		}//end else
	}//end verifyInitialSearchAuth method
	
	public static boolean switchToSearchAuthWindow(String ID, StringBuffer verificationErrors, BufferedWriter logFileWriter) throws Exception{
		String message = HandSOnTestSuite.searchAuthorizationUI.switchToSearchAuthWindow(ID, logFileWriter);
		if (message.isEmpty()){
			logFileWriter.write("... and we switched to the search Authorization Window, as expected");
			Thread.sleep(2000);
		}//end if
		else{
			logFileWriter.write(message);
			verificationErrors.append(message);
			return false;
		}
		logFileWriter.newLine();
		return true;
	}//end switchToSearchAuthWindow method
	
	protected static String[] selectInitialRecruitments(StringBuffer verificationErrors, int maxIndex, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** selectInitialRecruitments method called ***");
		logFileWriter.newLine();

		UIController.switchToStartPage(logFileWriter);
		
		//look up an SA with no Recruitment - it should be Active, with reasons either Search or Pending
		//open the Search Authorization Page
		processPageByLink("Search Authorization Lookup", verificationErrors, logFileWriter);
		Thread.sleep(3000);
		UIController.switchWindowByURL(UIController.searchAuthorizationLookupURL, logFileWriter);
		Thread.sleep(3000);
		//first, clear everything that was set before by any other test
		UIController.driver.findElement(By.linkText("CLEAR")).click();
		//select "Active"
		By by = By.id("searchStatus");
		UIController.waitAndSelectElementInMenu(by, "Active");
		//select the reason codes
		by = By.id("reasonCode");
		Select reasonCode = new Select(UIController.driver.findElement(by));
		reasonCode.selectByVisibleText("Search");
		reasonCode.selectByVisibleText("Pending");
		Thread.sleep(2000);
		//now click "Find" button
		UIController.waitAndClick(By.id("mybutton"));
		Thread.sleep(3000);
		try{
			UIController.driver.findElement(By.linkText("LAST")).click();
		}//end try clause
		catch(Exception e){
			logFileWriter.write("Couldn't acquire the 'LAST' link - search will happen on the first page only");
			logFileWriter.newLine();
		}//end catch clause
		Thread.sleep(3000);
		//select the appropriate rows which have no value in the //tr[##]/td[12]/a locator, ## being the rows
		//start by finding the bottom-most row
		String baseIDLocator = "//div/table/tbody/tr/td[2]/a";
		String baseNameLocator = "//div/table/tbody/tr/td[12]/a";
		UIController.waitForElementPresent(By.xpath(baseIDLocator));
		int bottomRow = HandSOnTestSuite.searchAuthorizationUI.getBottomRow(baseIDLocator, new String(), logFileWriter, true);
		if (bottomRow < maxIndex) {//there are fewer initial SA's than we need in the LAST page, so go to the PREVIOUS page
			bottomRow = getBottomRowPreviousPage(baseIDLocator, logFileWriter);
		}//end if
		
		ArrayList<String> IDsOnPage = getIDsFromSearchResults(bottomRow, baseIDLocator, baseNameLocator, logFileWriter);
		ArrayList<String> IDs = new ArrayList<String>();
		IDs = concatenateArrayLists(IDs, IDsOnPage, logFileWriter);
		while (IDs.size() < maxIndex) {// we don't have enough - we have to keep searching
			//if we got one but not two
			bottomRow = getBottomRowPreviousPage(baseIDLocator, logFileWriter);
			IDsOnPage = getIDsFromSearchResults(bottomRow, baseIDLocator, baseNameLocator, logFileWriter);
			IDs = concatenateArrayLists(IDs, IDsOnPage, logFileWriter);
			//return selectInitialRecruitments(verificationErrors, maxIndex+6, logFileWriter);
		}//end if 
		String[] IDArray = new String[maxIndex];
		logFileWriter.write("This is the finalized list of SA's to be used: ");
		for (int i=0; i<maxIndex; i++){
			IDArray[i] = IDs.get(i);
			logFileWriter.write(IDArray[i]+"; ");
		}//end for loop
		//now, iterate through all of the windows which are open and close those which are not in the IDArray
		logFileWriter.newLine();
		logFileWriter.write("Now we will close all of the windows which are not in the finalized list of SA's");
		logFileWriter.newLine();
		
		UIController.outputAvailableWindows(logFileWriter);
		Set<String> availableWindows = UIController.driver.getWindowHandles(); 
		for (String windowId: availableWindows){
			String switchedWindowURL = new String();
			try {
				switchedWindowURL=UIController.driver.switchTo().window(windowId).getCurrentUrl();
				logFileWriter.write("The URL for window ID "+windowId+" is "+switchedWindowURL);
				boolean shouldNotBeClosed = false;
				//first, iterate through the whole ID Array and see if it's there....
				for(int i=0; i<IDArray.length; i++) {
					String ID = IDArray[i];
					if ((switchedWindowURL.contains("showSearchAuthAction.do?authId="+ID)) || (! switchedWindowURL.contains("showSearchAuthAction.do?"))) {
						shouldNotBeClosed = true;
					}//end inner if loop
				}//end inner for loop
				//if it's not in the array, close it
				if (shouldNotBeClosed) {
					logFileWriter.write(switchedWindowURL+" contains an SA ID that will be used - leaving this tab open");
					logFileWriter.newLine();
				}//end if
				else {
					logFileWriter.write(switchedWindowURL + " does not contain an SA ID that will be used - closing this tab");
					logFileWriter.newLine();
					UIController.driver.close();
				}//end else
			}//end try
			catch(Exception e) {
				String errMsg = e.getMessage();
				logFileWriter.write("Could not switch to the window because of Exception "+errMsg);
				logFileWriter.newLine();
			}//end catch
		}//end for loop
		return IDArray;
	}//end selectInitialRecruitments method
	
	public static ArrayList<String> concatenateArrayLists(ArrayList<String> first, ArrayList<String> second, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method concatenateArrayLists called ***"); logFileWriter.newLine();
		logFileWriter.write("The initial ArrayList wasn't big enough - it has a size of "+first.size()); logFileWriter.newLine();
		logFileWriter.write("The second ArrayList we created has a size of "+second.size()); logFileWriter.newLine();
		ArrayList<String> concatenatedList = new ArrayList<String>();
		if (first.size() > 0) {
			for (int i=0; i<first.size(); i++) {
				concatenatedList.add(first.get(i));
				logFileWriter.write("Adding "+first.get(i) +" from the first ArrayList to the concatenated list");
				logFileWriter.newLine();
			}//end for loop
		}//end if
		if (second.size() > 0) {
			for (int j=0; j<second.size(); j++) {
				concatenatedList.add(second.get(j));
				logFileWriter.write("Adding "+second.get(j) +" from the first ArrayList to the concatenated list");
				logFileWriter.newLine();
			}//end for loop
		}//end if
		logFileWriter.write("Returning an ArrayList of size "+concatenatedList.size());
		logFileWriter.newLine();
		return concatenatedList;
	}//end method concatenateArrayLists
	
	public static ArrayList<String> getIDsFromSearchResults(int bottomRow, String baseIDLocator, String baseNameLocator, BufferedWriter logFileWriter) throws Exception{
		ArrayList<String> IDs = new ArrayList<String>();
		for (int row=1; row<=bottomRow; row++){
			String IDLocator = baseIDLocator;
			String NameLocator = baseNameLocator;
			String name = new String();
			if (row>1){
				IDLocator = baseIDLocator.replaceFirst("/tr", "/tr[" + row + "]");
				NameLocator = baseNameLocator.replaceFirst("/tr", "/tr[" + row + "]");
			}//end if
			name = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(By.xpath(NameLocator), logFileWriter);	
			if (name.isEmpty()){
				By by = By.xpath(IDLocator);
				String ID = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
				IDs.add(ID);
				logFileWriter.write("Adding ID "+ID+" to the array to be returned");
				logFileWriter.newLine();
				// the minute we have a proper SA ID selected, we open the page for that SA
				processPageByLink(ID, verificationErrors, logFileWriter);
				Thread.sleep(10000);
				UIController.switchWindowByURL(UIController.searchAuthorizationLookupURL, logFileWriter);
			}//end if - add this to the array
		}//end for loop
		return IDs;
	}//end getIDsFromSearchResults method
	
	public static int getBottomRowPreviousPage(String baseIDLocator, BufferedWriter logFileWriter) throws Exception{
		int bottomRow = 0;
		logFileWriter.write("Couldn't find enough SA's on the LAST page, so we're going to the PREVIOUS page");
		logFileWriter.newLine();
		try{
			UIController.driver.findElement(By.linkText("PREVIOUS")).click();
			Thread.sleep(6000);
			bottomRow = HandSOnTestSuite.searchAuthorizationUI.getBottomRow(baseIDLocator, new String(), logFileWriter, true);
		}//end try clause
		catch(Exception e){
			logFileWriter.write("Couldn't acquire the 'PREVIOUS' link - will attempt to acquire the 'FIRST' link");
			logFileWriter.newLine();
			try{
				UIController.driver.findElement(By.linkText("FIRST")).click();
				Thread.sleep(6000);
				bottomRow = HandSOnTestSuite.searchAuthorizationUI.getBottomRow(baseIDLocator, new String(), logFileWriter, true);
			}//end try clause
			catch(Exception e2){
				logFileWriter.write("Couldn't acquire the 'FIRST' link");
				logFileWriter.newLine();
			}//end catch clause
		}//end if
		return bottomRow;
	}//end getBottomRowPreviousPage
	
	public static void processPageByLink(String linkText, StringBuffer verificationErrors, BufferedWriter logFileWriter) throws Exception{
		String message = UIController.processPageByLink(linkText, logFileWriter)+";\n";
		logFileWriter.write(message); 
		logFileWriter.newLine();
		verificationErrors.append(message);
	}//end processPageByLink method
	
	/*
	 * This is written to test HANDSON-3777.
	 * 
	 * The 'Send' button will remain invisible if any of the following is true:
	 *         The iJournal status code has a value.

        The transfer status is empty.

        The transfer status is non-empty, but not set to ‘READY’.

        The transfer date has a value.

        The iJournal number has a value.

        The total amount is empty.

        The total amount is negative.

        The expense area is empty.

        Source PTA or destination PTA is empty.

        Subcategory is empty.

        fiscal year is empty.

        funding unit is empty.

        The commitment line item is rejected (red flag) or pending (yellow flag)
	 */
	protected void testSendButton(BufferedWriter logFileWriter, String commitmentNumber) throws Exception{
		logFileWriter.write("*** running method testSendButton with Commitment ID "+commitmentNumber);
		logFileWriter.newLine();
		String baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName("Commitment Transfer Line", "SendButton", 
				logFileWriter);
		logFileWriter.write("Base locator of Send Button is  "+baseLocator);
		String amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], 5, logFileWriter);
		logFileWriter.write("Amended locator of Send Button is "+amendedLocator);
		logFileWriter.newLine();
		
		//now instantiate the Commitment Transfer Line on the fifth row
		CommitmentTransferLine line = new CommitmentTransferLine(5, logFileWriter);
		
		String message = "Initial test - verifying the initial state of the 'Send' button";
		logFileWriter.write(message);
		logFileWriter.newLine();
		System.out.println(message);
		
		//the "Send" button should initially be visible
		boolean shouldBePresent = shouldBePresent(line, logFileWriter);
		testButtonAccessibility(amendedLocator, shouldBePresent, logFileWriter);
		
		//set the Funding Unit to a value other than "H&S" - should make the "Send" button invisible
		line.setValue(CommitmentTransferLine.FundingUnit, "Provost", logFileWriter);
		Thread.sleep(2000);
		line.setValue(CommitmentTransferLine.TransferStatus, "select", logFileWriter);
		Thread.sleep(2000);
		line.setValue(CommitmentTransferLine.TransferStatus, "Ready", logFileWriter);
		Thread.sleep(2000);
		line.initialize(logFileWriter);//make sure that the "Send" button's visibility is updated appropriately
		shouldBePresent = shouldBePresent(line, logFileWriter);
		message = "Second test - verifying that the 'Send' button is invisible when the Funding Unit is not 'H&S'";
		logFileWriter.write(message);
		logFileWriter.newLine();
		System.out.println(message);
		message = " - expectation of visibility is "+shouldBePresent;
		logFileWriter.write(message);
		logFileWriter.newLine();
		System.out.println(message);

		testButtonAccessibility(amendedLocator, shouldBePresent, logFileWriter);
		
		//now set the Funding Unit back to "H&S" - should make the "Send" button visible again
		line.setValue(CommitmentTransferLine.FundingUnit, "H&S", logFileWriter);
		Thread.sleep(2000);
		line.setValue(CommitmentTransferLine.TransferStatus, "select", logFileWriter);
		Thread.sleep(2000);
		line.setValue(CommitmentTransferLine.TransferStatus, "Ready", logFileWriter);
		Thread.sleep(2000);
		line.initialize(logFileWriter);//make sure that the "Send" button's visibility is updated appropriately
		shouldBePresent = shouldBePresent(line, logFileWriter);
		message = "Third test - verifying that the 'Send' button is invisible when the Funding Unit is not 'H&S'";
		logFileWriter.write(message);
		logFileWriter.newLine();
		System.out.println(message);
		message = " - expectation of visibility is "+shouldBePresent;
		logFileWriter.write(message);
		logFileWriter.newLine();
		System.out.println(message);
		testButtonAccessibility(amendedLocator, shouldBePresent, logFileWriter);
		
		//now set the Transfer Status to "Void" - should make the "Send" button invisible
		line.setValue(CommitmentTransferLine.TransferStatus, "select", logFileWriter);
		Thread.sleep(2000);
		line.setValue(CommitmentTransferLine.TransferStatus, "Void", logFileWriter);
		Thread.sleep(2000);
		
		//Check and Uncheck the Transfer Line Checkbox to shift the focus
		checkAndUncheckTransferLineCheckbox(logFileWriter, line, 5);
		
		line.initialize(logFileWriter);//make sure that the "Send" button's visibility is updated appropriately
		shouldBePresent = shouldBePresent(line, logFileWriter);
		message = "Fourth test - verifying that the 'Send' button is invisible when the Transfer Status is not 'Ready'";
		logFileWriter.write(message);
		logFileWriter.newLine();
		System.out.println(message);
		message = " - expectation of visibility is "+shouldBePresent;
		logFileWriter.write(message);
		logFileWriter.newLine();
		System.out.println(message);
		testButtonAccessibility(amendedLocator, shouldBePresent, logFileWriter);
		
		//now set the Transfer Status to "Ready" - should make the "Send" button visible again
		line.setValue(CommitmentTransferLine.TransferStatus, "select", logFileWriter);
		Thread.sleep(2000);
		line.setValue(CommitmentTransferLine.TransferStatus, "Ready", logFileWriter);
		Thread.sleep(2000);

		checkAndUncheckTransferLineCheckbox(logFileWriter, line, 5);

		line.initialize(logFileWriter);//make sure that the "Send" button's visibility is updated appropriately
		shouldBePresent = shouldBePresent(line, logFileWriter);
		message = "Fourth test, continued - verifying that the 'Send' button is invisible when the Transfer Status is returned to 'Ready'";
		logFileWriter.write(message);
		logFileWriter.newLine();
		System.out.println(message);
		message = " - expectation of visibility is "+shouldBePresent;
		logFileWriter.write(message);
		logFileWriter.newLine();
		System.out.println(message);
		testButtonAccessibility(amendedLocator, shouldBePresent, logFileWriter);
		
		testSendButtonVisibilityWithBlankValues(logFileWriter, line, 5);
		
		message = "Sixth test - verifying that the 'Send' button is invisible when the Amount is zero or negative";
		logFileWriter.write(message);
		logFileWriter.newLine();
		System.out.println(message);

		String originalAmountValue = line.getValueForField(CommitmentTransferLine.Amount, logFileWriter);
		logFileWriter.write("Original value for the Amount field is "+originalAmountValue);
		logFileWriter.newLine();
		logFileWriter.write("Initially, conditions are appropriate for the 'Send' button to appear (true or false): "+shouldBePresent);
		logFileWriter.newLine();
		testButtonAccessibility(amendedLocator, shouldBePresent, logFileWriter);

		String[] newValues = {"0.00", "-"+originalAmountValue};
		
		for (int i=0; i<newValues.length; i++) {

			line.setValue(CommitmentTransferLine.Amount, newValues[i], logFileWriter);
			Thread.sleep(2000);
			checkAndUncheckTransferLineCheckbox (logFileWriter, line, 5);
			line.initialize(logFileWriter);//make sure that the "Send" button's visibility is updated appropriately
			shouldBePresent = shouldBePresent(line, logFileWriter);
			message = "Amount will be set to '"+newValues[i]+"'";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			
			message = " - expectation of visibility is "+shouldBePresent;
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			testButtonAccessibility(amendedLocator, shouldBePresent, logFileWriter);
			
			//now set the Funding Unit back to the original value - should make the "Send" button visible again
			line.setValue(CommitmentTransferLine.Amount, originalAmountValue, logFileWriter);
			Thread.sleep(2000);
			checkAndUncheckTransferLineCheckbox(logFileWriter, line, 5);
			line.initialize(logFileWriter);//make sure that the "Send" button's visibility is updated appropriately
			shouldBePresent = shouldBePresent(line, logFileWriter);
			message = "Sixth test, continued - verifying that the 'Send' button is visible when the Amount is not zero or negative";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			message = " - expectation of visibility is "+shouldBePresent;
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			testButtonAccessibility(amendedLocator, shouldBePresent, logFileWriter);
		}//end for loop
		
		message = "Seventh test - verifying that the 'Send' button will be invisible if the Transfer Date, or iJournal is not blank";
		logFileWriter.write(message);
		logFileWriter.newLine();
		System.out.println(message);
		
		/*
		This test verifies that the 'Send' button does not appear if the Transfer Date is populated or iJournal field populated
		... even if the Transfer Status is 'Ready'
		It also verifies that the 'Send' button does appear if the iBudget field is populated
		
		The code will need to do the following:
		verify that the 'Send' button is initially visible
		adjust the Transfer Status to "Sent"
		check and uncheck the transfer line checkbox
		adjust the field to the new value
		adjust the Transfer Status to "Ready"
		check and uncheck the transfer line checkbox
		verify that the 'Send' button is now invisible (unless it's iBudget)
		adjust the Transfer Status to "Sent"
		check and uncheck the transfer line checkbox
		adjust the field back to its former (blank) value
		adjust the Transfer Status to "Ready"
		check and uncheck the transfer line checkbox
		verify that the 'Send' button is now visible
		
		 */
		String[] nonBlankValues = {"09/01/2021", "iJournal", "iBudget"};
		int[] valuesIndices = {CommitmentTransferLine.TransferDate, CommitmentTransferLine.iJournal, CommitmentTransferLine.iBudget};
		
		logFileWriter.write("Initially, conditions are appropriate for the 'Send' button to appear (true or false): "+shouldBePresent);
		logFileWriter.newLine();
		testButtonAccessibility(amendedLocator, true, logFileWriter);

		for(int i=0; i<valuesIndices.length; i++) {
			logFileWriter.write("Testing whether or not the '"+line.getStringValueForIndex(valuesIndices[i])
				+"' affects the visibility of the 'Send' button");
			logFileWriter.newLine();
			testButtonAccessibility(amendedLocator, shouldBePresent(line, logFileWriter), logFileWriter);
			
			//sent the Transfer Status to 'Sent' and set the field to a non-blank value
			line.setValue(CommitmentTransferLine.TransferStatus, "Sent", logFileWriter);
			Thread.sleep(2000);
			checkAndUncheckTransferLineCheckbox(logFileWriter, line, 5);
			Thread.sleep(2000);
			line.setValue(valuesIndices[i], nonBlankValues[i], logFileWriter);
			Thread.sleep(2000);
			//now, set the Transfer Status back to 'Ready' and see if the 'Send' button is now invisible
			line.setValue(CommitmentTransferLine.TransferStatus, "Ready", logFileWriter);
			Thread.sleep(2000);
			checkAndUncheckTransferLineCheckbox(logFileWriter, line, 5);
			line.initialize(logFileWriter);
			testButtonAccessibility(amendedLocator, shouldBePresent(line, logFileWriter), logFileWriter);

			//now, set the Transfer Status back to 'Sent', and adjust the field back to a blank value
			line.setValue(CommitmentTransferLine.TransferStatus, "Sent", logFileWriter);
			Thread.sleep(2000);
			checkAndUncheckTransferLineCheckbox(logFileWriter, line, 5);
			Thread.sleep(2000);
			line.setValue(valuesIndices[i], new String(), logFileWriter);
			Thread.sleep(2000);
			//now, set the Transfer Status back to 'Ready' and see if the 'Send' button is now invisible
			line.setValue(CommitmentTransferLine.TransferStatus, "Ready", logFileWriter);
			Thread.sleep(2000);
			checkAndUncheckTransferLineCheckbox(logFileWriter, line, 5);
			line.initialize(logFileWriter);
			testButtonAccessibility(amendedLocator, shouldBePresent(line, logFileWriter), logFileWriter);
		}//end for loop
		
		//the button should be accessible if it's an OPEN/APPROVED Commitment - try to click on it
		if (shouldBePresent) {
			try {
				By by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
				UIController.driver.findElement(by).click();
			}//end try clause
			catch(Exception e) {
				message = "*** ERROR - Could not click on the Send Button ***;\n";
				logFileWriter.write(message);
				logFileWriter.newLine();
				verificationErrors.append(message);
				System.out.println(message);
			}//end catch clause
		}//end if clause		
		else {
			logFileWriter.newLine();
			logFileWriter.write("**** The 'Send' button should not appear, so we won't try to click on it. ****");
			logFileWriter.newLine();
		}//end else
		
		Thread.sleep(3000);
		acceptAlertAndReportMessage(logFileWriter);// dismiss the initial confirmation to send the data to the iJournal system
		Thread.sleep(3000);
		acceptAlertAndReportMessage(logFileWriter);// dismiss the follow-up dialog reporting the data is sent
		Thread.sleep(3000);
		//now, re-initialize the line as the iJournal number is now present
		line.initialize(logFileWriter);
		//now get the iJournal Number of the new iJournal which is (hopefully) created
		String iJournalNumber = new String();
		try{
			iJournalNumber = line.getValueForField(CommitmentTransferLine.iJournal, logFileWriter);
		}
		catch(Exception e) {
			logFileWriter.write("Couldn't get the iJournal number because of Exception "+e.getMessage());
			logFileWriter.newLine();
		}
		logFileWriter.write("iJournal Number derived is '"+iJournalNumber+"';");
		logFileWriter.newLine();
	}//end testSendButton method

	/*
	 * The following scenarios will be tested:
        The total amount is empty.(textfield)
        Source PTA or destination PTA is empty. (textfield)
        The expense area is empty.(select)
	    The transfer status is empty.(select)
        Subcategory is empty.(select)
        fiscal year is empty.(select)
        funding unit is empty.(select)
        
        Of note, deleting the value of either the Source or Destination PTA won't immediately affect the visibility of
        the 'Send' button.  It apparently needs to be submitted before it affects the visibiliity of the 'Send' button.
        There is no way to submit a Commitment if one of the CLI's has a blank Source or Destination PTA.  So, testing
        the invisibility of the 'Send' button when the Source or Destination PTA is blank is not possible, given the 
        constraints of the system.
        
        Also, when the Fiscal Year is set to 'select', the Expense Area is automatically set to 'select' as well.  When
        testing, one must set the Expense Area to its original value when the Fiscal Year is set to its original value.
        
        Also, when the SubCategory is set to 'select', the Amount is automatically set to '0.00'.  When testing, we
        must set the Amount to its original value when the SubCategory is set to its original value.

		The code will need to do the following:
			Verify that the 'Send' button is initially visible
			Get the original value
			Set it to empty or zero
			Check and uncheck the transfer line checkbox
			re-initialize the line
			evaluate whether or not the 'Send' button should be present (need to adjust the 'shouldBePresent' method
			Verify that the 'Send' button is now invisible
			Set it back to the original value
			Check and uncheck the transfer line checkbox
			re-initialize the line
			re-evaluate whether or not the 'Send' button should be present
			Verify that the 'Send' button is now visible
			Report that the test either passed or failed
		
	 */
	public void testSendButtonVisibilityWithBlankValues(BufferedWriter logFileWriter, CommitmentTransferLine line, int row) throws Exception{
		
		String message = "Fifth test - verifying that the 'Send' button is invisible when values are blank.";
		logFileWriter.write(message);
		logFileWriter.newLine();
		System.out.println(message);

		int[] valuesArray = {CommitmentTransferLine.Amount, //CommitmentTransferLine.DestinationPTA, CommitmentTransferLine.SourcePTA,
				CommitmentTransferLine.ExpenseArea, CommitmentTransferLine.TransferStatus, CommitmentTransferLine.SubCategory,
				CommitmentTransferLine.FundingUnit, CommitmentTransferLine.FiscalYear};
		String baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName("Commitment Transfer Line", "SendButton", 
				logFileWriter);
		logFileWriter.write("Base locator of Send Button is  "+baseLocator);
		String amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], 5, logFileWriter);

		for (int i=0; i<valuesArray.length; i++) {
			String originalValue = new String();
			originalValue = line.getValueForField(valuesArray[i], logFileWriter);
			String expenseAreaValue = line.getValueForField(CommitmentTransferLine.ExpenseArea, logFileWriter);
			String amountValue = line.getValueForField(CommitmentTransferLine.Amount, logFileWriter);
			String summerNinthsValue = line.getValueForField(CommitmentTransferLine.SummerNinths, logFileWriter);
			//first, check to see if the "Send" button is visible
			boolean shouldBePresent = shouldBePresent(line, logFileWriter);
			message = " - expectation of visibility is "+shouldBePresent;
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			testButtonAccessibility(amendedLocator, shouldBePresent, logFileWriter);
			Thread.sleep(2000);

			//now set the value to a blank value - should make the "Send" button invisible
			String newValue = "select";
			if (valuesArray[i] == CommitmentTransferLine.Amount) 
				newValue = new String();
			line.setValue(valuesArray[i], newValue, logFileWriter);
			Thread.sleep(2000);
			//Check and Uncheck the Transfer Line Checkbox to shift the focus
			checkAndUncheckTransferLineCheckbox(logFileWriter, line, 5);
	
			line.initialize(logFileWriter);//make sure that the "Send" button's visibility is updated appropriately
			shouldBePresent = shouldBePresent(line, logFileWriter);
			message = "Just set "+line.getStringValueForIndex(valuesArray[i])+" to '"+newValue+"'";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			message = " - expectation of visibility is "+shouldBePresent;
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			testButtonAccessibility(amendedLocator, shouldBePresent, logFileWriter);
			checkAndUncheckTransferLineCheckbox(logFileWriter, line, 5);
			Thread.sleep(2000);
			line.setValue(valuesArray[i], originalValue, logFileWriter);
			Thread.sleep(2000);
			checkAndUncheckTransferLineCheckbox(logFileWriter, line, 5);
			Thread.sleep(2000);
			
			if (valuesArray[i] == CommitmentTransferLine.FiscalYear) {
				logFileWriter.write("Fiscal Year has been reset - resetting the Expense Area to '"+expenseAreaValue+"'");
				logFileWriter.newLine();
				line.setValue(CommitmentTransferLine.ExpenseArea, expenseAreaValue, logFileWriter);
				Thread.sleep(2000);
			}//end if
			else if (valuesArray[i] == CommitmentTransferLine.SubCategory) {
				logFileWriter.write("SubCategory has been reset - resetting the Amount value to '"+amountValue+"'");
				logFileWriter.newLine();
				line.setValue(CommitmentTransferLine.Amount, amountValue, logFileWriter);
				Thread.sleep(2000);
				checkAndUncheckTransferLineCheckbox(logFileWriter, line, 5);
				Thread.sleep(2000);
				if (line.getStringValueForIndex(CommitmentTransferLine.SubCategory).equalsIgnoreCase("Summer 9ths")) {
					logFileWriter.write("Subcategory is Summer 9ths - Summer 9ths value is '"+line.getValueForField(CommitmentTransferLine.SummerNinths, logFileWriter)+"'");
					logFileWriter.newLine();
					//set the Summer Ninths value
					line.setValue(CommitmentTransferLine.SummerNinths, summerNinthsValue, logFileWriter);
				}//end inner if - reset Summer Ninths as well
			}//end else if
			
			
			//Check and Uncheck the Transfer Line Checkbox to shift the focus
			checkAndUncheckTransferLineCheckbox(logFileWriter, line, 5);
			Thread.sleep(2000);
			line.initialize(logFileWriter);//make sure that the "Send" button's visibility is updated appropriately
			shouldBePresent = shouldBePresent(line, logFileWriter);
			message = "Just set "+line.getStringValueForIndex(valuesArray[i])+" to original value of '"+originalValue+"'";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			message = " - expectation of visibility is "+shouldBePresent;
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			testButtonAccessibility(amendedLocator, shouldBePresent, logFileWriter);

		}//end for loop

	}//end method testSendButtonVisibilityWithBlankValues
	
	public static void checkAndUncheckTransferLineCheckbox(BufferedWriter logFileWriter, CommitmentTransferLine line, int row) throws Exception{
		try {
			line.checkTransferLineBox (logFileWriter, verificationErrors, 5);
			logFileWriter.write("Transfer Line Checkbox successfully checked");
			logFileWriter.newLine();
			Thread.sleep(2000);
			line.checkTransferLineBox (logFileWriter, verificationErrors, 5);
			logFileWriter.write("Transfer Line Checkbox successfully unchecked");
			logFileWriter.newLine();
			Thread.sleep(2000);
		}//end try
		catch(Exception e) {
			logFileWriter.write("Could not complete the checking of the checkbox because of Exception "+e.getMessage());
			logFileWriter.newLine();
		}//end catch clause
	}//end checkAndUncheckTransferLineCheckbox method
	
	public static boolean shouldBePresent(CommitmentTransferLine line, BufferedWriter logFileWriter) throws Exception{
		boolean shouldBePresent = true;
		logFileWriter.write("*** Now determining whether or not the Send Button should be present ***");
		logFileWriter.newLine();
		
		//first, determine the status of the Commitment - is it already submitted?
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Commitment Page", "CommitmentStatusLabel", logFileWriter);
		String actualStatus = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		
		//if it's in "DRAFT" status - the "Send" button should not be there
		if (actualStatus.equalsIgnoreCase(NewFundingCommitmentTest.currentStatus)) {
			logFileWriter.write("Commitment Status is "+NewFundingCommitmentTest.currentStatus+"; The 'Send' Button should NOT appear;");
			logFileWriter.newLine();
			shouldBePresent = false;
		}//end if - it's in "DRAFT" status
		else {
			logFileWriter.write("Commitment Status is '"+actualStatus+"' - will not prevent the 'Send' Button from appearing");
			logFileWriter.newLine();
		}//end else - 

		double amount = 0;
		try {
			Double Amount = Double.valueOf(line.getValueForField(CommitmentTransferLine.Amount, logFileWriter));
			amount = Amount.doubleValue();
		}
		catch(Exception e) {
			logFileWriter.write("Could not initialize the amount because of Exception "+e.getMessage());
			logFileWriter.newLine();
		}
		if (amount==0) {
			logFileWriter.write("Dollar Amount is zero - 'Send' Button should not be present");
			logFileWriter.newLine();
			shouldBePresent = false;
		}//end if - Amount is zero
		else if (amount < 0) {
			logFileWriter.write("Dollar Amount is negative - 'Send' Button should not be present");
			logFileWriter.newLine();
			shouldBePresent = false;
		}//end else if - Amount is negative
		else {
			logFileWriter.write("Dollar Amount is "+amount+"' - will not prevent the 'Send' Button from appearing");
			logFileWriter.newLine();
		}//end else - Amount is nonzero positive
		
		if (! line.getValueForField(CommitmentTransferLine.FundingUnit, logFileWriter).equalsIgnoreCase("H&S")) {
			logFileWriter.write("Funding Unit is not H&S - 'Send' Button should not be present");
			logFileWriter.newLine();
			shouldBePresent = false;
		}//end if - Amount is zero
		else {
			logFileWriter.write("Funding Unit is H&S - will not prevent the 'Send' Button from appearing");
			logFileWriter.newLine();
		}//end else - Amount is not zero
		
		if (! line.getValueForField(CommitmentTransferLine.TransferStatus, logFileWriter).equalsIgnoreCase("Ready")) {
			logFileWriter.write("Transfer Status is not 'Ready' - 'Send' Button should not be present");
			logFileWriter.newLine();
			shouldBePresent = false;
		}//end if - Transfer Status is not 'Ready'
		else {
			logFileWriter.write("Transfer Status is 'Ready' - will not prevent the 'Send' Button from appearing");
			logFileWriter.newLine();
		}//end else - Transfer Status is 'Ready'
		
		if (line.getValueForField(CommitmentTransferLine.TransferDate, logFileWriter).length() > 0) {
			logFileWriter.write("Transfer Date is not empty - the 'Send' button should NOT appear.");
			logFileWriter.newLine();
			shouldBePresent = false;
		}//end if - the Transfer Date is not empty
		else {
			logFileWriter.write("Transfer Date is empty - it will not prevent the 'Send' button from appearing");
			logFileWriter.newLine();
		}//end else - Transfer Date is empty
		
		if (line.getValueForField(CommitmentTransferLine.iJournal, logFileWriter).length() > 0) {
			logFileWriter.write("iJournal field is not empty - the 'Send' button should NOT appear.");
			logFileWriter.newLine();
			shouldBePresent = false;
		}//end if - the Transfer Date is not empty
		else {
			logFileWriter.write("iJournal textfield is empty - it will not prevent the 'Send' button from appearing");
			logFileWriter.newLine();
		}//end else - Transfer Date is empty
		
		
		String fiscalYear = line.getValueForField(CommitmentTransferLine.FiscalYear, logFileWriter);
		logFileWriter.write("Fiscal Year is determined to be '"+fiscalYear+"'");
		logFileWriter.newLine();
		String yearPrefix = "20";
		// if the Fiscal Year of the CLI is not one of the defined ones, the 'Send' Button should not appear
		boolean isInAcceptedFiscalYear = false;
		if (fiscalYear.equalsIgnoreCase(yearPrefix+HandSOnTestSuite.PPY)) isInAcceptedFiscalYear = true;
		else if (fiscalYear.equalsIgnoreCase(yearPrefix+HandSOnTestSuite.PY)) isInAcceptedFiscalYear = true;
		else if (fiscalYear.equalsIgnoreCase(yearPrefix+HandSOnTestSuite.FY)) isInAcceptedFiscalYear = true;
		else if (fiscalYear.equalsIgnoreCase(yearPrefix+HandSOnTestSuite.NY)) isInAcceptedFiscalYear = true;
		else if (fiscalYear.equalsIgnoreCase(yearPrefix+HandSOnTestSuite.NNY)) isInAcceptedFiscalYear = true;
		else if (fiscalYear.equalsIgnoreCase(yearPrefix+HandSOnTestSuite.currentNY)) isInAcceptedFiscalYear = true;
		else if (fiscalYear.equalsIgnoreCase(yearPrefix+HandSOnTestSuite.currentNextNextYear)) isInAcceptedFiscalYear = true;
		if (isInAcceptedFiscalYear) {
			logFileWriter.write("Fiscal Year is determined to be within the range acceptable for the Send Button to appear.");
			logFileWriter.newLine();
		}
		else {
			logFileWriter.write("Fiscal Year is determined NOT to be within the range acceptable for the Send Button to appear.");
			logFileWriter.newLine();
		}
		if (! isInAcceptedFiscalYear)
			shouldBePresent = false;
		
		int[] valuesArray = {CommitmentTransferLine.ExpenseArea, CommitmentTransferLine.SubCategory};
		
		if (line.getValueForField(CommitmentTransferLine.ExpenseArea, logFileWriter).equalsIgnoreCase("select")) {
			shouldBePresent = false;
			logFileWriter.write("Expense Area is not selected - this should prevent the Send Button from appearing");
			logFileWriter.newLine();
		}//end if - Expense Area check
		
		if (line.getValueForField(CommitmentTransferLine.SubCategory, logFileWriter).equalsIgnoreCase("select")) {
			shouldBePresent = false;
			
			
			logFileWriter.write("SubCategory is not selected - this should prevent the Send Button from appearing");
			logFileWriter.newLine();
		}//end if - SubCategory check
	

		if (shouldBePresent) {
			logFileWriter.write("Data in the CLI is right for the Send Button to appear.");
			logFileWriter.newLine();
		}// end if
		else {
			logFileWriter.write("Data within the CLI is NOT right for the 'Send' Button to appear");
			logFileWriter.newLine();
		}
		return shouldBePresent;
	}
	
	public void acceptAlertAndReportMessage (BufferedWriter logFileWriter) throws Exception{
		try{
			Alert alert = UIController.driver.switchTo().alert();
			String alertText = alert.getText();
			logFileWriter.write("Alert appears as expected - alert text is "+alertText);
			logFileWriter.newLine();
			alert.accept();
		}//end try clause
		catch(Exception e){
			logFileWriter.write("Could not dismiss alert because of Exception "+e.getMessage());
			logFileWriter.newLine();
		}//end catch clause
		
	}//end acceptAlertAndReportMessage method
	
	public void testButtonAccessibility(String locator, boolean shouldBeAccessible, BufferedWriter logFileWriter) throws Exception{
		
		logFileWriter.write("*** testButtonAccessibility called with expected accessibility of Send Button being '"
						+shouldBeAccessible+"'");
		logFileWriter.newLine();
		
		boolean isActuallyPresent = isButtonPresent(locator, logFileWriter);
		logFileWriter.newLine();
		logFileWriter.write("Actual Accessibility of the 'Send' button: "+isActuallyPresent);
		logFileWriter.newLine();
		
		if (isActuallyPresent == shouldBeAccessible) {
			if (shouldBeAccessible)//it should be present and it is
				logFileWriter.write("Send Button located, as expected.");
			else//Send button should not be present
				logFileWriter.write("Send Button is not present, as expected.");
			logFileWriter.newLine();
		}//end if - things are as they should be
		else {
			if (shouldBeAccessible) {//the button should be there, and it's not
				String message = "**** ERROR: Send Button unexpectedly not present;\n";
				logFileWriter.write(message);
				verificationErrors.append(message);
			}//end inner if
			else {//the button should not be there, but it is
				String message = "**** ERROR: Send Button unexpectedly present;\n";
				logFileWriter.write(message);
				verificationErrors.append(message);
			}//end inner else
		}//end else - sendButton not located		
	}//end testButtonAccessibility method

	public boolean isButtonPresent(String locator, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** isButtonPresent called with locator '"	+ locator + "'");
		logFileWriter.newLine();
		
		logFileWriter.write("First, determine if the Send Button column is even present");
		logFileWriter.newLine();
		
		try{
			WebElement sendButtonColumnHeader = UIController.driver.findElement(By.linkText("Send  to  iJournal"));
			logFileWriter.write("Send Button column header acquired: Link Text is "+sendButtonColumnHeader.getAttribute("text"));
			logFileWriter.newLine();
		}
		catch(Exception e) {
			logFileWriter.write("Could not find the Send Button column - exiting");
			logFileWriter.newLine();
			return false;
		}
		
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(locator, logFileWriter);
		WebElement sendButton = null;
		try {
			sendButton = UIController.driver.findElement(by);
			logFileWriter.write("Found the Send Button.");
			logFileWriter.newLine();
		}
		catch (Exception e) {
			logFileWriter.write("*** Could NOT find the Send Button. ****");
			logFileWriter.newLine();
			return false;
		}
		if (UIController.isElementPresent(by)){
			logFileWriter.write("The 'Send' Button is present");
			logFileWriter.newLine();
		}//end if
		else {
			String message = "**** The 'Send' Button is NOT present ****";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			//verificationErrors.append(message);
			return false;
		}//end else
		if (sendButton.isEnabled()){
			logFileWriter.write("The 'Send' Button is enabled");
			logFileWriter.newLine();
		}//end if
		else {
			String message = "**** The 'Send' Button is NOT enabled ****";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			//verificationErrors.append(message);
			return false;
		}//end else
		
		if (sendButton.isDisplayed()) {
			logFileWriter.write("The 'Send' Button is displayed");
			logFileWriter.newLine();
		}//end if
		else {
			String message = "**** The 'Send' Button is NOT displayed ****";
			logFileWriter.write(message);
			logFileWriter.newLine();
			System.out.println(message);
			//verificationErrors.append(message);
			return false;
		}//end else
		
		//if we've gotten this far, it has passed all of the tests
		return true;
	}//end isButtonPresent method
	
	

	
/*
	private class CommitmentTransferLine {
		public String[] values;
		public int row = 1;
		public static final int SubCategory = 0;
		public static final int FiscalYear = 1;
		public static final int FundingUnit = 2;
		public static final int SourcePTA = 3;
		public static final int DestinationPTA = 4;
		public static final int ExpenseArea = 5;
		public static final int SummerNinths = 6;
		public static final int Amount = 7;
		public static final int Benefits = 8;
		public static final int TotalAmount = 9;
		public static final int TransferStatus = 10;
		public static final String myWorksheet = "Commitment Transfer Line";
		
		public CommitmentTransferLine(int row) throws Exception{
			values = new String[11];
			this.row = row;
			initialize();
		}//end constructor
				
		//this should only be used when we are on the Commitments Page
		public void initialize() throws Exception{
			String baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName (myWorksheet, "SubCategorySelect", logFileWriter);
			String amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			By by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			values[SubCategory] = HandSOnTestSuite.searchAuthorizationUI.getSelectedTextForBy(by, "SubCategorySelect", logFileWriter);
			
			baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName (myWorksheet, "FiscalYearSelect", logFileWriter);
			amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			values[FiscalYear] = HandSOnTestSuite.searchAuthorizationUI.getSelectedTextForBy(by, "FiscalYearSelect", logFileWriter);
			
			baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName (myWorksheet, "FundingUnitSelect", logFileWriter);
			amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			values[FundingUnit] = HandSOnTestSuite.searchAuthorizationUI.getSelectedTextForBy(by, "FundingUnitSelect", logFileWriter);

			baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName (myWorksheet, "SourcePTATextField", logFileWriter);
			amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			values[SourcePTA] = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);

			baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName (myWorksheet, "DestinationPTATextField", logFileWriter);
			amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			values[DestinationPTA] = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);

			baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName (myWorksheet, "ExpenseAreaSelect", logFileWriter);
			amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			values[ExpenseArea] = HandSOnTestSuite.searchAuthorizationUI.getSelectedTextForBy(by, "ExpenseAreaSelect", logFileWriter);

			baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName (myWorksheet, "SummerNinthsTextField", logFileWriter);
			amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			values[SummerNinths] = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);

			baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName (myWorksheet, "AmountTextField", logFileWriter);
			amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			values[Amount] = HandSOnTestSuite.searchAuthorizationUI.getStringValueFromElement(by, logFileWriter);

			baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName (myWorksheet, "BenefitsLabel", logFileWriter);
			amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			values[Benefits] = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);

			baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName (myWorksheet, "TotalAmountLabel", logFileWriter);
			amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			values[TotalAmount] = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);

			baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName (myWorksheet, "TransferStatusSelect", logFileWriter);
			amendedLocator = baseLocator.split("=")[0]+"="+HandSOnTestSuite.searchAuthorizationUI.getAmendedLocator(baseLocator.split("=")[1], row, logFileWriter);
			by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(amendedLocator, logFileWriter);
			values[TransferStatus] = HandSOnTestSuite.searchAuthorizationUI.getSelectedTextForBy(by, "TransferStatusSelect", logFileWriter);
		}//end initialize method
		
		public void compareToTransferLine(CommitmentTransferLine toCompareWith, 
				StringBuffer verificationErrors, BufferedWriter logFileWriter) throws Exception{
			for (int i=0; i<values.length; i++){
				String whichValue = new String();
				switch (i){
				case 0: whichValue = "SubCategory";
				break;
				case 1: whichValue = "Fiscal Year";
				break;
				case 2: whichValue = "Funding Unit";
				break;
				case 3: whichValue = "Source PTA";
				break;
				case 4: whichValue = "Destination PTA";
				break;
				case 5: whichValue = "Expense Area";
				break;
				case 6: whichValue = "Summer Ninths";
				break;
				case 7: whichValue = "Amount";
				break;
				case 8: whichValue = "Benefits";
				break;
				case 9: whichValue = "Total Amount";
				break;
				case 10: whichValue = "Transfer Status";
				break;
				}//end switch
				if (this.values[i].equalsIgnoreCase(toCompareWith.values[i])){
					logFileWriter.write(whichValue+" at row "+row+" is as expected");
					logFileWriter.newLine();
				}//end if
				else{
					String message = "ERROR - "+whichValue+" at row "+row+" expected value of "+this.values[i]
							+", but actual value is "+toCompareWith.values[i]+";\n ";
					logFileWriter.write(message);
					logFileWriter.newLine();
					verificationErrors.append(message);
					System.out.println(message);
				}//end else
			}//end for loop
		}//end compareToTransferLine method
	}//end private Class
*/
}//end Class
