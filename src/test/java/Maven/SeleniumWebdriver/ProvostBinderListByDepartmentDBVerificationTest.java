package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ProvostBinderListByDepartmentDBVerificationTest extends ProvostBinderListDBVerificationTest{


	private static final String[] sheetColumns = {"Name", "Rank", "Appt Type", "Dept FTE",
		"15-16 Final Salary", "16-17 Salary"};


	@Before
	public void setUp() throws Exception {
		myExcelReader = TestFileController.ProvostBinderListByDepartment;
		testName = "ProvostBinderListByDepartmentDBTest";
		testEnv = "Test";
		testCaseNumber = "049";
		testDescription = "DB Test of Provost Binder List By Department";
		firstDataRow = 14;
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}//end tearDown method

	@Test
	public void test() throws Exception {
		String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		BufferedWriter logFileWriter = TestFileController.getLogFileWriter(logFileName);
		logFileWriter.write("Now executing "+ testName);
		logFileWriter.newLine();
		
		myConnection = getConnection (logFileWriter);
		runStoredProcedure (myConnection, logFileWriter);
		String[] departments = getDepts(myConnection, logFileWriter);
		verifyValuesByDept(myConnection, departments, logFileWriter);
		
		logFileWriter.close();
	}//end test method

	private String[] getDepts(Connection conn, BufferedWriter logFileWriter) throws Exception{
		String[] depts = null;
		Statement stmt = null;
		try{
	        stmt=conn.createStatement();
	        String query = "SELECT DISTINCT ORG_NM FROM PROVOST_BINDER ORDER BY ORG_NM ";
	
	        //get the results set
	        ResultSet rs=stmt.executeQuery(query);  
	        ArrayList<String> list = new ArrayList<String>();
	        while(rs.next()){
	        	String dept = rs.getString("ORG_NM");
	        	list.add(dept);
	        	//logFileWriter.write("Adding dept " + dept +" to the list");
	        	//logFileWriter.newLine();
	        }//end while loop
	        if (! list.isEmpty()){
	        	depts = new String[list.size()];
	        	for (int i=0; i<depts.length; i++){
	        		depts[i] = (String)list.get(i);
	        	}
	        }//end if
		}//end try clause
	    catch (SQLException e) {
	        StackTraceElement[] stackTrace = e.getStackTrace();
	        for (int i=0; i<stackTrace.length; i++){
	        	logFileWriter.write(stackTrace[i].toString());
	        	logFileWriter.newLine();
	        }//end for loop
	    }//end catch clause
		finally{
			if (stmt != null) stmt.close();
		}//end finally clause
		if (depts == null){
			logFileWriter.write("returning null list of departments");
			logFileWriter.newLine();
		}
		return depts;
	}
	
	private void verifyValuesByDept(Connection conn, String[] depts, 
				BufferedWriter logFileWriter) throws Exception{
		Statement stmt = null;
		ResultSet rs=null;
		try {
	        stmt=conn.createStatement();
	        String query = "SELECT * FROM PROVOST_BINDER "
	        		+"ORDER BY ORG_NM, RANK_SORT_ORDER DESC, RANK_DESC, lower(NAME)";
	        rs=stmt.executeQuery(query);
	        ResultSetMetaData rsmd = rs.getMetaData();
	        int columnCount = rsmd.getColumnCount();
	        logFileWriter.write("Number of columns derived from the database is " + columnCount);
	        logFileWriter.newLine();
	        logFileWriter.write("Column names: types from the database are as follows: ");
	        for (int i=1; i<=columnCount; i++){
	        	logFileWriter.write(rsmd.getColumnName(i) + ": " + rsmd.getColumnTypeName(i));
	        	if (i<columnCount) logFileWriter.write(", ");
	        }//end for loop
	        logFileWriter.newLine();

	        logFileWriter.write("Column names from the worksheet are as follows: ");
	        for (int i=0; i<sheetColumns.length; i++){
	        	logFileWriter.write(sheetColumns[i]);
	        	if (i<(sheetColumns.length - 1)) logFileWriter.write(", ");
	        }//end for loop
	        logFileWriter.newLine();
	        logFileWriter.newLine();
	        
	        int deptIndex = 0;
	        int sheetRow = firstDataRow;
	        while (rs.next()){
	        	String deptFromDB = rs.getString("ORG_NM");
	        	String NameOnCurrentWorksheetRow = myExcelReader.getCellData(depts[deptIndex], "Name", sheetRow);
	        	if (! deptFromDB.equalsIgnoreCase(depts[deptIndex])){
	        		if ((NameOnCurrentWorksheetRow == null) ||(NameOnCurrentWorksheetRow.isEmpty())){
	        			logFileWriter.write("Reached the end of the worksheet for Department '"
	        					+ depts[deptIndex] +"', going to the next worksheet");
	        			logFileWriter.newLine();
	        			if (verificationErrors.length() == 0){
	        				logFileWriter.write("No mismatches found for department " + depts[deptIndex]);
	        			}//end if - no errors so far
	        			else{
	        				logFileWriter.write("MISMATCHES ARE FOUND for department " + depts[deptIndex]);
	        			}//end else - there have been errors found
	        			logFileWriter.newLine();
	        			deptIndex++;
	        			sheetRow = firstDataRow;
	        		}//end if - we're at the end of the worksheet & going to the next worksheet
	        		else{
	        			String message = "ERROR: There is one additional faculty in the row,"
	        					+" but not in the DB: " + NameOnCurrentWorksheetRow
	        					+"(on the worksheet), for Department '" + depts[deptIndex]
	        					+"', Department according to the DB is '" + deptFromDB
	        					+"', and according to the DB, the faculty member is '"
	        					+ rs.getString("NAME") +"'";
	        			logFileWriter.newLine();
	        			logFileWriter.write(message);
	        			verificationErrors.append(message);
	        			return;
	        		}//end else - there is a mismatch - we've found a faculty member in the department worksheet but not in the DB
	        	}//end if
	        	for (int i=0; i<sheetColumns.length; i++){
	        		String actual = myExcelReader.getCellData(depts[deptIndex], sheetColumns[i], sheetRow);
	        		String expected = getExpectedValue(rs, i);
	        		if (! actual.equalsIgnoreCase(expected)){
	        			String message = "'" + expected + "' vs '" + actual + "' (MISMATCH), ";
	        			logFileWriter.write(message);
		        		logFileWriter.newLine();
		        		if (verificationErrors.length()==0)
		        			verificationErrors.append("FAILURE due to mismatch - first mismatch is "+ message);
	        		}//end if - there's a mismatch - don't output anything if everything matches
	        	}//end for
	        	sheetRow++;
	        }//end while

		}//end try clause
	    catch (SQLException e) {
	        StackTraceElement[] stackTrace = e.getStackTrace();
	        for (int i=0; i<stackTrace.length; i++){
	        	logFileWriter.write(stackTrace[i].toString());
	        	logFileWriter.newLine();
	        }//end for loop
	    }//end catch clause
		finally{
			if (stmt != null) stmt.close();
		}//end finally clause
	}//end verifyValuesByDept method
	
	private String getExpectedValue(ResultSet rs, int sheetColumnIndex) throws Exception{
		String expectedValue = new String();
		switch (sheetColumnIndex){
		case 0: expectedValue = rs.getString("NAME"); break;
		case 1: expectedValue = rs.getString("RANK_DESC");break;
		case 2: expectedValue = rs.getString("APPT_TYPE");break;
		case 3: expectedValue = Integer.toString(rs.getInt("FTE"))+"%";break;
		case 4: NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
				int py_salary = rs.getInt("FINAL_PY_SALARY");
				expectedValue = numberFormat.format(py_salary);
				break;
		case 5:	numberFormat = NumberFormat.getNumberInstance(Locale.US);
				int cy_salary = rs.getInt("CY_100PCTFTE_SALARY");
				expectedValue = numberFormat.format(cy_salary);
				break;
		}//end switch block
		
		return expectedValue;
	}//end getExpectedValue method

}//end Class
