package Maven.SeleniumWebdriver;

import java.util.TreeMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/*
 * (1) The figures that show up are separate sums for Dept Shares per 
 * the Regular Merit Pool and the Market Share Pool as well as the sum of all Pool 
 * contributions to the DO Share.  
 * (2) The figure that shows up on the Report is influenced by the 
 * FTE Percent of the faculty member.  When retrieving values from the Control Sheet
 * Detail Report, we should specify the boolean variable "adjustForFTE" in ASCTest 
 * (which determines whether or not to adjust for the FTE) to "true".
 * (3) Dean Richard Saller does not appear on any of the Salary Setting Reports, but the Senior
 * Associate Deans do.  So, the "excludeDeans" boolean variable won't work in this case.
 * It has to be set to "true".  We have to specify "Saller, Richard" as one particular Faculty
 * member in the ExpectedASCDataSet to be excluded in the "facultyToExclude" String array.
 * (4) Previous Year Commitments don't show up on any of the Salary Setting Reports 
 * (5) Of note, any "Dept" contributions from any source Pool other than the Market Pool 
 * or the Regular Merit Pool will show up under the "Regular Merit Pool Dept Share" 
 * column.  This is a known issue. 
 * Similar behavior occurs in the Pool Allocation Report and this has been filed as 
 * HANDSON-3251.  
 * I have updated HANDSON-3251 to include this issue with this Report.
 * This test will be designed to fail until HANDSON-3251 is fixed.  
 * (6) FYI, this report will want the total raise amount per faculty, not per faculty per dept.
 * Therefore, if a faculty member belongs to more than one department, we have to add up the raises
 * for the multiple departments.
 * (7) If a faculty member has appointments in multiple departments within the 
 * School of Humanities and Sciences, then that faculty member will appear on multiple lines 
 * within the Dean Salary Setting Report, one line per department that this faculty member 
 * has an appointment in.
 * (8) If a faculty member has multiple ASC's, then the sum of them will be displayed on
 * one line in the Dean Salary Setting Report, if these pertain to a single department. If 
 * multiple ASC's each refer to a different department that this faculty member has an 
 * appointment in, then each ASC will affect the line pertinent to the department 
 * corresponding to the department that this ASC pertains to.  This will happen *unless*
 * that ASC is a Previous Year Commitment.  In that case, it will not affect any lines in
 * the Dean Salary Setting Report.
 * (9) If a faculty member has two separate ASC's, and one of them comes from a Pool other than the 
 * Regular Merit Pool, and this pool is one of the pools that gets inappropriately placed in
 * the Regular Merit Pool (as per HANDSON-3251), and the other comes from the Regular Merit Pool,
 * what will appear on the Dean Salary Setting Worksheet is the sum of both the raise from the ASC 
 * that comes from the Regular Merit Pool and the raise that comes from the other Pool, 
 * and that sum will appear in the column that corresponds to the Regular Merit Pool.
 * (10) All Senior Associate Deans appear on this report, as well as any who are granted an
 * "Dean Worksheet" Override in Salary Setting.
 */

public final class DeanSalarySettingWorksheetASCTest extends
		SalarySettingReportASCTest {

	@Before
	public void setUp() throws Exception {
		configureOutput();
		configureInclusionCriteria();
		super.setUp();
	}//end setUp() method
	
	private void configureOutput() throws Exception{
		myExcelReader = TestFileController.DeanSalarySettingReport;
		mySheetName = TestFileController.DeanSalarySettingWorksheetName;
		rowForColumnNames = TestFileController.DeanSalarySettingWorksheetHeaderRow;
		verificationErrors = new StringBuffer();
		testName = "DeanSalarySettingReportASCTest";
		testEnv = "Test";
		testCaseNumber = "034";
		testDescription = "ASC Test of Dean Salary Setting Report";
		logFileName = "Dean Salary Setting Report ASC Test.txt";
		logFileWriter = TestFileController.getLogFileWriter(logDirectoryName  +"/" + logFileName);
	}//end configureOutput method
	
	private void configureInclusionCriteria(){
		criteria = new TreeMap<String, String>();
		criteria.put(ExpectedASCData.StringAttributeKeys[ExpectedASCData.StageIndex], 
				ASCTest.stages[ASCTest.KnownStageIndex]);
		criteria.put(ExpectedASCData.isDean, "true");
		
		facultyToExclude = new String[1];
		facultyToExclude[0] = "Saller, Richard";
		
		facultyToInclude = HandSOnTestSuite.overrideList.get(SalarySettingManager.DeanWorksheetOverrideLabel);
		
		configureBooleanInclusionCriteria();

	}//end configureInclusionCriteria method
	
	private void configureBooleanInclusionCriteria(){
		excludeMarketShare = false;
		excludeZeroDOShare = false;
		adjustForFTE = true;
		excludeDeans = false;
		backoutPYs = true;
	}//end configureBooleanInclusionCriteria method


	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void test() throws Exception{
		super.test();
	}

}
