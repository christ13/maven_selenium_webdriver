package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;

import org.openqa.selenium.By;

public class FacultySearchPageUIController extends UIController {
	
	public static final String FACULTY_SEARCH_PAGE_URL_SUBSTRING = "processManageFaculty.do?ACTION=showFacultySearch";

	public FacultySearchPageUIController() {
		// TODO Auto-generated constructor stub
	}
	
	public static void navigateToSearchPage(BufferedWriter logFileWriter) throws Exception{
		/*
		logFileWriter.write("Current URL is "+UIController.driver.getCurrentUrl());
		logFileWriter.newLine();
		*/
		if (switchWindowByURL(facultySearchPageURL, logFileWriter)) {//page is already open
			logFileWriter.newLine();
			logFileWriter.write("Faculty Search Page is already open - switch is successful");
			return;
		}//end if - no need for further action here
		else {//we have to open the page
			logFileWriter.write("Faculty Search Page needs to be opened");
			logFileWriter.newLine();
		}//end else
		logFileWriter.write("Now switching to the Operations Page: ");
		if (switchWindowByURL(startPage, logFileWriter)) {
			logFileWriter.newLine();
			logFileWriter.write("success");
			driver.findElement(By.linkText("Faculty Search")).click();
		}//end if
		else {
			logFileWriter.newLine();
			logFileWriter.write("FAIL");
		}//end else
		logFileWriter.newLine();
		logFileWriter.write("Now attempting to switch to the Faculty Search Page: ");
		if (switchWindowByURL(facultySearchPageURL, logFileWriter)) {//it got to the Faculty Search Page
			logFileWriter.newLine();
			logFileWriter.write("success");
		}			
		else {//it failed to get to the search page
			logFileWriter.newLine();
			logFileWriter.write("FAIL");
		}
		//either way, do a carriage return and line feed
		logFileWriter.newLine();
	}//end openSearchPage method
	
	/*
	 * This method will be universally called prior to initiating any search for suitable faculty.
	 * If the input String adminAppt is "NONE" then there are only two widgets that are worked with - 
	 * the Faculty Status (should be "Active") and the Appointment Rank (should be "Full Professor").
	 * If the adminAppt value is anything else (either "Department Chair" or "Senior Associate Dean")
	 * then there are additional widgets - the Administrative Appointment is selected in the "Role Type"
	 * multi-select (either "Department Chair" or "Senior Associate Dean" is selected) and the Start Date
	 * Range is specified ("From" 9/1 of the year two years prior to the current one to 9/1 of the current year)
	 * and the End Date Range is specified ("To" 8/31 of the current year to 8/31 to two years after the current
	 * FY ends).
	 */
	public static void adjustSearchParameters(BufferedWriter logFileWriter, String adminAppt) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("*** Entering Search Parameters for the Faculty Search ***");
		logFileWriter.newLine();
		Thread.sleep(2000);
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Search Page", "ClearSearchButton", logFileWriter);
		//first, clear the previous search parameters, if applicable
		waitAndClick(by);
		Thread.sleep(2000);
		//now, we can enter the new search parameters
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Search Page", "StatusSelect", logFileWriter);
		if (selectElementInMenu(by, "Active")) {
			logFileWriter.write("Successfully selected 'Active' Status");
			logFileWriter.newLine();
		}//end if
		else {
			logFileWriter.write("Could not select 'Active' Status");
			logFileWriter.newLine();
		}//end else
		by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Search Page", "AppointmentRankSelect", logFileWriter);
		if (selectElementInMenu(by, "Full Professor")) {
			logFileWriter.write("Successfully selected 'Full Professor' Appointment Rank");
			logFileWriter.newLine();
		}//end if
		else {
			logFileWriter.write("Could not select 'Full Professor' Rank");
			logFileWriter.newLine();
		}//end else

		if (adminAppt.equalsIgnoreCase("NONE")) {
			logFileWriter.write("There is no administrative appointment to be selected for this Faculty member");
			logFileWriter.newLine();
		}//end if
		else {//do this if there is an Administrative Appointment
			logFileWriter.write("Administrative Appointment to be seleted for this Faculty member is "+adminAppt);
			logFileWriter.newLine();
			by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Search Page", "AdminApptRoleSelect", logFileWriter);
			if (selectElementInMenu(by, adminAppt)) {
				logFileWriter.write("Successfully selected '"+adminAppt+"' Administrative Appointment Role");
				logFileWriter.newLine();
			}//end if
			else {
				logFileWriter.write("Could not select '"+adminAppt+"' Administrative Appointment Role");
				logFileWriter.newLine();
			}//end else - Administrative Appointment Role could not be selected
			String StartFromDate = "09/01/2018";
			String StartToDate = "09/01/2020";
			String EndFromDate = "08/31/2021";
			String EndToDate = "08/31/2023";
			
			writeToTextField (logFileWriter, "StartDateFromTextField", StartFromDate);
			writeToTextField (logFileWriter, "StartDateToTextField", StartToDate);
			writeToTextField (logFileWriter, "EndDateFromTextField", EndFromDate);
			writeToTextField (logFileWriter, "EndDateToTextField", EndToDate);
			
		}//end outer else - selecting an Administrative Appointment
	}//end adjustSearchParameters method
	
	private static void writeToTextField(BufferedWriter logFileWriter, String nameOfTextField, String toBeWritten) throws Exception{
		logFileWriter.write("Writing "+toBeWritten+" to the "+nameOfTextField+" for this Faculty member");
		logFileWriter.newLine();
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Search Page", nameOfTextField, logFileWriter);
		UIController.driver.findElement(by).sendKeys(toBeWritten);
		Thread.sleep(2000);
	}//end writeToTextField method
	
	public static void initiateSearch(BufferedWriter logFileWriter, String adminAppt) throws Exception {
		adjustSearchParameters(logFileWriter, adminAppt);
		By by = HandSOnTestSuite.searchAuthorizationUI.getByForName("Faculty Search Page", "FindButton", logFileWriter);
		if (waitAndClick(by)) {
			logFileWriter.write("Successfully clicked on 'Find' button");
			logFileWriter.newLine();
		}//end if
		else {
			logFileWriter.write("Could not click on the 'Find' button");
			logFileWriter.newLine();
		}//end else
	}//end initiateSearch method
	
	public static String getModifiedLocator(String locator, int increment, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("**** getModifiedLocator method is being called *****");
		logFileWriter.newLine();
		logFileWriter.write("The existing locator is '"+locator+"'");
		logFileWriter.newLine();
		logFileWriter.write("... and the increment is "+increment);
		logFileWriter.newLine();

		System.out.println("**** getModifiedLocator method is being called *****");
		System.out.println("The existing locator is '"+locator+"'");
		System.out.println("... and the increment is "+increment);

		String searchString = "tr";
		
		//first we find out where the problematic tr value is in the String - get the index
		int from_index = 0;
		int last_tr_index = 0;
		last_tr_index = locator.indexOf(searchString);
		while (from_index != -1) {
    		from_index = locator.indexOf(searchString, last_tr_index+1);
    		System.out.println("Now searching for tr string at index "+from_index);
    		logFileWriter.write("Now searching for tr string at index "+from_index);
    		logFileWriter.newLine();
    		if (from_index != -1)
    			last_tr_index = from_index;
		}//end while
		//now, get the value of the row that the problematic tr is pointing to
		last_tr_index = last_tr_index + 3;
		System.out.println("Within the locator String, we believe that the number we want to replace is at index "+last_tr_index);
		logFileWriter.write("Within the locator String, we believe that the number we want to replace is at index "+last_tr_index);
		logFileWriter.newLine();
		int tr_row = 0;
		String tr_row_stringValue = new String();
		try {
			tr_row_stringValue = String.valueOf(locator.charAt(last_tr_index));
			System.out.println("Trying to parse "+tr_row_stringValue+" into a Number");
			logFileWriter.write("Trying to parse "+tr_row_stringValue+" into a Number");
			logFileWriter.newLine();
			tr_row = Integer.parseInt(tr_row_stringValue);
		}
		catch(NumberFormatException e) {
			System.out.println("We couldn't format "+tr_row_stringValue+" into a number - this method FAILED");
			logFileWriter.write("We couldn't format "+tr_row_stringValue+" into a number - this method FAILED");
			logFileWriter.newLine();
		}
		String modifiedLocator = new String();
		if (tr_row > 0) {//execute the following if and only if we have successfully extracted the row number of the tr.
			System.out.println("We have successfully parsed '"+tr_row_stringValue+"' into a number");
			logFileWriter.write("We have successfully parsed '"+tr_row_stringValue+"' into a number");
			logFileWriter.newLine();

			tr_row = tr_row + increment;
			tr_row_stringValue = String.valueOf(tr_row);
			modifiedLocator = locator.substring(0, last_tr_index)+tr_row_stringValue+locator.substring(last_tr_index+1);
			System.out.println("Modified locator is the following: " + modifiedLocator);
			logFileWriter.write("Modified locator is the following: " + modifiedLocator);
			logFileWriter.newLine();
		}//end if - we have extracted the tr
		return modifiedLocator;
	}//end getModifiedLocator method
	
	
	/*
	 * This method will select a valid Faculty member based on whether or not the Faculty member's Administrative Appointment
	 * matches the requirement of the test case
	 */
	public static String selectValidFaculty (BufferedWriter logFileWriter, String testCaseID, String adminAppt) throws Exception {
		logFileWriter.write("*** method selectValidFaculty is called ***");
		logFileWriter.newLine();
		String baseLocator = HandSOnTestSuite.searchAuthorizationUI.getLocatorByName("Faculty Search Page", "SearchResultsFirstRowLink", logFileWriter);
		logFileWriter.write("Base Locator of the first row for the HandSOn ID is "+baseLocator);
		logFileWriter.newLine();
		//before we pass the baseLocator to getBottomRow, we need to trim off the prefix 'xpath=' from the baseLocator
		int bottomRow = HandSOnTestSuite.searchAuthorizationUI.getBottomRow(baseLocator.substring(baseLocator.indexOf("=")+1), new String(), 3, logFileWriter, true);
		logFileWriter.write("Bottom-most row in the search results is "+bottomRow);
		logFileWriter.newLine();
		int rowIncrement = 0;
		String validFacultyName = new String();
		while((rowIncrement+2)<bottomRow && validFacultyName.length()==0) {
			String modifiedLocator = getModifiedLocator(baseLocator, rowIncrement++, logFileWriter);
			By by = HandSOnTestSuite.searchAuthorizationUI.getByForLocator(modifiedLocator, logFileWriter);
			String handSOnID = UIController.driver.findElement(by).getText();
			openFacultyPage(logFileWriter, by);
			if (FacultyUIController.isValidFacultyData(adminAppt, logFileWriter)) {
				String message = "Derived HandSOnID value of '"+handSOnID+"' for valid Faculty";
				logFileWriter.write(message); 
				logFileWriter.newLine();
				System.out.println(message);
				TestDataRecorder.setTestCaseData(testCaseID, TestDataRecorder.highLevelDataColumns[TestDataRecorder.HANDSONID_INDEX], handSOnID);
				validFacultyName = FacultyUIController.processFacultyAsValid (logFileWriter);
				FacultyUIController.closeCurrentTab(logFileWriter);
				return validFacultyName;
			}//end if - Faculty is valid
			else
				FacultyUIController.closeCurrentTab(logFileWriter);
			
			//Go back to the Faculty Search Page
			if (switchWindowByURL(facultySearchPageURL, logFileWriter)) {//it got to the Faculty Search Page
				logFileWriter.write("Successfully switched back to the Faculty Search Page");
				logFileWriter.newLine();
			}//end if

		}//end while - incrementing the row iteratively
		return validFacultyName;
	}//end selectValidFaculty method
	

	
	public static void openFacultyPage (BufferedWriter logFileWriter, By by) throws Exception{
		String linkText = HandSOnTestSuite.searchAuthorizationUI.getTextFromElement(by, logFileWriter);
		if (waitAndClick(by)) {
			logFileWriter.write("Successfully clicked on the link to open the Faculty Detail Record link with text "+linkText);
			logFileWriter.newLine();
		}//end if - opened the Faculty Detail Record
		else {
			logFileWriter.write("Could not click on the link to open the Faculty Detail Record with link text "+linkText);
			logFileWriter.newLine();
		}//end else - could not open Faculty Detail Record
		Thread.sleep(5000);
		if (switchWindowByURL("processManageFaculty.do?ACTION=load&facultyDTO.unvlId="+linkText, logFileWriter)) {
			logFileWriter.write("Got to the Faculty Window through link "+linkText);
			logFileWriter.newLine();
		}//end if - switching to the Faculty Record
		else {
			logFileWriter.write("Could not get to the Faculty window");
			logFileWriter.newLine();
		}//end else
	}//end openFacultyPage method
	

}//end Class
