package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;
import java.util.ArrayList;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;




public class SalarySettingTabUIController extends UIController {
	
	private static HSSFExcelReader myExcelReader;
	private static final String myWorksheetName = "Salary Setting Tab";
	private static int finalCYIndex = 0;
	private static int nameRightmostColumnIndex = 0;
	private static int rankRightmostColumnIndex = 0;
	private static int raiseAmountColumnIndex = 0;
	private static ArrayList<Integer> editableFieldIndices;

	public SalarySettingTabUIController() {
		myExcelReader = TestFileController.fileIn;
		editableFieldIndices = new ArrayList<Integer>();
	}//end constructor
	
	public boolean modifyUIValues() throws Exception{
		boolean wasModified = false;
		
		return wasModified;
	}//end modifyUIValues method
	
	
	public String downloadDepartmentSSW(String clusterName, String deptName, 
			BufferedWriter logFileWriter) throws Exception{
		String message = new String();

		if (switchWindowByURL(UIController.salarySettingURL, logFileWriter)){
			logFileWriter.write("successfully acquired Salary Setting Window with URL " + UIController.clusterReportDialogURL);
			logFileWriter.newLine();			
		}
		else {
			message = "Could not acquire the Salary Setting Window with URL " + UIController.clusterReportDialogURL +" \n";
			logFileWriter.write(message); 
			logFileWriter.newLine();
			return message;
		}//end else

		int sheetRow = myExcelReader.getCellRowNum(myWorksheetName, "Name", "DeptWorksheetDownloadButton");
		String locator = myExcelReader.getCellData(myWorksheetName, "Locator", sheetRow);
		waitForElementPresent(By.xpath(locator));
		waitAndClick(By.xpath(locator), 3);
		Thread.sleep(5000);//wait for the dialog to appear
		
		if (switchWindowByURL(UIController.deptReportDialogURL, logFileWriter)){
			logFileWriter.write("successfully acquired download dialog with URL " + UIController.deptReportDialogURL);
			logFileWriter.newLine();
		}//end if
		else {
			message = "Could not acquire the dialog with URL " + UIController.deptReportDialogURL;
			logFileWriter.write(message); 
			logFileWriter.newLine();
			return message;
		}//end else
		//assuming that we've got the dialog, do the download
		sheetRow = myExcelReader.getCellRowNum(myWorksheetName, "Name", "GenerateButtonOnDownloadDialog");
		locator = myExcelReader.getCellData(myWorksheetName, "Locator", sheetRow);
		waitForElementPresent(By.xpath(locator));
		waitAndClick(By.xpath(locator), 3);
		String worksheetName = "Department Salary Setting Worksheet " + deptName +".xlsm";
		TestFileController.copyFile(worksheetName, logFileWriter, ReportRunner.logDirectoryName);
		
		return message;
	}//end downloadDepartmentSSW method
	
	public String downloadClusterSSW(String clusterName, BufferedWriter logFileWriter) throws Exception{
		String message = new String();
		
		int sheetRow = myExcelReader.getCellRowNum(myWorksheetName, "Name", "ClusterWorksheetDownloadButton");
		String locator = myExcelReader.getCellData(myWorksheetName, "Locator", sheetRow);
		if (switchWindowByURL(UIController.salarySettingURL, logFileWriter)){
			logFileWriter.write("successfully acquired Salary Setting Window with URL " + UIController.clusterReportDialogURL);
			logFileWriter.newLine();			
		}
		else {
			message = "Could not acquire the Salary Setting Window with URL " + UIController.clusterReportDialogURL +" \n";
			logFileWriter.write(message); 
			logFileWriter.newLine();
			return message;
		}//end else
		waitForElementPresent(By.xpath(locator));
		waitAndClick(By.xpath(locator), 3);
		
		Thread.sleep(5000);//wait for the dialog to appear
		
		if (switchWindowByURL(UIController.clusterReportDialogURL, logFileWriter)){
			logFileWriter.write("successfully acquired download dialog with URL " + UIController.clusterReportDialogURL);
			logFileWriter.newLine();
		}//end if
		else {
			message = "Could not acquire the dialog with URL " + UIController.clusterReportDialogURL +" \n";
			logFileWriter.write(message); 
			logFileWriter.newLine();
			return message;
		}//end else
		//assuming that we've got the dialog, do the download
		sheetRow = myExcelReader.getCellRowNum(myWorksheetName, "Name", "GenerateButtonOnDownloadDialog");
		locator = myExcelReader.getCellData(myWorksheetName, "Locator", sheetRow);
		waitForElementPresent(By.xpath(locator));
		waitAndClick(By.xpath(locator), 3);
		String worksheetName = "Cluster Salary Setting Worksheet " + clusterName +".xlsx";
		TestFileController.copyFile(worksheetName, logFileWriter, ReportRunner.logDirectoryName);
		
		return message;
	}//end downloadClusterSSW method

	public String downloadSchoolSSW(String clusterName, BufferedWriter logFileWriter) throws Exception{
		String message = new String();		
		int sheetRow = myExcelReader.getCellRowNum(myWorksheetName, "Name", "SchoolWorksheetDownloadButton");
		String locator = myExcelReader.getCellData(myWorksheetName, "Locator", sheetRow);
		waitForElementPresent(By.xpath(locator));
		waitAndClick(By.xpath(locator), 3);
		
		if (switchWindowByURL(UIController.schoolReportDialogURL, logFileWriter)){
			logFileWriter.write("successfully acquired download dialog with URL " + UIController.schoolReportDialogURL);
			logFileWriter.newLine();
		}//end if
		else {
			message = "Could not acquire the dialog with URL " + UIController.schoolReportDialogURL;
			logFileWriter.write(message); 
			logFileWriter.newLine();
			return message;
		}//end else

		new Select(driver.findElement(By.xpath("//select"))).deselectAll();
		Thread.sleep(2000);
		new Select(driver.findElement(By.xpath("//select"))).selectByVisibleText(clusterName);
		sheetRow = myExcelReader.getCellRowNum(myWorksheetName, "Name", "GenerateButtonOnDownloadDialog");
		locator = myExcelReader.getCellData(myWorksheetName, "Locator", sheetRow);
		waitForElementPresent(By.xpath(locator));
		waitAndClick(By.xpath(locator), 3);
		String worksheetName = "School Salary Setting Worksheet " + clusterName +".xlsx";
		TestFileController.copyFile(worksheetName, logFileWriter, ReportRunner.logDirectoryName);
		
		return message;
	}//end downloadSchoolSSW method

	public String adjustSalarySettingConfiguration(String clusterName, String deptName, 
			BufferedWriter logFileWriter) throws Exception{
		String message = new String();
		if (! HandSOnTestSuite.salarySettingTabUI.selectReportConfiguration(clusterName, deptName, logFileWriter)){
			message = "Could not select the report configuration with the following values: " 
							+ clusterName + ", " + deptName;
			logFileWriter.write(message);
		}//end if
		else {
			logFileWriter.write("Successfully selected the report configuration with the following values: "
					+ clusterName + ", " + deptName);
		}//end else
		logFileWriter.newLine();
		return message;
	}//end adjustSalarySettingConfiguration method
	
	public boolean selectReportConfiguration(String clusterName, BufferedWriter logFileWriter) throws Exception{
		return selectReportConfiguration(clusterName, "Select", logFileWriter);
	}
	
	public boolean selectReportConfiguration(String clusterName, String deptName, BufferedWriter logFileWriter) throws Exception{
		boolean isSelected = selectReportConfiguration(clusterName, deptName, "Select", logFileWriter);
		String deptNameLocator = getLocator("DepartmentColumnTopRow", 1, logFileWriter);
		String deptNameDisplayed = this.getTextFromElement(By.xpath(deptNameLocator));
		logFileWriter.write("Verifying that the department name displayed is "+deptName+" as it should be");
		logFileWriter.newLine();
		if (! deptNameDisplayed.equalsIgnoreCase(deptName))
			isSelected = selectReportConfiguration(clusterName, deptName, "Select", logFileWriter);//call it again if it didn't work the first time
		return isSelected;
	}
	
	public boolean selectReportConfiguration(String clusterName, String deptName, String facultyName, BufferedWriter logFileWriter) throws Exception{
		return selectReportConfiguration(clusterName, deptName, facultyName, "Any", logFileWriter);
	}
	
	public boolean selectReportConfiguration(String clusterName, String deptName, String facultyName, String roleName, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method selectReportConfiguration called***");
		logFileWriter.newLine();
		boolean configSelected = false;
		configSelected = reportOnMenuSelection("Cluster", By.id("clusterLevel"), clusterName, logFileWriter);
		if (! configSelected) return configSelected;
		configSelected = false;
		configSelected= reportOnMenuSelection("Department", By.id("deptLevel"), deptName, logFileWriter);
		if (! configSelected) return configSelected;
		configSelected = false;
		configSelected = reportOnMenuSelection("Faculty", By.id("faculty"), facultyName, logFileWriter);
		if (! configSelected) return configSelected;
		configSelected = false;
		configSelected = reportOnMenuSelection("Role", By.id("role"), roleName, logFileWriter);
		if (! configSelected) return configSelected;
		Thread.sleep(5000);
		waitAndClick(By.xpath("//tr[4]/td/table/tbody/tr/td[9]/a"));
		//waitAndClick(By.linkText("Generate"));
		Thread.sleep(60000);

		return configSelected;
	}//end selectReportConfiguration method
	
	public void acceptTwoAlerts(BufferedWriter logFileWriter) throws Exception{
		try{
			Alert alert = UIController.driver.switchTo().alert();
			logFileWriter.write("Alert is present - text is "+alert.getText());
			logFileWriter.newLine();
			alert.accept();
			logFileWriter.write("Alert has been accepted");
			logFileWriter.newLine();
		}
		catch(Exception e){
			logFileWriter.write("Could not acquire alert because of Exception "+e.getMessage());
			logFileWriter.newLine();
		}
		Thread.sleep(6000);
		try{
			Alert alert = UIController.driver.switchTo().alert();
			logFileWriter.write("Second Alert is present - text is "+alert.getText());
			logFileWriter.newLine();
			alert.accept();
			logFileWriter.write("Second Alert has been accepted");
			logFileWriter.newLine();
		}//end try
		catch(Exception e){
			logFileWriter.write("Could not acquire second alert because of Exception "+e.getMessage());
			logFileWriter.newLine();
		}//end catch

	}//end method acceptTwoAlerts
	

	
	public String getLocatorNameFromRowNumber(int rowNum, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method getLocatorNameFromRowNumber called ***");
		logFileWriter.newLine();
		logFileWriter.write("Received spreadsheet row '" + rowNum +"' as input, ");
		String locatorName = myExcelReader.getCellData(myWorksheetName, "Name", rowNum);
		logFileWriter.write("Returning locator name " + locatorName);
		logFileWriter.newLine();
		return locatorName;
	}//end getLocatorNameFromRowNumber method
	
	/*
	 * There is additional functionality that has to be built into this.  The
	 * final current year salary can be in a variety of different places.  
	 * We need to (a) search through the column headers, using the base locator 
	 * value //table[@id='td2_table']/tbody/tr[3]/td[##] <-- ## is the variable.
	 * ## can be one of several discrete values - 29, 30, 32, 33, 34, 43.  It
	 * always has "16-17 H&S 100% Equivalent" as the column header.  To find 
	 * out which one it is, we can't read the column headers - WebDriver reads
	 * them all as blanks, unfortunately.  So, we have to iterate through all of 
	 * the columns, try to read a column to the right of the rightmost column, 
	 * let an Exception get thrown, and thereby find out the column number of the
	 * rightmost column.  The relevant Rank, Name and Final CY Salary values
	 * are always 2, 3, and 6 places to the right of the "Notes" column, which
	 * is always the rightmost column.
	 * 
	 * Once we know what the correct header values are, we put them into the
	 * base locator value //tr[3]/td[2]/div/table/tbody/tr/td[##] for the 
	 * current year salary, and for the name and for the rank.  
	 * 
	 * Whenever a new Salary Setting configuration is selected with a different
	 * Cluster or Department, we have to iterate through all of these values, 
	 * putting them into the base locator value and seeing whether or not the 
	 * label text at that locator equals the value under the "16-17 H&S 100% Equivalent"
	 * column.
	 * 
	 * 
	 */
	
	//call this method when bringing up faculty from another department
	public void updateDynamicIndices(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method updateDynamicIndices called ***");
		logFileWriter.newLine();
		editableFieldIndices = new ArrayList<Integer>();
		boolean noMatch = true;
		String CYColumnTitleBase = "//td[2]/div/table/tbody/tr[3]/td[##]";
		int i=28;
		for (i=28; i<55 && noMatch; i++){
			String indexStr = Integer.toString(i);
			String locatorStr = CYColumnTitleBase.replaceFirst("##", indexStr);
			String columnTitle = "Notes";
			try {
				  String foundText = driver.findElement(By.xpath(locatorStr)).getText();
				  logFileWriter.write("Found text '"+foundText+"' with locator '" 
						  				+ locatorStr +"', at index " + i);
				  logFileWriter.newLine();
				  if (foundText.equalsIgnoreCase(columnTitle)){
					  noMatch = false;
					  finalCYIndex = i-6;
					  break;
				  }
			} //end try clause
			catch (Exception e) {	
				logFileWriter.write("Exception thrown, end of the title row reached, aborting");
				finalCYIndex = i-7;
				break;
			}//end catch clause
			if (! noMatch) finalCYIndex = i;
		}//end for		
		raiseAmountColumnIndex = finalCYIndex - 4;
		nameRightmostColumnIndex = finalCYIndex + 2;
		rankRightmostColumnIndex = finalCYIndex + 3;
		logFileWriter.write("Found the index of CY Salary: " + finalCYIndex);
		logFileWriter.newLine();
		//now, find the indices for all of the editable text fields
		logFileWriter.write("Now adding all of the indices for editable text fields to modify");
		logFileWriter.newLine();

		int sheetRow = myExcelReader.getCellRowNum(myWorksheetName, "Name", "EditableFieldTopRow");
		String EditableFieldsLocatorBase = myExcelReader.getCellData(myWorksheetName, "Locator", sheetRow);
		logFileWriter.write("The following indices are added: ");
		for (i=10; i<50; i++){
			try{
				String indexStr = Integer.toString(i);
				String locatorStr = EditableFieldsLocatorBase.replaceFirst("##", indexStr);
				WebElement textfield = driver.findElement(By.xpath(locatorStr));
				textfield.clear();
				//textfield.sendKeys("1000");
				editableFieldIndices.add(new Integer(i));
			}//end try clause
			catch(Exception e){
				logFileWriter.newLine();
				logFileWriter.write("Exception "+e.getMessage() +" thrown at index " + i);
				logFileWriter.newLine();
			}//end catch clause
		}//end for loop
		logFileWriter.write("The following indices are known as editable: ");
		for (i=0; i<editableFieldIndices.size(); i++){
			if (i > 0){
				logFileWriter.write(", ");
			}//end if
			logFileWriter.write(((Integer)(editableFieldIndices.get(i))).toString());
		}//end for
		logFileWriter.newLine();
	}//end updateFinalCYIndex method
	
	public boolean updateUIValues(String toWrite, String UIRow, BufferedWriter logFileWriter) throws Exception{
		boolean writeSuccessful = true;
		int sheetRow = myExcelReader.getCellRowNum(myWorksheetName, "Name", "EditableFieldTopRow");
		String EditableFieldsLocatorBase = myExcelReader.getCellData(myWorksheetName, "Locator", sheetRow);

		for (int i=0; i<editableFieldIndices.size(); i++){
			String indexStr = Integer.toString(editableFieldIndices.get(i));
			String locatorStr = EditableFieldsLocatorBase.replaceFirst("##", indexStr);
			if (Integer.parseInt(UIRow) > 1){
				locatorStr = locatorStr.replaceFirst("/tbody/tr", "/tbody/tr["+UIRow+"]");
				logFileWriter.write("Appended UI Row - final locator is " + locatorStr);
				logFileWriter.newLine();
			}//end if - appending the UI Row if need be
			if (! super.writeStringToTextField(By.xpath(locatorStr), toWrite, logFileWriter))
				writeSuccessful = false;
		}//end for loop
		sheetRow = myExcelReader.getCellRowNum(myWorksheetName, "Name", "SaveButtonSalarySettingTab");
		String saveButtonLocator = myExcelReader.getCellData(myWorksheetName, "Locator", sheetRow);
		super.waitAndClick(By.xpath(saveButtonLocator), 3);
		return writeSuccessful;
	}//end updateUIValues method
	
	/*
	 * Given the locator of the leftmost column of a given row, this will return the 
	 * number of the rightmost column. 
	 */
	public int getRightmostColumn (String xpathBaseLocator, BufferedWriter logFileWriter, boolean writeToLogFile) throws Exception{
		int numberOfColumns = 0;
		
		String xpathLocator = xpathBaseLocator;
		boolean lastColumnReached = false;
		while (! lastColumnReached){
			numberOfColumns++;
			if (numberOfColumns > 1){
				xpathLocator = xpathBaseLocator.concat("[" + numberOfColumns +"]");
			}
			if (isElementPresent(By.xpath(xpathLocator))){
				if (writeToLogFile){
					logFileWriter.write("Column at " + numberOfColumns +" found, value is " 
							+ driver.findElement(By.xpath(xpathLocator)).getText());
					logFileWriter.newLine();
				}
			}//end if - element is found in this column
			else {
				if (numberOfColumns > 1){
					lastColumnReached = true;
					if (writeToLogFile){
						logFileWriter.write("End of row reached - rightmost column is " + numberOfColumns);
						logFileWriter.newLine();
					}
				}//if there's more than one column - presumably, the base xpath locator is valid
				else{
					logFileWriter.write("WARNING!!! Invalid base xpath locator given as input - returning invalid");
					return -1;
				}//end inner else - invalid base xpath locator
			}//end outer else - element not found
		}//end while 
		
		return numberOfColumns;
	}//end getRightmostColumn method
	
	
	public boolean verifyUIValues (String expected, String UIRow, BufferedWriter logFileWriter, boolean writeToLogFile) throws Exception{
		boolean valuesAsExpected = true;
		if (writeToLogFile){
			logFileWriter.write("*** method verifyUIValues called with expected String " + expected 
					+", and UI Row " + UIRow +" ***");
			logFileWriter.newLine();
		}//end if
		int sheetRow = myExcelReader.getCellRowNum(myWorksheetName, "Name", "EditableFieldTopRow");
		String EditableFieldsLocatorBase = myExcelReader.getCellData(myWorksheetName, "Locator", sheetRow);

		for (int i=0; i<editableFieldIndices.size(); i++){
			String indexStr = Integer.toString(editableFieldIndices.get(i));
			String locatorStr = EditableFieldsLocatorBase.replaceFirst("##", indexStr);
			if (Integer.parseInt(UIRow) > 1){
				locatorStr = locatorStr.replaceFirst("/tbody/tr", "/tbody/tr["+UIRow+"]");
				logFileWriter.write("Appended UI Row - final locator is " + locatorStr);
				logFileWriter.newLine();
			}//end if - appending the UI Row if need be
			String actual = getValueOfInputForLocator(locatorStr, logFileWriter, writeToLogFile).trim();
			if (! actual.equalsIgnoreCase(expected.trim())){
				valuesAsExpected = false;
				if (writeToLogFile){
					logFileWriter.write("MISMATCH - expected " + expected + ", but the actual value is " + actual);
					logFileWriter.newLine();
				}//end if - write to the log file
			}//end if - there's a mismatch
		}//end for loop
		
		return valuesAsExpected;
	}//end verifyUIValues method
	
	public static String getValueOfInputForLocator(String locator, BufferedWriter logFileWriter, boolean writeToLogFile) throws Exception{
		if (writeToLogFile){
			logFileWriter.write("*** method getValueOfInputForLocator called with locator "+ locator +"***");
			logFileWriter.newLine();
		}//write it only if the boolean value indicates this
		if (isElementPresent(By.xpath(locator))){
			return driver.findElement(By.xpath(locator)).getAttribute("value");
		}
		else return new String();
	}//end getValueOfInputForLocator method

	
	public String getLocator(String name, int UIrow, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method getLocator called ***");
		logFileWriter.newLine();
		String locator = new String();
		int rowNum = myExcelReader.getCellRowNum(myWorksheetName, "Name", name);
		locator = myExcelReader.getCellData(myWorksheetName, "Locator", rowNum);
		if (name.equalsIgnoreCase("FinalCYColumnTopRow"))
				locator = locator.replaceFirst("##", Integer.toString(finalCYIndex));
		else if (name.equalsIgnoreCase("FacultyNameRightmostColumnTopRow"))
			locator = locator.replaceFirst("##", Integer.toString(nameRightmostColumnIndex));
		else if (name.equalsIgnoreCase("RankRightmostColumnTopRow"))
			locator = locator.replaceFirst("##", Integer.toString(rankRightmostColumnIndex));
		else if (name.equalsIgnoreCase("FTEAdjRaiseTopRow"))
			locator = locator.replaceFirst("##", Integer.toString(raiseAmountColumnIndex));
		if (UIrow > 1){
			locator = locator.replaceFirst("tbody/tr/td", "tbody/tr[" 
					+ Integer.toString(UIrow) + "]/td");
		}//end if
		logFileWriter.write("Received name '" + name +"' as input, ");
		logFileWriter.write("Returning locator " + locator);
		logFileWriter.newLine();
		return locator;
	}//end getLocator method
	
	public void scrollTextIntoView(String xpathLocator, BufferedWriter logFileWriter) throws Exception{
		WebElement element = null;
		try{
			element = UIController.driver.findElement(By.xpath(xpathLocator));
			((JavascriptExecutor) UIController.driver).executeScript("arguments[0].scrollIntoView(true);", element);
			Thread.sleep(500);
		}
		catch(Exception e){
			logFileWriter.newLine();
			logFileWriter.write("Method scrollTextIntoView couldn't locate text with locator "+xpathLocator);
			logFileWriter.newLine();
			e.printStackTrace();
		}//end catch
		if ((element == null) || (element.getText().length() == 0)){
			logFileWriter.newLine();
			logFileWriter.write("Method scrollTextIntoView couldn't locate text with locator "+xpathLocator);
			logFileWriter.newLine();
		}//end if
	}//end method scrollTextIntoView
	
	public String getTextFromElement(String XPathLocator, BufferedWriter logFileWriter) throws Exception{
		String text = new String();
		scrollTextIntoView(XPathLocator, logFileWriter);
		try{
			text = super.getTextFromElement(By.xpath(XPathLocator));
		}//end try
		catch(Exception e){
			logFileWriter.write("Could not get text from element with xpath " + XPathLocator
					+", because of Exception " + e.getMessage());
		}//end catch
		return text;
	}
	
	public boolean reportOnMenuSelection(String menuType, By by, String menuItem, BufferedWriter logFileWriter) throws Exception{
		Thread.sleep(2000);//impose a quick wait for the menu items to adjust
		if (waitAndSelectElementInMenu(by, menuItem)){
			logFileWriter.write("Successfully acquired " + menuType +" '" + menuItem +"'");
			logFileWriter.newLine();
			return true;
		}//end if
		else {
			logFileWriter.write("Could not acquire " + menuType +" '" + menuItem +"'");
			logFileWriter.newLine();
			return false;
		}//end else
	}//end reportOnMenuSelection method

}//end class
