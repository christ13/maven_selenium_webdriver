package Maven.SeleniumWebdriver;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ClusterSalarySettingWorksheetNatSciASCTest extends
		ClusterSalarySettingWorksheetASCTest {

	@Before
	public void setUp() throws Exception {
		clusterName = "Natural Sciences Cluster";
		configureOutput();
		super.setUp();
	}

	private void configureOutput() throws Exception{
		myExcelReader = TestFileController.ClusterSalarySettingWorksheetNaturalSciences;
		mySheetName = TestFileController.ClusterSalarySettingWorksheetWorksheetName;
		rowForColumnNames = TestFileController.ClusterSalarySettingWorksheetHeaderRow;
		verificationErrors = new StringBuffer();
		testName = "ClusterSalarySettingWorksheetNatSciASCTest";
		testEnv = "Test";
		testCaseNumber = "039";
		testDescription = "ASC Test of Cluster Salary Setting Worksheet for Natural Sciences";
		logFileName = "Cluster Salary Setting NatSci ASC Test.txt";
		logFileWriter = TestFileController.getLogFileWriter(logDirectoryName  +"/" + logFileName);
	}//end configureOutput method

	
	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void test() throws Exception{
		super.test();
	}

}
