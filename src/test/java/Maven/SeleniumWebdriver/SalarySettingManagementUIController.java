/*
 * A few notes about Exceptions (Exclusions or Inclusions), and Overrides.  
 * 
 * If one is overridden to either the Dean Worksheet or the Chair Worksheet, then
 * the overridden faculty member will show up at the bottom of the School Salary
 * Setting Worksheet that corresponds to the Cluster that that faculty member 
 * belongs to.
 * 
 * If one is overridden to the Dean Worksheet, then the Dean Worksheet will have all
 * of these overrides at the bottom of it.
 * 
 * If one is overridden to either the Chair or Dean Worksheets, then the Cluster
 * worksheet for that individual's Cluster will have that individual.
 * 
 * If one is overridden to the Chair Worksheet, then the Chair Worksheet for that 
 * individual's Cluster will have these overrides at the bottom of it.
 * 
 * If one is overridden to either the Chair or Dean Worksheets, then the Department
 * SSW for that individual's department will have that individual at the bottom of
 * it as an override.
 * 
 * If one is excluded from Salary Setting, then that one will appear at the bottom
 * of the Department Salary Setting Worksheet (for the department that this faculty
 * member belongs to), the Cluster Salary Setting Worksheet, the School Salary 
 * Setting Worksheet, and the Master Salary Setting Worksheet as an Exclusion.
 * 
 * If one is included in Salary Setting, then that one will appear in the body of
 * the Master Salary Setting Worksheet, the School Salary Worksheet, the Cluster Salary 
 * Setting Worksheet, and the Department Salary Setting Worksheet, as an included 
 * Faculty member.  The faculty member is not going to appear on the Chair or 
 * Dean Salary Setting Worksheets unless the faculty member is a Chair or a Dean.
 * 
 */

package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeSet;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;


public class SalarySettingManagementUIController extends UIController {
	
	protected static HSSFExcelReader myExcelReader;
	protected static String[] departments;
	protected static final String myWorksheet = "Salary Setting Mgmt Tab";
	public static ArrayList<Override> overrideList;
	public static ArrayList<WorksheetException> exceptionList;
	private BufferedWriter myLogFileWriter;
	protected String logDirectoryName = ReportRunner.logDirectoryName;
	public static final String[] exceptionLocatorNames = {"FacultyExceptionName", 
		"FacultyExceptionCluster", "FacultyExceptionDept", "FacultyExceptionIncludeExclude", 
		"FacultyExceptionReason", "FacultyExceptionDescription", "FacultyExceptionNotes"};
	public static final String[] overrideLocatorNames = {"FacultyOverrideName", "FacultyOverrideCluster", 
		"FacultyOverrideDept", "FacultyOverrideWhichWorksheet", "FacultyOverrideDescription", 
		"FacultyOverrideNotes"};
	public static final String overrideSectionHeading = "Faculty Overridden from Salary Setting Worksheet";
	public static final String exclusionSectionHeading = "Faculty Excluded from FY17 Salary Setting";

	public SalarySettingManagementUIController(){
	}//end constructor
	
	public void initialize() throws Exception {
		myExcelReader = TestFileController.fileIn;
		myExcelReader.setRowForColumnNames(0);
		String fileName = "SalarySettingManagementUIController Setup.txt";
		String logFileName = ReportRunner.logDirectoryName +"/" + fileName;
		myLogFileWriter = TestFileController.getLogFileWriter(logFileName);
		
		switchWindowByURL(UIController.salarySettingURL, myLogFileWriter);
		HandSOnTestSuite.salarySettingTabUI.switchToSalarySettingTab();

		initializeOverrides();
		initializeWorksheetExceptions();	
		myLogFileWriter.close();
	}//end initialize() method
	
	
	public String[] getClusters(String overrideOrException, BufferedWriter logFileWriter) throws Exception{
		outputMessage("*** method getClusters called for " +overrideOrException +" ***", false, logFileWriter);
		TreeSet<String> clusterTreeSet = new TreeSet<String>();
		if (overrideOrException.equalsIgnoreCase("Override")){
			for(Override override: overrideList){
				String dept = override.getAttribute("Cluster");
				clusterTreeSet.add(dept);
				//outputMessage("Department "+ dept +" has been added", false, logFileWriter);
			}//end for loop - iterating through all of the overrides
		}//end if - overrides are requested
		else {
			for(WorksheetException exception: exceptionList){
				String dept = exception.getAttribute("Cluster");
				clusterTreeSet.add(dept);
				//outputMessage("Department "+ dept +" has been added", false, logFileWriter);
			}//end for loop - iterating through all of the overrides
		}//it's an Exception - treat it as such
		String[] clusters = new String[clusterTreeSet.size()];
		int index = 0;
		for (String cluster: clusterTreeSet){
			clusters[index] = cluster;
			index++;
			outputMessage("Cluster "+cluster+" will be returned", false, logFileWriter);
		}
		return clusters;
	}//end getClusters method
	
	public String[] getDepartments(String overrideOrException, BufferedWriter logFileWriter) throws Exception{
		outputMessage("*** method getDepartments called for " +overrideOrException +" ***", false, logFileWriter);
		TreeSet<String> deptTreeSet = new TreeSet<String>();
		if (overrideOrException.equalsIgnoreCase("Override")){
			for(Override override: overrideList){
				String dept = override.getAttribute("Dept");
				deptTreeSet.add(dept);
				//outputMessage("Department "+ dept +" has been added", false, logFileWriter);
			}//end for loop - iterating through all of the overrides
		}//end if - overrides are requested
		else {
			for(WorksheetException exception: exceptionList){
				String dept = exception.getAttribute("Dept");
				deptTreeSet.add(dept);
				//outputMessage("Department "+ dept +" has been added", false, logFileWriter);
			}//end for loop - iterating through all of the overrides
		}//it's an Exception - treat it as such
		String[] depts = new String[deptTreeSet.size()];
		int index = 0;
		for (String dept: deptTreeSet){
			depts[index] = dept;
			index++;
			outputMessage("Department "+dept+" will be returned", false, logFileWriter);
		}
		return depts;
	}//end getDepartmentsForOverrides method
	/*
	 * This method will return key-value pairs.  The keys are the set of departments that have 
	 * either overrides or exceptions for the given Cluster, and the values are the names of 
	 * professors for each department, contained in an array of Strings for each department.
	 */
	public HashMap<String, String[]> getFacultyForCluster(String expectedCluster, String overrideOrException, 
			String ChairOrDean, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method getFacultyForCluster called to get " + overrideOrException 
				+"s for Cluster " + expectedCluster +" ***");
		logFileWriter.newLine();
		
		HashMap<String, String[]> clusterFaculty = new HashMap<String, String[]>();
		if (overrideOrException.equalsIgnoreCase("Override")){
			for(Override override: overrideList){
				String actualCluster = override.getAttribute("Cluster");
				String whichWorksheet = override.getAttribute("WhichWorksheet");
				logFileWriter.write("Override analyzed has Cluster " + actualCluster +", and worksheet "+whichWorksheet);
				logFileWriter.newLine();
				if ((whichWorksheet.contains(ChairOrDean) || whichWorksheet.equalsIgnoreCase(ChairOrDean) || ChairOrDean.equalsIgnoreCase("both"))
						&& (actualCluster.contains(expectedCluster) || actualCluster.equalsIgnoreCase(expectedCluster))){
					String dept = override.getAttribute("Dept");
					logFileWriter.write("Adding faculty for Department " +dept);
					logFileWriter.newLine();
					clusterFaculty.put(dept, getFacultyForDepartment(dept, overrideOrException, ChairOrDean, logFileWriter));
				}//end if - it's a match
			}//end inner for
		}//end if - it's an override
		else {
			for(WorksheetException exception: exceptionList){
				String actualCluster = exception.getAttribute("Cluster");
				if (actualCluster.contains(expectedCluster) || actualCluster.equalsIgnoreCase(expectedCluster)){
					String dept = exception.getAttribute("Dept");
					logFileWriter.write("Adding faculty for Department " +dept);
					logFileWriter.newLine();
					clusterFaculty.put(dept, getFacultyForDepartment(dept, overrideOrException, logFileWriter));
				}//end if - it's a match
			}//end for loop
		}//else - we want the Exceptions
		return clusterFaculty;
	}//end getFacultyForCluster method
	
	public String[] getFacultyForDepartment(String department, String overrideOrException, 
			BufferedWriter logFileWriter) throws Exception{
		return getFacultyForDepartment(department, overrideOrException, "neither", logFileWriter);
	}//end getFacultyForDepartment method
	
	public ArrayList<String[]> getOverridesWithAttributeValue(String attributeName, 
				String attributeValue, BufferedWriter logFileWriter) throws Exception{
		outputMessage("*** method getFacultyWithAttributeValue called with attribute Name "+attributeName 
				+", and attribute value '" + attributeValue +"' ***", true, logFileWriter);
		
		ArrayList<String[]> faculty = new ArrayList<String[]>();		
		for (Override override: overrideList){
			String value = override.getAttribute(attributeName);
			if (value.equalsIgnoreCase(attributeValue)){
				faculty.add(override.getAttributes());
				outputMessage("MATCH - adding " + override.toString(), true, logFileWriter);
			}//end if
			else{
				outputMessage("NOT A MATCH - " + override.toString(), true, logFileWriter);
			}
		}//end for - iterating through the list of overrides
		
		return faculty;
	}//end getOverridesWithAttributeValue method
	
	public ArrayList<String[]> getExceptionsWithAttributeValue (String attributeName, 
			String attributeValue, BufferedWriter logFileWriter) throws Exception{
	outputMessage("*** method getExceptionsWithAttributeValue called with attribute Name "+attributeName 
			+", and attribute value '" + attributeValue +"' ***", true, logFileWriter);
	
	ArrayList<String[]> faculty = new ArrayList<String[]>();		
	for (WorksheetException exception: exceptionList){
		String value = exception.getAttribute(attributeName);
		if (value.equalsIgnoreCase(attributeValue)){
			faculty.add(exception.getAttributes());
			outputMessage("MATCH - adding " + exception.toString(), true, logFileWriter);
		}//end if
		else{
			outputMessage("NOT A MATCH - " + exception.toString(), true, logFileWriter);
		}
	}//end for - iterating through the list of overrides
	
	return faculty;
}//end getExceptionsWithAttributeValue method

	public String[] getFacultyForDepartment(String department, String overrideOrException, 
			String ChairOrDean, BufferedWriter logFileWriter) throws Exception{
		outputMessage("*** method getFacultyForDepartment called with department "+department 
				+", for '" + overrideOrException +"', and has Chair or Dean value '"+ChairOrDean
				+"' ***", false, logFileWriter);
		TreeSet<String> facultyTreeSet = new TreeSet<String>();
		if (overrideOrException.equalsIgnoreCase("Override")){
			for(Override override: overrideList){
				String dept = override.getAttribute("Dept");
				String whichWorksheet = override.getAttribute("WhichWorksheet");
				if (dept.equalsIgnoreCase(department) 
						&& (whichWorksheet.contains(ChairOrDean) || whichWorksheet.equalsIgnoreCase("both"))){
					facultyTreeSet.add(override.getAttribute("Name"));
				}//end if
			}//end for loop
		}//end if - they want the overrides
		else {
			for(WorksheetException exception: exceptionList){
				String dept = exception.getAttribute("Dept");
				if (dept.equalsIgnoreCase(department)){
					facultyTreeSet.add(exception.getAttribute("Name"));
				}//end if
			}//end for loop
		}//else - we want the Exceptions
		String[] faculty = new String[facultyTreeSet.size()];
		int index = 0;
		for (String prof: facultyTreeSet){
			faculty[index] = prof;
			index++;
			outputMessage("Professor "+prof+" will be returned", false, logFileWriter);
		}//end for loop - Professors
		return faculty;
		
	}//end getOverriddenFacultyForDepartment method
	
	public String getExceptionAttributeForNameAndDept(String name, String dept, String attributeName, 
			BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method getExceptionAttributeForNameAndDept called with name "+name 
				+", and department " + dept +", and attribute name "+attributeName +" ***");
		logFileWriter.newLine();
		String attribute = new String();
		for(WorksheetException exception: exceptionList){
			String actualName = exception.getAttribute("Name");
			String actualDept = exception.getAttribute("Dept");
			if (actualDept.equalsIgnoreCase(dept) 
					&& (actualName.equalsIgnoreCase(name))){
				attribute = exception.getAttribute(attributeName);
				logFileWriter.write("FOUND MATCH --> Name: " + name +", and Department: "+dept);
				logFileWriter.newLine();
				break;
			}//end if - match
			else{
				logFileWriter.write("No match - Exception had name " + actualName +", and dept " + actualDept);
				logFileWriter.newLine();
			}//end else - no match
		}//end for loop
		return attribute;
	}//end getExceptionAttributeForNameAndDept method
	
	public String getOverrideAttributeForNameAndDept(String name, String dept, String attributeName, 
			BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method getOverrideAttributeForNameAndDept called with name "+name 
				+", and department " + dept +", and attribute name "+attributeName +" ***");
		logFileWriter.newLine();
		String attribute = new String();
		for(Override override: overrideList){
			String actualName = override.getAttribute("Name");
			String actualDept = override.getAttribute("Dept");
			if (actualDept.equalsIgnoreCase(dept) 
					&& (actualName.equalsIgnoreCase(name))){
				attribute = override.getAttribute(attributeName);
				//logFileWriter.write("FOUND MATCH --> Name: " + name +", and Department: "+dept);
				//logFileWriter.write(", attribute name "+attributeName +", attribute value: "+attribute);
				logFileWriter.newLine();
				break;
			}//end if - match
			else{
				logFileWriter.write("No match - override had name " + actualName +", and dept " + actualDept);
				logFileWriter.newLine();
			}//end else - no match
		}//end for loop
		if (! attribute.isEmpty()){
			logFileWriter.write("FOUND MATCH for Name: " + name +", and Department: "+dept
					+", attribute name "+attributeName +", attribute value: "+ attribute);
			logFileWriter.newLine();
		}//end if
		return attribute;
	}//end getOverrideAttributeForNameAndDept method
	
	public ArrayList<String[]> getAllOverrides (BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method getAllOverrides called ***");
		logFileWriter.newLine();
		
		ArrayList<String[]> faculty = new ArrayList<String[]>();		
		for (Override override: overrideList){
			faculty.add(override.getAttributes());
			logFileWriter.write("Adding override with Name "+override.getAttribute("Name") 
					+", and Department "+override.getAttribute("Dept"));
			logFileWriter.newLine();
		}//end for loop
		
		return faculty;
	}//end getAllOverrides method
	
	public ArrayList<String[]> getAllExceptions (BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("*** method getAllExceptions called ***");
		logFileWriter.newLine();
		ArrayList<String[]> faculty = new ArrayList<String[]>();		
		for (WorksheetException exception: exceptionList){
			faculty.add(exception.getAttributes());
			logFileWriter.write("Adding Worksheet Exception with Name "+exception.getAttribute("Name") 
					+", and Department "+exception.getAttribute("Dept"));
			logFileWriter.newLine();
		}//end for loop
		
		return faculty;
	}//end getAllExceptions method
	
	public String[] getDepartmentsForExceptions(BufferedWriter logFileWriter) throws Exception{
		outputMessage("*** method getDepartmentsForExceptions called ***", false, logFileWriter);
		TreeSet<String> deptTreeSet = new TreeSet<String>();
		for (WorksheetException exception: exceptionList){
			String dept = exception.getAttribute("Dept");
			deptTreeSet.add(dept);
			//outputMessage("Department "+ dept +" has been added", false, logFileWriter);
		}//end for loop - iterating through all of the WorksheetExceptions
		String[] depts = new String[deptTreeSet.size()];
		int index = 0;
		for (String dept: deptTreeSet){
			depts[index] = dept;
			index++;
			outputMessage("Department "+dept+" will be returned", false, logFileWriter);
		}
		return depts;
	}//end getDepartmentsForExceptions method
	
	private void initializeOverrides() throws Exception{
		outputMessage("*** method initializeOverrides called ***");
		overrideList = new ArrayList<Override>();
		
		//first, switch to the Salary Setting Management tab
		switchToSalarySettingManagementTab();
		//second, get the number of rows
		String UIColName = overrideLocatorNames[0];
		
		int numberOfRows = getLastRowForUIColName(UIColName, myLogFileWriter);
		outputMessage("There are " + numberOfRows + " Overrides");
		
		//third, for each row, get the needed data and instantiate the Exceptions
		for (int row=1; row<=numberOfRows; row++){
			String[] values = new String[overrideLocatorNames.length];
			for (int overrideIndex=0; overrideIndex<overrideLocatorNames.length; overrideIndex++){
				String locatorName = overrideLocatorNames[overrideIndex];
				values[overrideIndex] = getStringForLocatorName(locatorName, row, myLogFileWriter);
				String message = "Value being used for "+locatorName+" is "+values[overrideIndex];
				outputMessage(message);
			}//end for loop
			overrideList.add(new Override(values));
			outputMessage("New Override added for row " + row);
		}//end for
		
	}//end initializeOverrides method
	
	private void initializeWorksheetExceptions() throws Exception{
		outputMessage("*** method initializeWorksheetExceptions called ***");
		exceptionList = new ArrayList<WorksheetException>();
		
		//first, switch to the Salary Setting Management tab
		switchToSalarySettingManagementTab();
		
		//second, get the number of rows
		String UIColName = exceptionLocatorNames[0];
		int numberOfRows = getLastRowForUIColName(UIColName, myLogFileWriter);
		outputMessage("There are " + numberOfRows + " Worksheet Exceptions");
		
		//third, for each row, get the needed data and instantiate the Exceptions
		for (int row=1; row<=numberOfRows; row++){
			String[] values = new String[exceptionLocatorNames.length];
			for (int exceptionIndex=0; exceptionIndex<exceptionLocatorNames.length; exceptionIndex++){
				String locatorName = exceptionLocatorNames[exceptionIndex];
				values[exceptionIndex] = getStringForLocatorName(locatorName, row, myLogFileWriter);
				String message = "Value being used for "+locatorName+" is "+values[exceptionIndex];
				outputMessage(message);
			}//end for loop
			exceptionList.add(new WorksheetException(values));
			outputMessage("New Worksheet Exception added for row " + row);
		}//end for
	}//end initializeExceptions method
	
	private String getStringForLocatorName(String locatorName, int row, BufferedWriter logFileWriter) throws Exception{
		outputMessage("*** method getStringForLocatorName called with locatorName " +locatorName
				+", and row " + row +" ***");
		
		//get the locator
		int rowNum = myExcelReader.getCellRowNum(myWorksheet, "Name", locatorName);
		String locator = myExcelReader.getCellData(myWorksheet, "Locator", rowNum);
		if (row>1){
			locator = locator.replace("/td/div/table/tbody/tr/td", "/td/div/table/tbody/tr[" + row +"]/td");
		}//end if
		String message = "Locator derived is " + locator;
		outputMessage(message);
		waitForElementPresent(By.xpath(locator));
	
		String value = new String();//this will be returned as the value
		//we may have to get the value of the widget or the text of the widget
		if (locator.contains("input") || locator.contains("textarea")){
			outputMessage("The locator is an input or span or text area element");
			value = driver.findElement(By.xpath(locator)).getAttribute("value");
		}//end if
		else if (locator.contains("select")){
			outputMessage("The locator is a select element");
			value = new Select(driver.findElement(By.xpath(locator))).getFirstSelectedOption().getText();
		}//end else if - select
		else{
			outputMessage("The locator is another element - we will simply get the text");
			value = driver.findElement(By.xpath(locator)).getText();
		}
		outputMessage("Value derived from the locator is " + value);
		return value;
	}//end getStringForLocatorName method
	
	public int getLastRowForUIColName(String UIColName, BufferedWriter logFileWriter) throws Exception{
		outputMessage ("*** method getLastRowForUIColName called with UI Column Name "+UIColName +" ***");
		outputMessage ("Attempting rough experiment with top row of Override list:");
		try {
			  outputMessage(driver.findElement(By.xpath("//table[2]/tbody/tr[2]/td/div/table/tbody/tr[3]/td/div/table/tbody/tr/td")).getText());
			} catch (Error e) {
				outputMessage(e.toString());
			}

		
		int lastRow = 0;
		boolean isLastRow = false;
		int locatorRow = myExcelReader.getCellRowNum(myWorksheet, "Name", UIColName, 1);
		outputMessage(UIColName + " is on row "+locatorRow +" of the worksheet " + myWorksheet);
		String BaseLocator = myExcelReader.getCellData(myWorksheet, "Locator", locatorRow);
		outputMessage("Derived base locator name is " + BaseLocator);
		int row = 1;
		while (! isLastRow){
			String locator = BaseLocator;//re-initialize it each row
			//see if the locator has a meaningful name
			if (row > 1)
				locator = BaseLocator.replace("/tbody/tr/", "/tbody/tr[" + row +"]/");
			if (! isElementPresent(By.xpath(locator))){
				isLastRow = true;
				lastRow = row -1;
				outputMessage("Locator '"+locator +"' doesn't exist - returning row "+lastRow);
			}//end if - take action appropriate if the element is present
			else{
				String text = getTextFromElement(By.xpath(locator));
				if (text.isEmpty()){
					isLastRow = true;
					lastRow = row -1;
					outputMessage("Locator '"+locator +"' is empty - returning row "+lastRow);
				}//end inner if
				else{
					outputMessage("Locator '" + locator +"' exists, yielding text "+text+", continuing to next row");
					row++;
				}//end inner else
			}//end else - the row exists
		}//end while loop - iterating until we get to the last row
		
		return lastRow;
	}//end getLastRowForUIColName method
	
	private void outputMessage(String message) throws Exception{
		outputMessage(message, true, myLogFileWriter);
	}//end outputMessage method
	/*
	private void outputMessage(String message, boolean outputToConsole) throws Exception{
		outputMessage(message, outputToConsole, myLogFileWriter);
	}//end outputMessage method
	*/
	private void outputMessage(String message, boolean outputToConsole, BufferedWriter logFileWriter) throws Exception{
		if (outputToConsole)
			System.out.println(message);
		logFileWriter.write(message);
		logFileWriter.newLine();
	}//end outputMessage method
	
	
	private class Override {
		private String name;
		private String cluster;
		private String dept;
		private String whichWorksheet;
		private String description;
		private String notes;
		
		public Override(String[] values){
			this.name = values[0];
			this.cluster = values[1];
			this.dept = values[2];
			this.whichWorksheet = values[3];
			this.description = values[4];
			this.notes = values[5];
		}//end constructor
		
		public String toString(){
			String returnString = "Override - ";
			returnString = returnString.concat("Name: " + this.name);
			returnString = returnString.concat(", Cluster: " + this.cluster);
			returnString = returnString.concat(", Department: " + this.dept);
			returnString = returnString.concat(", Which Worksheet: " + this.whichWorksheet);
			returnString = returnString.concat(", Description: "+ this.description);
			returnString = returnString.concat(", Notes: " + this.notes);
			return returnString;
		}//end toString() method
		
		public String[] getAttributes(){
			String[] returnStrings = new String[6];
			returnStrings[0] = this.name;
			returnStrings[1] = this.cluster;
			returnStrings[2] = this.dept;
			returnStrings[3] = this.whichWorksheet;
			returnStrings[4] = this.description;
			returnStrings[5] = this.notes;
			return returnStrings;
		}//end getAttributes method

		public String getAttribute(String attributeName){
			if (attributeName.equalsIgnoreCase("Name"))
				return this.name;
			else if (attributeName.equalsIgnoreCase("Cluster"))
				return this.cluster;
			else if (attributeName.equalsIgnoreCase("Dept"))
				return this.dept;
			else if (attributeName.equalsIgnoreCase("WhichWorksheet"))
				return this.whichWorksheet;
			else if (attributeName.equalsIgnoreCase("Description"))
				return this.description;
			else if (attributeName.equalsIgnoreCase("Notes"))
				return this.notes;
			else return new String();
		}//end getAttribute method
		/*
		public void setAttribute(String attributeName, String attributeValue){
			if (attributeName.equalsIgnoreCase("Name"))
				this.name = attributeValue;
			else if (attributeName.equalsIgnoreCase("Cluster"))
				this.cluster = attributeValue;
			else if (attributeName.equalsIgnoreCase("Dept"))
				this.dept = attributeValue;
			else if (attributeName.equalsIgnoreCase("WhichWorksheet"))
				this.whichWorksheet = attributeValue;
			else if (attributeName.equalsIgnoreCase("Description"))
				this.cluster = attributeValue;
			else if (attributeName.equalsIgnoreCase("Notes"))
				this.notes = attributeValue;
			else System.out.println("Class Override, method setAttribute method - couldn't identify attribute name given as input");
		}//end setAttribute method
		*/
	}
	
	private class WorksheetException {
		private String name;
		private String cluster;
		private String dept;
		private String excludeOrInclude;
		private String reason;
		private String description;
		private String notes;

		public WorksheetException(String[] values){
			this.name = values[0];
			this.cluster = values[1];
			this.dept = values[2];
			this.excludeOrInclude = values[3];
			this.reason = values[4];
			this.description = values[5];
			this.notes = values[6];
		}
		

		public String[] getAttributes(){
			String[] returnStrings = new String[7];
			returnStrings[0] = this.name;
			returnStrings[1] = this.cluster;
			returnStrings[2] = this.dept;
			returnStrings[3] = this.excludeOrInclude;
			returnStrings[4] = this.reason;
			returnStrings[5] = this.description;
			returnStrings[6] = this.notes;
			return returnStrings;
		}//end getAttributes method


		public String toString(){
			String returnString = "WorksheetException - ";
			returnString = returnString.concat("Name: " + this.name);
			returnString = returnString.concat(", Cluster: " + this.cluster);
			returnString = returnString.concat(", Department: " + this.dept);
			returnString = returnString.concat(", Exclude or Include: " + this.excludeOrInclude);
			returnString = returnString.concat(", Reason: " + this.reason);
			returnString = returnString.concat(", Description: "+ this.description);
			returnString = returnString.concat(", Notes: " + this.notes);
			return returnString;
		}//end toString() method

		public String getAttribute(String attributeName){
			if (attributeName.equalsIgnoreCase("Name"))
				return this.name;
			else if (attributeName.equalsIgnoreCase("Cluster"))
				return this.cluster;
			else if (attributeName.equalsIgnoreCase("Dept"))
				return this.dept;
			else if (attributeName.equalsIgnoreCase("ExcludeOrInclude"))
				return this.excludeOrInclude;
			else if (attributeName.equalsIgnoreCase("Reason"))
				return this.reason;
			else if (attributeName.equalsIgnoreCase("Description"))
				return this.description;
			else if (attributeName.equalsIgnoreCase("Notes"))
				return this.notes;
			else return new String();
		}//end getAttribute method
		
	}//end private Class WorksheetException

}//end SalarySettingManagementUIController class
