package Maven.SeleniumWebdriver;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ChairSalarySettingWorksheetNatSciASCTest extends
		ChairSalarySettingWorksheetASCTest {

	@Before
	public void setUp() throws Exception {
		clusterName = "Natural Sciences Cluster";
		configureOutput();
		super.setUp();
	}

	private void configureOutput() throws Exception{
		myExcelReader = TestFileController.ChairSalarySettingWorksheetNaturalSciences;
		mySheetName = TestFileController.ChairSalarySettingWorksheetWorksheetName;
		rowForColumnNames = TestFileController.ChairSalarySettingWorksheetNaturalSciencesHeaderRow;
		verificationErrors = new StringBuffer();
		testName = "ChairSalarySettingWorksheetNatSciASCTest";
		testEnv = "Test";
		testCaseNumber = "036";
		testDescription = "ASC Test of Chair Salary Setting Worksheet for Natural Sciences";
		logFileName = "Chair Salary Setting NatSci ASC Test.txt";
		logFileWriter = TestFileController.getLogFileWriter(logDirectoryName  +"/" + logFileName);
	}//end configureOutput method
		
	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void test() throws Exception{
		super.test();
	}

}
