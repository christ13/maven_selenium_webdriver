package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.TreeMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
	/*
	 * In this case, the DO Share
	 * Amount reflected in the Pivot Stage Report is the sum of all DO Shares
	 * per Name, Department, Category, and Subcategory, for all Pools.
	 * This is different from the doShares and deptShares summaries that are
	 * in the ASCFacultyValues Objects.  
	 * 
	 * We will have to make a customized summary values here by reading the 
	 * information from the Control Sheet Detail Report
	 * and summing up the values given the input criteria (Name, Stage,
	 * and Subcategory).  We will get one sum per faculty
	 * and verify that there's only one faculty 
	 * returned each time.  
	 * 
	 * Of note, the String attribute "Dept" is
	 * not a valid criterion.  If a faculty member belongs to more than
	 * one department, then all departments appear on each line, separated
	 * by a semicolon.  So, at least in this case, we'll have to evaluate
	 * the department as to whether or not it belongs to a substring. 
	 * Faculty who belong to multiple departments will not have one 
	 * department per line.  
	 * 
	 * In addition, for cluster-specific pivot reports, unless a faculty member
	 * belongs to a specific cluster that is being reflected on the report,
	 * that faculty member will not appear on the report.  For example, 
	 * a Faculty member with an appointment in the Natural Sciences division
	 * will not appear on the "Control Sheet Pivot_HumArts" Pivot Report.
	 * 
	 */
public class ControlSheetPivotReportASCTest extends ASCTest {

	protected String[] pivotCriteriaValues;
	protected String pivotCriteriaName;

	@Before
	public void setUp() throws Exception {
		myExcelReader = TestFileController.ControlSheetReportSuite;
		rowForColumnNames = TestFileController.ControlSheetPivotHeaderRow;
		verificationErrors = new StringBuffer();
		testEnv = "Test";
		searchDelimiterColumn = 0;
		excludeZeroDOShare = true;
		
		expectedSummaryNames = new String[1];
		actualSummaryNames = new String[1];
		actualSummaryNames[0] = expectedSummaryNames[0] = "FTE Adjusted DO Share Amount";
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void test() throws Exception{
		if (searchDelimiters.size() > 0){
			String message = "\n*** Test '" + testName + "' will be run as "
					+ searchDelimiters.size() + " subsets: ";
			for (int i=0; i< searchDelimiters.size(); i++){
				message = message.concat("attribute: " + searchDelimiters.get(i)[0]
						+", value: " + searchDelimiters.get(i)[1] + "; ");
			}
			message = message.concat("***\n");
			outputMessage(message, logFileWriter);
			for (int i=0; i< searchDelimiters.size(); i++){
				criteria.put(pivotCriteriaName, pivotCriteriaValues[i]);
				searchDelimiterIndex = i;
				super.test();
				criteria.remove(pivotCriteriaName);
			}//end for
		}//end if
	}

}
