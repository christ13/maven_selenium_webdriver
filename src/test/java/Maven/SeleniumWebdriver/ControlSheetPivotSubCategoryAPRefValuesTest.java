package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import java.io.BufferedWriter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ControlSheetPivotSubCategoryAPRefValuesTest extends
		ControlSheetReportSuiteAPRefValuesTest {

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void test() throws Exception{
		System.out.println("Now executing the test method for class ControlSheetPivotSubCategoryAPRefValuesTest.");
		verificationErrors = new StringBuffer();
		String testName = "Verify AP Values in Control Sheet Pivot Subcategory Report";
		System.out.println("Now executing test: " + testName);
		String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		BufferedWriter logFileWriter = TestFileController.getLogFileWriter(logFileName);
		verifyAPValuesInControlSheetPivotSubCategoryReport(logFileWriter);
		TestFileController.writeTestResults("Test", "005", "Comparison of FTE Adjusted Raises", "Control Sheet Pivot Subcategory Report Tests", verificationErrors);
		logFileWriter.close();
	}

	/*
	 * This applies specifically to the Control Sheet Pivot Subcategory Report worksheet in the 
	 * Control Sheet Report Suite. 
	 * 
	 * This will simply call getActualAPFacultyValues to get the values from the 
	 * Control Sheet Pivot Subcategory Report that are to be compared with the expected values, 
	 * and then call the superclass method "verifyAPValuesInControlSheetReportSuite",
	 * passing the actual values to it as a parameter.  
	 * 
	 * The superclass method will gather the expected values and do the comparison 
	 * between the expected and actual values.
	 * 
	 */
	
	public void verifyAPValuesInControlSheetPivotSubCategoryReport(BufferedWriter logFileWriter) throws Exception{
		TestFileController.ControlSheetReportSuite.setRowForColumnNames(TestFileController.ControlSheetPivotHeaderRow);
		String[][] actualValues = getActualAPFacultyValues(TestFileController.ControlSheetPivot_SubCatWorksheetName, logFileWriter);
		String[][] expectedValues = getExpectedAPFacultyValues(logFileWriter);
		super.verifyAPValuesInControlSheetReportSuite(expectedValues, actualValues, logFileWriter);
	}//end verifyAPValuesInControlSheetPivotSubCategoryReport method

}
