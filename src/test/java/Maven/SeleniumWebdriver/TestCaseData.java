package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;
/*
 * This Class holds the test data for one particular test case.  Once instantiated, it will do most of the recording to the 
 * RetentionData.xls Excel file through the SSExcelRead file.
 */

public class TestCaseData {


	//public static int TestCaseID;
	public static String testCaseIDString;
	private static String retentionID;
	private static String facultyName;
	private static boolean isNonZero;
	private Retention retention;
	private Faculty faculty;
	
	protected static String testName;
	protected static String testCaseNumber;
	protected static String testDescription;
	protected static String testEnv;
	protected static StringBuffer verificationErrors;
	protected static BufferedWriter logFileWriter;

	public TestCaseData(int TestCaseID) throws Exception {//this is the constructor 
		//this.TestCaseID = TestCaseID;
		testCaseIDString = "Test Case ID "+TestCaseID;

		testName = "RetentionDataTestCaseID"+TestCaseID;
		testEnv = "Test";
		testCaseNumber = new Integer(80+TestCaseID).toString();
		testDescription = "Retention Data "+testCaseIDString;
		verificationErrors = new StringBuffer();

		String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		logFileWriter = TestFileController.getLogFileWriter(logFileName);
		logFileWriter.write("Retention Data "+testCaseIDString+" has been instantiated.");
		logFileWriter.newLine();

		addFaculty(logFileWriter);
		addRetention();
		RetentionUIController.closeCurrentTab(logFileWriter);
		FacultyUIController.closeCurrentTab(logFileWriter);
		RetentionMasterDataController.writeToLogFile("TestCaseData Class for Test Case ID "+TestCaseID+" has been instantiated");
	}//end constructor
	
	private void addFaculty(BufferedWriter logFileWriter) throws Exception {
		System.out.println("Attempting to instantiate Faculty with Test Case ID "+testCaseIDString);
		this.faculty = new Faculty (testCaseIDString);
		String initialFacultyData = new String();
		try {
			initialFacultyData = faculty.getInitialFacultyData();
		}
		catch(Exception e) {
			logFileWriter.write("Could not get initial faculty data because of Exception "+e.getLocalizedMessage());
			logFileWriter.newLine();
		}
		logFileWriter.write(initialFacultyData);
		logFileWriter.newLine();
		this.faculty.getRemainingFacultyData(logFileWriter);
	}//end addFaculty method
	
	public void addRetention() throws Exception{
		retention = new Retention(testCaseIDString);
		logFileWriter.write(retention.getInitialRetentionData());
		logFileWriter.newLine();
		retentionID = retention.getRetentionID();
		logFileWriter.write("Retention ID for "+testCaseIDString+" is "+retentionID);
		logFileWriter.newLine();
	}//end addRetention method
	
	public void closeDataFile() throws Exception{
		logFileWriter.close();
	}//end closeDataFile method

}//end Class
