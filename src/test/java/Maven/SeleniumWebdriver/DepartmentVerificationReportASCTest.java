package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.TreeMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/*
 * With this report, all Pools except the Market Pool will figure into the 
 * “16-17 Commitments” figure.  Deans won’t show up.  Only Known ASC's for the current
 *  FY will be counted towards the “16-17 Commitments” figure.  That is the only amount figure
 *  that be tested, and it will come from the 
 *  So, we need to apply the subset() method of the ExpectedASCDataSet to exclude the Deans, 
 *  and exclude any ASC’s that have Market Pool as the Source Pool.  
 *  Contributions from other Pools which have zero DO Share contributions will be included.
 *  All ASC's that have non-blank and non-null Descriptions will contribute to the value in 
 *  the "16-17 Description" column.  
 *  
 *  This will be tested in several iterations.  Each iteration corresponds to a separate 
 *  department.  Naturally, each Faculty member should appear in the appropriate section 
 *  designated for that Faculty member's department.
 *  
 *  String values that will be tested include the following: 
 *  
 *  We might want to create a method that will test the Description column separately.
 *  Depending on the success of this method, we can instantiate it in the ASCTest superclass,
 *  and simply leave variables here that can be instantiated. 
 *  
 *  The Department Verification Report seems to have a set of rules for what appears:
 *  (1) Market Pool contributions don't show up - at all. 
 *  (2) Previous Year contributions don't show up - at all.
 *  (3) Governance Related contributions show up if they're ASC's.
 *  (4) All contributions from all other Categories from all other Source Pools show up,
 *  whether they are ASC's or otherwise. 
 *  (5) No FTE Adjustments are made.  
 *  (6) None of the Senior Associate Deans or Richard Saller show up in this report.
 *  
 */
public class DepartmentVerificationReportASCTest extends ASCTest {

	@Before
	public void setUp() throws Exception {
		myExcelReader = TestFileController.DepartmentVerificationReport;
		mySheetName = TestFileController.DepartmentVerificationReportWorksheetName;
		rowForColumnNames = TestFileController.DepartmentVerificationReportHeaderRow;
		verificationErrors = new StringBuffer();
		testName = "DeptVerificationReportASCTest";
		testEnv = "Test";
		testCaseNumber = "033";
		testDescription = "ASC Test of Dept Verification Report";
		logFileName = "Dept Verification Report ASC Test.txt";
		logFileWriter = TestFileController.getLogFileWriter(logDirectoryName  +"/" + logFileName);
		criteria = new TreeMap<String, String>();
		criteria.put(ExpectedASCData.StringAttributeKeys[ExpectedASCData.StageIndex], 
				ASCTest.stages[ASCTest.KnownStageIndex]);

		searchDelimiterColumn = 0;
		excludeMarketShare = true;
		excludeZeroDOShare = false;
		adjustForFTE = false;
		excludeDeans = true;
		backoutPYs = true;

		expectedStringAttributeNames = new String[3];
		actualStringAttributeNames = new String[3];
		expectedStringAttributeNames[0] = actualStringAttributeNames[0] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.FacultyIndex];
		expectedStringAttributeNames[1] = actualStringAttributeNames[1] = ASCFacultyValues.StringAttributeKeys[ASCFacultyValues.RankIndex];
		expectedStringAttributeNames[2] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.DescriptionIndex];
		actualStringAttributeNames[2] = HandSOnTestSuite.FY + " " + ExpectedASCData.StringAttributeKeys[ExpectedASCData.DescriptionIndex];

		expectedAmountAttributeNames = new String[2];
		actualAmountAttributeNames = new String[2];
		expectedAmountAttributeNames[0] = ExpectedASCData.amountAttributeKeys[ExpectedASCData.FTEPercentIndex];
		actualAmountAttributeNames[0] = "FTE";
		expectedAmountAttributeNames[1] = ExpectedASCData.amountAttributeKeys[ExpectedASCData.CurrentSalaryIndex];
		actualAmountAttributeNames[1] = "Final " + HandSOnTestSuite.PY + " Salary";
		
		shareNames = new String[1];
		shareNames[0] = HandSOnTestSuite.FY + " Commitments";
		expectedShareCriteria = new TreeMap<String, ArrayList<String>>();
		ArrayList<String> list = new ArrayList<String>();
		list.add(ASCFacultyValues.ShareTotal);
		list.add(ASCFacultyValues.ShareTotal);
		expectedShareCriteria.put(shareNames[0], list);

		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void test() throws Exception{
		String department = ExpectedASCData.StringAttributeKeys[ExpectedASCData.DepartmentIndex];
		String[] departments 
			= HandSOnTestSuite.expectedASCDataSet.getAllDiscreteStringAttributeValues(department, logFileWriter);
		String delimiterValueName = department;
		searchDelimiters = new ArrayList<String[]>(departments.length);
		for (int i=0; i<departments.length; i++){
			String[] criterion = {department, departments[i]};
			searchDelimiters.add(criterion);
		}//end for loop
		String category = "Category";
		String[] validCategories = {"Faculty Appointment Related", "Salary Promises"};
		for (int deptIndex=0; deptIndex<departments.length; deptIndex++){
			String message = "department iteration " + deptIndex + " with department name " +departments[deptIndex];
			criteria.put(department, departments[deptIndex]);
			outputMessage("\n**** starting " + message +" ****\n", logFileWriter);				
			searchDelimiterIndex = deptIndex;
			useASCOnly = false;
			for(int i=1; i<=validCategories.length; i++){
				message = "iteration " + i + " with criteria name " +category
						+" and value " + validCategories[i-1];
				outputMessage("\n**** starting " + message +" ****\n", logFileWriter);
				criteria.put(category, validCategories[i-1]);
				super.test();
				criteria.remove(category);
				outputMessage("\n**** ending " + message +" ****\n", logFileWriter);
			}//end for loop
			String validCategory = "Governance Related";
			message = "iteration " + (validCategories.length + 1) + " with criteria name " +delimiterValueName
					+" and value " + validCategory;
			outputMessage("\n**** starting " + message +" ****\n", logFileWriter);
			criteria.put(category, validCategory);
			useASCOnly = true;
			super.test();
			criteria.remove(category);
			outputMessage("\n**** ending " + message +" ****\n", logFileWriter);
			
			//now remove the department criterion
			criteria.remove(department);
		}//end outer for loop - iterating through all the departments
	}

}
