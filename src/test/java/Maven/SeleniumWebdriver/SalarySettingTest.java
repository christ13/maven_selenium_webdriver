package Maven.SeleniumWebdriver;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import java.io.BufferedReader; 
import java.io.BufferedWriter; 
import java.io.File; 
import java.io.FileReader; 
import java.io.FileWriter; 
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.Keys; 
import org.dbunit.ext.oracle.*;
import org.dbunit.dataset.excel.*;

/*
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
*/

public class SalarySettingTest {
	
	  public static final int FACULTY_NAME_INDEX = 0;
	  
	  private WebDriver driver;
	  private String baseUrl;
	  public static final String mainWindowURL = "apphs/showDocumentsTab.do";
	  public static final String fiscalYearDialogURL = "processSalarySetting.do?ACTION=getAvailableFiscalYear";
	  public static final String loadSalarySettingWindowURL = "processSalarySetting.do?ACTION=loadSalarySetting";
	  public static final String facultyRecordWindowURL = "processManageFaculty.do?ACTION=load&facultyDTO.unvlId=";
	  public static final String showRetentionWindowURL = "showRetention.do";
	  public static final String showRetentionLookupWindowURL = "showRetentionLookup.do";
	  public static final String annotationPopupWindowURL = "annotationPopup.jsp";
	  public static final String commitmentWindowURL = "showFundingCommitment.do";
	  private String directory = "C:/Users/christ13/Documents/HandSon/";
	  private String LogDirectoryName = directory + new SimpleDateFormat("yyyyMMddhhmm").format(new Date());
	  private String inputFileName = "Input Data.xls";
	  private String individualInputWorksheetName = "Individual Test Input Data";
	  private String globalInputWorksheetName = "Global Test Input Data";
	  private String outputFileName = "Test Results.xls";
	  private String outputWorksheetName = "Passed Tests";
	  private String errorWorksheetName = "Failed Tests";
	  private String fileUnderTestWorksheetName;
	  private boolean acceptNextAlert = true;
	  private StringBuffer verificationErrors;
	  //private String username;
	  private static String browserName;
	  private static String startPage = "apphs/processInboxTab.do";
	  //private static String login;
	  //private static String password = "Intrax##2";
	  
	  private HSSFExcelReader fileIn;
	  private HSSFExcelReader fileOut;
	  private HSSFExcelReader fileUnderTest;

	  @Before
	  public void setUp() throws Exception {

			browserName = System.getenv("browserName");
			System.out.println("Derived environmental variable browserName: " + browserName);
		
			if (browserName.equalsIgnoreCase(SFDC_APC_Application_Variables.CHROME_BROWSER)){
				driver = new ChromeDriver();
			}
			else if (browserName.equalsIgnoreCase(SFDC_APC_Application_Variables.FIREFOX_BROWSER)){
				driver = new FirefoxDriver();
			}
			else if (browserName.equalsIgnoreCase(SFDC_APC_Application_Variables.SAFARI_BROWSER)){
				driver = new SafariDriver();
			}
			else driver = new InternetExplorerDriver();
			
			
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			String envName = System.getenv("envName");
			System.out.println("Derived environmental name: " + envName);
			if (envName.equalsIgnoreCase("Test")){
				
				baseUrl = "https://handsonweb-"+HandSOnTestSuite.machineName+".stanford.edu/";
			}
			else{ 
				
				baseUrl = "https://handsonweb-dev2.stanford.edu/";
			}
			baseUrl = "https://handsonweb-"+HandSOnTestSuite.machineName+".stanford.edu/";
			
			startPage = baseUrl;
			
			System.out.println("derived start page is as follows: " + startPage);

			

			fileIn = new HSSFExcelReader(directory + inputFileName);
			fileOut = new HSSFExcelReader(directory + outputFileName);
			
			  if (! fileOut.addSheet(outputWorksheetName)){
				  System.out.println("Removing and re-creating Output File Worksheet");
				  fileOut.removeSheet(outputWorksheetName);
				  fileOut.addSheet(outputWorksheetName);
			  }
			  fileOut.addColumn(outputWorksheetName, "Test Env.");
			  fileOut.addColumn(outputWorksheetName, "Test Case #");
			  fileOut.addColumn(outputWorksheetName, "Test Description");
			  fileOut.addColumn(outputWorksheetName, "Test Case");

			  if (! fileOut.addSheet(errorWorksheetName)){
				  System.out.println("Removing and re-creating Error Worksheet");
				  fileOut.removeSheet(errorWorksheetName);
				  fileOut.addSheet(errorWorksheetName);
			  }
			  fileOut.addColumn(errorWorksheetName, "Test Env.");
			  fileOut.addColumn(errorWorksheetName, "Test Case #");
			  fileOut.addColumn(errorWorksheetName, "Test Description");
			  fileOut.addColumn(errorWorksheetName, "Test Case");
			  fileOut.addColumn(errorWorksheetName, "Cumulative Error String");
			  
			  File logDirectory = new File(LogDirectoryName);
				if (!logDirectory.exists()) {
					if (logDirectory.mkdir()) {
						System.out.println("Log Directory is created!");
					} else {
						System.out.println("Failed to create log directory!");
					}
				}
				else System.out.println("Log Directory already exists.");

	  }
	  
	  @Test
	  public void testSalarySetting() throws Exception {
		  verificationErrors = new StringBuffer();
		  int maxAttemptedTests = fileIn.getRowCount(individualInputWorksheetName);
		  System.out.println("Rows in the input file: " + maxAttemptedTests);
		  //int attemptedTests = 2;
		  String[] columnNames = fileIn.getColumnNames(individualInputWorksheetName);
		  System.out.print("Columns determined from input file: ");
		  for(int i = 0; i< columnNames.length; i++){
			  System.out.print(columnNames[i] +", ");
		  }
		  System.out.println();

		  String fileUnderTestName = "Control Sheet Detail Report.xls";
		  fileUnderTest = new HSSFExcelReader(directory + fileUnderTestName, 6);
		  fileUnderTestWorksheetName = "Control Sheet Detail";
		  int fileUnderTestLastRow = fileUnderTest.getRowCount(fileUnderTestWorksheetName);
		  System.out.println("Rows in the file under test: " + fileUnderTestLastRow);
		  System.out.println("Number of columns in the file under test: " + fileUnderTest.getColumnCount(fileUnderTestWorksheetName));
		  
		  /*
		  String query = "select f.unvl_id, f.ADD_UNVL_ID, p.person_first_name, p.person_middle_name, p.PERSON_LAST_NAME, "
				  +"f.faculty_status_type_cd, f.current_rank_cd, f.initial_su_hire_date," 
				  +"f.actual_tenure_date, f.exit_date, f.exit_status, f.cy_salary, f.cy_fte "
				  +"from faculty f, imirdx_person_t p " 
				  +"where f.unvl_id = 'HS_Parigi7' "
				  +"and f.unvl_id = p.person_unvl_id "
				  +"order by f.unvl_id";

		  runSQLQuery(query);
		  */
		  /*
		  verifyTerminalYearFaculty();
		  verifyMidYearFaculty();
		  setAPValuesinGUI();
		  //resetSampleData();
		  verifyControlSheetReport();
		  verifyDeptChairNumberInfoInGUI();
		  */
		  runReports();
		  /*
		  verifyColumnWidthInControlSheetDetailReport();
		  verifyFacultyWithSalaryRetentions();
		  verifyRegularAPFaculty();
		  verifyDeptChairInfoInReport();
		  verifyRegularSADFaculty();
		  verifyRegularFacultyWithSalaryPromise();
		  verifyFacultyWithSalaryPromiseIrregularDate();
		  verifyFacultyWithSalaryPromiseAndMktRetention();
		  verifyFacultyWithRegularAPRegularDates();
		  verifyFacultyWithRegularAPIrregularDates();
		  verifyDepartmentChairOneHundredPercent();
		  verifyDepartmentChairOneHundredPercentTwoDepts();
		  verifyDepartmentChairOneHundredPercentTwoSchools();
		  verifyDepartmentChairFiftyPercentTwoSchools();
		  verifyProgramDirectorMeritPool();
		  verifyDepartmentChairRetentionPromiseNineOne();
		  verifyDepartmentChairRetentionPromiseNotNineOne();
		  verifyDepartmentChairSuccessfulRecruitmentNotNineOne();
		  verifyDepartmentChairSuccessfulRecruitmentNineOne();
		  verifyDepartmentChairOverride();
		  verifyDepartmentChairRetentionPromiseMktRetention();
		  verifyClusterDeansMeritPool();
		  verifyClusterDeanRetentionPromise();
		  verifyClusterDeanOverride();
		  verifyClusterDeanRetentionPromiseMktRetention();
		  //getNamesForMeritPoolRetentionPromises();
		  verifyPositiveKnownRegularMeritPoolASC();
		  verifyNegativeKnownRegularMeritPoolASC();
		  verifyPositiveKnownMarketPoolASC();
		  verifyNegativeKnownMarketPoolASC();
		  verifyPositivePendingRegularMeritPoolASC();
		  verifyPositiveEstimatedRegularMeritPoolASC();
		  verifyPositiveEstimatedUnknownFacultyRegularMeritPoolASC();
		  verifyPositiveKnownAndPendingFacultyRegularMeritPoolASC();
		  verifyPositiveKnownLessThanFTEFacultyRegularMeritPoolASC();
		  verifyPositiveKnownGovernanceRelatedASC();
		  verifyNegativeKnownGovernanceRelatedASC();
		  verifyPositivePendingGovernanceRelatedASC();
		  verifySalaryLogRetroactiveChangeImpactOnControlSheetDetailReport();
		  
		  //The following two tests for HandSOn #60 and #61 are not valid tests 
		  //verifySalaryBaseForDeptChairs();
		  //verifySalaryBaseForSrAssociateDeans();

		  verifyKnownRegularMeritPoolSalaryPromises();
		  verifyPendingRegularMeritPoolSalaryPromises();
		  verifyKnownMarketPoolSalaryPromises();
		  verifyPendingMarketPoolSalaryPromises();
		  verifyKnownRegularMeritPoolRecruitmentPromises();
		  verifyKnownRegularMeritPoolGovernanceWithSalaryPromises();
		  verifyPendingRegularMeritPoolGovernanceWithSalaryPromises();
		  verifyKnownRegularMeritPoolSeniorAssociateDeanGovernanceWithSalaryPromises();
		  verifyPendingRegularMeritPoolSeniorAssociateDeanGovernanceWithSalaryPromises();
		  verifyKnownPreviousYearCommitmentsRetroactiveChange();
		  verifyKnownPreviousYearCommitmentsPostBinder();
		  verifyKnownPreviousYearCommitmentsPostLetter();
		  verifyKnownPreviousYearCommitmentsMidYear();
		  verifyKnownFacultyAppointmentReappointment();
		  verifyKnownFacultyAppointmentPromoNonTenure();
		  verifyKnownFacultyAppointmentPromoFull();
		  verifyKnownFacultyAppointmentPromoContTerm();
		  verifyKnownFacultyAppointmentPromoRemovalofSubjToPhD();
		  verifyKnownFacultyAppointmentTerminalYear();
		  verifyGovernanceRelatedChairsDirectors();
		  verifyGovernanceRelatedSeniorAssociateDeans();
		  verifyTransferInformation();
		  verifyReserveInformation();
		  verifyNotesForFaculty();
		  verifySumKnownRetentionsInControlSheetGrid();
		  verifySumPendingRetentionsInControlSheetGrid();
		  verifySumKnownMarketRetentionsInControlSheetGrid();
		  verifySumPendingMarketRetentionsInControlSheetGrid();
		  verifySumKnownRecruitmentPromisesInControlSheetGrid();S
		  verifySumKnownGovernanceWithSalaryPromisesInControlSheetGrid();
		  verifySumSalaryPromisesOtherInControlSheetGrid();
		  verifySumRetroactiveChangesInControlSheetGrid();
		  verifySumPostBinderChangesInControlSheetGrid();
		  verifySumPostLetterChangesInControlSheetGrid();
		  verifySumMidYearChangesInControlSheetGrid();
		  verifySumReappointmentsInControlSheetGrid();
		  verifySumPromotionsToNonTenureInControlSheetGrid();
		  verifySumPromotionsToFullInControlSheetGrid();
		  verifySumPromotionsToContinuingTermInControlSheetGrid();
		  verifySumRemovalofSubjecttoPhDInControlSheetGrid();
		  verifySumTerminalYearInControlSheetGrid();
		  verifySumGovernanceRelatedChairsAndDirectorsInControlSheetGrid();
		  verifySumGovernanceRelatedOtherGovInControlSheetGrid();
		  verifyTransferInformationInControlSheetGrid();
		  verifyReservesInformationInControlSheetGrid();
		  verifyControlSheetNotesInControlSheetGrid();
		  */
	  }
	  
	  /*
	   * This is a frequently-used method.  It is a shorthand way to put the "write(String)" 
	   * and the "newLine()" commands into a one-line command.
	   */
	  public void writeToLogFile(String toBeWritten, BufferedWriter logFileWriter) throws Exception{
		  logFileWriter.write(toBeWritten);
		  logFileWriter.newLine();
	  }
	  
	  public void runSQLQuery(String query, BufferedWriter logFileWriter) throws Exception{
		  String URL = "jdbc:oracle:thin:@//handsondb-tst5.stanford.edu:1568/HONTST";
		  //Connection con=DriverManager.getConnection(URL,"HSONRO","TYBUSJXcsNeCrZf5");
		  Connection con=DriverManager.getConnection(URL,"HSONRO",System.getenv("dbPassword"));
		  Statement stmt=con.createStatement();
		  ResultSet rs=stmt.executeQuery(query);  
		  ResultSetMetaData rsmd = rs.getMetaData();
		  int columnCount = rsmd.getColumnCount();
		  writeToLogFile("Retrieved " + columnCount + " columns from the database", logFileWriter);
		  while(rs.next()){
			  for(int column=1;column<=columnCount;column++){
				  logFileWriter.write(rs.getString(column)+"; ");
				  logFileWriter.newLine();
			  }
		  }  
		  con.close();
	  }
	  
	  public void resetSampleData() throws Exception{
		  verificationErrors = new StringBuffer();
		  String testName = "Reset of Sample Data";
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  System.out.println("Now executing test: " + testName);
		  //System.out.println("File to be written is " +logFileName);
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);

		  createSalaryRetention("Maryam", "Mirzakhani", logFileWriter);
		  createCommitment("Amir", "Eshel", logFileWriter);

		  logFileWriter.close();
		  
		  //finally, at the end of the test, write the results to the output file
		  writeTestResults("HandSOn Data Setup", "N/A", "Reset data values", "Re-create sample data after database is restored");
		  
	  }
	  
	  /*
	   * This is a GUI test and a verification of the Control Sheet Report.  
	   * The test verifies that the values in the Control Sheet Report match
	   * the values in the Control Sheet tab in the Salary Setting page in the web browser.
	   * The expected values are taken from the Control Sheet tab in the Salary Setting
	   * page and the values to be verified are in the Control Sheet Report.
	   * 
	   * Before this test is run, it is assumed that the
	   * Control Sheet Report has already been taken and saved to a particular place on
	   * the local machine's hard drive, and that this Control Sheet Report has been taken
	   * before or after any adjustments to the data have been taken so that it is 
	   * reasonable to assume that the values in the Control Sheet tab in the Salary Setting
	   * web page have not been affected by any data manipulation since the Control Sheet
	   * Report has been instantiated.
	   * 
	   * This test consists of several actions.  First of all, there are two ExcelReaders
	   * that are used for this particular test - one for the file under test
	   * (namely, the Control Sheet Report), and the other for the input file worksheet containing
	   * the locators for all of the row and column titles and values that are found in
	   * the Control Sheet tab of the Salary Setting web page.  This worksheet will also
	   * contain the row and column numbers for these same values found in the 
	   * Control Sheet Report.  This worksheet containing the locators, row numbers,
	   * and column numbers will be instantiated and populated with appropriate values 
	   * prior to the test being executed.
	   * 
	   * This test will (iteratively, one by one) read in the expected values from the 
	   * Control Sheet tab of the Salary Setting web page (using the locators in the input
	   * file) and verify that each value being read in matches the corresponding value 
	   * in the Control Sheet Report.  Corresponding value in the Control Sheet Report 
	   * is located by the row and column number recorded in the input file on the same
	   * row in the input file as the locator for the value on the Control Sheet tab 
	   * of the Salary Setting web page.
	   * 
	   * Once the values are verified, the test will write the results (whether pass or fail,
	   * and detailed information as to why the test failed, if it did) to the output file.
	   */
	  
	  public void verifyControlSheetReport() throws Exception{
		  
		  verificationErrors = new StringBuffer();
		  String testName = "HandSOn_TEST 2 (A) Verify existing business cases";
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  System.out.println("Now executing test: " + testName);
		  //System.out.println("File to be written is " +logFileName);
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
		  
		  HSSFExcelReader fileUnderTest = new HSSFExcelReader(directory + "Control Sheet Report.xls", 7);
		  String inputWorksheetName = "Control Sheet";
		  String fileUnderTestWorksheetname = "Control Sheet Report";
		  
		  if (openSalarySetting(logFileWriter)){
			  writeToLogFile("Successfully opened Salary Setting", logFileWriter);
		  }
		  else{ 
			  verificationErrors.append("Verify Control Sheet Report failed: Couldn't open Salary Setting");
			  writeToLogFile("Verify Control Sheet Report failed: Couldn't open Salary Setting", logFileWriter);
		  }
		  waitAndClick(By.id("control_a"));
		  //for some reason, it takes a while for the page to finish loading, so wait for it
		  Thread.sleep(30000);
		  
		  //once it's up, start doing the comparisons, one by one
		  for(int inputFileRow = 2; inputFileRow<=fileIn.getRowCount(inputWorksheetName); inputFileRow++){
			  String name = fileIn.getCellData(inputWorksheetName, 0, inputFileRow);
			  String locator = fileIn.getCellData(inputWorksheetName, 1, inputFileRow);
			  writeToLogFile("Derived locator is " + locator, logFileWriter);
			  waitForElementPresent(By.xpath(locator));
			  String expected = driver.findElement(By.xpath(locator)).getText();
			  writeToLogFile("Derived expected value is " + expected, logFileWriter);
			  String actual = "";
			  try{
				  actual = fileUnderTest.getCellData(fileUnderTestWorksheetname, 
						  new Float(fileIn.getCellData(inputWorksheetName, 2, inputFileRow)).intValue(), 
						  new Float(fileIn.getCellData(inputWorksheetName, 3, inputFileRow)).intValue());
				  writeToLogFile(" and actual value is "+actual, logFileWriter);
			  }
			  catch(NumberFormatException e){
				  writeToLogFile("end of values reached", logFileWriter);
				  //inputFileRow = fileIn.getRowCount(inputWorksheetName);
				  break;
			  }
			  if (! expected.replaceAll("\\s+","").equalsIgnoreCase(actual.replaceAll("\\s+",""))){
				  verificationErrors.append(" MISMATCH on "+name+": expected: " +expected +", actual: " +actual);
			  } else logFileWriter.write(name+" matches");
			  logFileWriter.newLine();
		  }//end for loop
		  logFileWriter.close();
		  
		  //finally, at the end of the test, write the results to the output file
		  writeTestResults("HandSOn TEST", "2", "(A) Verify existing business cases .", "Verify the format and the contents.");
		  
	  }
	  
	  public void verifyColumnWidthInControlSheetDetailReport() throws Exception{
		  verificationErrors = new StringBuffer();
		  String testName = "HandSOn_TEST 4 (A) Verify existing business cases";
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  System.out.println("Now executing test: " + testName);
		  //System.out.println("File to be written is " +logFileName);
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
		  
		  int numberOfColumns = fileUnderTest.getColumnCount(fileUnderTestWorksheetName);
		  for (int columnIndex=1;columnIndex<numberOfColumns;columnIndex++){
			  int actualWidth = fileUnderTest.getColumnWidthInCharacters(fileUnderTestWorksheetName, columnIndex, logFileWriter);
			  String debugMsg = "Column Index " + columnIndex + " has actual width of " + actualWidth + " characters, ";
			  int expectedWidth = new Integer(fileIn.getCellData("Format", 2, columnIndex + 1)).intValue();
			  if (actualWidth == expectedWidth) debugMsg = debugMsg.concat("as expected");
			  else{
				  debugMsg = debugMsg.concat("but expected width is " + expectedWidth + ": MISMATCH");
				  verificationErrors.append(debugMsg);
			  }
			  //now that we've got the results, report them
			  writeToLogFile(debugMsg, logFileWriter);
			  logFileWriter.newLine();
		  }

		  logFileWriter.close();
		  
		  //finally, at the end of the test, write the results to the output file
		  writeTestResults("HandSOn TEST", "4", "(A) Verify existing business cases .", "Verify the format of the report.");
	  }
	  
	  public void verifyFacultyWithSalaryRetentions() throws Exception{
		  
		  verificationErrors = new StringBuffer();
		  String testName = "HandSOn_TEST 11 (A.1) Verify Pre-Defined Use Cases (Regular Faculty).";
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  System.out.println("Now executing test: " + testName);
		  //System.out.println("File to be written is " +logFileName);
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);

		  HashMap<String, String> keysAndValues =  new HashMap<String, String>();
		  keysAndValues.put("Category", "Salary Promises");
		  keysAndValues.put("Subcategory", "Retention Promises");
		  
		  if(verifyNamesForFacultyGroup(keysAndValues, logFileWriter, fileIn, fileUnderTest)){
			  writeToLogFile("Test passed", logFileWriter);
		  } else writeToLogFile("Test FAILED", logFileWriter);
		  
		  
		  logFileWriter.close();

		  writeTestResults("HandSOn TEST", "11", "(A.1) Verify Pre-Defined Use Cases (Regular Faculty).", "Faculty who has a retention salary promise.");

	  }
	  
	  public void createSalaryRetention(String firstName, String lastName, BufferedWriter logFileWriter) throws Exception{
		  verificationErrors = new StringBuffer();

		  if (findFaculty(firstName, lastName, logFileWriter)){
			  writeToLogFile("Faculty found", logFileWriter);
		  }
		  else writeToLogFile("Faculty not found", logFileWriter);
		  String newRetentionRecord = addNewRetention(logFileWriter);
		  if (newRetentionRecord.equalsIgnoreCase("")){
			  writeToLogFile("Could not add new Retention Record", logFileWriter);
			  verificationErrors.append("Could not add new Retention Record");
		  }
		  else writeToLogFile("New Retention Record created is ID " + newRetentionRecord, logFileWriter);
		  if (addRetentionCost(newRetentionRecord, logFileWriter)){
			  writeToLogFile("Retention Cost Successfully Added", logFileWriter);
		  }
		  else verificationErrors.append("Retention Cost Not Successfully Added");
		  
		  if (switchWindow(By.linkText("Faculty Search"), logFileWriter).equalsIgnoreCase("Got the window")){
			  writeToLogFile("Got the main page again", logFileWriter);
		  }
		  else{
			  writeToLogFile("could not re-access main page", logFileWriter);
			  verificationErrors.append("Attempt to create commitment was unsuccessful: could not access main page");
		  }
		 
		  //now, close this faculty record
		  //first, re-acquire the window
		  if (switchWindowByURL(showRetentionWindowURL, logFileWriter)){
			writeToLogFile("Successfully re-acquired Retention Cost page", logFileWriter);
		  }
		  else{
			  writeToLogFile("could not re-acquire Retention Cost page", logFileWriter);
		  }
		  try{
			  driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "w");
		  }
		  catch(UnhandledAlertException e){
			  writeToLogFile("Alert is open - attempting to close it", logFileWriter);
			  Thread.sleep(2000);
			  driver.switchTo().alert().accept();
		  }
		  
		  Thread.sleep(5000);
		  writeToLogFile("Now attempting to close the window.", logFileWriter);
		  driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "w");
		  Thread.sleep(5000);
		  
		  switchWindowByURL(facultyRecordWindowURL, logFileWriter);
		  Thread.sleep(3000);
		  writeToLogFile("Now attempting to close the Faculty Record window", logFileWriter);
		  driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "w");
		  
		  //writeTestResults("HandSOn TEST", "11", "(A.1) Verify Pre-Defined Use Cases (Regular Faculty).", "Faculty who has a retention salary promise.");
	  }
	  
	  public String createCommitment(String firstName, String lastName, BufferedWriter logFileWriter) throws Exception {
		  verificationErrors = new StringBuffer();
		  String commitmentNumber = "";
		  if (switchWindow(By.linkText("Faculty Search"), logFileWriter).equalsIgnoreCase("Got the window")){
			  writeToLogFile("Got the main page", logFileWriter);
		  }
		  else{
			  writeToLogFile("could not access main page", logFileWriter);
			  verificationErrors.append("Attempt to create commitment was unsuccessful: could not access main page");
		  }

		  if (findFaculty(firstName, lastName, logFileWriter)){
			  writeToLogFile("Faculty found: " + firstName + " " +lastName, logFileWriter);
		  }
		  else{
			  writeToLogFile("Faculty not found", logFileWriter);
			  verificationErrors.append("Attempt to create commitment was unsuccessful: Faculty Not Found");
		  }
		  
		  driver.findElement(By.id("commit_a")).click();
		  waitAndClick(By.linkText("Create Commitment"));
		  Thread.sleep(3000);

		  
		  if (switchWindowByURL(commitmentWindowURL, logFileWriter)){
			  writeToLogFile("Successfully acquired New Commitment Window", logFileWriter);
		  }
		  else{
			  writeToLogFile("New Commitment Window couldn't be acquired", logFileWriter);
			  return commitmentNumber;
		  }
			  

		  if (isDropdownAccessible (By.xpath("//td[5]/select"), "2016-17", logFileWriter)){
			  writeToLogFile("2016-17 can be selected as the fiscal year", logFileWriter);
		  
			  if (isDropdownAccessible (By.xpath("//td[9]/select"), "Cont Salary", logFileWriter)){
				  writeToLogFile("Expense Area can be selected", logFileWriter);
			  }
			  else{
				  verificationErrors.append("Cannot select Expense Area");
			  }
			  waitForElementPresent(By.xpath("//textarea"));
			  driver.findElement(By.xpath("//textarea")).clear();
			  driver.findElement(By.xpath("//textarea")).sendKeys("test description");
			  new Select(driver.findElement(By.xpath("//select"))).selectByVisibleText("Retention");
			  driver.findElement(By.xpath("//td[2]/textarea")).clear();
			  driver.findElement(By.xpath("//td[2]/textarea")).sendKeys("test data source");
			  new Select(driver.findElement(By.xpath("//tbody/tr/td[4]/select"))).selectByVisibleText("Summer 9ths");

			  

			  driver.findElement(By.xpath("//td[7]/input")).clear();
			  driver.findElement(By.xpath("//td[7]/input")).sendKeys("1096117-1-AABBY");
			  Thread.sleep(1000);
			  //driver.findElement(By.xpath("//td[10]/input")).clear();
			  //Thread.sleep(1000);
			  driver.findElement(By.xpath("//td[10]/input")).sendKeys("3.000");
			  Thread.sleep(1000);
			  driver.findElement(By.xpath("//td[11]/input")).clear();
			  driver.findElement(By.xpath("//td[11]/input")).sendKeys("30000");
			  Thread.sleep(1000);
			  driver.findElement(By.xpath("//tbody/tr/td[4]/select")).click();
			  new Select(driver.findElement(By.xpath("//td[15]/select"))).selectByVisibleText("Ready");
			  driver.findElement(By.xpath("//span/input[4]")).click();
		  }
		  else{
			  verificationErrors.append("The fiscal year cannot be selected");
		  }
		  
		  Thread.sleep(2000);
		  driver.switchTo().alert().accept();
		  
		  waitForElementPresent(By.cssSelector("b"));
		  commitmentNumber = driver.findElement(By.cssSelector("b")).getText();
		  writeToLogFile("Commitment Number " + commitmentNumber + " has been created", logFileWriter);
		  //writeTestResults("HandSOn TEST", "85", "(D.4) Verify 'Previous Year Commitments' in Control Sheet Detail Report.", "Verify faculty with a salary log entry of 'retroactive change' appear in the Control Sheet Detail Report");
		  
		  return commitmentNumber;
	  }
	  
	  public void getNamesForMeritPoolRetentionPromises(BufferedWriter logFileWriter) throws Exception{
		  HashMap<String, String> keysAndValues =  new HashMap<String, String>();
		  keysAndValues.put("Subcategory", "Retention Promises");
		  keysAndValues.put("Source Pool", "Regular Merit Pool");
		  keysAndValues.put("Rank", "Full Professor");
		  HashMap<Integer, String> facultyNames = fileUnderTest.getMultipleFacultyValues(fileUnderTestWorksheetName, keysAndValues, "Name", logFileWriter);
		  
		  for (Object key: facultyNames.keySet()) {
			    logFileWriter.write("row on which name is found: " + key.toString());
			    writeToLogFile(", name found: " + facultyNames.get(key).toString(), logFileWriter);
		  }// end for loop
	  }
	  
	  /*
	   * Given the input parameters of a Category and a Sub-category, we
	   * get a list of faculty names from the input file.
	   * These are the expected values.
	   * 
	   * We then retrieve a list of faculty names from
	   * the file under test.  These are the actual values.
	   * 
	   * We then compare the expected values with the actual values and
	   * make sure that (a) all expected values are present - even duplicates, 
	   * and (b) that there are no unexpected values present.
	   * 
	   */
	  
	  public void verifyDeptChairInfoInReport() throws Exception{
		  verificationErrors = new StringBuffer();
		  String testName = "HandSOn TEST 15 (A.4) Verify Chairs and Directors and Other Gov";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  //System.out.println("File to be written is " +logFileName);
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);

		  HashMap<String, String> keysAndValues =  new HashMap<String, String>();
		  keysAndValues.put("Category", "Governance Related");
		  keysAndValues.put("Subcategory", "Chairs/Directors");
		  
		  
		  if(verifyNamesForFacultyGroup(keysAndValues, logFileWriter, fileIn, fileUnderTest)){
			  writeToLogFile("Test passed", logFileWriter);
		  } else writeToLogFile("Test FAILED", logFileWriter);


		  
		  logFileWriter.close();
		  writeTestResults("HandSOn TEST", "15", "(A.4) Verify Chairs/Directors and Other Gov.", "Chair");
		  
	  }
	  
	  /*
	   * This is a GUI test and a cross-check of the Control Sheet Detail Report.
	   * It verifies that the number of the department chairs displayed on the 
	   * Control Sheet tab of Salary Setting is equal to the number of names
	   * in the Control Sheet Detail Report. 
	   *  
	   */
	  
	  public void verifyDeptChairNumberInfoInGUI() throws Exception{
		  verificationErrors = new StringBuffer();
		  String testName = "HandSOn TEST 24 (A.4) Verify Chairs and Directors and Other Gov";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  //System.out.println("File to be written is " +logFileName);
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);

		  if (openSalarySetting(logFileWriter)){
			  writeToLogFile("Successfully opened Salary Setting", logFileWriter);
		  }
		  else{ 
			  verificationErrors.append("Verify Department Chair Information failed: Couldn't open Salary Setting");
		  }
		  waitAndClick(By.id("control_a"));
		  /*
		  if (switchWindow(By.xpath("//tr[32]/td[2]")).equalsIgnoreCase("Got the window")){
			  System.out.println("Acquired Salary Setting Window");
		  }
		  else{
			  verificationErrors.append("Verify Department Chair Information failed: can't open Salary Setting Window");
		  }
		  */
		  waitForElementPresent(By.xpath("//tr[32]/td[2]"));
		  int numberOfChairs = getIntValueFromText(By.xpath("//tr[32]/td[2]"));
		  writeToLogFile("Derived value for number of Chairs is " + numberOfChairs, logFileWriter);
		  int raiseAmountChairs = getIntValueFromText(By.xpath("//tr[32]/td[3]"));
		  writeToLogFile("Derived value for raise amount for all chairs is " + raiseAmountChairs, logFileWriter);
		  
		  //verify that the number of commitments is as expected
		  /*
		   * There's an error in the logic.  The number in the GUI is a discrete list of names.  
		   * The number of rows might be very different.  There could be two department chair
		   * positions and two Senior Associate Dean positions, both held by the same 
		   * faculty member.  We should employ a TreeSet data structure with the list of names
		   * to ensure that doesn't happen.
		   */
		  HashMap<Integer, String> governanceCommitments = fileUnderTest.getMultipleCellValues(fileUnderTestWorksheetName, "Chairs/Directors", "Subcategory", "FTE Adj Total Commitment", logFileWriter);
		  writeToLogFile("Found " + governanceCommitments.size() + " rows in the spreadsheet with Governance Related Commitments", logFileWriter);
		  if (numberOfChairs == governanceCommitments.size()) writeToLogFile("The number of rows in the spreadsheet with Governance Related Commitments is as expected", logFileWriter);
		  else verificationErrors.append("Expected " + numberOfChairs + " rows, but found " + governanceCommitments.size() + " rows in the spreadsheet with Governance Related Commitments");
		  //now, verify that the total amount of raises is as expected
		  float sumOfChairs = 0;
		  for (Object key: governanceCommitments.keySet()) {

			  	logFileWriter.write("key : " + key.toString());
			    writeToLogFile(", value : " + governanceCommitments.get(key), logFileWriter);
			    String formattedKey = ((Long)NumberFormat.getNumberInstance(java.util.Locale.US).parse(governanceCommitments.get(key).toString())).toString();
			    sumOfChairs += new Integer(formattedKey).intValue();
			}
		  
		  if((int)sumOfChairs == raiseAmountChairs){
			  writeToLogFile("Sum of raise amounts is as expected", logFileWriter);
		  }
		  else{
			  String message = "Sum of raise amounts is not as expected; Expected " + raiseAmountChairs 
					  +", but actually got " + (int)sumOfChairs +" in the spreadsheet";
			  writeToLogFile(message, logFileWriter);
			  verificationErrors.append(message);
		  }
		  
		  logFileWriter.close();
		  writeTestResults("HandSOn TEST", "24", "(A.4) Verify Chairs/Directors and Other Gov.", "Chair who has 100% H&S appointment in one department/program.");
		  
	  }
	  
	  public void runReports() throws Exception {
		  verificationErrors = new StringBuffer();
		  String testName = "HandSOn Experimentation TEST 000 - Run Reports using Selenium";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
		  
		  writeToLogFile("Now attempting to get results from database", logFileWriter);
		  String query = "select f.unvl_id, f.ADD_UNVL_ID, p.person_first_name, p.person_middle_name, p.PERSON_LAST_NAME, "
				  +"f.faculty_status_type_cd, f.current_rank_cd, f.initial_su_hire_date," 
				  +"f.actual_tenure_date, f.exit_date, f.exit_status, f.cy_salary, f.cy_fte "
				  +"from faculty f, imirdx_person_t p " 
				  +"where f.unvl_id = 'HS_Parigi7' "
				  +"and f.unvl_id = p.person_unvl_id "
				  +"order by f.unvl_id";
		  runSQLQuery(query, logFileWriter);
		  /*
		  writeToLogFile("Now attempting to switch to the Report tab of Salary Setting", logFileWriter);
		  switchToReportTab();
		  Thread.sleep(60000);
		  
		  String fileName = "Control Sheet Report Suite.xls";
		  runOneReportAtLocator(By.xpath("//td/div/a[4]"), logFileWriter);
		  copyFile (fileName, logFileWriter);
		  
		  fileName = "Department Verification Report.xls";
		  runOneReportAtLocator(By.xpath("//a[14]"), logFileWriter);
		  switchWindowByURL("https://handsonweb-"+HandSOnTestSuite.machineName+".stanford.edu/"+HandSOnTestSuite.envName+"/processSalSetWorksheetRequest.do?ACTION=showDeptSaSettingReport", logFileWriter);
		  waitForElementPresent(By.id("clusterLevel"));
		  if (isDropdownAccessible (By.id("clusterLevel"), "Any", logFileWriter))
			  	writeToLogFile("Successfully selected Cluster value Any", logFileWriter);
		  else writeToLogFile("Could not select Cluster value Any", logFileWriter);
		  
		  if (isDropdownAccessible (By.id("reportFormatType"), "XLS", logFileWriter))
			  	writeToLogFile("Successfully selected Report Format value XLS", logFileWriter);
		  else writeToLogFile("Could not select Report Format value XLS", logFileWriter);

		  waitAndClick(By.id("buttonSubmit"));
		  copyFile (fileName, logFileWriter);
		  String originalWindowURL = "https://handsonweb-"+HandSOnTestSuite.machineName+".stanford.edu/"+HandSOnTestSuite.envName+"/processSalarySetting.do";
		  //get back to the original window
		  switchWindowByURL(originalWindowURL, logFileWriter);
		  
		  //before clicking on the next link, make sure that the right window has the focus.
		  String baseFileName = "Department Salary Setting Worksheet";
		  waitForElementPresent(By.linkText(baseFileName));

		  
		  runOneReportAtLocator(By.linkText(baseFileName), logFileWriter);
		  String[] department = {"Economics", "English", "History", "Mathematics", "Physics", "Theater & Performance Studies"};
		  String fileExtension = ".xlsm";
		  for (int index=0; index < department.length; index++){
			  fileName = baseFileName+ " " + department[index] + fileExtension;
			  switchWindowByURL("https://handsonweb-"+HandSOnTestSuite.machineName+".stanford.edu/"+HandSOnTestSuite.envName+"/processSalSetWorksheetRequest.do?ACTION=goToWorkSheet&dto.level=dept&dto.rptType=deptSalSetWksh", logFileWriter);
			  if (isDropdownAccessible (By.id("deptCd"), department[index], logFileWriter))
				  	writeToLogFile("Successfully selected Department value " + department[index], logFileWriter);
			  else writeToLogFile("Could not select Department value " + department[index], logFileWriter);
			  driver.findElement(By.id("generate")).click();
			  copyFile (fileName, logFileWriter);
		  }
		  
		  //now we work with the Provost Binder Lists By Reports
		  String linkText = "Provost Binder Lists by Reports";
		  fileName = "Provost Binder List by Department.xls" ;
		  String partialURL = "https://handsonweb-"+HandSOnTestSuite.machineName+".stanford.edu/"+HandSOnTestSuite.envName+"/processSalSetWorksheetRequest.do?ACTION=showListByReports";
		  
		  switchWindowByURL(originalWindowURL, logFileWriter);
		  runOneReportAtLocator(By.linkText(linkText), logFileWriter);
		  switchWindowByURL(partialURL, logFileWriter);
		  if (isDropdownAccessible (By.id("clusterLevel"), "Department", logFileWriter)){
			  writeToLogFile("Successfully selected Department value on the Provost Binder Report Dialog", logFileWriter);
		  }
		  else writeToLogFile("Could not select Department value on the Provost Binder Report Dialog", logFileWriter);
		  waitAndClick(By.id("buttonSubmit"));
		  copyFile (fileName, logFileWriter);
		  
		  //now, run the School Salary Setting Worksheet
		  switchWindowByURL(originalWindowURL, logFileWriter);
		  linkText = "School Salary Setting Worksheet";
		  fileName = "School Salary Setting Worksheet.xlsx";
		  partialURL = "https://handsonweb-"+HandSOnTestSuite.machineName+".stanford.edu/"+HandSOnTestSuite.envName+"/processSalSetWorksheetRequest.do?ACTION=goToWorkSheet&dto.level=school&dto.rptType=schoolSalSetWksh";
		  
		  runOneReportAtLocator(By.linkText(linkText), logFileWriter);
		  switchWindowByURL(partialURL, logFileWriter);
		  waitAndClick(By.id("generate"));
		  copyFile (fileName, logFileWriter);

		  //Now, generate the Master Salary Setting Report
		  switchWindowByURL(originalWindowURL, logFileWriter);
		  linkText = "Master Salary Setting Report";
		  fileName = "Master Salary Setting Report.xls";
		  runOneReportAtLocator(By.linkText(linkText), logFileWriter);
		  copyFile (fileName, logFileWriter);
		  */

		  logFileWriter.close();
	  }
	  
	  public void switchToReportTab() throws Exception{
		  waitForElementPresent(By.id("report_a"));
		  waitAndClick(By.id("report_a"));
	  }
	  
	  public void runOneReportAtLocator(By by, BufferedWriter logFileWriter) throws Exception{
		  writeToLogFile("Now verifying that we have successfully switched to the Report tab of Salary Setting", logFileWriter);
		  waitForElementPresent(by);
		  writeToLogFile("Now attempting to run the report through the locator " + by.toString(), logFileWriter);
		  waitAndClick(by);
	  }
	  
	  public void copyFile(String fileName, BufferedWriter logFileWriter) throws Exception{
		  String sourceFileName = "C:\\Users\\christ13\\Downloads\\" + fileName;
		  File reportFile = new File(sourceFileName);
		  if (reportFile.exists()) writeToLogFile("File "+ sourceFileName +" has been created", logFileWriter);
		  else {
			  String message = "File "+ sourceFileName +" has not been created in the original directory - waiting one minute before copying";
			  System.out.println(message);
			  writeToLogFile(message, logFileWriter);
			  Thread.sleep(60000);
		  }
		  
		  writeToLogFile("Now attempting to copy the file from the old location to the new one", logFileWriter);
		  String destFileName = "C:\\Users\\christ13\\Documents\\HandSOn\\" + fileName;
		  File newReportFile = new File(destFileName);
		  if (newReportFile.exists()){ 
			  	writeToLogFile("Report file already exists in the new location " + destFileName + " - must delete and recreate", logFileWriter);
			  	FileUtils.forceDelete(newReportFile);
			  	Thread.sleep(2000);
			  	newReportFile = new File(destFileName);
		  }
		  else writeToLogFile("Report file does NOT already exist in the new location", logFileWriter);
		  
		  FileUtils.copyFile(reportFile, newReportFile);
		  if (newReportFile.exists()) 
			  	writeToLogFile("Report file successfully copied from the old location to the new one", logFileWriter);
		  else writeToLogFile("Report file NOT successfully copied from the old location to the new one", logFileWriter);
		  
		  FileUtils.forceDelete(reportFile);		  
	  }
	  
	  
	  public void verifyTerminalYearFaculty() throws Exception{
		  verificationErrors = new StringBuffer();
		  //retrieve the expected values
		  //find out what the row is that has the expected Terminal Year faculty data
		  String testName = "HandSOn TEST 5 (A.1) Verify Pre-Defined Use Cases - Regular Faculty. Faculty who is in Terminal Year.";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  //System.out.println("File to be written is " +logFileName);
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);


		  HashMap<String, String> keysAndValues =  new HashMap<String, String>();
		  keysAndValues.put("Category", "Faculty Appointment Related");
		  keysAndValues.put("Subcategory", "Terminal Year");
		  
		  if(verifyNamesForFacultyGroup(keysAndValues, logFileWriter, fileIn, fileUnderTest)){
			  writeToLogFile("Test passed", logFileWriter);
		  } else writeToLogFile("Test FAILED", logFileWriter);
		  


		  logFileWriter.close();

		  writeTestResults("HandSOn TEST", "5", "(A.1) Verify Pre-Defined Use Cases (Regular Faculty).", "Faculty who is in Terminal Year.");
	  }
	  
	  public void verifyMidYearFaculty() throws Exception{
		  verificationErrors = new StringBuffer();
		  String testName = "HandSOn TEST 8 (A.1) Verify Pre-Defined Use Cases (Regular Faculty). Faculty who has Mid-Year adjustment (entry in salary log) since previous year salary setting.";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  //System.out.println("File to be written is " +logFileName);
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
		  
		  HashMap<String, String> keysAndValues =  new HashMap<String, String>();
		  keysAndValues.put("Category", "Previous Year Commitments");
		  keysAndValues.put("Subcategory", "Mid-Year");
		  
		  
		  if(verifyNamesForFacultyGroup(keysAndValues, logFileWriter, fileIn, fileUnderTest)){
			  writeToLogFile("Test passed", logFileWriter);
		  } else writeToLogFile("Test FAILED", logFileWriter);

		  
		  logFileWriter.close();

		  writeTestResults("HandSOn TEST", "8", "(A.1) Verify Pre-Defined Use Cases (Regular Faculty).", "Faculty who has Mid-Year adjustment (entry in salary log) since previous year salary setting.");
		  
	  }
	  
	  public void verifyRegularAPFaculty() throws Exception{
		  verificationErrors = new StringBuffer();
		  String testName = "HandSOn TEST 14 (A.3) Verify Regular Faculty with A&P. Faculty who has A&P event with salary increase effective on 9-1.";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  //System.out.println("File to be written is " +logFileName);
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
		  
		  HashMap<String, String> keysAndValues =  new HashMap<String, String>();
		  keysAndValues.put("Category", "Faculty Appointment Related");
		  	  
		  if(verifyNamesForFacultyGroup(keysAndValues, logFileWriter, fileIn, fileUnderTest)){
			  writeToLogFile("Test passed", logFileWriter);
		  } else writeToLogFile("Test FAILED", logFileWriter);

		  
		  logFileWriter.close();

		  writeTestResults("HandSOn TEST", "14", "(A.3) Verify Regular Faculty with A&P.", "Faculty who has A&P event with salary increase effective on 9-1.");
		  
	  }

	  public void verifyRegularSADFaculty() throws Exception{
		  verificationErrors = new StringBuffer();
		  String testName = "HandSOn TEST 16 (A.4) Verify Other Gov. Senior Associate Dean";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  //System.out.println("File to be written is " +logFileName);
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
		  
		  HashMap<String, String> keysAndValues =  new HashMap<String, String>();
		  keysAndValues.put("Category", "Governance Related");
		  keysAndValues.put("Subcategory", "Other Gov");
		  	  
		  if(verifyNamesForFacultyGroup(keysAndValues, logFileWriter, fileIn, fileUnderTest)){
			  writeToLogFile("Test passed", logFileWriter);
		  } else writeToLogFile("Test FAILED", logFileWriter);

		  
		  logFileWriter.close();

		  writeTestResults("HandSOn TEST", "16", "(A.4) Verify Other Gov.", "Senior Associate Dean");
		  
	  }
	  
	  public void verifyRegularFacultyWithSalaryPromise() throws Exception{
		  verificationErrors = new StringBuffer();
		  String testName = "HandSOn TEST 17 (A.2) Verify Regular Faculty with Salary Promise. Faculty who has salary promise effective dated 9-1-xx from a success retention.";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  //System.out.println("File to be written is " +logFileName);
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
		  
		  String name = "Skotheim, Jan";
		  String[] columnNames = {"Category", "Subcategory", "Stage", "Effective Date"};
		  
		  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName)){
			  writeToLogFile("Test passed", logFileWriter);
		  } else writeToLogFile("Test FAILED", logFileWriter);

		  logFileWriter.close();
		  writeTestResults("HandSOn TEST", "17", "(A.2) Verify Regular Faculty with Salary Promise.", "Faculty who has salary promise effective dated 9-1-xx from a success retention.");
	  }
	  
	  public void verifyFacultyWithSalaryPromiseIrregularDate() throws Exception{
		  verificationErrors = new StringBuffer();
		  String testName = "HandSOn TEST 18 (A.2) Verify Regular Faculty with Salary Promise. Faculty who has salary promise effective dated 7-1-xx (any non-9-1 date) from a success retention";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  //System.out.println("File to be written is " +logFileName);
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
		  
		  String name = "Bashir, Shahzad";
		  String[] columnNames = {"Category", "Subcategory", "Stage", "Effective Date"};
		  
		  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName)){
			  writeToLogFile("Test passed", logFileWriter);
		  } else writeToLogFile("Test FAILED", logFileWriter);

		  logFileWriter.close();
		  writeTestResults("HandSOn TEST", "18", "(A.2) Verify Regular Faculty with Salary Promise.", "Faculty who has salary promise effective dated 7-1-xx (any non-9-1 date) from a success retention");
		  
	  }

	  public void verifyFacultyWithSalaryPromiseAndMktRetention() throws Exception{
		  verificationErrors = new StringBuffer();
		  String testName = "HandSOn TEST 21 (A.2) Verify Regular Faculty with Salary Promise. Faculty has salary promise and is also eligible for more than one pool.  For example, faculty is one of the designees of Market Retention Pool.";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  //System.out.println("File to be written is " +logFileName);
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
		  
		  String name = "Chatterjee, Sourav";
		  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Effective Date"};
		  
		  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName)){
			  writeToLogFile("Test passed", logFileWriter);
		  } else writeToLogFile("Test FAILED", logFileWriter);

		  logFileWriter.close();
		  writeTestResults("HandSOn TEST", "21", "(A.2) Verify Regular Faculty with Salary Promise.", "Faculty has salary promise and is also eligible for more than one pool.  For example, faculty is one of the designees of Market Retention Pool.");
		  
	  }
	  
	  public void verifyFacultyWithRegularAPRegularDates() throws Exception{
		  verificationErrors = new StringBuffer();
		  String testName = "HandSOn TEST 22 (A.3) Verify Regular Faculty with A&P. Faculty who has A&P event with salary increase effective on 9-1";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  //System.out.println("File to be written is " +logFileName);
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
		  
		  String[] names = {"Hill, Leslie", "Paris, Helen", "Thiranagama, Sharika", "Menon, Jisha", "Hartnoll, Sean", "Wieman, Carl", "Hoxby, Blair", "Vasy, Andras", "Donaldson, David"};
		  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment"};
		  
		  boolean[] matches = verifyValuesForMultipleFacultyNames(names, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName);
		  
		  for (int i = 0; i < matches.length; i++){
			  if (! matches[i]){
				  String errMsg = "Test FAILED due to mismatch on faculty " + names[i];
				  writeToLogFile(errMsg, logFileWriter);
				  verificationErrors.append(errMsg);
			  }
		  }

		  logFileWriter.close();
		  writeTestResults("HandSOn TEST", "22", "(A.3) Verify Regular Faculty with A&P.", "Faculty who has A&P event with salary increase effective on 9-1");
		  
	  }
	  

	  public void verifyFacultyWithRegularAPIrregularDates() throws Exception{
		  verificationErrors = new StringBuffer();
		  String testName = "HandSOn TEST 23 (A.3) Verify Regular Faculty with A&P. Faculty who has A&P event with salary increase effective on non 9-1 (e.g. 7-1).";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  //System.out.println("File to be written is " +logFileName);
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
		  
		  String name = "Tambar, Kabir";
		  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment"};
		  
		  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName)){
			  writeToLogFile("Test passed", logFileWriter);
		  } else writeToLogFile("Test FAILED", logFileWriter);

		  logFileWriter.close();
		  writeTestResults("HandSOn TEST", "23", "(A.3) Verify Regular Faculty with A&P.", "Faculty who has A&P event with salary increase effective on non 9-1 (e.g. 7-1).");		  
	  }

	  public void verifyDepartmentChairOneHundredPercent() throws Exception{
		  verificationErrors = new StringBuffer();
		  String testName = "HandSOn TEST 24 (A.4) Verify Chairs,Directors and Other Gov. Chair who has 100% H&S appointment in one department or program.";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  //System.out.println("File to be written is " +logFileName);
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
		  
		  String name = "Edelstein, Dan";
		  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment"};
		  
		  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName)){
			  writeToLogFile("Test passed", logFileWriter);
		  } else writeToLogFile("Test FAILED", logFileWriter);

		  logFileWriter.close();
		  writeTestResults("HandSOn TEST", "24", "(A.4) Verify Chairs/Directors and Other Gov.", "Chair who has 100% H&S appointment in one department or program.");		  
	  }

	  public void verifyDepartmentChairOneHundredPercentTwoDepts() throws Exception{
		  verificationErrors = new StringBuffer();
		  String testName = "HandSOn TEST 25 (A.4) Verify Chairs,Directors and Other Gov. Chair who has 100% H&S appointment in two departments or programs.";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  //System.out.println("File to be written is " +logFileName);
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
		  
		  String name = "Candes, Emmanuel";
		  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment"};
		  
		  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName)){
			  writeToLogFile("Test passed", logFileWriter);
		  } else writeToLogFile("Test FAILED", logFileWriter);

		  logFileWriter.close();
		  writeTestResults("HandSOn TEST", "25", "(A.4) Verify Chairs, Directors and Other Gov.", "Chair who has 100% H&S appointment in two departments or programs.");		  
	  }

	  
	  public void verifyDepartmentChairOneHundredPercentTwoSchools() throws Exception{
		  verificationErrors = new StringBuffer();
		  String testName = "HandSOn TEST 26 (A.4) Verify Chairs, Directors and Other Gov. Chair who has 100% FTE - H&S appointment and non-H&S appointment.";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  //System.out.println("File to be written is " +logFileName);
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
		  
		  String[] names = {"Wieman, Carl", "Jurafsky, Daniel", "Zhou, Xueguang"};
		  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment"};
		  
		  boolean[] matches = verifyValuesForMultipleFacultyNames(names, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName);
		  
		  for (int i = 0; i < matches.length; i++){
			  if (! matches[i]){
				  String errMsg = "Test FAILED due to mismatch on faculty " + names[i];
				  writeToLogFile(errMsg, logFileWriter);
				  verificationErrors.append(errMsg);
			  }
		  }

		  logFileWriter.close();
		  writeTestResults("HandSOn TEST", "26", "(A.4) Verify Chairs, Directors and Other Gov.", "Chair who has 100% FTE - H&S appointment and non-H&S appointment.");
		  
	  }

	  public void verifyDepartmentChairFiftyPercentTwoSchools() throws Exception{
		  verificationErrors = new StringBuffer();
		  String testName = "HandSOn TEST 27 (A.4) Verify Chairs,Directors and Other Gov. Chair who has total FTE less than 100%. (e.g. 25% with physics and 25% with SLAC).";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  //System.out.println("File to be written is " +logFileName);
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
		  
		  String name = "Hodgson, Keith";
		  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment"};
		  
		  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName)){
			  writeToLogFile("Test passed", logFileWriter);
		  } else writeToLogFile("Test FAILED", logFileWriter);

		  logFileWriter.close();
		  writeTestResults("HandSOn TEST", "27", "(A.4) Verify Chairs, Directors and Other Gov.", "Chair who has total FTE less than 100%. (e.g. 25% with physics and 25% with SLAC).");		  
	  }
	  
	  public void verifyProgramDirectorMeritPool() throws Exception{
		  verificationErrors = new StringBuffer();
		  String testName = "HandSOn TEST 28 (A.4) Verify Chairs, Directors and Other Gov. Program Director has 'governance' allocation from Regular Merit Pool.";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  //System.out.println("File to be written is " +logFileName);
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
		  
		  String[] names = {"Winterer, Caroline", "Tomz, Michael"};
		  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment"};
		  
		  boolean[] matches = verifyValuesForMultipleFacultyNames(names, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName);
		  
		  for (int i = 0; i < matches.length; i++){
			  if (! matches[i]){
				  String errMsg = "Test FAILED due to mismatch on faculty " + names[i];
				  writeToLogFile(errMsg, logFileWriter);
				  verificationErrors.append(errMsg);
			  }
		  }

		  logFileWriter.close();
		  writeTestResults("HandSOn TEST", "28", "(A.4) Verify Chairs, Directors and Other Gov.", "Program Director has 'governance' allocation from Regular Merit Pool.");
		  
		  
	  }

	  public void verifyDepartmentChairRetentionPromiseNineOne() throws Exception{
		  verificationErrors = new StringBuffer();
		  String testName = "HandSOn TEST 29 (A.4) Verify Chairs,Directors and Other Gov. Governance has salary promise from a success retention with effective date 9-1.";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  //System.out.println("File to be written is " +logFileName);
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
		  
		  String name = "Findlen, Paula";
		  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "Effective Date", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool"};
		  
		  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName)){
			  writeToLogFile("Test passed", logFileWriter);
		  } else writeToLogFile("Test FAILED", logFileWriter);

		  logFileWriter.close();
		  writeTestResults("HandSOn TEST", "29", "(A.4) Verify Chairs, Directors and Other Gov.", "Governance has salary promise from a success retention with effective date 9-1.");		  
		  
	  }

	  public void verifyDepartmentChairRetentionPromiseNotNineOne() throws Exception{
		  verificationErrors = new StringBuffer();
		  String testName = "HandSOn TEST 30 (A.4) Verify Chairs,Directors and Other Gov. Governance' has salary promise from a success retention with effective date 7-1 (or any non-9-1 date).";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  //System.out.println("File to be written is " +logFileName);
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
		  
		  String name = "Harrison, Paul";
		  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "Effective Date", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Description", "Notes"};
		  
		  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName)){
			  writeToLogFile("Test passed", logFileWriter);
		  } else writeToLogFile("Test FAILED", logFileWriter);

		  logFileWriter.close();
		  writeTestResults("HandSOn TEST", "30", "(A.4) Verify Chairs, Directors and Other Gov.", "Governance' has salary promise from a success retention with effective date 7-1 (or any non-9-1 date).");		  
		  
	  }
	  
	  public void verifyDepartmentChairSuccessfulRecruitmentNotNineOne() throws Exception{
		  verificationErrors = new StringBuffer();
		  String testName = "HandSOn TEST 31 (A.4) Verify Chairs,Directors and Other Gov. Governance' has salary promise from a success recruitment with effective date non-9-1 date";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  //System.out.println("File to be written is " +logFileName);
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
		  
		  String name = "Freese, Jeremy";
		  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "Effective Date", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Description"};
		  
		  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName)){
			  writeToLogFile("Test passed", logFileWriter);
		  } else writeToLogFile("Test FAILED", logFileWriter);

		  logFileWriter.close();
		  writeTestResults("HandSOn TEST", "31", "(A.4) Verify Chairs, Directors and Other Gov.", "Governance' has salary promise from a success recruitment with effective date 7-1 (or any non-9-1 date).");		  
		  
	  }
	  
	  public void verifyDepartmentChairSuccessfulRecruitmentNineOne() throws Exception{
		  verificationErrors = new StringBuffer();
		  String testName = "HandSOn TEST 32 (A.4) Verify Chairs,Directors and Other Gov. Governance' has salary promise from a success recruitment with effective date 9-1";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  //System.out.println("File to be written is " +logFileName);
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
		  
		  String name = "Lee, Chang-rae";
		  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "Effective Date", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Description"};
		  
		  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName)){
			  writeToLogFile("Test passed", logFileWriter);
		  } else writeToLogFile("Test FAILED", logFileWriter);

		  logFileWriter.close();
		  writeTestResults("HandSOn TEST", "32", "(A.4) Verify Chairs, Directors and Other Gov.", "Chair has salary promise from an accepted recruitment.");		  
		  
	  }
	  
	  
	  
	  
	  public void verifyDepartmentChairOverride() throws Exception{
		  verificationErrors = new StringBuffer();
		  String testName = "HandSOn TEST 33 (A.4) Verify Chairs,Directors and Other Gov. Faculty who has been overriden as 'Chair'.";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  //System.out.println("File to be written is " +logFileName);
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
		  
		  String name = "Mabuchi, Hideo";
		  String[] columnNames = {"Department", "Name", "Rank", "Salary", "Override", "Description"};
		  
		  String fileUnderTestName = "Chair Salary Setting Worksheet Natural Sciences.xls";
		  HSSFExcelReader departmentChairWorkbook = new HSSFExcelReader(directory + fileUnderTestName);
		  
		  String departmentChairWorksheetName = "Chair Salary Setting";
		  
		  int rowForColumnNames = departmentChairWorkbook.getCellRowNum(departmentChairWorksheetName, 0, "Faculty Overridden from Salary Setting Worksheet", 0);
		  writeToLogFile("row for column names is initialized at " + new Integer(rowForColumnNames).toString(), logFileWriter);
		  departmentChairWorkbook.setRowForColumnNames(rowForColumnNames);
		  writeToLogFile("leftmost column of Department Chair logsheet column header row is as follows: " + departmentChairWorkbook.getCellData(departmentChairWorksheetName, 0, rowForColumnNames), logFileWriter);
		  writeToLogFile("leftmost column of Department Chair logsheet data is as follows: " + departmentChairWorkbook.getCellData(departmentChairWorksheetName, 0, rowForColumnNames+1), logFileWriter);
		  
		  String departmentChairWorksheetInputName = "DepartmentChairWorksheetInput";
		  
		  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, departmentChairWorksheetInputName, departmentChairWorkbook, departmentChairWorksheetName)){
			  writeToLogFile("Test passed", logFileWriter);
		  } else writeToLogFile("Test FAILED", logFileWriter);
		  
		  name = "Digonnet, Michel";

		  if (! verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, departmentChairWorksheetInputName, departmentChairWorkbook, departmentChairWorksheetName)){
			  writeToLogFile("Test passed", logFileWriter);
		  } else writeToLogFile("Test FAILED", logFileWriter);

		  logFileWriter.close();
		  writeTestResults("HandSOn TEST", "33", "(A.4) Verify Chairs, Directors and Other Gov.", "Faculty who has been overriden as 'Chair'.");		  
		  
	  }

	  
	  public void verifyDepartmentChairRetentionPromiseMktRetention() throws Exception{
		  verificationErrors = new StringBuffer();
		  String testName = "HandSOn TEST 34 (A.4) Verify Chairs,Directors and Other Gov. Governance' has retention salary promise and is also eligible for Market Retention Pool.";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  //System.out.println("File to be written is " +logFileName);
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
		  
		  String name = "Findlen, Paula";
		  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "Effective Date", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool"};
		  
		  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName)){
			  writeToLogFile("Test passed", logFileWriter);
		  } else writeToLogFile("Test FAILED", logFileWriter);

		  logFileWriter.close();
		  writeTestResults("HandSOn TEST", "34", "(A.4) Verify Chairs, Directors and Other Gov.", "Governance' has retention salary promise and is also eligible for Market Retention Pool.");		  
		  
	  }
	  
	  public void verifyClusterDeansMeritPool() throws Exception{
		  verificationErrors = new StringBuffer();
		  String testName = "HandSOn TEST 35 (A.4) Verify Chairs, Directors and Other Gov. Cluster Dean has 'Sr. Associate Deans' allocation (as defined in Salary Increases and Percentages box) from Regular Merit Pool.";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  //System.out.println("File to be written is " +logFileName);
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
		  
		  String[] names = {"Cohen, Ralph", "Saller, Richard"};
		  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment"};
		  
		  boolean[] matches = verifyValuesForMultipleFacultyNames(names, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName);
		  
		  for (int i = 0; i < matches.length; i++){
			  if (! matches[i]){
				  String errMsg = "Test FAILED due to mismatch on faculty " + names[i];
				  writeToLogFile(errMsg, logFileWriter);
				  verificationErrors.append(errMsg);
			  }
		  }

		  logFileWriter.close();
		  writeTestResults("HandSOn TEST", "35", "(A.4) Verify Chairs, Directors and Other Gov.", "Cluster Dean has 'Sr. Associate Deans' allocation (as defined in Salary Increases and Percentages box) from Regular Merit Pool.");
		  
		  
	  }

	  public void verifyClusterDeanRetentionPromise() throws Exception{
		  verificationErrors = new StringBuffer();
		  String testName = "HandSOn TEST 36 (A.4) Verify Chairs, Directors and Other Gov. Cluster Dean has salary promise from an accepted retention with effective date 9-1.";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  //System.out.println("File to be written is " +logFileName);
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
		  
		  String name = "Cohen, Ralph";
		  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "Effective Date", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool"};
		  
		  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName)){
			  writeToLogFile("Test passed", logFileWriter);
		  } else writeToLogFile("Test FAILED", logFileWriter);

		  logFileWriter.close();
		  writeTestResults("HandSOn TEST", "36", "(A.4) Verify Chairs, Directors and Other Gov.", "Cluster Dean has salary promise from an accepted retention with effective date 9-1.");		  
		  
	  }
	  
	  public void verifyClusterDeanOverride() throws Exception{
		  verificationErrors = new StringBuffer();
		  String testName = "HandSOn TEST 37 (A.4) Verify Chairs,Directors and Other Gov. Faculty who has been overriden as 'Cluster Dean'.";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  //System.out.println("File to be written is " +logFileName);
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
		  
		  String name = "Markman, Ellen";
		  String[] columnNames = {"Cluster", "Department", "Name", "Rank", "Salary", "Override", "Description"};
		  
		  String fileUnderTestName = "Dean Salary Setting Worksheet.xls";
		  HSSFExcelReader clusterDeanWorkbook = new HSSFExcelReader(directory + fileUnderTestName);
		  
		  String clusterDeanWorksheetName = "Dean Salary Setting";
		  
		  int rowForColumnNames = clusterDeanWorkbook.getCellRowNum(clusterDeanWorksheetName, 0, "Faculty Overridden from Salary Setting Worksheet", 0);
		  writeToLogFile("row for column names is initialized at " + new Integer(rowForColumnNames).toString(), logFileWriter);
		  clusterDeanWorkbook.setRowForColumnNames(rowForColumnNames);
		  writeToLogFile("leftmost column of Cluster Dean logsheet column header row is as follows: " + clusterDeanWorkbook.getCellData(clusterDeanWorksheetName, 0, rowForColumnNames), logFileWriter);
		  writeToLogFile("leftmost column of Cluster Dean logsheet data is as follows: " + clusterDeanWorkbook.getCellData(clusterDeanWorksheetName, 0, rowForColumnNames+1), logFileWriter);
		  
		  String clusterDeanWorksheetInputName = "ClusterDeanWorksheetInput";
		  
		  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, clusterDeanWorksheetInputName, clusterDeanWorkbook, clusterDeanWorksheetName)){
			  writeToLogFile("Test passed", logFileWriter);
		  } else writeToLogFile("Test FAILED", logFileWriter);
		  
		  name = "Byer, Robert";
		  if (! verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, clusterDeanWorksheetInputName, clusterDeanWorkbook, clusterDeanWorksheetName)){
			  writeToLogFile("Test passed", logFileWriter);
		  } else writeToLogFile("Test FAILED", logFileWriter);

		  logFileWriter.close();
		  writeTestResults("HandSOn TEST", "37", "(A.4) Verify Chairs, Directors and Other Gov.", "Faculty who has been overriden as 'Cluster Dean'.");		  
		  
	  }

	  
	  	  public void verifyClusterDeanRetentionPromiseMktRetention() throws Exception{
		  verificationErrors = new StringBuffer();
		  String testName = "HandSOn TEST 38 (A.4) Verify Chairs,Directors and Other Gov. Cluster Dean has salary promise from an accepted retention and is also eligible for more than one pool.";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  //System.out.println("File to be written is " +logFileName);
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
		  
		  String name = "Cohen, Ralph";
		  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "Effective Date", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool"};
		  
		  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName)){
			  writeToLogFile("Test passed", logFileWriter);
		  } else writeToLogFile("Test FAILED", logFileWriter);

		  logFileWriter.close();
		  writeTestResults("HandSOn TEST", "38", "(A.4) Verify Chairs, Directors and Other Gov.", "Cluster Dean has salary promise from an accepted retention and is also eligible for more than one pool.");		  
		  
	  }
	  
	  	  
		  public void verifyPositiveKnownRegularMeritPoolASC() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 40 (B.1) Enter Additional Salary Considerations for a faculty. Create ASC entry in Known stage with positive amount for each 'category' for a faculty with DO amount from Regular Merit Pool.";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String[] names = {"Abramitzky, Ran", "Anderson, R.", "Barletta, Vincent", "Saller, Richard"};
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool"};
			  
			  boolean[] matches = verifyValuesForMultipleFacultyNames(names, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName);
			  
			  for (int i = 0; i < matches.length; i++){
				  if (! matches[i]){
					  String errMsg = "Test FAILED due to mismatch on faculty " + names[i];
					  writeToLogFile(errMsg, logFileWriter);
					  verificationErrors.append(errMsg);
				  }
			  }

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "40", "(B.1) Enter Additional Salary Considerations for a faculty.", "Create ASC entry in Known stage with positive amount for each 'category' for a faculty with DO amount from Regular Merit Pool.");
			  
			  
		  }

		  public void verifyNegativeKnownRegularMeritPoolASC() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 41 (B.1) Enter Additional Salary Considerations for a faculty. Create ASC entry in Known stage with negative amount for each 'category' for a faculty with DO amount from Regular Merit Pool.";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String[] names = {"Alduy, Cecile", "Allen, Steven", "Acharya, Avidit", "Sano, Stephen"};
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool"};
			  
			  boolean[] matches = verifyValuesForMultipleFacultyNames(names, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName);
			  
			  for (int i = 0; i < matches.length; i++){
				  if (! matches[i]){
					  String errMsg = "Test FAILED due to mismatch on faculty " + names[i];
					  writeToLogFile(errMsg, logFileWriter);
					  verificationErrors.append(errMsg);
				  }
			  }

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "41", "(B.1) Enter Additional Salary Considerations for a faculty.", "Create ASC entry in Known stage with negative amount for each 'category' for a faculty with DO amount from Regular Merit Pool.");
		  }
		  
		  public void verifyPositiveKnownMarketPoolASC() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 42 (B.1) Enter Additional Salary Considerations for a faculty. Create ASC entry in Known stage with positive amount for each 'category' for a faculty from Market Pool.";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String[] names = {"Bagwell, Kyle", "Stearns, Timothy", "Applebaum, Mark", "Anttila, Arto"};
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool"};
			  
			  boolean[] matches = verifyValuesForMultipleFacultyNames(names, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName);
			  
			  for (int i = 0; i < matches.length; i++){
				  if (! matches[i]){
					  String errMsg = "Test FAILED due to mismatch on faculty " + names[i];
					  writeToLogFile(errMsg, logFileWriter);
					  verificationErrors.append(errMsg);
				  }
			  }

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "42", "(B.1) Enter Additional Salary Considerations for a faculty.", "Create ASC entry in Known stage with positive amount for each 'category' for a faculty from Market Pool.");
			  
			  
		  }

		  
		  public void verifyNegativeKnownMarketPoolASC() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 43 (B.1) Enter Additional Salary Considerations for a faculty. Create ASC entry in Known stage with negative amount for each 'category' for a faculty from Market Pool.";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String[] names = {"Bailenson, Jeremy", "Baker, Keith", "Barry, Fabio", "Michelson, Peter"};
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool"};
			  
			  boolean[] matches = verifyValuesForMultipleFacultyNames(names, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName);
			  
			  for (int i = 0; i < matches.length; i++){
				  if (! matches[i]){
					  String errMsg = "Test FAILED due to mismatch on faculty " + names[i];
					  writeToLogFile(errMsg, logFileWriter);
					  verificationErrors.append(errMsg);
				  }
			  }

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "43", "(B.1) Enter Additional Salary Considerations for a faculty.", "Create ASC entry in Known stage with negative amount for each 'category' for a faculty from Market Pool.");
			  
			  
		  }

		  public void verifyPositivePendingRegularMeritPoolASC() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 44 (B.1) Enter Additional Salary Considerations for a faculty. Create ASC entry in Pending stage with positive amount for each 'category' for a faculty with DO amount from Regular Merit Pool.";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String[] names = {"Barth, George", "Basu, Riddhipratim", "Bauer, Andrew", "Parker, Grant"};
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool"};
			  
			  boolean[] matches = verifyValuesForMultipleFacultyNames(names, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName);
			  
			  for (int i = 0; i < matches.length; i++){
				  if (! matches[i]){
					  String errMsg = "Test FAILED due to mismatch on faculty " + names[i];
					  writeToLogFile(errMsg, logFileWriter);
					  verificationErrors.append(errMsg);
				  }
			  }

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "44", "(B.1) Enter Additional Salary Considerations for a faculty.", "Create ASC entry in Pending stage with positive amount for each 'category' for a faculty with DO amount from Regular Merit Pool.");
		  }

		  public void verifyPositiveEstimatedRegularMeritPoolASC() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 45 (B.1) Enter Additional Salary Considerations for a faculty. Create ASC entry in Estimated stage with positive amount for each 'category' for a faculty with DO amount from Regular Merit Pool.";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String[] names = {"Beinin, Joel", "Berger, Jonathan", "Bauer, Andrew", "Turner, Frederick"};
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool"};
			  
			  boolean[] matches = verifyValuesForMultipleFacultyNames(names, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName);
			  
			  for (int i = 0; i < matches.length; i++){
				  if (! matches[i]){
					  String errMsg = "Test FAILED due to mismatch on faculty " + names[i];
					  writeToLogFile(errMsg, logFileWriter);
					  verificationErrors.append(errMsg);
				  }
			  }

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "45", "(B.1) Enter Additional Salary Considerations for a faculty.", "Create ASC entry in Estimated stage with positive amount for each 'category' for a faculty with DO amount from Regular Merit Pool.");
		  }
  
		  public void verifyPositiveEstimatedUnknownFacultyRegularMeritPoolASC() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 46 (B.1) Enter Additional Salary Considerations for a faculty - Estimated stage with positive amount for an Unknown faculty with DO amount from Regular Merit Pool.";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String name = "";
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "Effective Date", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool"};
			  
			  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName)){
				  writeToLogFile("Test passed", logFileWriter);
			  } else writeToLogFile("Test FAILED", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "46", "(B.1) Enter Additional Salary Considerations for a faculty.", "Create ASC entry in Estimated stage with positive amount for each 'category' for an Unknown faculty with DO amount from Regular Merit Pool.");
		  }

		  
		  public void verifyPositiveKnownAndPendingFacultyRegularMeritPoolASC() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 47 (B.1) Enter Additional Salary Considerations for a faculty - two ASC entries -  for Promotion to Full - for the same faculty - one Known stage and one Pending stage";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String name = "Bergmann, Dominique";
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "Effective Date", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool"};
			  
			  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName)){
				  writeToLogFile("Test passed", logFileWriter);
			  } else writeToLogFile("Test FAILED", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "47", "(B.1) Enter Additional Salary Considerations for a faculty.", "Create two ASC entries for the same faculty; one is extra increase above the pool in 'Known' stage and one is in 'Pending' stage for 'Promotion to Full'");
		  }

		  public void verifyPositiveKnownLessThanFTEFacultyRegularMeritPoolASC() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 48 (B.1) Enter Additional Salary Considerations for a faculty. Create ASC for a faculty who has H&S appointment less than 100% FTE. Enter a DO amount and a Department Amount";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String name = "Bender, John";
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "Effective Date", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool"};
			  
			  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName)){
				  writeToLogFile("Test passed", logFileWriter);
			  } else writeToLogFile("Test FAILED", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "48", "(B.1) Enter Additional Salary Considerations for a faculty.", "Create ASC for a faculty who has H&S appointment less than 100% FTE. Enter a DO amount and a Department Amount");
		  }

		  public void verifyPositiveKnownGovernanceRelatedASC() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 50 (B.2) Manually Enter Additional Salary Considerations for Governance. Create ASC entry in Known stage with positive amount for Chair and Cluster Dean and Program Director";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String[] names = {"Sano, Stephen", "Stearns, Timothy", "Michelson, Peter", "Saller, Richard"};
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool"};
			  
			  boolean[] matches = verifyValuesForMultipleFacultyNames(names, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName);
			  
			  for (int i = 0; i < matches.length; i++){
				  if (! matches[i]){
					  String errMsg = "Test FAILED due to mismatch on faculty " + names[i];
					  writeToLogFile(errMsg, logFileWriter);
					  verificationErrors.append(errMsg);
				  }
			  }

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "50", "(B.2) Manually Enter Additional Salary Considerations for 'Governance'.", "Create ASC entry in Known stage with positive amount for each 'category' for Governance (Chair-Cluster Dean-Program Director).");
		  }

		  public void verifyNegativeKnownGovernanceRelatedASC() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 51 (B.2) Manually Enter Additional Salary Considerations for Governance - Create ASC entry in Known stage - negative amount for Chair and Cluster Dean and Program Director";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String[] names = {"Sano, Stephen", "Edelstein, Dan", "Goldstein, Judith"};
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool"};
			  
			  boolean[] matches = verifyValuesForMultipleFacultyNames(names, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName);
			  
			  for (int i = 0; i < matches.length; i++){
				  if (! matches[i]){
					  String errMsg = "Test FAILED due to mismatch on faculty " + names[i];
					  writeToLogFile(errMsg, logFileWriter);
					  verificationErrors.append(errMsg);
				  }
			  }

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "51", "(B.2) Manually Enter Additional Salary Considerations for 'Governance'.", "Create ASC entry in Known stage with negative amount for each 'category' for Governance (Chair-Cluster Dean-Program Director).");
		  }

		  public void verifyPositivePendingGovernanceRelatedASC() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 52 (B.2) Manually Enter Additional Salary Considerations for Governance - Create ASC entry in Pending stage with positive amount for Chair and Cluster Dean and Program Director";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String[] names = {"Parker, Grant", "Egan, Ronald", "Bernheim, B."};
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool"};
			  
			  boolean[] matches = verifyValuesForMultipleFacultyNames(names, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName);
			  
			  for (int i = 0; i < matches.length; i++){
				  if (! matches[i]){
					  String errMsg = "Test FAILED due to mismatch on faculty " + names[i];
					  writeToLogFile(errMsg, logFileWriter);
					  verificationErrors.append(errMsg);
				  }
			  }

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "52", "(B.2) Manually Enter Additional Salary Considerations for 'Governance'.", "Create ASC entry in Pending stage with positive amount for each 'category' for Governance (Chair-Cluster Dean-Program Director).");
		  }

		  public void verifySalaryLogRetroactiveChangeImpactOnControlSheetDetailReport() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 57 (C) Manually Enter Salary Log entries.  Click 'Salary Log' in faculty module. Enter adjustment type 'Retroactive Change' with effective date 9-1. Enter all required data fields and click 'Save'.";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String name = "Cain, Bruce";
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "Effective Date", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool"};
			  
			  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName)){
				  writeToLogFile("Test passed", logFileWriter);
			  } else writeToLogFile("Test FAILED", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "57", "(C) Manually Enter Salary Log entries.","Click 'Salary Log' in faculty module. Enter adjustment type 'Retroactive Change' with effective date 9-1. Enter all required data fields and click 'Save'.");
		  }

		  
		  public void verifySalaryBaseForDeptChairs() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 60 (D.1) Verify Salary Increases and Percentages box.  Verify Salary Base for Chairs and Directors faculty.";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);

			  String searchColumn = "Description";
			  String searchString = "Chair -";
			  String[] columnNames = {"Name", "Department"};
			  TreeSet<String> names = fileUnderTest.getDiscreteOrderedListOfValues(fileUnderTestWorksheetName, searchString, searchColumn, columnNames, logFileWriter);
			  searchString = "Program Director -";
			  TreeSet<String> progDirectorNames = fileUnderTest.getDiscreteOrderedListOfValues(fileUnderTestWorksheetName, searchString, searchColumn, columnNames, logFileWriter);
			  names.addAll(progDirectorNames);

			  //using the locator, find the expected salary base for all Senior Associate Deans and convert it to a Long
			  String inputWorksheetName = "Control Sheet";
			  String name = "DeptChairsSalaryBase";
			  String locator = fileIn.getCellData(inputWorksheetName, 1, fileIn.getCellRowNum(inputWorksheetName, 0, name));
			  //first, retrieve the text of what's in the browser page
			  String expected = driver.findElement(By.xpath(locator)).getText();
			  //trim off the dollar sign
			  expected = expected.substring(1, expected.length());
			  
			  //now convert it to a long so it can be compared, number to number
			  long expectedBaseValue = ((Long)NumberFormat.getNumberInstance(java.util.Locale.US).parse(expected)).longValue();

			  /*
			   * iterate through the TreeSet of names, finding the "Current 100% Salary" value
			   * for each name.  For each salary value, add it to the "actualSalaryBase" sum.
			   */
			  Iterator<String> iterator = names.iterator();

			  long actualBaseValue = 0;
			  while (iterator.hasNext()){
				  String discreteName = iterator.next();
				  if(columnNames.length > 1){
					  discreteName = discreteName.substring(0, discreteName.indexOf('-'));
				  }
				  logFileWriter.write("name:" + discreteName + ";");
			      String individualSalaryString = fileUnderTest.getValueForColumnNameAndValue(fileUnderTestWorksheetName, discreteName, "Name", "Current FTE Adj Salary", logFileWriter, true);
			      logFileWriter.write(" salary:" + individualSalaryString);
			      actualBaseValue += ((Long)NumberFormat.getNumberInstance(java.util.Locale.US).parse(individualSalaryString)).longValue();
			  }

			  //determine whether the test passed based on whether the expected value equals the actual sum, given an accepted variance
			  int variance = 1;
			  if ((actualBaseValue < expectedBaseValue-variance)||(actualBaseValue > expectedBaseValue+variance)){
				  verificationErrors.append("Expected value " + expectedBaseValue + " does not match actual base value " + actualBaseValue);
				  writeToLogFile("Test FAILED", logFileWriter);
			  } else writeToLogFile("Test passed", logFileWriter);

			  
			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "60", "(D.1) Verify Salary Increases and Percentages box.","Verify Salary Base for Chairs and Directors faculty.");

		  }

		  public void verifySalaryBaseForSrAssociateDeans() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 61 (D.1) Verify Salary Increases and Percentages box.  Verify Salary Base for Sr. Associate Deans .";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  //get a discrete, ordered list of the names of the Senior Associate Deans
			  String searchColumn = "Description";
			  String searchString = "Senior Associate Dean, H&S Dean's Office;";
			  String[] columnNames = {"Name"};
			  TreeSet<String> names = fileUnderTest.getDiscreteOrderedListOfValues(fileUnderTestWorksheetName, searchString, searchColumn, columnNames, logFileWriter);
			  
			  //using the locator, find the expected salary base for all Senior Associate Deans and convert it to a Long
			  String inputWorksheetName = "Control Sheet";
			  String name = "SrAssociateDeansSalaryBase";
			  String locator = fileIn.getCellData(inputWorksheetName, 1, fileIn.getCellRowNum(inputWorksheetName, 0, name));
			  //first, retrieve the text of what's in the browser page
			  String expected = driver.findElement(By.xpath(locator)).getText();
			  //trim off the dollar sign
			  expected = expected.substring(1, expected.length());
			  
			  //now convert it to a long so it can be compared, number to number
			  long expectedBaseValue = ((Long)NumberFormat.getNumberInstance(java.util.Locale.US).parse(expected)).longValue();
			  /*
			   * iterate through the TreeSet of names, finding the "Current 100% Salary" value
			   * for each name.  For each salary value, add it to the "actualSalaryBase" sum.
			   */
			  Iterator<String> iterator = names.iterator();

			  long actualBaseValue = 0;
			  while (iterator.hasNext()){
				  String discreteName = iterator.next();
				  if(columnNames.length > 1){
					  discreteName = discreteName.substring(0, discreteName.indexOf('-'));
				  }
				  logFileWriter.write("name:" + discreteName + ";");
			      String individualSalaryString = fileUnderTest.getValueForColumnNameAndValue(fileUnderTestWorksheetName, discreteName, "Name", "Current FTE Adj Salary", logFileWriter);
			      logFileWriter.write(" salary:" + individualSalaryString);
			      actualBaseValue += ((Long)NumberFormat.getNumberInstance(java.util.Locale.US).parse(individualSalaryString)).longValue();
			  }

			  //determine whether the test passed based on whether the expected value equals the actual sum, given an accepted variance
			  int variance = 1;
			  if ((actualBaseValue < expectedBaseValue-variance)||(actualBaseValue > expectedBaseValue+variance)){
				  verificationErrors.append("Expected value " + expectedBaseValue + " does not match actual base value " + actualBaseValue);
				  writeToLogFile("Test FAILED", logFileWriter);
			  } else writeToLogFile("Test passed", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "61", "(D.1) Verify Salary Increases and Percentages box.","Verify Salary Base for Sr. Associate Deans .");
		  }
		  
		  
		  public void verifyKnownRegularMeritPoolSalaryPromises() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 75 (D.3) Verify Known Salary Promises with Successful Retention in Control Sheet Detail Report";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String[] names = {"Bashir, Shahzad", "Johnson, Adam", "Key, Alexander"};
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool", "Description"};
			  
			  boolean[] matches = verifyValuesForMultipleFacultyNames(names, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName);
			  
			  for (int i = 0; i < matches.length; i++){
				  if (! matches[i]){
					  String errMsg = "Test FAILED due to mismatch on faculty " + names[i];
					  writeToLogFile(errMsg, logFileWriter);
					  verificationErrors.append(errMsg);
				  }
			  }

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "75", "(D.3) Verify 'Salary Promises' in Control Sheet Detail Report: RETENTION PROMISES - KNOWN", "Verify faculty with salary promises for succesfull Retention appear on Control Sheet Detail Report.");
		  }

		  public void verifyPendingRegularMeritPoolSalaryPromises() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 76 (D.3) Verify Pending Salary Promises with Non-Successful Retention in Control Sheet Detail Report";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String[] names = {"Algee Hewitt, Mark", "Barth, George", "Cui, Bianxiao", "Fraser, Hunter", "Gozani, Or", "Mirzakhani, Maryam", "Schultz, Anna", "Skotheim, Jan", "Susskind, Leonard"};
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool", "Description"};
			  
			  boolean[] matches = verifyValuesForMultipleFacultyNames(names, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName);
			  
			  for (int i = 0; i < matches.length; i++){
				  if (! matches[i]){
					  String errMsg = "Test FAILED due to mismatch on faculty " + names[i];
					  writeToLogFile(errMsg, logFileWriter);
					  verificationErrors.append(errMsg);
				  }
			  }

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "76", "(D.3) Verify 'Salary Promises' in Control Sheet Detail Report: RETENTION PROMISES - PENDING", "Verify faculty with salary promises for 'non-success' Retention appear on Control Sheet Detail Report.");
		  }

		  public void verifyKnownMarketPoolSalaryPromises() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 77 (D.3) Verify Known Salary Promises with Successful Retention for Market Eligible Faculty in Control Sheet Detail Report";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String name = "Chatterjee, Sourav";
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "Effective Date", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool", "Description"};
			  
			  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName)){
				  writeToLogFile("Test passed", logFileWriter);
			  } else writeToLogFile("Test FAILED", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "77", "(D.3) Verify 'Salary Promises' in Control Sheet Detail Report: MKT RETENTION","Verify salary promises for faculty that are market eligble with a successful retention  appear on the Control Sheet Detail Report.");
		  }

		  public void verifyPendingMarketPoolSalaryPromises() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 78 (D.3) Verify Pending Salary Promises with Non-Successful Retention for faculty eligible for the Market Pool in Control Sheet Detail Report";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String[] names = {"Brendle, Simon", "Galatius, Soren"};
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool", "Description"};
			  
			  boolean[] matches = verifyValuesForMultipleFacultyNames(names, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName);
			  
			  for (int i = 0; i < matches.length; i++){
				  if (! matches[i]){
					  String errMsg = "Test FAILED due to mismatch on faculty " + names[i];
					  writeToLogFile(errMsg, logFileWriter);
					  verificationErrors.append(errMsg);
				  }
			  }

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "78", "(D.3) Verify 'Salary Promises' in Control Sheet Detail Report: RECRUITMENT PROMISES", "Verify salary promises for faculty that are market eligble with a 'non-success' retention appear on the Control Sheet Detail Report.");
		  }
		  

		  public void verifyKnownRegularMeritPoolRecruitmentPromises() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 79 (D.3) Verify Known Salary Promises with Successful Recruitment for Faculty in Control Sheet Detail Report";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String name = "Chatterjee, Sourav";
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "Effective Date", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool", "Description"};
			  
			  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName)){
				  writeToLogFile("Test passed", logFileWriter);
			  } else writeToLogFile("Test FAILED", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "79", "(D.3) Verify 'Salary Promises' in Control Sheet Detail Report: RECRUITMENT PROMISES - KNOWN","Verify faculty with salary promises for accepted Recruitment appear on Control Sheet Detail Report.");
		  }


		  public void verifyKnownRegularMeritPoolGovernanceWithSalaryPromises() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 81 (D.3) Verify Known Governance With Salary Promises in Control Sheet Detail Report";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String[] names = {"Cohen, Ralph", "Edelstein, Dan", "Findlen, Paula", "Harrison, Paul", "Michelson, Peter"};
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool", "Description"};
			  
			  boolean[] matches = verifyValuesForMultipleFacultyNames(names, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName);
			  
			  for (int i = 0; i < matches.length; i++){
				  if (! matches[i]){
					  String errMsg = "Test FAILED due to mismatch on faculty " + names[i];
					  writeToLogFile(errMsg, logFileWriter);
					  verificationErrors.append(errMsg);
				  }
			  }

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "81", "(D.3) Verify 'Salary Promises' in Control Sheet Detail Report: GOVERNANCE WITH RETENTION/PROMISE - KNOWN", "Verify faculty with governance and salary promise for success Retention appear on Control Sheet Detail Report.");
		  }

		  public void verifyPendingRegularMeritPoolGovernanceWithSalaryPromises() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 82 (D.3) Verify Pending Governance With Salary Promises in Control Sheet Detail Report";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String name = "Palumbi, Stephen";
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "Effective Date", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool", "Description"};
			  
			  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName)){
				  writeToLogFile("Test passed", logFileWriter);
			  } else writeToLogFile("Test FAILED", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "82", "(D.3) Verify 'Salary Promises' in Control Sheet Detail Report: GOVERNANCE WITH RETENTION/PROMISE - PENDING", "Verify faculty with governance and salary promise for pending Retention appear on Control Sheet Detail Report.");
		  }

		  public void verifyKnownRegularMeritPoolSeniorAssociateDeanGovernanceWithSalaryPromises() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 83 (D.3) Verify 'Salary Promises' in Control Sheet Detail Report - OTHER - Known";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String name = "Cohen, Ralph";
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "Effective Date", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool", "Description"};
			  
			  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName)){
				  writeToLogFile("Test passed", logFileWriter);
			  } else writeToLogFile("Test FAILED", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "83", "(D.3) Verify 'Salary Promises' in Control Sheet Detail Report: OTHER - Known", "Verify faculty with known 'other' salary  appear on Control Sheet Detail Report.");
		  }

		  public void verifyPendingRegularMeritPoolSeniorAssociateDeanGovernanceWithSalaryPromises() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 84 (D.3) Verify 'Salary Promises' in Control Sheet Detail Report - OTHER - Pending";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String name = "Satz, Debra";
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "Effective Date", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool", "Description"};
			  
			  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName)){
				  writeToLogFile("Test passed", logFileWriter);
			  } else writeToLogFile("Test FAILED", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "84", "(D.3) Verify 'Salary Promises' in Control Sheet Detail Report: OTHER - Pending", "Verify faculty with pending 'other' salary  appear on Control Sheet Detail Report.");
		  }

		  public void verifyKnownPreviousYearCommitmentsRetroactiveChange() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 85 (D.4) Verify 'Previous Year Commitments - Retroactive Change' in Control Sheet Detail Report.";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String[] names = {"Abel, Tom", "Allen, Steven", "Bergmann, Dominique", "Cain, Bruce", "Johnson, Adam", "Shatz, Carla"};
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool", "Description"};
			  
			  boolean[] matches = verifyValuesForMultipleFacultyNames(names, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName);
			  
			  for (int i = 0; i < matches.length; i++){
				  if (! matches[i]){
					  String errMsg = "Test FAILED due to mismatch on faculty " + names[i];
					  writeToLogFile(errMsg, logFileWriter);
					  verificationErrors.append(errMsg);
				  }
			  }

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "85", "(D.4) Verify 'Previous Year Commitments' in Control Sheet Detail Report.", "Verify faculty with a salary log entry of 'retroactive change'  appear in the Control Sheet Detail Report");
		  }


		  public void verifyKnownPreviousYearCommitmentsPostBinder() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 86 (D.4) Verify 'Previous Year Commitments - Post Binder Change' in Control Sheet Detail Report.";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String[] names = {"Johnson, Adam", "Frank, Zephyr", "Khosla, Chaitan"};
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool", "Description"};
			  
			  boolean[] matches = verifyValuesForMultipleFacultyNames(names, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName);
			  
			  for (int i = 0; i < matches.length; i++){
				  if (! matches[i]){
					  String errMsg = "Test FAILED due to mismatch on faculty " + names[i];
					  writeToLogFile(errMsg, logFileWriter);
					  verificationErrors.append(errMsg);
				  }
			  }

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "86", "(D.4) Verify 'Previous Year Commitments' in Control Sheet Detail Report.", "Verify faculty with a salary log entry of 'Post Binder'  appear in the Control Sheet Detail Report");
		  }

		  public void verifyKnownPreviousYearCommitmentsPostLetter() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 87 (D.4) Verify 'Previous Year Commitments - Post Letter Change' in Control Sheet Detail Report.";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String[] names = {"Mudgett, Mary", "Willer, Robb"};
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool", "Description"};
			  
			  boolean[] matches = verifyValuesForMultipleFacultyNames(names, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName);
			  
			  for (int i = 0; i < matches.length; i++){
				  if (! matches[i]){
					  String errMsg = "Test FAILED due to mismatch on faculty " + names[i];
					  writeToLogFile(errMsg, logFileWriter);
					  verificationErrors.append(errMsg);
				  }
			  }

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "87", "(D.4) Verify 'Previous Year Commitments' in Control Sheet Detail Report.", "Verify faculty with a salary log entry of 'Post Letter  appear in the Control Sheet Detail Report");
		  }

		  public void verifyKnownPreviousYearCommitmentsMidYear() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 88 (D.4) Verify 'Previous Year Commitments - Mid-Year - in Control Sheet Detail Report.";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String[] names = {"Applebaum, Mark", "Bashir, Shahzad", "Hartnoll, Sean", "Hill, Leslie", "Menon, Jisha", "Paris, Helen", "Red-Horse, Mary"};
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool", "Description"};
			  
			  boolean[] matches = verifyValuesForMultipleFacultyNames(names, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName);
			  
			  for (int i = 0; i < matches.length; i++){
				  if (! matches[i]){
					  String errMsg = "Test FAILED due to mismatch on faculty " + names[i];
					  writeToLogFile(errMsg, logFileWriter);
					  verificationErrors.append(errMsg);
				  }
			  }

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "88", "(D.4) Verify 'Previous Year Commitments' in Control Sheet Detail Report.", "Verify faculty with a salary log entry of 'mid-year' appear in the Control Sheet Detail Report");
		  }
		  
		  public void verifyKnownFacultyAppointmentReappointment() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 89 (D.5) Verify Faculty Appointment Related -Reappointment - in Control Sheet Detail Report";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String[] names = {"Hill, Leslie", "Paris, Helen", "Red-Horse, Mary", "Tambar, Kabir", "Thiranagama, Sharika"};
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "Fiscal Year", "Effective Date", "100% FTE Salary", "Language of Commitments", 
					  "% Increase Pct", "100% FTE $ Increase Amt", "100% FTE Total Amount", "% Increase Above Pool", "100% FTE $ Dept Contribution", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool", "Description"};
			  
			  boolean[] matches = verifyValuesForMultipleFacultyNames(names, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName);
			  
			  for (int i = 0; i < matches.length; i++){
				  if (! matches[i]){
					  String errMsg = "Test FAILED due to mismatch on faculty " + names[i];
					  writeToLogFile(errMsg, logFileWriter);
					  verificationErrors.append(errMsg);
				  }
			  }

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "89", "(D.5) Verify 'Faculty Appointment Related' in Control Sheet Detail Report", "Verify category of 'Faculty Appointment Related' - Reappointment");
		  }

		  public void verifyKnownFacultyAppointmentPromoNonTenure() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 90 (D.5) Verify Faculty Appointment Related - promotion to Non-Tenure - in Control Sheet Detail Report";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String name = "Hoxby, Blair";
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "Fiscal Year", "Effective Date", "100% FTE Salary", "Language of Commitments", 
					  "% Increase Pct", "100% FTE $ Increase Amt", "100% FTE Total Amount", "% Increase Above Pool", "100% FTE $ Dept Contribution", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool", "Description"};
			  
			  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName)){
				  writeToLogFile("Test passed", logFileWriter);
			  } else writeToLogFile("Test FAILED", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "90", "(D.5) Verify 'Faculty Appointment Related' in Control Sheet Detail Report", "Verify category of 'Faculty Appointment Related' - Non-Tenure");
		  }

		  public void verifyKnownFacultyAppointmentPromoFull() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 91 (D.5) Verify Faculty Appointment Related - promotion to Full - in Control Sheet Detail Report";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String name = "Vasy, Andras";
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "Fiscal Year", "Effective Date", "100% FTE Salary", "Language of Commitments", "% Increase Pct", 
					  "100% FTE $ Increase Amt", "100% FTE Total Amount", "% Increase Above Pool", "100% FTE $ Dept Contribution", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool", "Description"};
			  
			  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName)){
				  writeToLogFile("Test passed", logFileWriter);
			  } else writeToLogFile("Test FAILED", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "91", "(D.5) Verify 'Faculty Appointment Related' in Control Sheet Detail Report", "Verify category of 'Faculty Appointment Related' - Promo To Full");
		  }

		  public void verifyKnownFacultyAppointmentPromoContTerm() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 92 (D.5) Verify Faculty Appointment Related - promotion to Continuing Term - in Control Sheet Detail Report";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String name = "Wieman, Carl";
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "Fiscal Year", "Effective Date", "100% FTE Salary", "Language of Commitments", "% Increase Pct", 
					  "100% FTE $ Increase Amt", "100% FTE Total Amount", "% Increase Above Pool", "100% FTE $ Dept Contribution", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool", "Description"};
			  
			  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName)){
				  writeToLogFile("Test passed", logFileWriter);
			  } else writeToLogFile("Test FAILED", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "92", "(D.5) Verify 'Faculty Appointment Related' in Control Sheet Detail Report", "Verify category of 'Faculty Appointment Related' - Promo To Continuing Term");
		  }

		  public void verifyKnownFacultyAppointmentPromoRemovalofSubjToPhD() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 93 (D.5) Verify Faculty Appointment Related - Removal of Subject to PhD - in Control Sheet Detail Report";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String name = "Donaldson, David";
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "Fiscal Year", "Effective Date", "100% FTE Salary", "Language of Commitments", "% Increase Pct", 
					  "100% FTE $ Increase Amt", "100% FTE Total Amount", "% Increase Above Pool", "100% FTE $ Dept Contribution", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool", "Description"};
			  
			  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName)){
				  writeToLogFile("Test passed", logFileWriter);
			  } else writeToLogFile("Test FAILED", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "93", "(D.5) Verify 'Faculty Appointment Related' in Control Sheet Detail Report", "Verify category of 'Faculty Appointment Related' - Removal of Subject to PhD");
		  }

		  public void verifyKnownFacultyAppointmentTerminalYear() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 94 (D.5) Verify Faculty Appointment Related - Terminal Year - in Control Sheet Detail Report";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String name = "Parigi, Paolo";
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "Fiscal Year", "Effective Date", "100% FTE Salary", "Language of Commitments", "% Increase Pct", 
					  "100% FTE $ Increase Amt", "100% FTE Total Amount", "% Increase Above Pool", "100% FTE $ Dept Contribution", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool", "Description"};
			  
			  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName)){
				  writeToLogFile("Test passed", logFileWriter);
			  } else writeToLogFile("Test FAILED", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "94", "(D.5) Verify 'Faculty Appointment Related' in Control Sheet Detail Report", "Verify category of 'Faculty Appointment Related' - Terminal Year");
		  }

		  public void verifyGovernanceRelatedChairsDirectors() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 95 (D.6) Verify Governance Related - Chairs and Directors - in Control Sheet Detail Report";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String[] names = {"Bernheim, B.", "Chu, Steve", "Mabuchi, Hideo", "Tomz, Michael", "Winterer, Caroline"};
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "Fiscal Year", "Effective Date", "100% FTE Salary", "Language of Commitments", 
					  "% Increase Pct", "100% FTE $ Increase Amt", "100% FTE Total Amount", "% Increase Above Pool", "100% FTE $ Dept Contribution", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool", "Description"};
			  
			  boolean[] matches = verifyValuesForMultipleFacultyNames(names, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName);
			  
			  for (int i = 0; i < matches.length; i++){
				  if (! matches[i]){
					  String errMsg = "Test FAILED due to mismatch on faculty " + names[i];
					  writeToLogFile(errMsg, logFileWriter);
					  verificationErrors.append(errMsg);
				  }
			  }
			  if (verificationErrors.length() == 0)
				  writeToLogFile("Test Passed", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "95", "(D.6) Verify 'Governance Related' in Control Sheet Detail Report.", "Verify category of 'Governance Related'");
		  }


		  public void verifyGovernanceRelatedSeniorAssociateDeans() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 96 (D.6) Verify Governance Related - Senior Associate Deans - in Control Sheet Detail Report";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String name = "Markman, Ellen";
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "Fiscal Year", "Effective Date", "100% FTE Salary", "Language of Commitments", "% Increase Pct", 
					  "100% FTE $ Increase Amt", "100% FTE Total Amount", "% Increase Above Pool", "100% FTE $ Dept Contribution", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool", "Description"};
			  
			  if (verifyValuesForFacultyName(name, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName)){
				  writeToLogFile("Test passed", logFileWriter);
			  } else writeToLogFile("Test FAILED", logFileWriter);


			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "96", "(D.6) Verify 'Governance Related' in Control Sheet Detail Report.", "Verify category of 'Governance Related'");
		  }
		  
		  public void verifyTransferInformation() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 97 (D.7) Verify Transfers in Control Sheet Detail Report";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  //specify the HashMap which contains the search criteria
			  HashMap<String, String> searchCriteria = new HashMap<String, String>();
			  searchCriteria.put("Category", "Transfers");
			  searchCriteria.put("Subcategory", "Transfers");
			  
			  //specify the array of Strings which contains the column names we want values for
			  
			  String[] columnNames = {"Fiscal Year", "Effective Date", "100% FTE Salary", "Language of Commitments", "% Increase Pct", "100% FTE $ Increase Amt", "100% FTE Total Amount", 
					  "% Increase Above Pool", "100% FTE $ Dept Contribution", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool", "Destination Pool", "Description"};
			  LinkedList<HashMap<String, String>> listOfExpectedValues = fileIn.getMultipleValuesInRows(individualInputWorksheetName, searchCriteria , columnNames , logFileWriter);
			  LinkedList<HashMap<String, String>> listOfActualValues = fileUnderTest.getMultipleValuesInRows(fileUnderTestWorksheetName, searchCriteria , columnNames , logFileWriter);
			  
			  if (verifyFacultyDataSetsMultipleValues(listOfExpectedValues, listOfActualValues, columnNames, logFileWriter))
				  writeToLogFile("Test passed", logFileWriter);
			  else writeToLogFile("Test FAILED", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "97", "(D.7) Verify Transfers in Control Sheet Detail Report", "Verify category of 'Transfers'");

		  }// end of method
		  
		  public void verifyReserveInformation() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 98 (D.8) Verify Reserves in Control Sheet Detail Report";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  //specify the HashMap which contains the search criteria
			  HashMap<String, String> searchCriteria = new HashMap<String, String>();
			  searchCriteria.put("Category", "Reserves");
			  searchCriteria.put("Subcategory", "Other Reserves");
			  
			  //specify the array of Strings which contains the column names we want values for
			  
			  String[] columnNames = {"Fiscal Year", "Effective Date", "100% FTE Salary", "Language of Commitments", "% Increase Pct", "100% FTE $ Increase Amt", "100% FTE Total Amount", 
					  "% Increase Above Pool", "100% FTE $ Dept Contribution", "FTE Adjusted DO Share Amount", "FTE Adj Total Commitment", "Source Pool", "Destination Pool", "Description"};
			  LinkedList<HashMap<String, String>> listOfExpectedValues = fileIn.getMultipleValuesInRows(individualInputWorksheetName, searchCriteria , columnNames , logFileWriter);
			  LinkedList<HashMap<String, String>> listOfActualValues = fileUnderTest.getMultipleValuesInRows(fileUnderTestWorksheetName, searchCriteria , columnNames , logFileWriter);
			  
			  if (verifyFacultyDataSetsMultipleValues(listOfExpectedValues, listOfActualValues, columnNames, logFileWriter))
				  writeToLogFile("Test passed", logFileWriter);
			  else writeToLogFile("Test FAILED", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "98", "(D.8) Verify 'Reserves' in Control Sheet Detail Report", "Verify category of 'Reserves'");

		  }// end of method

		  public void verifyNotesForFaculty() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 99 (D.8) Verify Description-Notes in Control Sheet Detail Report";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String[] names = {"Barletta, Vincent", "Bauer, Andrew", "Bergmann, Dominique", ""};
			  String[] columnNames = {"Category", "Subcategory", "Department", "Stage", "Dept FTE", "Fiscal Year", "Effective Date", "100% FTE Salary", "Language of Commitments", 
					  "% Increase Pct", "100% FTE $ Increase Amt", "100% FTE Total Amount", "% Increase Above Pool", "100% FTE $ Dept Contribution", "FTE Adjusted DO Share Amount", 
					  "FTE Adj Total Commitment", "Source Pool", "Description", "Notes"};
			  
			  boolean[] matches = verifyValuesForMultipleFacultyNames(names, columnNames, logFileWriter, fileIn, individualInputWorksheetName, fileUnderTest, fileUnderTestWorksheetName);
			  
			  for (int i = 0; i < matches.length; i++){
				  if (! matches[i]){
					  String errMsg = "Test FAILED due to mismatch on faculty " + names[i];
					  writeToLogFile(errMsg, logFileWriter);
					  verificationErrors.append(errMsg);
				  }
			  }
			  if (verificationErrors.length() == 0)
				  writeToLogFile("Test Passed", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "99", "(D.8) Verify Description/Notes in Control Sheet Detail Report", "Verify the Description field and Notes field contain the text that they should");
		  }

		  public void verifySumKnownRetentionsInControlSheetGrid() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 100 (D.3) Verify Salary Promises in Control Sheet Grid - RETENTION PROMISES - KNOWN";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  HashMap<String, String> criteria = new HashMap<String, String>();
			  criteria.put("Subcategory", "Retention Promises");
			  criteria.put("Stage", "Known");
			  String[] columnNames = {"FTE Adjusted DO Share Amount"};
			  DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			  Date minDate = formatter.parse("09/02/2015");
			  Date maxDate = formatter.parse("09/01/2016");
			  BigDecimal[] summary = fileUnderTest.getColumnSummaryValues(fileUnderTestWorksheetName, criteria, true, false, false, columnNames, minDate, maxDate, logFileWriter);
			  
			  writeToLogFile("found value: " + summary[0].intValue() + " as faculty count", logFileWriter);
			  writeToLogFile("found value: " + summary[1].intValue() + " as '" + columnNames[0] +"' summary", logFileWriter);

			  //compare the faculty count found in the report with the count displayed on the grid
			  int increment = 84;
			  verificationErrors = verificationErrors.append(compareSummaryInformationToControlSheet(increment, summary, columnNames, logFileWriter));
			  if (verificationErrors.length() == 0)
				  writeToLogFile("Test Passed", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "100", "(D.3) Verify 'Salary Promises' in Control Sheet Grid: RETENTION PROMISES - KNOWN", "Verify faculty with salary promises for succesfull Retention appear on Control Sheet Grid.");
		  }

		  public void verifySumPendingRetentionsInControlSheetGrid() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 101 (D.3) Verify Salary Promises in Control Sheet Grid - RETENTION PROMISES - PENDING";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  
			  HashMap<String, String> criteria = new HashMap<String, String>();
			  criteria.put("Subcategory", "Retention Promises");
			  criteria.put("Stage", "Pending");
			  String[] columnNames = {"FTE Adjusted DO Share Amount"};
			  DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			  Date minDate = formatter.parse("09/02/2015");
			  Date maxDate = formatter.parse("09/01/2016");
			  BigDecimal[] summary = fileUnderTest.getColumnSummaryValues(fileUnderTestWorksheetName, criteria, false, true, false, columnNames, minDate, maxDate, logFileWriter);
			  
			  writeToLogFile("found value: " + summary[0].intValue() + " as faculty count", logFileWriter);
			  writeToLogFile("found value: " + summary[1].intValue() + " as '" + columnNames[0] +"' summary", logFileWriter);

			  //compare the faculty count found in the report with the count displayed on the grid
			  int increment = 88;
			  verificationErrors = verificationErrors.append(compareSummaryInformationToControlSheet(increment, summary, columnNames, logFileWriter));
			  
			  if (verificationErrors.length() == 0)
				  writeToLogFile("Test Passed", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "101", "(D.3) Verify 'Salary Promises' in Control Sheet Grid: RETENTION PROMISES - PENDING", "Verify faculty with salary promises for succesfull Retention appear on Control Sheet Grid.");
		  }

		  public void verifySumKnownMarketRetentionsInControlSheetGrid() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 102 (D.3) Verify Salary Promises in Control Sheet Grid - MARKET RETENTION PROMISES - KNOWN";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  HashMap<String, String> criteria = new HashMap<String, String>();
			  criteria.put("Subcategory", "Mkt Retention");
			  criteria.put("Stage", "Known");
			  String[] columnNames = {"FTE Adjusted DO Share Amount"};
			  DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			  Date minDate = formatter.parse("09/02/2015");
			  Date maxDate = formatter.parse("09/01/2016");
			  BigDecimal[] summary = fileUnderTest.getColumnSummaryValues(fileUnderTestWorksheetName, criteria, true, false, false, columnNames, minDate, maxDate, logFileWriter);
			  
			  writeToLogFile("found value: " + summary[0].intValue() + " as faculty count", logFileWriter);
			  writeToLogFile("found value: " + summary[1].intValue() + " as '" + columnNames[0] +"' summary", logFileWriter);
			  
			  //compare the faculty count found in the report with the count displayed on the grid
			  int increment = 100;
			  verificationErrors = verificationErrors.append(compareSummaryInformationToControlSheet(increment, summary, columnNames, logFileWriter));
			  
			  if (verificationErrors.length() == 0)
				  writeToLogFile("Test Passed", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "102", "(D.3) Verify 'Salary Promises' in Control Sheet Grid: MARKET RETENTION PROMISES - KNOWN", "Verify faculty with salary promises for succesfull Retention appear on Control Sheet Grid.");
		  }

		  public void verifySumPendingMarketRetentionsInControlSheetGrid() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 103 (D.3) Verify Salary Promises in Control Sheet Grid - MARKET RETENTION PROMISES - PENDING";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  HashMap<String, String> criteria = new HashMap<String, String>();
			  criteria.put("Subcategory", "Mkt Retention");
			  criteria.put("Stage", "Pending");
			  String[] columnNames = {"FTE Adjusted DO Share Amount"};
			  DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			  Date minDate = formatter.parse("09/02/2015");
			  Date maxDate = formatter.parse("09/01/2016");
			  BigDecimal[] summary = fileUnderTest.getColumnSummaryValues(fileUnderTestWorksheetName, criteria, true, true, true, columnNames, minDate, maxDate, logFileWriter);
			  
			  writeToLogFile("found value: " + summary[0].intValue() + " as faculty count", logFileWriter);
			  writeToLogFile("found value: " + summary[1].intValue() + " as '" + columnNames[0] +"' summary", logFileWriter);
			  
			  //compare the faculty count found in the report with the count displayed on the grid
			  int increment = 104;
			  verificationErrors = verificationErrors.append(compareSummaryInformationToControlSheet(increment, summary, columnNames, logFileWriter));
			  
			  if (verificationErrors.length() == 0)
				  writeToLogFile("Test Passed", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "103", "(D.3) Verify 'Salary Promises' in Control Sheet Grid: MARKET RETENTION PROMISES - PENDING", "Verify faculty with salary promises for non-successful Retentions appear on Control Sheet Grid.");
		  }
		  
		  public void verifySumKnownRecruitmentPromisesInControlSheetGrid() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 104 (D.3) Verify Salary Promises in Control Sheet Grid - RECRUITMENT PROMISES - KNOWN";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  HashMap<String, String> criteria = new HashMap<String, String>();
			  criteria.put("Subcategory", "Recruitment Promises");
			  criteria.put("Stage", "Known");
			  String[] columnNames = {"FTE Adjusted DO Share Amount"};
			  DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			  Date minDate = formatter.parse("09/02/2015");
			  Date maxDate = formatter.parse("09/01/2016");
			  BigDecimal[] summary = fileUnderTest.getColumnSummaryValues(fileUnderTestWorksheetName, criteria, false, false, false, columnNames, minDate, maxDate, logFileWriter);
			  
			  writeToLogFile("found value: " + summary[0].intValue() + " as faculty count", logFileWriter);
			  writeToLogFile("found value: " + summary[1].intValue() + " as '" + columnNames[0] +"' summary", logFileWriter);
			  
			  //compare the faculty count found in the report with the count displayed on the grid
			  int increment = 116;
			  verificationErrors = verificationErrors.append(compareSummaryInformationToControlSheet(increment, summary, columnNames, logFileWriter));
			  
			  if (verificationErrors.length() == 0)
				  writeToLogFile("Test Passed", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "104", "(D.3) Verify 'Salary Promises' in Control Sheet Grid: RECRUITMENT PROMISES - KNOWN", "Verify faculty with salary promises for accepted Recruitment appear on Control Sheet Grid.");
		  }

		  public void verifySumKnownGovernanceWithSalaryPromisesInControlSheetGrid() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 106 (D.3) Verify Salary Promises in Control Sheet Grid - GOVERNANCE WITH RETENTION OR PROMISE - KNOWN";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  HashMap<String, String> criteria = new HashMap<String, String>();
			  criteria.put("Subcategory", "Governance with Salary Promise");
			  criteria.put("Stage", "Known");
			  criteria.put("Source Pool", "Regular Merit Pool");
			  String[] columnNames = {"FTE Adjusted DO Share Amount"};
			  DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			  Date minDate = formatter.parse("09/02/2015");
			  Date maxDate = formatter.parse("09/01/2016");
			  BigDecimal[] summary = fileUnderTest.getColumnSummaryValues(fileUnderTestWorksheetName, criteria, false, false, false, columnNames, minDate, maxDate, logFileWriter);
			  
			  writeToLogFile("found value: " + summary[0].intValue() + " as faculty count", logFileWriter);
			  writeToLogFile("found value: " + summary[1].intValue() + " as '" + columnNames[0] +"' summary", logFileWriter);
			  
			  //compare the faculty count found in the report with the count displayed on the grid
			  int increment = 356;
			  verificationErrors = verificationErrors.append(compareSummaryInformationToControlSheet(increment, summary, columnNames, logFileWriter));
			  
			  if (verificationErrors.length() == 0)
				  writeToLogFile("Test Passed", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "106", "(D.3) Verify 'Salary Promises' in Control Sheet Grid: GOVERNANCE WITH RETENTION/PROMISE - KNOWN", "Verify faculty with governance and salary promise for success Retention appear on Control Sheet Grid.");
		  }

		  public void verifySumSalaryPromisesOtherInControlSheetGrid() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 107 (D.3) Verify 'Salary Promises' in Control Sheet Grid: OTHER - Known";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  HashMap<String, String> criteria = new HashMap<String, String>();
			  criteria.put("Category", "Salary Promises");
			  criteria.put("Subcategory", "Other");
			  criteria.put("Source Pool", "Regular Merit Pool");
			  criteria.put("Stage", "Known");
			  String[] columnNames = {"FTE Adjusted DO Share Amount"};
			  DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			  Date minDate = formatter.parse("09/02/2015");
			  Date maxDate = formatter.parse("09/01/2016");
			  BigDecimal[] summary = fileUnderTest.getColumnSummaryValues(fileUnderTestWorksheetName, criteria, false, false, false, columnNames, minDate, maxDate, logFileWriter);
			  
			  writeToLogFile("found value: " + summary[0].intValue() + " as faculty count", logFileWriter);
			  writeToLogFile("found value: " + summary[1].intValue() + " as '" + columnNames[0] +"' summary", logFileWriter);
			  
			  //compare the faculty count found in the report with the count displayed on the grid
			  int increment = 132;
			  verificationErrors = verificationErrors.append(compareSummaryInformationToControlSheet(increment, summary, columnNames, logFileWriter));
			  
			  if (verificationErrors.length() == 0)
				  writeToLogFile("Test Passed", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "107", "(D.3) Verify 'Salary Promises' in Control Sheet Grid: OTHER - Known", "Verify faculty with known 'other' salary  appear on Control Sheet Grid.");
		  }


		  public void verifySumRetroactiveChangesInControlSheetGrid() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 108 (D.4) Verify 'Previous Year Commitments' in Control Sheet Grid.";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  HashMap<String, String> criteria = new HashMap<String, String>();
			  criteria.put("Category", "Previous Year Commitments");
			  criteria.put("Subcategory", "Retroactive Change");
			  criteria.put("Source Pool", "Regular Merit Pool");
			  criteria.put("Stage", "Known");
			  String[] columnNames = {"FTE Adjusted DO Share Amount"};
			  DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			  Date minDate = formatter.parse("09/01/2015");
			  Date maxDate = formatter.parse("08/31/2016");
			  BigDecimal[] summary = fileUnderTest.getColumnSummaryValues(fileUnderTestWorksheetName, criteria, false, false, false, columnNames, minDate, maxDate, logFileWriter);
			  
			  writeToLogFile("found value: " + summary[0].intValue() + " as faculty count", logFileWriter);
			  writeToLogFile("found value: " + summary[1].intValue() + " as '" + columnNames[0] +"' summary", logFileWriter);
			  
			  //compare the faculty count found in the report with the count displayed on the grid
			  int increment = 148;
			  verificationErrors = verificationErrors.append(compareSummaryInformationToControlSheet(increment, summary, columnNames, logFileWriter));
			  
			  if (verificationErrors.length() == 0)
				  writeToLogFile("Test Passed", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "108", "(D.4) Verify 'Previous Year Commitments' in Control Sheet Grid.", "Verify faculty with a salary log entry of 'retroactive change'  appear in the Control Sheet Grid");
		  }


		  public void verifySumPostBinderChangesInControlSheetGrid() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 109 (D.4) Verify 'Previous Year Commitments' - Post Binder - in Control Sheet Grid.";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  HashMap<String, String> criteria = new HashMap<String, String>();
			  criteria.put("Category", "Previous Year Commitments");
			  criteria.put("Subcategory", "Post Binder");
			  criteria.put("Source Pool", "Regular Merit Pool");
			  criteria.put("Stage", "Known");
			  String[] columnNames = {"FTE Adjusted DO Share Amount"};
			  DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			  Date minDate = formatter.parse("09/01/2015");
			  Date maxDate = formatter.parse("08/31/2016");
			  BigDecimal[] summary = fileUnderTest.getColumnSummaryValues(fileUnderTestWorksheetName, criteria, false, false, false, columnNames, minDate, maxDate, logFileWriter);
			  
			  writeToLogFile("found value: " + summary[0].intValue() + " as faculty count", logFileWriter);
			  writeToLogFile("found value: " + summary[1].intValue() + " as '" + columnNames[0] +"' summary", logFileWriter);
			  
			  //compare the faculty count found in the report with the count displayed on the grid
			  int increment = 164;
			  verificationErrors = verificationErrors.append(compareSummaryInformationToControlSheet(increment, summary, columnNames, logFileWriter));
			  
			  if (verificationErrors.length() == 0)
				  writeToLogFile("Test Passed", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "109", "(D.4) Verify 'Previous Year Commitments' in Control Sheet Grid.", "Verify faculty with a salary log entry of 'Post Binder'  appear in the Control Sheet Grid");
		  }

		  public void verifySumPostLetterChangesInControlSheetGrid() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 110 (D.4) Verify 'Previous Year Commitments' - Post Letter - in Control Sheet Grid.";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  HashMap<String, String> criteria = new HashMap<String, String>();
			  criteria.put("Category", "Previous Year Commitments");
			  criteria.put("Subcategory", "Post Letter");
			  criteria.put("Source Pool", "Regular Merit Pool");
			  criteria.put("Stage", "Known");
			  String[] columnNames = {"FTE Adjusted DO Share Amount"};
			  DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			  Date minDate = formatter.parse("09/01/2015");
			  Date maxDate = formatter.parse("08/31/2016");
			  BigDecimal[] summary = fileUnderTest.getColumnSummaryValues(fileUnderTestWorksheetName, criteria, false, false, false, columnNames, minDate, maxDate, logFileWriter);
			  
			  writeToLogFile("found value: " + summary[0].intValue() + " as faculty count", logFileWriter);
			  writeToLogFile("found value: " + summary[1].intValue() + " as '" + columnNames[0] +"' summary", logFileWriter);
			  
			  //compare the faculty count found in the report with the count displayed on the grid
			  int increment = 180;
			  verificationErrors = verificationErrors.append(compareSummaryInformationToControlSheet(increment, summary, columnNames, logFileWriter));
			  
			  if (verificationErrors.length() == 0)
				  writeToLogFile("Test Passed", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "110", "(D.4) Verify 'Previous Year Commitments' in Control Sheet Grid.", "Verify faculty with a salary log entry of 'Post Letter'  appear in the Control Sheet Grid");
		  }

		  public void verifySumMidYearChangesInControlSheetGrid() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 111 (D.4) Verify 'Previous Year Commitments' - Mid-Year - in Control Sheet Grid.";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  HashMap<String, String> criteria = new HashMap<String, String>();
			  criteria.put("Category", "Previous Year Commitments");
			  criteria.put("Subcategory", "Mid-Year");
			  criteria.put("Source Pool", "Regular Merit Pool");
			  criteria.put("Stage", "Known");
			  String[] columnNames = {"FTE Adjusted DO Share Amount"};
			  DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			  Date minDate = formatter.parse("09/01/2015");
			  Date maxDate = formatter.parse("08/31/2016");
			  BigDecimal[] summary = fileUnderTest.getColumnSummaryValues(fileUnderTestWorksheetName, criteria, false, false, false, columnNames, minDate, maxDate, logFileWriter);
			  
			  writeToLogFile("found value: " + summary[0].intValue() + " as faculty count", logFileWriter);
			  writeToLogFile("found value: " + summary[1].intValue() + " as '" + columnNames[0] +"' summary", logFileWriter);
			  
			  //compare the faculty count found in the report with the count displayed on the grid
			  int increment = 196;
			  verificationErrors = verificationErrors.append(compareSummaryInformationToControlSheet(increment, summary, columnNames, logFileWriter));
			  
			  if (verificationErrors.length() == 0)
				  writeToLogFile("Test Passed", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "111", "(D.4) Verify 'Previous Year Commitments' in Control Sheet Grid.", "Verify faculty with a salary log entry of 'Mid-Year'  appear in the Control Sheet Grid");
		  }

		  public void verifySumReappointmentsInControlSheetGrid() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 112 (D.5) Verify 'Faculty Appointment Related' - Reappointment - in Control Sheet Grid";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  HashMap<String, String> criteria = new HashMap<String, String>();
			  criteria.put("Category", "Faculty Appointment Related");
			  criteria.put("Subcategory", "Reappointment");
			  criteria.put("Source Pool", "Regular Merit Pool");
			  criteria.put("Stage", "Known");
			  String[] columnNames = {"FTE Adjusted DO Share Amount"};
			  DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			  Date minDate = formatter.parse("09/02/2015");
			  Date maxDate = formatter.parse("09/01/2016");
			  BigDecimal[] summary = fileUnderTest.getColumnSummaryValues(fileUnderTestWorksheetName, criteria, false, false, false, columnNames, minDate, maxDate, logFileWriter);
			  
			  writeToLogFile("found value: " + summary[0].intValue() + " as faculty count", logFileWriter);
			  writeToLogFile("found value: " + summary[1].intValue() + " as '" + columnNames[0] +"' summary", logFileWriter);
			  
			  //compare the faculty count found in the report with the count displayed on the grid
			  int increment = 212;
			  verificationErrors = verificationErrors.append(compareSummaryInformationToControlSheet(increment, summary, columnNames, logFileWriter));
			  
			  if (verificationErrors.length() == 0)
				  writeToLogFile("Test Passed", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "112", "(D.5) Verify 'Faculty Appointment Related' in Control Sheet Grid", "Verify category of 'Faculty Appointment Related' - Reappointment");
		  }

		  public void verifySumPromotionsToNonTenureInControlSheetGrid() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 113 (D.5) Verify 'Faculty Appointment Related' - Promotion to Non-Tenure - in Control Sheet Grid";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  HashMap<String, String> criteria = new HashMap<String, String>();
			  criteria.put("Category", "Faculty Appointment Related");
			  criteria.put("Subcategory", "Promotion to Non-Tenure");
			  criteria.put("Source Pool", "Regular Merit Pool");
			  criteria.put("Stage", "Known");
			  String[] columnNames = {"FTE Adjusted DO Share Amount"};
			  DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			  Date minDate = formatter.parse("09/02/2015");
			  Date maxDate = formatter.parse("09/01/2016");
			  BigDecimal[] summary = fileUnderTest.getColumnSummaryValues(fileUnderTestWorksheetName, criteria, false, false, false, columnNames, minDate, maxDate, logFileWriter);
			  
			  writeToLogFile("found value: " + summary[0].intValue() + " as faculty count", logFileWriter);
			  writeToLogFile("found value: " + summary[1].intValue() + " as '" + columnNames[0] +"' summary", logFileWriter);
			  
			  //compare the faculty count found in the report with the count displayed on the grid
			  int increment = 244;
			  verificationErrors = verificationErrors.append(compareSummaryInformationToControlSheet(increment, summary, columnNames, logFileWriter));
			  
			  if (verificationErrors.length() == 0)
				  writeToLogFile("Test Passed", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "113", "(D.5) Verify 'Faculty Appointment Related' in Control Sheet Grid", "Verify category of 'Faculty Appointment Related' - Promotion to Non-Tenure");
		  }

		  public void verifySumPromotionsToFullInControlSheetGrid() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 114 (D.5) Verify 'Faculty Appointment Related' - Promotion to Full - in Control Sheet Grid";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  HashMap<String, String> criteria = new HashMap<String, String>();
			  criteria.put("Category", "Faculty Appointment Related");
			  criteria.put("Subcategory", "Promotion to Full");
			  criteria.put("Source Pool", "Regular Merit Pool");
			  criteria.put("Stage", "Known");
			  String[] columnNames = {"FTE Adjusted DO Share Amount"};
			  DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			  Date minDate = formatter.parse("09/02/2015");
			  Date maxDate = formatter.parse("09/01/2016");
			  BigDecimal[] summary = fileUnderTest.getColumnSummaryValues(fileUnderTestWorksheetName, criteria, false, false, false, columnNames, minDate, maxDate, logFileWriter);
			  
			  writeToLogFile("found value: " + summary[0].intValue() + " as faculty count", logFileWriter);
			  writeToLogFile("found value: " + summary[1].intValue() + " as '" + columnNames[0] +"' summary", logFileWriter);
			  
			  //compare the faculty count found in the report with the count displayed on the grid
			  int increment = 260;
			  verificationErrors = verificationErrors.append(compareSummaryInformationToControlSheet(increment, summary, columnNames, logFileWriter));
			  
			  if (verificationErrors.length() == 0)
				  writeToLogFile("Test Passed", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "114", "(D.5) Verify 'Faculty Appointment Related' in Control Sheet Grid", "Verify category of 'Faculty Appointment Related' - Promotion to Full");
		  }

		  public void verifySumPromotionsToContinuingTermInControlSheetGrid() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 115 (D.5) Verify 'Faculty Appointment Related' - Promotion to Continuing Term - in Control Sheet Grid";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  HashMap<String, String> criteria = new HashMap<String, String>();
			  criteria.put("Category", "Faculty Appointment Related");
			  criteria.put("Subcategory", "Promotion to Continuing Term");
			  criteria.put("Source Pool", "Regular Merit Pool");
			  criteria.put("Stage", "Known");
			  String[] columnNames = {"FTE Adjusted DO Share Amount"};
			  DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			  Date minDate = formatter.parse("09/02/2015");
			  Date maxDate = formatter.parse("09/01/2016");
			  BigDecimal[] summary = fileUnderTest.getColumnSummaryValues(fileUnderTestWorksheetName, criteria, false, false, false, columnNames, minDate, maxDate, logFileWriter);
			  
			  writeToLogFile("found value: " + summary[0].intValue() + " as faculty count", logFileWriter);
			  writeToLogFile("found value: " + summary[1].intValue() + " as '" + columnNames[0] +"' summary", logFileWriter);
			  
			  //compare the faculty count found in the report with the count displayed on the grid
			  int increment = 276;
			  verificationErrors = verificationErrors.append(compareSummaryInformationToControlSheet(increment, summary, columnNames, logFileWriter));
			  
			  if (verificationErrors.length() == 0)
				  writeToLogFile("Test Passed", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "115", "(D.5) Verify 'Faculty Appointment Related' in Control Sheet Grid", "Verify category of 'Faculty Appointment Related' - Promotion to Continuing Term");
		  }


		  public void verifySumRemovalofSubjecttoPhDInControlSheetGrid() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 116 (D.5) Verify 'Faculty Appointment Related' - Removal of Subject to PhD - in Control Sheet Grid";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  HashMap<String, String> criteria = new HashMap<String, String>();
			  criteria.put("Category", "Faculty Appointment Related");
			  criteria.put("Subcategory", "Removal of \"Subject to PhD\"");
			  criteria.put("Source Pool", "Regular Merit Pool");
			  criteria.put("Stage", "Known");
			  String[] columnNames = {"FTE Adjusted DO Share Amount"};
			  DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			  Date minDate = formatter.parse("09/02/2015");
			  Date maxDate = formatter.parse("09/01/2016");
			  BigDecimal[] summary = fileUnderTest.getColumnSummaryValues(fileUnderTestWorksheetName, criteria, false, false, false, columnNames, minDate, maxDate, logFileWriter);
			  
			  writeToLogFile("found value: " + summary[0].intValue() + " as faculty count", logFileWriter);
			  writeToLogFile("found value: " + summary[1].intValue() + " as '" + columnNames[0] +"' summary", logFileWriter);
			  
			  //compare the faculty count found in the report with the count displayed on the grid
			  int increment = 292;
			  verificationErrors = verificationErrors.append(compareSummaryInformationToControlSheet(increment, summary, columnNames, logFileWriter));
			  
			  if (verificationErrors.length() == 0)
				  writeToLogFile("Test Passed", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "116", "(D.5) Verify 'Faculty Appointment Related' in Control Sheet Grid", "Verify category of 'Faculty Appointment Related' - Removal of Subject to PhD");
		  }

		  public void verifySumTerminalYearInControlSheetGrid() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 117 (D.5) Verify 'Faculty Appointment Related' - Terminal Year - in Control Sheet Grid";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  HashMap<String, String> criteria = new HashMap<String, String>();
			  criteria.put("Category", "Faculty Appointment Related");
			  criteria.put("Subcategory", "Terminal Year");
			  criteria.put("Source Pool", "Regular Merit Pool");
			  criteria.put("Stage", "Known");
			  String[] columnNames = {"FTE Adjusted DO Share Amount"};
			  DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			  Date minDate = formatter.parse("09/02/2015");
			  Date maxDate = formatter.parse("09/01/2016");
			  BigDecimal[] summary = fileUnderTest.getColumnSummaryValues(fileUnderTestWorksheetName, criteria, false, false, false, columnNames, minDate, maxDate, logFileWriter);
			  
			  writeToLogFile("found value: " + summary[0].intValue() + " as faculty count", logFileWriter);
			  writeToLogFile("found value: " + summary[1].intValue() + " as '" + columnNames[0] +"' summary", logFileWriter);
			  
			  //compare the faculty count found in the report with the count displayed on the grid
			  int increment = 308;
			  verificationErrors = verificationErrors.append(compareSummaryInformationToControlSheet(increment, summary, columnNames, logFileWriter));
			  
			  if (verificationErrors.length() == 0)
				  writeToLogFile("Test Passed", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "117", "(D.5) Verify 'Faculty Appointment Related' in Control Sheet Grid", "Verify category of 'Terminal Year' - Removal of Subject to PhD");
		  }

		  public void verifySumGovernanceRelatedChairsAndDirectorsInControlSheetGrid() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 118 (D.6) Verify 'Governance Related' - Chairs and Directors - in Control Sheet Grid";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  HashMap<String, String> criteria = new HashMap<String, String>();
			  criteria.put("Category", "Governance Related");
			  criteria.put("Subcategory", "Chairs/Directors");
			  criteria.put("Source Pool", "Regular Merit Pool");
			  criteria.put("Stage", "Known");
			  String[] columnNames = {"FTE Adjusted DO Share Amount"};
			  DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			  Date minDate = formatter.parse("09/02/2015");
			  Date maxDate = formatter.parse("09/01/2016");
			  BigDecimal[] summary = fileUnderTest.getColumnSummaryValues(fileUnderTestWorksheetName, criteria, false, false, false, columnNames, minDate, maxDate, logFileWriter);
			  
			  writeToLogFile("found value: " + summary[0].intValue() + " as faculty count", logFileWriter);
			  writeToLogFile("found value: " + summary[1].intValue() + " as '" + columnNames[0] +"' summary", logFileWriter);
			  
			  //compare the faculty count found in the report with the count displayed on the grid
			  int increment = 324;
			  verificationErrors = verificationErrors.append(compareSummaryInformationToControlSheet(increment, 5, summary, columnNames, logFileWriter));
			  
			  if (verificationErrors.length() == 0)
				  writeToLogFile("Test Passed", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "118", "(D.6) Verify 'Governance Related' in Control Sheet Grid.", "Verify category of 'Governance Related'");
		  }

		  public void verifySumGovernanceRelatedOtherGovInControlSheetGrid() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 119 (D.6) Verify 'Governance Related' - Other Gov - in Control Sheet Grid";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  HashMap<String, String> criteria = new HashMap<String, String>();
			  criteria.put("Category", "Governance Related");
			  criteria.put("Subcategory", "Other Gov");
			  criteria.put("Source Pool", "Regular Merit Pool");
			  criteria.put("Stage", "Known");
			  String[] columnNames = {"FTE Adjusted DO Share Amount"};
			  DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			  Date minDate = formatter.parse("09/02/2015");
			  Date maxDate = formatter.parse("09/01/2016");
			  BigDecimal[] summary = fileUnderTest.getColumnSummaryValues(fileUnderTestWorksheetName, criteria, false, false, false, columnNames, minDate, maxDate, logFileWriter);
			  
			  writeToLogFile("found value: " + summary[0].intValue() + " as faculty count", logFileWriter);
			  writeToLogFile("found value: " + summary[1].intValue() + " as '" + columnNames[0] +"' summary", logFileWriter);
			  
			  //compare the faculty count found in the report with the count displayed on the grid
			  int increment = 340;
			  verificationErrors = verificationErrors.append(compareSummaryInformationToControlSheet(increment, summary, columnNames, logFileWriter));
			  
			  if (verificationErrors.length() == 0)
				  writeToLogFile("Test Passed", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "119", "(D.6) Verify 'Governance Related' in Control Sheet Grid.", "Verify category of 'Other Gov'");
		  }
		  public void verifyTransferInformationInControlSheetGrid() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 120 (D.7) Verify 'Transfers' in Control Sheet Grid";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  HashMap<String, String> criteria = new HashMap<String, String>();
			  criteria.put("Category", "Transfers");
			  criteria.put("Subcategory", "Transfers");
			  criteria.put("Source Pool", "Dean's Office");
			  criteria.put("Destination Pool", "Regular Merit Pool");
			  criteria.put("Stage", "Known");
			  String[] columnNames = {"FTE Adjusted DO Share Amount"};
			  DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			  Date minDate = formatter.parse("09/02/2015");
			  Date maxDate = formatter.parse("09/01/2016");
			  BigDecimal[] summary = fileUnderTest.getColumnSummaryValues(fileUnderTestWorksheetName, criteria, false, false, false, columnNames, minDate, maxDate, logFileWriter);
			  summary[1] = summary[1].negate();
			  
			  writeToLogFile("found value: " + summary[0].intValue() + " as count", logFileWriter);
			  writeToLogFile("found value: " + summary[1].intValue() + " as '" + columnNames[0] +"' summary", logFileWriter);
			  
			  //compare the faculty count found in the report with the count displayed on the grid
			  int increment = 372;
			  verificationErrors = verificationErrors.append(compareSummaryInformationToControlSheet(increment, summary, columnNames, logFileWriter));
			  
			  if (verificationErrors.length() == 0)
				  writeToLogFile("Test Passed", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "120", "(D.7) Verify 'Transfers' in Control Sheet Grid", "Verify category of 'Transfers'");
		  }



		  public void verifyReservesInformationInControlSheetGrid() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 121 (D.8) Verify 'Reserves' in Control Sheet Grid";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  HashMap<String, String> criteria = new HashMap<String, String>();
			  criteria.put("Category", "Reserves");
			  criteria.put("Subcategory", "Other Reserves");
			  criteria.put("Source Pool", "Regular Merit Pool");
			  criteria.put("Stage", "Known");
			  String[] columnNames = {"FTE Adjusted DO Share Amount"};
			  DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			  Date minDate = formatter.parse("09/02/2015");
			  Date maxDate = formatter.parse("09/01/2016");
			  BigDecimal[] summary = fileUnderTest.getColumnSummaryValues(fileUnderTestWorksheetName, criteria, false, false, false, columnNames, minDate, maxDate, logFileWriter);
			  
			  writeToLogFile("found value: " + summary[0].intValue() + " as count", logFileWriter);
			  writeToLogFile("found value: " + summary[1].intValue() + " as '" + columnNames[0] +"' summary", logFileWriter);
			  
			  //compare the faculty count found in the report with the count displayed on the grid
			  int increment = 436;
			  verificationErrors = verificationErrors.append(compareSummaryInformationToControlSheet(increment, summary, columnNames, logFileWriter));
			  
			  if (verificationErrors.length() == 0)
				  writeToLogFile("Test Passed", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "121", "(D.8) Verify 'Reserves' in Control Sheet Grid", "Verify category of 'Reserves'");
		  }

		  
		  public void verifyControlSheetNotesInControlSheetGrid() throws Exception{
			  verificationErrors = new StringBuffer();
			  String testName = "HandSOn TEST 122 (D.9) Verify 'Control Sheet Notes'.";
			  System.out.println("Now executing test: " + testName);
			  String logFileName = LogDirectoryName +"/" + testName +".txt";
			  //System.out.println("File to be written is " +logFileName);
			  BufferedWriter logFileWriter = getLogFileWriter(logFileName);
			  
			  String expected = "Control Sheet Notes Test Entry.  \nControl Sheet Notes Test Entry.  \nControl Sheet Notes Test Entry.  \nControl Sheet Notes Test Entry.  ";
			  String actual = driver.findElement(By.name("salarySettingDTO.salSetConstantsDTO.notes")).getAttribute("value");
			  
			  if (! expected.contentEquals(actual)){
				  verificationErrors.append("MISMATCH: expected Notes: " + expected + ", actual Notes: " + actual);
			  }

			  if (verificationErrors.length() == 0)
				  writeToLogFile("Test Passed", logFileWriter);

			  logFileWriter.close();
			  writeTestResults("HandSOn TEST", "122", "(D.9) Verify 'Control Sheet Notes'.", "Enter notes in Control Sheet Notes and click 'Save'.");
		  }


		  
	  @After
	  public void tearDown() throws Exception {
		    driver.quit();
	  }
	  
	  public String verifySalarySettinginGUI() throws Exception{
		  verificationErrors = new StringBuffer();
		  String testName = "Verification of Salary Setting in the GUI";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);

		  if (openSalarySetting(logFileWriter)){
			  writeToLogFile("Salary Setting successfully opened", logFileWriter);
		  }
		  //Now that we've got the Salary Setting opened, open the Salary Worksheet
		  else verificationErrors.append("Salary Setting not successfully opened");
		  if (getSalarySettingWorksheet(logFileWriter)){
			  writeToLogFile("Salary Worksheet successfully opened", logFileWriter);
		  }
		  else verificationErrors.append("Salary Worksheet not successfully opened");
		  //Now that the Salary Worksheet is opened, set the values
		  if (setSalarySettingWorksheet(logFileWriter)){
			  writeToLogFile("Salary Worksheet is successfully set", logFileWriter);
		  }
		  else verificationErrors.append("Salary Worksheet not successfully set.  ");
		  Thread.sleep(4000);
		  if (areSalaryCalculationsCorrect(logFileWriter)){
			  writeToLogFile("Salary Calculations are correct", logFileWriter);
		  }
		  else{
			  verificationErrors.append("Salary Calculations are not correct.  ");
		  }
		  logFileWriter.close();
		  return verificationErrors.toString();

	  }
	  
	  public void writeTestResults(String testEnv, String testCaseNumber, String testDescription, String testCase){
		    String verificationErrorString = verificationErrors.toString();
		    int lastRow = 1;
		    if (!"".equals(verificationErrorString)) {
		    	System.out.println("Test FAILED");
		    	System.out.println(verificationErrorString);
		    	lastRow = fileOut.getRowCount(errorWorksheetName) + 1;
		    	fileOut.setCellData(errorWorksheetName, "Test Env.", lastRow, testEnv);
		    	fileOut.setCellData(errorWorksheetName, "Test Case #", lastRow, testCaseNumber);
		    	fileOut.setCellData(errorWorksheetName, "Test Description", lastRow, testDescription);
		    	fileOut.setCellData(errorWorksheetName, "Test Case", lastRow, testCase);
		    	fileOut.setCellData(errorWorksheetName, "Cumulative Error String", lastRow, verificationErrorString);
		    	//we had to comment this out.  If one test fails, we still want the other tests executed.  
		    	//This code, unfortunately, kills all test execution once the "fail" method is called.
		    	//fail(verificationErrorString);
		    }
		    else {
		    	System.out.println("Test passed");
		    	lastRow = fileOut.getRowCount(outputWorksheetName) + 1;
		    	fileOut.setCellData(outputWorksheetName, "Test Env.", lastRow, testEnv);
		    	fileOut.setCellData(outputWorksheetName, "Test Case #", lastRow, testCaseNumber);
		    	fileOut.setCellData(outputWorksheetName, "Test Description", lastRow, testDescription);
		    	fileOut.setCellData(outputWorksheetName, "Test Case", lastRow, testCase);
		    }
		  
	  }
	  public void setAPValuesinGUI() throws Exception{
		  verificationErrors = new StringBuffer();
		  String testName = "Setting AP Values in GUI";
		  System.out.println("Now executing test: " + testName);
		  String logFileName = LogDirectoryName +"/" + testName +".txt";
		  BufferedWriter logFileWriter = getLogFileWriter(logFileName);

		  openSalarySetting(logFileWriter);
		  
		  waitForElementPresent(By.xpath("//thead/tr/td[2]/input"));
		  
		  //String Reappointment = driver.findElement(By.xpath("//thead/tr/td[2]/input")).getAttribute("value");
		  setIndividualAPValueInGUI("Reappointment", logFileWriter);
		  setIndividualAPValueInGUI("PromoNonTenure", logFileWriter);
		  setIndividualAPValueInGUI("PromoContTerm", logFileWriter);
		  setIndividualAPValueInGUI("PromoTenure", logFileWriter);
		  setIndividualAPValueInGUI("PromoFull", logFileWriter);
		  setIndividualAPValueInGUI("PromoSubjPhD", logFileWriter);
		  
		  //Now, click on the "Save" button
		  waitAndClick(By.id("buttonSubmit"));
		  Thread.sleep(2000);
		  driver.switchTo().alert().accept();
		  logFileWriter.close();


	  }
	  
	  /*
	  public String[][] getMarketCriteria(String sheetName, BufferedWriter logFileWriter) throws Exception{
		  	writeToLogFile("method getMarketCriteria has been called", logFileWriter);
			String marketCriteriaSheetName = sheetName;
			String[][] marketCriteria = new String[fileIn.getRowCount(marketCriteriaSheetName)-1][fileIn.getColumnCount(marketCriteriaSheetName)];
			logFileWriter.write("Market Criteria is specified - retrieving Market Criteria");
			logFileWriter.newLine();
			//read the Market Criteria from the Market Criteria worksheet
			for (int rowIndex = 1; rowIndex<fileIn.getRowCount(marketCriteriaSheetName) -1; rowIndex++){
				for (int columnIndex = 0; columnIndex < fileIn.getColumnCount(marketCriteriaSheetName); columnIndex++){
					String marketCriterion = fileIn.getCellData(marketCriteriaSheetName, rowIndex, columnIndex);
					marketCriteria[rowIndex][columnIndex] = marketCriterion;
					logFileWriter.write("Market Criteria added value " + marketCriterion 
							+ " at row " + rowIndex + " at column " + columnIndex);
					logFileWriter.newLine();
				}
			}
			return marketCriteria;
	  }
	  */
	  
	  public void setIndividualAPValueInGUI(String name, BufferedWriter logFileWriter) throws Exception{
		  String value = fileIn.getValueForColumnNameAndValue(globalInputWorksheetName, name, "Name", "Value", logFileWriter);
		  String locator = fileIn.getValueForColumnNameAndValue(globalInputWorksheetName, name, "Name", "Locator", logFileWriter);
		  writeToLogFile("Initial " + name + " figure is " + value, logFileWriter);
		  driver.findElement(By.xpath(locator)).clear();
		  driver.findElement(By.xpath(locator)).sendKeys(value);

	  }
  
	  public boolean openSalarySetting(BufferedWriter logWriter) throws Exception{
		  writeToLogFile("Now opening Salary Setting", logWriter);
		  driver.get(startPage);
		  waitAndClick (By.linkText("Operations"));
		  waitAndClick (By.linkText("Salary Setting"));
		  try{
			  writeToLogFile("Trying to switch to the dialog", logWriter);
			  if(switchWindowByURL (fiscalYearDialogURL, logWriter)){
				  writeToLogFile("Successfully acquired dialog", logWriter);
			  }
			  //driver.switchTo().window("ha_dialog");
		  }
		  catch (Exception e){
			  e.printStackTrace();
			  return false;
		  }
		  try{
			  writeToLogFile("Trying to get the fiscal year dropdown", logWriter);
			  new Select(driver.findElement(By.id("fiscalYear"))).selectByVisibleText("2016-17");
		  }
		  catch (Exception e){
			  e.printStackTrace();
			  return false;
		  }
		  try{
			  writeToLogFile("Trying to get to the button on the dialog", logWriter);
			  driver.findElement(By.id("mybutton")).click();
		  }
		  catch (Exception e){
			  writeToLogFile(e.getStackTrace().toString(), logWriter);
			  return false;
		  }
		  while (! switchWindowByURL(loadSalarySettingWindowURL, logWriter)){
			  writeToLogFile("Salary Setting Window not yet located", logWriter);
			  Thread.sleep(1000);
		  }
		  writeToLogFile("Salary Setting Window located", logWriter);
		  if (isDropdownAccessible(By.id("statusId"), "Closed", logWriter)) writeToLogFile("Set status to Closed", logWriter);
		  else{
			  writeToLogFile("Unable to set status to Closed", logWriter);
		  }
		  Thread.sleep(2000);
		  if (isDropdownAccessible(By.id("statusId"), "Open", logWriter)) writeToLogFile("Set status to Open", logWriter);
		  else{
			  writeToLogFile("Unable to set status to Open", logWriter);
		  }
		  Thread.sleep(2000);
		  return true;
	  }
	  
	  public boolean getSalarySettingWorksheet(BufferedWriter logWriter) throws Exception{
		waitAndClick(By.id("salarySetting_a"));
		/*
		if (dismissDialog(By.linkText("Reset to Database Values"))){
			System.out.println("Dialog successfully dismissed - continuing to Salary Setting Worksheet");
		}
		else{
			System.out.println("Could not dismiss the dialog - aborting attempt to get to Salary Setting Worksheet");
			return false;
		}
		*/
		if (isDropdownAccessible(By.id("clusterLevel"), "Humanities and Arts Cluster", logWriter)){
			writeToLogFile("Got the widget on the Salary Worksheet", logWriter);
			return true;
		}
		else{ 
			writeToLogFile("Did not get the widget on the Salary Worksheet - cannot assume it's loaded", logWriter);
			return false;
		}

	  }
	  
	  public boolean dismissDialog(By by, BufferedWriter logWriter) throws Exception{
			try{
				  writeToLogFile("Trying to switch to the dialog", logWriter);
				  writeToLogFile(switchWindow(by, logWriter), logWriter);
			  }
			  catch (Exception e){
				  e.printStackTrace();
				  return false;
			  }
			writeToLogFile("Got the dialog - now clicking on dialog widget", logWriter);
			waitAndClick(by);
			writeToLogFile("Dialog successfully clicked.  Searching for worksheet widget", logWriter);
			return true;
		  
	  }
	  
	  public boolean setSalarySettingWorksheet(BufferedWriter logWriter) throws Exception{
		  if (isDropdownAccessible(By.id("clusterLevel"), "Humanities and Arts Cluster", logWriter))
			  driver.findElement(By.cssSelector("#clusterLevel > option[value=\"PCXU\"]")).click();
		  new Select(driver.findElement(By.id("deptLevel"))).selectByVisibleText("History");
		  //isDropdownAccessible(By.id("faculty"), getFacultyName(2));
		  new Select(driver.findElement(By.id("role"))).selectByVisibleText("Department");
		  if(waitAndClick(By.xpath("//tr[4]/td/table/tbody/tr/td[9]/a")))
		  return true;
		  else return false;
	  }

	  public boolean areSalaryCalculationsCorrect(BufferedWriter logFileWriter) throws Exception{
		  //first, let's assign our locators
		  By nameLocator = By.xpath("//div/table/tbody/tr[3]/td/div/table/tbody/tr/td[3]");
		  By prevSalarylocator = By.xpath("//tr[3]/td[2]/div/table/tbody/tr/td[8]");
		  By raisePercentLocator = By.xpath("//tr[3]/td[2]/div/table/tbody/tr/td[9]");
		  By currSalaryLocator = By.xpath("//tr[3]/td[2]/div/table/tbody/tr/td[10]");
		  //now, verify that the previous Salary figure is displayed as expected
		  waitForElementPresent(nameLocator);
		  writeToLogFile("Verifying salary information for Faculty " + driver.findElement(nameLocator).getText(), logFileWriter);
		  String prevSalaryText = driver.findElement(prevSalarylocator).getText();
		  if (! isTextPresent (prevSalaryText, prevSalarylocator, logFileWriter)){
			  return false;
		  }
		  //String expectedText = getSalaryFigure(1, 2);
		  // Now that we know it's there, let's format it and verify its value is correct
		  String formattedPrevSalaryText = ((Long)NumberFormat.getNumberInstance(java.util.Locale.US).parse(prevSalaryText)).toString();
		  int prevSalary = new Integer(formattedPrevSalaryText).intValue();
		  writeToLogFile("Previous Salary Figure is " + formattedPrevSalaryText, logFileWriter);
		  
		  //Now that we have the previous Salary, let's work with the percentage raise
		  String raisePercent = driver.findElement(raisePercentLocator).getText();
		  float multiplier = new Float(raisePercent.substring(0, raisePercent.length()-1)).floatValue();
		  multiplier = (multiplier/100) + 1;
		  writeToLogFile("multiplier to be used to calculate current salary is " + new Float(multiplier).toString(), logFileWriter);
		  
		  //Now that we have the previous Salary and the raise percentage, let's verify the current Salary
		  int expectedCurrSalary = (int) new Float(multiplier * prevSalary).floatValue();
		  writeToLogFile("base expected current salary figure is " +expectedCurrSalary, logFileWriter);
		  int variance = 10;
		  String currSalaryText = ((Long)NumberFormat.getNumberInstance(java.util.Locale.US).parse(driver.findElement(currSalaryLocator).getText())).toString();
		  writeToLogFile("actual current salary figure is " + currSalaryText, logFileWriter);
		  int actualCurrSalary = new Integer(currSalaryText).intValue();
		  if (actualCurrSalary < expectedCurrSalary-variance || actualCurrSalary > expectedCurrSalary + variance) return false;
		  else writeToLogFile("Salary figure falls within expected variance", logFileWriter);
		  return true;
	  }
	  
	  public boolean findFaculty(String firstName, String lastName, BufferedWriter logFileWriter) throws Exception{
		  //boolean facultyFound = false;
		  driver.get(baseUrl + "/apphs/processManageFaculty.do?ACTION=showFacultySearch#results");
		  writeToLogFile("Current window URL is : " + driver.getCurrentUrl(), logFileWriter);
		  waitForElementPresent(By.id("facultyName"));
		  driver.findElement(By.id("facultyName")).clear();
		  driver.findElement(By.id("facultyName")).sendKeys(firstName);
		  driver.findElement(By.name("facultyQueryDTO.lastName")).clear();
		  driver.findElement(By.name("facultyQueryDTO.lastName")).sendKeys(lastName);
		  driver.findElement(By.xpath("//div[2]/table/tbody/tr[2]/td/a")).click();
		  Thread.sleep(3000);
		  // Now, open the page that applies to this particular faculty member
		  //click on the search widget
		  String searchResult = driver.findElement(By.xpath("//div[9]/table/tbody/tr[2]/td/a")).getText();
		  writeToLogFile("Name derived from the search is : " +searchResult, logFileWriter);
		  driver.findElement(By.xpath("//div[9]/table/tbody/tr[2]/td/a")).click();
		  //now, switch to the window (just opened) that shows the Faculty Record
		  if (switchWindowByURL(facultyRecordWindowURL + searchResult, logFileWriter)){
			  try {
				  String fullName = driver.findElement(By.cssSelector("td.readState")).getText();
				  if (fullName.contains(firstName) && fullName.contains(lastName)){
					  return true;
				  }
				  else{ 
					  writeToLogFile("Faculty that was searched for is not found: " +firstName + " " + lastName, logFileWriter);
					  return false;
				  }
				} catch (Error e) {
				  verificationErrors.append(e.toString());
				  return false;
				}
		  }
		  else return false;
/*
			  waitAndClick(By.linkText("Retention"));
	*/	  
		  

	  }
	  
	  public String addNewRetention(BufferedWriter logFileWriter) throws Exception{
		  String newRetentionID = "";

		  if (switchWindowByURL(facultyRecordWindowURL, logFileWriter)){
			  waitAndClick(By.linkText("Retention"));
			  writeToLogFile("Add Retention Record Link is successfully located", logFileWriter);
			  waitAndClick(By.linkText("Add New Retention Record"));
		  }
		  else writeToLogFile("could not access Add New Retention Record link on the Faculty Record", logFileWriter);

		  if (switchWindowByURL(showRetentionWindowURL, logFileWriter)){
			  writeToLogFile("Got the new retention record window", logFileWriter);
		  }
		  else writeToLogFile("could not access New Retention Record window", logFileWriter);
		  
		  if (isDropdownAccessible (By.id("offerStatus"), "Offer Outstanding", logFileWriter)){
			  if (isDropdownAccessible (By.name("retentionDTO.retentionTypeCd"), "Preemptive", logFileWriter)){
				  driver.findElement(By.id("effectiveYearSel")).clear();
				  driver.findElement(By.id("effectiveYearSel")).sendKeys("2017");
				  driver.findElement(By.id("rentOpenDate")).clear();
				  driver.findElement(By.id("rentOpenDate")).sendKeys("9/2/2015");
				  driver.findElement(By.id("rentCloseDate")).clear();
				  driver.findElement(By.id("rentCloseDate")).sendKeys("9/2/2015");
				  driver.findElement(By.id("rentApprDate")).clear();
				  driver.findElement(By.id("rentApprDate")).sendKeys("9/2/2016");
				  driver.findElement(By.id("buttonSubmit")).click();
				  Thread.sleep(3000);
				  try{
					  driver.switchTo().alert().accept();
				  }
				  catch(NoAlertPresentException e){
					  writeToLogFile(e.getMessage(), logFileWriter);
				  }
				  Thread.sleep(2000);
				  waitForElementPresent(By.id("retentionIdVal"));
				  newRetentionID = driver.findElement(By.id("retentionIdVal")).getText();
				  writeToLogFile("Got the new Retention Record ID: " + newRetentionID, logFileWriter);
			  }
		  }
		  else {
			  writeToLogFile("Could not select the offer status - returning", logFileWriter);
			  return "";
		  
		  }


		  return newRetentionID;
	  }
	  
	  public boolean addRetentionCost(String retentionRecord, BufferedWriter logFileWriter) throws Exception{
		  driver.get(baseUrl + "/apphs/processManageFaculty.do?ACTION=showFacultySearch#results");
		  if (switchWindow(By.linkText("Retention Search"), logFileWriter).equalsIgnoreCase("Got the window")){
			  writeToLogFile("Got the main page", logFileWriter);
		  }
		  else{
			  writeToLogFile("could not access main page", logFileWriter);
			  return false;
		  }

		  driver.findElement(By.linkText("Retention Search")).click();
		  Thread.sleep(2000);
		  //driver.switchTo().alert().accept();
		  Thread.sleep(2000);
		  
		  if (switchWindowByURL(showRetentionLookupWindowURL, logFileWriter)){
			  writeToLogFile("Got the Retention Search page", logFileWriter);
		  }
		  else{
			  writeToLogFile("could not access Retention Search page", logFileWriter);
			  return false;
		  }
		  waitForElementPresent(By.id("retentionId"));
		  driver.findElement(By.id("retentionId")).clear();
		  driver.findElement(By.id("retentionId")).sendKeys(retentionRecord);
		  driver.findElement(By.id("mybutton")).click();
		  driver.findElement(By.linkText(retentionRecord)).click();

		  //Now, click on the "Costs" tab
		  if (switchWindowByURL(showRetentionWindowURL + "?retId=" + retentionRecord, logFileWriter)){
			  writeToLogFile("Got the costs tab of retention record ID " + retentionRecord, logFileWriter);
		  }
		  else{
			  writeToLogFile("could not access costs tab of retention record ID " + retentionRecord, logFileWriter);
			  return false;
		  }
		  
		  driver.findElement(By.id("costs_a")).click();
		  if (isDropdownAccessible (By.id("salaryLangCommitTypeCd0"), "$ Increase", logFileWriter)){
			  writeToLogFile("Able to access cost-related widgets for retention", logFileWriter);
			  waitForElementPresent(By.id("salaryEstIncAmt0"));
			  driver.findElement(By.id("salaryEstIncAmt0")).clear();
			  driver.findElement(By.id("salaryEstIncAmt0")).sendKeys("10000.00");
			  driver.findElement(By.id("salaryDeptContribution_0")).clear();
			  driver.findElement(By.id("salaryDeptContribution_0")).sendKeys("4,000.00");
			  driver.findElement(By.id("salaryFundingNotes_0")).clear();
			  driver.findElement(By.id("salaryFundingNotes_0")).sendKeys("Department pays only 40%");
			  String startingSalary = driver.findElement(By.id("startingSalary")).getAttribute("value");
			  String endingSalary = new Float(new Float((Long)NumberFormat.getNumberInstance(java.util.Locale.US).parse(startingSalary)).floatValue() + 10000.00).toString();
			  driver.findElement(By.id("salaryAmt0")).clear();
			  driver.findElement(By.id("salaryAmt0")).sendKeys(endingSalary);
			  driver.findElement(By.xpath("(//input[@id='buttonSubmit'])[2]")).click();
			  if (switchWindowByURL(annotationPopupWindowURL, logFileWriter)){
				  writeToLogFile("Got the change reason dialog", logFileWriter);
			  }
			  else{
				  writeToLogFile("could not get the change reason dialog", logFileWriter);
				  return false;
			  }
			  waitAndClick(By.id("buttonSubmit"));
			  
			  return true;
		  }
		  else return false;
	  }
	  
	  public boolean verifyFacultyDataSetsMultipleValues(LinkedList<HashMap<String, String>> listOfExpectedValues, LinkedList<HashMap<String, String>> listOfActualValues, String[] columnNames, BufferedWriter logFileWriter) throws Exception{
		  
		  //now, compare the contents of the two LinkedList Objects - listOfExpectedValues and listOfActualValues
		  //first, verify that the two LinkedLists are of equal size
		  if(listOfExpectedValues.size() == listOfActualValues.size()){
			  writeToLogFile("Number of expected values and number of actual values is equal", logFileWriter);
			  //now, iterate through the LinkedList of expected values and see if the LinkedList of actual values matches it
			  Iterator<HashMap<String, String>> expectedValuesIterator = listOfExpectedValues.iterator();
			  //go through the expected values and verify that actual values matches up
			  while(expectedValuesIterator.hasNext()){
				  HashMap<String, String> expected = expectedValuesIterator.next();
				  writeToLogFile("Trying to find a match for LinkedList of expected values "+expected.toString(), logFileWriter);
				  //go through the LinkedList of actual values until we either find a match or finish going through the LinkedList
				  Iterator<HashMap<String, String>> actualValuesIterator = listOfActualValues.iterator();
				  boolean match = true;
				  while (actualValuesIterator.hasNext()){
					  match = true;//re-initialize each time
					  HashMap<String, String> actual = actualValuesIterator.next();
					  writeToLogFile("Comparing to LinkedList of actual values " + actual.toString(), logFileWriter);
					  for(int columnIndex = 0; columnIndex < columnNames.length && match; columnIndex++){
						  String expectedValueString = expected.get(columnNames[columnIndex]);
						  String actualValueString = actual.get(columnNames[columnIndex]);
						  match = expectedValueString.equalsIgnoreCase(actualValueString);
						  if (match) writeToLogFile("Match: on column "+columnNames[columnIndex], logFileWriter);
						  else writeToLogFile("MISMATCH on column "+columnNames[columnIndex]+", expected: "+expectedValueString+", actual: "+actualValueString, logFileWriter);
					  }//end for loop
					  if (match) break;
				  }// end inner while loop - iterating through the list of actual values
				  if (! match){
					  String errMsg = "MISMATCH: Expected values (could not find match) are as follows: " + expected.toString();
					  verificationErrors.append(errMsg);
					  writeToLogFile(errMsg, logFileWriter);
					  return false;
				  }
			  }//end while - the two LinkedLists have been compared to each other
		  }//end if - the number of expected and actual values matches
		  else{//the number of values returned does not match
			  String errMsg = "Number of expected values and number of actual values DOES NOT MATCH";
			  writeToLogFile(errMsg, logFileWriter);
			  verificationErrors.append(errMsg + "; ");
			  errMsg = "Number of expected values: " + listOfExpectedValues.size();
			  writeToLogFile(errMsg, logFileWriter);
			  verificationErrors.append(errMsg + "; ");
			  errMsg = "Number of actual values: " + listOfActualValues.size();
			  writeToLogFile(errMsg, logFileWriter);
			  verificationErrors.append(errMsg + "; ");
			  return false;
		  }// end else - the number of expected and actual values does not match
		  return true;

	  }
	  
	  public String compareSummaryInformationToControlSheet(int increment, BigDecimal[] summary, String[] columnNames, BufferedWriter logFileWriter) throws Exception{
		  return compareSummaryInformationToControlSheet(increment, 1, summary, columnNames, logFileWriter);
	  }
	  
	  public String compareSummaryInformationToControlSheet(int increment, int variance, BigDecimal[] summary, String[] columnNames, BufferedWriter logFileWriter) throws Exception{
		  StringBuffer verificationErrors = new StringBuffer();
		  String controlSheetWorksheetName = "Control Sheet";
		  for(int i=0; i<2; i++){
			  String locator = fileIn.getCellData(controlSheetWorksheetName, 1, i + increment);
			  writeToLogFile("Derived locator is " + locator, logFileWriter);
			  waitForElementPresent(By.xpath(locator));
			  String expectedString = driver.findElement(By.xpath(locator)).getText();
			  writeToLogFile("Derived value is " + expectedString, logFileWriter);
			  long expected = ((Long)NumberFormat.getNumberInstance(java.util.Locale.US).parse(expectedString)).longValue();
			  if (summary[i].longValue() != expected){
				  if (expected > 1000){
					  if(summary[i].longValue() < expected-variance || summary[i].longValue() > expected + variance){
						  String errMsg = "MISMATCH - Expected value=" + expected + ", actual value=" + summary[i].longValue() + "; ";
						  writeToLogFile(errMsg, logFileWriter);
						  verificationErrors.append(errMsg);
					  }
					  else writeToLogFile("value found is "+ summary[i].longValue() +", actual falls within expected variance", logFileWriter);
				  }
				  else{
					  String errMsg = "MISMATCH - Expected value=" + expected + ", actual value=" + summary[i].longValue() + "; ";
					  writeToLogFile(errMsg, logFileWriter);
					  verificationErrors.append(errMsg);
				  }
			  }
			  else writeToLogFile("value found is "+ summary[i].longValue() +", as expected", logFileWriter);
		  }
		  return verificationErrors.toString();
	  }	  
	  
	  public boolean verifyNamesForFacultyGroup(HashMap keysAndValues, BufferedWriter logFileWriter, HSSFExcelReader fileIn, HSSFExcelReader fileUnderTest) throws Exception{
		  boolean namesMatch = true;
		  LinkedList<String> expectedValues = fileIn.getListOfFacultyValues(individualInputWorksheetName, keysAndValues, "Name", logFileWriter);
		  LinkedList<String> actualValues = fileUnderTest.getListOfFacultyValues(fileUnderTestWorksheetName, keysAndValues, "Name", logFileWriter);
		  
		  if (expectedValues.size() != actualValues.size()){
			  String errMsg = "MISMATCH: number of expected names: " + expectedValues.size() 
					  	+", actual number of names: " + actualValues.size();
			  verificationErrors.append(errMsg);
			  writeToLogFile(errMsg, logFileWriter);
			  namesMatch = false;
			  return namesMatch;
		  }
		  else {
			  Iterator<String> expectedValuesIterator = expectedValues.iterator();
			  Iterator<String> actualValuesIterator = actualValues.iterator();
			  
			  while(expectedValuesIterator.hasNext()){
				  String expected = expectedValuesIterator.next();
				  String actual = actualValuesIterator.next();
				  if (! expected.equalsIgnoreCase(actual)){
					  namesMatch = false;
					  writeToLogFile("MISMATCH - expected: " + expected + ", actual: " + actual, logFileWriter);
				  }//end if
			  }//end while
		  }//end else

		  return namesMatch;
	  }


	  
	  /*
	   * This takes an array of faculty names and an array of columns holding the values to verify
	   * for each faculty name.  It calls the "verifyValuesForFacultyName" method repeatedly 
	   * (in a "for" loop) for each of the faculty names.
	   */
	  public boolean[] verifyValuesForMultipleFacultyNames(String[] names, String[] columnNames, BufferedWriter logFileWriter, HSSFExcelReader fileIn, String infileSheetName, HSSFExcelReader fileUnderTest, String fileUnderTestWorksheetName) throws Exception{
		  boolean[] valuesMatch = new boolean[names.length];
		  
		  for (int i = 0; i < valuesMatch.length; i++){
			  writeToLogFile("Now verifying faculty " + names[i] + ";", logFileWriter);
			  valuesMatch[i] = verifyValuesForFacultyName(names[i], columnNames, logFileWriter, fileIn, infileSheetName, fileUnderTest, fileUnderTestWorksheetName);
		  }
		  
		  return valuesMatch;
	  }
	  
	  public boolean verifyValuesForFacultyName(String name, String[] columnNames, BufferedWriter logFileWriter, HSSFExcelReader fileIn, String infileSheetName, HSSFExcelReader fileUnderTest, String fileUnderTestWorksheetName) throws Exception{
		  boolean valuesMatch = true;
		  
		  HashMap<Integer, String[]> expectedValues = fileIn.getMultipleValuesForFacultyName(infileSheetName, name, columnNames, logFileWriter);
		  HashMap<Integer, String[]> actualValues = fileUnderTest.getMultipleValuesForFacultyName(fileUnderTestWorksheetName, name, columnNames, logFileWriter);

		  if (expectedValues.size() != actualValues.size()){
			  String errMsg = "MISMATCH: number of expected rows with values: " + expectedValues.size() 
					  	+", actual number of rows with values: " + actualValues.size();
			  verificationErrors.append(errMsg);
			  writeToLogFile(errMsg, logFileWriter);
			  valuesMatch = false;
			  return valuesMatch;
		  }
		  else {//OK, then, the two HashMaps have the same size, so verify contents
				for (Object key: expectedValues.keySet()) {
					logFileWriter.write("Row number: " + new Integer((Integer) key).toString());
					logFileWriter.newLine();
					String[] values = expectedValues.get(key);
					for(int columnIndex = 0; columnIndex<values.length; columnIndex++){
						logFileWriter.write("Trying to find a match on column " + columnNames[columnIndex]);
						logFileWriter.write(", with value " + values[columnIndex]);
						logFileWriter.newLine();
						boolean match = false;
						for (Object actualKey: actualValues.keySet()){
							String actual = actualValues.get(actualKey)[columnIndex];
							if (actual.equalsIgnoreCase(values[columnIndex])){
								logFileWriter.write("Match found: " + actual);
								logFileWriter.newLine();
								match = true;
								break;
							}// end if
						}//end for loop - looping through actual values
						if (! match){
							valuesMatch = false;
							logFileWriter.write("No match found for column "+ columnNames[columnIndex]);
							logFileWriter.newLine();
						}// end if
					}// end inner for loop
				}//end for loop
		  }

		  
		  return valuesMatch;
	  }
	  
	  public BufferedWriter getLogFileWriter(String logFileName) throws Exception{
		  File logFile = new File(logFileName);
		  
		  if (! logFile.exists()){
			  if(logFile.createNewFile()){
				  System.out.println("New Log file created: " + logFileName);
			  }
			  else{
				  System.out.println("Couldn't create new logfile: "+ logFileName);
			  }
		  }
		  else{
			  System.out.println("Log file already exists: " + logFileName);
			  logFile.delete();
			  if(logFile.createNewFile()){
				  System.out.println("New Log file created: " + logFileName);
			  }
			  else{
				  System.out.println("Couldn't create new logfile: " + logFileName);
			  }
		  }
		  
		  FileWriter FW = new FileWriter(logFile);
		  return new BufferedWriter(FW);

	  }
	  
	  
	  
	  public String switchWindow(By by, BufferedWriter logWriter){
	        try {
	        	for(String winHandle : driver.getWindowHandles()){
	        		driver.switchTo().window(winHandle);
	        		writeToLogFile("Window switched - driver is now on Window URL: " + driver.getCurrentUrl(), logWriter);
	        		try{
	        			if (isElementPresent(by)) break;
	        		}
	        		catch (Exception e){
	        			e.printStackTrace();
	        			System.out.print("couldn't locate element " +by.toString());
	        			writeToLogFile("switching to next window", logWriter);
	        		}
	        		writeToLogFile("element " +by.toString() + " not found - going to next window", logWriter);
	        	}
	        } catch(Exception e){
	        	return "FAIL: Unable to Switch Window because of Exception: " + e.getMessage();
	        }

	        if (isElementPresent(by)) return "Got the window";
	        else return "couldn't locate the window";
	  }

	  public boolean switchWindowByURL(String URL, BufferedWriter logWriter) throws Exception{
	        try {
	            String currentWindow = driver.getWindowHandle(); 
	            Set<String> availableWindows = driver.getWindowHandles(); 
	            if (!availableWindows.isEmpty()) { 
		            for (String windowId : availableWindows) {
		                String switchedWindowURL=driver.switchTo().window(windowId).getCurrentUrl();
		                if ((switchedWindowURL.equals(URL))||(switchedWindowURL.contains(URL))){
		                	writeToLogFile("Found Window with URL " + driver.getCurrentUrl(), logWriter);
		                    return true; 
		                } else { 
		                  driver.switchTo().window(currentWindow); 
		                } 
		            } 
	            }
	        }
	        catch (Exception e){
	        	writeToLogFile("Couldn't finish searching for target URL " + URL +" because of Exception " + e.getMessage(), logWriter);
	        	if (e.getMessage().contains("target window already closed")){
		        	Thread.sleep(1000);
		        	return switchWindowByURL(URL, logWriter);
	        	}//end if
	        	else return false;
	        }
	        writeToLogFile("Couldn't locate window with URL " + URL, logWriter);
	        return false;
	   }

	  
	  public String getSalaryFigure(int column, int row_index, BufferedWriter logWriter) throws Exception{
		  logWriter.write("Now retrieving Expected Salary figure - column name: ");
		  String columnName = fileIn.getCellData(individualInputWorksheetName, column, 1);
		  writeToLogFile(columnName, logWriter);
		  logWriter.write("... for Faculty Member " + getFacultyName(row_index, logWriter));
		  String salaryFigure = fileIn.getCellData(individualInputWorksheetName, column, row_index);
		  writeToLogFile("Derived Expected Salary Figure is " + salaryFigure, logWriter);
		  return salaryFigure;
	  }
	  
	  public String getFacultyName(int row_index, BufferedWriter logWriter) throws Exception{
		  logWriter.write("Now deriving Faculty Name: ");
		  String facultyName = fileIn.getCellData(individualInputWorksheetName, FACULTY_NAME_INDEX, row_index);
		  writeToLogFile(facultyName, logWriter);
		  return facultyName;
	  }
	  
	  public int getIntValueFromText(By by) throws Exception{
		  String rawText = driver.findElement(by).getText();
		  String formattedText = ((Long)NumberFormat.getNumberInstance(java.util.Locale.US).parse(rawText)).toString();
		  int intValue = new Integer(formattedText).intValue();
		  return intValue;
	  }
	  
	  
	  public boolean isWidgetClickAble (By by, BufferedWriter logWriter) throws Exception{
		  try{
			    for (int second = 0;; second++) {
			    	if (second >= 5) {
			    		writeToLogFile("widget " + by.toString() + " is not clickable ... returning", logWriter);
			    		return false;
			    	}
			    	try { if (isElementClickable(by)) break; } catch (Exception e) {}
			    	writeToLogFile("waiting for element to be click-able, second: " + second, logWriter);
			    	Thread.sleep(1000);
			    }
		  }
		  catch (Exception e){
			  e.printStackTrace();
			  return false;
		  }
		  writeToLogFile("widget " + by.toString() + " is clickable ... returning", logWriter);
		  return true;
	  }
	  
	  public boolean isDropdownAccessible (By by, String selection, BufferedWriter logWriter) throws Exception{
		  try{
			    for (int second = 0;; second++) {
			    	if (second >= 5) {
			    		writeToLogFile("widget " + by.toString() + " is not select-able ... returning", logWriter);
			    		return false;
			    	}
			    	try { 
			    		if (isElementPresent(by)){
			    			new Select(driver.findElement(by)).selectByVisibleText(selection);
			    			break; 
			    		}
			    	} 
			    	catch (Exception e) {}
			    	writeToLogFile("waiting for element to be select-able, second: " + second, logWriter);
			    	Thread.sleep(1000);
			    }
		  }
		  catch (Exception e){
			  e.printStackTrace();
			  return false;
		  }
		  writeToLogFile("widget " + by.toString() + " is select-able ... returning", logWriter);
		  return true;
		  
	  }
	  
	  
	  private boolean isElementPresent(By by) {
		    try {
		      driver.findElement(by);
		      return true;
		    } catch (NoSuchElementException e) {
		      return false;
		    }
		  }
		  
		  private boolean isElementClickable (By by){
			  try{
					  driver.findElement(by).click();
			  }
			  catch(WebDriverException e){
					  return false;
			  }
			  return true;
		  }
		  
		  private boolean waitAndClick (By by) throws Exception{
			    //System.out.print("waiting for the following element to render so it can be clicked ");
			    //System.out.println(by.toString());
			    for (int second = 0;; second++) {
			    	if (second >= 30) return false;
			    	try { if (isElementClickable(by)) break; } catch (Exception e) {}
			    	Thread.sleep(1000);
			    }
			    return true;

		  }


	  private void waitForElementPresent (By by) throws Exception{
		    for (int second = 0;; second++) {
		    	if (second >= 30) fail("timeout - element not present: " + by.toString());
		    	try { if (isElementPresent(by)) break; } catch (Exception e) {}
		    	Thread.sleep(1000);
		    }
	  }

	  private void waitAndType (By by, String textToEnter) throws Exception{
		  waitForElementPresent (by);
		  waitAndClick(by);
		  driver.findElement(by).clear();
		  driver.findElement(by).sendKeys(textToEnter);
	  }

	  private boolean isTextPresent (String expectedText, By by, BufferedWriter logFileWriter) throws Exception{
		  try {
			  /*
			  	System.out.println("examining the element:");
			  	System.out.println("element name: " + driver.findElement(by).getAttribute("name"));
			  	System.out.println("element id: " + driver.findElement(by).getAttribute("title"));
			  	System.out.println("element value: " + driver.findElement(by).getAttribute("value"));
			  	System.out.println("element text: " + driver.findElement(by).getText());
			  	*/
		        assertEquals(expectedText, driver.findElement(by).getText());
		      } catch (Throwable e) {
		        //verificationErrors.append("Unexpected text: "+ e.toString());
		        writeToLogFile("Unexpected text: "+ e.toString(), logFileWriter);
		        return false;
		      }
		  writeToLogFile("Text is as expected: " + expectedText, logFileWriter);
		  return true;
	  }
	  
	  public boolean isAlertPresent(BufferedWriter logWriter) throws Exception
	  { 
	      try 
	      { 
	          driver.switchTo().alert(); 
	          writeToLogFile("Alert successfully found", logWriter);
	          return true; 
	      }   // try 
	      catch (NoAlertPresentException Ex) 
	      { 
	    	  writeToLogFile("Alert is not found - returning", logWriter);
	          return false; 
	      }   // catch 
	  }   // isAlertPresent()

}