package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.TreeSet;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

public class SalaryIncreasePercentagesDeptVsDOShareTest {
	protected String mySheetName;
	protected String testName;
	protected StringBuffer verificationErrors;
	protected BufferedWriter logFileWriter;
	protected String logFileName;
	protected String testEnv;
	protected String testCaseNumber;
	protected String testDescription;
	protected String logDirectoryName = ReportRunner.logDirectoryName;
	private HSSFExcelReader inputFile;
	private static final String inputFileSheetName = "DO vs. Dept Share";
	private HSSFExcelReader locators;
	private static final String locatorFileSheetName = "Control Sheet";
	private static final String notApplicable = "N/A";
	private static final String deptChair = "Chairs / Directors";
	private static final String srAssocDean = "Sr. Associate Deans";
	protected SSExcelReader myExcelReader;
	protected int rowForColumnNames;
	private boolean locatorsToOutputFile = false;
	private boolean ExcelReaderToOutputFile = false;
	
	private static final String dollarIncrease = "$ Increase";
	private static final String percentIncrease = "% Increase";
	private static final String percentAbovePool = "% Increase Above Pool";
	private static final String totalAmount = "Total Amount";
	private static final String prevYearCommitment = "Previous Year Commitments";
	private static final String apEvent = "Faculty Appointment Related";

	@Before
	public void setUp() throws Exception {
		myExcelReader = TestFileController.ControlSheetReportSuite;
		myExcelReader.setRowForColumnNames(TestFileController.ControlSheetDetailReportColumnHeaderRow);
		mySheetName = TestFileController.ControlSheetDetailReportWorksheetName;
		rowForColumnNames = TestFileController.ControlSheetDetailReportColumnHeaderRow;
		
		locators = TestFileController.locators;
		locators.setRowForColumnNames(0);
		
		inputFile = TestFileController.fileIn;
		inputFile.setRowForColumnNames(0);
		verificationErrors = new StringBuffer();
		testName = "SalaryIncreasePercentagesDeptVsDOShareTest";
		testEnv = "Test";
		testCaseNumber = "044";
		testDescription = "Salary Increase - Department vs. DO Shares Verification";
		logFileName = "Dept vs DO Shares Test.txt";
	}//end setUp() method

	@After
	public void tearDown() throws Exception {
		TestFileController.writeTestResults(testEnv, testCaseNumber, testName, testDescription, verificationErrors);
		logFileWriter.close();
		myExcelReader.closeWorkbook();
	}

	@Test
	public void test() throws Exception{
		  //this is additional setup so that testing can happen		
		  System.out.println("Now executing test: " + testName);
		  logFileName = ReportRunner.logDirectoryName +"/" + logFileName;
		  logFileWriter = TestFileController.getLogFileWriter(logFileName);
		  ControlSheetUI.switchToControlSheetTab();
		  //this is the actual testing
		  verifyAmounts(logFileWriter);
	}//end test() method
	
	  public void writeToLogFile(String toBeWritten, BufferedWriter logFileWriter) throws Exception{
		  logFileWriter.write(toBeWritten);
		  logFileWriter.newLine();
	  }//end writeToLogFile method
	  
	  private void verifyAmounts(BufferedWriter logFileWriter) throws Exception{
		  writeToLogFile("*** method verifyAmounts called ***", logFileWriter);
		  //first, test an overall failure condition
		  if (! verifyAllZeroDOAmountsForMarketPool(logFileWriter)){
			  String message = " Found nonzero amounts for DO Share where the Source Pool is Market Pool ";
			  writeToLogFile(message, logFileWriter);
			  verificationErrors.append(message);
		  }//end if - there's a nonzero amount for DO Share where the Source Pool is Market Pool
		  else writeToLogFile ("All amounts for DO Share are zero where the Source Pool is Market Pool, as expected.", logFileWriter);
		  /*
		   * We're going to extract the names from the input file,
		  and, for each name, read the Departments into a split() function, 
		  and run the determineCorrectFormula method on each unique name-department
		  combination
		  */
		  for(int rowNum=inputFile.getRowForColumnNames()+2; rowNum <=inputFile.getRowCount(inputFileSheetName); rowNum++){
			  String name = inputFile.getCellData(inputFileSheetName, "Faculty Name", rowNum);
			  String deptString = inputFile.getValueForColumnNameAndValue(inputFileSheetName, 
					  name, "Faculty Name", "Departments", logFileWriter, ExcelReaderToOutputFile);
			  writeToLogFile("Department String obtained from input file is the following: "
					  + deptString, logFileWriter);
			  String regex = ",\\s";
			  String depts[] = deptString.split(regex);
			  for (int i=0; i<depts.length; i++){
				  determineCorrectFormula (name, depts[i]);
				  logFileWriter.newLine();
			  }//end inner for loop - iterating through all departments per faculty member
		  }//end for loop - iterating through all the faculty
	  }//end verifyAmounts method
	  
	  /*
	   * Given the name of the Faculty member, read the information from the input file
	   * and use that information to determine which formula should be used.
	   * 
	   */
	  private void determineCorrectFormula(String name, String dept) throws Exception{
		  boolean hasRetentionOrRecruitment = hasRetentionOrRecruitment(name);
		  boolean marketPoolEligible = isMarketPoolEligible(name, logFileWriter);
		  boolean governance = isGovernance(name, logFileWriter);
		  String languageOfCommitments 
		  	= inputFile.getValueForColumnNameAndValue(inputFileSheetName, 
		  			name, "Faculty Name", "Language of Commitments", logFileWriter, ExcelReaderToOutputFile);
		  
		  writeToLogFile("*** method determineCorrectFormula called with faculty name " 
				  	+ name +" and department " + dept +" ***", logFileWriter);
		  //if this is Type VIII, calculate it and then exit this method
		  if (calculateTypeEightFormula(name, dept, logFileWriter)) return;

		  if (hasRetentionOrRecruitment){
			  writeToLogFile(name +" has Retention or Recruitment", logFileWriter);
			  if (governance){
				  writeToLogFile(name +" has a Governance-related increase", logFileWriter);
				  if (marketPoolEligible){
					  writeToLogFile(name +" is eligible for the Market Pool", logFileWriter);
					  calculateTypeFourFormula(name, dept, languageOfCommitments, logFileWriter);
				  }//end if - the faculty member is eligible for the Market Pool
				  else{
					  writeToLogFile(name +" is NOT eligible for the Market Pool", logFileWriter);
					  calculateTypeThreeFormula(name, dept, languageOfCommitments, logFileWriter);
				  }//end else - the faculty member is not eligible for the Market Pool
			  }//end if - it's Governance-related
			  else{
				  writeToLogFile(name +" has NO Governance-related increase", logFileWriter);
				  if (marketPoolEligible){
					  writeToLogFile(name +" is eligible for the Market Pool", logFileWriter);
					  calculateTypeTwoFormula(name, dept, languageOfCommitments, logFileWriter);
				  }//end if - the faculty member is eligible for the Market Pool
				  else{
					  writeToLogFile(name +" is NOT eligible for the Market Pool", logFileWriter);
					  calculateTypeOneFormula(name, dept, languageOfCommitments, logFileWriter);
				  }//end else - the faculty member is not eligible for the Market Pool
			  }//end else - it's not Governance-related
		  }//end if - there is either a Retention or a Recruitment
		  else{
			  writeToLogFile(name +" has no Retention or Recruitment", logFileWriter);
			  if (governance){
				  writeToLogFile(name +" has a Governance-related increase", logFileWriter);
				  if (marketPoolEligible){
					  writeToLogFile(name +" is eligible for the Market Pool", logFileWriter);
					  calculateTypeSixFormula(name, dept, logFileWriter);
				  }//end if - the faculty member is eligible for the Market Pool
				  else{
					  writeToLogFile(name +" is NOT eligible for the Market Pool", logFileWriter);
					  calculateTypeFiveFormula(name, dept, logFileWriter);
				  }//end else - the faculty member is not eligible for the Market Pool
			  }//end if - it's Governance-related
			  else{
				  writeToLogFile(name +" has NO Governance-related increase", logFileWriter);
				  calculateTypeSevenFormula(name, dept, logFileWriter);
			  }//end else - it's not Governance-related
		  }//end else - there is no Retention or Recruitment
		  
	  }//end determineCorrectFormula method

	  /*
	   * Type One Formula:
	   * Has Recruitment or Retention, No Governance, Merit Pool only (not Market Pool Qualified):
	   * 
	   * a.	If Language of Commitments is “% Increase”, look it up in the Control Sheet Detail Report 
	   * “% Increase Pct” column– that’s the Increase Percent)
	   * 
	   * (if Language of Commitments is “% Increase Above Pool”, look it up in the Control Sheet 
	   * Detail Report “% Increase Above Pool” column and add it to the % Raise value in 
	   * the “Department Faculty” row in the “Salary Increase Percentages and Amounts” section 
	   * of the “Control Sheet” tab of the Salary Setting UI – that’s the Increase Percent).
	   * 
	   * 	Salary Promise = Base Salary  * Increase Percent / 100
	   * 	Dept Share = Base Salary * Dept Increase Percent  / 100
	   * 	DO Amount = Salary Promise – Dept Share
	   * 
	   * b.	(if Language of Commitments is “$ Increase” or “Total Amount”, look up the 
	   * “100% FTE $ Increase Amt”)
	   * 
	   * 	Salary Promise = “100% FTE $ Increase Amt” * “Dept FTE” / 100
	   * 	Dept Share = (same as above)
	   * 	DO Amount = (same as above)
	   */

	  private void calculateTypeOneFormula(String name, String dept, String languageOfCommitments, 
			  BufferedWriter logFileWriter) throws Exception{
		  writeToLogFile (name + " has compensation calculated through the Type I Formula "
				  +"- Retention/Recruitment, No Governance, not Market Eligible", logFileWriter);
		  writeToLogFile("Language of Commitments is "+ languageOfCommitments, logFileWriter);

		  float baseSalary = getBaseSalary(name, dept, logFileWriter);
		  double salaryPromise = getAmountForFacultyAndLanguageOfCommitment(name, dept, languageOfCommitments, baseSalary, logFileWriter);
		  double expectedDeptAmount = 0;
		  double expectedDOAmount = 0;
		  double deptRaisePercent = getDepartmentFacultyRaisePercent(logFileWriter);
		  
		  writeToLogFile("Department Faculty Raise Percent is " + deptRaisePercent, logFileWriter);
		  expectedDeptAmount = baseSalary * deptRaisePercent;
		  writeToLogFile("Expected Department Share is " + expectedDeptAmount, logFileWriter);
		  expectedDOAmount = salaryPromise - expectedDeptAmount;
		  writeToLogFile("Expected DO Share is " + expectedDOAmount, logFileWriter);
		  String whichPool = "Regular Merit Pool";
		  compareDeptAmounts (expectedDeptAmount, name, dept, whichPool, logFileWriter);
		  compareDOAmounts (expectedDOAmount, name, dept, logFileWriter);
		  compareSalaryPromiseAmounts(salaryPromise, name, dept, logFileWriter);
	  }// end calculateTypeOneFormula
	  
	  /*
	   * Has Recruitment or Retention, No Governance, Market Pool Qualified: 
	   * 
	   * (if Language of Commitments is “% Increase or “% Increase Above Pool”, 
	   * calculate the Increase Percent the same way as mentioned above)
	   * 	Salary Promise = Base Salary * Increase Percent / 100
	   * 	Dept Share = Base Salary * Dept Increase Percent / 100
	   * 	Market Share = Base Salary * Market Pool Designated % / 100
	   * 	DO Amount = Salary Promise – Dept Share – Market Share
	   * 
	   * (if Language of Commitments is “$ Increase” or “Total Amount”, look up the 
	   * “100% FTE $ Increase Amt” and the “Dept FTE” in the Control Sheet Detail Report)
	   * 	Salary Promise = “100% FTE $ Increase Amt” * “Dept FTE” / 100
	   * 	Dept Share = (same as above)
	   * 	Market Share = (same as above)
	   * 	DO Amount = (same as above)
	   */

	  private void calculateTypeTwoFormula(String name, String dept, String languageOfCommitments, 
			  BufferedWriter logFileWriter) throws Exception{
		  writeToLogFile (name + " has compensation calculated through the Type II Formula "
				  +"- Retention/Recruitment, No Governance, Market Eligible", logFileWriter);
		  writeToLogFile("Language of Commitments is "+ languageOfCommitments, logFileWriter);
		  float baseSalary = getBaseSalary(name, dept, logFileWriter);
		  double salaryPromise = getAmountForFacultyAndLanguageOfCommitment(name, dept, languageOfCommitments, baseSalary, logFileWriter);
		  double marketShare = 0;
		  double expectedDeptAmount = 0;
		  double expectedDOAmount = 0;
		  double deptRaisePercent = getDepartmentFacultyRaisePercent(logFileWriter);
		  double marketPoolDesignatedPercent = getMarketPoolDesignatedPercent(name, dept, logFileWriter);
		  if (marketPoolDesignatedPercent == 0){
			  String message = "Could not find matching Market Pool Row for Formula Type II Faculty " + name;
			  writeToLogFile(message, logFileWriter);
			  verificationErrors.append(message);
		  }// end if - the faculty member is supposedly Market Qualified, but can't be found in a Designee row
		  else
			  writeToLogFile("Market Pool Designated Percent is " + marketPoolDesignatedPercent, logFileWriter);
		  marketShare = baseSalary * (marketPoolDesignatedPercent/100);
		  writeToLogFile("Department Designated Percent is " + deptRaisePercent, logFileWriter);
		  expectedDeptAmount = baseSalary * (deptRaisePercent);
		  expectedDOAmount = salaryPromise - marketShare - expectedDeptAmount;
		  writeToLogFile("DO Share is " + expectedDOAmount, logFileWriter);
		  
		  String whichPool = "Regular Merit Pool";
		  compareDeptAmounts (expectedDeptAmount, name, dept, whichPool, logFileWriter);
		  compareDOAmounts (expectedDOAmount, name, dept, logFileWriter);
	  
		  whichPool = "Market Pool";
		  compareDeptAmounts (marketShare, name, dept, whichPool, logFileWriter);
		  verifyLineItemShareEqualsCommitment(name, dept, whichPool, logFileWriter);
		  compareSalaryPromiseAmounts(salaryPromise, name, dept, logFileWriter);
	  }// end calculateTypeTwoFormula
	  
	  /*
	   * Has Recruitment or Retention, Governance, Merit Pool only (not Market Pool Qualified):
	   * 
	   * (if Language of Commitments is “% Increase”, look it up in the Control Sheet Detail Report 
	   * “% Increase Pct” column– that’s the Governance Increase %)
	   * 
	   * (if Language of Commitments is “% Increase Above Pool”, look it up in the 
	   * Control Sheet Detail Report “% Increase Above Pool” column and add it to the 
	   * % Raise value in the “Chairs / Directors” row or the “Sr. Associate Deans” 
	   * row in the “Salary Increase Percentages and Amounts” section of the “Control Sheet” tab 
	   * of the Salary Setting UI – that’s the Governance Increase %)
	   * 
	   * 	Salary Promise = DO Amount = Base Salary * Governance Increase % / 100
	   * 
	   *  (if Language of Commitments is “$ Increase” or “Total Amount”, look up the 
	   *  “100% FTE $ Increase Amt”)
	   *  
	   *  	Salary Promise = DO Amount = “100% FTE $ Increase Amt” * “Dept FTE” / 100
	   */

	  private void calculateTypeThreeFormula(String name, String dept, String languageOfCommitments, 
			  BufferedWriter logFileWriter) throws Exception{
		  writeToLogFile (name + " has compensation calculated through the Type III Formula "
				  +"- Retention/Recruitment, Governance, not Market Eligible", logFileWriter);
		  writeToLogFile("Language of Commitments is "+ languageOfCommitments, logFileWriter);

		  float baseSalary = getBaseSalary(name, dept, logFileWriter);
		  double salaryPromise = getAmountForFacultyAndLanguageOfCommitment(name, dept, languageOfCommitments, baseSalary, logFileWriter);
		  double expectedDeptAmount = 0;
		  double expectedDOAmount = salaryPromise;
		  
		  String whichPool = "Regular Merit Pool";
		  compareDeptAmounts (expectedDeptAmount, name, dept, whichPool, logFileWriter);
		  compareDOAmounts (expectedDOAmount, name, dept, logFileWriter);
		  compareSalaryPromiseAmounts(salaryPromise, name, dept, logFileWriter);
	  }// end calculateTypeThreeFormula
	  
	  /*
	   * Has Recruitment or Retention, Governance, Market Pool Qualified:

	   * a.	(if Language of Commitments is “% Increase”, look it up in the Control Sheet Detail 
	   Report “% Increase Pct” column – that’s the Governance Increase %)
			(if Language of Commitments is “% Increase Above Pool”, look it up in the 
		Control Sheet Detail Report “% Increase Above Pool” column and add it to the 
		% Raise value in the “Chairs / Directors” row or the “Sr. Associate Deans” 
		row in the “Salary Increase Percentages and Amounts” section of the “Control Sheet” 
		tab of the Salary Setting UI – that’s the Governance Increase %)
		
			Salary Promise = Base Salary * (Governance Increase % / 100) 
			Dept Amount = Base Salary * (Market Pool Designated % / 100)
			DO Amount – Salary Promise – Dept Amount
			
		b.	 (if Language of Commitments is “$ Increase” or “Total Amount”, look up the 
		“100% FTE $ Increase Amt”, multiply it by the “Dept FTE” value, divide it by 100, 
		and that is the Salary Promise.  Once that is calculated, do the following:
		
			Salary Promise = “100% FTE $ Increase Amt” * (“Dept FTE” / 100)
			Dept Amount = (same as above)
			DO Amount = (same as above)
	   */
	  private void calculateTypeFourFormula(String name, String dept, String languageOfCommitments, 
			  BufferedWriter logFileWriter) throws Exception{
		  writeToLogFile (name + " has compensation calculated through the Type IV Formula "
				  	+ " - Retention/Recruitment, Governance, Market Eligible", logFileWriter);
		  writeToLogFile("Language of Commitments is "+ languageOfCommitments, logFileWriter);
		  //float governanceIncrease = 0;
		  float baseSalary = getBaseSalary(name, dept, logFileWriter);
		  double salaryPromise = getAmountForFacultyAndLanguageOfCommitment(name, dept, languageOfCommitments, baseSalary, logFileWriter);
		  double expectedDeptAmount = 0;
		  double expectedDOAmount = 0;
		  double marketPoolDesignatedPercent = getMarketPoolDesignatedPercent(name, dept, logFileWriter);
		  if (marketPoolDesignatedPercent == 0){
			  String message = "Could not find matching Market Pool Row for Formula Type IV Faculty "+ name;
			  writeToLogFile(message, logFileWriter);
			  verificationErrors.append(message);
		  }// end if - the faculty member is supposedly Market Qualified, but can't be found in a Designee row
		  else
			  writeToLogFile("Market Pool Designated Percent is " + marketPoolDesignatedPercent, logFileWriter);
		  expectedDeptAmount = baseSalary * (marketPoolDesignatedPercent/100);
		  writeToLogFile("Department Amount is determined to be "+expectedDeptAmount, logFileWriter);
		  expectedDOAmount = salaryPromise - expectedDeptAmount;
		  writeToLogFile ("DO Amount is determined to be " + expectedDOAmount, logFileWriter);
		  compareSalaryPromiseAmounts(salaryPromise, name, dept, logFileWriter);
		  String whichPool = "Market Pool";
		  compareDeptAmounts (expectedDeptAmount, name, dept, whichPool, logFileWriter);
		  verifyLineItemShareEqualsCommitment (name, dept, whichPool, logFileWriter);
		  //the DO Share for any line in which the Source Pool is "Market Pool" should be zero.
		  //This was tested already by another method.
		  whichPool = "Regular Merit Pool";
		  compareDeptAmounts (0, name, dept, whichPool, logFileWriter);
		  compareDOAmounts (expectedDOAmount, name, dept, logFileWriter);
		  verifyLineItemShareEqualsCommitment (name, dept, whichPool, logFileWriter);
		  compareSalaryPromiseAmounts(salaryPromise, name, dept, logFileWriter);
		}// end calculateTypeFourFormula
	  
		/*
		 * No Recruitment or Retention, Governance, Merit Pool only (not Market Pool Qualified):  
		 * (The Governance Increase is the % Raise value in the “Chairs / Directors” row 
		 * or the “Sr. Associate Deans” row in the “Salary Increase Percentages and Amounts” 
		 * section of the “Control Sheet” tab of the Salary Setting UI 
		 * – that’s the Governance Increase %)
		 * 
		 * DO Amount = (Base Salary * Governance Increase % / 100)
		 * Salary Promise = DO Amount
		 * 
		 */
	private void calculateTypeFiveFormula(String name, String dept, 
				BufferedWriter logFileWriter) throws Exception{
		  calculateTypeFiveFormula(name, dept, logFileWriter, false);
	}

	private void calculateTypeFiveFormula(String name, String dept, 
			BufferedWriter logFileWriter, boolean isActuallyTypeSix) throws Exception{
		  if (! isActuallyTypeSix){
			  writeToLogFile (name + " has compensation calculated through the Type V Formula "
					  	+ " - No Retention/Recruitment, Governance, Not Market Eligible", logFileWriter);
			  writeToLogFile("Language of Commitments is irrelevant", logFileWriter);
		  }
		  //get the DO Amount by multiplying the Governance% by the base salary
		  float baseSalary = getBaseSalary(name, dept, logFileWriter);
		  double expectedDeptAmount = 0;
		  double expectedDOAmount = getAmountForFacultyAndLanguageOfCommitment(name, dept, percentAbovePool, baseSalary, logFileWriter);
		  double marketPoolDesignatedPercent = getMarketPoolDesignatedPercent(name, dept, logFileWriter);
		  //if the outputIdentifyingMethod boolean value is false, this is actually a Type VI faculty member
		  if (marketPoolDesignatedPercent == 0 && isActuallyTypeSix){
			  String message = "Could not find matching Market Pool Row for Formula Type VI Faculty " + name;
			  writeToLogFile(message, logFileWriter);
			  verificationErrors.append(message);
		  }// end if - the faculty member is supposedly Market Qualified, but can't be found in a Designee row
		  else if (isActuallyTypeSix)
			  writeToLogFile("Market Pool Designated Percent is " + marketPoolDesignatedPercent, logFileWriter);
		  else if (! isActuallyTypeSix && (marketPoolDesignatedPercent == 0))
			  writeToLogFile("Market Pool Designated Percent is zero, as expected for Type V Faculty", logFileWriter);
		  else {//this is a Formula Type V Faculty member and designated for the Market Pool
			  String message = "There is a matching Market Pool Row for Formula Type V Faculty " + name;
			  writeToLogFile(message, logFileWriter);
			  verificationErrors.append(message);
		  }
		  double expectedSalaryPromiseAmount = expectedDeptAmount + expectedDOAmount;
		  String whichPool = "Regular Merit Pool";
		  compareDeptAmounts (expectedDeptAmount, name, dept, whichPool, logFileWriter);
		  compareDOAmounts (expectedDOAmount, name, dept, logFileWriter);
		  compareSalaryPromiseAmounts(expectedSalaryPromiseAmount, name, dept, logFileWriter);
	  }// end calculateTypeFiveFormula

	/*
	 * VI.	No Recruitment or Retention, Governance, Market Qualified:
	 * 		DO Amount = (same as above)
	 * 		Salary Promise = DO Amount (same as above)
	 * 
	 */
	  private void calculateTypeSixFormula(String name, String dept, 
			  BufferedWriter logFileWriter) throws Exception{
		  writeToLogFile (name + " has compensation calculated through the Type VI Formula "
				  	+ " - No Retention/Recruitment, Governance, Market Eligible", logFileWriter);
		  writeToLogFile("This is calculated the same way as Type V Formula", logFileWriter);
		  calculateTypeFiveFormula(name, dept, logFileWriter, true);
	  }// end calculateTypeSixFormula

	  /*
	   * VII.	No Recruitment or Retention, No Governance, Market Qualified or not 
	   * 		Such Faculty members will not appear on the Control Sheet Detail Report.
	   * 
	   * The purpose of this test is to verify that we cannot find the combination of the faculty
	   * member and the department in the Control Sheet Detail Report.
	   */
	  private void calculateTypeSevenFormula(String name, String dept, 
			  BufferedWriter logFileWriter) throws Exception{
		  writeToLogFile (name + " has compensation calculated through the Type VII Formula "
				  	+ " - No Retention/Recruitment, No Governance, Market Eligible or Not Market Eligible", logFileWriter);
		  writeToLogFile("Language of Commitments is irrelevant "
				  	+"- faculty should not be in the Control Sheet Detail Report", logFileWriter);
		  int rowNum = myExcelReader.getCellRowNum(mySheetName, "Name", name);
		  if (rowNum != -1){
			  String actualDept = myExcelReader.getCellData(mySheetName, "Department", rowNum);
			  if (actualDept.equalsIgnoreCase(dept)){
				  String message = "Faculty-Department combination was found in the report but shouldn't be";
				  writeToLogFile(message, logFileWriter);
				  verificationErrors.append(message);
			  }//we found the name-department combination in the Control Sheet Detail Report - that's a failure
			  else{
				  writeToLogFile("Faculty member was found in the report but the department wasn't - this is acceptable", logFileWriter);
			  }//end else - could find the name but not the department - that is acceptable
		  }
		  else{
			  writeToLogFile("Faculty name was not found in the Control Sheet Detail Report - this is acceptable", logFileWriter);
		  }//end else - could not find the Faculty Name - that is acceptable
		  
	  }// end calculateTypeSevenFormula

	  
	  private boolean hasRetentionOrRecruitment(String name) throws Exception{
		  int rowNum = inputFile.getCellRowNum(inputFileSheetName, "Faculty Name", name);
		  boolean hasNoRetention 
		  	= inputFile.getCellData(inputFileSheetName, "Retention ID", rowNum).equalsIgnoreCase(notApplicable);
		  boolean hasNoRecruitment
		  	= inputFile.getCellData(inputFileSheetName, "Recruitment ID", rowNum).equalsIgnoreCase(notApplicable);
		  return ! (hasNoRetention && hasNoRecruitment);
	  }//end hasRetentionOrRecruitment method
	  
	  private boolean isMarketPoolEligible(String name, BufferedWriter logFileWriter) throws Exception{
		  return inputFile.getValueForColumnNameAndValue(inputFileSheetName, 
				  name, "Faculty Name", "Market Pool eligible", logFileWriter, ExcelReaderToOutputFile).equalsIgnoreCase("Yes");
	  }//end isMarketPoolEligible method
	  
	  private boolean isGovernance(String name, BufferedWriter logFileWriter) throws Exception{
		  String governanceString = inputFile.getValueForColumnNameAndValue(inputFileSheetName, 
				  name, "Faculty Name", "Governance", logFileWriter, ExcelReaderToOutputFile);
		  return governanceString.equalsIgnoreCase(deptChair) || governanceString.equalsIgnoreCase(srAssocDean);
	  }//end isMarketPoolEligible method
	  
	  private double getAmountForFacultyAndLanguageOfCommitment(String name, String dept, String languageOfCommitments, 
			  float baseSalary, BufferedWriter logFileWriter) throws Exception{
		  writeToLogFile("*** method getAmountForFacultyAndLanguageOfCommitment called ***", logFileWriter);
		  float increasePercent = 0;
		  double salaryPromise = 0;
		  writeToLogFile("Base Salary is at " + baseSalary, logFileWriter);
		  
		  if (languageOfCommitments.equalsIgnoreCase(percentIncrease)){
			  increasePercent = getPercentIncreaseForFaculty(name, dept, logFileWriter);
			  writeToLogFile("Percent Increase Found is " +increasePercent, logFileWriter);
			  salaryPromise = baseSalary * increasePercent;
		  }//end if - Percent Increase
		  else if (languageOfCommitments.equalsIgnoreCase(percentAbovePool)){
			  increasePercent = getPercentIncreaseAbovePool(name, dept, logFileWriter);
			  writeToLogFile("Percent Increase Above Pool is " +increasePercent, logFileWriter);
			  String typeOfRaise = inputFile.getValueForColumnNameAndValue(inputFileSheetName, name, 
					  "Faculty Name", "Governance", logFileWriter, ExcelReaderToOutputFile);
			  increasePercent += ControlSheetUI.getSalaryIncreasePercentFromUI(typeOfRaise, logFileWriter);
			  writeToLogFile("Combined Percent Increase Found is " +increasePercent, logFileWriter);
			  salaryPromise = baseSalary * increasePercent;
		  }//end else if - percent above Pool
		  else if (languageOfCommitments.equalsIgnoreCase(dollarIncrease) ||
				  			(languageOfCommitments.equalsIgnoreCase(totalAmount))){
			  salaryPromise = getFTEAdjustedDollarIncrease(name, dept, logFileWriter);
			  writeToLogFile("Salary Promise is " + salaryPromise, logFileWriter);
		  }//end else if - dollar increase or total amount
		  return salaryPromise;
	  }
	  
	  private void compareDOAmounts (double expectedDOAmount, String name, String dept, 
			  BufferedWriter logFileWriter) throws Exception{
		  double actualDOAmount = getActualDOAmount (name, dept, logFileWriter);
		  logFileWriter.newLine();
		  if (! isApproximatelyEqual(expectedDOAmount, actualDOAmount)){
			  String message = "MISMATCH: Expected a DO amount of " + expectedDOAmount
					  	+", but actual amount is " + actualDOAmount;
			  writeToLogFile(message, logFileWriter);
			  verificationErrors.append(message);
		  }
		  else writeToLogFile("Actual DO amount is as expected", logFileWriter);
	  }//end compareDOAmounts method

	  private void compareDeptAmounts(double expectedDeptAmount, String name, String dept,
			  String whichPool, BufferedWriter logFileWriter) throws Exception{
		  compareDeptAmounts(expectedDeptAmount, name, dept, whichPool, new String(), logFileWriter);
	  }

	  private void compareDeptAmounts(double expectedDeptAmount, String name, String dept,
			  String whichPool, String typeEightCategory, BufferedWriter logFileWriter) throws Exception{
		  double actualDeptAmount = getActualDeptAmount(name, dept, whichPool, typeEightCategory, logFileWriter);
		  logFileWriter.newLine();
		  if (! isApproximatelyEqual(expectedDeptAmount, actualDeptAmount)){
			  String message = "MISMATCH: Expected a dept amount of " + expectedDeptAmount
					  	+", but actual amount is " + actualDeptAmount;
			  writeToLogFile(message, logFileWriter);
			  verificationErrors.append(message);
		  }
		  else writeToLogFile("Actual dept amount is as expected", logFileWriter);
	  }//end compareDeptAmounts method

	  private void compareSalaryPromiseAmounts(double expectedSalaryPromiseAmount, String name, String dept,
			  BufferedWriter logFileWriter) throws Exception{
		  double actualSalaryPromiseAmount = getFTEAdjustedDollarIncrease(name, dept, logFileWriter);
		  logFileWriter.newLine();
		  if (! isApproximatelyEqual(expectedSalaryPromiseAmount, actualSalaryPromiseAmount)){
			  String message = "MISMATCH: Expected a Salary Promise amount of " + expectedSalaryPromiseAmount
					  	+", but actual amount is " + actualSalaryPromiseAmount;
			  writeToLogFile(message, logFileWriter);
			  verificationErrors.append(message);
		  }
		  else writeToLogFile("Actual Salary Promise amount is as expected", logFileWriter);
	  }//end compareDeptAmounts method

	  private boolean isApproximatelyEqual(double expected, double actual){
		  return ((expected > actual - 2) && (expected < actual + 2));
	  }
	  

	  private double getActualDOAmount(String name, String dept, BufferedWriter logFileWriter) throws Exception{
		  return getActualDOAmount(name, dept, logFileWriter, new String());
	  }

	  private double getActualDOAmount(String name, String dept, BufferedWriter logFileWriter, String TypeEightCategory) throws Exception{
		  HashMap<String, String> criteria = new HashMap<String, String>();
		  criteria.put("Name", name);
		  criteria.put("Department", dept);
		  criteria.put("Source Pool", "Regular Merit Pool");
		  criteria.put("Stage", "Known");
		  if (! TypeEightCategory.isEmpty())
			  criteria.put("Category", TypeEightCategory);
		  String[] columns = new String[1];
		  columns[0] = "FTE Adjusted DO Share Amount";

		  String actualDeptStringAmt = myExcelReader.getSingleFacultyMultipleValues(mySheetName, criteria, columns, logFileWriter)[0];
		  double d = 0;
		  try{
			  d = ((Number)NumberFormat.getNumberInstance(java.util.Locale.US).parse(actualDeptStringAmt)).doubleValue();
		  }
		  catch (Exception e){
			  writeToLogFile("Exception thrown: "+e.getMessage(), logFileWriter);
		  }
		  return d;
	  }//end getActualDOAmount method

	  private double getActualDeptAmount(String name, String dept, String whichPool, BufferedWriter logFileWriter) throws Exception{
		  return getActualDeptAmount(name, dept, whichPool, new String(), logFileWriter);
	  }

	  private double getActualDeptAmount(String name, String dept, String whichPool, String TypeEightCategory, BufferedWriter logFileWriter) throws Exception{
		  HashMap<String, String> criteria = new HashMap<String, String>();
		  criteria.put("Name", name);
		  criteria.put("Department", dept);
		  criteria.put("Source Pool", whichPool);
		  criteria.put("Stage", "Known");
		  if (! TypeEightCategory.isEmpty())
			  criteria.put("Category", TypeEightCategory);
		  String[] columns = new String[1];
		  columns[0] = "FTE Adjusted Department Share Amount";

		  String actualDeptStringAmt = myExcelReader.getSingleFacultyMultipleValues(mySheetName, criteria, columns, logFileWriter)[0];
		  double d = 0;
		  try{
			  d = ((Number)NumberFormat.getNumberInstance(java.util.Locale.US).parse(actualDeptStringAmt)).doubleValue();
		  }
		  catch (Exception e){
			  writeToLogFile("Exception thrown: "+e.getMessage(), logFileWriter);
		  }
		  return d;
	  }//end getActualDOAmount method

	  
	  private double getDepartmentFacultyRaisePercent (BufferedWriter logFileWriter) throws Exception{
		  writeToLogFile("*** method getDepartmentFacultyRaisePercent called ***", logFileWriter);
		  String locator = locators.getValueForColumnNameAndValue(locatorFileSheetName, 
				  "DepartmentFacultyPercentageAmount", "Name", "Locator", logFileWriter, ExcelReaderToOutputFile);
		  String rowPercent = ControlSheetUI.getValueOfInputForLocator(locator, logFileWriter, locatorsToOutputFile);
		  //verify that the rowPercent retrieved is a valid value
		  if (rowPercent.length() == 0){
			  writeToLogFile("Could not get a valid value for Department Faculty Raise Percent - returning zero",
					  logFileWriter);
			  return 0;
		  }//end if - the rowPercent length is not a valid value
		  try{
			  return ((Number)NumberFormat.getNumberInstance(java.util.Locale.US).parse(rowPercent)).doubleValue()/100;
		  }//end try
		  catch(ParseException e){
			  verificationErrors.append(e.getMessage());
			  writeToLogFile("Could not parse the value retrieved for Department Faculty Raise Percent", logFileWriter);
			  writeToLogFile(e.getMessage(), logFileWriter);
			  return 0;
		  }
	  }//end getDepartmentFacultyRaisePercent method 

	  private float getBaseSalary (String name, String dept,
				BufferedWriter logFileWriter) throws Exception{
		  writeToLogFile("*** method getBaseSalary called ***", logFileWriter);
		  HashMap<String, String> criteria = new HashMap<String, String>();
		  criteria.put("Name", name);
		  criteria.put("Department", dept);
		  String percentString = myExcelReader.getDiscreteOrderedListOfValues(mySheetName, 
				  criteria, "Current FTE Adj Salary", logFileWriter).first();
		  return Float.valueOf((Long) NumberFormat.getNumberInstance(java.util.Locale.US).parse(percentString));
	  }//end getBaseSalary method 

	  //this needs to be adjusted so that it will still return true if it finds a non-faculty line with a nonzero DO Share
	  //Example: If the "Category" is either "Transfers" or "Reserves", then nonzero DO Shares for Market Pool are OK
	  private boolean verifyAllZeroDOAmountsForMarketPool(BufferedWriter logFileWriter) throws Exception{
		  writeToLogFile("*** method verifyAllZeroDOAmountsForMarketPool called ***", logFileWriter);
		  //first, we total up all of the amounts
		  HashMap<String, String> criteria = new HashMap<String, String>();
		  criteria.put("Source Pool", "Market Pool");
		  TreeSet<String> AllAmounts = myExcelReader.getDiscreteOrderedListOfValues(mySheetName, 
				  criteria, "FTE Adjusted DO Share Amount", logFileWriter);		  
		  if (AllAmounts.size() != 1) {
			  //now we total up the Exceptions 
			  String[] category = {"Reserves", "Transfers"};
			  for (int i=0; i<category.length; i++) {
				  criteria.put("Category", category[i]);
				  TreeSet<String> exceptionAmounts = myExcelReader.getDiscreteOrderedListOfValues(mySheetName, 
						  criteria, "FTE Adjusted DO Share Amount", logFileWriter);
				  if (exceptionAmounts.size() != 1) {
					  //iterate through AllAmounts and remove any entries which are Reserves
					  Iterator<String> allAmountsIterator = AllAmounts.iterator();
					  Iterator<String> exceptionAmountsIterator = exceptionAmounts.iterator();
					  while (allAmountsIterator.hasNext()) {
						  String amount = allAmountsIterator.next();
						  while (exceptionAmountsIterator.hasNext()) {
							  if ((amount.equalsIgnoreCase(exceptionAmountsIterator.next())) && (! amount.equalsIgnoreCase("0"))){
								  AllAmounts.remove(amount);
								  logFileWriter.write(amount+" does not count, as it is one of the "+category);
								  logFileWriter.newLine();
							  }//remove it if it is nonzero
						  }
					  }//end while
				  }//end inner if
				  criteria.remove("Category");
			  }//end for loop
		  }//end outer if
		  if ((AllAmounts.size() != 1) && (! AllAmounts.first().equalsIgnoreCase("0"))) return false;
		  else return true;
	  }
	  
	  private float getPercentIncreaseAbovePool (String name, String dept,
				BufferedWriter logFileWriter) throws Exception{
		  writeToLogFile("*** method getPercentIncreaseAbovePool called ***", logFileWriter);
		  HashMap<String, String> criteria = new HashMap<String, String>();
		  criteria.put("Name", name);
		  criteria.put("Department", dept);
		  String percentString = myExcelReader.getDiscreteOrderedListOfValues(mySheetName, 
				  criteria, "% Increase Above Pool", logFileWriter).first();
		  return ((Number)NumberFormat.getNumberInstance(java.util.Locale.US).parse(percentString)).floatValue()/100;
	  }//end getPercentIncreaseAbovePool method

	  private float getPercentIncreaseForFaculty (String name, String dept,
			BufferedWriter logFileWriter) throws Exception{
		  writeToLogFile("*** method getPercentIncreaseForFaculty called ***", logFileWriter);
		  HashMap<String, String> criteria = new HashMap<String, String>();
		  criteria.put("Name", name);
		  criteria.put("Department", dept);
		  String percentString = myExcelReader.getDiscreteOrderedListOfValues(mySheetName, 
				  criteria, "% Increase Pct", logFileWriter).first();
		  return ((Number)NumberFormat.getNumberInstance(java.util.Locale.US).parse(percentString)).floatValue()/100;
	  }//end getPercentIncreaseForFaculty method
	  
	  private double getMarketPoolDesignatedPercent(String name, String dept, 
			  BufferedWriter logFileWriter) throws Exception{
		  writeToLogFile("*** method getMarketPoolDesignatedPercent called ***", logFileWriter);
		  //we have to get the rank and (perhaps) the gender to look up the Faculty member successfully
		  String rank = getRankForFaculty (name, dept, logFileWriter);
		  int rowNum = inputFile.getCellRowNum(inputFileSheetName, "Faculty Name", name);
		  String gender = inputFile.getCellData(inputFileSheetName, "Gender", rowNum);
		  writeToLogFile("Derived rank is " + rank, logFileWriter);
		  writeToLogFile("Derived gender is " + gender, logFileWriter);
		  int[] deptMatches = getDepartmentMatches(dept, logFileWriter);
		  int[] rankMatches = getRankMatches(rank, logFileWriter);
		  int[] genderMatches = getGenderMatches(gender, logFileWriter);
		  int matchingRow = getMatchingRow(deptMatches, rankMatches, genderMatches, logFileWriter);
		  if (matchingRow != -1)
			  writeToLogFile("Common row found is "+matchingRow, logFileWriter);
		  else writeToLogFile ("No common row found", logFileWriter);
		  double rowPercent = 0;
		  if (matchingRow == -1){
			  writeToLogFile("Could not find matching Designee row - returning zero percent"
					  , logFileWriter);
			  //rowPercent = getGeneralMarketPoolRaisePercent(logFileWriter);
		  }//end if - there was no Designee Row that matches
		  else
			  rowPercent = getDesigneeRowPercent(matchingRow, logFileWriter);
		  writeToLogFile("Got a row percent value of " + rowPercent, logFileWriter);
		  return rowPercent;
	  }//end getMarketPoolDesignatedPercent method
	  /*
	  private float getGeneralMarketPoolRaisePercent(BufferedWriter logFileWriter) throws Exception{
		  writeToLogFile("*** method getGeneralMarketPoolRaisePercent called ***", logFileWriter);
		  String locator = locators.getValueForColumnNameAndValue(locatorFileSheetName, 
				  "MarketPoolPercentageAmount", "Name", "Locator", logFileWriter, ExcelReaderToOutputFile);
		  String rowPercent = ControlSheetUI.getValueOfInputForLocator(locator, logFileWriter, locatorsToOutputFile);
		  if (rowPercent.length() == 0){
			  writeToLogFile("Could not get a valid value for General Market Pool Raise Percent - returning zero",
					  logFileWriter);
			  return 0;
		  }
		  return ((Number)NumberFormat.getNumberInstance(java.util.Locale.US).parse(rowPercent)).floatValue();
	  }//end getGeneralMarketPoolRaisePercent method
	  */
	  private double getDesigneeRowPercent(int rowNum, BufferedWriter logFileWriter) throws Exception{
		  writeToLogFile("*** method getDesigneeRowPercent called with row number "
				  	+ rowNum +"****", logFileWriter);
		  String locator = locators.getValueForColumnNameAndValue(locatorFileSheetName, 
				  "MarketPoolPercentSalaryBaseColumnBase", "Name", "Locator", logFileWriter, ExcelReaderToOutputFile);
		  if (rowNum > 1){
			  locator = locator.replaceFirst("tr", "tr[" + rowNum + "]");
		  }//end if - adjusting for the row number
		  String rowPercent = ControlSheetUI.getValueOfInputForLocator(locator, logFileWriter, locatorsToOutputFile);
		  if (rowPercent.length() == 0){
			  writeToLogFile("Could not get a valid value for Designee Row Percent - returning zero",
					  logFileWriter);
			  return 0;
		  }
		  writeToLogFile("Row Percent value found is "+rowPercent, logFileWriter);
		  double d = 0;
		  try{
			  d = ((Number)NumberFormat.getNumberInstance(java.util.Locale.US).parse(rowPercent)).doubleValue();
		  }
		  catch (Exception e){
			  writeToLogFile("Exception thrown: "+e.getMessage(), logFileWriter);
		  }
		  return d;
	  }//end getDesigneeRowPercent method
	  
	  private int[] getDepartmentMatches(String dept, BufferedWriter logFileWriter) throws Exception{
		  writeToLogFile("*** method getDepartmentMatches called ***", logFileWriter);
		  String marketPoolDepartmentColumnBase = locators.getValueForColumnNameAndValue(locatorFileSheetName, 
				  "MarketPoolDepartmentColumnBase", "Name", "Locator", logFileWriter, ExcelReaderToOutputFile);
		  writeToLogFile("Derived Market Pool Department Column Base locator is " 
				  + marketPoolDepartmentColumnBase, logFileWriter);
		  return getMatchingMarketPoolRows(marketPoolDepartmentColumnBase, dept, logFileWriter);
	  }//end getDepartmentMatches method
	  
	  private int[] getRankMatches(String rank, BufferedWriter logFileWriter) throws Exception{
		  writeToLogFile("*** method getRankMatches called ***", logFileWriter);
		  String marketPoolRankColumnBase = locators.getValueForColumnNameAndValue(locatorFileSheetName, 
				  "MarketPoolRankColumnBase", "Name", "Locator", logFileWriter, ExcelReaderToOutputFile);
		  writeToLogFile("Derived Market Pool Rank Column Base locator is " 
				  + marketPoolRankColumnBase, logFileWriter);
		  return getMatchingMarketPoolRows(marketPoolRankColumnBase, rank, logFileWriter);
	  }//end getRankMatches method

	  private int[] getGenderMatches(String gender, BufferedWriter logFileWriter) throws Exception{
		  writeToLogFile("*** method getGenderMatches called ***", logFileWriter);
		  String marketPoolGenderColumnBase = locators.getValueForColumnNameAndValue(locatorFileSheetName, 
				  "MarketPoolGenderColumnBase", "Name", "Locator", logFileWriter, ExcelReaderToOutputFile);
		  writeToLogFile("Derived Market Pool Rank Column Base locator is " 
				  + marketPoolGenderColumnBase, logFileWriter);
		  return getMatchingMarketPoolRows(marketPoolGenderColumnBase, gender, logFileWriter);
	  }//end getGenderMatches method
	  
	  private int getMatchingRow(int[] deptMatches, int[] rankMatches, int[] genderMatches,
			  	BufferedWriter logFileWriter) throws Exception{
		  int matchingRow = -1;
		  writeToLogFile("*** method getMatchingRow called ***", logFileWriter);
		  for(int i=0; i<deptMatches.length; i++){
			  for(int j=0; j<rankMatches.length; j++){
				  for(int k=0; k<genderMatches.length; k++){
					  if (deptMatches[i]==rankMatches[j] && rankMatches[j]==genderMatches[k])
						  return deptMatches[i];
				  }//end inner-inner for; iterating through genderMatches and finding the matching row
			  }//end inner for - iterating through rankMatches
		  }//end outer for - iterating through deptMatches
		  
		  return matchingRow;//this will return -1 if it iterates all the way through and doesn't find a match
	  }//end getMatchingRow method


	  private int[] getMatchingMarketPoolRows(String columnBase, String target,
			  BufferedWriter logFileWriter) throws Exception{
		  writeToLogFile("*** method getMatchingMarketPoolRows called ***", logFileWriter);
		  TreeSet<Integer> treeSetIndices = new TreeSet<Integer>();
		  String columnLocator = columnBase;
		  int rowNum = 1;
		  while (ControlSheetUI.isElementPresent(By.xpath(columnLocator))){
			  String found = ControlSheetUI.getStringValueForLocator(columnLocator, logFileWriter, locatorsToOutputFile);
			  if (locatorsToOutputFile)
				  writeToLogFile("found value " + found +" for locator "+columnLocator, logFileWriter);
			  if (found.equalsIgnoreCase(target) || found.equalsIgnoreCase("Any")) 
				  treeSetIndices.add(new Integer(rowNum));
			  rowNum++;
			  //writeToLogFile("row number updated to "+rowNum, logFileWriter);
			  
			  if (rowNum == 2){
				  columnLocator = columnLocator.replaceFirst("tr", "tr["+rowNum+"]");
			  }
			  else {
				  String prevRow = "tr\\[" + Integer.toString(rowNum - 1) +"\\]";
				  //writeToLogFile("Previous Row locator value is " + prevRow, logFileWriter);
				  String currRow = "tr[" + Integer.toString(rowNum)+"]";
				  //writeToLogFile("Current Row locator value is " + currRow, logFileWriter);
				  columnLocator = columnLocator.replaceFirst(prevRow, currRow);
			  }
			  //writeToLogFile("column locator updated to "+columnLocator, logFileWriter);
		  }//end while loop
		  int[] intArray = new int[treeSetIndices.size()];
		  Iterator<Integer> IntegerIterator = treeSetIndices.iterator();
		  int i = 0;
		  while (IntegerIterator.hasNext()){
			  intArray[i++] = IntegerIterator.next().intValue();
		  }
		  writeToLogFile("method getMatchingMarketPoolRows returning the following indices: ", logFileWriter);
		  for(i=0; i<intArray.length; i++){
			  logFileWriter.write(intArray[i] + ", ");
		  }
		  logFileWriter.newLine();
		  return intArray;
	  }
	  
	  
	  private String getRankForFaculty(String name, String dept, 
			  BufferedWriter logFileWriter) throws Exception{
		  writeToLogFile("*** method getRankForFaculty called ***", logFileWriter);
		  int rowNum = myExcelReader.getCellRowNum(mySheetName, "Name", name);
		  return myExcelReader.getCellData(mySheetName, "Rank", rowNum);
	  }//end getRankForFaculty method

	  private double getFTEAdjustedDollarIncrease (String name, String dept,
				BufferedWriter logFileWriter) throws Exception{
		  return getFTEAdjustedDollarIncrease(name, dept, new String(), logFileWriter);
	  }

	  private double getFTEAdjustedDollarIncrease (String name, String dept, String TypeEightCategory,
				BufferedWriter logFileWriter) throws Exception{
		  writeToLogFile("*** method getFTEAdjustedDollarIncrease called ***", logFileWriter);
		  HashMap<String, String> criteria = new HashMap<String, String>();
		  criteria.put("Name", name);
		  criteria.put("Department", dept);
		  criteria.put("Stage", "Known");
		  if (! TypeEightCategory.isEmpty())
			  	criteria.put("Category", TypeEightCategory);
		  String[] columnNames = {"FTE Adj Total Commitment"};
		  String minDateStr = "09/01/2015"; 
		  String maxDateStr = "08/31/2018";
		  Date minDate = new Date();
		  Date maxDate = new Date();
		  try{
			  DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy"); 
			  minDate = (Date)formatter.parse(minDateStr); 
			  maxDate = (Date)formatter.parse(maxDateStr); 
		  }
		  catch (ParseException e){
			  verificationErrors.append(e.getStackTrace().toString());
		  }
		  BigDecimal dollarIncrease = myExcelReader.getColumnSummaryValues(mySheetName, 
				  criteria, false, false, false, columnNames, minDate, maxDate, logFileWriter)[1];
		  double dollarIncreaseAmt = dollarIncrease.doubleValue();
		  return dollarIncreaseAmt;
	  }//end getFTEAdjustedDollarIncrease method

	  private boolean calculateTypeEightFormula(String name, String dept, 
			  BufferedWriter logFileWriter) throws Exception{
		  writeToLogFile("*** method calculateTypeEightFormula called ***", logFileWriter);
		  String category = new String();
		  boolean isTypeEightFormula = false;
		  String prevYearCommitmentType = notApplicable;
		  String apEventType = notApplicable;
		  int rowNum = inputFile.getCellRowNum(inputFileSheetName, "Faculty Name", name);
		  prevYearCommitmentType = inputFile.getCellData(inputFileSheetName, prevYearCommitment, rowNum);
		  apEventType = inputFile.getCellData(inputFileSheetName, apEvent, rowNum);
		  boolean isNotPrevYearCommitment = prevYearCommitmentType.equalsIgnoreCase(notApplicable);
		  boolean isNotAPEvent = apEventType.equalsIgnoreCase(notApplicable);
		  isTypeEightFormula = ! (isNotPrevYearCommitment && isNotAPEvent);
		  if (! isTypeEightFormula) return false;
		  double expectedDeptAmount = 0;
		  String whichPool = "Regular Merit Pool";
		  if (! isNotPrevYearCommitment ){
			  category = prevYearCommitment;
			  writeToLogFile(name + " has compensation calculated through the Type VIII Formula with category '" 
					  + category + "', Language of Commitments is irrelevant", logFileWriter);
			  verifyDOShareEqualsSummaryCommitment(name, dept, category, logFileWriter);
			  compareDeptAmounts (expectedDeptAmount, name, dept, whichPool, category, logFileWriter);
		  }
		  if (! isNotAPEvent){
			  category = apEvent;
			  writeToLogFile(name + " has compensation calculated through the Type VIII Formula with category '" 
					  + category + "', Language of Commitments is irrelevant", logFileWriter);
			  verifyDOShareEqualsSummaryCommitment(name, dept, category, logFileWriter);
			  verifyAmountCorrectForAPType(name, dept, apEventType, logFileWriter);
			  compareDeptAmounts (expectedDeptAmount, name, dept, whichPool, category, logFileWriter);
		  }
		  return true;
	  }// end calculateTypeEightFormula
	  
	  private void verifyAmountCorrectForAPType(String name, String dept, String apEventType, 
			  BufferedWriter logFileWriter) throws Exception{
		  writeToLogFile("*** method verifyAmountCorrectForAPType called ***", logFileWriter);
		  double expectedAPAmount = 0;
		  if (apEventType.equalsIgnoreCase("Terminal Year")){
			  writeToLogFile("AP Event Type is Terminal Year - special calculation required", logFileWriter);
			  expectedAPAmount = getTerminalYearRaisePercent(logFileWriter) 
					  	* getBaseSalary (name, dept, logFileWriter);
		  }//it's a terminal year AP Event - handle it special
		  else{//it is either Original data or Adjusted data
			  boolean originalValues = this.logDirectoryName.contains(ReportRunner.ORIGINAL);
			  writeToLogFile("AP Event examined is "+apEventType, logFileWriter);
			  ReferenceValues values = new ReferenceValues(TestFileController.AlteredAPValuesWorksheetName);
			  if (originalValues){
				  values = new ReferenceValues(TestFileController.OriginalAPValuesWorksheetName);
				  writeToLogFile("The data used involves Original AP Values", logFileWriter);
			  }
			  else 
				  writeToLogFile("The data used involves Altered AP Values", logFileWriter);
			  expectedAPAmount = values.getFloatAmountForAPType(apEventType);
		  }//not a Terminal Year AP Event - handle it through the Reference Values
		  
		  //find out the dept FTE and multiply it by the AP Amount and verify that the amount
		  //... in the Control Sheet Detail Report matches the expected AP Amount
		  expectedAPAmount *= getDeptFTEForNameAndDept(name, dept, logFileWriter)/100;
		  double actualAPAmount = getFTEAdjustedDollarIncrease(name, dept, apEvent, logFileWriter);
		  if (isApproximatelyEqual(expectedAPAmount, actualAPAmount)){
			  writeToLogFile("AP Amount type is as expected: " + expectedAPAmount, logFileWriter);
		  }//end if - the amount is as expected
		  else{
			  String message = "MISMATCH: expected AP Amount of " + expectedAPAmount
					  +", but actual AP Amount is " + actualAPAmount;
			  verificationErrors.append(message);
			  writeToLogFile(message, logFileWriter);
		  }//end else - the amount is not as expected
	  }//end verifyAmountCorrectForAPType method
	  
	  private float getDeptFTEForNameAndDept (String name, String dept, BufferedWriter logFileWriter) throws Exception{
		  HashMap<String, String> criteria = new HashMap<String, String>();
		  criteria.put("Name", name);
		  criteria.put("Department", dept);
		  String percentString = myExcelReader.getDiscreteOrderedListOfValues(mySheetName, 
				  criteria, "Dept FTE", logFileWriter).first();
		  return Float.valueOf((Long) NumberFormat.getNumberInstance(java.util.Locale.US).parse(percentString));
	  }

	  private void verifyDOShareEqualsSummaryCommitment (String name, String dept, BufferedWriter logFileWriter) throws Exception{
		  verifyDOShareEqualsSummaryCommitment (name, dept, new String(), logFileWriter);
	  }
	  
	  private void verifyLineItemShareEqualsCommitment (String name, String dept, String sourcePool,
			  BufferedWriter logFileWriter) throws Exception {
		  verifyLineItemShareEqualsCommitment (name, dept, sourcePool, new String(), logFileWriter);
	  }
	  
	  private void verifyLineItemShareEqualsCommitment (String name, String dept, String sourcePool, 
			  String category, BufferedWriter logFileWriter) throws Exception {
		  writeToLogFile("*** method verifyLineItemShareEqualsCommitment called ***", logFileWriter);
		  HashMap<String, String> criteria = new HashMap<String, String>();
		  criteria.put("Name", name);
		  criteria.put("Department", dept);
		  criteria.put("Source Pool", sourcePool);
		  criteria.put("Stage", "Known");
		  if (! category.isEmpty())
			  criteria.put("Category", category);
		  String[] columns = new String[2];
		  
		  columns[0] = "FTE Adjusted Department Share Amount";
		  if (sourcePool.equalsIgnoreCase("Regular Merit Pool"))
			  columns[0] = "FTE Adjusted DO Share Amount";
		  columns[1] = "FTE Adj Total Commitment";
		  
		  String[] values = myExcelReader.getSingleFacultyMultipleValues(mySheetName, criteria, columns, logFileWriter);
		  if (values[0].equalsIgnoreCase(values[1])){//the test passes - print info
			  writeToLogFile(columns[0] + " is equal to " + columns[1] + ", as expected", logFileWriter);
			  return;
		  }
		  String shareStringAmt = values[0];
		  String commitmentStringAmt = values[1];
		  double shareAmt = 0;
		  double commitmentAmt = 0;
		  try{
			  shareAmt = ((Number)NumberFormat.getNumberInstance(java.util.Locale.US).parse(shareStringAmt)).doubleValue();
			  commitmentAmt = ((Number)NumberFormat.getNumberInstance(java.util.Locale.US).parse(commitmentStringAmt)).doubleValue();
		  }
		  catch (Exception e){
			  writeToLogFile("Exception thrown: "+e.getMessage(), logFileWriter);
		  }
		  if (this.isApproximatelyEqual(shareAmt, commitmentAmt)){
			  writeToLogFile(columns[0] + " is approximately equal to " + columns[1] + ", as expected", logFileWriter);
		  }
		  else{
			  String message = columns[0] + " with value " + values[0]+ " is not equal to " 
					  	+ columns[1] +" with value " + values[1];
			  verificationErrors.append(message);
			  writeToLogFile(message, logFileWriter);
		  }
	  }

	  
	  private void verifyDOShareEqualsSummaryCommitment (String name, String dept, String category, BufferedWriter logFileWriter) throws Exception{
		  writeToLogFile("*** method verifyDOShareEqualsSummaryCommitment called", logFileWriter);
		  double commitment = getFTEAdjustedDollarIncrease(name, dept, category, logFileWriter);
		  double doShare = 0;
		  doShare = getActualDOAmount(name, dept, logFileWriter, category);
		  if (! isApproximatelyEqual(commitment, doShare)){
			  String message = "Unexpected mismatch: DO Share of " +doShare
					  +"does not match commitment of " +commitment;
			  verificationErrors.append(message);
			  writeToLogFile(message, logFileWriter);
		  }
		  else {
			  logFileWriter.newLine();
			  writeToLogFile("DO Share and Total Commitment match, as expected", logFileWriter);
		  }
	  }
	  
	  private double getTerminalYearRaisePercent (BufferedWriter logFileWriter) throws Exception{
		  writeToLogFile("*** method getTerminalYearRaisePercent called ***", logFileWriter);
		  double raisePercent = 0;
		  String locator = locators.getValueForColumnNameAndValue(locatorFileSheetName, 
				  "TerminalYearPercentageAmount", "Name", "Locator", logFileWriter, ExcelReaderToOutputFile);
		  String rowPercent = ControlSheetUI.getValueOfInputForLocator(locator, logFileWriter, locatorsToOutputFile);
		  //verify that the rowPercent retrieved is a valid value
		  if (rowPercent.length() == 0){
			  writeToLogFile("Could not get a valid value for Terminal Year Raise Percent - returning zero",
					  logFileWriter);
			  return 0;
		  }//end if - the rowPercent length is not a valid value
		  try{
			  raisePercent = ((Number)NumberFormat.getNumberInstance(java.util.Locale.US).parse(rowPercent)).doubleValue()/100;
			  writeToLogFile("Returning a percentage value of " + raisePercent, logFileWriter);
			  return raisePercent;
		  }//end try
		  catch(ParseException e){
			  verificationErrors.append(e.getMessage());
			  writeToLogFile("Could not parse the value retrieved for Terminal Year Raise Percent", logFileWriter);
			  writeToLogFile(e.getMessage(), logFileWriter);
			  return 0;
		  }
	  }//end getDepartmentFacultyRaisePercent method 

}//end Class
