package Maven.SeleniumWebdriver;

import java.util.concurrent.TimeUnit;

import org.junit.*;

import static org.junit.Assert.*;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;

/*
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
*/
import Maven.SeleniumWebdriver.SFDC_APC_Application_Variables;

public class DataMigrator {
	  private WebDriver driver;
	  private String baseUrl;
	  private String directory = "C:/Users/christ13/Documents/HandSon/";
	  private String inputFileName = "Input Data.xls";
	  private String inputWorksheetName = "Test Input Data";
	  private String outputFileName = "Test Results.xls";
	  private String outputWorksheetName = "Passed Tests";
	  private String errorWorksheetName = "Failed Tests";
	  private boolean acceptNextAlert = true;
	  private StringBuffer verificationErrors;
	  //private String username;
	  private static String browserName;
	  private static String startPage = "apphs/processInboxTab.do";
	  //private static String login;
	  //private static String password = "Intrax##2";
	  
	  private HSSFExcelReader fileIn;
	  private HSSFExcelReader fileOut;

	  @Before
	  public void setUp() throws Exception {
			browserName = System.getenv("browserName");
			System.out.println("Derived environmental variable browserName: " + browserName);
		
			if (browserName.equalsIgnoreCase(SFDC_APC_Application_Variables.CHROME_BROWSER)){
				driver = new ChromeDriver();
			}
			else if (browserName.equalsIgnoreCase(SFDC_APC_Application_Variables.FIREFOX_BROWSER)){
				driver = new FirefoxDriver();
			}
			else if (browserName.equalsIgnoreCase(SFDC_APC_Application_Variables.SAFARI_BROWSER)){
				driver = new SafariDriver();
			}
			else driver = new InternetExplorerDriver();
			baseUrl = "https://cs40.salesforce.com/";
			
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			String envName = System.getenv("envName");
			System.out.println("Derived environmental name: " + envName);
			if (envName.equalsIgnoreCase("Ustage")){
				
				baseUrl = "https://cs45.salesforce.com/";
			}
			else{ 
				
				baseUrl = "https://cs23.salesforce.com/";
			}
			baseUrl = "https://handsonweb-"+HandSOnTestSuite.machineName+".stanford.edu/";
			
			startPage = baseUrl + startPage;
			
			System.out.println("derived start page is as follows: " + startPage);
			
			

			fileIn = new HSSFExcelReader(directory + inputFileName);
			fileOut = new HSSFExcelReader(directory + outputFileName);
			
			  if (! fileOut.addSheet(outputWorksheetName)){
				  System.out.println("Removing and re-creating Output File Worksheet");
				  fileOut.removeSheet(outputWorksheetName);
				  fileOut.addSheet(outputWorksheetName);
			  }
			  fileOut.addColumn(outputWorksheetName, "Application ID");

			  if (! fileOut.addSheet(errorWorksheetName)){
				  System.out.println("Removing and re-creating Error Worksheet");
				  fileOut.removeSheet(errorWorksheetName);
				  fileOut.addSheet(errorWorksheetName);
			  }
			  fileOut.addColumn(errorWorksheetName, "Application ID");

	  }
	  @Test
	  public void testDataMigration() throws Exception {
		  verificationErrors = new StringBuffer();
		  int maxAttemptedSyncs = fileIn.getRowCount(inputWorksheetName);
		  System.out.println("Rows in the input file: " + maxAttemptedSyncs);
		  int attemptedSyncs = 2;
		  driver.get(startPage);
	  }


	  @After
	  public void tearDown() throws Exception {
		    driver.quit();
		    String verificationErrorString = verificationErrors.toString();
		    if (!"".equals(verificationErrorString)) {
		    	System.out.println(verificationErrorString);
		      fail(verificationErrorString);
		    }
	  }
/*	  
	  public boolean loginToSalesForce() throws Exception{
		  System.out.println("Now logging into Salesforce");
		  driver.get(startPage);
		  waitAndType (By.id("username"), login);
		  Thread.sleep(1000);
		  driver.findElement(By.id("password")).sendKeys(password);
		  Thread.sleep(2000);
		  driver.findElement(By.id("Login")).click();
		  return true;
	  }
*/	  
	  public String getApplicationID(int row_index) throws Exception{
		  System.out.print("Now deriving Application ID: ");
		  String appId = fileIn.getCellData(inputWorksheetName, 0, row_index);
		  System.out.println(appId);
		  System.out.println("... for APC PT with index " + row_index);
		  return appId;
	  }
	  
	  public String getApplicationName(int row_index) throws Exception{
		  System.out.print("Now deriving Application Name: ");
		  String appName = fileIn.getCellData(inputWorksheetName, 111, row_index);
		  System.out.println(appName);
		  return appName;
	  }
	  
	  public boolean syncApplication(String appName) throws Exception{
		  System.out.println("Now sync-ing Application " + appName);
		  waitAndClick(By.name("sync"));
		  if (isApplicationSyncAble(By.id("j_id0:j_id1:i:f:pb:pbb:next"))){
			  if (isApplicationSyncAble(By.id("j_id0:j_id1:i:f:pb:pbb:bottom:next"))){
				  if (isApplicationSyncAble(By.id("j_id0:j_id1:i:f:pb:pbb:bottom:finish"))){
					  System.out.println("Application is successfully sync'd");
					  return true;
				  }
				  else {
					  if (isApplicationSyncAble(By.id("j_id0:j_id1:i:f:pb:pbb:bottom:next"))){
						  System.out.println("There was a third next button");
						  if (isApplicationSyncAble(By.id("j_id0:j_id1:i:f:pb:pbb:bottom:finish"))){
							  System.out.println("Application is successfully sync'd");
							  return true;
						  }
					  }
					  System.out.println ("finish button is not accessible");
					  return false;
				  }
			  }
			  else {
				  System.out.println ("second next button is not accessible");
				  waitAndClick(By.id("j_id0:j_id1:i:f:pb:pbb:bottom:finish"));
				  return false;
			  }
		  }//end if
		  else {
			  System.out.println ("initial next button is not accessible");
			  waitAndClick(By.id("j_id0:j_id1:i:f:pb:pbb:bottom:finish"));
			  return false;
		  }
	  }//end of method
	  
	  public boolean isApplicationSyncAble (By by){
		  try{
			    for (int second = 0;; second++) {
			    	if (second >= 3) return false;
			    	try { if (isElementClickable(by)) break; } catch (Exception e) {}
			    	System.out.println("waiting for element to be click-able, second: " + second);
			    	Thread.sleep(1000);
			    }
		  }
		  catch (Exception e){
			  return false;
		  }
		  return true;
	  }
	  
	  public boolean verifySync() throws Exception{
		  System.out.println("Now verifying that sync was successful");
		  waitAndClick(By.name("edit"));
		  waitForElementPresent (By.id("CF00Na000000B2Kea"));
		  if (isTextPresent ("", By.id("CF00Na000000B2Kea"))){
			  driver.findElement(By.name("cancel")).click();
			  System.out.println("Sync was unsuccessful");
			  return false;
		  }
		  else{
			  System.out.println("Sync was successful");
			  driver.findElement(By.name("cancel")).click();
			  return true;
		  }
	  }
	  
	  public void logoutOfSalesForce() throws Exception{
		  System.out.println("Now logging out of Salesforce");
	  }
	  
	  private boolean isElementPresent(By by) {
		    try {
		      driver.findElement(by);
		      return true;
		    } catch (NoSuchElementException e) {
		      return false;
		    }
		  }
		  
		  private boolean isElementClickable (By by){
			  try{
					  driver.findElement(by).click();
			  }
			  catch(WebDriverException e){
					  return false;
			  }
			  return true;
		  }
		  
		  private void waitAndClick (By by) throws Exception{
			    for (int second = 0;; second++) {
			    	if (second >= 60) fail("timeout for element " +by.toString());
			    	try { if (isElementClickable(by)) break; } catch (Exception e) {}
			    	Thread.sleep(1000);
			    }

		  }


	  private void waitForElementPresent (By by) throws Exception{
		    for (int second = 0;; second++) {
		    	if (second >= 30) fail("timeout");
		    	try { if (isElementPresent(by)) break; } catch (Exception e) {}
		    	Thread.sleep(1000);
		    }
	  }

	  private void waitAndType (By by, String textToEnter) throws Exception{
		  waitForElementPresent (by);
		  waitAndClick(by);
		  driver.findElement(by).clear();
		  driver.findElement(by).sendKeys(textToEnter);
	  }

	  private boolean isTextPresent (String expectedText, By by){
		  try {
		        assertEquals(expectedText, driver.findElement(by).getAttribute("value"));
		      } catch (Throwable e) {
		        //verificationErrors.append("Unexpected text: "+ e.toString());
		        System.out.println("Unexpected text: "+ e.toString());
		        return false;
		      }
		  return true;
	  }

}