package Maven.SeleniumWebdriver;

import java.util.TreeMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/*
 * (1) The figures that show up are separate sums for Dept Shares per 
 * the Regular Merit Pool and the Market Share Pool as well as the sum of all Pool 
 * contributions to the DO Share.  
 * (2) The figure that shows up on the Report is influenced by the 
 * FTE Percent of the faculty member.  When retrieving values from the Control Sheet
 * Detail Report, we should specify the boolean variable "adjustForFTE" in ASCTest 
 * (which determines whether or not to adjust for the FTE) to "true".
 * (3) Dean Richard Saller does not appear on any of the Salary Setting Reports, and neither
 * do the Senior Associate Deans.  So, the "excludeDeans" boolean variable has to be set 
 * to "true".  
 * (4) Previous Year Commitments don't show up on any of the Salary Setting Reports 
 * (5) Of note, any "Dept" contributions from any source Pool other than the Market Pool 
 * or the Regular Merit Pool will show up under the "Regular Merit Pool Dept Share" 
 * column.  This is a known issue. 
 * Similar behavior occurs in the Pool Allocation Report and this has been filed as 
 * HANDSON-3251.  
 * I have updated HANDSON-3251 to include this issue with this Report.
 * This test will be designed to fail until HANDSON-3251 is fixed.  
 * (6) FYI, this report will want the total raise amount per faculty, not per faculty per dept.
 * Therefore, if a faculty member belongs to more than one department, we have to add up 
 * the raises for the multiple departments.
 * (7) If a faculty member has appointments in multiple departments within the 
 * School of Humanities and Sciences, then that faculty member will appear on multiple lines 
 * within the Dean Salary Setting Report, one line per department that this faculty member 
 * has an appointment in.
 * (8) If a faculty member has multiple ASC's, then the sum of them will be displayed on
 * one line in the Dean Salary Setting Report, if these pertain to a single department. If 
 * multiple ASC's each refer to a different department that this faculty member has an 
 * appointment in, then each ASC will affect the line pertinent to the department 
 * corresponding to the department that this ASC pertains to.  This will happen *unless*
 * that ASC is a Previous Year Commitment.  In that case, it will not affect any lines in
 * the Dean Salary Setting Report.
 * (9) If a faculty member has two separate ASC's, and one of them comes from a Pool other 
 * than the Regular Merit Pool, and this pool is one of the pools that gets inappropriately 
 * placed in the Regular Merit Pool (as per HANDSON-3251), and the other comes from the 
 * Regular Merit Pool, what will appear on the Dean Salary Setting Worksheet is the sum of 
 * both the raise from the ASC that comes from the Regular Merit Pool and the raise that 
 * comes from the other Pool, and that sum will appear in the column that corresponds to the 
 * Regular Merit Pool.
 * (10) All Department Chairs appear on this report, as well as any who are granted an
 * "Chair Worksheet" Override in Salary Setting.
 */

public class ChairSalarySettingWorksheetASCTest extends
		SalarySettingReportASCTest {
	
	protected String clusterName;


	@Before
	public void setUp() throws Exception {
		dataPresentCriterionName = ASCFacultyValues.StringAttributeKeys[ASCFacultyValues.ClusterIndex];
		dataPresentCriterionValue = clusterName;
		useASCOnly = true;
		configureInclusionCriteria();
		super.setUp();
	}
	
	//override the setStringAttributes method in the SalarySettingReportASCTest - omit Cluster here
	protected void setStringAttributes(){
		expectedStringAttributeNames = new String[4];
		actualStringAttributeNames = new String[4];
		expectedStringAttributeNames[0] = actualStringAttributeNames[0] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.DepartmentIndex];
		expectedStringAttributeNames[1] = actualStringAttributeNames[1] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.FacultyIndex];
		expectedStringAttributeNames[2] = actualStringAttributeNames[2] = ASCFacultyValues.StringAttributeKeys[ASCFacultyValues.RankIndex];
		expectedStringAttributeNames[3] = ExpectedASCData.StringAttributeKeys[ExpectedASCData.DescriptionIndex];
		actualStringAttributeNames[3] = HandSOnTestSuite.FY + " " + ExpectedASCData.StringAttributeKeys[ExpectedASCData.DescriptionIndex];

	}

	

	private void configureInclusionCriteria(){
		criteria = new TreeMap<String, String>();
		criteria.put(ExpectedASCData.StringAttributeKeys[ExpectedASCData.StageIndex], 
				ASCTest.stages[ASCTest.KnownStageIndex]);
		criteria.put(ExpectedASCData.isDeptChair, "true");

		configureBooleanInclusionCriteria();
	}//end configureInclusionCriteria method
	
	private void configureBooleanInclusionCriteria(){
		excludeMarketShare = false;
		excludeZeroDOShare = false;
		adjustForFTE = true;
		excludeDeans = true;
		backoutPYs = true;
	}//end configureBooleanInclusionCriteria method

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void test() throws Exception{
		super.test();
	}

}
