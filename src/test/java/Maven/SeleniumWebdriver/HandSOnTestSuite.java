package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.TreeSet;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import junit.framework.TestSuite;
import junitparams.JUnitParamsRunner;

import org.junit.internal.TextListener;
import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/*
@RunWith(Suite.class)
@Suite.SuiteClasses({ReportRunner.class})
*/

public class HandSOnTestSuite extends TestSuite {
	
	public static final String browserName = "CHROME";
	public static final String machineName = "tst4";
	public static final String envName = "tst";
	public static final String currentNY = "19-20";
	public static final String currentNextNextYear = "20-21";	
	public static final String currentThirdNextYear = "21-22";
	public static final String NNY = "18-19";
	public static final String NY = "17-18";
	public static final String FY = "16-17";
	public static final String PY = "15-16";
	public static final String PPY = "14-15";
	public static TestFileController fileController;
	public static SalarySettingManager salarySettingManager;
	public static ControlSheetUI controlSheetUI;
	public static SalarySettingTabUIController salarySettingTabUI;
	
	
	//public static RetentionUIController retentionUIController;
	public static SalarySettingManagementUIController salarySettingMgrTabUI;
	public static SearchAuthorizationUIController searchAuthorizationUI;
	public static StandardCostMgrUIController stdCostMgrUI;
	public static StartPageUIController startPageUI;
	public static ReferenceValues referenceValues;
	public static APFacultyValues apFacultyValues;
	public static ExpectedASCDataSet expectedASCDataSet;
	public static HashMap<String, TreeSet<String>> overrideList;
	public static HashMap<String, String> codeToDept;
	public static final int numberOfNewData = 7;
	public static String[] searchAuthorizationIDs;
	public static String[] recruitmentIDs;
	public static String[] facultyNames;//each is a String with format "LastName, FirstName"
	public static String[] retentionIDs;
	
	
	@Before
	public void setUp() throws Exception {
		fileController = new TestFileController();
		salarySettingManager = new SalarySettingManager();
		controlSheetUI = new ControlSheetUI();
		salarySettingTabUI = new SalarySettingTabUIController();
		salarySettingMgrTabUI = new SalarySettingManagementUIController();
		searchAuthorizationUI = new SearchAuthorizationUIController();
		stdCostMgrUI = new StandardCostMgrUIController();
		startPageUI = new StartPageUIController();
		//retentionUIController = new RetentionUIController();
		referenceValues = new ReferenceValues(TestFileController.OriginalAPValuesWorksheetName);
		apFacultyValues = new APFacultyValues(TestFileController.FacultyValuesWorksheetName);
		expectedASCDataSet = new ExpectedASCDataSet();
		overrideList = new HashMap<String, TreeSet<String>>();
		searchAuthorizationIDs = new String[numberOfNewData];
		recruitmentIDs = new String[numberOfNewData];
		retentionIDs = new String[numberOfNewData];
		facultyNames = new String[numberOfNewData];
		initCodeToDept();
		System.out.println("setUp of Test Suite is running");
	}//end setUp method
	
	public static void renew() throws Exception{
		fileController = new TestFileController();
		ReportRunner.logDirectoryName = TestFileController.LogDirectoryName +"/" + ReportRunner.referenceWhichValues;
		salarySettingManager = new SalarySettingManager();
		controlSheetUI = new ControlSheetUI();
		//we are adding the following five lines to make sure that they are initialized
		salarySettingTabUI = new SalarySettingTabUIController();
		salarySettingMgrTabUI = new SalarySettingManagementUIController();
		searchAuthorizationUI = new SearchAuthorizationUIController();
		stdCostMgrUI = new StandardCostMgrUIController();
		startPageUI = new StartPageUIController();
		
		referenceValues = new ReferenceValues(TestFileController.OriginalAPValuesWorksheetName);
		apFacultyValues = new APFacultyValues(TestFileController.FacultyValuesWorksheetName);
		expectedASCDataSet = new ExpectedASCDataSet();
		overrideList = new HashMap<String, TreeSet<String>>();
		initCodeToDept();
		for (int i=0; i<numberOfNewData; i++) {
			searchAuthorizationIDs[i] = new String();
			recruitmentIDs[i] = new String();
			retentionIDs[i] = new String();
			facultyNames[i] = new String();
		}//end for loop

		System.out.println("renew of Test Suite is running");
	}//end renew method
	
	public static void initCodeToDept(){
		codeToDept = new HashMap<String, String>();
		codeToDept.put("PXIE", "Psychology");
		codeToDept.put("QAGT", "Applied Physics");
		codeToDept.put("PDUN", "Classics");
		codeToDept.put("PJIM", "Philosophy");
	}//end initCodeToDept method

	@After
	public void tearDown() throws Exception {
		System.out.println("tearDown of Test Suite is running");
	}

	@Test
	public void test() {
		System.out.println("Test Suite test method is running");
		JUnitCore junit = new JUnitCore();
		junit.addListener(new TextListener(System.out));
		junit.run(ReportRunner.class);

	}
}
