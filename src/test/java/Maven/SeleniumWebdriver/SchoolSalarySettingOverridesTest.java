package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SchoolSalarySettingOverridesTest {
	protected static String testName;
	protected static String testCaseNumber;
	protected static String testDescription;
	protected static String testEnv;
	protected static StringBuffer verificationErrors;
	protected BufferedWriter logFileWriter;
	protected SSExcelReader myReader;

	@Before
	public void setUp() throws Exception {
		testName = "SchoolSalarySettingOverridesTest";
		testEnv = "Test";
		testCaseNumber = "062";
		testDescription = "Test of Overrides for the School SSW";
	}//end setUp method

	@After
	public void tearDown() throws Exception {
		TestFileController.writeTestResults(testEnv, testCaseNumber, testName, testDescription, verificationErrors);
		logFileWriter.newLine();
		logFileWriter.close();
	}//end tearDown method

	@Test
	public void test() throws Exception{
		verificationErrors = new StringBuffer();
		String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		logFileWriter = TestFileController.getLogFileWriter(logFileName);
		logFileWriter.write("The School Salary Setting Worksheet will be examined ");
		logFileWriter.newLine();
		
		ReportRunner.downloadSchoolSSW (ReportRunner.referenceWhichValues, logFileWriter);
		
		myReader 
			= new SSExcelReader(TestFileController.directory + TestFileController.SchoolSalarySettingReportName, 
					TestFileController.SchoolSalarySettingReportHeaderRow);
		
		if (! checkForOverriddenFaculty(logFileWriter)){
			String message = "Couldn't locate overridden faculty in the SSW";
			logFileWriter.write(message);
			logFileWriter.newLine();
			verificationErrors.append(message);
		}//end if
		
		TestFileController.closeOneExcelReader(myReader);
	}//end test method
	
	protected boolean checkForOverriddenFaculty(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.newLine();
		logFileWriter.write("**** method checkForOverriddenFaculty called ****");
		logFileWriter.newLine();
		
		ArrayList<String[]> overrideAttributes = HandSOnTestSuite.salarySettingMgrTabUI.getAllOverrides(logFileWriter);

		int rowForColNames = myReader.getCellRowNum(TestFileController.SchoolSalarySettingReportWorksheetName, 0, SalarySettingManagementUIController.overrideSectionHeading);
		logFileWriter.write("Estimated location of overridden faculty at row " + rowForColNames);
		logFileWriter.newLine();

		int actualRowForColNames = myReader.getCellRowNum(TestFileController.SchoolSalarySettingReportWorksheetName, 0, "Cluster", rowForColNames);
		logFileWriter.write("Actual header row for overridden faculty is " + actualRowForColNames);
		logFileWriter.newLine();
		
		
		boolean foundUnexpectedString = false;
		int rowNum = -1;
		for (int i=0; i<overrideAttributes.size(); i++){
			String[] overrideArray = overrideAttributes.get(i);
			logFileWriter.write("Now examining the file for faculty name "+overrideArray[0] 
					+", and department "+overrideArray[2]);
			logFileWriter.newLine();

			if (actualRowForColNames != -1){
				myReader.setRowForColumnNames(rowForColNames);
				String[] colNames = {"Name", "Cluster", "Department", "Override", "Description"};
				for (int j=0; j<colNames.length; j++){
					if (j==0)
						rowNum = myReader.getCellRowNum(TestFileController.SchoolSalarySettingReportWorksheetName, colNames[j], overrideArray[0]);
					logFileWriter.write("Found initial value name "+overrideArray[0] +", at row "+rowNum);
					logFileWriter.newLine();
					//obtain and output the value obtained
					String value = myReader.getCellData(TestFileController.SchoolSalarySettingReportWorksheetName, colNames[j], rowNum);
					logFileWriter.write("Value for the "+ colNames[j] +" column is found in the SSW: "+ value);
					logFileWriter.newLine();

					String UIValue = overrideArray[j];
					if ((value == null) || (UIValue == null) || (value.isEmpty()) || (UIValue.isEmpty()) || (! UIValue.equalsIgnoreCase(value))){
						logFileWriter.write("MISMATCH found --> SSW value: " + value +", UI Value: " + UIValue);
						logFileWriter.newLine();
						foundUnexpectedString = true;
					}//end if - there's a mismatch
				}//end for - iterating through the columns
			}//end if - we found the actual row for column names
		}//end for
		logFileWriter.newLine();
		logFileWriter.write("Values determined by this method: ");
		logFileWriter.newLine();
		logFileWriter.write("Row containing the Override Section Header: " + rowForColNames);
		logFileWriter.newLine();
		logFileWriter.write("Row containing the Column Headers: " + actualRowForColNames);
		logFileWriter.newLine();
		logFileWriter.write("Row containing the Faculty values: " + rowNum);
		logFileWriter.newLine();
		logFileWriter.write("Unexpected String found: " + foundUnexpectedString);
		logFileWriter.newLine();
		return ((rowForColNames != -1) && (actualRowForColNames != -1) && (rowNum != -1)
				&& (actualRowForColNames == rowForColNames + 1) && (! foundUnexpectedString));
	}//end checkForOverriddenFaculty method
	

}//end Class
