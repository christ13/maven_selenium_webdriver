package Maven.SeleniumWebdriver;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;

import org.junit.*;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import java.util.Calendar;
/*
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
*/
import java.util.Hashtable;

public class SFDC_APC_Application_Variables {
	  private WebDriver driver;
	  private String baseUrl;
	  private boolean acceptNextAlert = true;
	  private StringBuffer verificationErrors = new StringBuffer();
	  
	  public static String[] Individual_AP_Data_Title_Names = {"apnum", "ap_username", "ap_firstName", "ap_lastName", 
		  "ap_email", "marital_status", "country_citizenship", "country_residence", "nationality", "birthdate", 
		  "address_country", "prior_US_AP_experience", "eligibility_US_Citizen", "visa_denied", 
		  "childcare_exp?", "grad_highschool?", "criminal_hist?", "drivers_license?", "health", 
		  "physical_disability?", "been_hospitalized?", "taking_medication?", "psychiatric_care?", "pregnant?", 
		  "contagious_disease?", "drug_alcohol_dependency?", "Allergies?", "qualified?", "Qualification Factor under Test"};
	  public static String[] output_title_names = {"apnum", "ap_username", "test_result", "exception?", "verification failure"};
	  public static final String input_file_path = "C:/Test Results/APC/ap_data_input.xls";
	  public static final String output_file_path = "C:/Test Results/APC/ap_data_output.xls";
	  public static final String AP_Individual_input_wksheet_name = "Individual APC PTs";
	  public static final String AP_Universal_input_wksheet_name = "All APC PTs";
	  public static final String output_ind_wksheet_name = "Test_Results_Overall";
	  public static final String UNQUALIFIED = "Unqualified";
	  public static final String FLAGGED = "Flagged";
	  public static final String QUALIFIED = "Qualified";
	  public static final int TOO_OLD = 27;
	  public static final int TOO_YOUNG = 16;
	  public static final int RIGHT_AGE = 24;
	  public static final String address_US = "United States";
	  public static final String nationality_US = "American (US)";
	  public static final String SFDC_LOGIN = "cmcfadden@intraxinc.com.";
	  public static final String SFDC_PASSWORD = "Intrax##2";
	  public static final String SFDC_URL = "https://test.salesforce.com/";
	  
	  //indices specific to the input file
	  public static final int how_many_APs = 36;
	  public static final int index_apnum = 0;
	  public static final int index_username = 1;
	  public static final int index_firstName = 2;
	  public static final int index_lastName = 3;
	  public static final int index_email = 4;
	  public static final int index_marital = 5;
	  public static final int index_citizenship = 6;
	  public static final int index_residence = 7;
	  public static final int index_nationality = 8;
	  public static final int index_birthdate = 9;
	  public static final int index_address_country = 10;
	  public static final int index_prior_US_AP = 11; 
	  public static final int index_eligibility_citizenship = 12;
	  public static final int index_visa_denied = 13;
	  public static final int index_childcare_exp = 14;
	  public static final int index_grad_highschool = 15;
	  public static final int index_criminal_hist = 16;
	  public static final int index_drivers = 17;
	  public static final int index_health = 18;
	  public static final int index_disability = 19;
	  public static final int index_hospitalized = 20;
	  public static final int index_medication = 21;
	  public static final int index_psychiatric = 22;
	  public static final int index_pregnant = 23;
	  public static final int index_contagious = 24;
	  public static final int index_alc_drg_depend = 25;
	  public static final int index_allergy = 26;
	  public static final int index_qualified = 27;
	  public static final int index_qualifying_factor = 28;
	  
	  //indices specific to the output file
	  public static final int index_testresult = 2;
	  public static final int index_exception = 3;
	  public static final int index_verification_errors = 4;
	  
	  //AP-specific test data
	  public static final int index_AP_normal = 1;
	  public static final int index_AP_domesticPartner = 2;
	  public static final int index_AP_married = 3;
	  public static final int index_AP_AmericanCitizen = 4;
	  public static final int index_AP_AmericanResident = 5;
	  public static final int index_AP_AmericanNational = 6;
	  public static final int index_AP_too_old = 7;
	  public static final int index_AP_too_young = 8;
	  public static final int index_AP_maybe_too_old = 9;
	  public static final int index_AP_maybe_too_young = 10;
	  public static final int index_AP_US_Address = 11;
	  public static final int index_prior_US_AP_successful = 12;
	  public static final int index_prior_US_AP_unsuccessful = 13;
	  public static final int index_eligibility_US_Citizen = 14;
	  public static final int index_visadenied_B1 = 15;
	  public static final int index_visadenied_B2 = 16;
	  public static final int index_visadenied_F1 = 17;
	  public static final int index_visadenied_F2 = 18;
	  public static final int index_visadenied_J1 = 19;
	  public static final int index_visadenied_H1B = 20;
	  public static final int index_no_childcare_exp = 21;
	  public static final int index_not_graduated_gotdate = 22;
	  public static final int index_not_graduated_nodate = 23;
	  public static final int index_got_criminal_hist = 24;
	  public static final int index_no_drivers = 25;
	  public static final int index_poor_health = 26;
	  public static final int index_fair_health = 27;
	  public static final int index_good_health = 28;
	  public static final int index_disabled = 29;
	  public static final int index_got_hosp = 30;
	  public static final int index_got_meds = 31;
	  public static final int index_got_psych = 32;
	  public static final int index_got_pregnant = 33;
	  public static final int index_got_contagion = 34;
	  public static final int index_got_alcydrugs = 35;
	  public static final int index_got_allergy = 36;
	  
	  public static String[] qualifying_factors = {"All normal", "Domestic Partner", "Married", "American Citizen", "American Resident",
		  "American National", "Too Old to Qualify", "Too Young to Qualify", "Old enough to get Flagged", "Young enough to get Flagged",
		  "US Address", "Prior AP experience - Successful", "Prior AP experience - not successful", "US Citizen - Ineligible",
		  "Visa Denied - B1", "Visa Denied - B2", "Visa Denied - F1", "Visa Denied - F2", "Visa Denied - J1", "Visa Denied - H1B",
		  "No Childcare experience", "Not graduated High School - got a graduation date", "Not graduated high school - no graduation date",
		  "Got Criminal History", "No Driver's License", "Poor Health", "Fair Health", "Good Health", "Physical Disability", 
		  "Been Hospitalized in the last 12 months", "Taking Medications", "Received Psychiatric Treatment", "Pregnant",
		  "Got a contagious disease", "Got a Dependency on Alcohol/Drugs", "Got Allergies"};
	  

	  public static final String QA = "qa-intraxinc.cs23.force.com";
	  public static final String USTAGE = "ustage-intraxinc.cs24.force.com";
	  public static final String site_register = "/intraxappengine/AppEngine_SiteRegister?uin=APCPT";
	  public static final String site_login = "/intraxappengine/AppEngine_SiteLoginPage?uIn=APCPT&";

	  public static final String SFDC_login = "https://test.salesforce.com/";

	  public static final String CHROME_BROWSER = "CHROME";
	  public static final String FIREFOX_BROWSER = "FIREFOX";
	  public static final String SAFARI_BROWSER = "SAFARI";
	  public static String[] Universal_AP_Data_NameValues = {"NAMES", "apnum", "ap_password", "email", "default_country", "grad_date", "arrival_date", "SFDC_password", 
		  "prior_AP_agency", "thirteenth_birthdate", "fourteenth_birthdate", "fifteenth_birthdate", "sixteenth_birthdate", "tenth_birthdate", 
		  "over_weekly_hrs", "max_weekly_hrs", "over_daily_hrs", "max_daily_hrs", "over_once_hrs", "max_once_hrs", "over_monthly_hrs", "max_monthly_hrs"};
	  //put the data into this - the names are above, the values are calculated below.
	  private Hashtable Universal_AP_Data = new Hashtable();


  @Before
  public void setUp() throws Exception {
    driver = new ChromeDriver();
    baseUrl = "http://";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  
  @Test
  public void testSFDCAPCApplicationVariables() throws Exception {
	  String ap_password = "Intrax##2";
	  String email = "apc.pt.email@gmail.com";
	  String default_country = "Argentina";
	  String grad_date = "9/9/2010";
	  String arrival_date = "April 2015"; 
	  String SFDC_password = "Intrax##2";
	  String prior_AP_agency = "Agent Au Pair";
	  String birthdate;
	  String thirteenth_birthdate;
	  String fourteenth_birthdate;
	  String fifteenth_birthdate;
	  String sixteenth_birthdate;
	  String tenth_birthdate; 
	  String over_weekly_hrs = "90";
	  String max_weekly_hrs = "60";
	  String over_daily_hrs = "20";
	  String max_daily_hrs = "12";
	  String over_once_hrs = "50";
	  String max_once_hrs = "24"; 
	  String over_monthly_hrs = "300";
	  String max_monthly_hrs = "240";

    
    SimpleDateFormat df = new SimpleDateFormat("YYYYMMddHHmmss");
    SimpleDateFormat yearFormat = new SimpleDateFormat("YYYY");
    SimpleDateFormat monthFormat = new SimpleDateFormat("MM");
    SimpleDateFormat dayFormat = new SimpleDateFormat ("dd");
    
    Date d = new Date();
    String apnum = df.format(d);


    //calculate the year values
	int year = new Integer(yearFormat.format(d)).intValue();
	int too_old = year - TOO_OLD;
	int too_young = year - TOO_YOUNG;
	int right_birthYear = year - RIGHT_AGE;

    String month = monthFormat.format(d);
    String day = dayFormat.format(d);
    int monthNumber = new Integer(month).intValue();	
    //get nextMonth value - in case we need to make her birthday one day later and it's next month
    String nextMonth = new Integer((monthNumber + 1) % 12).toString();
    if (nextMonth.length() < 2) nextMonth = "0" + nextMonth;
	String birthdate_MMDD = month +"/" +dayFormat.format(d) + "/";
	String tomorrow_DD =  new Integer(new Integer(day).intValue() + 1).toString();
	String tomorrow_MMDD =  month +"/" + tomorrow_DD + "/";
	if (tomorrow_DD.equals("32")) tomorrow_MMDD = nextMonth + "/01/";
	else if ((tomorrow_DD.equals("31")) && (month == "04" || month =="06" || month =="09" || month == "11")) tomorrow_MMDD = nextMonth + "/01/";
	else if (tomorrow_DD.equals("29") && month =="02" && year % 4 != 0) tomorrow_MMDD = nextMonth + "/01/";
	//now, calculate the day and month value of the APC PT Applicant who is 26.5 years old
	//In order to make sure we've got someone who is at least 26 years and six months, her birthdate must two days ago, plus six months, minus 27 years.
	/*
		Date myDate = new Date(System.currentTimeMillis());
        Date twoDaysBefore = new Date(myDate.getTime() - (2 * 24 * 60 * 60 * 1000));     
        String dateStr = dateFormat.format(twoDaysBefore);      
        System.out.println("result is "+dateStr);
	*/
	Date myDate = new Date(System.currentTimeMillis());
	Calendar cal = Calendar.getInstance();
	cal.setTime(myDate);
	cal.add(Calendar.DATE, 180);
	//Date monthPlusSixMinusTwoDays = new Date(myDate.getTime() + (180 * 24 * 60 * 60 * 1000));
	//get the month value plus six - use mod operator and make it two digits
	//monthNumber = new Integer(tomorrow_MMDD.substring(0, 1)).intValue();
	String monthPlusSix = monthFormat.format(cal.getTime()) + "/" +dayFormat.format(cal.getTime()) + "/";
	/*
    if (monthPlusSix.length() < 2) monthPlusSix = "0" + monthPlusSix;
    monthPlusSix = monthPlusSix.concat("/" +dayFormat.format(d) + "/");
	*/
	
	//now, calculate key birthday values that will be used to determine when work experience happened
	thirteenth_birthdate = birthdate_MMDD + new Integer(right_birthYear + 13).toString();
	fourteenth_birthdate = birthdate_MMDD + new Integer(right_birthYear + 14).toString();
	fifteenth_birthdate = birthdate_MMDD + new Integer(right_birthYear + 15).toString();
	sixteenth_birthdate = birthdate_MMDD + new Integer(right_birthYear + 16).toString();
	tenth_birthdate = birthdate_MMDD + new Integer(right_birthYear + 10).toString(); 
	
		  HSSFExcelReader APC_Data_File = new HSSFExcelReader (input_file_path);
		  if (! APC_Data_File.addSheet(AP_Individual_input_wksheet_name)){
			  System.out.println("Removing and re-creating Individual APC Data File");
			  APC_Data_File.removeSheet(AP_Individual_input_wksheet_name);
			  APC_Data_File.addSheet(AP_Individual_input_wksheet_name);
		  }
		  if (! APC_Data_File.addSheet(AP_Universal_input_wksheet_name)){
			  System.out.println("Removing and re-creating Universal APC Data File");
			  APC_Data_File.removeSheet(AP_Universal_input_wksheet_name);
			  APC_Data_File.addSheet(AP_Universal_input_wksheet_name);
		  }
		  

		  for (int i=0; i<Individual_AP_Data_Title_Names.length; i++){
			  APC_Data_File.addColumn(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[i]);
		  }
		  
		  for (int arrayIndex = 1; arrayIndex <= how_many_APs; arrayIndex ++){
			  int rowIndex = arrayIndex + 1;
			  //first, set the apnum
			  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_apnum], rowIndex, apnum);
			  
			  //then, set the username for the AP which will be used to login
			  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_username], rowIndex, "apc.aupair." + apnum + "@gmail.com");
			  
			  //then, set the firstName value for the AP
			  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_firstName], rowIndex, "AuPairCare");
			  
			  //then, set the lastName value for the AP
			  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_lastName], rowIndex, "Aupair_" + apnum);
			  
			  //then, set the e-mail for the AP which will actually receive auto-generated e-mails
			  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_email], rowIndex, email);
			  
			  //set the Marital Status for the AP applicant			 
			  if (arrayIndex == index_AP_domesticPartner) 
				  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_marital], rowIndex, "Domestic Partner");
			  else if (arrayIndex==index_AP_married) 
				  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_marital], rowIndex, "Married");
			  else APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_marital], rowIndex, "Single");
			  
			  //set the Citizenship Status for the AP applicant			  
			  if (arrayIndex==index_AP_AmericanCitizen) 
				  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_citizenship], rowIndex, address_US);
			  else APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_citizenship], rowIndex, default_country);
			  
			  //set the Residence Status for the AP applicant
			  if (arrayIndex==index_AP_AmericanResident) 
				  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_residence], rowIndex, address_US);
			  else APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_residence], rowIndex, default_country);
			  
			  //set the Nationality Status for the AP applicant
			  if (arrayIndex==index_AP_AmericanNational) 
				  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_nationality], rowIndex, nationality_US);
			  else APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_nationality], rowIndex, "Argentine");
			  
			  //set the date of birth
			  if (arrayIndex==index_AP_too_old) birthdate = birthdate_MMDD + new Integer(too_old).toString();
			  else if (arrayIndex==index_AP_too_young) birthdate = tomorrow_MMDD + new Integer(too_young).toString();
			  else if (arrayIndex==index_AP_maybe_too_old)  birthdate = monthPlusSix + new Integer(too_old).toString();
			  else if (arrayIndex==index_AP_maybe_too_young) birthdate = tomorrow_MMDD + new Integer(too_young - 1).toString();
			  else birthdate = birthdate_MMDD + new Integer(right_birthYear).toString(); 
			  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_birthdate], rowIndex, birthdate);
			  
			  //set the address info, specifically, the country
			  if (arrayIndex==index_AP_US_Address) 
				  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_address_country], rowIndex, address_US);
			  else APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_address_country], rowIndex, default_country);

			  //set prior US AP experience
			  if (arrayIndex==index_prior_US_AP_successful) 
				  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_prior_US_AP], rowIndex, "Success");
			  else if (arrayIndex==index_prior_US_AP_unsuccessful) 
				  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_prior_US_AP], rowIndex, "Not_Success");
			  else APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_prior_US_AP], rowIndex, "N/A");

			  //set US Citizenship in Eligibility
			  if (arrayIndex==index_eligibility_US_Citizen) 
				  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_eligibility_citizenship], rowIndex, address_US);
			  else APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_eligibility_citizenship], rowIndex, default_country);
			  
			  //set the value for any visa denied, if any
			  String visaDenied = "N/A";
			  if (arrayIndex==index_visadenied_B1) visaDenied = "B-1 Visa (Business)";
			  else if (arrayIndex==index_visadenied_B2) visaDenied = "B-2 Visa (Tourist)";
			  else if (arrayIndex==index_visadenied_F1) visaDenied = "F-1 Visa (Student)";
			  else if (arrayIndex==index_visadenied_F2) visaDenied = "F-2 Visa (Dependent of Student)";
			  else if (arrayIndex==index_visadenied_J1) visaDenied = "J-1 Visa (Au Pair, Scholar, Trainee, Researcher or Teacher)";
			  else if (arrayIndex==index_visadenied_H1B) visaDenied = "H-1B Visa (Professionals)";
			  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_visa_denied], rowIndex, visaDenied); 
			  
			  //Now, set the determination as to whether or not the AP has childcare experience
			  if (arrayIndex==index_no_childcare_exp) 
				  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_childcare_exp], rowIndex, "No");
			  else APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_childcare_exp], rowIndex, "Yes");
			  
			  //Now, set the determination as to whether or not the AP has graduated high school or expects to graduate soon
			  if (arrayIndex==index_not_graduated_gotdate) 
				  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_grad_highschool], rowIndex, "6/9/2015");
			  else if (arrayIndex==index_not_graduated_nodate) 
				  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_grad_highschool], rowIndex, "No");
			  else APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_grad_highschool], rowIndex, "Yes");
			  
			  //Now, set the presence or absence of a criminal history for the Applicant
			  if (arrayIndex==index_got_criminal_hist) 
				  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_criminal_hist], rowIndex, "Criminal History Description");
			  else APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_criminal_hist], rowIndex, "No");
			  
			  //Now, set the determination as to the Applicant's possession of a driver's license
			  if (arrayIndex==index_no_drivers) 
				  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_drivers], rowIndex, "No");
			  else APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_drivers], rowIndex, "Yes");
			  
			//Now, set the status of the Applicant's Health
			  String health = "Excellent";
			  if (arrayIndex==index_good_health) health = "Good";
			  else if (arrayIndex==index_fair_health) health = "Fair";
			  else if (arrayIndex==index_poor_health) health = "Poor";
			  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_health], rowIndex, health);
			  
			  //Now, set the presence or absence of a physical disability 
			  if (arrayIndex==index_disabled) 
				  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_disability], rowIndex, "Physical Disability Description");
			  else APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_disability], rowIndex, "No");
			  
			  //Now, set the presence or absence of a hospitalization
			  if (arrayIndex==index_got_hosp) 
				  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_hospitalized], rowIndex, "Hospitalization Description");
			  else APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_hospitalized], rowIndex, "No");
			  
			  //Now, set the presence or absence of medication
			  if (arrayIndex==index_got_meds) 
				  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_medication], rowIndex, "Medication Description");
			  else APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_medication], rowIndex, "No");
			  
			  //Now, set the presence or absence of a history of psychiatric care
			  if (arrayIndex==index_got_psych) 
				  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_psychiatric], rowIndex, "Psychiatric Care Description");
			  else APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_psychiatric], rowIndex, "No");
			  
			  //Now, set the presence or absence of a pregnancy
			  if (arrayIndex==index_got_pregnant) 
				  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_pregnant], rowIndex, "Yes");
			  else APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_pregnant], rowIndex, "No");
			  
			  //Now, set the presence or absence of a contagious disease
			  if (arrayIndex==index_got_contagion) 
				  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_contagious], rowIndex, "Contagious Disease Description");
			  else APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_contagious], rowIndex, "No");
			  
			  //Now, set the presence or absence of a drug and/or alcohol dependency
			  if (arrayIndex==index_got_alcydrugs) 
				  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_alc_drg_depend], rowIndex, "Drug and Alcohol Dependency Description");
			  else APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_alc_drg_depend], rowIndex, "No");
			  
			  //Now, set the presence or absence of an allergy
			  if (arrayIndex==index_got_allergy) 
				  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_allergy], rowIndex, "Allergy Description");
			  else APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_allergy], rowIndex, "No");
			  
			  //set the expectation of whether or not the AP applicant is qualified
			  if (	  (arrayIndex==index_prior_US_AP_successful) ||
					  (arrayIndex==index_AP_normal) ||
					  (arrayIndex==index_not_graduated_gotdate) ||
					  (arrayIndex==index_good_health) ||
					  (arrayIndex==index_no_drivers) ||
					  (arrayIndex==index_got_allergy)
					  ) APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_qualified], rowIndex, QUALIFIED);
			  else if (
					  (arrayIndex==index_AP_married) ||
					  (arrayIndex==index_AP_AmericanCitizen) ||
					  (arrayIndex==index_AP_AmericanResident) ||
					  (arrayIndex==index_AP_AmericanNational) ||
					  (arrayIndex==index_AP_too_young) ||
					  (arrayIndex==index_AP_too_old) ||
					  (arrayIndex==index_AP_US_Address) ||
					  (arrayIndex==index_prior_US_AP_unsuccessful) ||
					  (arrayIndex==index_eligibility_US_Citizen) ||
					  (arrayIndex==index_visadenied_J1)
					  ) APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_qualified], rowIndex, UNQUALIFIED);
			  else if (
					  (arrayIndex==index_AP_domesticPartner) ||
					  (arrayIndex==index_AP_maybe_too_old) ||
					  (arrayIndex==index_AP_maybe_too_young) ||
					  (arrayIndex==index_visadenied_B1) ||
					  (arrayIndex==index_visadenied_B2) ||
					  (arrayIndex==index_visadenied_F1) ||
					  (arrayIndex==index_visadenied_F2) ||
					  (arrayIndex==index_visadenied_H1B) ||
					  (arrayIndex==index_no_childcare_exp) ||
					  (arrayIndex==index_not_graduated_nodate) ||
					  (arrayIndex==index_got_criminal_hist) ||
					  (arrayIndex==index_fair_health) ||
					  (arrayIndex==index_poor_health) ||
					  (arrayIndex==index_disabled) ||
					  (arrayIndex==index_got_hosp) ||
					  (arrayIndex==index_got_meds) ||
					  (arrayIndex==index_got_psych) ||
					  (arrayIndex==index_got_pregnant) ||
					  (arrayIndex==index_got_contagion) ||
					  (arrayIndex==index_got_alcydrugs)
					  )APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_qualified], rowIndex, FLAGGED);
			  else APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_qualified], rowIndex, "UNDEFINED");
			  
			  //now, write out the qualifying factor so that it's clear what's under test:
			  APC_Data_File.setCellData(AP_Individual_input_wksheet_name, Individual_AP_Data_Title_Names[index_qualifying_factor], rowIndex, qualifying_factors[arrayIndex-1]);

			  //now, increment so that the value will be different
			  apnum = new Long(new Long(apnum).longValue() + 1).toString();
		  }
		  
		  //now, associate the universal values with the universal names in the Hashtable
		  Universal_AP_Data.put(Universal_AP_Data_NameValues[0], "VALUES");
		  Universal_AP_Data.put(Universal_AP_Data_NameValues[1], apnum);
		  Universal_AP_Data.put(Universal_AP_Data_NameValues[2], ap_password);
		  Universal_AP_Data.put(Universal_AP_Data_NameValues[3], email);
		  Universal_AP_Data.put(Universal_AP_Data_NameValues[4], default_country);
		  Universal_AP_Data.put(Universal_AP_Data_NameValues[5], grad_date);
		  Universal_AP_Data.put(Universal_AP_Data_NameValues[6], arrival_date);
		  Universal_AP_Data.put(Universal_AP_Data_NameValues[7], SFDC_password);
		  Universal_AP_Data.put(Universal_AP_Data_NameValues[8], prior_AP_agency);
		  Universal_AP_Data.put(Universal_AP_Data_NameValues[9], thirteenth_birthdate);
		  Universal_AP_Data.put(Universal_AP_Data_NameValues[10], fourteenth_birthdate);
		  Universal_AP_Data.put(Universal_AP_Data_NameValues[11], fifteenth_birthdate);
		  Universal_AP_Data.put(Universal_AP_Data_NameValues[12], sixteenth_birthdate);
		  Universal_AP_Data.put(Universal_AP_Data_NameValues[13], tenth_birthdate);
		  Universal_AP_Data.put(Universal_AP_Data_NameValues[14], over_weekly_hrs);
		  Universal_AP_Data.put(Universal_AP_Data_NameValues[15], max_weekly_hrs);
		  Universal_AP_Data.put(Universal_AP_Data_NameValues[16], over_daily_hrs);
		  Universal_AP_Data.put(Universal_AP_Data_NameValues[17], max_daily_hrs);
		  Universal_AP_Data.put(Universal_AP_Data_NameValues[18], over_once_hrs);
		  Universal_AP_Data.put(Universal_AP_Data_NameValues[19], max_once_hrs);
		  Universal_AP_Data.put(Universal_AP_Data_NameValues[20], over_monthly_hrs);
		  Universal_AP_Data.put(Universal_AP_Data_NameValues[21], max_monthly_hrs);

/*
 * 	  public static String[] Universal_AP_Data_NameValues = {"NAMES", "apnum", "ap_password", "email", "default_country", "grad_date", 
 * "arrival_date", "SFDC_password", "prior_AP_agency", "thirteenth_birthdate", "fourteenth_birthdate", "fifteenth_birthdate", 
 * "sixteenth_birthdate", "tenth_birthdate", "over_weekly_hrs", "max_weekly_hrs", "over_daily_hrs", "max_daily_hrs", "over_once_hrs", 
 * "max_once_hrs", "over_monthly_hrs", "max_monthly_hrs"};		  
 */

		  /*
		   * Put here a "for" loop which takes all of the "name" values in the Universal_AP_Data_NameValues String array
		   * and associates them with the values that we just calculated.  Eventually, these values will be extracted with the
		   * getValueForName(HSSFSheet sheet, String name) method, accessed by other classes.
		   */
		  //public boolean setCellData(String sheetName,String colName, int rowNum, String data)
		  //APC_Data_File
		  if (! APC_Data_File.addColumn(AP_Universal_input_wksheet_name, Universal_AP_Data_NameValues[0])) 
			  	fail("Could not create NAMES column of universal data value worksheet");
		  if (! APC_Data_File.addColumn(AP_Universal_input_wksheet_name, Universal_AP_Data.get(Universal_AP_Data_NameValues[0]).toString())) 
			  	fail("Could not create VALUES column of universal data value worksheet");
		  for (int arrayIndex=1; arrayIndex<Universal_AP_Data_NameValues.length; arrayIndex++){
			  int rowIndex = arrayIndex + 1;
			  System.out.println("Now working with name " + Universal_AP_Data_NameValues[arrayIndex] +", and value " + Universal_AP_Data.get(Universal_AP_Data_NameValues[arrayIndex]).toString());
			  APC_Data_File.setCellData(AP_Universal_input_wksheet_name, Universal_AP_Data_NameValues[0], rowIndex, Universal_AP_Data_NameValues[arrayIndex]);
			  APC_Data_File.setCellData(AP_Universal_input_wksheet_name, Universal_AP_Data.get(Universal_AP_Data_NameValues[0]).toString(), rowIndex, Universal_AP_Data.get(Universal_AP_Data_NameValues[arrayIndex]).toString());
		  }
		  
/*
 * 		  univ_wksheet.getRow(findRow(univ_wksheet, 
 				  Universal_AP_Data_NameValues[0])).getCell(1).setCellValue(Universal_AP_Data.get(Universal_AP_Data_NameValues[0]).toString());
		  univ_wksheet.getRow(findRow(univ_wksheet, 
				  Universal_AP_Data_NameValues[7])).getCell(1).setCellValue(Universal_AP_Data.get(Universal_AP_Data_NameValues[7]).toString());
*/
		  
		  
		  
	    System.out.println(site_register);    
	    System.out.println(site_login);
	    System.out.println(grad_date);    
	    System.out.println(arrival_date);
	    System.out.println(SFDC_login);
	    System.out.println(SFDC_password);
	    System.out.println(thirteenth_birthdate);
	    System.out.println(fourteenth_birthdate);
	    System.out.println(fifteenth_birthdate);
	    System.out.println(sixteenth_birthdate);
	    System.out.println(tenth_birthdate);
	    System.out.println(over_weekly_hrs);
	    System.out.println(max_weekly_hrs);
	    System.out.println(over_daily_hrs);
	    System.out.println(max_daily_hrs);
	    System.out.println(over_once_hrs);
	    System.out.println(max_once_hrs);
	    System.out.println(over_monthly_hrs);
	    System.out.println(max_monthly_hrs);

  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }
  //this will be what is accessed by the other classes so that they can get to the values in the spreadsheet
  //make sure that "2" is the correct index for the value
  /*
  public static String getValueForName(HSSFSheet sheet, String name){
	  return sheet.getRow(findRow(sheet, name)).getCell(1).getStringCellValue();
  }
  
  
  private static boolean setValueForName (HSSFSheet sheet, String name, String value){
	  int row = findRow(sheet, name);
	  if (row == -1) return false;
	  else {
		  try{
			  sheet.getRow(row).getCell(1).setCellValue(value);
			  return true;  
		  }
		  catch (Throwable t){
			  return false;
		  }
		    
	  }
	  
  }

  private static int findRow(HSSFSheet sheet, String cellContent) {
	    for (Row row : sheet) {
	        for (Cell cell : row) {
	            if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
	                if (cell.getStringCellValue().trim().equals(cellContent)) {
	                    return row.getRowNum();  
	                }
	            }
	        }
	    }               
	    return -1;
	}

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
  */
}
