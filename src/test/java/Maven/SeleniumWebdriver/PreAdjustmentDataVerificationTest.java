package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PreAdjustmentDataVerificationTest {

	protected static String testName;
	protected static String testCaseNumber;
	protected static String testDescription;
	protected static String testEnv;
	protected static StringBuffer verificationErrors;
	protected static BufferedWriter logFileWriter;

	@Before
	public void setUp() throws Exception {
		testName = "PreAdjustmentDataVerificationTest";
		testEnv = "Test";
		testCaseNumber = "81";
		testDescription = "Pre Retention Adjustment Data Verification Test";
		verificationErrors = new StringBuffer();

		String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		logFileWriter = TestFileController.getLogFileWriter(logFileName);
		logFileWriter.write("setUp method of PreAdjustmentDataVerificationTest Class is running");
		logFileWriter.newLine();
	}

	@After
	public void tearDown() throws Exception {
		logFileWriter.write("tearDown method of PreAdjustmentDataVerificationTest Class is running");
		logFileWriter.newLine();
		logFileWriter.close();
	}

	@Test
	public void test() throws Exception{
		logFileWriter.write("test method of PreAdjustmentDataVerificationTest Class is running");
		logFileWriter.newLine();
	}

}
