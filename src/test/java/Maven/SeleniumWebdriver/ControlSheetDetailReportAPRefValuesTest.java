package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.TreeSet;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ControlSheetDetailReportAPRefValuesTest extends ControlSheetReportSuiteAPRefValuesTest{

	private StringBuffer verificationErrors;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws Exception{
		verificationErrors = new StringBuffer();
		String testName = "Verify AP Values in Control Sheet Detail Report";
		System.out.println("Now executing test: " + testName);
		String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		BufferedWriter logFileWriter = TestFileController.getLogFileWriter(logFileName);
		verifyAPValuesInControlSheetDetailReport(logFileWriter);
		storeSummaryValuesPerAPType(logFileWriter);
		TestFileController.writeTestResults("Test", "001", "Comparison of FTE Adjusted Raises", "Control Sheet Detail Report Tests", verificationErrors);
		logFileWriter.close();
	}

	/*
	 * This applies specifically to the Control Sheet Detail Report worksheet in the 
	 * Control Sheet Report Suite. 
	 * 
	 * This will simply call getActualAPFacultyValues to get the values from the 
	 * Control Sheet Detail Report that are to be compared with the expected values, 
	 * and then call the superclass method "verifyAPValuesInControlSheetReportSuite",
	 * passing the actual values to it as a parameter.  
	 * 
	 * The superclass method will gather the expected values and do the comparison 
	 * between the expected and actual values.
	 * 
	 */
	public void verifyAPValuesInControlSheetDetailReport(BufferedWriter logFileWriter) throws Exception{
		TestFileController.ControlSheetReportSuite.setRowForColumnNames(TestFileController.ControlSheetDetailReportColumnHeaderRow);
		//in this case, we have to override the method of the superclass
		String[][] actualValues = getActualAPFacultyValues(logFileWriter);
		String[][] expectedValues = getExpectedAPFacultyValues(logFileWriter);
		super.verifyAPValuesInControlSheetReportSuite(expectedValues, actualValues, logFileWriter);
	}//end verifyAPValuesInControlSheetDetailReport method

	
	/*
	 * This will extract AP-related values from the Control Sheet Detail tab of the Control Sheet Report Suite.xls file and store them
	 * in a two-dimensional String array which it will return.  We will employ the getMultipleValuesInRows method of the 
	 * ExcelReader class.  Here is the description of that particular method:
	 * 
	 * Given a HashMap of column names and values for those columns, and given a list of 
	 * other column names, we want to return a LinkedList of HashMaps - one HashMap for every
	 * row found which contains the column values found for the column names given in the
	 * HashMap which is specified as input.  Each HashMap will contain name-value pairs.
	 * The names are equal to the other column names given in the input list of column names.
	 * The values are equal to the column values found for those columns in the column list
	 * where the name-value criteria given in a HashMap are specified.
	 * 
	 * ... and here's the method declaration:
	 * LinkedList<HashMap<String, String>> getMultipleValuesInRows (String sheetName, 
	 * 													HashMap<String, String> searchCriteria, 
	 * 													String[] columnNames, 
	 * 													BufferedWriter logFileWriter)
	 * 
	 * So, we need a HashMap of column names and values for those columns that comprise our search criteria and we
	 * will use this as one of the inputs.  We will also use an array of String Objects as the columns whose values we want returned.
	 * That will be another input.  The only two other inputs are pretty self-explanatory.
	 * 
	 * We will accept both the aforementioned HashMap and array of String Objects as input parameters from a subclass.
	 * The subclass can instantiate both the HashMap and the array of String Objects and call this method.
	 * 
	 * 
	 * Once we have the LinkedList of HashMaps, we will need to convert it to a two-dimensional array of Strings.
	 * 
	 */

	private String[][] getActualAPFacultyValues(BufferedWriter logFileWriter) throws Exception{
		TestFileController.writeToLogFile("method getActualAPFacultyValues is executing", logFileWriter);
		String[][] facultyNamesDepts = HandSOnTestSuite.apFacultyValues.getUniqueFacultyNameDepartmentCombinations(logFileWriter);
		String[] columnNames = {"Name", "Department", "Description", "FTE Adjusted DO Share Amount"};
		String[][] actualAPFacultyValues = new String[facultyNamesDepts[0].length][columnNames.length];
		for (int facultyNameIndex=0; facultyNameIndex<facultyNamesDepts[0].length; facultyNameIndex++){
			String name = facultyNamesDepts[0][facultyNameIndex];
			String department = facultyNamesDepts[1][facultyNameIndex];
			String description = HandSOnTestSuite.apFacultyValues.getAPReferenceValueForFacultyName(name);
			HashMap<String, String> searchCriteria = new HashMap<String, String>();
			searchCriteria.put("Category", "Faculty Appointment Related");
			searchCriteria.put("Subcategory", description);
			searchCriteria.put("Source Pool", "Regular Merit Pool");
			searchCriteria.put("Department", department);
			searchCriteria.put("Name", name);
			searchCriteria.put("Description", description);
			//first, let's just get the values, then we'll format the values into a two-dimensional array of Strings
			System.out.println("Search Criteria for Control Sheet Detail Report is as follows "+searchCriteria.toString());
			LinkedList<HashMap<String, String>> rawValues 
				= TestFileController.ControlSheetReportSuite.getMultipleValuesInRows(TestFileController.ControlSheetDetailReportWorksheetName, searchCriteria, columnNames, logFileWriter);
			System.out.println("Raw Values gathered from the Control Sheet Detail Report tab is as follows: "+rawValues.toString());
			TestFileController.writeToLogFile("Number of rows in Control Sheet Detail Report that match search criteria: " + rawValues.size(), logFileWriter);
			for (int columnNameIndex=0; columnNameIndex<columnNames.length; columnNameIndex++){
				if (rawValues.size() != 1){
					String warning = "*** WARNING *** There is more than one row with this search criteria - please adjust search criteria";
					System.out.println(warning);
					TestFileController.writeToLogFile(warning, logFileWriter);
				}
				//we're going to assume that there's always one and only one HashMap returned in the LinkedList
				actualAPFacultyValues[facultyNameIndex][columnNameIndex] = (String)rawValues.get(0).get(columnNames[columnNameIndex]);
			}//end inner for - the LinkedList of values
		}//end outer for = facultyNameIndex
		return actualAPFacultyValues;
	}//end getActualAPFacultyValues method

		
		
		/*
		 * This method will use the ExcelReader.getColumnSummaryValues to get the total dollar amounts in "Known" stage for 
		 * each given AP Type.
		 * 
		 * Of note, it will also retrieve the number of discrete faculty for each AP Type.
		 * 
		 * It will use the method to retrieve the values and then put them into the ControlSheetUI Object
		 * and then it will retrieve the summary values that it put into the ControlSheetUI Object from
		 * the ControlSheetUI Object and output them to the log file.
		 * 
		 * Here's the method declaration:
		 * BigDecimal[] getColumnSummaryValues(String sheetName, HashMap<String, String> criteria, 
			boolean emptyDatesExcluded, boolean outsideDatesIncrementFacultyCount, boolean outsideDatesIncrementSalarySum, 
			String[] columnNames, Date minDate, Date maxDate, BufferedWriter logFileWriter)
		 */
		private void storeSummaryValuesPerAPType(BufferedWriter logFileWriter) throws Exception{
			TestFileController.writeToLogFile("method storeTotalDollarAmountsPerAPType has been called", logFileWriter);
			String[] APTypes = HandSOnTestSuite.referenceValues.getLabels();
			String sheetName = TestFileController.ControlSheetDetailReportWorksheetName;
			HashMap<String, String> criteria = new HashMap<String, String>();
			criteria.put("Stage", "Known");
			//criteria.put("Source Pool", "Regular Merit Pool");
			String[] columnNames = {"Name", "Source Pool", "FTE Adjusted DO Share Amount", "Effective Date"};
			for (int APTypeIndex=0; APTypeIndex<APTypes.length; APTypeIndex++){
				criteria.put("Subcategory", APTypes[APTypeIndex]);
				String[][] facultyValues = TestFileController.ControlSheetReportSuite.getMultipleFacultyMultipleValues(sheetName, criteria, 
						columnNames, logFileWriter);
				TreeSet<String> names = new TreeSet<String>();
				BigDecimal dollarSum = new BigDecimal(0);
				for (int rowIndex=0; rowIndex<facultyValues.length;rowIndex++){
					//first of all, add the dollar amount to the total
					long toBeAdded = ((Long)NumberFormat.getNumberInstance(java.util.Locale.US).parse(facultyValues[rowIndex][2])).longValue();
					dollarSum = dollarSum.add(new BigDecimal(toBeAdded));
					//then, check to see if the faculty should be added, and, if so, add the faculty
					boolean isASC = false;
					if ((facultyValues[rowIndex][3].isEmpty()) || (facultyValues[rowIndex][3].equalsIgnoreCase(" "))){//it's not blank in the Control Sheet Detail Report
						isASC = true;
					}//end if - it's an ASC
					boolean isRegularMeritPool = facultyValues[rowIndex][1].equalsIgnoreCase("Regular Merit Pool");
					boolean hasZeroDOShare = facultyValues[rowIndex][2].equalsIgnoreCase("0");
					if ( (isRegularMeritPool && ! isASC) || (! hasZeroDOShare)){
							names.add(facultyValues[rowIndex][0]);
					}//end if - add the name to faculty
				}//end for loop
				HandSOnTestSuite.controlSheetUI.setDollarAmountsForAPType(APTypes[APTypeIndex], dollarSum);
				HandSOnTestSuite.controlSheetUI.setNumberOfFacultyWithAPType(APTypes[APTypeIndex], names.size());
				criteria.remove("Subcategory");
			}//end for loop
			logFileWriter.newLine();
			int[] numberOfFacultyWithAPTypes = HandSOnTestSuite.controlSheetUI.getNumberOfFacultyWithAPTypes();
			BigDecimal[] dollarAmountSums = HandSOnTestSuite.controlSheetUI.getDollarAmountsForAPType();
			for (int APTypeIndex=0; APTypeIndex<APTypes.length; APTypeIndex++){
				logFileWriter.write("Number of discete faculty with Subcategory '" + APTypes[APTypeIndex] + "'="+numberOfFacultyWithAPTypes[APTypeIndex]);
				logFileWriter.write(", and the total dollar amount is " + dollarAmountSums[APTypeIndex].toPlainString());
				logFileWriter.newLine();
			}//end for loop - verifying the number of discrete faculty per AP Type is stored correctly
		}

		/*
		 * This method is not used at the moment because the method "storeSummaryValuesPerAPType" 
		 * already retrieves all discrete faculty names.  
		 * 
		 * However, we may have a use for this code later on, so we keep the method, 
		 * and, at the moment, we simply don't call this method from the "test" method,
		 * so the code is not used.
		 * 
		 * We will use the ExcelReader.getDiscreteOrderedListOfValues method to find all Faculty names found 
		 * that satisfy the search criteria into a TreeSet.
		 * 
		 * This is the method declaration: 
		 * TreeSet<String> getDiscreteOrderedListOfValues(String sheetName, 
		 * 													HashMap<String, String> keysAndValues, 
		 * 													String value, 
		 * 													BufferedWriter logFileWriter)
		 * 
		 * As a TreeSet can only have unique values, this will be a list of all discrete values. The size of
		 * these discrete values will be stored in the instantiated static ControlSheetUI Object 
		 * - one integer value per A & P Reference Value.
		 * 
		 * The search criteria we will use are the following: Stage="Known", Source="Regular Merit Pool" and
		 * Subcategory=<the current APType being examined>
		 */
		
		private void storeNumberOfDiscreteFaculty(BufferedWriter logFileWriter) throws Exception{
			TestFileController.writeToLogFile("method storeNumberOfDiscreteFaculty has been called", logFileWriter);
			String[] APTypes = HandSOnTestSuite.referenceValues.getLabels();
			String sheetName = TestFileController.ControlSheetDetailReportWorksheetName;
			HashMap<String, String> keysAndValues = new HashMap<String, String>();
			keysAndValues.put("Stage", "Known");
			//keysAndValues.put("Source Pool", "Regular Merit Pool");
			String value = "Name";
			for (int APTypeIndex=0; APTypeIndex<APTypes.length; APTypeIndex++){
				keysAndValues.put("Subcategory", APTypes[APTypeIndex]);
				HandSOnTestSuite.controlSheetUI.setNumberOfFacultyWithAPType(APTypes[APTypeIndex],
									TestFileController.ControlSheetReportSuite.getDiscreteOrderedListOfValues(sheetName, keysAndValues, value, logFileWriter).size());
				keysAndValues.remove("Subcategory");
			}//end for loop
			logFileWriter.newLine();
			int[] numberOfFacultyWithAPTypes = HandSOnTestSuite.controlSheetUI.getNumberOfFacultyWithAPTypes();
			for (int APTypeIndex=0; APTypeIndex<APTypes.length; APTypeIndex++){
				logFileWriter.write("Number of discete faculty with Subcategory '" + APTypes[APTypeIndex] + "' = "+numberOfFacultyWithAPTypes[APTypeIndex]);
				logFileWriter.newLine();
			}//end for loop - verifying the number of discrete faculty per AP Type is stored correctly
		}//end method storeNumberOfDiscreteFaculty


}
