package Maven.SeleniumWebdriver;

import org.apache.poi.hssf.usermodel.HSSFCreationHelper;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFHyperlink;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFDataFormatter;

import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.hssf.usermodel.FontDetails;


import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.*;

//import jxl.Hyperlink;

import org.apache.poi.ss.usermodel.Color;
//import java.awt.Color;
import java.io.*;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.TreeSet;
import java.util.Properties;

public class HSSFExcelReader {
	public  String path;
	public  FileInputStream fis = null;
	public  FileOutputStream fileOut =null;
	private HSSFWorkbook workbook = null;
	private HSSFSheet sheet = null;
	private HSSFRow row   =null;
	private HSSFCell cell = null;
	private int rowForColumnNames = 0;
	
	public HSSFExcelReader(String path) {
		
		this.path=path;
		try {
			fis = new FileInputStream(path);
			workbook = new HSSFWorkbook(fis);
			sheet = workbook.getSheetAt(0);
			fis.close();
		} catch (Exception e) {
			
			e.printStackTrace();
		} 
		
	}
	
	public void setRowForColumnNames(int newRowForColumnNames){
		rowForColumnNames = newRowForColumnNames;
	}
	
	public int getRowForColumnNames (){
		return rowForColumnNames;
	}
	
	public void closeWorkbook() throws Exception {
		fileOut.flush();
		fileOut.close();
		workbook.close();
	}
	
	public HSSFExcelReader(String path, int rowForColumnNames) throws Exception{
		ZipSecureFile.setMinInflateRatio(0.0d);
		this.rowForColumnNames = rowForColumnNames;
		this.path=path;
		try {
			fis = new FileInputStream(path);
			workbook = new HSSFWorkbook(fis);
			sheet = workbook.getSheetAt(0);
			fis.close();
		} catch (FileNotFoundException e) {	
			System.out.println("Could not open the file because the file was not found.... Attempting to create the file");
			fileOut = new FileOutputStream(path);
			workbook = new HSSFWorkbook();
			workbook.write(fileOut);
		}
		//System.out.println("Excel file with column names initialized at row "+ rowForColumnNames);
	}
	
	// returns the row count in a sheet
	public int getRowCount(String sheetName){
		int index = workbook.getSheetIndex(sheetName);
		if(index==-1)
			return 0;
		else{
		sheet = workbook.getSheetAt(index);
		int number=sheet.getLastRowNum()+1;
		return number;
		}
		
	}
	
	public int getNumberOfSheets() throws Exception{
		return workbook.getNumberOfSheets();
	}

	
	public String[] getColumns(String sheetName){
		int index = workbook.getSheetIndex(sheetName);
		if(index==-1)
			return new String[0];
		
		sheet = workbook.getSheetAt(index);
		row=sheet.getRow(rowForColumnNames);
		String[] columnNames = new String[row.getLastCellNum()];
		for(int i=0;i<row.getLastCellNum();i++){
			//System.out.println(row.getCell(i).getStringCellValue().trim());
			columnNames[i] = row.getCell(i).getStringCellValue().trim();
		}
		return columnNames;
	}
	
	
	
	// returns the data from a cell
	public String getCellData(String sheetName,String colName,int rowNum){
		//System.out.println("method getCellData has been called with worksheet " + sheetName + ", column name: " + colName + ", row number " + rowNum);
		try{
			if(rowNum <=0)
				return "";
		
		int index = workbook.getSheetIndex(sheetName);
		int col_Num=-1;
		if(index==-1)
			return "";
		String cellText = "";
		sheet = workbook.getSheetAt(index);
		row=sheet.getRow(rowForColumnNames);
		//System.out.println("Searching column names in worksheet " + sheetName + " for column " + colName);
		for(int i=0;i<row.getLastCellNum();i++){
			HSSFCell cell = row.getCell(i);
			if(cell.getCellType()==CellType.STRING) cellText = cell.getStringCellValue();
			else if(cell.getCellType()==CellType.NUMERIC || cell.getCellType()==CellType.FORMULA ){
				//System.out.println("Numeric value is being processed");
				cellText  = new HSSFDataFormatter(java.util.Locale.US).formatCellValue(cell);
			}
			//System.out.print("Column name being evaluated is " + cellText.trim());
			if(cellText.trim().equals(colName.trim())){
				//System.out.println(", MATCH - column number: " + i);
				col_Num=i;
				break;
			}//end if - we found the correct cell
			//else System.out.println(", no match for column number " + i);
		}
		//System.out.println();
		//System.out.println("...for worksheet " + sheetName);
		if(col_Num==-1)
			return "";
		
		sheet = workbook.getSheetAt(index);
		row = sheet.getRow(rowNum-1);
		if(row==null){
			//System.out.println("row is null - returning a blank");
			return "";
		}
		cell = row.getCell(col_Num);
		
		if(cell==null){
			//System.out.println("cell is null - returning a blank");
			return "";
		}
		FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
		cell = (HSSFCell)evaluator.evaluateInCell(cell);
		CellType cellType = cell.getCellType();
		if(cellType==CellType.STRING){
			  //System.out.println("returning value " + cell.getStringCellValue());
			  return cell.getStringCellValue();
		}
		else if(cellType==CellType.NUMERIC || cellType==CellType.FORMULA ){
			  
			  //String cellText  = String.valueOf(cell.getNumericCellValue());
			cellText = new HSSFDataFormatter(java.util.Locale.US).formatCellValue(cell);
			//System.out.println("cell text for worksheet " + sheetName + " is being returned as " + cellText);
			  if (HSSFDateUtil.isCellDateFormatted(cell)) {
		           
				  double d = cell.getNumericCellValue();

				  Calendar cal =Calendar.getInstance();
				  cal.setTime(HSSFDateUtil.getJavaDate(d));
		            cellText =
		             (String.valueOf(cal.get(Calendar.YEAR))).substring(2);
		           cellText = cal.get(Calendar.DAY_OF_MONTH) + "/" +
		                      cal.get(Calendar.MONTH)+1 + "/" + 
		                      cellText;
		           
		          

		         }

			  
			  
			  return cellText;
		  }else if(cellType==CellType.BLANK)
		      return ""; 
		  else 
			  return String.valueOf(cell.getBooleanCellValue());
		
		}
		catch(Exception e){
			
			e.printStackTrace();
			return "row "+rowNum+" or column "+colName +" does not exist in xls";
		}
	}
	
	/*
	 * The goal of this method is to calculate and return the width of a column,
	 * given the name of the worksheet, and the column number as input parameters
	 */
	public int getColumnWidthInCharacters(String sheetName, int colNum, BufferedWriter logFileWriter) throws Exception{
		HSSFSheet worksheet;
		try{
			if(colNum <=0)
				return 0;
		
			int index = workbook.getSheetIndex(sheetName);
			if(index==-1)
				return 0;
			worksheet = workbook.getSheetAt(index);
			return worksheet.getColumnWidth(colNum) / 256;
		}
		catch (Exception e){
			logFileWriter.write("Exception has been thrown: " + e.getMessage());
			e.printStackTrace();
			return 0;
		}
	}
	

	
	// returns the data from a cell
	public String getCellData(String sheetName,int colNum,int rowNum){
		try{
			if(rowNum <=0)
				return "";
		
		int index = workbook.getSheetIndex(sheetName);

		if(index==-1)
			return "";
		
	
		sheet = workbook.getSheetAt(index);
		row = sheet.getRow(rowNum-1);
		if(row==null)
			return "";
		cell = row.getCell(colNum);
		if(cell==null)
			return "";
		
	  if(cell.getCellType()==CellType.STRING)
		  return cell.getStringCellValue();
	  else if(cell.getCellType()==CellType.NUMERIC || cell.getCellType()==CellType.FORMULA ){
		  
		  //String cellText  = String.valueOf(cell.getNumericCellValue());
			String cellText  = new HSSFDataFormatter(java.util.Locale.US).formatCellValue(cell);
			//System.out.println("cell text for worksheet " + sheetName + " is being returned as " + cellText);
		  if (HSSFDateUtil.isCellDateFormatted(cell)) {
	           // format in form of M/D/YY
			  double d = cell.getNumericCellValue();

			  Calendar cal =Calendar.getInstance();
			  cal.setTime(HSSFDateUtil.getJavaDate(d));
	            cellText =
	             (String.valueOf(cal.get(Calendar.YEAR))).substring(2);
	           cellText = cal.get(Calendar.MONTH)+1 + "/" +
	                      cal.get(Calendar.DAY_OF_MONTH) + "/" +
	                      cellText;
	           
	         

	         }

		  
		  
		  return cellText;
	  }else if(cell.getCellType()==CellType.BLANK)
	      return "";
	  else 
		  return String.valueOf(cell.getBooleanCellValue());
		}
		catch(Exception e){
			
			e.printStackTrace();
			return "row "+rowNum+" or column "+colNum +" does not exist  in xls";
		}
	}
	
	
	
	
	// returns true if data is set successfully else false
	public boolean setCellData(String sheetName,String colName,int rowNum, String data){
		try{
		fis = new FileInputStream(path); 
		workbook = new HSSFWorkbook(fis);

		if(rowNum<=0)
			return false;
		
		int index = workbook.getSheetIndex(sheetName);
		int colNum=-1;
		if(index==-1)
			return false;
		
		
		sheet = workbook.getSheetAt(index);
		

		row=sheet.getRow(rowForColumnNames);
		for(int i=0;i<row.getLastCellNum();i++){
			//System.out.println(row.getCell(i).getStringCellValue().trim());
			if(row.getCell(i).getStringCellValue().trim().equals(colName))
				colNum=i;
		}
		if(colNum==-1)
			return false;

		sheet.autoSizeColumn(colNum); 
		row = sheet.getRow(rowNum-1);
		if (row == null)
			row = sheet.createRow(rowNum-1);
		
		cell = row.getCell(colNum);	
		if (cell == null)
	        cell = row.createCell(colNum);

	    
	    cell.setCellValue(data);

	    fileOut = new FileOutputStream(path);

		workbook.write(fileOut);

	    fileOut.close();	

		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	
	
	// returns true if data is set successfully else false
	public boolean setCellData(String sheetName,String colName,int rowNum, String data,String url){
		
		try{
		fis = new FileInputStream(path); 
		workbook = new HSSFWorkbook(fis);

		if(rowNum<=0)
			return false;
		
		int index = workbook.getSheetIndex(sheetName);
		int colNum=-1;
		if(index==-1)
			return false;
		
		
		sheet = workbook.getSheetAt(index);
		
		row=sheet.getRow(rowForColumnNames);
		for(int i=0;i<row.getLastCellNum();i++){
			
			if(row.getCell(i).getStringCellValue().trim().equalsIgnoreCase(colName))
				colNum=i;
		}
		
		if(colNum==-1)
			return false;
		sheet.autoSizeColumn(colNum); 
		row = sheet.getRow(rowNum-1);
		if (row == null)
			row = sheet.createRow(rowNum-1);
		
		cell = row.getCell(colNum);	
		if (cell == null)
	        cell = row.createCell(colNum);
			
	    cell.setCellValue(data);

	    //cell style for hyperlinks
	    
	    CellStyle hlink_style = workbook.createCellStyle();
	    HSSFFont hlink_font = workbook.createFont();
	    hlink_font.setUnderline(XSSFFont.U_SINGLE);
	    hlink_font.setColor(IndexedColors.BLUE.getIndex());
	    hlink_style.setFont(hlink_font);
	    //hlink_style.setWrapText(true);
	    
	    HSSFCreationHelper createHelper = workbook.getCreationHelper();

	    //HSSFHyperlink link = createHelper.createHyperlink(HSSFHyperlink.LINK_FILE);
	    Hyperlink link = (Hyperlink) createHelper.createHyperlink(HyperlinkType.FILE);
	    link.setAddress(url);
	    cell.setHyperlink(link);
	    cell.setCellStyle(hlink_style);
	    
	    fileOut = new FileOutputStream(path);
		workbook.write(fileOut);

	    fileOut.close();	

		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public String getCellHyperLinkURL(String sheetName,String colName,int rowNum){
		try{
			if(rowNum <=0)
				return "";
		
		int index = workbook.getSheetIndex(sheetName);
		int col_Num=-1;
		if(index==-1)
			return "";
		
		sheet = workbook.getSheetAt(index);
		row=sheet.getRow(rowForColumnNames);
		for(int i=0;i<row.getLastCellNum();i++){
			//System.out.println(row.getCell(i).getStringCellValue().trim());
			if(row.getCell(i).getStringCellValue().trim().equals(colName.trim()))
				col_Num=i;
		}
		if(col_Num==-1)
			return "";
		
		sheet = workbook.getSheetAt(index);
		row = sheet.getRow(rowNum-1);
		if(row==null)
			return "";
		cell = row.getCell(col_Num);
		
		if(cell==null)
			return "";
		}
		catch(Exception e){
			
			e.printStackTrace();
			return "";
			
		}

		
		
	    return cell.getHyperlink().getAddress();

	}
	
	// returns true if sheet is created successfully else false
	public boolean addSheet(String  sheetname){		
		
		FileOutputStream fileOut;
		try {
			 workbook.createSheet(sheetname);	
			 fileOut = new FileOutputStream(path);
			 workbook.write(fileOut);
		     fileOut.close();		    
		} catch (Exception e) {			
			System.out.println("Couldn't create the sheet because of "+e.getMessage());
			return false;
		}
		return true;
	}
	
	// returns true if sheet is removed successfully else false if sheet does not exist
	public boolean removeSheet(String sheetName){		
		int index = workbook.getSheetIndex(sheetName);
		if(index==-1)
			return false;
		
		FileOutputStream fileOut;
		try {
			workbook.removeSheetAt(index);
			fileOut = new FileOutputStream(path);
			workbook.write(fileOut);
		    fileOut.close();		    
		} catch (Exception e) {			
			e.printStackTrace();
			return false;
		}
		return true;
	}
	// returns true if column is created successfully
	public boolean addColumn(String sheetName,String colName){
		
		
		try{				
			fis = new FileInputStream(path); 
			workbook = new HSSFWorkbook(fis);
			int index = workbook.getSheetIndex(sheetName);
			if(index==-1)
				return false;
			
		HSSFCellStyle style = workbook.createCellStyle();
		style.setFillForegroundColor(HSSFColor.HSSFColorPredefined.GREY_40_PERCENT.getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		
		sheet=workbook.getSheetAt(index);
		
		row = sheet.getRow(rowForColumnNames);
		if (row == null)
			row = sheet.createRow(0);
		
		
		if(row.getLastCellNum() == -1)
			cell = row.createCell(0);
		else
			cell = row.createCell((int)row.getLastCellNum());
	        
	        cell.setCellValue(colName);
	        cell.setCellStyle(style);
	        
	        fileOut = new FileOutputStream(path);
			workbook.write(fileOut);
		    fileOut.close();		    

		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
		return true;
		
		
	}
	
	
	
	// removes a column and all the contents
	public boolean removeColumn(String sheetName, int colNum) {
		try{
		if(!isSheetExist(sheetName))
			return false;
		fis = new FileInputStream(path); 
		workbook = new HSSFWorkbook(fis);
		sheet=workbook.getSheet(sheetName);
		HSSFCellStyle style = workbook.createCellStyle();
		style.setFillForegroundColor(HSSFColor.HSSFColorPredefined.GREY_40_PERCENT.getIndex());
		style.setFillPattern(FillPatternType.NO_FILL);
		
	    
	
		for(int i =rowForColumnNames;i<getRowCount(sheetName);i++){
			row=sheet.getRow(i);	
			if(row!=null){
				cell=row.getCell(colNum);
				if(cell!=null){
					cell.setCellStyle(style);
					row.removeCell(cell);
				}
			}
		}
		fileOut = new FileOutputStream(path);
		workbook.write(fileOut);
	    fileOut.close();
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
		
	}
	
	
  // find whether sheets exists	
	public boolean isSheetExist(String sheetName){
		int index = workbook.getSheetIndex(sheetName);
		if(index==-1){
			index=workbook.getSheetIndex(sheetName.toUpperCase());
				if(index==-1)
					return false;
				else
					return true;
		}
		else
			return true;
	}
	
	/*
	 * This returns a TreeSet - a discrete, ordered list of values - for one or more columns 
	 * (given as an array of input parameters), given another column name and a value String as input parameters.
	 * The value String given as an input parameter is a substring used to determine which 
	 * rows contain the desired data).  If the values in the second column name contain the
	 * value String as a substring, the values of the array of columns is added to the TreeSet.
	 * It iterates through the entire worksheet of the file under test until it has found
	 * all matches and then it returns the TreeSet.
	 */
	
	public TreeSet<String> getDiscreteOrderedListOfValues(String sheetName, String searchString, String searchColumn, String[] columnNames, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("method getDiscreteOrderedListOfValues has been called");
		logFileWriter.newLine();
		TreeSet<String> matches = new TreeSet<String>();
		/*
		 * iterate through the worksheet, finding all values for columnName that have
		 * a value for searchColumn that meet the search criteria.  The search criteria
		 * is that the value for SearchColumn must contain the searchString as a substring
		 * or match the value of the searchString.
		 */
		int foundRowIndex = getCellRowNumWithSubstringMatch(sheetName, searchColumn, searchString, rowForColumnNames);
		while (foundRowIndex != -1){
			String foundValue = getCellData(sheetName, columnNames[0], foundRowIndex);
			if (columnNames.length > 1){
				for (int i=1; i<columnNames.length; i++){
					foundValue = foundValue.concat("-"+getCellData(sheetName, columnNames[i], foundRowIndex));
				}//end for
			}//end if
			matches.add(foundValue);
			logFileWriter.write("Found value " + foundValue +" at row " + foundRowIndex);
			logFileWriter.newLine();
			//now, advance the initial row index to search from so that it equals the value of the index it found
			foundRowIndex = getCellRowNumWithSubstringMatch(sheetName, searchColumn, searchString, foundRowIndex + 1);
		}
		
		return matches;
	}
	
	/*
	 * returns a HashMap of key-value pairs - the key is the row # and the value is the value
	 * found for the column name given as a parameter for that row#, given the search String
	 * given as another parameter and the search Column given as another parameter.  
	 * It returns the row numbers and values for the column name, where the search string is 
	 * found as the value for the search column
	 */
	public HashMap<Integer, String> getMultipleCellValues(String sheetName, String searchString, String searchColumn, String columnName, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("method getMultipleCellValues has been called");
		logFileWriter.newLine();
		HashMap<Integer, String> cache = new HashMap<Integer, String>();
		
		//initialize the first key in the HashMap to be returned
		int foundRowIndex = getCellRowNum(sheetName, searchColumn, searchString, rowForColumnNames);
		while (foundRowIndex != -1){
			String foundValue = getCellData(sheetName, columnName, foundRowIndex);
			cache.put(foundRowIndex, foundValue);
			logFileWriter.write("Found value " + foundValue +" at row " + foundRowIndex);
			logFileWriter.newLine();
			//now, advance the initial row index to search from so that it equals the value of the index it found
			foundRowIndex = getCellRowNum(sheetName, searchColumn, searchString, foundRowIndex + 1);
		}
		
		return cache;
	}
	
	  /*
	   * Given a HashMap of column names and values for those columns, and given a list of 
	   * other column names, we want to return a LinkedList of HashMaps - one HashMap for every
	   * row found which contains the column values found for the column names given in the
	   * HashMap which is specified as input.  Each HashMap will contain name-value pairs.
	   * The names are equal to the other column names given in the input list of column names.
	   * The values are equal to the column values found for those columns in the column list
	   * where the name-value criteria given in a HashMap are specified.
	   */
	
	public LinkedList<HashMap<String, String>> getMultipleValuesInRows (String sheetName, HashMap<String, String> searchCriteria, String[] columnNames, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("****Method HSSFExcelReader.getMultipleValuesInRows has been called for worksheet " + sheetName + "****");
		logFileWriter.newLine();		
		
		LinkedList<HashMap<String, String>> matchedRows = new LinkedList<HashMap<String, String>>();
		
		//start with the first name-value pair in the HashMap - look for a row that matches it
		//rowIndex will store the row number we're in so we don't try matching the same row twice
		String name = (String)searchCriteria.keySet().toArray()[0];
		String value = (String)searchCriteria.get(name);
		int rowIndex = getCellRowNum(sheetName, name, value, rowForColumnNames);
		String message = "initial value found at row " + rowIndex +"\n";
		logFileWriter.write(message);
		//System.out.println(message);
		
		//write the first column and value to the output file and indicate that a match is found
		message = "first criterion column name: " + name + ", first criterion column value: " + value;
		logFileWriter.write(message);
		//System.out.println(message);
		logFileWriter.newLine();
		int numberOfCriteria = searchCriteria.size();
		//look for rows that match the searchCriteria
		//while we still haven't gone through every row in the worksheet
		while( rowIndex != -1){
			
			String foundValue = getCellData(sheetName, name, rowIndex);
			boolean match = true;
			//next, when a match is found, loop (inner loop) through the rest of the name-value pairs
			for (int criterionIndex = 1; criterionIndex < numberOfCriteria && match; criterionIndex++){
				//send information to the output file - it matches the criteria we've examined so far
				//System.out.println(", match");
				//do a comparison on the value in the next column in the row that matched the first value
				//update name and value to the next column name and value
				name = (String)searchCriteria.keySet().toArray()[criterionIndex];
				value = (String)searchCriteria.get(name);
				message = "column name : " + name + ", column value : " + value;
				//System.out.println(message);
				logFileWriter.write(message);
				//retrieve the cell value in the next column in the same row
				foundValue = getCellData(sheetName, name, rowIndex);
				//compare the actual value found to the expected value
				match = foundValue.equalsIgnoreCase(value);
				//if match = false, write it to the output file
				if (match){
					logFileWriter.write(", match");
				}//end if - match
				else{
					//System.out.println(", MISMATCH - going on to find the next match");
					//logFileWriter.write(", MISMATCH - going on to find the next match");
				}// end else - mismatch
				logFileWriter.newLine();
			}// end inner for loop
			/* 
			 * If we've gone all the way through all the criteria and it matches the criteria, 
			 * then we want the values in this row that correspond to the column names given as input
			 */
			
			if(match){
				logFileWriter.newLine();
				message = "Found a matching row (row number " +rowIndex + ")- creating HashMap of matching row values";
				//System.out.println(message);
				logFileWriter.write(message);
				logFileWriter.newLine();
				HashMap<String, String> matchedRow = new HashMap<String, String>();
				//loop through all the column names and retrieve values in this row for them
				for (int columnIndex = 0; columnIndex<columnNames.length; columnIndex++){
					String targetColumn = columnNames[columnIndex];
					String targetValue = getCellData(sheetName, targetColumn, rowIndex);
					logFileWriter.write("Adding value... column name: " + targetColumn);
					logFileWriter.write(", column value: " + targetValue);
					logFileWriter.newLine();
					matchedRow.put(targetColumn, targetValue);
				}// end for - all target values in the row have been added to this HashMap
				matchedRows.add(matchedRow);
				logFileWriter.write("Added the matching row at row index " + rowIndex + " to the list of matches");
				logFileWriter.newLine();
			}//end if - the match criteria for the row is satisfied

			//now that we know we've had a matched row, reset the search criterion
			name = (String)searchCriteria.keySet().toArray()[0];
			value = (String)searchCriteria.get(name);
			
			//increment the rowIndex and search for the next match on the first criterion
			rowIndex = getCellRowNum(sheetName, name, value, ++rowIndex);
			if (rowIndex == -1){
				//System.out.println("The worksheet " + sheetName + " has been searched.  There are no more matches.");
				logFileWriter.write("The worksheet " + sheetName + " has been searched.  There are no more matches.");
				logFileWriter.newLine();
			}//end if - the output file has been updated with the information

		}// end of outer while loop - we're all done adding HashMaps to the LinkedList of HashMaps
		
		//and when we're finally finished going through all of the rows of the worksheet, return the LinkedList
		return matchedRows;
	}
	
	/*
	 * The objective of this method is to return one or more sums of numbers in column names given as 
	 * input. A HashMap of column names and values is also given as input.  A two-dimensional String array
	 * is also given as Market Criteria.  It may or may not be used.  If it is empty, it is not used.  Also,
	 * maximum and minimum dates are given to specify the date range for the data to be returned.
	 * The HashMap lists the criteria for numbers to be included in the sum(s) to be returned.
	 * These are criteria which must all be satisfied to be included in the summary information to 
	 * be returned.  
	 * The boolean indicates whether or not Market Criteria is there which make faculty eligible to be 
	 * included in the summary information.  Only one set of Market Criteria needs to be satisfied to make the 
	 * faculty eligible.  The Market Criteria are read from another input worksheet, the name of which is
	 * stored as a constant. 
	 * There are also two date values given as input - one is the latest date, and the other is the
	 * earliest date.
	 * The numbers to be returned are returned as a sum of rows in which the conditions are satisfied
	 * and one or more sums of the columns the names of which are given as input.  They will be returned
	 * as an array of BigDecimal values, the size of which is equal to the size of the array (of column names
	 * given as input) plus one.  The sum is always the first number in the array of BigDecimal values
	 * returned.
	 */
	
	public BigDecimal[] getColumnSummaryValues(String sheetName, HashMap<String, String> criteria, 
			boolean emptyDatesExcluded, boolean outsideDatesIncrementFacultyCount, boolean outsideDatesIncrementSalarySum, 
			String[] columnNames, Date minDate, Date maxDate, BufferedWriter logFileWriter) throws Exception{
		//first, record what method is being called
		//System.out.println("method getColumnSummaryValues has been called");
		logFileWriter.write("method getColumnSummaryValues has been called");
		logFileWriter.newLine();
		
		if (emptyDatesExcluded){
			logFileWriter.write("Rows with empty dates for Effective Dates will NOT increment the number of faculty and the sum of DO share salary increases");
			logFileWriter.newLine();
		}
		else {
			logFileWriter.write("Rows with empty dates for Effective Dates will STILL increment the number of faculty and the sum of DO share salary increases");
			logFileWriter.newLine();
		}
		if (outsideDatesIncrementFacultyCount){
			logFileWriter.write("Rows with dates outside the accepted range will still increment the number of faculty");
			logFileWriter.newLine();
		}
		else{
			logFileWriter.write("Rows with dates outside the accepted range will NOT increment the number of faculty");
			logFileWriter.newLine();
		}
		if (outsideDatesIncrementSalarySum){
			logFileWriter.write("Rows with dates outside the accepted range will still increment the sum of DO share salary increases");
			logFileWriter.newLine();
		}
		else{
			logFileWriter.write("Rows with dates outside the accepted range will NOT increment the sum of DO share salary increases");
			logFileWriter.newLine();
		}

		
		
		BigDecimal[] summary = new BigDecimal[columnNames.length + 1];
		for (int i=0; i<summary.length; i++) summary[i] = new BigDecimal(0);
		//System.out.println("Number of values to be returned is " + summary.length);
		logFileWriter.write("Number of values to be returned is " + summary.length);
		logFileWriter.newLine();
		TreeSet<String> names = new TreeSet<String>();
		
		/*
		 * Read through the entire worksheet, and find rows which satisfy the input criteria, including the max and min Dates.  
		 * When we find one row that satisfies the first input criterion, iterate through the rest of the input criteria and verify
		 * that the rest of the input criteria are satisfied for that row.  If the input criteria are satisfied for 
		 * that row, then increment the count of rows found by one, and then add the value(s) found for that row
		 * for the column name(s) into the summary value(s) being returned.
		 */
		
		//iterate through the worksheet, finding the first row that satisfies the first input criterion
		//start with the first name-value pair in the HashMap - look for a row that matches it
		//rowIndex will store the row number we're in so we don't try matching the same row twice
		String name = (String)criteria.keySet().toArray()[0];
		String value = (String)criteria.get(name);
		//write the first column and value to the output file and indicate that a match is found
		//System.out.print("criterion column name : " + name);
		logFileWriter.write("criterion column name : " + name);
		//System.out.print(", criterion column value : " + value);
		logFileWriter.write(", criterion column value : " + value);
		int rowIndex = getCellRowNum(sheetName, name, value, rowForColumnNames);
		//System.out.print(", found at row number " + rowIndex);
		logFileWriter.write(", found at row number " + rowIndex); 
		//when we find the first row that satisfies the first input criterion, iterate through the rest of the criteria, see if it still fits
		//write the first column and value to the output file and indicate that a match is found
		
		int numberOfCriteria = criteria.size();
		//look for rows that match the searchCriteria
		//while we still haven't gone through every row in the worksheet
		while( rowIndex != -1){
			
			String foundValue = getCellData(sheetName, name, rowIndex);
			boolean match = true;//reset this every time we examine another row of data
			//next, when a match is found, loop (inner loop) through the rest of the name-value pairs
			for (int criterionIndex = 1; criterionIndex < numberOfCriteria && match; criterionIndex++){
				//send information to the output file - it matches the criteria we've examined so far
				//System.out.print(", data found: " + foundValue);
				logFileWriter.write(", data found: " + foundValue);
				//System.out.println(", Criteria match");
				logFileWriter.write(", Criteria match");
				logFileWriter.newLine();
		
				//if it satisfies all the Criteria, verify that it falls within the correct date range (between max and min dates)
				name = (String)criteria.keySet().toArray()[criterionIndex];
				value = (String)criteria.get(name);
				//System.out.print("criterion column name : " + name);
				logFileWriter.write("criterion column name : " + name);
				//System.out.print(", criterion column value : " + value);
				logFileWriter.write(", criterion column value : " + value);
				//retrieve the cell value in the next column in the same row
				foundValue = getCellData(sheetName, name, rowIndex);
				//compare the actual value found to the expected value
				if(! foundValue.equalsIgnoreCase(value)) match = false;
				//if match = false, write it to the output file
				if (! match){
					//System.out.println(", MISMATCH - going on to find the next match");
					//logFileWriter.write(", MISMATCH - going on to find the next match");
					//logFileWriter.newLine();
					break;//get out of the "for" loop
				}// end if
				else{//there's a match - check that it falls within the date range and satisfies Criteria, if specified
					//check the date range first ... that's always required
					logFileWriter.newLine();
					logFileWriter.write("Criteria match at row " +rowIndex);
					logFileWriter.newLine();
					//System.out.print("Checking Date Range ... earliest date is " + minDate.toString());
					logFileWriter.write("Checking Date Range ... earliest date is " + minDate.toString());
					//System.out.print(", latest date is " + maxDate.toString());
					logFileWriter.write(", latest date is " + maxDate.toString());
					//System.out.print(", and the date found is: ");
					logFileWriter.write(", and the date found is: ");
					Date effectiveDate = null;
					try{
						DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
						String effectiveDateString = getCellData(sheetName, "Effective Date", rowIndex);
						logFileWriter.write("'" + effectiveDateString +"', ");
						if ((! effectiveDateString.isEmpty()) && (! effectiveDateString.equalsIgnoreCase(" "))){//it's not blank in the Control Sheet Detail Report
							effectiveDate = formatter.parse(effectiveDateString);
						} 
						else {
							logFileWriter.write("a blank value");//leave effectiveDate as null - this is an ASC
							logFileWriter.newLine();
						}
					}
					catch(Exception e){
						//System.out.println("not a determined date.  Exception thrown:");
						logFileWriter.write("not a determined date.  Exception thrown:");
						//System.out.println(e.toString());
						logFileWriter.write(e.toString());
						logFileWriter.newLine();
						match = false;
					}
					if (effectiveDate == null){
						if (emptyDatesExcluded){
							match = false;
							logFileWriter.newLine();
							logFileWriter.write("This row (Row " + rowIndex +") represents an ASC - it is not counted in this case");
							logFileWriter.newLine();
						}
						else{ //with every case except for Market Retention, an ASC is counted
							logFileWriter.newLine();
							logFileWriter.write("This row (Row " + rowIndex +") represents an ASC - it is still included in this case");
							logFileWriter.newLine();
						}
					}
					else if ((effectiveDate.after(minDate) || effectiveDate.equals(minDate)) && (effectiveDate.before(maxDate) || effectiveDate.equals(maxDate))){
						//System.out.print(effectiveDate.toString());
						//logFileWriter.write(effectiveDate.toString());
						//System.out.println(", and the date matches");
						logFileWriter.write(", and the date falls into the required range");
						logFileWriter.newLine();
					}
					else{
						match = false;//if the date doesn't fall in the right range, it's not a match
						logFileWriter.write(", and the date doesn't fall in the required range");
						logFileWriter.newLine();
						// ... but it might still increment the faculty count and DO share salary increase sum
						if (outsideDatesIncrementFacultyCount){
							logFileWriter.write("However, the number of faculty is still incremented by row " + rowIndex);
							logFileWriter.newLine();
							names.add(getCellData(sheetName, "Name", rowIndex));
							summary[0] = new BigDecimal(names.size());
							logFileWriter.write("Incrementing count of faculty found to " + summary[0].toPlainString());
							logFileWriter.newLine();
						}
						else{
							logFileWriter.write("The number of faculty is NOT incremented by row " + rowIndex);
							logFileWriter.newLine();
						}
						if(outsideDatesIncrementSalarySum){
							logFileWriter.write("However, the DO share salary increase sum is still incremented by row " + rowIndex);
							logFileWriter.newLine();
							for (int columnIndex = 0; columnIndex<columnNames.length; columnIndex++){
								long addString = ((Long)NumberFormat.getNumberInstance(java.util.Locale.US).parse(getCellData(sheetName, columnNames[columnIndex], rowIndex))).longValue();
								BigDecimal toBeAdded = new BigDecimal(addString);
								summary[columnIndex+1] = summary[columnIndex+1].add(toBeAdded);
								//System.out.print(", and adding " + toBeAdded.toPlainString() + " to sum of " +summary[columnIndex+1].toPlainString());
								logFileWriter.write(", and adding " + toBeAdded.toPlainString() + " to sum of " +summary[columnIndex+1].toPlainString());
							}
							logFileWriter.newLine();
						}
						else{
							logFileWriter.write("The DO share salary increase sum is NOT incremented by row " + rowIndex);
							logFileWriter.newLine();
						}
					}
				}
			}//end for loop - going through the criteria for this one row

			if (match){
				names.add(getCellData(sheetName, "Name", rowIndex));
				summary[0] = new BigDecimal(names.size());
				logFileWriter.write("Match found at row " + rowIndex);
				logFileWriter.newLine();
				//System.out.print("incrementing count of faculty found to " + summary[0].toPlainString());
				logFileWriter.write("Incrementing count of faculty found to " + summary[0].toPlainString());
				logFileWriter.newLine();
				for (int columnIndex = 0; columnIndex<columnNames.length; columnIndex++){
					long addString = ((Long)NumberFormat.getNumberInstance(java.util.Locale.US).parse(getCellData(sheetName, columnNames[columnIndex], rowIndex))).longValue();
					BigDecimal toBeAdded = new BigDecimal(addString);
					summary[columnIndex+1] = summary[columnIndex+1].add(toBeAdded);
					//System.out.print(", and adding " + toBeAdded.toPlainString() + " to sum of " +summary[columnIndex+1].toPlainString());
					logFileWriter.write(", and adding " + toBeAdded.toPlainString() + " to sum of " +summary[columnIndex+1].toPlainString());
				}
				logFileWriter.newLine();
			}

			//now that we know we've had a matched row, reset the search criterion
			name = (String)criteria.keySet().toArray()[0];
			value = (String)criteria.get(name);
			
			//increment the rowIndex and search for the next match on the first criterion
			rowIndex = getCellRowNum(sheetName, name, value, ++rowIndex);
			if (rowIndex == -1){
				//System.out.println("The worksheet " + sheetName + " has been searched.  There are no more matches.");
				logFileWriter.write("The worksheet " + sheetName + " has been searched.  There are no more matches.");
				logFileWriter.newLine();
			}//end if - the output file has been updated with the information

		}//end while loop - going through all of the rows in the worksheet
		return summary;
	}
	
	/*
	 * The objective of this method is to return a discrete ordered list of String values for a 
	 * given set of search criteria.  
	 * The String values to be returned are the values found in a column, given as an input parameter.
	 * The search criteria are given as a HashMap of name-value pairs.
	 * 
	 * Please note the limitations of this - it will only return discrete values from one column.
	 * It can have multiple search criteria.  Most of the time, this will be used to derive a list
	 * of discrete Faculty names, given search criteria as input.
	 * 
	 * It works by calling and passing the parameters to the getMultipleFacultyValues method and then 
	 * exporting the contents of the HashMap it returns into a TreeSet.
	 */
	public TreeSet<String> getDiscreteOrderedListOfValues(String sheetName, HashMap<String, String> keysAndValues, String value, BufferedWriter logFileWriter) throws Exception{
		TreeSet<String> discreteValues = new TreeSet<String>();
		HashMap<Integer, String> nonDiscreteValues = getMultipleFacultyValues(sheetName, keysAndValues, value, logFileWriter);
		logFileWriter.newLine();
		//iterate through the HashMap, getting the String values and putting them into the TreeSet
		for (Integer key: nonDiscreteValues.keySet()) {
			discreteValues.add(nonDiscreteValues.get(key));
			logFileWriter.write(nonDiscreteValues.get(key) + ", ");
		}//end for loop
		logFileWriter.newLine();
		logFileWriter.write("method getDiscreteOrderedListOfValues is returning a TreeSet of size " + discreteValues.size());
		logFileWriter.newLine();
		return discreteValues;
	}
	
	/*
	 * The objective of this method is to return a list of faculty values from a spreadsheet,
	 * given the input, which is a HashMap of column names and values which serve as criteria
	 * for selecting the faculty to be returned.  The values returned will be not be a distinct
	 * list of values.  The HashMap returned will be a list of key-value pairs.  The key will
	 * be the row number on which the faculty value was found and the value will be value of 
	 * the faculty member found.
	 * 
	 * This works using a simple algorithm.  It iterates through the contents of the worksheet
	 * given as input.  Once it locates a row containing the first searched-for
	 * value in the first searched-on column, it then iterates through a "for" loop, for that
	 * row, and it breaks if and when one of the searched-for values in that row differs from 
	 * the value actually found for one of the searched-on columns given as input.
	 * 
	 */
	public HashMap<Integer, String> getMultipleFacultyValues(String sheetName, HashMap<String, String> keysAndValues, String value, BufferedWriter logFileWriter) throws Exception{
		HashMap<Integer, String> faculty = new HashMap<Integer, String>();
		logFileWriter.newLine();
		logFileWriter.write("method getMultipleFacultyValues has been called");
		logFileWriter.newLine();
		
		/*
		 * first, retrieve the first key and first value, 
		 * which are the first searched-on column and first searched-for value
		 */
		
		String firstKey = (String)keysAndValues.keySet().toArray()[0];
		logFileWriter.write("First key examined: " + firstKey);
		String firstValue = (String)keysAndValues.get(firstKey);
		logFileWriter.write(", First value examined: " + firstValue);
		logFileWriter.newLine();
		

		int foundRowIndex = getCellRowNum(sheetName, firstKey, firstValue, rowForColumnNames);
		/*
		 * outer loop iterates through the sheet, 
		 * finding any and all rows which match the first searched-on column 
		 * and first searched-for value.
		 */
		while (foundRowIndex != -1){
			String foundValue = getCellData(sheetName, value, foundRowIndex);
			/*
			 * Once a match is found, iterate through the rest of the searched-on column names 
			 * and searched-for column values.
			 */
			boolean match = true;
			for (Object key: keysAndValues.keySet()) {
			    logFileWriter.write("column name : " + key.toString());
			    logFileWriter.write(", column value : " + keysAndValues.get(key).toString());
			    logFileWriter.newLine();
			    if (getCellData(sheetName, key.toString(), foundRowIndex).equalsIgnoreCase(keysAndValues.get(key).toString())){
			    	match = true;
			    	logFileWriter.write(": Match");
			    	logFileWriter.newLine();
			    } //end if
			    else{
			    	logFileWriter.write(": MISMATCH");
			    	logFileWriter.newLine();
			    	match = false;
			    	break;
			    }//end else
			}//end for loop
			
			// if this row matches all searched-for values for all searched-on columns, add it in
			if (match) {
				faculty.put(foundRowIndex, foundValue);
				logFileWriter.write("Found value " + foundValue +" at row " + foundRowIndex);
				logFileWriter.newLine();
			}//end if
			
			//now, advance the initial row index to search from so that it equals the value of the index it found
			foundRowIndex = getCellRowNum(sheetName, firstKey, firstValue, foundRowIndex + 1);
		}// end outer "while" loop
		return faculty;
	}
	
	/*
	 * The goal of this method is to return all values in a spreadsheet, given a faculty name
	 * that the values pertain to and the column names corresponding to the values to be returned.
	 * 
	 * The mechanics of this method are similar to the ""getMultipleFacultyValues" method.  
	 * The key difference is that the "getMultipleFacultyValues" method will return a list of one
	 * type of value (usually faculty names) given multiple column names and values as input to 
	 * match on.  This method will return multiple column values given the names of the columns
	 * to be returned and the name of the faculty.
	 * 
	 * It simply goes through the worksheet until it finds a row with the faculty name searched for
	 * and then iterates through that row, finding all values for the columns that are searched for.
	 */
	
	public HashMap <Integer, String[]> getMultipleValuesForFacultyName(String sheetName, String name, String[] columns, BufferedWriter logFileWriter) throws Exception{
		HashMap <Integer, String[]> columnValues = new HashMap <Integer, String[]>();

		// first, retrieve the first row which contains that name
		int foundRowIndex = getCellRowNum(sheetName, "Name", name, rowForColumnNames);

		while (foundRowIndex != -1){//iterate through the worksheet, finding every row that the name is on
			logFileWriter.write("Found row with name " + name + " at row number " + new Integer(foundRowIndex).toString()+" for worksheet " + sheetName);
			logFileWriter.newLine();
			String[] values = new String[columns.length];
			
			//Now, iterate through the array of column names, finding all corresponding values for that row
			for(int columnIndex = 0; columnIndex < columns.length; columnIndex ++){
				logFileWriter.write(", column name : " + columns[columnIndex]);
				String value = getCellData(sheetName, columns[columnIndex], foundRowIndex);
				logFileWriter.write(", column value: " + value);
				logFileWriter.newLine();
				values[columnIndex] = value;
			}
			
			columnValues.put(new Integer(foundRowIndex), values);

			foundRowIndex = getCellRowNum(sheetName, "Name", name, foundRowIndex + 1);
		}// end outer while loop

		return columnValues;
	}

	
	/*
	 * This takes precisely the same input as the "getMultipleFacultyValues" method above, 
	 * but there's a difference - it returns a sorted LinkedList instead of a HashMap so that faculty
	 * values can be extracted more easily in a situation in which row numbers aren't relevant
	 * information
	 */
	public LinkedList<String> getListOfFacultyValues (String sheetName, HashMap<String, String> keysAndValues, String value, BufferedWriter logFileWriter) throws Exception{
		LinkedList<String> listValues = new LinkedList<String>();
		logFileWriter.write("Calling method getListOfFacultyValues");
		logFileWriter.newLine();
		HashMap<Integer, String> hashValues = getMultipleFacultyValues(sheetName, keysAndValues, value, logFileWriter);
		  for (Object key: hashValues.keySet()) {
			  listValues.add(hashValues.get(key).toString());
		  }// end for loop
		  logFileWriter.newLine();
		  java.util.Collections.sort(listValues);
		  logFileWriter.write("List to be returned is as follows: ");
		  Iterator<String> listValueIterator = listValues.iterator();
		  while (listValueIterator.hasNext()) logFileWriter.write(listValueIterator.next() + "; ");
		  logFileWriter.newLine();
		  return listValues;
		  
	}

	public String getValueForColumnNameAndValue(String sheetName, String searchString, String searchColumn, 
			String columnName, BufferedWriter logWriter) throws Exception{
		return getValueForColumnNameAndValue(sheetName, searchString, searchColumn, columnName, logWriter, true);
	}

	/*
	 * 
	 * Given a worksheet name, a column name, and a value for that column, find the value for 
	 * another (given) column on the same row that the given value is found.
	 * 
	 */
	public String getValueForColumnNameAndValue(String sheetName, String searchString, String searchColumn, 
			String columnName, BufferedWriter logWriter, boolean writeToOutputFile) throws Exception{
		if (writeToOutputFile){
			logWriter.newLine();
			logWriter.write("HSSFExcelReader.getValueForColumnNameAndValue method called");
			logWriter.newLine();
		}
		String value = "";
		int foundRowIndex = getCellRowNum(sheetName, searchColumn, searchString, rowForColumnNames);
		if (writeToOutputFile){
			logWriter.write("value found at row:" + foundRowIndex + "; ");
		}
		if (foundRowIndex == -1) return value;
		else value = getCellData(sheetName, columnName, foundRowIndex);
		if (writeToOutputFile){
			logWriter.write("found value: " + value);
			logWriter.newLine();
		}
		return value;
	}
	// returns number of columns in a sheet	
	public int getColumnCount(String sheetName){
		// check if sheet exists
		if(!isSheetExist(sheetName))
		 return -1;
		
		sheet = workbook.getSheet(sheetName);
		row = sheet.getRow(rowForColumnNames);
		
		if(row==null)
			return -1;
		
		return row.getLastCellNum();
		
		
		
	}
	
	public String[] getColumnNames (String sheetName){
		String[] columnNames = new String[getColumnCount(sheetName)];
		
		for (int i = 0; i < columnNames.length; i++){
			columnNames[i] = getCellData(sheetName, i, rowForColumnNames+1) + "; ";
		}
		
		return columnNames;
	}
	
	/*
	//String sheetName, String testCaseName,String keyword ,String URL,String message
	public boolean addHyperLink(String sheetName,String screenShotColName,String testCaseName,int index,String url,String message){
		
		
		url=url.replace('\\', '/');
		if(!isSheetExist(sheetName))
			 return false;
		
	    sheet = workbook.getSheet(sheetName);
	    
	    for(int i=2;i<=getRowCount(sheetName);i++){
	    	if(getCellData(sheetName, 0, i).equalsIgnoreCase(testCaseName)){
	    		
	    		setCellData(sheetName, screenShotColName, i+index, message,url);
	    		break;
	    	}
	    }


		return true; 
	}
	*/
	
	public int getCellRowNumWithSubstringMatch(String sheetName,String colName,String cellValue, int startIndex){
		for(int i=startIndex;i<=getRowCount(sheetName);i++){
	    	if(getCellData(sheetName,colName , i).contains(cellValue)){
	    		return i;
	    	}
		}
		
		return -1;
	}
	
	public int getCellRowNum(String sheetName,String colName,String cellValue){
		//System.out.println("method getCellRowNum has been called");
		for(int i=rowForColumnNames;i<=getRowCount(sheetName);i++){
			//System.out.println("value of cell for column " + colName +" at row " + i + " is " + getCellData(sheetName,colName, i));
	    	if(getCellData(sheetName,colName, i).equalsIgnoreCase(cellValue)){
	    		//System.out.println("method getCellRowNum has found a match");
	    		return i;
	    	}
	    }
		return -1;
		
	}

	public int getCellRowNum(String sheetName, String colName, String cellValue, int startIndex){
		//System.out.println("method getCellRowNum has been called for sheet " +sheetName +", for column '" + colName + "', for value '" + cellValue +"', for starting row index " + startIndex);
		for(int i=startIndex;i<=getRowCount(sheetName);i++){
			//System.out.println("value of cell for column " + colName +" at row " + i + " is " + getCellData(sheetName,colName, i));
	    	if(getCellData(sheetName,colName , i).equalsIgnoreCase(cellValue)){
	    		return i;
	    	}
	    }
		return -1;
		
	}

	
	public int getCellRowNum(String sheetName,int colNum,String cellValue){
		
		for(int i=rowForColumnNames;i<=getRowCount(sheetName);i++){
	    	if(getCellData(sheetName,colNum , i).equalsIgnoreCase(cellValue)){
	    		//System.out.println(getCellData(sheetName,colNum , i));
	    		return i;
	    	}
	    }
		return -1;
		
	}
	
	public int getCellRowNum(String sheetName, int colNum, String cellValue, int startIndex){
		for(int i=startIndex;i<=getRowCount(sheetName);i++){
	    	if(getCellData(sheetName,colNum , i).equalsIgnoreCase(cellValue)){
	    		//System.out.println("value found for worksheet " + sheetName +" at row " +i +"is " + getCellData(sheetName,colNum , i));
	    		return i;
	    	}
	    }
		
		return -1;
	}
	

	// to run this on stand alone
	public static void main(String arg[]) throws IOException{
		
		
		HSSFExcelReader datatable = null;
		

			 datatable = new HSSFExcelReader("C:\\CM3.0\\app\\test\\Framework\\AutomationBvt\\src\\config\\xlfiles\\Controller.xlsx");
				for(int col=0 ;col< datatable.getColumnCount("TC5"); col++){
					System.out.println(datatable.getCellData("TC5", col, 1));
				}
	}
	
	

}
