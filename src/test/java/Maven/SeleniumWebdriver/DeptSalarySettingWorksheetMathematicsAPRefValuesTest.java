package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DeptSalarySettingWorksheetMathematicsAPRefValuesTest extends
		DeptSalarySettingWorksheetAPRefValuesTest {

	@Before
	public void setUp() throws Exception {
		departmentName = "Mathematics";
		myExcelReader = TestFileController.DepartmentSalarySettingMathematicsReport;
		worksheetName = TestFileController.DepartmentSalarySettingWorksheetName;
		myExcelReader.setRowForColumnNames(TestFileController.DepartmentSalarySettingWorksheetHeaderRow);
		testEnv = "Test";
		testName = "Test Department Salary Setting Worksheet " + departmentName;
		testDescription = "Verify AP Values in Department Salary Setting Worksheet " + departmentName;
		testCaseNumber = "012";
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void test() throws Exception{
		super.test();
	}

}
