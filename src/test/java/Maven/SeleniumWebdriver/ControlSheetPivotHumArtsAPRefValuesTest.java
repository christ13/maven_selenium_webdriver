package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import java.io.BufferedWriter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ControlSheetPivotHumArtsAPRefValuesTest extends
		ControlSheetReportSuiteAPRefValuesTest {

	public static final String clusterName = "HumArts";

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void test() throws Exception{
		System.out.println("Now executing the test method for class ControlSheetPivotHumArtsAPRefValuesTest.");
		verificationErrors = new StringBuffer();
		String testName = "Verify AP Values in Control Sheet Pivot HumArts Report";
		System.out.println("Now executing test: " + testName);
		String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		BufferedWriter logFileWriter = TestFileController.getLogFileWriter(logFileName);
		verifyAPValuesInControlSheetPivotHumArtsReport(logFileWriter);
		TestFileController.writeTestResults("Test", "006", "Comparison of FTE Adjusted Raises", "Control Sheet Pivot Humanities and Arts Report Tests", verificationErrors);
		logFileWriter.close();
	}
	/*
	 * This applies specifically to the Control Sheet Pivot Humanities and Arts Report worksheet 
	 * in the Control Sheet Report Suite. 
	 * 
	 * This will simply call getActualAPFacultyValues to get the values from the 
	 * Control Sheet Pivot Humanities and Arts Report that are to be compared with the expected values, 
	 * and then call the superclass method "verifyAPValuesInControlSheetReportSuite",
	 * passing the actual values to it as a parameter.  
	 * 
	 * The superclass method will gather the expected values and do the comparison 
	 * between the expected and actual values.
	 * 
	 */
	
	public void verifyAPValuesInControlSheetPivotHumArtsReport(BufferedWriter logFileWriter) throws Exception{
		TestFileController.ControlSheetReportSuite.setRowForColumnNames(TestFileController.ControlSheetPivotHeaderRow);
		String[][] expectedValues = getExpectedAPFacultyValues(clusterName, logFileWriter);
		String[][] actualValues = getActualAPFacultyValues(clusterName, TestFileController.ControlSheetPivot_HumArtsWorksheetName, logFileWriter);
		super.verifyAPValuesInControlSheetReportSuite(expectedValues, actualValues, logFileWriter);
	}//end verifyAPValuesInControlSheetPivotHumArtsReport method
	

}
