package Maven.SeleniumWebdriver;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	DepartmentSalarySettingWorksheetDBVerification.class,
	SalarySettingModificationByUploadVerification.class
})

public class SalarySettingTabTestSuite {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
	}

}
