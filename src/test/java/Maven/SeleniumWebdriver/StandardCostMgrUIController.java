package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.openqa.selenium.By;
/*
 * The Standard Costs are applied to Recruitments, SA's and Retentions.  
 * There is an order to the application of Standard Costs.  If an Recruitment, SA,
 * or Retention matches multiple Standard Costs, the Standard Cost with the greater
 * level of specificity is applied.  Sometimes, one attribute is specified and another
 * is not specified, or vice versa, in different sets of Standard Costs.  In such a 
 * case, some attributes take priority.  If a higher priority attribute is specified
 * and a lower priority attribute is not specified in one set of Standard Costs, 
 * and another set of Standard Costs has the same higher priority attribute unspecified
 * and a lower priority attribute specified, then the set of Standard Costs with the
 * higher priority attribute specified will be applied to the new SA, Recruitment, 
 * or Retention.
 * 
 * In terms of priority, here is the order in which different sets of Standard Costs
 * are applied to the same SA, Recruitment, or Retention which fits all these 
 * criteria:
 * (1) Every Attribute Specified
 * (2) Lab Unspecified
 * (3) Level Unspecified
 * (4) Level and Lab Unspecified
 * (5) Department Unspecified
 * (6) Department and Lab Unspecified
 * (7) Department and Level Unspecified
 * (8) Department, Level, and Lab unspecified
 * 
 * Additional limitations are: (1) No Costs will be added if the Amount is zero,
 * and (2) No Cost will be added if it is Summer Ninths and the Amount exceeds 9.
 * 
 */

public class StandardCostMgrUIController extends UIController {

	private static HSSFExcelReader myExcelReader;
	private static final String myWorksheetName = "Standard Costs Page";
	public static final String myURL = UIController.manageStdCostsURL;
	public static final String clusterName = "ClusterColumn";
	public static final String deptName = "DeptColumn";
	public static final String levelName = "LevelColumn";
	public static final String labYNName = "LabColumn";
	
	public static HashMap<String, String> HousingPrograms;


	public StandardCostMgrUIController() {
		myExcelReader = TestFileController.fileIn;
		HousingPrograms = new HashMap<String, String>();
		HousingPrograms.put("FL", "Forgivable loan");
		HousingPrograms.put("DPCA", "Cash Assistance Program");
		HousingPrograms.put("DIP", "DIP");
		HousingPrograms.put("HAS", "Housing Assistance Sup (HAS)");
	}//end constructor
	
	public String getSelectedText(String name, BufferedWriter logFileWriter) throws Exception{
		return getSelectedText(myExcelReader, myWorksheetName, name, logFileWriter);
	}//end getSelectedText method
	
	public String getTextFromElement(By by, BufferedWriter logFileWriter) throws Exception{
		String text = new String();
		try{
			text = super.getTextFromElement(by);
		}
		catch(Exception e){
			logFileWriter.write("The following Exception was thrown when attempting to get the text of "+by.toString());
			logFileWriter.write(e.getMessage());
			logFileWriter.newLine();
		}
		return text;
	}//end getTextFromElement method
	
	public String getTextFromLocatorName(int row, String name, BufferedWriter logFileWriter) throws Exception{
		String locator = getLocatorByName(row, name, logFileWriter);
		By by = getByForLocator(locator, logFileWriter);
		return getTextFromElement(by, logFileWriter);
	}//end getTextFromLocatorName method
	
	public String getStringValueFromLocatorName(int row, String name, BufferedWriter logFileWriter) throws Exception{
		String locator = getLocatorByName(row, name, logFileWriter);
		By by = getByForLocator(locator, logFileWriter);
		return getStringValueFromElement(by, logFileWriter);
	}//end getTextFromLocatorName method
	
	public String getStringValueFromElement(By by, BufferedWriter logFileWriter) throws Exception{
		String text = new String();
		try{
			text = super.getStringValueFromElement(by);
			if (text == null)
				text = new String();	
		}
		catch(Exception e){
			logFileWriter.write("The following Exception was thrown when attempting to get the text of "+by.toString());
			logFileWriter.write(e.getMessage());
			logFileWriter.newLine();
		}
		return text;
	}//end getStringValueFromElement method
	
	public String[] getLocatorNamesBelowLocator(String belowLocatorName, BufferedWriter logFileWriter) throws Exception{
		return super.getLocatorNamesBelowLocator(myExcelReader, myWorksheetName, belowLocatorName, logFileWriter);
	}//end getLocatorNamesBelowLocator method implemented for this Class

	public By getByForName(String name, BufferedWriter logFileWriter) throws Exception{
		return getByForName(myExcelReader, myWorksheetName, name, logFileWriter);
	}//end getByForName method
	
	public By getByForName(int row, String name, BufferedWriter logFileWriter) throws Exception{
		String rawLocator = getLocatorByName (row, name, logFileWriter);
		return getByForLocator(rawLocator, logFileWriter);
	}//end getByForName method
	
	public int getLastRowForColumnName(String name, BufferedWriter logFileWriter) throws Exception{
		boolean lastRow = false;
		int row = 0;
		while(! lastRow){
			row++;
			String locator = getLocatorByName(row, name, logFileWriter);
			By by = getByForLocator(locator, logFileWriter);
			if (! UIController.isElementPresent(by)){
				logFileWriter.write("Found last row - it's row number "+row);
				logFileWriter.newLine();
				lastRow = true;
			}//end if - it's the last row
		}//end while
		return row;
	}//end getLastRowForColumnName
	
	public String getLocatorByName(int row, String name, BufferedWriter logFileWriter) throws Exception{
		String locator = super.getLocatorByName(myExcelReader, myWorksheetName, name, logFileWriter);
		if (row>1)
			locator = locator.replace("/tbody/tr/", "/tbody/tr["+row+"]/");
		/*
		logFileWriter.write("After row adjustment, final locator is "+locator);
		logFileWriter.newLine();
		*/
		return locator;
	}//end getLocatorByName method
	
	public String getLocatorByName(String name, BufferedWriter logFileWriter) throws Exception{
		return super.getLocatorByName(myExcelReader, myWorksheetName, name, logFileWriter);
	}//end getLocatorByName method
	
	public String getCostType(int row, BufferedWriter logFileWriter) throws Exception{
		String costType = getTextFromLocatorName(row, "CostTypeColumn", logFileWriter);
		if (costType.equalsIgnoreCase("Housing")){
			costType = costType.concat("-"+getTextFromLocatorName(row, "HousingProgramColumn", logFileWriter));
		}
		logFileWriter.write("method getCostType has a cost type of "+costType);
		logFileWriter.newLine();
		return costType;
	}//end getCostType method
	
	public Integer getCostAmount(int row, BufferedWriter logFileWriter) throws Exception{
		String costString = getTextFromLocatorName(row, "AmountColumn", logFileWriter);
		costString = costString.replace("$", "");//remove the dollar sign
		costString = costString.replace(".00", "");//get rid of the cents
		costString = costString.replaceAll(",", "");//get rid of the commas
		return new Integer(costString);
	}
/*
	public boolean costApplies(String ID, String cluster, int row, boolean writeToOutput, BufferedWriter logFileWriter) throws Exception{
		return costApplies(ID, cluster, new String(), new String(), new String(), row, writeToOutput, logFileWriter);
	}//end costApplies method
	
	public boolean costApplies(String ID, String cluster, int row, BufferedWriter logFileWriter) throws Exception{
		return costApplies(ID, cluster, new String(), new String(), new String(), row, false, logFileWriter);
	}//end costApplies method
	
	public boolean costApplies(String ID, String cluster, String dept,  
			int row, boolean writeToOutput, BufferedWriter logFileWriter) throws Exception{
		return costApplies(ID, cluster, dept, new String(), new String(), row, writeToOutput, logFileWriter);
	}//end costApplies method
	
	public boolean costApplies(String ID, String cluster, String dept,  
			int row, BufferedWriter logFileWriter) throws Exception{
		return costApplies(ID, cluster, dept, new String(), new String(), row, false, logFileWriter);
	}//end costApplies method
	
	public boolean costApplies(String ID, String cluster, String dept, String level,  
			int row, BufferedWriter logFileWriter) throws Exception{
		return costApplies(ID, cluster, dept, level, new String(), row, false, logFileWriter);
	}//end costApplies method

	public boolean costApplies(String ID, String cluster, String dept, String level,  
			int row, boolean writeToOutput, BufferedWriter logFileWriter) throws Exception{
		return costApplies(ID, cluster, dept, level, new String(), row, writeToOutput, logFileWriter);
	}//end costApplies method
	
	public boolean costApplies(String ID, String cluster, String dept, String level, String labYN, 
			int row, BufferedWriter logFileWriter) throws Exception{
		return costApplies(ID, cluster, dept, level, labYN, row, false, logFileWriter);
	}//end costApplies method
	
*/
	
	public boolean costApplies(String ID, String cluster, String dept, String level, String labYN, 
			int row, boolean writeToOutput, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.write("Now attempting to determine if the cost applies for ID "+ID
				+" - given Cluster '"+cluster+"', Department '"+dept
				+"', and level '"+level+"', and lab '"+labYN+"'; ");
		logFileWriter.newLine();
		
		if (! UIController.driver.getCurrentUrl().contains(myURL)){
			UIController.switchWindowByURL(myURL, logFileWriter);
		}//end if - switch to the Standard Costs Page if we're not there already
		
		String actualCluster = getTextFromLocatorName(row, clusterName, logFileWriter);
		String actualDept = getTextFromLocatorName(row, deptName, logFileWriter);
		String actualLevel = getTextFromLocatorName(row, levelName, logFileWriter);
		String actualLabYN = getTextFromLocatorName(row, labYNName, logFileWriter);
		
		boolean matchCluster = cluster.equalsIgnoreCase(actualCluster);
		boolean matchDept = dept.equalsIgnoreCase(actualDept);
		boolean matchLevel = (level.equalsIgnoreCase(actualLevel) 
				|| (level.equalsIgnoreCase("Open") &&(actualLevel.isEmpty())));
		boolean matchLabYN = labYN.equalsIgnoreCase(actualLabYN);
		boolean totalMatch = matchCluster && matchDept && matchLevel && matchLabYN;
		
		if (totalMatch){
			logFileWriter.write("TOTAL MATCH on row "+row);
			logFileWriter.newLine();
		}//end if
		else {
			if ((! matchCluster) && writeToOutput){
				logFileWriter.write("There was a mismatch - expected Cluster was "+cluster 
						+", but actual Cluster value is "+actualCluster);
				logFileWriter.newLine();
			}//end if - no match on the Cluster
			if ((! matchDept) && writeToOutput){
				logFileWriter.write("There was a mismatch - expected Department was "+dept 
						+", but actual Department value is "+actualDept);
				logFileWriter.newLine();
			}//end if - no match on the Department
			if ((! matchLevel) && writeToOutput){
				logFileWriter.write("There was a mismatch - expected Level was "+level 
						+", but actual Level value is "+actualLevel);
				logFileWriter.newLine();
			}//end if - no match on the Level
			if ((! matchLabYN) && writeToOutput){
				logFileWriter.write("There was a mismatch - expected Lab(Yes/No) was "+labYN 
						+", but actual Lab(Yes/No) value is "+actualLabYN);
				logFileWriter.newLine();
			}//end if - no match on the Lab(Y/N)
		}//end else - no match
		
		return totalMatch;
	}//end costApplies method

	public HashMap<String, Integer> getExpectedStandardCosts(String ID, String cluster, String dept, String level,
			String labYN, boolean writeToOutput, BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		logFileWriter.write("*** method getExpectedStandardCosts called for ID "+ID
				+" with Cluster '"+cluster+"', and Department '"+dept+"', and Level"
				+level+"', and Lab (Yes/No) '"+labYN+"' ***");
		logFileWriter.newLine();
		String[] firstSet = {cluster, dept, level, labYN, "Cluster: "+cluster+", Dept: "+dept+", Level: "+level+", Lab: "+labYN};
		String[] secondSet = {cluster, dept, level, new String(), "Cluster: "+cluster+", Dept: "+dept+", Level: "+level+", blank Lab"};
		String[] thirdSet = {cluster, dept, new String(), labYN, "Cluster: "+cluster+", Dept: "+dept+", blank Level, Lab: "+labYN};
		String[] fourthSet = {cluster, dept, new String(), new String(), "Cluster: "+cluster+", Dept: "+dept+", blank Level, blank Lab"};
		String[] fifthSet = {cluster, new String(), level, labYN, "Cluster: "+cluster+", blank Dept, Level: "+level+", Lab: "+labYN};
		String[] sixthSet = {cluster, new String(), level, new String(), "Cluster: "+cluster+", , blank Dept, Level: "+level+", blank Lab"};
		String[] seventhSet = {cluster, new String(), new String(), labYN, "Cluster: "+cluster+", blank Dept, blank Level, Lab: "+labYN};
		String[] eighthSet = {cluster, new String(), new String(), new String(), "Cluster: "+cluster+", blank Dept, blank Level, blank Lab"};
		
		String[][] criteria = {firstSet, secondSet, thirdSet, fourthSet, fifthSet, sixthSet, seventhSet, eighthSet};
		
		HashMap<String, Integer> expectedStdCosts = new HashMap<String, Integer>();
		UIController.switchWindowByURL(UIController.manageStdCostsURL, logFileWriter);
		int lastRow = HandSOnTestSuite.stdCostMgrUI.getLastRowForColumnName("CostIDColumn", logFileWriter);
		logFileWriter.write("Last row is determined to be "+lastRow);
		logFileWriter.newLine();
		
		for (int criterionIteration = 0; criterionIteration<criteria.length; criterionIteration++){
			String[] thisCriteria = criteria[criterionIteration];
			logFileWriter.write("Iteration "+criterionIteration+" through the standard costs for ID "+ID
					+" - this time criterion with values "+ thisCriteria[thisCriteria.length - 1]);
			logFileWriter.newLine();

			for (int row=1; row<lastRow; row++){
				if (costApplies(ID, thisCriteria[0], thisCriteria[1], thisCriteria[2], thisCriteria[3], row, writeToOutput, logFileWriter)){
					String costType = getCostType(row, logFileWriter);
					Integer costAmt = getCostAmount(row, logFileWriter);
					Integer prevCost = expectedStdCosts.putIfAbsent(costType.replaceAll("\\s+", ""), costAmt);
					if ((prevCost == null) && writeToOutput){
						logFileWriter.write("MATCH on row "+row);
						logFileWriter.write(", adding Cost Type "+costType+", with Amount "+costAmt);
						logFileWriter.newLine();
					}//end if - output indicator
				}//end if - costApplies
				else if (writeToOutput){
					logFileWriter.write("No match on row "+row);
					logFileWriter.newLine();
				}//end else if - indicate no match
			}//end outer for - iterating through the rows
		}//end outer-outer for - iterating through the criteria
		Iterator iterator = expectedStdCosts.entrySet().iterator();
		ArrayList<String> toBeDeleted = new ArrayList<String>();
		while(iterator.hasNext()){
			Map.Entry pair = (Map.Entry)iterator.next();
			String key = (String)pair.getKey();
			if (expectedStdCosts.get(key).intValue() == 0){
				logFileWriter.write("Removing Cost type "+key+", because the Cost Amount is zero");
				logFileWriter.newLine();
				toBeDeleted.add(key);
			}//end if - we remove it if the Cost Amount is zero
		}//end while loop - specifying costs with zero amounts
		
		//unfortunately, if we try to remove the costs in the above loop, we'll get a ConcurrentModificationException
		for (int i=0; i<toBeDeleted.size(); i++)
			expectedStdCosts.remove(toBeDeleted.get(i));
		
		outputCosts(expectedStdCosts, "Expected Costs", logFileWriter);
		
		return expectedStdCosts;
	}//end getExpectedStandardCosts method
	


}//end Class
