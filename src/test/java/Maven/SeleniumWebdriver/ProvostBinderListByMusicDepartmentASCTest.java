package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import java.util.TreeMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ProvostBinderListByMusicDepartmentASCTest extends
		ProvostBinderListByDepartmentASCTest {

	@Before
	public void setUp() throws Exception {
		testName = "ProvostBinderListByMusicDepartmentASCTest";
		testEnv = "Test";
		testCaseNumber = "032";
		testDescription = "ASC Test of Provost Binder List By Music Department";
		logFileName = "Provost Binder Music Department ASC Test.txt";
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void test() throws Exception {
		super.test();
	}

}
