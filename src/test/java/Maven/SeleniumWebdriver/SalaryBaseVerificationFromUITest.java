package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.text.NumberFormat;
import org.openqa.selenium.NoSuchElementException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

public class SalaryBaseVerificationFromUITest {
	protected String locatorSheetName;
	protected String testName;
	protected StringBuffer verificationErrors;
	protected BufferedWriter logFileWriter;
	protected String logFileName;
	protected String testEnv;
	protected String testCaseNumber;
	protected String testDescription;
	protected String logDirectoryName = ReportRunner.logDirectoryName;
	private HSSFExcelReader inputFile;
	private HSSFExcelReader locators;
	private static final String SalaryBaseDollarsColumnBase = "SalaryBaseDollarsColumnBase";
	private static final String AllocationPercentColumnBase = "AllocationPercentColumnBase";
	private static final String PercentSalaryBaseColumnBase = "PercentSalaryBaseColumnBase";
	private static final String AllocationDollarsColumnBase = "AllocationDollarsColumnBase";
	
	private static final String AdjustmentSourcePoolColumnBase = "AdjustmentSourcePoolColumnBase";
	private static final String AdjustmentDestinationPoolColumnBase = "AdjustmentDestinationPoolColumnBase";
	private static final String AdjustmentAmountColumnBase = "AdjustmentAmountColumnBase";
	
	protected int rowForColumnNames;
	private boolean locatorsToOutputFile = true;
	private boolean ExcelReaderToOutputFile = true;
	
	private double[] salaryBases = new double[ASCTest.sourcePools.length];
	private double[] salaryBasePercentages = new double [ASCTest.sourcePools.length];
	private double[] salaryBaseRaiseAmounts = new double[ASCTest.sourcePools.length];

	@Before
	public void setUp() throws Exception {
		testName = "SalaryBaseVerificationFromUITest";
		testEnv = "Test";
		testCaseNumber = "046";
		testDescription = "Salary Base Verification From UI";
		logFileName = testDescription + ".txt";
		logFileName = ReportRunner.logDirectoryName +"/" + logFileName;
		logFileWriter = TestFileController.getLogFileWriter(logFileName);

		locators = TestFileController.locators;
		locators.setRowForColumnNames(0);
		
		inputFile = TestFileController.fileIn;
		inputFile.setRowForColumnNames(0);
		verificationErrors = new StringBuffer();
		locatorSheetName = "Control Sheet";
}

	@After
	public void tearDown() throws Exception {
		TestFileController.writeTestResults(testEnv, testCaseNumber, testName, testDescription, verificationErrors);
		logFileWriter.close();
	}

	@Test
	public void test() throws Exception{
		String message = "Now executing the '" + testDescription +"' test";
		writeToLogFile(message, logFileWriter);
		System.out.println(message);
		if (! ControlSheetUI.openSalarySetting(logFileWriter)){
			fail("Could not open Salary Setting");
		}//end if Salary Setting is not open
		
		//here is where we do the bona fide testing
		verifyAggregateBasesAndAmounts(logFileWriter);
		verifyDesigneePercentagesAndAmounts(logFileWriter);
		verifyDeductions(logFileWriter);
		verifyAdjustments(logFileWriter);
		verifyPoolAmountAfterDeductions(logFileWriter);
		verifyReserveAmounts(logFileWriter);
		verifyVarianceAmounts(logFileWriter);
	}//end test method
	
	/*
	 * There are several steps to this.  Iterate through the list of Pools to:
	 * (1) Determine the "Pool Amount after Deductions" amount in the "Pool Summary" section 
	 * of the UI for the Pool.
	 * (2) Determine the "Department Allocation" amount in the "Pool Summary" section of the UI
	 * for the Pool.
	 * (3) Determine the "Reserves" amount in the "Pool Summary" section of the UI for the Pool.
	 * (4) Determine the "Amount Allocated" in the "Pool Management" section of the UI for the Pool.
	 * (5) Determine the "Variance" amount in the "Pool Summary" section of the UI for the Pool.
	 * (6) For each Pool, determine whether or not the "Pool Amount after Deductions" amount minus 
	 * the "Department Allocation" amount minus the "Reserves" amount equals the "Variance amount.
	 * (7) Fail the test if there is an inequality in one of the Pools.
	 */
	
	private void verifyVarianceAmounts (BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		writeToLogFile("*** method verifyVarianceAmounts called ***", logFileWriter);
		for(int i=0; i<ASCTest.sourcePools.length; i++){
			double poolAmtAfterDeductions = getPoolAmountForLocatorName(ASCTest.sourcePools[i], 
					"SummaryPoolAmountAfterDeductions", logFileWriter, locatorsToOutputFile);
			double deptAllocation = getPoolAmountForLocatorName(ASCTest.sourcePools[i], 
					"SummaryDepartmentAllocation", logFileWriter, locatorsToOutputFile);
			String locatorSuffix = "SummaryReserves";
			if (i == ASCTest.MarketPoolIndex)
				locatorSuffix = "ReserveAmount";
			double deptReserves = getPoolAmountForLocatorName(ASCTest.sourcePools[i], 
					locatorSuffix, logFileWriter, locatorsToOutputFile);
			double poolAmtAllocated = getPoolAmountForLocatorName(ASCTest.sourcePools[i], 
					"AmountAllocated", logFileWriter, locatorsToOutputFile);
			double expectedAmt = poolAmtAfterDeductions - deptAllocation 
					- deptReserves - poolAmtAllocated;
			double actualAmt = getPoolAmountForLocatorName(ASCTest.sourcePools[i], 
					"SummaryVariance", logFileWriter, locatorsToOutputFile);
			areAmountsEqual(ASCTest.sourcePools[i], expectedAmt, actualAmt, logFileWriter);
		}//end for loop
	}//end verifyVarianceAmounts method

	/*
	 * There are several steps to this.  Iterate through the list of Pools to:
	 * (1) Determine if the Pool has a Reserve associated with it.
	 * (2) If so, read the Summary Reserve Amount for that Pool as the expected amount.
	 * (3) If not, the Summary Reserve Amount for that Pool is zero.
	 * (4) Compare the Summary Reserve Amount for that Pool with the "Reserves" Amount
	 * in the "Pool Summary" Section (the actual amount).
	 * (5) If the expected and actual amounts differ for any Pool, the test fails.
	 * 
	 */

	private void verifyReserveAmounts(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		writeToLogFile("*** method verifyReserveAmounts called ***", logFileWriter);
		String[] reserveArray = {"MeritPoolNaturalSciencesDeanReserve-Test-2017", 
				"TestPoolTestFundingReserveName2017-TEST", 
				"AlbertEustacePeasmarchFundAlbertEustacePeasmarchFundReserve2017"};
		for (int i=0; i<ASCTest.sourcePools.length; i++){
			double expectedAmt = 0;//initialize to default value
			if (ExcelReaderToOutputFile)
				writeToLogFile("Now working with Pool "+ASCTest.sourcePools[i], logFileWriter); 
			String poolSubstring = ASCTest.sourcePools[i].replaceAll("\\s+", "");
			if (poolSubstring.contains("Regular"))
				poolSubstring = poolSubstring.replaceAll("Regular", "");
			for (int j=0; j<reserveArray.length; j++){
				if (reserveArray[j].contains(poolSubstring)){
					expectedAmt = getPoolAmountForLocatorName(reserveArray[j], "SummaryPoolAmount", 
							logFileWriter, locatorsToOutputFile);
					break;
				}//it's a match - set the expectedAmt
			}//end inner for - iterating through the Reserve Array
			double actualAmt = getPoolAmountForLocatorName(ASCTest.sourcePools[i], "SummaryReserves", 
					logFileWriter, locatorsToOutputFile);
			areAmountsEqual(ASCTest.sourcePools[i], expectedAmt, actualAmt, logFileWriter);
		}//end for loop
	}//end verifyReserveAmounts method
	
	/*
	 * There are several steps to this.  Iterate through the the list of Pools to:
	 * (1) Extract both the “Total Raise Pool Amount” and the “Total Deductions” amount 
	 * for each Pool in the UI.
	 * (2) Subtract the latter from the former to get the expected amount.
	 * (3) Extract the actual amount of “Pool Amount after Deductions” for each Pool from the UI.
	 * (4) Compare the expected amount with the actual amount and fail the test if there's a difference.
	 * 
	 */
	private void verifyPoolAmountAfterDeductions(BufferedWriter logFileWriter) throws Exception{
		logFileWriter.newLine();
		writeToLogFile("**** method verifyPoolAmountAfterDeductions called ****", logFileWriter);
		
		for (int i=0; i<ASCTest.sourcePools.length; i++){
			if (ExcelReaderToOutputFile)
				writeToLogFile("Now working with Pool "+ASCTest.sourcePools[i], logFileWriter); 
			double totalRaisePoolAmt = getPoolAmountForLocatorName(ASCTest.sourcePools[i], "SummaryTotalRaisePoolAmount", 
					logFileWriter, ExcelReaderToOutputFile);
			double totalDeductions = getPoolAmountForLocatorName(ASCTest.sourcePools[i], "SummaryTotalDeductions", 
					logFileWriter, ExcelReaderToOutputFile);
			double expectedAmt = totalRaisePoolAmt - totalDeductions;
			double actualAmt = getPoolAmountForLocatorName(ASCTest.sourcePools[i], "SummaryPoolAmountAfterDeductions", 
					logFileWriter, ExcelReaderToOutputFile);
			areAmountsEqual(ASCTest.sourcePools[i], expectedAmt, actualAmt, logFileWriter);
		}//end for loop
	}//end verifyPoolAmountAfterDeductions method
	
	private double getPoolAmountForLocatorName(String pool, String suffix, 
			BufferedWriter logFileWriter, boolean writeToLogFile) throws Exception{
		if (writeToLogFile)
			writeToLogFile("method getPoolAmountForLocatorName called with Pool "+pool 
					+", and suffix " + suffix +"; ", logFileWriter);
		String locatorName = pool.replaceAll("\\s+", "") + suffix;
		if (locatorName.contains("Regular"))
			locatorName = locatorName.replace("Regular", "");
		if (writeToLogFile)
			writeToLogFile("Locator Name derived is "+locatorName, logFileWriter);
		int rowNum = locators.getCellRowNum(locatorSheetName, "Name", locatorName);
		if (writeToLogFile)
			writeToLogFile("Row number derived is "+rowNum, logFileWriter);
		String locator = locators.getCellData(locatorSheetName, "Locator", rowNum);
		if (writeToLogFile)
			writeToLogFile("Locator found is "+locator, logFileWriter);
		String actualAmtString = ControlSheetUI.getStringValueForLocator(locator, logFileWriter, writeToLogFile);
		double actualAmt = ControlSheetUI.getDoubleFromString(actualAmtString);
		if (writeToLogFile)
			writeToLogFile("Amount being returned is "+actualAmt, logFileWriter);
		return actualAmt;
	}
	
	/*
	 * First, we will read all of the Transfer amounts at the bottom of the Salary Setting UI
	 * and add them up, per Pool, as indicated in the "Destination" column.
	 * 
	 * Then, we will read all of the Transfer amounts at the bottom of the Salary Setting UI
	 * and subtract them, per Pool, as indicated in the "Source" column.
	 * 
	 * Then, we compare each Transfer amount per Pool with the amounts on the UI, in the 
	 * Pool Summary section.  
	 * 
	 * Then, we will read all of the other Adjustment amounts at the bottom of the Salary Setting
	 * UI and add them up, per Pool.  If the Destination is a recognized Pool, it will 
	 * increment the amount.  The final amount per Pool will be compared with the amount listed
	 * in the UI "Pool Summary" section at the top of the page in the "Transfers" line.
	 * 
	 */
	private void verifyAdjustments (BufferedWriter logFileWriter) throws Exception {
		//this is effectively a constant
		String suffix = "ColumnBase";
			
		//these values are going to be key for the whole method:
		int[] totalTransfer = new int[ASCTest.sourcePools.length];
		int[] addlFundAmounts = new int[ASCTest.sourcePools.length];
			
		String adjustmentType = new String();
		boolean rowExists = true;
		String prefix = "Adjustment Type";
		String locatorName = prefix.replaceAll("\\s+", "") + suffix;

		String locator = getLocatorForLocatorName(locatorName, logFileWriter, locatorsToOutputFile);
		for (int UIrow = 1; rowExists; UIrow++ ){
			adjustmentType = getStringValueForLocatorAndRow(locator, UIrow, logFileWriter, locatorsToOutputFile);
			if ((adjustmentType == null) || (adjustmentType.isEmpty())){
				rowExists = false;
				if (locatorsToOutputFile)
					writeToLogFile("Row does not exist", logFileWriter);
				break;
			}//end if - we've reached the end - no more rows
			else{//we've still got rows to analyze
				if (locatorsToOutputFile){
					writeToLogFile(prefix + " derived from UI is "+adjustmentType, logFileWriter);
				}//end if - we do this output
				int poolIndexDest = getPoolIndex(AdjustmentDestinationPoolColumnBase, UIrow, logFileWriter, locatorsToOutputFile);
				int amount = getAmountForRow(UIrow, logFileWriter, locatorsToOutputFile);
				if (adjustmentType.equalsIgnoreCase("Transfer")){
					int poolIndexSource = getPoolIndex(AdjustmentSourcePoolColumnBase, UIrow, logFileWriter, locatorsToOutputFile);
					totalTransfer = handleTransfer(totalTransfer, poolIndexSource, poolIndexDest, amount, logFileWriter, locatorsToOutputFile);
				}//end if - it's a Transfer
				else if (adjustmentType.equalsIgnoreCase("Additional Funding")){
					addlFundAmounts = handleAddlFunding(addlFundAmounts, poolIndexDest, amount, logFileWriter, locatorsToOutputFile);
				}//end else if - the adjustment type is Additional Funding
				else{
					writeToLogFile("We are not considering adjustment types of "+adjustmentType, logFileWriter);
				}//end else - the adjustment type is not useful or recognized
			}//end else - the row has been found
		}//end for
		
		for (int i=0; i<totalTransfer.length; i++){
			writeToLogFile("Transfer amount for Pool "+ASCTest.sourcePools[i] 
					+ " is "+totalTransfer[i], logFileWriter);
			verifyPoolAmount(totalTransfer[i], "Transfers"+ASCTest.sourcePools[i], "KnownDollars", logFileWriter, locatorsToOutputFile);
			verifyPoolAmount(totalTransfer[i], "Transfers"+ASCTest.sourcePools[i], "TotalDollars", logFileWriter, locatorsToOutputFile);
		}//end for - listing the contents of transfer funds for pools
		
		for (int i=0; i<addlFundAmounts.length; i++){
			writeToLogFile("Additional Funding amount for Pool "+ASCTest.sourcePools[i] 
					+ " is "+addlFundAmounts[i], logFileWriter);
			verifyPoolAmount(addlFundAmounts[i], ASCTest.sourcePools[i], "SummaryAdjustments", logFileWriter, locatorsToOutputFile);
		}//end for - listing the contents of transfer funds for pools
		
	}//end verifyAdjustments method
	
	private void verifyPoolAmount(double expectedAmt, String pool, String suffix, 
			BufferedWriter logFileWriter, boolean writeToLogFile) throws Exception{
		writeToLogFile("*** method verifyPoolAmount called with pool "+pool
				+" and suffix "+suffix+" ***", logFileWriter);
		String locatorName = pool.replaceAll("\\s+", "") + suffix;
		if ((locatorName.contains("Regular")) &&(locatorName.contains("SummaryAdjustments")))
			locatorName = locatorName.replace("Regular", "");
		if (writeToLogFile)
			writeToLogFile("Locator Name derived is "+locatorName, logFileWriter);
		int rowNum = locators.getCellRowNum(locatorSheetName, "Name", locatorName);
		if (writeToLogFile)
			writeToLogFile("Row number derived is "+rowNum, logFileWriter);
		String locator = locators.getCellData(locatorSheetName, "Locator", rowNum);
		if (writeToLogFile)
			writeToLogFile("Locator found is "+locator, logFileWriter);
		String actualAmtString = ControlSheetUI.getStringValueForLocator(locator, logFileWriter, writeToLogFile);
		double actualAmt = ControlSheetUI.getDoubleFromString(actualAmtString);
		areAmountsEqual(pool, expectedAmt, actualAmt, logFileWriter);
	}//end verifyPoolAmount method
	
	private void areAmountsEqual(String pool, double expectedAmt, double actualAmt, BufferedWriter logFileWriter) throws Exception{
		if (! ControlSheetUI.isApproximatelyEqual(expectedAmt, actualAmt)){
			String message = "MISMATCH! Expected amount for Pool " + pool + " is "
						+expectedAmt+", but actual amount is "+actualAmt+"; ";
			verificationErrors.append(message);
			writeToLogFile(message, logFileWriter);
			logFileWriter.newLine();
		}//end if - the actual amount does not equal the expected amount
		else{
			writeToLogFile("Actual amount for Pool " + pool + " is "
						+actualAmt+", as expected", logFileWriter);
			logFileWriter.newLine();
		}//end else - the actual amount equals the expected amount
	}//end areAmountsEqual method
	
	private int getAmountForRow(int UIrow, BufferedWriter logFileWriter, boolean writeToLogFile) throws Exception{
		String locatorName = AdjustmentAmountColumnBase;
		String locator = getLocatorForLocatorName(locatorName, logFileWriter, writeToLogFile);
		String amountString = getStringValueForLocatorAndRow(locator, UIrow, logFileWriter, writeToLogFile);
		if (writeToLogFile){
			writeToLogFile("Amount derived from UI is "+amountString, logFileWriter);
		}//end if - we do this output
		return (int)ControlSheetUI.getDoubleFromString(amountString);
	}//end getAmountForRow method
	
	private int[] handleTransfer(int[] transfers, int poolIndexSource, int poolIndexDest, int amount, 
			BufferedWriter logFileWriter, boolean writeToLogFile) throws Exception{
		writeToLogFile("*** method handleTransfer called ***", logFileWriter);
		if (poolIndexSource != -1){
			transfers[poolIndexSource] +=amount;
			if (writeToLogFile){
				writeToLogFile("Pool "+ASCTest.sourcePools[poolIndexSource]
						+" has amount incremented to "+transfers[poolIndexSource], logFileWriter);
			}//end if - write this to the output file
		}//end if - the pool is recognized
		if (poolIndexDest != -1){
		transfers[poolIndexDest] -= amount;
			if (writeToLogFile){
				writeToLogFile("Pool "+ASCTest.sourcePools[poolIndexDest]
						+" has amount decremented to "+transfers[poolIndexDest], logFileWriter);
			}//end if - write this to the output file
		}//end if - the pool is recognized
		return transfers;
	}//end handleTransfer method
	
	private int[] handleAddlFunding(int[] addlFunds, int poolIndexDest, int amount, 
			BufferedWriter logFileWriter, boolean writeToLogFile) throws Exception{
		writeToLogFile("*** method handleAddlFunding called ***", logFileWriter);
		if (poolIndexDest != -1){
			addlFunds[poolIndexDest] += amount;
			if (writeToLogFile){
				writeToLogFile("Pool "+ASCTest.sourcePools[poolIndexDest]
						+" has amount incremented to "+addlFunds[poolIndexDest], logFileWriter);
			}//end if - write this to the output file
		}//end if - the pool is recognized
		return addlFunds;
	}//end handleAddlFunding method
	
	private int getPoolIndex(String locatorName, int UIrow, BufferedWriter logFileWriter, boolean writeToLogFile) throws Exception{
		String locator = getLocatorForLocatorName(locatorName, logFileWriter, writeToLogFile);
		if (locatorsToOutputFile)
			writeToLogFile("Locator found is "+locator, logFileWriter);
		String poolName = getStringValueForLocatorAndRow(locator, UIrow, logFileWriter, locatorsToOutputFile);
		if (locatorsToOutputFile)
			writeToLogFile("Pool name found is "+poolName, logFileWriter);
		int poolIndex = getPoolIndexForPoolName(poolName);
		if (locatorsToOutputFile)
			writeToLogFile("Pool Index determined to be "+poolIndex, logFileWriter);
		return poolIndex;
	}
	
	private int getPoolIndexForPoolName(String poolName){
		int poolIndex = -1;
		for(int i=0; i<ASCTest.sourcePools.length; i++){
			if (poolName.equalsIgnoreCase(ASCTest.sourcePools[i]))
				poolIndex = i;
		}//end for
		return poolIndex;
	}//end getPoolIndexForPoolName method
	
	private String getLocatorForLocatorName(String locatorName, BufferedWriter logFileWriter, boolean writeToLogFile) throws Exception{
		int rowNum = locators.getCellRowNum(locatorSheetName, "Name", locatorName);
		if (locatorsToOutputFile)
			writeToLogFile("Row number found in locator file for "+locatorName+" is "+rowNum, logFileWriter);
		String locator = locators.getCellData(locatorSheetName, "Locator", rowNum);
		if (locatorsToOutputFile)
			writeToLogFile("Locator derived is "+locator, logFileWriter);
		return locator;
	}

	/*
	 * The goals of this method are threefold: 
	 * (1) verify that the sum of each column in the Control Sheet grid is the number at the bottom
	 * (2) verify that the sum of each row per Stage in the Control Sheet grid is the number to the left.
	 * (3) verify that the sum for all stages (per Deduction type per Pool) in the Control Sheet is in 
	 * the corresponding cell in the "Total" section.
	 * 
	 *  In order to achieve this, we have to store all of the numbers in the Control Sheet grid in a
	 *  three-dimensional array of primitive doubles.  One dimension is the type of deduction (such as
	 *  Retention Promises plus one slot for the total - total size is 26), another dimension is the stage 
	 *  (Known, Pending, Estimated, plus one slot for the total), and the last dimension is the Pool
	 *  (all Pools, plus an additional slot for the total).  
	 *  
	 *  We will need to gather this information by initializing the names of the dimensions, then iterating
	 *  through the "Locators.xls" file to find all locator names with row values between 20 and 46 that
	 *  qualify.
	 */
	public void verifyDeductions(BufferedWriter logFileWriter) throws Exception{
		writeToLogFile("*** method verifyDeductions called ***", logFileWriter);
		String[] pools = new String[ASCTest.sourcePools.length + 1];
		String[] stages = new String[ASCTest.stages.length + 1];
		String[] deductions = new String[21];
		double[][][] deductionValues = new double[stages.length][pools.length][deductions.length];
		//this is used effectively as a constant
		String total = "Total";
		
		//initialize all of the names to be used - to label the dimensions of deductionValues
		for (int i=0; i<ASCTest.sourcePools.length; i++)
			pools[i] = ASCTest.sourcePools[i].replaceAll("\\s+", "");
		pools[ASCTest.sourcePools.length] = total;
		for (int i=0; i<ASCTest.stages.length; i++)
			stages[i] = ASCTest.stages[i];
		stages[ASCTest.stages.length] = total;

		//this is used effectively as a constant
		String regularMeritPool = pools[0];
		
		/*
		 * Iterate through all deductions in the "Locators.xls" file that apply to the Regular Merit Pool 
		 * and are Known.  Get the substring of each locator name that applies to the deduction name
		 * by using substring(0, locatorName.indexOf("Regular")), and store each substring in the deductions
		 * array.
		 */
		
		int UIrow = 20;
		String substring = regularMeritPool+"Known";
		int row = locators.getCellRowNumWithSubstringMatch(locatorSheetName, "Name", substring, locators.getRowForColumnNames());
		for (int i=0; i<deductions.length && UIrow<=46; i++){
			String locatorName = locators.getCellData(locatorSheetName, "Name", row);
			int substringIndex = locatorName.indexOf(substring);
			if ((row != -1) && (substringIndex != -1)){
				logFileWriter.write("locator name found is "+locatorName +", at row " + row +" - ");
				try{UIrow = Integer.parseInt(locators.getCellData(locatorSheetName, "Row", row));}
				catch(Exception e){logFileWriter.newLine(); writeToLogFile(e.getLocalizedMessage(), logFileWriter);}
				logFileWriter.write(" with UI Row " + UIrow);
				deductions[i] = locatorName.substring(0, substringIndex);
				writeToLogFile(", deduction name found is "+deductions[i], logFileWriter);
				logFileWriter.newLine();
			}//end if - the row is found and the substring is present
			row = locators.getCellRowNumWithSubstringMatch(locatorSheetName, "Name", substring, ++row);
		}//end for loop - deductions
		int[][] totalSums = new int[stages.length][deductions.length];
		for (int pool = 0; pool<pools.length; pool++){
			
			int[] poolValueSum = new int[deductions.length];//the total sum of all stages for a given Pool
			for(int stage = 0;stage<stages.length; stage++){
				int deductionValueSum = 0;//the sum of all deductions for a given stage-pool combo
				for (int deduction=0; deduction<deductions.length; deduction++){
					String locatorString = deductions[deduction]+pools[pool]+stages[stage]+"Dollars";
					writeToLogFile("Locator Name being used is "+locatorString, logFileWriter);
					int rowNum = locators.getCellRowNum(locatorSheetName, "Name", locatorString);
					writeToLogFile("Row number found is "+rowNum, logFileWriter);
					String locator = locators.getCellData(locatorSheetName, "Locator", rowNum);
					String deductionString = ControlSheetUI.getStringValueForLocator(locator, logFileWriter, locatorsToOutputFile);
					deductionValues[stage][pool][deduction] = ControlSheetUI.getDoubleFromString(deductionString);
					
					if (deduction<(deductions.length - 1)){
						deductionValueSum += deductionValues[stage][pool][deduction];
						writeToLogFile("Deduction value for "+deductions[deduction]+" is "+deductionString
								+" and is being added to existing sum "+deductionValueSum, logFileWriter);
					}//end if - this is added to the sum
					else{//it's the sum
						writeToLogFile("Sum of deductions is "+deductionValueSum, logFileWriter);
						verificationErrors = ControlSheetUI.compareValues(deductionValues[stage][pool][deduction], 
								deductionValueSum, verificationErrors, logFileWriter);
					}//end else - make sure that the sum is equal to the last entry, the total
					if (stage<(stages.length - 1)){
						poolValueSum[deduction] += deductionValues[stage][pool][deduction];
						writeToLogFile("Pool sum for Pool " +pools[pool] 
								+" for deduction " +deductions[deduction]
								+ " updated to "+poolValueSum[deduction], logFileWriter);
					}//end if - we're still adding to the sum of all stages for that pool
					else{
						writeToLogFile("Sum of stages for deduction " +deductions[deduction] 
								+" for Pool " +pools[pool] +" is "+poolValueSum[deduction], logFileWriter);
						verificationErrors = ControlSheetUI.compareValues(deductionValues[stage][pool][deduction], 
								poolValueSum[deduction], verificationErrors, logFileWriter);
					}//end else - we're comparing the sum of all stages for the pool we've got to the actual UI figure
					if (pool<(pools.length - 1)){
						totalSums[stage][deduction] += deductionValues[stage][pool][deduction];
						writeToLogFile("Total sum for Pool "+pools[pool] 
								+" for deduction " +deductions[deduction]
								+ " updated to "+totalSums[stage][deduction], logFileWriter);
					}//end if - we're adding up all of the deductions for all pools for that stage
					else{
						writeToLogFile("Sum of Pools for deduction " +deductions[deduction] 
								+" for stage " +stages[stage] +" is "+totalSums[stage][deduction], logFileWriter);
						verificationErrors = ControlSheetUI.compareValues(deductionValues[stage][pool][deduction], 
								totalSums[stage][deduction], verificationErrors, logFileWriter);
					}//end if - we're adding up all of the deductions for all stages for that pool
				}//end for loop - going through all of the deductions
			}//end outer for loop - going through all of the stages
		}//end outer-outer for loop - going through all of the Pools
	}//end verifyDeductions method
	
	private void verifyAggregateBasesAndAmounts(BufferedWriter logFileWriter) throws Exception{
		writeToLogFile("*** method verifyDesigneePercentagesAndAmounts called ***", logFileWriter);

		//determine what the actual base and amount values are
		initializeBasesAndAmounts(logFileWriter);

		//test the salary bases
		verifyAggregateBases(logFileWriter);

		//test the amounts
		verifyAggregateAmounts(logFileWriter);
		
		//test the Deduction amounts
		verifyDeductionAmounts(logFileWriter);
	}//end method verifyAggregateBasesAndAmounts
	
	private void initializeBasesAndAmounts(BufferedWriter logFileWriter) throws Exception{
		
		for (int i=1; i<ASCTest.sourcePools.length; i++){
			logFileWriter.newLine();
			//store these in the global variables
			salaryBases[i] = getDoubleValueForPool(ASCTest.sourcePools[i], 1, "SalaryBase", logFileWriter, locatorsToOutputFile);
			salaryBasePercentages[i] = getDoubleValueForPool(ASCTest.sourcePools[i], 1, "PercentageAmount", logFileWriter, locatorsToOutputFile);
			salaryBaseRaiseAmounts[i] = getDoubleValueForPool(ASCTest.sourcePools[i], 1, "RaiseAmount", logFileWriter, locatorsToOutputFile);
		}//end for loop
		//initialize the Regular Merit Pool Salary Base - it was verified by another Test Class
		String meritPool = "MeritPool";
		salaryBases[ASCTest.RegularMeritPoolIndex] 
				= getDoubleValueForPool(meritPool, 1, "SalaryBase", logFileWriter, locatorsToOutputFile);
		salaryBasePercentages[ASCTest.RegularMeritPoolIndex] 
				= getDoubleValueForPool(meritPool, 1, "PercentageAmount", logFileWriter, locatorsToOutputFile);
		salaryBaseRaiseAmounts[ASCTest.RegularMeritPoolIndex] 
				= getDoubleValueForPool(meritPool, 1, "RaiseAmount", logFileWriter, locatorsToOutputFile);
		
	}//end initializeBasesAndAmounts method
	
	private void verifyAggregateBases(BufferedWriter logFileWriter) throws Exception{
		double expectedSalaryBaseSum = 0; 
		String message = new String();

		for (int i=1; i<ASCTest.sourcePools.length; i++){
			expectedSalaryBaseSum 
	  		= ControlSheetUI.getSalaryBaseDollarSumForPool(ASCTest.sourcePools[i], logFileWriter).doubleValue();
		writeToLogFile("Expected " + ASCTest.sourcePools[i] +" Salary Base is " + expectedSalaryBaseSum, logFileWriter);
		if (! ControlSheetUI.isApproximatelyEqual(expectedSalaryBaseSum, salaryBases[i])){
			message = "MISMATCH - "+ASCTest.sourcePools[i] 
					+ ", expected salary base is " + expectedSalaryBaseSum 
					+ ", but actual salary base is " + salaryBases[i] + ";\n ";
			verificationErrors.append(message);
			writeToLogFile(message, logFileWriter);
		}//end if
		else writeToLogFile(ASCTest.sourcePools[i] +" Salary Base is as expected", logFileWriter);
		}//end for loop - iterating through all the Pools' salary bases

	}//end verifyAggregateBases method
	
	private void verifyAggregateAmounts(BufferedWriter logFileWriter) throws Exception{
		String message = new String();
		
		for (int i=0; i<ASCTest.sourcePools.length; i++){
			writeToLogFile("Actual " + ASCTest.sourcePools[i] +" Salary Base is " + salaryBases[i], logFileWriter);
			writeToLogFile("Actual " + ASCTest.sourcePools[i] +" Salary Base Percentage is " + salaryBasePercentages[i], logFileWriter);
			writeToLogFile("Actual " + ASCTest.sourcePools[i] +" Salary Base Amount is " + salaryBaseRaiseAmounts[i], logFileWriter);
			double expectedAmt = salaryBases[i] * salaryBasePercentages[i]/100;
			if (ControlSheetUI.isApproximatelyEqual(expectedAmt, salaryBaseRaiseAmounts[i])){
				writeToLogFile("... and actual raise amount is as expected", logFileWriter);
			}//end if - the raise amount is as expected
			else{
				message = "MISMATCH: Expected amount is " + expectedAmt 
						+", but actual amount is " + salaryBaseRaiseAmounts[i] + ";\n  ";
				verificationErrors.append(message);
				writeToLogFile(message, logFileWriter);
			}//end else - the raise amount is not as expected
		}//end for
	}//end verifyAggregateAmounts method
	
	/*
	 * There are several steps to this.
	 * Get the Net Pool Amount for that Pool.
	 * 
	 * Do the rest of the steps for each Designee - this can happen within a "for" loop.
	 * Get the data for that Designee. 
	 * 		Determine what the Language of Allocation is for that Designee.
	 * 
	 * Once we have the Language of Allocation, determine the course of action.
	 * 		If the “Language of Allocation” value is “Allocation Amount”, 
	 * 		then we need to calculate the “Allocation %” value.  
	 * 		Divide the “Allocation $” amount by the “Net Pool Amount” and multiply by 100.  
	 * 		Compare this to the actual value under the “Allocation %” column.
	 * 		If the amounts are not equal, append an error message, 
	 * 			including the expected and actual amounts, to verificationErrors
	 * 
	 * 		If the “Language of Allocation” value is “% of Available Pool”, 
	 * 		then we need to calculate the “Allocation $”.
	 * 		Multiply the “Net Pool Amount” by the “Allocation %” and then divide by one hundred.  
	 * 		Compare the resulting quotient to the actual amount under the “Allocation $” column
	 * 		If the amounts are not equal, append an error message, 
	 * 			including the expected and actual amounts, to verificationErrors
	 * 
	 * 		If the “Language of Allocation” value is “% of Base Salary”, 
	 * 		then we need to calculate the “Allocation $”. 
	 * 		Multiply the “Salary Base” amount by the “% of Salary Base” and then divide by one hundred, 
	 * 		Compare the quotient to the actual amount under the “Allocation $” column.
	 * 		If the amounts are not equal, append an error message, 
	 * 			including the expected and actual amounts, to verificationErrors
	 * 
	 */
	private void verifyDesigneePercentagesAndAmounts(BufferedWriter logFileWriter) throws Exception{
		writeToLogFile("*** method verifyDesigneePercentagesAndAmounts called ***", logFileWriter);
		
		String languageOfAllocation = new String();
		boolean writeToLogFile = locatorsToOutputFile;
		
		for (int index = 1; index < ASCTest.sourcePools.length; index++){
			boolean rowExists = true;
			for (int row = 1; rowExists; row++ ){
				languageOfAllocation = getStringValue(ASCTest.sourcePools[index], row, "LanguageOfAllocationColumnBase", 
						logFileWriter, writeToLogFile);
				
				logFileWriter.newLine();
				if (languageOfAllocation.isEmpty()){
					rowExists = false;
					writeToLogFile("Row number "+ row +" not found - exiting", logFileWriter);
					break;
				}
				writeToLogFile("Row number "+ row +" found", logFileWriter);
				writeToLogFile("Language of Allocation found for pool " + ASCTest.sourcePools[index] 
						+ " is " + languageOfAllocation, logFileWriter);
				if (languageOfAllocation.equalsIgnoreCase("Allocation Amount"))
					verifyPercentSalaryBase(ASCTest.sourcePools[index], row, AllocationPercentColumnBase, logFileWriter, writeToLogFile);
				else if (! (languageOfAllocation == null) && !(languageOfAllocation.isEmpty()))
					verifyAllocationDollars(ASCTest.sourcePools[index], row, AllocationDollarsColumnBase, languageOfAllocation, 
							logFileWriter, writeToLogFile);
				else {//either the languageOfAllocation is null or an empty String
					String message = "ERROR - cannot determine Language Of Allocation for Pool" 
							+ ASCTest.sourcePools[index] + " for row " + row; 
					writeToLogFile(message, logFileWriter);
					verificationErrors.append(message);
				}//end else
			}//end for - going through all of the rows
			writeToLogFile("Pool "+ ASCTest.sourcePools[index] +" has been evaluated", logFileWriter);
			logFileWriter.newLine();
			writeToLogFile("***************************", logFileWriter);
			logFileWriter.newLine();
		}//end for - going through all of the Pools
	}//end verifyDesigneePercentagesAndAmounts method
	
	
	
	private void verifyDeductionAmounts(BufferedWriter logFileWriter) throws Exception{
		writeToLogFile("*** method verifyDeductionAmounts called ***", logFileWriter);
		int minRow = locators.getRowForColumnNames();
		writeToLogFile("Min Row set at " + minRow, logFileWriter);
		int maxRow = locators.getCellRowNumWithSubstringMatch(locatorSheetName, "Name", "TotalDeductionsTotal", minRow);
		writeToLogFile("Max Row set at " + maxRow, logFileWriter);
		for (int pool = 0; pool<ASCTest.sourcePools.length; pool++){
			String poolName = ASCTest.sourcePools[pool].replaceAll("\\s+","");
			double poolDeductionSum = 0;
			for (int stage = 0; stage < ASCTest.stages.length; stage++){
				int row = minRow;
				String stageName = ASCTest.stages[stage];
				String locatorSubString = poolName + stageName +"Dollars";
				double poolStageDeductionSum = 0;
				while ((row < maxRow) && (row != -1)){
					row = locators.getCellRowNumWithSubstringMatch(locatorSheetName, "Name", locatorSubString, ++row);
					String locatorName = locators.getCellData(locatorSheetName, "Name", row);
					String column = locators.getCellData(locatorSheetName, "Column", row);
					String locator = locators.getCellData(locatorSheetName, "Locator", row);
					String valueString = ControlSheetUI.getStringValueForLocator(locator, logFileWriter, locatorsToOutputFile);
					logFileWriter.write("Locator substring '" + locatorSubString + "' ");
					logFileWriter.write("and found locator name " + locatorName);
					logFileWriter.write(" at row " + row +" and column '" + column +"'");
					logFileWriter.write(", found locator '" + locator +"'");
					logFileWriter.write(", and has value '" + valueString);
					double value = -1;
					String message = new String();
					if ((valueString != null) && (! valueString.isEmpty()))
						value = ((Number)NumberFormat.getNumberInstance(java.util.Locale.US).parse(valueString)).doubleValue();
					if ((row < maxRow) && (row != -1)){
						poolStageDeductionSum += value;
						writeToLogFile("', which makes the deduction sum equal to " + poolStageDeductionSum, logFileWriter);
					}
					else{
						logFileWriter.write("', and the ");
						if (value == poolStageDeductionSum)
							message = "deduction sum is as expected: " + valueString;
						else{
							message = "deduction sum for locator substring " +locatorSubString +" is NOT as expected; " 
									+"expected " + poolStageDeductionSum +", actual: " + valueString +"; ";
							verificationErrors.append(message);
						}//end else - the sum of the deductions is not as expected
						writeToLogFile(message, logFileWriter);
					}//end outer else - we're evaluating the sum of the deductions
				}//end while
				poolDeductionSum += poolStageDeductionSum;
			}//end inner for loop - iterating through the Stages
			
			//get the actual Deduction sum for that Pool - for all Stages
			String locatorSubString = "TotalDeductions" + poolName +"TotalDollars";
			int row = locators.getCellRowNumWithSubstringMatch(locatorSheetName, "Name", locatorSubString, minRow);
			String locator = locators.getCellData(locatorSheetName, "Locator", row);
			String valueString = ControlSheetUI.getStringValueForLocator(locator, logFileWriter, locatorsToOutputFile);
			double actualPoolDeductionSum = -1;
			if ((valueString != null) && (! valueString.isEmpty()))
				actualPoolDeductionSum = ((Number)NumberFormat.getNumberInstance(java.util.Locale.US).parse(valueString)).doubleValue();
			logFileWriter.newLine();
			if (poolDeductionSum == actualPoolDeductionSum)
				writeToLogFile("Deduction sum for pool "+ ASCTest.sourcePools[pool] 
						+" is as expected - expected "+ poolDeductionSum +"; ", logFileWriter);
			else{
				String message = "Deduction sum for pool "+ ASCTest.sourcePools[pool] 
						+" is NOT as expected - expected "+ poolDeductionSum
						+", actual: " + actualPoolDeductionSum +"; ";
				writeToLogFile(message, logFileWriter);
				verificationErrors.append(message);
			}//end else - getting the total deduction amount for that pool - all Stages
			
			//get the actual figure for that pool - in the "Pool Summary" section:
			if (pool == 0)
					poolName = "MeritPool";
			locatorSubString = poolName +"SummaryTotalDeductions";
			row = locators.getCellRowNumWithSubstringMatch(locatorSheetName, "Name", locatorSubString, ++row);
			locator = locators.getCellData(locatorSheetName, "Locator", row);
			valueString = ControlSheetUI.getStringValueForLocator(locator, logFileWriter, locatorsToOutputFile);
			actualPoolDeductionSum = -1;
			if ((valueString != null) && (! valueString.isEmpty())){
				actualPoolDeductionSum = ControlSheetUI.getDoubleFromString(valueString);
			}
			logFileWriter.newLine();
			if (poolDeductionSum == actualPoolDeductionSum)
				writeToLogFile("Total Deduction summary value for pool "+ ASCTest.sourcePools[pool] 
						+" is as expected - expected "+ poolDeductionSum +"; ", logFileWriter);
			else{
				String message = "Total Deduction summary value for pool "+ ASCTest.sourcePools[pool] 
						+" is NOT as expected - expected "+ poolDeductionSum
						+", actual: " + actualPoolDeductionSum +"; ";
				writeToLogFile(message, logFileWriter);
				verificationErrors.append(message);
			}//end else - getting the total deduction amount for that pool - all Stages
			logFileWriter.newLine();
		}//end outer for loop - iterating through the Pools
	}//end verifyDeductionAmounts method
	
	private void verifyAllocationDollars(String poolName, int rowNum, String locatorNameSuffix, 
			String languageOfAllocation, BufferedWriter logFileWriter, boolean writeToLogFile) throws Exception{
		double expectedAllocationDollars = 0;
		if (languageOfAllocation.equalsIgnoreCase("% of Available Pool")){
			//Multiply the “Net Pool Amount” by the “Allocation %” and then divide by one hundred. 
			double netPoolAmount = getDoubleValueForPool (poolName, 1, "NetAmount", logFileWriter, writeToLogFile);
			double allocationPercent = getDoubleValue(poolName, rowNum, AllocationPercentColumnBase, logFileWriter, writeToLogFile);
			expectedAllocationDollars = (netPoolAmount * allocationPercent)/100;
		}//end if - % of Available Pool
		else if (languageOfAllocation.equalsIgnoreCase("% of Base Salary")){
			//Multiply the “Salary Base” amount by the “% of Salary Base” and then divide by one hundred,
			double salaryBaseAmount = getDoubleValue(poolName, rowNum, SalaryBaseDollarsColumnBase, logFileWriter, writeToLogFile);
			double percentSalaryBase = getDoubleValue(poolName, rowNum, PercentSalaryBaseColumnBase, logFileWriter, writeToLogFile);
			expectedAllocationDollars = (salaryBaseAmount * percentSalaryBase)/100;
		}//end else if - % of Base Salary
		double actualAllocationDollars = getDoubleValue(poolName, rowNum, AllocationDollarsColumnBase, logFileWriter, writeToLogFile);
		String message = new String();
		if (writeToLogFile){
			writeToLogFile("Expected Allocation Dollar Amount is " + expectedAllocationDollars, logFileWriter);
			writeToLogFile("Actual Allocation Dollar Amount is " + actualAllocationDollars, logFileWriter);
		}//end if - this is written to the log file
		if (ControlSheetUI.isApproximatelyEqual(expectedAllocationDollars, actualAllocationDollars)){
			message = "Allocation Dollar Amount is as expected";
		}//end if
		else{
			message = "Actual Allocation Dollar Amount is not as expected";
			verificationErrors.append(message);
		}//end else
		writeToLogFile(message, logFileWriter);
	}//end verifyAllocationDollars method
	
	private void verifyPercentSalaryBase(String poolName, int rowNum, String locatorNameSuffix, 
			BufferedWriter logFileWriter, boolean writeToLogFile) throws Exception{
		
		double allocationDollarAmount = getDoubleValue(poolName, rowNum, AllocationDollarsColumnBase, logFileWriter, writeToLogFile);
		double netPoolAmount = getDoubleValueForPool (poolName, 1, "NetAmount", logFileWriter, writeToLogFile);
		double expectedPercentSalaryBase = (allocationDollarAmount/netPoolAmount) * 100;
		double actualPercentSalaryBase = getDoubleValue(poolName, rowNum, AllocationPercentColumnBase, logFileWriter, writeToLogFile);
		String message = new String();
		if (writeToLogFile){
			writeToLogFile("Expected Salary Base is " + expectedPercentSalaryBase, logFileWriter);
			writeToLogFile("Actual Salary Base is " + actualPercentSalaryBase, logFileWriter);
		}//end if - this is written to the log file
		if (ControlSheetUI.isApproximatelyEqual(expectedPercentSalaryBase, actualPercentSalaryBase)){
			message = "Percent Salary Base is as expected";
		}//end if
		else{
			message = "Actual Salary Base is not as expected";
			verificationErrors.append(message);
		}//end else
		writeToLogFile(message, logFileWriter);
	}
	
	private double getDoubleValue(String poolName, int rowNum, String locatorNameSuffix, 
			BufferedWriter logFileWriter, boolean writeToLogFile) throws Exception{
		if (writeToLogFile) 
			writeToLogFile("*** method getDoubleValue called ***", logFileWriter);
		double value = 0;
		String valueString = getStringValue(poolName, rowNum, locatorNameSuffix, logFileWriter, writeToLogFile);
		if ((valueString != null) && (! valueString.isEmpty()))
			value = ((Number)NumberFormat.getNumberInstance(java.util.Locale.US).parse(valueString)).doubleValue();
		
		return value;
	}//end getDoubleValue method
	
	public static String getStringValue(String poolName, int rowNum, String locatorNameSuffix, 
			BufferedWriter logFileWriter, boolean writeToLogFile) throws Exception{
		if (writeToLogFile) 
			writeToLogFile("*** method getStringValue called ***", logFileWriter);
		String value = new String();
		String locatorName = poolName.replaceAll("\\s+","") + locatorNameSuffix;
		String locator = ControlSheetUI.getLocator(locatorName);
		if (writeToLogFile){
			writeToLogFile("Pool Name submitted is " + poolName, logFileWriter);
			writeToLogFile("locator name suffix is " + locatorNameSuffix, logFileWriter);
			writeToLogFile("LocatorName derived is "+locatorName, logFileWriter);
			writeToLogFile("locator found is "+locator, logFileWriter);
		}//end if
		value = getStringValueForLocatorAndRow(locator, rowNum, logFileWriter, writeToLogFile);
		if (! (value == null) && (! value.isEmpty())){
			if (value.startsWith("$"))
				value = value.substring(1);//always remove the initial "$"
		}//end if

		if (writeToLogFile){
			writeToLogFile("Value derived is " + value, logFileWriter);
		}//end if
		return value;
	}//end getStringValue method
	
	public static String getStringValueForLocatorAndRow (String locator, int rowNum, 
			BufferedWriter logFileWriter, boolean writeToLogFile) throws Exception{
		String value = new String();
		if (rowNum > 1){
			int rowIndex = locator.indexOf("tr");
			String prefix = locator.substring(0, rowIndex + 2)+"[";
			String suffix = "]" + locator.substring(rowIndex + 2);
			locator = prefix + rowNum + suffix;
		}//end if
		try{
			if (locator.contains("select"))
				value
					= new Select(ControlSheetUI.driver.findElement(By.xpath(locator))).getFirstSelectedOption().getText();
			else if (locator.contains("span"))
				value = ControlSheetUI.getStringValueForLocator(locator, logFileWriter, writeToLogFile);
			else value = ControlSheetUI.getValueOfInputForLocator(locator, logFileWriter, writeToLogFile);
			if (value == null)
				value = ControlSheetUI.getStringValueForLocator(locator, logFileWriter, writeToLogFile);
		}//end try
		catch (NoSuchElementException e){
			if (writeToLogFile)
				writeToLogFile("Element not found", logFileWriter);
		}//end catch
		return value;
	}//end getStringValueForLocatorAndRow method
	
	
	private double getDoubleValueForPool(String poolName, int rowNum, String suffix,
			BufferedWriter logFileWriter, boolean writeToLogFile) throws Exception{
		if (writeToLogFile) 
			writeToLogFile("*** method getDoubleDollarValueForPool called ***", logFileWriter);
		String dollarAmtString = getStringValue(poolName, rowNum, suffix, logFileWriter, writeToLogFile);
		if (! (dollarAmtString == null) && (! dollarAmtString.isEmpty())){
			return ((Number)NumberFormat.getNumberInstance(java.util.Locale.US).parse(dollarAmtString)).doubleValue();
		}//end if - the dollar amount can be found on the screen and is meaningful
		else return 0;
	}//end getDoubleDollarValueForPool
	
	
	  public static void writeToLogFile(String toBeWritten, BufferedWriter logFileWriter) throws Exception{
		  logFileWriter.write(toBeWritten);
		  logFileWriter.newLine();
	  }//end writeToLogFile method


}
