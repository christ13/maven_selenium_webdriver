package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.TreeSet;

import oracle.net.*;

/*
 * This Class serves as a container for ExpectedASCData objects.  Most
 * of what it does is provide a subset (a smaller ExpectedASCDataSet)
 * of ExpectedASCData Objects.
 * 
 */
public class ExpectedASCDataSet {
	
	private ArrayList<ExpectedASCData> myList;
	public ArrayList<ExpectedASCData> PYs;
	
	private ASCFacultyValuesSet ascFacultyValuesSet;

	private StringBuffer verificationErrors;
	//private BufferedWriter logFileWriter;
	private String logFileName;
	private String testEnv;
	private String testCaseNumber;
	private String testDescription;
	private String logDirectoryName = ReportRunner.logDirectoryName;


	public ExpectedASCDataSet() throws Exception{
		myList = new ArrayList<ExpectedASCData>();
		PYs = new ArrayList<ExpectedASCData>();
		verificationErrors = new StringBuffer();
		ascFacultyValuesSet = new ASCFacultyValuesSet();
	}
	
	public int add(ExpectedASCData element){
		myList.add(element);
		return myList.indexOf(element);
	}//end add method
	
	public boolean remove(ExpectedASCData element){
		return myList.remove(element);
	}//end remove method
	
	/*
	 * This yields an TreeSet of Strings, each of which will have two entries
	 * - a faculty name, and a department that that faculty member has an appointment in.
	 * The two substrings will be separated by a dash ('-') and the individual components
	 * will be separated using the split("-") function into an array of Strings 
	 * in other methods.
	 */
	public TreeSet<String> getUniqueNamesAndDepts(){
		TreeSet<String> uniqueNamesAndDepts = new TreeSet<String>();
		String message = new String("*** method getUniqueNamesAndDepts has been called ***");
		System.out.println(message);
		for(int i=0;i<myList.size(); i++){
			String nameAndDept = myList.get(i).getStringAttribute(ExpectedASCData.FacultyIndex)
				+"-"+ (myList.get(i).getStringAttribute(ExpectedASCData.DepartmentIndex));
			uniqueNamesAndDepts.add(nameAndDept);
		}//end for loop
		Iterator<String> iterator = uniqueNamesAndDepts.iterator();
		while (iterator.hasNext()){
			String[] split = ((String)iterator.next()).split("-");
			if (split.length == 0){
				split = new String[2];
				split[0] = split[1] = new String();
			}
			message = "Element derived from TreeSet: Name: " + split[0]
							+", Department: " + split[1] + "; ";
			//System.out.println(message);
		}//end for
		return uniqueNamesAndDepts;
	}//end getUniqueNamesAndDepts method
	
	public void populateListFromGUI(BufferedWriter logFileWriter) throws Exception{
		TestFileController.writeToLogFile("*** method populateListFromGUI is running ***", logFileWriter);
		ControlSheetUI.gotoControlSheetUITab();
		int numberOfASCs = ControlSheetUI.getNumberOfASCsInGUI(logFileWriter);
		System.out.println("Number of ASC's found in the GUI is " + numberOfASCs);
		for(int i=0; i<numberOfASCs; i++){
		//for(int i=0; i<10; i++){//implemented just for demo purposes
			HashMap<String, String> stringValues = ControlSheetUI.getASCStringAttributes(i, logFileWriter);
			HashMap<String, Integer> intValues = ControlSheetUI.getASCIntAttributes(i, logFileWriter);
			myList.add(new ExpectedASCData(stringValues, intValues));
		}//end for loop
		TreeSet<String> uniqueNamesAndDepts = this.getUniqueNamesAndDepts();
		ascFacultyValuesSet.addAllElements(uniqueNamesAndDepts, logFileWriter);
		for(int j=0; j<myList.size(); j++){
			String name = myList.get(j).getStringAttribute(ExpectedASCData.FacultyIndex);
			String dept = myList.get(j).getStringAttribute(ExpectedASCData.DepartmentIndex);
			myList.get(j).assignASCFacultyValues(ascFacultyValuesSet.getASCFacultyValueForNameAndDept(name, dept));
			String prevYearCommitment = ASCFacultyValuesSet.validCategories[ASCFacultyValuesSet.PreviousYearCommitmentsIndex];
			String KnownStage = ASCTest.stages[ASCTest.KnownStageIndex];
			if (myList.get(j).getStringAttribute(ExpectedASCData.CategoryIndex).equalsIgnoreCase(prevYearCommitment)
					&& (myList.get(j).getStringAttribute(ExpectedASCData.StageIndex).equalsIgnoreCase(KnownStage))){
				PYs.add(myList.get(j));
				outputMessage("\nExpectedASCData Object for faculty name " + name + " with department " + dept + " is a PY\n", logFileWriter);
			}
		}//end for loop - iterating through the list
		printMyList(logFileWriter);
	}//end populateListFromGUI method
	
	public boolean containsASCWithNameAndDept(String name, String dept){
		if (ascFacultyValuesSet.getASCFacultyValueForNameAndDept(name, dept) == null) return false;
		else return true;
	}//end containsASCWithNameAndDept method
	
	public static ArrayList<ExpectedASCData> getExpectedASCDataForNameAndDept(String name, String dept) throws Exception{
		ArrayList<ExpectedASCData> list = new ArrayList<ExpectedASCData>();
		for (int i=0; i<HandSOnTestSuite.expectedASCDataSet.size(); i++){
			ExpectedASCData asc = HandSOnTestSuite.expectedASCDataSet.getExpectedASCDataFromIndex(i);
			String actualName = asc.getStringAttribute(ExpectedASCData.FacultyIndex);
			String actualDept = asc.getStringAttribute(ExpectedASCData.DepartmentIndex);
			if (name.equalsIgnoreCase(actualName) && dept.equalsIgnoreCase(actualDept))
				list.add(asc);
		}//end for loop
		return list;
	}//end getExpectedASCDataForNameAndDept method
	
	/*
	 * We need to populate the local PYs list with the summary values (one per Pool,
	 * all Known stage, plus the sum of all Pools) of the comprehensive set of PYs.
	 * This should be done in several steps:
	 * (1) find out the summary values (per name, per pool, plus all pool total)
	 * of the comprehensive list of PYs.
	 * (2) update the summary values of the local PYs with the summary values derived
	 * from the comprehensive list of PYs.
	 */
	private TreeMap<String, ArrayList<Integer>> summarizePYValues(String typeOfShare, BufferedWriter logFileWriter) throws Exception{
		outputMessage("\n*** method summarizePYValues has been called ***\n", logFileWriter);
		int sizeOfComprehensiveList = HandSOnTestSuite.expectedASCDataSet.PYs.size();
		TreeSet<String> uniqueNameSet = new TreeSet<String>();
		//first, get the unique faculty names
		for(int i=0; i<sizeOfComprehensiveList; i++){
			uniqueNameSet.add(HandSOnTestSuite.expectedASCDataSet.PYs.get(i).getStringAttribute(ExpectedASCData.FacultyIndex));
		}
		String[] uniqueNames = uniqueNameSet.toArray(new String[uniqueNameSet.size()]);
		String message = "Unique names found in the comprehensive set are as follows: ";
		for (int i=0; i<uniqueNames.length; i++){
			message = message.concat(" " + uniqueNames[i]);
			if (i<uniqueNames.length-1) message = message.concat(",");
		}//end for - initializing the message string to report unique names
		outputMessage("\n"+message+"\n", logFileWriter);
		
		//next, get the discrete faculty names and the Source Pool sums of Known PY ASC's per faculty
		TreeMap<String, ArrayList<Integer>> faculty = new TreeMap<String, ArrayList<Integer>>();
		for(int i=0; i<uniqueNames.length; i++){
			int[] sourcePoolTotals = new int[ASCTest.sourcePools.length + 1];
			int sum = 0;
			for (int j=0;j<sizeOfComprehensiveList; j++){
				ExpectedASCData asc = HandSOnTestSuite.expectedASCDataSet.PYs.get(j);
				String name = asc.getStringAttribute(ExpectedASCData.FacultyIndex);
				String dept = asc.getStringAttribute(ExpectedASCData.DepartmentIndex);
				if (name.equalsIgnoreCase(uniqueNames[i]) && this.containsASCWithNameAndDept(name, dept)){
					String poolName = asc.getStringAttribute(ExpectedASCData.PoolIndex);
					int k= this.getIndexOfStringInArray(ASCTest.sourcePools, poolName);			
					if (typeOfShare.equalsIgnoreCase(ASCFacultyValues.ShareTotal))
						sourcePoolTotals[k] += asc.getAmountAttribute(ExpectedASCData.FTEAdjustedTotalAmountIndex);
					else if (typeOfShare.equalsIgnoreCase(ASCFacultyValues.DeptShare))
						sourcePoolTotals[k] += asc.getAmountAttribute(ExpectedASCData.FTEAdjustedDepartmentShareIndex);
					else sourcePoolTotals[k] += asc.getAmountAttribute(ExpectedASCData.FTEAdjustedDOShareIndex);
					sum +=sourcePoolTotals[k];
				}//end if - name matches
			}//end inner for - iterating through the comprehensive list of Known PY ASC's
			sourcePoolTotals[sourcePoolTotals.length - 1] = sum;
			ArrayList<Integer> sums = new ArrayList<Integer>();
			for (int j=0; j<sourcePoolTotals.length; j++){
				sums.add(new Integer(sourcePoolTotals[j]));
			}
			faculty.put(uniqueNames[i], sums);
		}//end outer for - iterating through each of the unique names that we know now
		return faculty;
	}
	
	public void populateMyASCFacultyValuesList(BufferedWriter logFileWriter) throws Exception{
		ascFacultyValuesSet.populateElements(logFileWriter);
	}//end populateMyASCFacultyValuesList method
	
	public void printMyList(BufferedWriter logFileWriter) throws Exception{
		TestFileController.writeToLogFile("Contents of the ExpectedASCDataSet are as follows", logFileWriter);
		for(int i=0; i<myList.size(); i++){
			TestFileController.writeToLogFile(myList.get(i).toString() + "\n", logFileWriter);
		}//end for
	}//end printMyList method

	public String[][] getExpectedStringDataValues (String[] attributeNames, BufferedWriter logFileWriter) throws Exception{
		outputMessage("*** method getExpectedStringDataValues called ***", logFileWriter);
		String[][] expectedData = new String[myList.size()][attributeNames.length];
		for(int i=0; i<myList.size(); i++){
			for (int j=0; j<attributeNames.length; j++){
				boolean match = false;
				for (int k=0; k<ExpectedASCData.StringAttributeKeys.length && (! match); k++){
					if (attributeNames[j].equalsIgnoreCase(ExpectedASCData.StringAttributeKeys[k])){
						expectedData[i][j] = myList.get(i).getStringAttribute(ExpectedASCData.StringAttributeKeys[k]);
						match = true;//stop iterating through the list once we have a match
						outputMessage("match found for attribute " + ExpectedASCData.StringAttributeKeys[k]
										+": value: " + expectedData[i][j], logFileWriter);
					}//end inner if - it's a match
				}//end inner-inner for - attribute name of each possible String attribute name for ExpectedASCData values
				for (int k=0; k<ASCFacultyValues.StringAttributeKeys.length && (! match); k++){
					if (attributeNames[j].equalsIgnoreCase(ASCFacultyValues.StringAttributeKeys[k])){
						expectedData[i][j] = myList.get(i).getASCFacultyValue(attributeNames[j]);
						match = true;//stop iterating through the list once we have a match
						outputMessage("match found for attribute " + attributeNames[j]
										+": value: " + expectedData[i][j], logFileWriter);
					}//end inner if - it's a match
				}//end inner-inner for - attribute name of each possible String attribute name for ASCFacultyValues values
			}//end inner for - attribute names of each ASCExpectedData Object in the list
		}//end outer for - iterating through myList
		return expectedData;
	}//end getExpectedStringDataValues method
	
	public int[][] getASCAmounts(String[] shareNames, BufferedWriter logFileWriter) throws Exception{
		return getASCAmounts(shareNames, false, false, false, false, logFileWriter);
	}//end getASCTotalAmounts method
	
	/*
	 * This method will return a two-dimensional int array much like the getExpectedShareValues method.
	 * 
	 * This method assumes that, within the shareNames String array passed as a parameter, 
	 * the "Total Amounts" is always the last element in the String array.  
	 * 
	 * Of note, we will have to build logic into this that will determine whether sums of ASC's are
	 * to be returned, and if so, determine what those sums are.  
	 * 
	 * We'll have to get the name of each faculty member in each ASC, and then call the 
	 * "getNumberOfASCsWithStringValue" method with the attribute "Name" and the name of the 
	 * faculty member and, if the number is greater than one, call the 'getASCFacultyValuesWithAttribute"
	 * method with the attribute "Name" and the faculty member's name, and then get the sum of 
	 * all applicable ASC amounts.
	 * 
	 * An additional factor in this is that, for some Faculty members with two ASC's, the pool contributions
	 * per ASC are added together - the sums of all values (maybe FTE-adjusted, maybe not) need to be
	 * returned in the two-dimensional array.  If the Faculty member is represented multiple times in the 
	 * two-dimensional array to be returned (entirely possible - the first dimension of the array is 
	 * the number of ASC's, not the number of discrete faculty members), then there will be multiple
	 * identical sums returned.
	 * 
	 * If we want to return these sums, we'll have to take the values from the 
	 * HandSOnTestSuite.expectedASCDataSet, not the local "myList" ArrayList.  The local "myList"
	 * might not have all of the relevant information as this method will probably be called from 
	 * a subset of HandSOnTestSuite.expectedASCDataSet, and won't have all the values needed for an
	 * adequate summary.  
	 * 
	 * For each and every faculty name that has multiple ASC's, we will need (1) the relevant indices
	 * of this faculty member for the two-dimensional array to be returned, (2) Some way to map the 
	 * String array shareNames (given as input) to the source pools, so we know which source pool 
	 * values to put into each one of the int arrays, each of which represents the ASC amounts to be
	 * returned, and (3) the relevant values to put into each of these arrays.  
	 * 
	 * Bottom line, we need to map different values to each other.  We have to map each name of faculty
	 * members with multiple ASC's to set of indices (within the first dimension of the 
	 * two-dimensional array) to be updated.  
	 * 
	 * We also have to have a separate array of values, each corresponding to one of the values 
	 * in the ASCTest class, each of which represents a source pool, plus the last value representing 
	 * the total.  This array, sourcePools, is the size of the shareNames String array, and is 
	 * intended to be a representation of shareNames, so that each value in the shareNames array has a 
	 * corresponding value in the sourcePools array at the same index in sourcePools 
	 * that the corresponding value has in the shareNames array (and, significantly, the same 
	 * second-dimension index in the two-dimensional array to be returned).  
	 * 
	 * Lastly, we need the sums to be placed.  We need to know if the values are to be FTE-adjusted,
	 * and if the Market Pool is to be included in the total, and if we are considering outside 
	 * appointments.  Based on these boolean variables, we adjust the values, then we sum them up.
	 * Adjust for FTE first, then add, because different ASC's for the same name might have different FTEs.
	 * In order to do this, we have to iterate through the comprehensive list of ASC's (in the 
	 * HandSOnTestSuite.expectedASCDataSet), and find the each ASC relevant to each faculty member
	 * that has multiple ASC's.  Every time we find such an ASC, we determine if we are to FTE-adjust
	 * it using the boolean variables, and find out the source pool of the ASC, and store it into 
	 * a one-dimensional array called "sums", each value of which corresponds to the sum of values 
	 * in each source pool.  We determine the index of the array to store the sum in based on the 
	 * source pool  of the ASC and the values stored in the "sourcePools" array.  It is assumed 
	 * that each value in the "sums" array will start at zero, and, when we store the value, we
	 * store it by incrementing it by the value we find in each relevant ASC.
	 * 
	 * Lastly, once we have the finalized "sums" array, we look up the relevant (first dimension) indices
	 * of the two-dimensional array to be returned, and update each sub-array with all of the values 
	 * in the "sums" array.  We do this for each ASC corresponding to a faculty member.
	 * 
	 * We also need to do this for each and every faculty member that has multiple ASC values.  So, 
	 * we will need to iterate through the list of faculty members and their corresponding indices,
	 * and do the steps outlined in the previous two paragraphs for every such faculty member.
	 * 
	 */
	
	public int[][] getASCAmounts(String[] shareNames, boolean adjustForFTE, boolean excludeMarketShare, 
			boolean consideringOutsideAppt, boolean summarizingASCs, BufferedWriter logFileWriter) throws Exception{
		outputMessage("\n\n*** method getASCAmounts called ***\n\n", logFileWriter);
		int[][] totalAmounts = new int[myList.size()][shareNames.length];
		String[] poolToShares = getPoolToShares(shareNames);
		for(int i=0; i<myList.size(); i++){
			String poolName = myList.get(i).getStringAttribute(ExpectedASCData.StringAttributeKeys[ExpectedASCData.PoolIndex]);
			outputMessage("Now working with Pool Name " + poolName + " at index " + i + " in the ExpectedASCDataSet", logFileWriter);
			int index = getIndexOfStringInArray(poolToShares, poolName);
			if ( index>=0 && index<shareNames.length){
				outputMessage("Pool Name "+ poolName + " found at index " + index +" in array of names given", logFileWriter);
			}//end if
			else{
				outputMessage("Pool " + poolName + " was not found in array of names given - assigning to the last name in the array", logFileWriter);
				index = shareNames.length - 1;//the last is always the sum
			}//end else
			outputMessage("adjustForFTE = " + adjustForFTE 
					+", summarizing ASC's = " + summarizingASCs
					+ ", consideringOutsideAppt = " + consideringOutsideAppt
					+ ", hasOutsideAppt = " + myList.get(i).hasOutsideAppt(), logFileWriter);
			if (excludeMarketShare && poolName.equalsIgnoreCase(ASCTest.sourcePools[ASCTest.MarketPoolIndex])){
				totalAmounts[i][index] = 0;
				totalAmounts[i][shareNames.length - 1] = 0;
				outputMessage("The Pool of the ASC is Market Pool - evaluating to zero", logFileWriter);
			}//end if - it's Market Pool
			else if ( adjustForFTE 
					&& (!(consideringOutsideAppt && myList.get(i).hasOutsideAppt())) 
					&& (myList.get(i).getAmountAttribute(ExpectedASCData.FTEPercentIndex) != 100) ){
				totalAmounts[i][index] = myList.get(i).getAmountAttribute(getCorrectAmountKey(shareNames[index], true));
				totalAmounts[i][shareNames.length - 1] = myList.get(i).getAmountAttribute(getCorrectAmountKey(shareNames[shareNames.length - 1], true));
				outputMessage("We are adjusting for FTE - make it the FTE-adjusted amount for Faculty " 
						+ myList.get(i).getStringAttribute(ExpectedASCData.FacultyIndex), logFileWriter);
			}
			else{
				outputMessage("We are not adjusting for FTE - make it the unadjusted amount for Faculty " 
						+ myList.get(i).getStringAttribute(ExpectedASCData.FacultyIndex), logFileWriter);
				totalAmounts[i][index] = myList.get(i).getAmountAttribute(getCorrectAmountKey(shareNames[index], false));
				totalAmounts[i][shareNames.length - 1] = myList.get(i).getAmountAttribute(getCorrectAmountKey(shareNames[shareNames.length - 1], false));
			}
			outputMessage("int value for ASC at index " + i +" with source pool " + poolName
				+ " found at index "+ index + " for column " + shareNames[index] + " is " + totalAmounts[i][index]
						+", and the total amount is " + totalAmounts[i][shareNames.length - 1], logFileWriter);
		}//end for loop - iterating through the list after all the amounts are present
		totalAmounts = adjustForSummarizedASCs(poolToShares, shareNames, totalAmounts, summarizingASCs, adjustForFTE, logFileWriter);
		return totalAmounts;
	}//end getASCAmounts method
	
	public int getNumberOfASCsWithStringValue(String attributeName, String attributeValue){
		int count = 0;
		for (int i=0; i<myList.size(); i++){
			ExpectedASCData asc = myList.get(i);
			if (asc.getStringAttribute(attributeName).equalsIgnoreCase(attributeValue)) count++;
		}//end for loop
		return count;
	}//end method getNumberOfASCsWithStringValue
	
	private String getCorrectAmountKey(String shareName, boolean adjustForFTE){
		if (shareName.toLowerCase().contains(ASCFacultyValues.DeptShare.toLowerCase())){
			if (adjustForFTE) return ExpectedASCData.amountAttributeKeys[ExpectedASCData.FTEAdjustedDepartmentShareIndex];
			else return ExpectedASCData.amountAttributeKeys[ExpectedASCData.OneHundredPercentDeptShareAmountIndex];
		}
		else if (shareName.toLowerCase().contains(ASCFacultyValues.DoShare.toLowerCase())){
			if (adjustForFTE) return ExpectedASCData.amountAttributeKeys[ExpectedASCData.FTEAdjustedDOShareIndex];
			else return ExpectedASCData.amountAttributeKeys[ExpectedASCData.OneHundredPercentDOShareAmountIndex];
		}
		else{
			if (adjustForFTE) return ExpectedASCData.amountAttributeKeys[ExpectedASCData.FTEAdjustedTotalAmountIndex];
			else return ExpectedASCData.amountAttributeKeys[ExpectedASCData.TotalAmountIndex];
		}
	}
	
	private int[][] adjustForSummarizedASCs(String[] poolToShares, String[] shareNames, int[][] preTotals, boolean summarizeCrossDepts, 
			boolean adjustForFTE, BufferedWriter logFileWriter) throws Exception{
		outputMessage("\n** For this test, ASC's belonging to the same faculty are summed up together **\n", logFileWriter);
		TreeMap<String, ArrayList<Integer>> multipleASCData = getMultipleASCData(summarizeCrossDepts, logFileWriter);
		for(Map.Entry<String, ArrayList<Integer>> entry : multipleASCData.entrySet()) {
			int[] summaryValues = new int[poolToShares.length];
			int FTEPercent = 100;
			int summaryAmt = 0;
			boolean addToSummary = false;
			String name = entry.getKey();
			ArrayList<Integer> indices = entry.getValue();
			outputMessage("\nFaculty "+ name + " has multiple ASC's\n", logFileWriter);
			for(int i=0; i<indices.size(); i++){
				  int summaryIndex = indices.get(i).intValue();//this is the index within the HandSOnTestSuite.expectedASCDataSet.
				  /*
				   * We have an issue.  With regards to Faculty members who have DO shares
				   * or values that should appear in the total of the summaries but who have
				   * Dept. Shares in Source Pools that also explicitly figure into the report.
				   * The Dept. Share gets captured, yes, but the summary information
				   * (such as the DO Share for Robert Byer, who has two ASC's, one of which
				   * (corresponding to the Gender Pool) gets summary information captured
				   * (the DO Share on the Dean and School Salary Setting reports) but the other 
				   * of which corresponds to the Regular Merit Pool and the Dept. Share gets 
				   * captured in the summary totals but the DO Share component doesn't get 
				   * captured in the summary totals.  If we simply make the summary total 
				   * get updated each and every time, something else gets thrown off.  I notice
				   * that the Total Raise $ for the Provost Binder Reports get thrown off.
				   * It becomes twice what it should be. 
				   * The root of the problem seems to be that, if we add code to update 
				   * and add to the summary from the ASC "DO Share" value to the summary
				   * in the first block of code, which looks at the ASC, it does get added
				   * when the Source Pool of the ASC doesn't figure into the column names
				   * of the report.  However, if it does figure into the column names of 
				   * the report, and the column name is found to be compatible with the 
				   * summary, it actually gets added twice.  
				   * In the case of Robert Byer, we have the Gender Pool as one of the ASC's
				   * Source Pools, and it is not one of the columns so 
				   * it does get added in the first block of code but not in the second block, 
				   * which looks only at the columns of the report, not the values in the ASC.
				   * 
				   * This merits careful investigation.
				   * Offhand, there is a temptation to simply add a line of code which tells
				   * the system to getCorrectAmountKey, and pass the name of the summary
				   * column to it when the shareNameIndex has a valid value (not -1) but
				   * this, again, results in the Total Raise $ amount for the Provost Binder
				   * Reports getting double what it should.
				   */
				  ExpectedASCData asc = HandSOnTestSuite.expectedASCDataSet.getExpectedASCDataFromIndex(summaryIndex);
				  String poolName = asc.getStringAttribute(ExpectedASCData.PoolIndex);
				  FTEPercent = asc.getAmountAttribute(ExpectedASCData.FTEPercentIndex);
				  summaryAmt = asc.getAmountAttribute(getCorrectAmountKey(shareNames[shareNames.length - 1], adjustForFTE));
				  outputMessage("Summary column is " + shareNames[shareNames.length - 1], logFileWriter);
				  outputMessage("Pool being analyzed has name "+poolName, logFileWriter);
				  outputMessage("ASC being analyzed has index "+summaryIndex, logFileWriter);
				  outputMessage("Summary Amount from ASC is determined to be "+summaryAmt, logFileWriter);
				  if (poolName != null && poolName.length() > 0){
					  int shareNameIndex = getIndexOfStringInArray(poolToShares, poolName);
					  if (shareNameIndex == -1){//the source pool is not found
						  outputMessage("Pool "+ poolName +" is not found in shareNames array - adding to summary only", logFileWriter);
						  summaryValues[summaryValues.length - 1] += summaryAmt;
						  outputMessage("Summary column " + shareNames[shareNames.length - 1] 
								  + " is updated to have value " + summaryValues[summaryValues.length - 1], logFileWriter);
					  }//end if - the source pool is not found
					  else{
						  outputMessage("Name of Pool being analyzed is " + poolName 
								  + " from ASC at index " + summaryIndex, logFileWriter);
						  int poolAmt = asc.getAmountAttribute(getCorrectAmountKey(shareNames[shareNameIndex], adjustForFTE));
						  outputMessage("Adding amount " + poolAmt + " from Pool " + poolName
								  + " to the summary values - column " + shareNames[shareNameIndex] 
										  + " at pool index " + shareNameIndex, logFileWriter);
						  summaryValues[shareNameIndex] += poolAmt;
						  /*
						  if (summaryAmt > 0){
							  summaryValues[summaryValues.length - 1] += summaryAmt;
							  outputMessage("Summary column " + shareNames[shareNames.length - 1] 
									  + " is also updated to have value " + summaryValues[summaryValues.length - 1], logFileWriter);
						  }
						  */
					  }//end else - the source pool is found
					  /*
					  summaryValues[summaryValues.length - 1] += summaryAmt;
					  outputMessage("Summary column " + shareNames[shareNames.length - 1] 
							  + " is updated to have value " + summaryValues[summaryValues.length - 1], logFileWriter);
					   */
				  }//end if
			 }//end for - iterating through the list of Integer values
			 outputMessage("Faculty " + name + " has the following expected values:", logFileWriter);
			 for (int j=0; j<poolToShares.length - 1; j++){
				 boolean poolAmtIsDeptShare = shareNames[j].toLowerCase().contains(ASCFacultyValues.DeptShare.toLowerCase());
				 boolean poolAmtIsDOShare = shareNames[j].toLowerCase().contains(ASCFacultyValues.DoShare.toLowerCase());
				 boolean summaryAmtIsDOShare = shareNames[summaryValues.length-1].toLowerCase().contains(ASCFacultyValues.DoShare.toLowerCase());
				 boolean summaryAmtIsDeptShare = shareNames[summaryValues.length-1].toLowerCase().contains(ASCFacultyValues.DeptShare.toLowerCase());
				 //if the total column is truly total and not "DO Share" only or "Dept Share" only, add it
				 if ( (poolAmtIsDeptShare && (! summaryAmtIsDOShare) ) 
						 || (poolAmtIsDOShare && (! summaryAmtIsDeptShare))
						 || ((! poolAmtIsDOShare) && (! poolAmtIsDeptShare))
						 ){
					 int amountToAdd = summaryValues[j];
					 if ((! adjustForFTE) && (FTEPercent != 100)) amountToAdd = (summaryValues[j]/100)*FTEPercent;
					 outputMessage("Column " + shareNames[j] +" is compatible with summary " 
						 + shareNames[summaryValues.length-1] 
							+ ", adding value " + amountToAdd, logFileWriter);
					 summaryValues[summaryValues.length-1] += amountToAdd;
				 }//end if - we can add this to the summary
				 else{
					 outputMessage("Column " + shareNames[j] +" is NOT compatible with summary " 
							 + shareNames[summaryValues.length-1] + ", so it WON'T be added", logFileWriter);
					 outputMessage("... but the corresponding DO Share of its ASC ("+ summaryAmt
							 +") will be", logFileWriter);
					 //put code in here to determine the summary amount in the ASC and add it
					 addToSummary = true;
				 }//end else - adding to the summary value if the column is not compatible
			 }//end for - iterating through the pools to see what can be added
			 if (addToSummary){
				 summaryValues[summaryValues.length-1] += summaryAmt;
				 outputMessage("Summary Column " + shareNames[summaryValues.length-1]
						 +" is updated to " + summaryValues[summaryValues.length-1], logFileWriter);
			 }//end if - add the summary value of the ASC to the total
			 for (int j=0; j<poolToShares.length; j++){
				  outputMessage("Source Pool: " + poolToShares[j] +", column " + shareNames[j]
						  +" has the following value: " + summaryValues[j], logFileWriter);
			 }//end for - iterating through all of the share names for the source pool
			 //OK, now that we know what the summary values are, update totalAmounts appropriately
			 for (int j=0; j<myList.size(); j++){
				 ExpectedASCData asc = myList.get(j);
				 if(asc.getStringAttribute(ExpectedASCData.FacultyIndex).equalsIgnoreCase(name)){
					 outputMessage("Name match on multiple ASC's - Faculty " + name, logFileWriter);
					 preTotals[j] = summaryValues;
				 }
			 }
		}//end for - iterating through the list of Faculty members, each having multiple ASC's
	return preTotals;
	}
	
	public String[] getPoolToShares(String[] shareNames){
		//if (shareNames.length == 0) return new String[0];
		String[] poolToShares = new String[shareNames.length];
		//do this kludge until HANDSON-3290 is fixed
		int nullIndex = getIndexOfShareAmount(shareNames, "null");
		if (nullIndex != -1){
			poolToShares[nullIndex] = ASCTest.sourcePools[ASCTest.GenderPoolIndex];
		}//end inner if - do this if the Gender Pool column still has the "null" in it
		for (int i=0; i<poolToShares.length-1; i++){
			for (int j=0; j<shareNames.length-1; j++){
				if (shareNames[i].contains(ASCTest.sourcePools[j])){
					poolToShares[i] = ASCTest.sourcePools[j];
					break;
				}//end if
			}//end inner for - iterating through the shareNames array
		}//end outer for - iterating through the poolToShares array
		int lastIndex = shareNames.length - 1;
		poolToShares[lastIndex] = "Total";
		return poolToShares;
	}
	/*
	 * We have an issue - if the Faculty has multiple ASC's but Appointment(s)
	 * within one department, this works fine, but if the Faculty member has
	 * Appointments in more than one department, and therefore has multiple
	 * ASCFacultyValues objects, then the necessary addition of the different
	 * numbers in the ASCFacultyValues Objects doesn't happen.
	 */
	
	public TreeMap<String, ArrayList<Integer>> getMultipleASCData(boolean summarizeCrossDepts, BufferedWriter logFileWriter) throws Exception{
		outputMessage("\n*** method getMultipleASCData called ***\n", logFileWriter);
		TreeMap<String, ArrayList<Integer>> multipleASCData = new TreeMap<String, ArrayList<Integer>>();
		ExpectedASCDataSet superset = HandSOnTestSuite.expectedASCDataSet;
		String facultyAttributeName = ExpectedASCData.StringAttributeKeys[ExpectedASCData.FacultyIndex];
		String deptAttributeName = ExpectedASCData.StringAttributeKeys[ExpectedASCData.DepartmentIndex];
		for (int i=0; i<superset.size(); i++){
			ArrayList<Integer> multipleASCIndices = new ArrayList<Integer>();
			ExpectedASCData asc = superset.getExpectedASCDataFromIndex(i);
			String name = asc.getStringAttribute(facultyAttributeName);
			String dept = asc.getStringAttribute(deptAttributeName);
			if ( (superset.getNumberOfASCsWithStringValue(facultyAttributeName, name)>1)
					&& asc.getStringAttribute(ExpectedASCData.StageIndex).equalsIgnoreCase(ASCTest.stages[ASCTest.KnownStageIndex])
					&& (! asc.getStringAttribute(ExpectedASCData.CategoryIndex).equalsIgnoreCase(ASCFacultyValuesSet.validCategories[ASCFacultyValuesSet.PreviousYearCommitmentsIndex]))
					){
				//find out what the indices are (values of i) which share the common name, and put them into the ArrayList
				//iterate through the list, getting the indices of the other ExpectedASCData Objects which share this same name
				multipleASCIndices.add(new Integer(i));
				for (int j=0;j<superset.size(); j++){
					if (superset.getExpectedASCDataFromIndex(j).getStringAttribute(ExpectedASCData.FacultyIndex).equalsIgnoreCase(name)
							&& ( summarizeCrossDepts || superset.getExpectedASCDataFromIndex(j).getStringAttribute(ExpectedASCData.DepartmentIndex).equalsIgnoreCase(dept))
							&& (j != i) 
							&& superset.getExpectedASCDataFromIndex(j).getStringAttribute(ExpectedASCData.StageIndex).equalsIgnoreCase(ASCTest.stages[ASCTest.KnownStageIndex])
							&& (! superset.getExpectedASCDataFromIndex(j).getStringAttribute(ExpectedASCData.CategoryIndex).equalsIgnoreCase(ASCFacultyValuesSet.validCategories[ASCFacultyValuesSet.PreviousYearCommitmentsIndex]))
							){
						multipleASCIndices.add(new Integer(j));
					}//end if - add the 
				}//end inner for - iterating through the list again to find matches
				if (multipleASCIndices.size() > 1){
					multipleASCData.put(name, multipleASCIndices);
					String message = "Faculty " + name + " has the following multiple indices: ";
					for(int k=0; k<multipleASCIndices.size(); k++)
						message = message.concat(multipleASCIndices.get(k) + "; ");
					outputMessage(message, logFileWriter);
				}
			}//end if - there are multiple ASC's involved
		}//end for loop - iterating through the list
		return multipleASCData;
	}//end getMultipleASCData method
	
	public ExpectedASCData getExpectedASCDataFromIndex(int index){
		return myList.get(index);
	}
	
	//use this to return the index of the String shareName in the shareNames String array
	protected int getIndexOfShareAmount(String[] shareNames, String targetString){
		int index = -1;
		for (int i=0; i<shareNames.length; i++){
			if (shareNames[i].contains(targetString)){
				index = i;
				break;
			}//end if
		}//end for loop - iterating through the shareNames array
		return index;
	}//end getIndexOfShareAmount array
	
	protected int getIndexOfStringInArray(String[] values, String value){
		int index = -1;
		for (int i=0; i<values.length; i++){
			if (values[i].equalsIgnoreCase(value)){
				index = i;
				break;
			}//end if
		}//end for loop - iterating through the shareNames array
		return index;
	}//end getIndexOfStringInArray array



	public int[][] getExpectedAmountDataValues(String[] attributeNames, BufferedWriter logFileWriter) throws Exception{
		outputMessage("*** method getExpectedAmountDataValues called ***", logFileWriter);
		int[][] expectedData = new int[myList.size()][attributeNames.length];
		for(int i=0; i<myList.size(); i++){
			for (int j=0; j<attributeNames.length; j++){
				boolean match = false;
				for (int k=0; k<ExpectedASCData.amountAttributeKeys.length && (! match); k++){
					if (attributeNames[j].equalsIgnoreCase(ExpectedASCData.amountAttributeKeys[k])){
						expectedData[i][j] = myList.get(i).getAmountAttribute(ExpectedASCData.amountAttributeKeys[k]);
						match = true;//stop iterating through the list once we have a match
						outputMessage("match found for attribute " + ExpectedASCData.amountAttributeKeys[k]
										+": value: " + expectedData[i][j], logFileWriter);
					}//end if - it's a match
				}//end inner-inner for - iterating through the possible key matches
			}//end inner for - iterating through the list of attributes passed to this method
		}//end outer for - iterating through the list of ExpectedASCData Objects
		return expectedData;
	}//end getExpectedAmountAttributes method
	
	/*
	 * This will extract summary values from the Control Sheet Detail Report
	 * and store them in a two-dimensional int array and return it.
	 * 
	 * It will use the getColumnSummaryValues method, take the first of the BigDecimal
	 * values returned, verify that it equals "1" and then take the second BigDecimal
	 * and store it as one entry in the int[][] array to be returned.
	 * It will take the following inputs: 
	 * String sheetName, 
	 * HashMap<String, String> criteria, 
	 * boolean emptyDatesExcluded, 
	 * boolean outsideDatesIncrementFacultyCount, 
	 * boolean outsideDatesIncrementSalarySum, 
	 * String[] columnNames, 
	 * Date minDate, 
	 * Date maxDate, 
	 * BufferedWriter logFileWriter) throws Exception{
	 * 
	 */
	public int[][] getExpectedSummaryValues(TreeMap<String, ArrayList<String>> criteria, BufferedWriter logFileWriter) throws Exception{
		outputMessage("*** method getExpectedSummaryValues called ***", logFileWriter);
		int[][] summaryData = new int[myList.size()][criteria.size()];
		for(int i=0; i<myList.size(); i++){
			int j = 0;
			for(Map.Entry<String, ArrayList<String>> entry : criteria.entrySet()){
				String key = entry.getKey();
				ArrayList<String> valueKeys = criteria.get(key);
				String sheetName = TestFileController.ControlSheetDetailReportWorksheetName;
				HashMap<String, String> searchCriteria = new HashMap<String, String>();
				outputMessage("Summary Value Criteria includes the following name-value pairs:", logFileWriter);
				for (int k=0; k<valueKeys.size(); k++){
					String attribName = valueKeys.get(k);
					String attribValue = myList.get(i).getStringAttribute(attribName);
					searchCriteria.put(attribName, attribValue);
					outputMessage("Name: " + attribName + ", Value: " + attribValue, logFileWriter);
				}//end inner for - the searchCriteria are initialized
				boolean emptyDatesExcluded = false;
				boolean outsideDatesIncrementFacultyCount = true;
				boolean outsideDatesIncrementSalarySum = true;
				String[] columnNames = new String[1];
				columnNames[0] = key;
				outputMessage("Summary Value column name to be examined is " + columnNames[0], logFileWriter);
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				Date minDate = sdf.parse("09/01/2016");
				Date maxDate = sdf.parse("09/01/2017");
				TestFileController.ControlSheetReportSuite.setRowForColumnNames(TestFileController.ControlSheetDetailReportColumnHeaderRow);
				BigDecimal[] rawValues = TestFileController.ControlSheetReportSuite.getColumnSummaryValues(sheetName, 
						searchCriteria, emptyDatesExcluded, outsideDatesIncrementFacultyCount, 
						outsideDatesIncrementSalarySum, columnNames, minDate, maxDate, logFileWriter);
				if (rawValues[0].intValue() != 1) outputMessage ("*** WARNING - more than one Faculty shares this search criteria", logFileWriter);
				summaryData[i][j++] = rawValues[1].intValue();
				outputMessage("Summary value to be used for Faculty " 
							+ myList.get(i).getStringAttribute(ExpectedASCData.FacultyIndex)
							+ " is " + rawValues[1].intValue(), logFileWriter);
				logFileWriter.newLine();
			}//end inner for
		}//end outer for
		return summaryData;
	}//end getExpectedSummaryValues method
	
	/*
	 * This method will access each individual ExpectedASCData Object in myList
	 * and obtain values and/or sums from the deptShares and doShares arrays contained
	 * in the ASCFacultyValues Object associated with the ExpectedASCData Object.
	 * There are some very specific given as input.
	 * Each element in the "criteria" TreeMap has a key and a value.  The key is 
	 * the name of the column that the deptShare/doShare value and/or sum is to map to.
	 * The value is an ArrayList consisting of three String values.  
	 * The first is the indicator of whether the doShare array is to be accessed or 
	 * the deptShare array is to be accessed or the sum of the two.  
	 * The second is the indicator of which Source Pool is to be accessed or the sum 
	 * of all Source Pools.
	 * The third is the indicator of which Stage is to be accessed.  It may or may not
	 * be present.  If it is not present, then it is assumed that the Stage desired is "Known".
	 * If a sum is desired in either of the first two indicators, the value "Total" is submitted.
	 * It is assumed that the constant "Total" will never be applied to the third indicator,
	 * corresponding to the stage.
	 * Of note, in at least one case - the Department Verification Report, all Market Shares are
	 * excluded.  Hence, we give the users the option to exclude the Market Share from the total
	 * calculation through the boolean "excludeMarketShare" value.
	 * Also, of note, there is at least one case - the Provost Binder Reports - in which the
	 * values returned are sums of FTE-adjusted values, if one or more of the faculty has two or
	 * more ASC's.  This is the case even if only one of those ASC's is represented in the myList 
	 * ArrayList.
	 */
	public int[][] getExpectedShareValues(String[] shareNames, TreeMap<String, ArrayList<String>> criteria, 
			boolean adjustForFTE, boolean excludeMarketShare, boolean consideringOutsideAppt, 
			boolean backoutPYs, boolean summarizingASCs, BufferedWriter logFileWriter) throws Exception{
		if (shareNames.length == 0) return new int[myList.size()][0];
		outputMessage("\n*** method getExpectedShareValues called ***\n", logFileWriter);
		int[][] shareData = new int[myList.size()][criteria.size()];
		String[] poolToShares = getPoolToShares(shareNames);
		for(int i=0; i<myList.size(); i++){
			ExpectedASCData asc = myList.get(i);
			String facultyName = asc.getStringAttribute(ExpectedASCData.FacultyIndex);
			for(int j=0;j<shareNames.length; j++){
				ArrayList<String> valueKeys = criteria.get(shareNames[j]);
				int stage = -1;
				if(valueKeys.size() == 3) stage = java.util.Arrays.asList(ASCTest.stages).indexOf(valueKeys.get(2));
				else stage = ASCTest.KnownStageIndex;
				String shareName = valueKeys.get(1);
				//we have to add this to accommodate bug HANDSON-3290 so the whole test doesn't fail
				int sourcePool = -1;
				if (valueKeys.get(1).equalsIgnoreCase(ASCFacultyValues.ShareTotal))
					sourcePool = -2;
				//until HANDSON-3290 is fixed, we have to have this kludge so that the test produces meaningful results
				else if (shareName.contains("null")){
					sourcePool = ASCTest.GenderPoolIndex;
				}//end else if - it's the Gender Pool
				else{
					for (int k=0; k< ASCTest.sourcePools.length; k++){
						if (shareName.contains(ASCTest.sourcePools[k])){
							sourcePool = k;
							break;
						}//end if
					}//end for
				}//end else - it's a defined source pool
				if (valueKeys.get(0).equalsIgnoreCase(ASCFacultyValues.DeptShare)){
					if (shareName.contains(ASCFacultyValues.ShareTotal)){
						shareData[i][j] = asc.getAllDeptSharesForStage(stage, excludeMarketShare);
					}//end if - all source pools
					else{
						shareData[i][j] = asc.getDeptShare(stage, sourcePool);
					}//end else - one source pool
					if (backoutPYs && (stage == ASCTest.KnownStageIndex)){
						shareData[i][j] -= backOutPYs(asc, sourcePool, logFileWriter, ASCFacultyValues.DeptShare);
					}
				}//end if - the Dept Share is desired in this case
				else if (valueKeys.get(0).equalsIgnoreCase(ASCFacultyValues.ShareTotal)){
					if (shareName.contains(ASCFacultyValues.ShareTotal)){
						shareData[i][j] = asc.getAllSharesForStage(stage, excludeMarketShare);
						//we can implement this later - we need to exclude any Previous Year Commitment amounts
						if (backoutPYs && (stage == ASCTest.KnownStageIndex)){
							shareData[i][j] -= backOutPYs(asc, sourcePool, logFileWriter);
						}//end if - we're backing out PY's
					}//end if - all source pools
					else {
						shareData[i][j] = asc.getAllSharesForStageAndPool(stage, sourcePool);
						if (backoutPYs && (stage == ASCTest.KnownStageIndex)){
							shareData[i][j] -= backOutPYs(asc, sourcePool, logFileWriter);
						}//end if - we're backing out PY's
					}//end else - one source pool
				}//end if - the DO Share plus the Dept Share is desired in this case
				else{//only the DO Share is desired
					if (shareName.contains(ASCFacultyValues.ShareTotal)){
						shareData[i][j] = asc.getAllDOSharesForStage(stage);
					}//end if - get the sum for all source pools
					else {
						shareData[i][j] = asc.getDOShare(stage, sourcePool);
					}//end else - get the DO Share for the source pool
					if (backoutPYs && (stage == ASCTest.KnownStageIndex)){
						shareData[i][j] -= backOutPYs(asc, sourcePool, logFileWriter, ASCFacultyValues.DoShare);
					}
				}//end else - the DO Share is desired in this case
				if (! ((!adjustForFTE) || shareData[i][j] == 0 || asc.getAmountAttribute(ExpectedASCData.FTEPercentIndex) == 100 ) ){
					if (consideringOutsideAppt){
						if (asc.hasOutsideAppt()){
							double temp = shareData[i][j];
							double divisor = (double) asc.getAmountAttribute(ExpectedASCData.FTEPercentIndex);
							double multiplier = (double) 100;
							double temp2 = temp/divisor; 
							double temp3 = temp2*multiplier;
							shareData[i][j] = (int)Math.round(temp3);
							outputMessage("\nadjustment for FTE is being reversed as Faculty " +facultyName +" has outside appointment, "
									+" divisor is " + divisor
									+", when divided, the interim value is " + temp2
									+", when multiplied, the second interim double value is " + temp3
									+", and final int value is "
									+ shareData[i][j] + "\n", logFileWriter);
						}//end if - the Faculty member has an outside appointment
						else{
							outputMessage("\nWe are still adjusting for FTE as Faculty " +facultyName +" has no outside appointment" 
											+ " - make it the FTE-adjusted amount of "+ shareData[i][j] + "\n", logFileWriter);
						}//end else - no outside appointment, so we still adjust for FTE
					}//end if - outside appointments are considered in this test case
					else{
						outputMessage("\n FTE adjustment is being applied for Faculty " +facultyName 
								+", and final int value is "
										+ shareData[i][j] + ";\n", logFileWriter);
					}//end else - we have to reverse the adjustment, and there's no outside appointment
				}//end if - the share is eligible for FTE adjustment
				else if (! (shareData[i][j] == 0 || asc.getAmountAttribute(ExpectedASCData.FTEPercentIndex) == 100) ){
					double temp = shareData[i][j];
					double divisor = (double) asc.getAmountAttribute(ExpectedASCData.FTEPercentIndex);
					double multiplier = (double) 100;
					double temp2 = temp/divisor; 
					double temp3 = temp2*multiplier;
					shareData[i][j] = (int) Math.round(temp3);
					outputMessage("\n FTE adjustment is being reversed for Faculty " +facultyName 
							+"; divisor is " + divisor
							+", when divided, the interim value is " + temp2
							+", when multiplied, the second interim double value is " + temp3
							+", and final int value is "
									+ shareData[i][j] + ";\n", logFileWriter);
				}//end else - we have to reverse the adjustment, and there's no outside appointment
				String sourcePoolName = shareNames[j];
				if (sourcePool>0 && sourcePool<ASCTest.sourcePools.length)
					sourcePoolName = ASCTest.sourcePools[sourcePool];
				outputMessage("value being returned for column " + shareNames[j]
						+ " for share type " + valueKeys.get(0)
						+ " with Stage " + ASCTest.stages[stage] 
						+ " and Pool " + sourcePoolName
						+ " is " + shareData[i][j] +";\n", logFileWriter);
			}//end inner for - iterating through all of the criteria
		}//end outer for - iterating through myList
		shareData = adjustForSummarizedASCs(poolToShares, shareNames, shareData, summarizingASCs, adjustForFTE, logFileWriter);
		return shareData;
	}//end method
	
	private int backOutPYs(ExpectedASCData asc, int sourcePool, BufferedWriter logFileWriter, String typeOfShare) throws Exception{
		outputMessage("\nBacking Out Previous Year Commitments\n", logFileWriter);
		TreeMap<String, ArrayList<Integer>> faculty = summarizePYValues(typeOfShare, logFileWriter);
		String pyFaculty = asc.getStringAttribute(ExpectedASCData.FacultyIndex);
		if (faculty.containsKey(pyFaculty)){
			//extract the total value of all shares
			ArrayList<Integer> sumsToBackOut = faculty.get(pyFaculty);
			String poolName = new String();
			if (sourcePool == -2){
				poolName = "Total";
				sourcePool = sumsToBackOut.size() - 1;//the last index in sumsToBackOut is the sum
			}
			else if (sourcePool == -1) poolName = "Not Found";
			else poolName = ASCTest.sourcePools[sourcePool];
			outputMessage(pyFaculty + ":: Amt backed out from source pool " 
					+ poolName + " = " + sumsToBackOut.get(sourcePool), logFileWriter);
			return sumsToBackOut.get(sourcePool);
		}//end if - the faculty member's name is included
		return 0;
	}//end backOutPYs method
	
	private int backOutPYs(ExpectedASCData asc, int sourcePool, BufferedWriter logFileWriter) throws Exception{
		return backOutPYs(asc, sourcePool, logFileWriter, ASCFacultyValues.ShareTotal);
	}
	
	public ArrayList<ASCFacultyValues> getASCFacultyValuesWithAttribute(String attributeName, String attributeValue){
		return ascFacultyValuesSet.getASCFacultyValuesWithAttribute(attributeName, attributeValue);
	}


	
	//if the "excludeMarketShare" value is not included, assume it's false
	public int[][] getExpectedShareValues(String[] shareNames, TreeMap<String, ArrayList<String>> criteria, 
			boolean adjustForFTE, BufferedWriter logFileWriter) throws Exception{
		return getExpectedShareValues(shareNames, criteria, adjustForFTE, false, false, false, false, logFileWriter);
	}
	/*
	 * This method takes multiple criteria of multiple sorts and returns a subset of 
	 * ExpectedASCData Objects in an ExpectedASCDataSet.  The criteria is a TreeMap
	 * of Strings representing attribute names which map to attribute values.
	 * 
	 * It looks at the first criterion in the TreeMap only, then removes it from the TreeMap
	 * 
	 * It determines what sort of criterion it is (that is, what kind of comparison needs to be
	 * made to determine whether or not the criterion is satisfied).
	 * 
	 * If there are more criteria in the TreeMap to be satisfied, it recursively calls 
	 * itself with the TreeMap which has been reduced by one.
	 */
	public ExpectedASCDataSet subset(TreeMap<String, String> criteria, 
				BufferedWriter logFileWriter) throws Exception{
		outputMessage("*** method subset called ***", logFileWriter);
		outputMessage("There are " + criteria.size() + " criteria remaining", logFileWriter);
		if (criteria.isEmpty()){
			return this;
		}//end if - criteria is empty
		ExpectedASCDataSet setForCriteria = new ExpectedASCDataSet();
		Entry<String, String> entry = criteria.firstEntry();
		String key = entry.getKey();
		String value = entry.getValue();
		outputMessage("criterion analyzed has key " + key + " and value " + value, logFileWriter);
		TreeMap<String, String> subsetCriteria = new TreeMap<String, String>();
		subsetCriteria.putAll(criteria);
		subsetCriteria.remove(key);
		outputMessage("Key " + key + " with value " + value + " has been removed from the criteria", logFileWriter);
		if (
			key.equalsIgnoreCase(ExpectedASCData.StringAttributeKeys[ExpectedASCData.DescriptionIndex])
			|| key.equalsIgnoreCase(ExpectedASCData.StringAttributeKeys[ExpectedASCData.NotesIndex])
							){
			setForCriteria = getSetForStringSubsetCriteria(key, value, logFileWriter);
		}
		else if (key.equalsIgnoreCase(ExpectedASCData.isDean) 
				|| key.equalsIgnoreCase(ExpectedASCData.isDeptChair)){
			setForCriteria = getSetForBooleanCriteria (key, value.equalsIgnoreCase("true"), logFileWriter);
		}
		else setForCriteria = getSetForStringCriteria(key, value, logFileWriter);
		
		if (subsetCriteria.isEmpty()){
			outputMessage ("last criterion has been applied ... returning", logFileWriter);
			setForCriteria.populateMyASCFacultyValuesList(logFileWriter);
			return setForCriteria;
		}
		else{
			outputMessage ("more criteria need to be applied ... calling subset recursively", logFileWriter);
			return setForCriteria.subset(subsetCriteria, logFileWriter);
		}
	}
	
	public ExpectedASCDataSet getSetForStringCriteria(String attributeName, 
			String attributeValue, BufferedWriter logFileWriter) throws Exception{
		outputMessage("*** method getSetForStringCriteria called ***", logFileWriter);
		ExpectedASCDataSet setForCriteria = new ExpectedASCDataSet();
		for (int i=0; i<myList.size(); i++){
			if (myList.get(i).getStringAttribute(attributeName).equalsIgnoreCase(attributeValue)){
				setForCriteria.add(myList.get(i));
				outputMessage("Match found on attribute " + attributeName
							+": " + myList.get(i).getStringAttribute(attributeName)
							+", for Faculty " + myList.get(i).getStringAttribute(ExpectedASCData.FacultyIndex)
							+", at index " + i, logFileWriter);
			}//end if - it's a match; add this to the ExpectedASCDataSet to be returned
		}//end for - iterating through myList
		setForCriteria.addAllElements(setForCriteria.getUniqueNamesAndDepts(), logFileWriter);
		//this.addAllElements(this.getUniqueNamesAndDepts(), logFileWriter);
		outputMessage("ExpectedASCDataSet being returned with size " 
						+ setForCriteria.size(), logFileWriter);
		return setForCriteria;
	}//end getSetForCriteria method 
	
	public ExpectedASCDataSet getSetForBooleanCriteria (String attributeName, 
			boolean attributeValue, BufferedWriter logFileWriter) throws Exception{
		outputMessage("*** method getSetForBooleanCriteria called ***", logFileWriter);
		ExpectedASCDataSet setForCriteria = new ExpectedASCDataSet();
		for (int i=0; i<myList.size(); i++){
			if (attributeName.equalsIgnoreCase(ExpectedASCData.isDean)){
				if (myList.get(i).isDean() == attributeValue){
					setForCriteria.add(myList.get(i));
					outputMessage("Match found on attribute " + attributeName
							+": " + attributeValue +", at index " + i, logFileWriter);
				}//end if - the criteria (of whether the faculty is a dean or not) matches
			}//if this concerns the dean, then see if it matches the expected criteria - true or false?
			if (attributeName.equalsIgnoreCase(ExpectedASCData.isDeptChair)){
				if (myList.get(i).isDeptChair() == attributeValue){
					setForCriteria.add(myList.get(i));
					outputMessage("Match found on attribute " + attributeName
							+": " + attributeValue +", at index " + i, logFileWriter);
				}//end if - the criteria (of whether the faculty is a Dept Chair or not) matches
			}//if this concerns the dean, then see if it matches the expected criteria - true or false?
		}//end for - iterating through the list
		outputMessage("ExpectedASCDataSet being returned with size " 
				+ setForCriteria.size(), logFileWriter);
		setForCriteria.addAllElements(setForCriteria.getUniqueNamesAndDepts(), logFileWriter);
		//this.addAllElements(this.getUniqueNamesAndDepts(), logFileWriter);
		return setForCriteria;
	}//end getSetForBooleanCriteria method

	public ExpectedASCDataSet getSetForStringSubsetCriteria (String attributeName, 
						String subset, BufferedWriter logFileWriter) throws Exception{
		outputMessage("*** method getSetForStringSubsetCriteria called ***", logFileWriter);
		ExpectedASCDataSet setForCriteria = new ExpectedASCDataSet();
		for (int i=0; i<myList.size(); i++){
			if (! myList.get(i).getStringAttribute(attributeName).isEmpty()){
				if (myList.get(i).getStringAttribute(attributeName).contains(subset)){
					setForCriteria.add(myList.get(i));
					outputMessage("Substring is contained within attribute " + attributeName
							+": " + myList.get(i).getStringAttribute(attributeName) 
							+", at index " + i, logFileWriter);
				}//end if - the subset is contained in the attribute value
			}//end outer if - the list is not empty
		}//end for - iterating through the list
		outputMessage("ExpectedASCDataSet being returned with size " 
				+ setForCriteria.size(), logFileWriter);
		setForCriteria.addAllElements(setForCriteria.getUniqueNamesAndDepts(), logFileWriter);
		//this.addAllElements(this.getUniqueNamesAndDepts(), logFileWriter);
		return setForCriteria;		
	}//end getSetForStringSubsetCriteria method
	
	public ExpectedASCDataSet removeAllZeroDOShares(BufferedWriter logFileWriter) throws Exception{
		outputMessage("\n****method removeAllZeroDOShares has been called****\n", logFileWriter);
		ExpectedASCDataSet subset = new ExpectedASCDataSet();
		for(int i=0; i<myList.size(); i++){
			ExpectedASCData element = myList.get(i);
			if (element.hasNonZeroDOShare()) subset.add(element);
		}
		subset.addAllElements(subset.getUniqueNamesAndDepts(), logFileWriter);
		//this.addAllElements(this.getUniqueNamesAndDepts(), logFileWriter);
		return subset;
	}
	
	public ExpectedASCDataSet removeDeans(BufferedWriter logFileWriter) throws Exception{
		outputMessage("\n****method removeDeans has been called****\n", logFileWriter);
		ExpectedASCDataSet subset = new ExpectedASCDataSet();
		for(int i=0; i<myList.size(); i++){
			ExpectedASCData element = myList.get(i);
			if (! element.isDean()) subset.add(element);
		}//end for - iterating through the list
		subset.addAllElements(subset.getUniqueNamesAndDepts(), logFileWriter);
		return subset;
	}//end removeDeans method

	public ExpectedASCDataSet removeChairs(BufferedWriter logFileWriter) throws Exception{
		outputMessage("\n****method removeChairs has been called****\n", logFileWriter);
		ExpectedASCDataSet subset = new ExpectedASCDataSet();
		for(int i=0; i<myList.size(); i++){
			ExpectedASCData element = myList.get(i);
			if (! element.isDeptChair()) subset.add(element);
		}//end for - iterating through the list
		subset.addAllElements(subset.getUniqueNamesAndDepts(), logFileWriter);
		return subset;
	}//end removeChairs method

	public ExpectedASCDataSet removeFacultyNames (String[] facultyToExclude, BufferedWriter logFileWriter) throws Exception{
		String message = "\n****method removeFacultyNames has been called with faculty";
		for (int i=0; i<facultyToExclude.length; i++){
			message = message.concat(" "+facultyToExclude[i]);
		}
		message = message.concat("****\n");
		outputMessage(message, logFileWriter);
		ExpectedASCDataSet subset = new ExpectedASCDataSet();
		for(int i=0; i<myList.size(); i++){
			ExpectedASCData element = myList.get(i);
			int index = java.util.Arrays.asList(facultyToExclude).indexOf(element.getStringAttribute(ExpectedASCData.FacultyIndex));
			if (index == -1) subset.add(element);
		}//end for
		subset.addAllElements(subset.getUniqueNamesAndDepts(), logFileWriter);
		message = "\n****method removeFacultyNames is returning a subset of size " + subset.size() + " ****\n";
		outputMessage(message, logFileWriter);
		return subset;
	}
	public void addAllElements(TreeSet<String> uniqueNamesAndDepts, BufferedWriter logFileWriter) throws Exception{
		ascFacultyValuesSet.addAllElements(this.getUniqueNamesAndDepts(), logFileWriter);
	}
	
	public String[] getAllDiscreteStringAttributeValues(String attributeName, String requiredSubstring, BufferedWriter logFileWriter) throws Exception{
		return getAllDiscreteStringAttributeValues(attributeName, requiredSubstring, false, logFileWriter);
	}

	public String[] getAllDiscreteStringAttributeValues(String attributeName, BufferedWriter logFileWriter) throws Exception{
		return getAllDiscreteStringAttributeValues(attributeName, new String(), false, logFileWriter);
	}
	
	public String[] getAllDiscreteStringAttributeValues(String attributeName, boolean isASCData, BufferedWriter logFileWriter) throws Exception{
		return getAllDiscreteStringAttributeValues(attributeName, new String(), isASCData, logFileWriter);
	}

	public String[] getAllDiscreteStringAttributeValues(String attributeName, String requiredSubstring, boolean isASCData, BufferedWriter logFileWriter) throws Exception{
		outputMessage("*** method getAllDiscreteStringAttributeValues is being called ***\n", logFileWriter);
		outputMessage("name of attribute: "+attributeName + "\n", logFileWriter);
		outputMessage("number of expected elements in the list: " +size() + "\n", logFileWriter);
		outputMessage("number of ASCFacultyValues in this list: " + getNumberOfASCFacultyValues() + "\n", logFileWriter);
		if (isASCData) return ascFacultyValuesSet.getAllDiscreteStringAttributeValues(attributeName, requiredSubstring, logFileWriter);
		TreeSet<String> rawAttributeValues = new TreeSet<String>();
		for(int i=0; i<myList.size(); i++){
			String attribute = myList.get(i).getStringAttribute(attributeName);
			boolean toBeAdded = false;
			if ( (! attribute.isEmpty()) && (! attribute.equalsIgnoreCase(" ")) ){
				if (requiredSubstring.isEmpty()){
					toBeAdded = true;
				}//end if - there's no required substring, just add it
				else{
					if (attribute.contains(requiredSubstring)){
						toBeAdded = true;
					}//end if - it contains the required substring and is to be added
				}//end else - there's a required substring
			}//end if - the String is not empty or whitespace
			if (toBeAdded){
				rawAttributeValues.add(attribute);
				outputMessage("adding the following attribute: " + attribute +"\n", logFileWriter);
			}//end if - it is to be added
		}//end for
		String[] attribValues = new String[rawAttributeValues.size()];
		Iterator<String> iterator = rawAttributeValues.iterator();
		int i=0;
		outputMessage("method getAllDiscreteStringAttributeValues is returning the following values: ", logFileWriter);
		while(iterator.hasNext()){
			attribValues[i] = (String)iterator.next();
			outputMessage(attribValues[i++] + "; ", logFileWriter);
		}
		outputMessage("\n", logFileWriter);
		return attribValues;
	}//end getAllDiscreteStringAttributeValues method


	public int size(){
		return myList.size();
	}//end method size
	
	public int getNumberOfASCFacultyValues(){
		return ascFacultyValuesSet.size();
	}
	
	public void outputMessage(String message, BufferedWriter logFileWriter) throws Exception{
		TestFileController.writeToLogFile(message, logFileWriter);
		System.out.println(message);
	}//end method outputMessage

}
