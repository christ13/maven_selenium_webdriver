package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import java.util.TreeSet;

public class APFacultyValues {

	/*
	 * The Faculty Values will be read into this class – APFacultyValues – with similar structure 
	 * and similar methods.  The Name, FTE, Department, Dept %, Cluster, and 
	 * A&P Reference Value Tested will all be stored in a two-dimensional String array 
	 * with a fixed size.  Constants will be used so that no one has to remember what index 
	 * the Faculty member associated with each of the A&P Reference values is associated with.
	 * The FTE will usually be returned as a primitive float – something that can be multiplied 
	 * by a number (either an integer or another float) to yield the expected 
	 * FTE Adjusted DO Share Amount for that Faculty member.  
	 * This value is the one that is returned most frequently when the APFacultyValues class 
	 * is accessed.  Although one Faculty member can belong to two departments 
	 * (and two Clusters, potentially!) every Faculty member has one and only one FTE value, 
	 * so all that we need to retrieve that is the name of the Faculty member as a String input.
	 * Occasionally, the FTE will be returned as a String value, just for reporting purposes.  
	 * The A&P Reference Value associated with each Faculty member will always be returned 
	 * as a String value.  Each Faculty member will be associated with one and only one 
	 * A&P Reference Value, so it can be retrieved using only the name of the Faculty member 
	 * as a String input. 
	 * The Name of each Faculty member will always be returned as a String. 
	 * The list of Names of Faculty associated with a given Cluster 
	 * (Cluster Name is given as an input String parameter) will be returned as an array of 
	 * String Objects.
	 * The list of Names of Faculty associated with a given Department 
	 * (Department Name is given as an input String parameter) will be returned as an 
	 * array of String Objects.
	 * The list of Departments that each Faculty member is associated with can be returned 
	 * as an array of String Objects.
	 * The Dept % for each Faculty member for each Department that the Faculty member 
	 * is part of is returned as a primitive float given both the Faculty Name and the 
	 * Faculty Department Name as String inputs.
	 */
	
	
	
	private String[][] FacultyValues;
	
	private String sheetName;
	
	public static final int FacultyName = 0;
	public static final int FTE = 1;
	public static final int Department = 2;
	public static final int DeptPercent = 3;
	public static final int Cluster = 4;
	public static final int APReferenceValue = 5;
	
	public static final int numberOfColumns = 6;
	public static final int numberOfValues = 10;

	
	
	public APFacultyValues (String sheetName) {
		// this takes the name of a worksheet as an input parameter
		this.sheetName = sheetName;
		FacultyValues = new String[numberOfColumns][numberOfValues];
		System.out.println("Referenced worksheet is '" +sheetName +"' with " + TestFileController.fileIn.getRowCount(sheetName) +" rows");
		for (int ValueType=FacultyName; ValueType<=APReferenceValue; ValueType++){
			for (int Value=0; Value < numberOfValues; Value++){
				FacultyValues[ValueType][Value] = TestFileController.fileIn.getCellData(sheetName, ValueType, Value+2);
				//System.out.println("Reference Value at [" + ValueType +"][" + Value + "] is " + TestFileController.fileIn.getCellData(sheetName, ValueType, APType+2));
				System.out.println("Faculty Value at [" + ValueType +"][" + Value + "] is " + FacultyValues[ValueType][Value]);
			}//end inner for loop
		}//end outer for loop
	}//end constructor
	
	
	/*
	 * The goal of this method is to return a list (as an array of Strings) of discrete Faculty Names with no duplicates
	 */
	public String[] getUniqueFacultyNames(){
		String facultyNameString = "";
		for (int facultyNameIndex = 0; facultyNameIndex < numberOfValues; facultyNameIndex++){
			if (! TestFileController.fileIn.getCellData(sheetName, FacultyName, facultyNameIndex+2).equalsIgnoreCase(TestFileController.fileIn.getCellData(sheetName, FacultyName, facultyNameIndex+1))){
				facultyNameString = facultyNameString.concat(TestFileController.fileIn.getCellData(sheetName, FacultyName, facultyNameIndex+2) + ";");
			}//end if
		}//end for
		return facultyNameString.split(";");
	}//end method getFacultyNames
	
	public String[] getUniqueDepartmentNames(){
		HashSet<String> departmentNames = new HashSet<String>();
		for (int i=0; i<numberOfValues; i++){
			departmentNames.add(TestFileController.fileIn.getCellData(sheetName, Department, i+2));
		}//end for loop - adding department names to the HashSet
		List<String> departmentList = new ArrayList<String>(departmentNames);
		String[] departmentArray = new String[departmentList.size()];
		return departmentList.toArray(departmentArray);
	}//end getUniqueDepartmentNames method
	
	public String[] getUniqueClusterNames(){
		HashSet<String> clusterNames = new HashSet<String>();
		for (int i=0; i<numberOfValues; i++){
			clusterNames.add(TestFileController.fileIn.getCellData(sheetName, Cluster, i+2));
		}//end for loop - adding cluster names to the HashSet
		List<String> clusterList = new ArrayList<String>(clusterNames);
		String[] clusterArray = new String[clusterList.size()];
		return clusterList.toArray(clusterArray);
	}//end getUniqueClusterNames method
	
	public String getFTEStringValueForFacultyName(String facultyName){
		for (int ValueIndex=0; ValueIndex < numberOfValues; ValueIndex++){
			if (facultyName.equalsIgnoreCase(FacultyValues[FacultyName][ValueIndex])){
				//System.out.println("Match found: " + APFacultyValues[FacultyName][ValueIndex]);
				return FacultyValues[FTE][ValueIndex];
			}//end if
		}//end for loop
		System.out.println("FTE Match **NOT** found for Faculty member: '" + facultyName +"'");
		return "";
	}//end of getFTEStringValueForFacultyName method
	
	public String getStringDeptPercentForFacultyNameAndDept(String facultyName, String departmentName){
		for (int ValueIndex=0; ValueIndex < numberOfValues; ValueIndex++){
			if (facultyName.equalsIgnoreCase(FacultyValues[FacultyName][ValueIndex]) 
						&& (departmentName.equalsIgnoreCase(FacultyValues[Department][ValueIndex]))){
				return FacultyValues[DeptPercent][ValueIndex];
			}//end if - found an entry which matches both faculty name and department name
		}//end for loop - iterating through all entries
		System.out.println("Dept Percent Match **NOT** found for Faculty member: '" + facultyName 
					+"' and Department '" + departmentName +"'");
		return "";
	}//end of getStringDeptPercentForFacultyNameAndDept method - returns the dept percent as a String
	
	public String[][] getUniqueFacultyNameDepartmentCombinations(BufferedWriter logFileWriter) throws Exception{
		StringBuffer output = new StringBuffer();
		logFileWriter.newLine();
		logFileWriter.write("method getUniqueFacultyNameDepartmentCombinations has been called");
		logFileWriter.newLine();
		LinkedList<String[]> combinations = new LinkedList<String[]>();
		for (int rowIndex=0; rowIndex < numberOfValues; rowIndex++){
			if (! FacultyValues[Department][rowIndex].startsWith("(")){
				String[] validCombination = {FacultyValues[FacultyName][rowIndex], FacultyValues[Department][rowIndex]};
				combinations.add(validCombination);
				output.append("Added combination " + validCombination[0] + ", " + validCombination[1] +" to the TreeSet");
				logFileWriter.write(output.toString());
				System.out.println(output.toString());
				output = new StringBuffer();
				logFileWriter.newLine();
			}
		}
		String[][] finalCombinations = new String[2][combinations.size()];
		logFileWriter.newLine();
		output.append("Interim (LinkedList) Combination to be returned has the following size: " + combinations.size());
		System.out.println(output.toString());
		logFileWriter.write(output.toString());
		logFileWriter.newLine();
		for (int comboIndex = 0; comboIndex < finalCombinations[0].length; comboIndex++){
			finalCombinations[0][comboIndex] = combinations.get(comboIndex)[0];
			finalCombinations[1][comboIndex] = combinations.get(comboIndex)[1];
		}
		output = new StringBuffer();
		output.append("Final Combination (String array) to be returned has size: " + finalCombinations[0].length);
		System.out.println(output.toString());
		logFileWriter.write(output.toString());
		logFileWriter.newLine();
		for(int i=0; i<finalCombinations[0].length; i++){
			output = new StringBuffer();
			output.append("Final Combination at row index " + i + " is " + finalCombinations[0][i] + ", " + finalCombinations[1][i]);
			TestFileController.writeToLogFile(output.toString(), logFileWriter);
			System.out.println(output.toString());
		}
		logFileWriter.newLine();
		return finalCombinations;
	}
	
	public float getPrimitiveFloatDeptPercentForFacultyNameAndDept (String facultyName, String departmentName){
		return Float.parseFloat(getStringDeptPercentForFacultyNameAndDept(facultyName, departmentName))/100;
	}// end of getPrimitiveFloatDeptPercentForFacultyNameAndDept - returns the dept percent as primitive float
	
	public float getPrimitiveFloatFTEForFacultyName(String facultyName) throws ParseException{
		return Float.parseFloat(getFTEStringValueForFacultyName(facultyName))/100;
	}//end getPrimitiveFloatFTEForFacultyName method
	
	public double getPrimitiveDoubleDeptFTEForFacultyNameAndDept(String facultyName, String departmentName) throws Exception{
		return getPrimitiveFloatDeptPercentForFacultyNameAndDept (facultyName, departmentName) * getPrimitiveFloatFTEForFacultyName(facultyName);
	}// end getPrimitiveDoubleDeptFTEForFacultyNameAndDept method
	
	public String getStringDeptFTEForFacultyNameAndDept(String facultyName, String departmentName) throws Exception{
		return String.valueOf((int)(getPrimitiveDoubleDeptFTEForFacultyNameAndDept(facultyName, departmentName)*100));
	}//end getStringDeptFTEForFacultyNameAndDept method
	
	public String getAPReferenceValueForFacultyName(String facultyName){
		for (int ValueIndex=0; ValueIndex < numberOfValues; ValueIndex++){
			if (facultyName.equalsIgnoreCase(FacultyValues[FacultyName][ValueIndex])){
				//System.out.println("Match found: " + APFacultyValues[FacultyName][ValueIndex]);
				return FacultyValues[APReferenceValue][ValueIndex];
			}//end if
		}//end for loop
		System.out.println("AP Reference Match **NOT** found for Faculty member: '" + facultyName +"'");
		return "";
	}//end of getFTEStringValueForFacultyName method
	
	public String getDepartmentNameForFaculty(String facultyName) throws Exception{
		for (int ValueIndex=0; ValueIndex < numberOfValues; ValueIndex++){
			if (facultyName.equalsIgnoreCase(FacultyValues[FacultyName][ValueIndex])){
				return FacultyValues[Department][ValueIndex];
			}//end if
		}//end for loop
		System.out.println("Department Match **NOT** found for Faculty member: '" + facultyName +"'");
		return "";
	}//end of getDepartmentNameForFaculty method
	
	public String[] getFacultyNamesInDepartment (String departmentName){
		String facultyNameString = "";
		for (int index = 0; index < numberOfValues; index++){
			if (TestFileController.fileIn.getCellData(sheetName, Department, index+2).equalsIgnoreCase(departmentName)){
				facultyNameString = facultyNameString.concat(TestFileController.fileIn.getCellData(sheetName, FacultyName, index+2) + ";");
			}//end if
		}//end for
		return facultyNameString.split(";");
	}//end of getFacultyNamesInDepartment method

	
	public String[][] getAllValuesForCluster(String clusterName, BufferedWriter logFileWriter) throws Exception{
		String message = "method getAllValuesForCluster has been called";
		logFileWriter.newLine();
		logFileWriter.write(message);
		System.out.println(message);
		logFileWriter.newLine();
		LinkedList<String[]> subset= new LinkedList<String[]>();
		for (int i=0;i<numberOfValues;i++){
			if (FacultyValues[Cluster][i].equalsIgnoreCase(clusterName)){
				String[] values = new String[numberOfColumns];
				message = "adding the following array to the subset to be returned: ";
				for (int j=0;j<numberOfColumns;j++){
					values[j] = FacultyValues[j][i];
					message = message.concat(FacultyValues[j][i] +", ");
				}
				subset.add(values);
				System.out.println(message);
				TestFileController.writeToLogFile(message, logFileWriter);
			}//end if - it's a match and it's added
		}//end outer for - iterating through the faculty values
		String[][] finalSubset = new String[numberOfColumns][subset.size()];
		for(int i=0; i<subset.size();i++){
			for (int j=0; j<numberOfColumns; j++){
				finalSubset[j][i] = subset.get(i)[j];
			}
		}
		System.out.println("method getAllValuesForCluster "
						+"returning a String array with a first dimension of " + finalSubset.length
							+" and a second dimension of " + finalSubset[0].length);
		return finalSubset;
	}
	
	public String[][] getNameDeptCombosForCluster(String clusterName, BufferedWriter logFileWriter) throws Exception{
		System.out.println("method getNameDeptCombosForCluster has been called");
		String[][] allValuesForCluster = getAllValuesForCluster(clusterName, logFileWriter);
		String[][] facultyNamesAndDepartments = new String[2][allValuesForCluster[0].length];
		System.out.println("array to be returned has first dimension of 2 and second dimension of "
							+ allValuesForCluster[0].length);
		for (int i=0;i<allValuesForCluster[0].length;i++){
			facultyNamesAndDepartments[0][i] = allValuesForCluster[FacultyName][i];
			facultyNamesAndDepartments[1][i] = allValuesForCluster[Department][i];
			System.out.print("assigning faculty column a value of " + facultyNamesAndDepartments[0][i]);
			System.out.println(", and assigning department column a value of " + facultyNamesAndDepartments[1][i]);
		}
		return facultyNamesAndDepartments;
	}

	public String[] getFacultyNamesInCluster (String clusterName){
		String facultyNameString = "";
		for (int index = 0; index < numberOfValues; index++){
			if (TestFileController.fileIn.getCellData(sheetName, Cluster, index+2).equalsIgnoreCase(clusterName)){
				facultyNameString = facultyNameString.concat(TestFileController.fileIn.getCellData(sheetName, FacultyName, index+2) + ";");
			}//end if
		}//end for
		return facultyNameString.split(";");
	}//end getFacultyNamesInCluster method
	
	

}// end of APFacultyValues class
