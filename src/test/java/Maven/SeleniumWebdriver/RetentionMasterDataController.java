package Maven.SeleniumWebdriver;

import java.io.BufferedWriter;
import java.io.FileOutputStream;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class RetentionMasterDataController {
	
	protected static String testName;
	protected static String testCaseNumber;
	protected static String testDescription;
	protected static String testEnv;
	protected static StringBuffer verificationErrors;
	protected static BufferedWriter logFileWriter;
	protected static String[] IDStrings;

	private RetentionUIController retentionUIController;
	private FacultyUIController facultyUIController;
	private TestDataRecorder testDataRecorder;
	public static Retention[] retentions = new Retention[12];
	public static TestCaseData[] testCaseDataArray = new TestCaseData[12];


	public RetentionMasterDataController () throws Exception {
		testName = "RetentionMasterDataController";
		testEnv = "Test";
		testCaseNumber = "80";
		testDescription = "Test of the Retention Master Data Collector setup";
		verificationErrors = new StringBuffer();
	
		String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		logFileWriter = TestFileController.getLogFileWriter(logFileName);
		logFileWriter.write("RetentionMasterDataController Class has been instantiated.");
		logFileWriter.newLine();
		
		facultyUIController = new FacultyUIController();
		retentionUIController = new RetentionUIController();
		
		//testDataRecorder = new TestDataRecorder();
		TestDataRecorder.initializeRetentionDataFile();

		for (int i=1; i<=12; i++) {
			testCaseDataArray[i-1] = new TestCaseData(i);
			//retentions[i-1] = new Retention(i);
			logFileWriter.write("Retention Class for Test Case ID "+i+" has been instantiated");
			logFileWriter.newLine();
			logFileWriter.newLine();
			testCaseDataArray[i-1].closeDataFile();
		}//end for loop
	}//end constructor
	
	public static void writeToLogFile(String toBeWritten) throws Exception{
		logFileWriter.write(toBeWritten);
		logFileWriter.newLine();
	}//end writeToLogFile method
	
	public static void runAllReports(String beforeOrAfter) throws Exception {
		ReportRunner.runControlSheetReportSuite (ReportRunner.referenceWhichValues, logFileWriter);
		TestFileController.resetAllExcelReaders();
		Thread.sleep(10000);
		copyReportFile(TestFileController.ControlSheetReportSuiteFileName, beforeOrAfter);

	}//end runAllReports method
	
	public static void copyReportFile(String originalFileName, String beforeOrAfter) throws Exception {
		//String oldFileName = "CommitmentQueryExport.csv";
		//String srcDirectory = "\"C:\\Users\\christ13\\Documents\\HandSOn\\\"";
		String baseFileName = originalFileName.substring(0, originalFileName.lastIndexOf("."));
		String fileExtension = originalFileName.substring(originalFileName.lastIndexOf("."), originalFileName.length());
		String newFileName = baseFileName+" - " + beforeOrAfter + fileExtension;
		
		//of note, it may take up to six or possibly seven minutes for the CSV Export file to finish downloading to the "Downloads" directory
		TestFileController.copyFile(TestFileController.LogDirectoryName+"\\"+ReportRunner.referenceWhichValues+"\\", originalFileName, newFileName, ReportRunner.logDirectoryName, logFileWriter, 40);

	}//end copyReportFile method
	
	public static void closeDataFile() throws Exception{
		logFileWriter.close();
	}//end closeDataFile method
}//end Class
