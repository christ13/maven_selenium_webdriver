package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DeptSalarySettingWorksheetTheatreAndPerformanceAPRefValuesTest
		extends DeptSalarySettingWorksheetAPRefValuesTest {

	public void setUp() throws Exception {
		departmentName = "Theater & Performance Studies";
		myExcelReader = TestFileController.DepartmentSalarySettingTheaterAndPerformanceStudiesReport;
		worksheetName = TestFileController.DepartmentSalarySettingWorksheetName;
		myExcelReader.setRowForColumnNames(TestFileController.DepartmentSalarySettingWorksheetHeaderRow);
		testEnv = "Test";
		testName = "Test Department Salary Setting Worksheet Theatre And Perf";
		testDescription = "Verify AP Values in Department Salary Setting Worksheet Theatre And Perf";
		testCaseNumber = "014";
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void test() throws Exception{
		super.test();
	}

}
