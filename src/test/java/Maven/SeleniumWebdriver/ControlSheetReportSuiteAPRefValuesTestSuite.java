package Maven.SeleniumWebdriver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({
	ControlSheetDetailReportAPRefValuesTest.class/*, 
	ControlSheetPivotStageAPRefValuesTest.class, 
	ControlSheetPivotClusterAPRefValuesTest.class,
	ControlSheetPivotCategoryAPRefValuesTest.class,
	ControlSheetPivotSubCategoryAPRefValuesTest.class,
	ControlSheetPivotHumArtsAPRefValuesTest.class,
	ControlSheetPivotNatSciAPRefValuesTest.class,
	ControlSheetPivotSocSciAPRefValuesTest.class*/
	})

public class ControlSheetReportSuiteAPRefValuesTestSuite {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws Exception{
	}
	
}
