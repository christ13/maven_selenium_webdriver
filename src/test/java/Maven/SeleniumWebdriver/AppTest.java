package Maven.SeleniumWebdriver;

import java.util.concurrent.TimeUnit;

import org.junit.*;

import static org.junit.Assert.*;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;

import Maven.SeleniumWebdriver.SFDC_APC_Application_Variables;

public class AppTest {
	  private WebDriver driver;
	  private String baseUrl;
		private String directory = "C:/Users/christ13/Documents/HandSon/";
		private String inputFileName = "Input Data.xls";
		private String inputWorksheetName = "Test Input Data";
		private String outputFileName = "Test Results.xls";
		private String outputWorksheetName = "Passed Tests";
		private String errorWorksheetName = "Failed Tests";
		private boolean acceptNextAlert = true;
		private StringBuffer verificationErrors;
		//private String username;
		private static String browserName;
		private static String startPage = "apphs/processInboxTab.do";
		//private static String login;
		//private static String password = "";

		private HSSFExcelReader fileIn;
		private HSSFExcelReader fileOut;
		

	  @Before
	  public void setUp() throws Exception {
			browserName = System.getenv("browserName");
			System.out.println("Derived environmental variable browserName: " + browserName);
		
			if (browserName.equalsIgnoreCase(SFDC_APC_Application_Variables.CHROME_BROWSER)){
				driver = new ChromeDriver();
			}
			else if (browserName.equalsIgnoreCase(SFDC_APC_Application_Variables.FIREFOX_BROWSER)){
				driver = new FirefoxDriver();
			}
			else if (browserName.equalsIgnoreCase(SFDC_APC_Application_Variables.SAFARI_BROWSER)){
				driver = new SafariDriver();
			}
			else driver = new InternetExplorerDriver();
			
			
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			String envName = System.getenv("envName");
			System.out.println("Derived environmental name: " + envName);
			if (envName.equalsIgnoreCase("Test")){
				baseUrl = "https://handsonweb-"+HandSOnTestSuite.machineName+".stanford.edu/";
			}
			else{ 
				baseUrl ="https://handsonweb-dev2.stanford.edu/";
			}
			startPage = baseUrl + startPage;
			System.out.println("derived start page is as follows: " + startPage);
			

			fileIn = new HSSFExcelReader(directory + inputFileName);
			fileOut = new HSSFExcelReader(directory + outputFileName);
			
			  if (! fileOut.addSheet(outputWorksheetName)){
				  System.out.println("Removing and re-creating Output File Worksheet");
				  fileOut.removeSheet(outputWorksheetName);
				  fileOut.addSheet(outputWorksheetName);
			  }
			  fileOut.addColumn(outputWorksheetName, "Application ID");

			  if (! fileOut.addSheet(errorWorksheetName)){
				  System.out.println("Removing and re-creating Error Worksheet");
				  fileOut.removeSheet(errorWorksheetName);
				  fileOut.addSheet(errorWorksheetName);
			  }
			  fileOut.addColumn(errorWorksheetName, "Application ID");

	  }
	  @Test
	  public void testDataMigration() throws Exception {
		  verificationErrors = new StringBuffer();
		  driver.get(startPage);
	  }


	  @After
	  public void tearDown() throws Exception {
		    driver.quit();
		    String verificationErrorString = verificationErrors.toString();
		    if (!"".equals(verificationErrorString)) {
		    	System.out.println(verificationErrorString);
		      fail(verificationErrorString);
		    }
	  }
	  
	  

}