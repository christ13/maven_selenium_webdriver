package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.LinkedList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
/*
 * This is a superclass.  It contains most of the methods that will be inherited
 * by the JUnit test subclasses that will actually do the work
 */
public class ProvostBinderListByDepartmentAPRefValuesTest {
	public static final int FacultyName = 0;
	public static final int Description = 1;
	public static final int currentYearRegularMeritPool = 2;
	public static final int numberOfColumns = 3;
	
	protected static String worksheetName;
	protected static String departmentName;
	protected static SSExcelReader myExcelReader;
	
	protected static String testName;
	protected static String testCaseNumber;
	protected static String testDescription;
	protected static String testEnv;
	protected static StringBuffer verificationErrors;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws Exception{
		System.out.println("Now executing test: " + testName);
		verificationErrors = new StringBuffer();
		String logFileName = ReportRunner.logDirectoryName +"/" + testName +".txt";
		BufferedWriter logFileWriter = TestFileController.getLogFileWriter(logFileName);
		String[][] expectedValues = getExpectedValues(departmentName, logFileWriter);
		String[][] actualValues = getActualValues(departmentName, logFileWriter);
		verifyAPValues(expectedValues, actualValues, logFileWriter);
		logFileWriter.close();
		TestFileController.writeTestResults(testEnv, testCaseNumber, testName, testDescription, verificationErrors);
	}
	
	/*
	 * Given a department as input, we perform the following:
	 * (1) derive the names of the faculty that are in this department,
	 * (2) from the faculty names, obtain the corresponding AP types being tested, 
	 * and the 100% FTE amount promised for each AP Type.
	 */
	public String[][] getExpectedValues(String departmentName, BufferedWriter logFileWriter) throws Exception{
		StringBuffer message = new StringBuffer();
		String[] facultyNames = HandSOnTestSuite.apFacultyValues.getFacultyNamesInDepartment(departmentName);
		int numberOfRows = facultyNames.length;
		String[][] expectedValues = new String[numberOfColumns][numberOfRows];
		message=message.append("Expected values to be returned are as follows: ");
		for(int j=0;j<numberOfRows;j++){
			expectedValues[FacultyName][j] = facultyNames[j];
			expectedValues[Description][j] = HandSOnTestSuite.apFacultyValues.getAPReferenceValueForFacultyName(facultyNames[j]);
			expectedValues[currentYearRegularMeritPool][j] = 
					String.valueOf(HandSOnTestSuite.referenceValues.getFloatAmountForAPType(expectedValues[Description][j]).intValue());
			message.append("(row " + j +") " + expectedValues[FacultyName][j] + "; "
					+ expectedValues[Description][j] + "; "
					+ expectedValues[currentYearRegularMeritPool][j] + "; "
					);
		}//end for loop
		System.out.println(message.toString());
		logFileWriter.write(message.toString());
		return expectedValues;
		
	}

	/*
	 * Given a department name as input, derive the faculty names that we have associated
	 * with that department in the ReferenceValues and APFacultyValues Objects, 
	 * then pull the values for those names from the appropriate tab in the Provost Binder Report.
	 */
	public String[][] getActualValues(String departmentName, BufferedWriter logFileWriter) throws Exception {
		StringBuffer message = new StringBuffer();
		String[] facultyNames = HandSOnTestSuite.apFacultyValues.getFacultyNamesInDepartment(departmentName);
		int numberOfRows = facultyNames.length;
		String[][] actualValues = new String[numberOfColumns][numberOfRows];
		String[] columnNames = {"Name", HandSOnTestSuite.FY + " Description", HandSOnTestSuite.FY + " Regular Merit Pool"};
		for (int row=0; row<numberOfRows; row++){
			String name = facultyNames[row];
			HashMap<String, String> searchCriteria = new HashMap<String, String>();
			searchCriteria.put("Name", name);
			System.out.println("Search Criteria for " + worksheetName +" is as follows "+searchCriteria.toString());
			LinkedList<HashMap<String, String>> rawValues 
				= myExcelReader.getMultipleValuesInRows(worksheetName, searchCriteria, columnNames, logFileWriter);
			message.append("\nNumber of rows in " + worksheetName + " that match search criteria: " + rawValues.size());
			TestFileController.writeToLogFile(message.toString(), logFileWriter);
			System.out.println(message.toString());
			message.append("\nContents of the row returned are as follows: ");
			for (int column=0; column<columnNames.length; column++){
				if (rawValues.size() == 0){
					message.append("*** TEST FAILED *** search in the file for name '" + name +"' yielded no results");
					System.out.println(message.toString());
					TestFileController.writeToLogFile(message.toString(), logFileWriter);
					logFileWriter.close();
					TestFileController.writeTestResults(testEnv, testCaseNumber, testDescription, testName, message);
					fail(message.toString());
				}
				else if (rawValues.size() != 1){
					String warning = "*** WARNING *** There is more than one row with this search criteria - please adjust search criteria";
					System.out.println(warning);
					TestFileController.writeToLogFile(warning, logFileWriter);
				}
				//we're going to assume that there's always one and only one HashMap returned in the LinkedList
				actualValues[column][row] = (String)rawValues.get(0).get(columnNames[column]);
				message.append(actualValues[column][row] + "; ");
			}//end inner for - columns
		}//end outer for - rows
		return actualValues;
	}//end getActualValues method

	public void verifyAPValues (String[][] expectedValues, String[][] actualValues, BufferedWriter logFileWriter) throws Exception{
		verificationErrors = new StringBuffer();
		String message = "";
		logFileWriter.newLine();
		TestFileController.writeToLogFile("Method verifyAPValues executing", logFileWriter);
		message = "Expected vs Actual Value Comparisons are as follows: ";
		TestFileController.writeToLogFile(message, logFileWriter);
		System.out.println(message);
		int numberOfRows = expectedValues[0].length;
		for (int row=0; row<numberOfRows; row++){
			for (int column=0; column<numberOfColumns; column++){
				message = "Expected: " + expectedValues[column][row] + " vs Actual: " + actualValues[column][row];
				logFileWriter.write(message);
				System.out.print(message);
				if (column == currentYearRegularMeritPool){
					double expected = Double.valueOf((Long) NumberFormat.getNumberInstance(java.util.Locale.US).parse(expectedValues[column][row]));
					double actual = Double.valueOf((Long) NumberFormat.getNumberInstance(java.util.Locale.US).parse(actualValues[column][row]));
					//do float-to-float comparison
					logFileWriter.write(" (Number-to-number comparison invoked) - expected number is " + expected +", actual number is "+actual);
					System.out.print(" (Number-to-number comparison invoked) - expected number is " + expected +", actual number is "+actual);
					if (expected == actual){
						System.out.print(", match");
						logFileWriter.write(", match");
					}//end if - it matches
					else{
						verificationErrors.append("MISMATCH: " + message);
						logFileWriter.write(", MISMATCH");
						System.out.print(", MISMATCH");
					}//end else - it doesn't match
					
				}//end if - number-to-number comparison
				else{
					logFileWriter.write(" (String-to-String comparison invoked)");
					//do String-to-String comparison
					if (expectedValues[column][row].equalsIgnoreCase(actualValues[column][row])){
						logFileWriter.write(", match");
					}//end if - it matches
					else{
						verificationErrors.append("MISMATCH: " + message);
						System.out.print(", MISMATCH");
						logFileWriter.write(", MISMATCH");
					}//end else - it doesn't match
				}//end outer else - it's not the 16-17 Regular Merit Pool, thus requiring String-to-String comparison
				logFileWriter.newLine();
				System.out.println();
			}//end inner for loop - columns
		}//end outer for loop - rows
	}//end verifyAPValues method


}
