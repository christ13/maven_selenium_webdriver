package Maven.SeleniumWebdriver;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ControlSheetPivotNatSciClusterReportASCTest extends
		ControlSheetPivotOneClusterReportASCTest {

	@Before
	public void setUp() throws Exception {
		clusterName = "Natural Sciences Cluster";

		mySheetName = TestFileController.ControlSheetPivot_NatSciWorksheetName;
		testName = "ControlSheetPivotNatSciClusterReportASCTest";
		testCaseNumber = "029";
		testDescription = "ASC Test of Pivot NatSci Cluster Report";
		logFileName = "Control Sheet Pivot NatSci Cluster ASC Test.txt";
		
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void test() throws Exception{
		super.test();
	}

}
