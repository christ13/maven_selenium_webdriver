package ExportCSV;

import static org.junit.Assert.fail;

import java.io.BufferedWriter;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import Maven.SeleniumWebdriver.HandSOnTestSuite;
import Maven.SeleniumWebdriver.TestFileController;

public class UIController {

	public static WebDriver driver;
	public static final String baseUrl = "https://handsonweb-"+HandSOnTestSuite.machineName+".stanford.edu";
	
	public UIController() {
		driver = new ChromeDriver();
	}
	
	public static boolean fillInTextField(By by, String toBeWritten) throws Exception{
		waitForElementPresent(by);
		WebElement textfield = driver.findElement(by);
		textfield.clear();
		textfield.sendKeys(toBeWritten);

		String actualText = textfield.getAttribute("value");
		System.out.println("Value derived from text field is "+actualText);
		if (! actualText.equalsIgnoreCase(toBeWritten)){
			System.out.println("Could not fill in the first name element");
			return false;
		}//end if
		else return true;
	}//end fillInTextField method
	
	  public static boolean waitAndClick (By by) throws Exception{
		    //System.out.print("waiting for the following element to render so it can be clicked ");
		    //System.out.println(by.toString());
		    for (int second = 0;; second++) {
		    	if (second >= 30) return false;
		    	try { if (isElementClickable(by)) break; } catch (Exception e) {}
		    	Thread.sleep(1000);
		    }//end for
		    return true;
	  }//end waitAndClick method
	  
	  public static boolean isElementClickable (By by){
		  try{
				  driver.findElement(by).click();
		  }//end try
		  catch(WebDriverException e){
				  return false;
		  }//end catch
		  return true;
	  }//end isElementClickable method


	  public static boolean isElementPresent(By by) {
		    try {
		      driver.findElement(by);
		      return true;
		    }//end try clause
		    catch (NoSuchElementException e) {
		      return false;
		    }//end catch clause
	  }//end isElementPresent method


	  public static void waitForElementPresent (By by) throws Exception{
		    for (int second = 0;; second++) {
		    	if (second >= 15){
		    		System.out.println("element not found: "+by.toString());
		    		fail("timeout - element not present: " + by.toString());
		    	}//end if
		    	try {
		    			if (isElementPresent(by)) {
		    				System.out.println("Element found: "+by.toString());
		    				break; 
		    			}//end if
		    	}//end try 
		    	catch (Exception e) {}
		    	Thread.sleep(1000);
		    }//end for
	  }//end waitForElementPresent method
	  
	  public static boolean switchWindowByURL(String URL) throws Exception{
		  String currentWindow = null;
		  Set<String> availableWindows;
		  try {
	            availableWindows = driver.getWindowHandles(); 
	            currentWindow = (String) availableWindows.toArray()[0];
	            //currentWindow = driver.getWindowHandle(); 
	            if (! availableWindows.isEmpty()) { 
		            for (String windowId : availableWindows) {
		                String switchedWindowURL=driver.switchTo().window(windowId).getCurrentUrl();
		                System.out.println("Examining window with URL " + driver.getCurrentUrl());
		                if ((switchedWindowURL.equals(URL))||(switchedWindowURL.contains(URL))){
		                	System.out.println("Found Window with URL " + driver.getCurrentUrl());
		                    return true; 
		                } else { 
		                	System.out.println("attempting to switch back to window with URL " + URL);
		                	driver.switchTo().window(currentWindow); 
		                } 
		            } 
	            }
	        }
	        catch (Exception e){
	        	System.out.println("Couldn't finish searching for window with URL "+ URL +" because of Exception " + e.getMessage());
	        	if (e.getMessage().contains("target window already closed")){
	        		Thread.sleep(2000);
	        		currentWindow = (String) driver.getWindowHandles().toArray()[0];
	        		return switchWindowByURL(URL);
	        	}//end if
	        	else if (e.getMessage().contains("unexpected alert open")){
	        		try{
	        			driver.switchTo().alert().accept();
	        		}//end try
	        		catch(Exception e2){
	        			System.out.println("Couldn't close the alert");
	        		}//end catch
	        	}//end else if
	        	return false;
	        }
		  	System.out.println("Couldn't locate window with URL " + URL);
	        return false;
	   }//end switchWindowByURL method


}
