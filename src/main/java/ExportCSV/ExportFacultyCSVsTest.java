package ExportCSV;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import Maven.SeleniumWebdriver.HandSOnTestSuite;


public class ExportFacultyCSVsTest {
	
	private WebDriver driver;
	private String baseUrl;


	@Before
	public void setUp() throws Exception {
		driver = new ChromeDriver();
		baseUrl = "https://handsonweb-"+HandSOnTestSuite.machineName+".stanford.edu";
	}

	@After
	public void tearDown() throws Exception {
	    driver.quit();
	}

	@Test
	public void test() throws Exception{
		
		String expectedFirstNameValue = "Kyle";
		String expectedLastNameValue = "Bagwell";

		driver.get(baseUrl + "/tst/showDocumentsTab.do");
		waitAndClick(By.linkText("Operations"));
		Thread.sleep(2000);
		waitAndClick(By.id("Faculty Search"));
		Thread.sleep(2000);
		driver.get(baseUrl + "/tst/processManageFaculty.do?ACTION=showFacultySearch#results");
		//By by = By.name("facultyQueryDTO.firstName");
		By by = By.id("facultyName");
		
		waitForElementPresent(by);
		WebElement firstName = driver.findElement(by);
		firstName.clear();
		firstName.sendKeys(expectedFirstNameValue);

		String firstNameValue = firstName.getAttribute("value");
		System.out.println("Value derived as first name is "+firstNameValue);
		if (! firstNameValue.equalsIgnoreCase(expectedFirstNameValue))
			fail("Could not fill in the first name element");

		driver.findElement(By.name("facultyQueryDTO.lastName")).clear();
		driver.findElement(By.name("facultyQueryDTO.lastName")).sendKeys(expectedLastNameValue);
		waitAndClick(By.xpath("//div[2]/table/tbody/tr[2]/td/a"));
		Thread.sleep(2000);
		waitAndClick(By.xpath("//tr[44]/td/a"));
		Thread.sleep(2000);
		driver.get(baseUrl + "/tst/processManageFaculty.do?ACTION=openExportPage");
		waitAndClick(By.xpath("//a[contains(@href, \"javascript:selectOperate('ALL')\")]"));
		Thread.sleep(2000);
		waitAndClick(By.id("buttonSubmit"));
		Thread.sleep(2000);
		waitAndClick(By.xpath("//a[contains(@href, \"javascript:selectOperate('Export #2')\")]"));
		Thread.sleep(2000);
		waitAndClick(By.id("buttonSubmit"));
		Thread.sleep(2000);
		waitAndClick(By.xpath("//a[contains(@href, \"javascript:selectOperate('Export #3')\")]"));
		Thread.sleep(2000);
		waitAndClick(By.id("buttonSubmit"));
		Thread.sleep(2000);
		waitAndClick(By.xpath("//a[contains(@href, \"javascript:selectOperate('NONE')\")]"));
		Thread.sleep(2000);
		waitAndClick(By.id("buttonSubmit"));

	}
	
	  private boolean isElementClickable (By by){
		  try{
				  driver.findElement(by).click();
		  }
		  catch(WebDriverException e){
				  return false;
		  }
		  return true;
	  }
	  
	  private boolean waitAndClick (By by) throws Exception{
		    //System.out.print("waiting for the following element to render so it can be clicked ");
		    //System.out.println(by.toString());
		    for (int second = 0;; second++) {
		    	if (second >= 30) return false;
		    	try { if (isElementClickable(by)) break; } catch (Exception e) {}
		    	Thread.sleep(1000);
		    }
		    return true;

	  }

	  private boolean isElementPresent(By by) {
		    try {
		      driver.findElement(by);
		      return true;
		    } catch (NoSuchElementException e) {
		      return false;
		    }
		  }

	  private void waitForElementPresent (By by) throws Exception{
		    for (int second = 0;; second++) {
		    	if (second >= 15){
		    		System.out.println("element not found: "+by.toString());
		    		fail("timeout - element not present: " + by.toString());
		    	}//end if
		    	try {
		    			if (isElementPresent(by)) {
		    				System.out.println("Element found: "+by.toString());
		    				break; 
		    			}//end if
		    	} //end try
		    	catch (Exception e) {}
		    	Thread.sleep(1000);
		    }//end for	    
	  }//end waitForElementPresent method


}
