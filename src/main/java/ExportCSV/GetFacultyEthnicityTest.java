package ExportCSV;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class GetFacultyEthnicityTest {

	private WebDriver driver;
	private String baseUrl;

	@Before
	public void setUp() throws Exception {
		UIController controller = new UIController();
		driver = UIController.driver;
		baseUrl = UIController.baseUrl;
	}

	@After
	public void tearDown() throws Exception {
	    driver.quit();
	}

	@Test
	public void test() throws Exception{
		
		String expectedFirstNameValue = "Caroline";
		String expectedLastNameValue = "Hoxby";

		driver.get(baseUrl + "/apphs/showDocumentsTab.do");
		UIController.waitAndClick(By.linkText("Operations"));
		Thread.sleep(2000);
		UIController.waitAndClick(By.id("Faculty Search"));
		Thread.sleep(2000);
		UIController.switchWindowByURL(baseUrl + "/apphs/processManageFaculty.do?ACTION=showFacultySearch#results");
		
		if (! UIController.fillInTextField(By.id("facultyName"), expectedFirstNameValue)) 
			fail("Couldn't fill in the first name text field");

		if (! UIController.fillInTextField(By.name("facultyQueryDTO.lastName"), expectedLastNameValue))
			fail("Couldn't fill in the last name text field");
		
		if (! UIController.waitAndClick(By.xpath("//body/table/tbody/tr/td/table[2]/tbody/tr/td/form/table[2]/tbody/tr/td/div[9]/table/tbody/tr[2]/td[1]/a")))
			fail("Couldn't click on the link to the faculty record for "+expectedFirstNameValue +" "+expectedLastNameValue);
		
		Thread.sleep(5000);
		
		if (! UIController.waitAndClick(By.id("demographics_a"))){
			fail("Couldn't click on the demographics tab for the faculty record for "+expectedFirstNameValue +" "+expectedLastNameValue);
		}//end if
		
		Thread.sleep(2000);
		
		
	}//end test method

/*	
	  private boolean waitAndClick (By by) throws Exception{
		    //System.out.print("waiting for the following element to render so it can be clicked ");
		    //System.out.println(by.toString());
		    for (int second = 0;; second++) {
		    	if (second >= 30) return false;
		    	try { if (isElementClickable(by)) break; } catch (Exception e) {}
		    	Thread.sleep(1000);
		    }//end for
		    return true;
	  }//end waitAndClick method
	  
	  private boolean isElementClickable (By by){
		  try{
				  driver.findElement(by).click();
		  }//end try
		  catch(WebDriverException e){
				  return false;
		  }//end catch
		  return true;
	  }//end isElementClickable method


	  private boolean isElementPresent(By by) {
		    try {
		      driver.findElement(by);
		      return true;
		    }//end try clause
		    catch (NoSuchElementException e) {
		      return false;
		    }//end catch clause
	  }//end isElementPresent method


	  private void waitForElementPresent (By by) throws Exception{
		    for (int second = 0;; second++) {
		    	if (second >= 15){
		    		System.out.println("element not found: "+by.toString());
		    		fail("timeout - element not present: " + by.toString());
		    	}//end if
		    	try {
		    			if (isElementPresent(by)) {
		    				System.out.println("Element found: "+by.toString());
		    				break; 
		    			}//end if
		    	}//end try 
		    	catch (Exception e) {}
		    	Thread.sleep(1000);
		    }//end for
	  }//end waitForElementPresent method
	  
	  */
}
