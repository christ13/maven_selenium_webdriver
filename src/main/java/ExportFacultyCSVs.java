import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;

import Maven.SeleniumWebdriver.HandSOnTestSuite;


public class ExportFacultyCSVs {
	
	private WebDriver driver;
	private String baseUrl;


	@Before
	public void setUp() throws Exception {
		driver = new ChromeDriver();
		baseUrl = "https://handsonweb-"+HandSOnTestSuite.machineName+".stanford.edu";
	}

	@After
	public void tearDown() throws Exception {
	    driver.quit();
	}

	@Test
	public void test() throws Exception{

		driver.get(baseUrl + "/apphs/showDocumentsTab.do");
		waitAndClick(By.linkText("Operations"));
		Thread.sleep(2000);
		waitAndClick(By.linkText("Faculty Search"));
		Thread.sleep(2000);
		driver.get(baseUrl + "/apphs/processManageFaculty.do?ACTION=showFacultySearch#results");
		waitForElementPresent(By.id("facultyName"));
		driver.findElement(By.id("facultyName")).clear();
		driver.findElement(By.id("facultyName")).sendKeys("Kyle");
		driver.findElement(By.name("facultyQueryDTO.lastName")).clear();
		driver.findElement(By.name("facultyQueryDTO.lastName")).sendKeys("Bagwell");
		waitAndClick(By.xpath("//div[2]/table/tbody/tr[2]/td/a"));
		Thread.sleep(2000);
		waitAndClick(By.xpath("//tr[44]/td/a"));
		Thread.sleep(2000);
		driver.get(baseUrl + "/apphs/processManageFaculty.do?ACTION=openExportPage");
		waitAndClick(By.xpath("//a[contains(@href, \"javascript:selectOperate('ALL')\")]"));
		Thread.sleep(2000);
		waitAndClick(By.id("buttonSubmit"));
		Thread.sleep(2000);
		waitAndClick(By.xpath("//a[contains(@href, \"javascript:selectOperate('Export #2')\")]"));
		Thread.sleep(2000);
		waitAndClick(By.id("buttonSubmit"));
		Thread.sleep(2000);
		waitAndClick(By.xpath("//a[contains(@href, \"javascript:selectOperate('Export #3')\")]"));
		Thread.sleep(2000);
		waitAndClick(By.id("buttonSubmit"));
		Thread.sleep(2000);
		waitAndClick(By.xpath("//a[contains(@href, \"javascript:selectOperate('NONE')\")]"));
		Thread.sleep(2000);
		waitAndClick(By.id("buttonSubmit"));

	}
	
	  private boolean isElementClickable (By by){
		  try{
				  driver.findElement(by).click();
		  }
		  catch(WebDriverException e){
				  return false;
		  }
		  return true;
	  }
	  
	  private boolean waitAndClick (By by) throws Exception{
		    //System.out.print("waiting for the following element to render so it can be clicked ");
		    //System.out.println(by.toString());
		    for (int second = 0;; second++) {
		    	if (second >= 30) return false;
		    	try { if (isElementClickable(by)) break; } catch (Exception e) {}
		    	Thread.sleep(1000);
		    }
		    return true;

	  }

	  private boolean isElementPresent(By by) {
		    try {
		      driver.findElement(by);
		      return true;
		    } catch (NoSuchElementException e) {
		      return false;
		    }
		  }

	  private void waitForElementPresent (By by) throws Exception{
		    for (int second = 0;; second++) {
		    	if (second >= 30) fail("timeout - element not present: " + by.toString());
		    	try { if (isElementPresent(by)) break; } catch (Exception e) {}
		    	Thread.sleep(1000);
		    }
	  }


}
